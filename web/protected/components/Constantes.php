<?php

/**
 * Listado de constantes de las tablas maestras de las bases de datos, a excepcion del UserGroups
 *
 * 
 */
class Constantes 
{
    
    //inicio: variable genericas sin tablas maestras
    const ESTATUS_ACTIVO='A';
    const ESTATUS_INACTIVO='I';
    const DESC_ESTATUS_ACT="ACTIVO"; // Descripcion para el estatus activo
    const DESC_ESTATUS_INACT="INACTIVO"; // Descripcion para el estatus inactivo
    const NAC_VENEZOLANO='V'; // NAC: NACIONALIDAD
    const NAC_EXTRANJERO='E';
    const NAC_PASAPORTE='P';
    const DESC_NAC_VENEZOLANO="VENEZOLANO"; // Descripcion Nacionalidad Extranjero
    const DESC_NAC_EXTRANJERO="EXTRANJERO"; // Descripcion Nacionalidad Extranjero
    const DESC_NAC_PASAPORTE="PASAPORTE"; // Descripcion Nacionalidad Pasaporte
    const DESC_NAC_OTRO="NO REGISTRADO"; // Descripcion Nacionalidad Otros
    const MAX_HORAS_ASIGNADAS=54; // Maximo de Horas Asignadas para Personal (Administrativo,Docente,Obrero)
    const MIN_HORAS_ASIGNADAS=1; // Maximo de Horas Asignadas para Personal (Administrativo,Docente,Obrero)
    const ESTATUS_PROCESO_ACTIVO="A";
    const ESTATUS_PROCESO_CERRADO="C";

    // fin: variables genericas sin tablas maestras
    
    

    /* INICIO DE VARIABLES PARA EL ESQUEMA: Personal, Tabla: tipo_personal  */
    const ADMINISTRATIVO=1;
    const OBRERO=2;
    const DOCENTE = 3;
    const COORDINADOR = 5;
    const ENFERMERA = 6;
    const MENSAJERO = 7;
    const LOCUTOR = 8;    
    /* FIN DE VARIABLES PARA EL ESQUEMA: Personal, Tabla: tipo_personal  */
    
    
    
        /* INICIO DE VARIABLES PARA EL ESQUEMA: Personal, Tabla: personal_condicion_nomina  */
    const PERS_COND_ACT_EN_PLANTEL=1; // Activo en el plantel
    const PERS_COND_COBRA_PLANTEL_LAB_OTR=2; // Cobra en plantel y labora en otro
    const PERS_COND_LABORA_PLANTEL_COB_OTR = 3; // labora en plantel y cobra en otro
    const PERS_COND_OTROS = 4; // otro estatus que surja y que no ha sido definido aun
   
    /* FIN DE VARIABLES PARA EL ESQUEMA: Personal, Tabla: personal_condicion_nomina  */
    
    
    
        
        /* INICIO DE VARIABLES PARA EL ESQUEMA: Personal, Tabla: denominacion  */
    const PERS_DENOM_DOC_AULA=1; //  Docente aula
    const PERS_DENOM_DOC_COORD=2; // Docente coordinador
    const PERS_DENOM_DOC_DIREC = 3; // Docente directivo
    const PERS_DENOM_DOC_SUPERV = 4; // Docente supervisor
    const PERS_DENOM_DOC_OTRO = 5; // Otro tipo de docente
   
    /* FIN DE VARIABLES PARA EL ESQUEMA: Personal, Tabla: denominacion  */
    
    
    
    

} // Fin de la clase para el empleo de las contantes de los modulos de tablas maestras 

