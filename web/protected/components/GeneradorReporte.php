<?php
/**
 * Author: Maikel Ortega Hernandez
 * Email: maikeloh@gmail.com
 * Date: 30/10/14
 * Time: 22:24
 *
 *  Estructura Parametros Generador de reportes:
 *  parametros en formato json
 *
 *   select
 *     nombre_campo,
 *     [nombre_campo, alias_campo ]
 *     …
 *   from
 *     nombre_tabla
 *     [nombre_tabla, alias_tabla]
 *   left_join
 *     [[relacion, alias], condicion_para_on]
 *     ...
 *   where
 *     condicional
 *   order_by
 *     ordenamiento
 *   group_by
 *     agrupamiento
 *   union
 *     select
 *       nombre_campo,
 *       [nombre_campo, alias_campo ]
 *       …
 *     from
 *       nombre_tabla
 *       [nombre_tabla, alias_tabla]
 *     left_join
 *       [[relacion, aslias], condicion_para_on]
 *       ...
 *     where
 *       condicional
 *     order_by
 *       ordenamiento
 *
 */



class GeneradorReporte extends CApplicationComponent {

    protected $tableName;
    protected $sqlQuery = null;
    protected $jsonQuery;
    protected $tableMetadata;

    public function setJsonQuery( $query )
    {
        if(!is_array($query)){
            throw new InvalidArgumentException("The param query must be array type.");
        }
        $this->sqlQuery = null;
        $this->sqlQuery = '';
        $this->proccessJsonQuery($query);
        $this->endSqlQuery();
        $this->cleanUnwantedSqlInyections();
        //throw new Exception($this->sqlQuery);

    }

    public function getData()
    {
        /**
        $db = Yii::app()->db;

        $meta = $db->schema->getTable('gplantel.cargo');
        $gplantel = $meta->columnNames;
         */

        if($this->sqlQuery === null){
            throw new Exception('There is not query to execute defined. Check the paremeters.');
        }
        $query     = Yii::app()->db->createCommand($this->sqlQuery);
        $resultado = $query->queryAll();

        if( isset($this->jsonQuery['output']) ){
            switch($this->jsonQuery['output']){
                case 'json':
                    return $this->generateOutputJson($resultado);
                    break;
                case 'csv':
                    return $this->generateOutputCsv($resultado);
                    break;
            }
        }


        return $resultado;
    }


    protected function proccessJsonQuery($query)
    {

        $requiredParts = array('select', 'from');
        foreach($requiredParts as $part){
            if(!isset($query[$part])){
                throw new Exception("Required query part: $part is not defined in parameters.");
            }
        }
        if( !is_array($query['select']) ){
            throw new Exception("Required query part: 'select' has incorrect values.");
        }

        $table = $query['from'];
        $this->setTableName($table);
        $this->checkTableName();
        $this->jsonQuery = $query;
        $this->checkColumnsNames();
        $this->addFromTable();

        $this->processJoins();
        //inners joins
        //$this->proccessInnerJoins();
        //left joins
        //$this->proccessLeftJoins();
        //where
        $this->processWhere();
        //order by
        $this->processOrderBy();
        //group by
        $this->processGroupBy();
        //union
        $this->proccessUnion();
    }

    protected function setTableName($tableName)
    {
        if( !is_array($tableName) ){
            $tableAlias = uniqid("table_");
            $this->tableName = array(
                $tableName,
                $tableAlias
            );
        } else {
            $this->tableName = array(
                $tableName[0],
                $tableName[1]
            );
        }
    }

    protected function checkTableName()
    {
        $db    = Yii::app()->db;
        $this->tableMetadata = $db->schema->getTable($this->tableName[0]);

        if($this->tableMetadata === null){
            throw new Exception("Table name '".$this->tableName[0]."' is incorrect. ");
        }
    }

    protected function checkColumnsNames()
    {
        $columnsNames = $this->tableMetadata->columnNames;
        $this->sqlQuery .= 'SELECT ';
        $append = ', ';
        $len    = count($this->jsonQuery['select']);

        $patron_alias = "/{$this->tableName[1]}\./";

        foreach( $this->jsonQuery['select']  as $index => $selectField){
            if( $index == $len - 1 ){
                $append = ' ';
            }

            if( is_array($selectField) ){
                $cleanFieldName = preg_replace($patron_alias, "", $selectField[0] );
                if( (!in_array($cleanFieldName, $columnsNames) || $selectField[0] == '*') && preg_match("/^{$this->tableName[1]}\./", $selectField[0]) == true ){
                    throw new Exception('Your query has incorrect columns in select clause: '. $selectField[0]);
                }
                $this->sqlQuery .= "{$selectField[0]} as {$selectField[1]}{$append}";
            } elseif(is_string($selectField)){
                $cleanFieldName = preg_replace($patron_alias, "", $selectField);
                if( !in_array($cleanFieldName, $columnsNames) && $selectField != '*' && preg_match("/^{$this->tableName[1]}\./", $selectField) == true ){
                    throw new Exception('Your query has incorrect columns in select clause: '. $selectField);
                }
                $this->sqlQuery .= "{$selectField}{$append}";
            }
        }
    }

    protected function addFromTable()
    {
        $this->sqlQuery .= "FROM {$this->tableName[0]} {$this->tableName[1]}";
    }

    protected function endSqlQuery()
    {
        $this->sqlQuery .= ';';
    }


    protected function processJoins()
    {
        foreach($this->jsonQuery as $key => $join){
            switch($key){
                case 'left_join':
                    $this->proccessLeftJoins();
                    break;
                case 'inner_join':
                    $this->proccessInnerJoins();
                    break;
                default:
                    break;
            }
        }
    }

    protected function proccessLeftJoins()
    {
        if(!isset($this->jsonQuery['left_join'])){
            return;
        }

        if(!is_array($this->jsonQuery['left_join'])){
            throw new Exception('The left joins values are not valid.');
        }

        foreach( $this->jsonQuery['left_join'] as $left_join){
            if( !is_array($left_join) || !is_array($left_join[0])){
                throw new Exception('The left joins values are incorrects.');
            }
            $this->sqlQuery .= " LEFT JOIN {$left_join[0][0]} {$left_join[0][1]} ON {$left_join[1]}";
        }
    }

    protected function proccessInnerJoins()
    {
        if(!isset($this->jsonQuery['inner_join'])){
            return;
        }

        if(!is_array($this->jsonQuery['inner_join'])){
            throw new Exception('The inner joins values are not valid.');
        }

        foreach( $this->jsonQuery['inner_join'] as $left_join){
            if( !is_array($left_join) || !is_array($left_join[0])){
                throw new Exception('The inner joins values are incorrects.');
            }
            $this->sqlQuery .= " INNER JOIN {$left_join[0][0]} {$left_join[0][1]} ON {$left_join[1]}";
        }
    }

    protected function processWhere()
    {
        if(isset($this->jsonQuery['where'])){
            $this->sqlQuery .= " WHERE {$this->jsonQuery['where']}";
        }
    }

    protected function processOrderBy()
    {
        if(isset($this->jsonQuery['order_by'])){
            $this->sqlQuery .= " ORDER BY {$this->jsonQuery['order_by']}";
        }
    }

    protected function processGroupBy()
    {
        if(isset($this->jsonQuery['group_by'])){
            $this->sqlQuery .= " GROUP BY {$this->jsonQuery['group_by']}";
        }
    }

    protected function proccessUnion()
    {
        if(!isset($this->jsonQuery['union'])){
            return;
        }

        if(!is_array($this->jsonQuery['union'])){
            throw new Exception('The union data structure are incorrect.');
        }

        foreach($this->jsonQuery['union'] as $union){
            $this->sqlQuery .= " UNION ";
            $this->proccessJsonQuery($this->jsonQuery['union']);
        }
    }

    protected function cleanUnwantedSqlInyections()
    {
        //$this->sqlQuery .= "INSERT INTO pepe (`a`, `b`) values ('1', '2', '3');DROP table `pepito`;";

        $patrones = array(
            "/INSERT(.*);/",
            "/UPDATE(.*);/",
            "/DROP(.*);/",
            "/DELETE(.*);/"
        );
        //$patrones = array("INSERT");
        $cleanSQl = $this->sqlQuery;
        foreach($patrones as $patron){

            $result = preg_replace($patron, "", $cleanSQl);
            if($result != null){
                $cleanSQl = $result;
            }
        }
        $this->sqlQuery = $cleanSQl;
    }

    protected function generateOutputCsv($resultado)
    {
        ob_start();
        $csv = fopen("php://output", 'w');
        fputcsv($csv, array_keys(reset($resultado)), ';');
        foreach ($resultado as $row) {
            fputcsv($csv, $row, ';');
        }
        fclose($csv);
        return ob_get_clean();
    }

    protected function generateOutputJson($resultado)
    {
        return CJSON::encode($resultado);
    }
} 