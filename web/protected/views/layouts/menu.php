<?php

//Se encarga de capturar los items a pintar en el Menu
function getMenu() {

    $items = array();
    $sub_items = array();

    //Inicio
    $items[] = array('name' => 'Inicio', 'code' => 'inicio', 'icon' => 'icon-home', 'link' => array('/site'));

    //Zona Educativa
    if (Yii::app()->user->pbac('zonaEducativa.zonaEducativa.read')) {
        $items[] = array('name' => 'Zonas Educativas', 'code' => 'zonaEducativa', 'icon' => 'icon-building', 'link' => array('/zonaEducativa'));
    }

    //Planteles
    if (Yii::app()->user->pbac('planteles.consultar.read') || (Yii::app()->user->pbac("planteles.consultar.write"))) {
        $items[] = array('name' => 'Planteles', 'code' => 'planteles', 'icon' => 'icon-institution', 'link' => array('/planteles/consultar/'));
    }

    //Estudiante
    if (Yii::app()->user->pbac('estudiante.consultar.read') || (Yii::app()->user->pbac("estudiante.consultar.write")) || (Yii::app()->user->pbac("estudiante.consultar.admin"))) {
        $items[] = array('name' => 'Estudiantes', 'code' => 'estudiantes', 'icon' => 'icon-group', 'link' => array('/estudiante/'));
    }

    //Fundamentos Juridicos
    if (Yii::app()->user->pbac('fundamentoJuridico.fundamentoJuridico.read')) {
        $items[] = array('name' => 'Fundamentos Juridicos', 'code' => 'fundamentoJuridico', 'icon' => 'icon-legal', 'link' => array('/fundamentoJuridico'));
    }
    //Mapa

    if (Yii::app()->user->pbac('mapa.default.read') || (Yii::app()->user->pbac("mapa.default.admin"))) {
//        $ip = base64_encode(Yii::app()->request->userHostAddress);
//        $entre = 'Gescolar';
//        $url = 'http://172.16.3.119/mapa/web/?g=' . $ip . '&id=' . $entre;
        //$url = 'localhost/mapa/web/?g=' . $ip . '&id=' . $entre;
        $items[] = array('name' => 'Mapa', 'code' => 'mapa', 'icon' => 'icon-map-marker',
            'link' => array('/mapa/'),
            // 'link' => array($url),
            //  'target' => '_blank'
            //'class' => 'ocultar'
        );
        //   . '</div>';
    }

    //Título
    if (Yii::app()->user->pbac("titulo.registro.read") || Yii::app()->user->pbac("titulo.registro.write") || Yii::app()->user->pbac("titulo.atencionSolicitud.read") || Yii::app()->user->pbac("titulo.atencionSolicitud.write") || Yii::app()->user->pbac("titulo.seguimientoTitulo.read") || Yii::app()->user->pbac("titulo.seguimientoTitulo.write") || Yii::app()->user->pbac("titulo.consultarTitulo.read")) {
        //if (in_array(Yii::app()->user->group, array(UserGroups::JEFE_ZONA, UserGroups::JEFE_DRCEE_ZONA, UserGroups::ADMIN_0, UserGroups::ADMIN_DRCEE, UserGroups::JEFE_DRCEE)))
        $items[] = array('name' => 'Títulos', 'code' => 'titulo', 'icon' => 'icon-graduation-cap', 'sub' => getSubMenu('Titulo'));
    }

    //Constancia
    if (Yii::app()->user->pbac('estudiante.constancia.read')) {
        $items[] = array('name' => 'Constancia', 'code' => 'constancia', 'icon' => 'icon-columns','link' => array('/estudiante/constancia'));
    }

    //verificación
    if (Yii::app()->user->pbac('verificacion.tituloDigital.read')) {
        $items[] = array('name' => 'Verificacion', 'code' => 'verificacion', 'icon' => 'icon-archive','sub' => getSubMenu('Verificacion'));
    }
    //Catologos
    if (Yii::app()->user->pbac('catalogo.default.read')) {
        $items[] = array('name' => 'Catálogos', 'code' => 'catalogo', 'icon' => 'icon-tags', 'link' => array('/catalogo'));
    }

    //Control
    if (Yii::app()->user->pbac("control.autoridadesPlantel.read") || Yii::app()->user->pbac("control.autoridadesPlantel.write") || Yii::app()->user->pbac("control.autoridadesZona.read") || Yii::app()->user->pbac("control.autoridadesZona.write")) {
        $items[] = array('name' => 'Control', 'code' => 'control', 'icon' => 'icon-signal', 'sub' => getSubMenu('Control'));
        //$items[] = array('name' => 'Reporte', 'code' => 'Reporte de Notificaciones', 'link' => array('/control/estadisticoTicket/'));
    }

    //Seguridad
    if (Yii::app()->user->pbac('Basic.traza.read') || Yii::app()->user->pbac("userGroups.admin.admin") || Yii::app()->user->pbac("userGroups.usuario.admin") || Yii::app()->user->pbac("userGroups.grupo.admin")) {
        $items[] = array('name' => 'Seguridad', 'code' => 'seguridad', 'icon' => 'icon-lock', 'sub' => getSubMenu('Seguridad'));
    }

    //Perfil del Usuario
    if (!Yii::app()->user->isGuest) {
        $items[] = array('name' => 'Mi Perfil', 'code' => 'mi-perfil', 'icon' => 'icon-user', 'link' => array('/perfil'));
    }

    //Ayuda
    if (!Yii::app()->user->isGuest) {
        $items[] = array('name' => 'Ayuda', 'code' => 'ayuda', 'icon' => 'icon-question', 'sub' => getSubMenu('Ayuda'));
    }

    //Administración
    if (Yii::app()->user->pbac('administracion.configuracion.read') || Yii::app()->user->pbac("administracion.configuracion.write") || Yii::app()->user->pbac("administracion.configuracion.admin" || Yii::app()->user->pbac('administracion.generadorCodigoCatalogo.admin'))) {
        $items[] = array('name' => 'Administracion', 'code' => 'Administracion', 'icon' => 'icon-wrench', 'sub' => getSubMenu('Administracion'));
    }
    //Cerrar Sesión
    $items[] = array('name' => 'Cerrar Sesión', 'code' => 'cerrar-sesion', 'icon' => 'icon-off', 'link' => array('/logout'));
    return $items;
}

// Se encarga de capturar los items a pintar en el SubMenu del Menu
function getSubMenu($menu) {
    $items = array();
    switch ($menu) {
        case 'Seguridad':
            if (Yii::app()->user->pbac("userGroups.admin.admin") || Yii::app()->user->pbac("userGroups.usuario.admin") || Yii::app()->user->pbac("userGroups.grupo.admin")) {
                if (Yii::app()->user->pbac("userGroups.grupo.admin")) {
                    $items[] = array('name' => 'Grupos', 'code' => 'usuarios', 'link' => array('/userGroups/grupo/'));
                }
                if (Yii::app()->user->pbac("userGroups.usuario.admin")) {
                    $items[] = array('name' => 'Usuario', 'code' => 'usuarios', 'link' => array('/userGroups/usuario/'));
                }
                if (Yii::app()->user->pbac("userGroups.admin.admin")) {
                    $items[] = array('name' => 'Administracion', 'code' => 'admin', 'link' => array('/userGroups/admin/'));
                }
            }

            if (Yii::app()->user->pbac('Basic.traza.read')) {
                $items[] = array('name' => 'Buscar Traza', 'code' => 'buscar-traza', 'link' => array('/traza/admin'));
                //$items[] = array('name' => 'Ver Trazas', 'code' => 'ver-traza', 'link' => array('/traza/index'));
            }
            break;
        case 'Titulo':
            if (Yii::app()->user->pbac("titulo.consultarTitulo.read") || Yii::app()->user->pbac("titulo.consultarTitulo.write")) {
                $items[] = array('name' => 'Consultar Titulo', 'code' => 'ConsultarTitula', 'link' => array('/titulo/consultarTitulo/'));
            }

            if (Yii::app()->user->pbac("titulo.registro.read") || Yii::app()->user->pbac("titulo.registro.write") || Yii::app()->user->pbac("titulo.asignacionSerialesEstados.read") || Yii::app()->user->pbac("titulo.asignacionSerialesEstados.write")) {
                $items[] = array('name' => 'Registro de Seriales', 'code' => 'registroSeriales', 'link' => array('/titulo/registro/'));

                // $items[
                // $items[] = array('name' => 'Solicitud de Títulos', 'code' => 'solicitudTitulo', 'link' => array('#'));
                if (Yii::app()->user->pbac("titulo.asignacionSerialesEstados.admin"))
                    $items[] = array('name' => 'Asignación de seriales por Estado', 'code' => 'asignacionSerialesEstados', 'link' => array('/titulo/asignacionSerialesEstados/'));
            }
            if (Yii::app()->user->pbac("titulo.seguimientoTitulo.read") || Yii::app()->user->pbac("titulo.seguimientoTitulo.write")) {
                if (in_array(Yii::app()->user->group, array(UserGroups::ADMIN_0, UserGroups::ADMIN_DRCEE, UserGroups::JEFE_DRCEE)))
                    $items[] = array('name' => 'Entrega de Seriales a Zona Educativa', 'code' => 'otorgamientoZonaEdu', 'link' => array('/titulo/seguimientoTitulo/indexDrcee'));
            }
            if (Yii::app()->user->pbac("titulo.seguimientoTitulo.read") || Yii::app()->user->pbac("titulo.seguimientoTitulo.write")) {
                if (in_array(Yii::app()->user->group, array(UserGroups::JEFE_ZONA, UserGroups::JEFE_DRCEE_ZONA))) {

                    $items[] = array('name' => 'Entrega de Seriales al Plantel', 'code' => 'otorgamientoPlantel', 'link' => array('/titulo/seguimientoTitulo/indexZonaEdu'));
                }
            }
            if (Yii::app()->user->pbac("titulo.digitalTitulo.read") || Yii::app()->user->pbac("titulo.digitalTitulo.write")) {
                $items[] = array('name' => 'Digitalizar Titulo', 'code' => 'DigitalTitulo', 'link' => array('/titulo/DigitalTitulo/'));
            }
            break;
        case 'Verificacion':
            if (Yii::app()->user->pbac("verificacion.tituloDigital.read") || Yii::app()->user->pbac("verificacion.tituloDigital.write")) {
                $items[] = array('name' => 'Verificacion de Titulo', 'code' => 'VerificacionTitulo', 'link' => array('/verificacion/tituloDigital'));
                $items[] = array('name' => 'Verificacion de Constancia', 'code' => 'VerificacionConstancia', 'link' => array('/verificacion/ConstanciaVerificar'));
                //$items[] = array('name' => 'Digitalizar Titulo', 'code' => 'DigitalTitulo', 'link' => array('/titulo/DigitalTitulo/'));
            }
            break;

//            if (Yii::app()->user->pbac("titulo.seguimientoTitulo.read") || Yii::app()->user->pbac("titulo.seguimientoTitulo.write")) {
//                if (in_array(Yii::app()->user->group, array(UserGroups::DIRECTOR)))
//                    $items[] = array('name' => 'Otorgamiento a Estudiante', 'code' => 'otorgamientoEstudiante', 'link' => array('/titulo/seguimientoTitulo/mostrarPlanteles'));
//            }
        // $items[] = array('name' => 'Atención de Solicitud', 'code' => 'atencionSolicitud', 'link' => array('/titulo/atencionSolicitud/'));

        case 'Control':
            if (Yii::app()->user->pbac("control.autoridadesPlantel.read") || Yii::app()->user->pbac("control.autoridadesPlantel.write") || Yii::app()->user->pbac("control.autoridadesZona.read") || Yii::app()->user->pbac("control.autoridadesZona.write")) {
                $items[] = array('name' => 'Autoridades de Plantel', 'code' => 'autoridad-plantel', 'link' => array('/control/autoridadesPlantel/'));

                if (Yii::app()->user->pbac("ayuda.ticket.admin") and Yii::app()->user->group == UserGroups::ADMIN_0) {
                    $items[] = array('name' => 'Reporte de Notificaciones', 'code' => 'Reporte de Notificaciones', 'link' => array('/control/estadisticoTicket/'));
                }
            }
            break;
        case 'Ayuda':
            if (!Yii::app()->user->isGuest) {
                $items[] = array('name' => 'Notificaciones', 'code' => 'ayuda-notificaciones', 'link' => array('/ayuda/ticket'));
                $items[] = array('name' => 'Instructivos', 'code' => 'ayuda-instructivo', 'link' => array('/ayuda/instructivo'));
            }
            break;
        case 'Administracion':
            if (Yii::app()->user->pbac('comunicaciones.noticia.admin')) {
                $items[] = array('name' => 'Noticias', 'code' => 'noticias', 'link' => array('/comunicaciones/'));
            }
            if (in_array(Yii::app()->user->group, array(UserGroups::ADMIN_0, UserGroups::DESARROLLADOR))) {
                $items[] = array('name' => 'Configuración', 'code' => 'Configuracion', 'link' => array('/administracion/configuracion'));
            }
            if (Yii::app()->user->pbac('administracion.generadorCodigoCatalogo.admin')) {
                $items[] = array('name' => 'Generador de Código', 'code' => 'generadorCodigoCatalogo', 'link' => array('/administracion/generadorCodigoCatalogo'));
            }

            break;
    }

    return $items;
}

$_SESSION['_items_menu'] = getMenu();
//Defino mi lista de items a mostrar (menus y submenus) si y solo si ya no lo tengo en session
if (!isset($_SESSION['_items_menu'])) {
    $_SESSION['_items_menu'] = getMenu();
}

//Pinto el menu
//	$this->widget('application.extensions.mbmenu.MbMenu',array('items'=>$_SESSION['_items_menu']));
if (Yii::app()->user) {
    $this->widget('application.widgets.EMenuWidget', array('items' => $_SESSION['_items_menu']));
}
