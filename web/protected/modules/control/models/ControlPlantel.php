<?php

/**
 * Description of DirectoresPlantel
 *
 * @author Jose Gabriel Gonzalez
 */
class ControlPlantel extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gplantel.plantel';
    }

    public function     reporteDetalladoPlantel($column, $level, $dependency, $fecha = null, $orderBy = null) {

        $columnFilter = $this->getColumnFilter($column, $fecha,$dependency);
        $drillDownFilter = $this->getGeoDrillDownFilter($level, $dependency);
    //    var_dump($dependency);die();
        
        //$where = $drillDownFilter . ' AND ' . $columnFilter;
    $where = $columnFilter ;
    //var_dump($where);die();

        if (is_null($orderBy)) {
            $orderBy = 'cod_plantel,'
                    . 'nombre '
                    ;
        }

        $sql = " select distinct
            a.cod_plantel	,
a.cod_estadistico	,
a.denominacion	,
a.nombre	,
a.zona_educativa	,
a.tipo_dependencia	,
a.estatus	,
a.fundacion	,
a.estado	,
a.municipio	,
a.parroquia	,
a.direccion	,
a.correo	,
a.telefono_fijo	,
a.telefono_otro	,
a.zona_ubicacion	,
a.clase_plantel	,
a.categoria	,
a.condicion_estudio	,
a.tipo_matricula	,
a.turno	,
a.modalidad	,
a.dir_cedula	,
a.dir_nombre	,
a.dir_apellido	,
a.dir_usuario	,
a.dir_telefono	,
a.dir_celular	,
a.dir_email	,
a.dir_twitter	,
es.id

from control.autoridad_plantel a

                LEFT JOIN public.estado es ON a.estado = es.nombre
                LEFT JOIN public.municipio mc ON a.municipio = mc.nombre
                LEFT JOIN public.parroquia pq ON a.parroquia = pq.nombre
                 
                WHERE
                    $where
                ORDER BY
                    $orderBy";
//echo $sql; die();
        $connection = Yii::app()->dbEstadistica;
        $command = $connection->createCommand($sql);
        $resultado = $command->queryAll();
        //var_dump($resultado);die();
        return $resultado;
    }

    private function getColumnFilter($column, $fecha = null , $dependency= null) {

        switch ($column) {
            case 'planteles':
                $where = "a.cod_plantel IS NOT NULL AND es.id='$dependency'";
                break;
            case 'con_director':
                $where = "a.cod_plantel IS NOT NULL AND dir_nombre IS NOT NULL AND es.id='$dependency' ";
                break;
            case 'publ_sin_director':
                $where = "a.cod_plantel IS NOT NULL  AND es.id='$dependency' AND dir_nombre IS NULL AND (a.tipo_dependencia ='Estadal' OR a.tipo_dependencia=  'Nacional' OR a.tipo_dependencia='Municipal' OR a.tipo_dependencia= 'Autonoma') ";
                break;
            case 'publ_con_director':
                $where = "a.cod_plantel IS NOT NULL AND dir_nombre IS NOT NULL AND es.id='$dependency' AND (a.tipo_dependencia IN ('Estadal', 'Nacional', 'Municipal', 'Autonoma'))";
                break;
            case 'priv_sin_director':
                $where = "a.cod_plantel IS NOT NULL AND dir_nombre IS NULL AND tipo_dependencia='Privada' AND es.id='$dependency'";
                break;
            case 'priv_con_director':
                $where = "a.cod_plantel IS NOT NULL AND dir_nombre IS NOT NULL AND tipo_dependencia='Privada' AND es.id='$dependency'";
                break;
            case 'otros_sin_director':
                $where = "a.cod_plantel IS NOT NULL AND dir_nombre IS NULL AND tipo_dependencia NOT IN ('Estadal', 'Nacional', 'Municipal', 'Autonoma', 'Privada') AND es.id='$dependency'";
                break;
            case 'otros_con_director':
                $where = "a.cod_plantel IS NOT NULL AND dir_nombre IS NOT NULL AND tipo_dependencia  NOT IN ('Estadal', 'Nacional', 'Municipal', 'Autonoma', 'Privada') AND es.id='$dependency'";
                break;
            case 'con_director_fecha':
                $where = "a.cod_plantel IS NOT NULL AND dir_nombre IS NOT NULL AND to_char(ap.fecha_ini,'YYYY-MM-DD')='$fecha' AND es.id='$dependency'";
                break;
            default:
                $where = "a.cod_plantel IS NOT NULL AND es.id='$dependency'";
                break;
        }

        return $where;
    }

    private function getGeoDrillDownFilter($level, $dependency) {
        $level='estado';
        switch ($level) {
            case 'region':
                if ((int) $dependency !== 0)
                    $where = 'es.region_id = ' . $dependency . ' AND es.id != 45'; //No incluye Dependencias Federales (45)
                else
                    $where = 'es.id != 45'; //No incluye Dependencias Federales (45)
                break;
            case 'estado':
                $where = 'es.id = ' . $dependency . ' AND es.id != 45'; //No incluye Dependencias Federales (45)
                break;
            case 'municipio':
                $where = 'mc.id = ' . $dependency . ' AND es.id != 45'; //No incluye Dependencias Federales (45)
                break;
            default:
                $where = '1 = 1';
                break;
        }

        return $where;
    }

}
