<?php
/**
 *
 *
 * Author: Maikel Ortega Hernandez
 * Email: maikeloh@gmail.com
 * Date: 20/11/14
 * Time: 10:45
 */

class AutoridadPlantel extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gplantel.autoridad_plantel';
    }

    public function reporteGrafico($estadoId=null){

        $resultado = array();
        $camposSeleccionados = "p.estado, 'AAA'||p.estado AS titulo  ";
        $camposAgrupados = "p.estado  ";
        $where = ' ';
        $orderBy = 'titulo ASC, p.estado ASC';

        if(!is_null($estadoId) && is_numeric($estadoId)){
            //$where .= " AND e.id = $estadoId ";
        }

        $sql = "SELECT  $camposSeleccionados
                        , SUM(CASE WHEN (p.cod_plantel IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
                        , SUM(CASE WHEN (p.cod_plantel IS NOT NULL AND p.dir_nombre IS NOT NULL) THEN 1 ELSE 0 END) AS con_director
                        , SUM(CASE WHEN (p.cod_plantel IS NOT NULL AND p.dir_nombre IS NULL) THEN 1 ELSE 0 END) AS sin_director
                FROM control.autoridad_plantel p
                GROUP BY
                    $camposAgrupados
                ORDER BY
                    $orderBy";

        //ld($sql);
        $dependency = new CDbCacheDependency('SELECT SUM(CASE WHEN (p.cod_plantel IS NOT NULL) THEN 1 ELSE 0 END) FROM control.autoridad_plantel p;');

        $connection = Yii::app()->dbEstadistica;
        //$command = $connection->createCommand($sql);
        $command   = Yii::app()->dbEstadistica->cache(28800, $dependency)->createCommand($sql);
        $resultado = $command->queryAll();

        return $resultado;

    }


} 