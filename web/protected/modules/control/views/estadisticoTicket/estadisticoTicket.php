<?php $porcentaje_total=0;?>
<?php foreach ($dataReport as $data): 
$porcentaje_total=$porcentaje_total+$data['total'];
 endforeach;?>
<div class="space-6"></div>
<table class="report table table-striped table-bordered table-hover">

    <thead>
        <tr>
            <th title="Total" rowspan="2" class="center">
                Estado
            </th>

            <th title="Total" rowspan="2" class="center">
                Total
            </th>
            <th title="Activos" rowspan="2" class="center">
                Activos
            </th>

            <th title="Asignados" rowspan="2" class="center">
                Asignados
            </th>

                <th title="Devueltos" rowspan="2" class="center">
                Devueltos
            </th>

            <th title="Resuelto" rowspan="2" class="center">
                Resueltos
            </th>

            <th title="Porcentaje" rowspan="2" class="center">
                % de resueltos
            </th>
        </tr>
    </thead>
    <?php
    $cActivos = 0;
    $cResueltos = 0;
    $cDevueltos = 0;
    $cAsignados = 0;
    $acumulador = 0;
    $acumuladorT = 0;
    $total_generazado=0;
    ?>
    <tbody>
        <?php if (empty($dataReport)): ?>
            <tr>
                <td colspan="11">
                    <div class="alertDialogBox" style="margin-top: 10px;">
                        <p>
                            No se han encontrado ningunas unidades responsables .
                        </p>
                    </div>
                </td>
            </tr>
        <?php else: ?>

            <?php foreach ($dataReport as $data): ?>
                <?php
                $contar = 0;
                $porcentaje = 0;
                $contar = $data['resueltos'] *100;
                $acumuladorT = $acumuladorT + $data['total'];
                if ($contar <= 0) {
                    $porcentaje = 0;
                } else {
                    $porcentaje = $data['resueltos']*100/$data['total'];
                    $acumulador = $acumulador + $porcentaje;
                }
                $avance = 0;
                $dependencyId = 0;
                ?>
                <?php
                $cActivos = $cActivos + $data['activos'];
                $cResueltos = $cResueltos + $data['resueltos'];
                $cDevueltos = $cDevueltos + $data['devueltos'];
                $cAsignados = $cAsignados + $data['asignados'];
                ?>
                <tr>
                 <?php $id=base64_encode($data['id_estado']);?>
                    <?php $estado_id=base64_encode($data['id_estado']);?>

                    <td class="center">


                        <a title="" onclick="reporteEstadisticoEstado('<?php echo $id;?>');"><?php echo ucwords(strtolower($data['estado'])); ?></a>
                    </td>
                    <td class="center">
                 <a href="/control/estadisticoTicket/ReporteDetalladoTicket?estado=<?php echo $estado_id; ?>&estatus=<?php echo "";?>&unidad=<?php echo "";?>">
        <?php echo $data['total']; ?>
                        </a>
                    </td>

                    <td class="center">
                        <a href="/control/estadisticoTicket/ReporteDetalladoTicket?estado=<?php echo $estado_id; ?>&estatus=<?php echo "A";?>&unidad=<?php echo ""?>">
                            <?php echo $data['activos']; ?>
                        </a>
                    </td>

                    <td class="center">
                      <a href="/control/estadisticoTicket/ReporteDetalladoTicket?estado=<?php echo $estado_id; ?>&estatus=<?php echo "P";?>&unidad=<?php echo "";?>">
        <?php echo $data['asignados']; ?>
                        </a>
                    </td>

                         <td class="center">
                        <a href="/control/estadisticoTicket/ReporteDetalladoTicket?estado=<?php echo $estado_id; ?>&estatus=<?php echo "D";?>&unidad=<?php echo "";?>">
        <?php echo $data['devueltos']; ?>
                        </a>
                    </td>

                    <td class="center">
                   <a href="/control/estadisticoTicket/ReporteDetalladoTicket?estado=<?php echo $estado_id; ?>&estatus=<?php echo "S";?>&unidad=<?php echo "";?>">
        <?php echo $data['resueltos']; ?>
                        </a>
                    </td>
                    <td class="center">
        <?php echo
        number_format($porcentaje, 2, ',', '.');
        ?>
                        </a>
                    </td>
                </tr>

            <?php endforeach; ?>
        <thead>
            <tr>
                <th title="Devueltos" rowspan="2" class="center">
                    Total
                </th>

                <td class="center">
                        <?php echo $acumuladorT; ?>
                </td>

                <td class="center">
                        <?php echo $cActivos; ?>
                </td>

                 <td class="center">
                            <?php echo $cAsignados; ?>
                    </td>

                    <td class="center">
                        <?php echo $cDevueltos; ?>
                </td>

                <td class="center">
                        <?php echo $cResueltos; ?>
                </td>
                      <td class="center">
                            <?php $total_generazado=$acumulador/24;?>
                            <?php echo
                            number_format($total_generazado, 2, ',', '.');
                            ?>
                    </td>

        </thead>
    <?php endif; ?>
</tbody>

</table>
<span class="small">Reporte: <?php echo date("d-m-Y H:i:s"); ?></span>
<script type="text/javascript">
    $(document).ready(function() {

        $('.contact-data').unbind('click');
        $('.contact-data').on('click',
                function(e) {
                    e.preventDefault();
                    var estado_id = $(this).attr('data-id');
                    verDatosContacto('zona_educativa', estado_id);
                }
        );

        $('.observ-data').unbind('click');
        $('.observ-data').on('click',
                function(e) {
                    e.preventDefault();
                    var estado_id = $(this).attr('data-id');
                    var estado = $(this).attr('data-estado');
                    dialogObservacion(estado_id, estado);
                }
        );

        $('.rep-control').unbind('click');
        $('.rep-control').on('click',
                function(e) {
                    e.preventDefault();
                    var estado_id = $(this).attr('data-id');
                    var estado = $(this).attr('data-estado');
                    dialogRegistroControl('zona_educativa', estado_id, estado);
                }
        );

    });
</script>