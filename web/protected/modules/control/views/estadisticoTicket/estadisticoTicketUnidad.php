
<?php
$this->breadcrumbs = array(
    'Estados' => array('/control/estadisticoTicket'),
);
?>
<?php $resultado = Ticket::model()->estadisticaUnidades();
?>
<?php foreach ($resultado as $dataT):
$comprobar=$dataT['unidad_responsable'];
 endforeach;?>
<div class="space-6"></div>
<table class="report table table-striped table-bordered table-hover">
    <thead>
        <tr>
         <th title="Total" rowspan="2" class="center">
           Unidad Responsable
           </th>

            <th title="Total" rowspan="2" class="center">
                Total
            </th>
            <th title="Activos" rowspan="2" class="center">
                Activos
            </th>

            <th title="Resuelto" rowspan="2" class="center">
                Resueltos
            </th>

            <th title="Devueltos" rowspan="2" class="center">
                Devueltos
            </th>

            <th title="Asignados" rowspan="2" class="center">
                Asignados
            </th>
            <th title="Porcentaje" rowspan="2" class="center">
                %
            </th>
        </tr>
        <?php
        $cActivos = 0;
        $cResueltos = 0;
        $cDevueltos = 0;
        $cAsignados = 0;
        $acumulador = 0;
        $acumuladorT = 0;
        ?>
    </thead>
    <tbody>
        <?php if (empty($comprobar)): ?>
            <tr>
                <td colspan="11">
                    <div class="alertDialogBox" style="margin-top: 10px;">
                        <p>
                            No se han encontrado Estados.
                        </p>
                    </div>
                </td>
            </tr>
        <?php else: ?>
            <?php foreach ($resultado as $data): ?>
                <?php
                $contar = 0;
                $porcentaje = 0;
                $contar = $data['activos'] + $data['resueltos'] + $data['devueltos'] + $data['asignados'] * 100;
                $acumuladorT = $acumuladorT + $data['total'];
                if ($contar <= 0) {
                    $porcentaje = 0;
                } else {
                    $porcentaje = $contar / $data['total'];
                    $acumulador = $acumulador + $porcentaje;
                }
                ?>
                <?php
                $cActivos = $cActivos + $data['activos'];
                $cResueltos = $cResueltos + $data['resueltos'];
                $cDevueltos = $cDevueltos + $data['devueltos'];
                $cAsignados = $cAsignados + $data['asignados'];
                ?>
                <tr>
                    <td class="center">
                        <a href="#">  <?php echo $data['estado']; ?> </a>
                    </td>
                    <td class="center">
                        <a href="#">
                            <?php echo $data['total']; ?>
                        </a>
                    </td>

                    <td class="center">
                        <a href="#">
                            <?php echo $data['activos']; ?>
                        </a>
                    </td>

                    <td class="center">
                        <a href="#">
                            <?php echo $data['resueltos']; ?>
                        </a>
                    </td>

                    <td class="center">
                        <a href="#">
                            <?php echo $data['devueltos']; ?>
                        </a>
                    </td>

                    <td class="center">
                        <a href="#">
                            <?php echo $data['asignados']; ?>
                        </a>
                    </td>

                    <td class="center">
                        <a href="#">
                            <?php echo
                            number_format($porcentaje, 2, ',', '.');
                            ?>
                        </a>
                    </td>
                </tr>

            <?php endforeach; ?>
        <thead>
            <tr>
                <th title="Devueltos" rowspan="2" class="center">
                    Total
                </th>

                <td class="center">
                    <a href="#">
                        <?php echo $acumuladorT; ?>
                    </a>
                </td>

                <td class="center">
                    <a href="#">
                        <?php echo $cActivos; ?>
                    </a>
                </td>

                <td class="center">
                    <a href="#">
                        <?php echo $cResueltos; ?>
                    </a>
                </td>

                <td class="center">
                    <a href="#">
                        <?php echo $cDevueltos; ?>
                    </a>
                </td>

                <td class="center">
                    <a href="#">
                        <?php echo $cAsignados; ?>
                    </a>
                </td>

                <td class="center">
                    <a href="#">
                        <?php echo number_format($acumulador, 2, ',', '.'); ?>
                    </a>
                </td>
        </thead>
    <?php endif; ?>
</tbody>

</table>
<span class="small">Reporte: <?php echo date("d-m-Y H:i:s"); ?></span>
<script type="text/javascript">
    $(document).ready(function() {

        $('.contact-data').unbind('click');
        $('.contact-data').on('click',
                function(e) {
                    e.preventDefault();
                    var estado_id = $(this).attr('data-id');
                    verDatosContacto('zona_educativa', estado_id);
                }
        );

        $('.observ-data').unbind('click');
        $('.observ-data').on('click',
                function(e) {
                    e.preventDefault();
                    var estado_id = $(this).attr('data-id');
                    var estado = $(this).attr('data-estado');
                    dialogObservacion(estado_id, estado);
                }
        );

        $('.rep-control').unbind('click');
        $('.rep-control').on('click',
                function(e) {
                    e.preventDefault();
                    var estado_id = $(this).attr('data-id');
                    var estado = $(this).attr('data-estado');
                    dialogRegistroControl('zona_educativa', estado_id, estado);
                }
        );

    });
</script>