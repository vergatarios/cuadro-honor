<?php

class EstadisticoTicketController extends Controller {

    const NUEVO_USUARIO = 1;
    const ERROR_SISTEMA = 2;
    const RESET_CLAVE = 3;
    CONST SOLICITUD_OTRA = 6;
    const NUEVO_PLANTEL = 5;
    const PLANTEL_INACTIVO = 7;
    const A_ACTIVO = 'A';
    const A_RESUELTO = 'S';
    const A_DEVUELTO = 'D';
    const A_REDIRECCIONADO = 'R';
    const A_ASIGNADO = 'P';
    const A_REVISION = 'C';

    static $_permissionControl = array(
        'read' => 'Consulta de Ticket',
        'write' => 'Registrar de Ticket',
        'admin' => 'Atender de Ticket',
        'label' => 'Reporte Ticket'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'estadisticas','estadisticasEstado','estadisticasUnidad','estadisticasEstatus','reporteDetalladoTicket'),
                'pbac' => array('read', 'write',),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'reporte'),
                'pbac' => array('admin',),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'estadisticas','estadisticasEstado','estadisticasUnidad','estadisticasEstatus','reporteDetalladoTicket'),
                'pbac' => array('admin', 'write',),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionEstadisticas() { 
                $model = new Ticket();
                $dataReport = $model->estadisticas();
                $this->renderPartial('estadisticoTicket', array('dataReport' => $dataReport), false, true);
    
    }
    public function actionEstadisticasEstado(){
        $id=$_REQUEST['id'];
        $id_estado=  base64_decode($id);
        Yii::app()->getSession()->add('id_estado',$id_estado);
      $this->renderPartial('estadisticoTicketEstado');
}
public function actionEstadisticasUnidad(){
        $id=$_REQUEST['id'];
        Yii::app()->getSession()->add('id_unidad',$id);
      $this->renderPartial('estadisticoTicketUnidad');
}

public function actionReporteDetalladoTicket() {
    $estado=$_REQUEST['estado'];
    $estado_id=base64_decode($estado);
    $estatus=$_GET['estatus'];
    $unidad_id=$_REQUEST['unidad'];
    $unidad=base64_decode($unidad_id);
        if (!Yii::app()->request->isAjaxRequest) {
                    $headers = array(
                        'Nombre',
                        'Unidad Responsable',
                        'Tipo de Notificacion',
                        'Codigo',
                        'Observacion');
                    $colDef = array(
                        'nombre' => array(),
                        'unidad_responsable' => array(),
                        'tipo_ticket' => array(),
                        'codigo' => array(),
                        'observacion' => array());

                    $ticket = new Ticket();
                    if($estatus=='A' and empty($unidad)){
                    $dataReport = $ticket->seleccionar_estatus_activo($estado_id,$estatus);
                    }elseif($estatus=='' and $unidad==''){
                     $dataReport = $ticket->seleccionar_total($estado_id);
                    }else if($unidad!='' and $estatus!='' and $estado_id!=''){
                    $dataReport = $ticket->seleccionar_estatus_activo_unidad($estado_id,$estatus,$unidad);
                    }else if($unidad!='' and $estado_id!='' and $estatus==''){
                    $dataReport = $ticket->seleccionar_total_estado($estado_id, $unidad);
                   }else{
                    $dataReport = $ticket->seleccionar_estatus($estado_id,$estatus);
                    }
//                    if (Yii::app()->user->pbac('admin')) { // Si tienes permisos de administrador puedes descargar el CSV/EXCEL
                    $fileName = 'notificacion-' . date('Y-m-d-H:i:s') . '.csv';
                    CsvExport::export(
                            $dataReport, // a CActiveRecord array OR any CModel array
                            $colDef, $headers, true, $fileName);
            } else {
                throw new CHttpException(404, 'Recurso no encontrado. Recargue la página e intentelo de nuevo.');
            }
}

}
