<?php

class MatriculacionReporteController extends Controller {

    public $defaultAction = 'index';
    //despues de la declaración de la clase va el siguiente codigo
    public $layout = '//layouts/main';
    static $_permissionControl = array(
        'read' => 'Consulta de Estudiante Matriculado Por Región y Estado',
        'write' => 'Consulta de Estudiante Matriculado Por Región y Estado',
        'admin' => 'Consulta de Estudiante Matriculado Por Región y Estado',
        'label' => 'Consulta de Estudiante Matriculado Por Región y Estado'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('index', 'ReporteDetalladoMatricula', 'estadisticoMatricula', 'reporteCSV'),
                'pbac' => array('read', 'write', 'admin'),
            ),
            array('allow',
                'actions' => array('index', 'estadisticoMatricula'),
                'pbac' => array('read',),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionEstadisticoMatricula_sin_filtrar() {
        if (Yii::app()->request->isAjaxRequest) {

            if ($this->getPost('nivel') && $this->getPost('dependency')) {

                if (Yii::app()->user->pbac('admin')) {
                    $nivel = 'estado';
                    $dependency = $this->getPost('dependency');
                } else {
                    $nivel = 'municipio';
                    $dependency = Yii::app()->user->estado;
                }

                $region = null;
                $estado = null;
                $municipio = null;
                /**
                  if ($nivel == 'region') {
                  $titulo = 'Región';
                  $anteriorNivel = 'region';
                  $siguienteNivel = 'estado';
                  } else
                 */
                if ($nivel == 'estado') {
                    //$region = new Region('search');
                    //$result = $region->findByPk($dependency);
                    //           $titulo = 'Estado';
                    // $anteriorNivel = 'estado';
                    //     $siguienteNivel = 'municipio';
                    /**
                      $region = new stdClass();
                      $region->nombre = $result->nombre;
                      $region->id = $dependency;
                     */
                } elseif ($nivel == 'municipio') {
                    $estado = new Estado('search');
                    //   $result = $estado->findByPk($dependency);
                    //           $titulo = 'Municipio';
                    //  $anteriorNivel = 'estado';
                    //   $siguienteNivel = 'parroquia';
                    // $estado = new stdClass();
                    //      $estado->nombre = $result->nombre;
                    //       $estado->id = $dependency;
                    /**
                      $region = new stdClass();
                      $region->nombre = $result->region->nombre;
                      $region->id = $result->region_id;
                     */
                }

                if (empty($dependency)) {
                    $dependency = null;
                }
                //    var_dump($dependency);die();
                $model = new Matriculacion();
                $dataReport = $model->reporteEstadisticoMatriculado($nivel, $dependency);
                //           var_dump($dataReport);
//                die();
                $this->renderPartial('estadisticoMatricula', array('dataReport' => $dataReport, 'nivel' => $nivel, 'dependency' => $dependency, 'region' => $region, 'estado' => $estado, 'municipio' => $municipio), false, true);
            } else {
                throw new CHttpException(404, 'Recurso no encontrado. Recargue la página e intentelo de nuevo.');
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    //prueba de carga de CGridView desde tabla totalizada - Meylin

    public function actionEstadisticoMatricula() {
                
        if (Yii::app()->request->isAjaxRequest) {

            if ($this->getPost('nivel') && $this->getPost('dependency')) {

                if (Yii::app()->user->pbac('admin')) {
                    $nivel = 'estado';
                    $dependency = $this->getPost('dependency');
                } else {
                    $nivel = 'municipio';
                    $dependency = Yii::app()->user->estado;
                }
//var_dump($nivel);die();
                $region = null;
                $estado = null;
                $municipio = null;
                /**
                  if ($nivel == 'region') {
                  $titulo = 'Región';
                  $anteriorNivel = 'region';
                  $siguienteNivel = 'estado';
                  } else
                 */
                if ($nivel == 'estado') {
                    //$region = new Region('search');
                    //$result = $region->findByPk($dependency);
                    //           $titulo = 'Estado';
                    // $anteriorNivel = 'estado';
                    //     $siguienteNivel = 'municipio';
                    /**
                      $region = new stdClass();
                      $region->nombre = $result->nombre;
                      $region->id = $dependency;
                     */
                } elseif ($nivel == 'municipio') {
                    $estado = new Estado('search');
                      $result = $estado->findByPk($dependency);
                    //           $titulo = 'Municipio';
                    //  $anteriorNivel = 'estado';
                    //   $siguienteNivel = 'parroquia';
                    // $estado = new stdClass();
                     $estado_nombre= $estado->nombre = $result->nombre;
//                     var_dump($estado_nombre);
//                                          die();
                    //       $estado->id = $dependency;
                    /**
                      $region = new stdClass();
                      $region->nombre = $result->region->nombre;
                      $region->id = $result->region_id;
                     */
                }

                if (empty($dependency)) {
                    $dependency = null;
                }
                // var_dump($nivel);die();
                $model = new Matriculacion();
                $dataReport = $model->reporteMatriculadoMes($nivel, $dependency);
                         var_dump($dataReport);die();
                 //       $dataTotal = $model->reporteEstadisticoMatriculado($nivel, $dependency);
                 //          var_dump($dataT);
//                die();
                $this->renderPartial('estadisticoMatricula', array('dataReport' => $dataReport, 'nivel' => $nivel, 'dependency' => $dependency, 'region' => $region, 'estado' => $estado, 'municipio' => $municipio), false, true);
            } else {
                throw new CHttpException(404, 'Recurso no encontrado. Recargue la página e intentelo de nuevo.');
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionReporteDetalladoMatricula($col, $lev, $dep = 0) {

        //         var_dump($col);
        //        var_dump($lev);
      //       var_dump($dep);
        //    die();

        $mEstado = Estado::model()->findByPk($dep);
        $estado_nombre = $mEstado->nombre;
    //    var_dump($estado_nombre);die();
        $caso1 = 'matricula_inicial';
        $caso2 = 'matricula_primaria';
        $caso3 = 'matricula_otros';
        $caso4 = 'planteles';
        $caso5 = 'matricula';
        if (!Yii::app()->request->isAjaxRequest) {

            if (in_array($lev, array('estado', 'municipio'))) {

                if (is_numeric($dep)) {


                    if ($col == $caso5) {
                        $headers = array(
                            'Cod Plantel',
                            'Cod Estadistico',
                            'Denominación',
                            'Nombre',
                            'Zona Educativa',
                            'Dependencia',
                            'Estatus',
                            'Año de Fundación',
                            'Estado',
                            'Municipio',
                            'Parroquia',
                            'Dirección',
                            'Correo',
                            'Teléfono Fijo',
                            'Teléfono Otro',
                            'Zona Ubicación',
                            'Clase de Plantel',
                            'Categoria',
                            'Condición Estudio',
                            'Tipo Matrícula',
                            'Turno',
                            'Modalidad',
                            'Director Cédula',
                            'Director Nombre',
                            'Director Apellido',
                            //'Director Usuario',
                            'Director Teléfono',
                            'Director Celular',
                            'Director Email',
                            'Director Twitter',
                            'Total de Estudiantes',
                            'cantidad Estudiante Inicial',
                            'cantidad Estudiante Primaria',
                            'cantidad Estudiante Otros'
//                        'cantidad cantidad_estudiante_total'
                        );



                        $colDef = array(
                            'cod_plantel' => array(),
                            'cod_estadistico' => array(),
                            'denominacion' => array(),
                            'nombre' => array(),
                            'zona_educativa' => array(),
                            'tipo_dependencia' => array(),
                            'estatus' => array(),
                            'fundacion' => array(),
                            'estado' => array(),
                            'municipio' => array(),
                            'parroquia' => array(),
                            'direccion' => array(),
                            'correo' => array(),
                            'telefono_fijo' => array(),
                            'telefono_otro' => array(),
                            'zona_ubicacion' => array(),
                            'clase_plantel' => array(),
                            'categoria' => array(),
                            'condicion_estudio' => array(),
                            'tipo_matricula' => array(),
                            'turno' => array(),
                            'modalidad' => array(),
                            'dir_cedula' => array(),
                            'dir_nombre' => array(),
                            'dir_apellido' => array(),
                            //'dir_usuario' => array(),
                            'dir_telefono' => array(),
                            'dir_celular' => array(),
                            'dir_email' => array(),
                            'dir_twitter' => array(),
                            'cantidad_estudiante_total' => array(), //Cantidad Total de estudiantes matriculados
                            'cantidad_estudiante_inicial' => array(), // Cantidad detallada de matricula por nivel
                            'cantidad_estudiante_primaria' => array(),
                            'cantidad_estudiante_otros' => array()
//                        'cantidad_estudiante_total' => array()
                        );
                    }
                    if ($col == $caso4) {
                        $headers = array(
                            'Cod Plantel',
                            'Cod Estadistico',
                            'Denominación',
                            'Nombre',
                            'Zona Educativa',
                            'Dependencia',
                            'Estatus',
                            'Año de Fundación',
                            'Estado',
                            'Municipio',
                            'Parroquia',
                            'Dirección',
                            'Correo',
                            'Teléfono Fijo',
                            'Teléfono Otro',
                            'Zona Ubicación',
                            'Clase de Plantel',
                            'Categoria',
                            'Condición Estudio',
                            'Tipo Matrícula',
                            'Turno',
                            'Modalidad',
                            'Director Cédula',
                            'Director Nombre',
                            'Director Apellido',
                            //'Director Usuario',
                            'Director Teléfono',
                            'Director Celular',
                            'Director Email',
                            'Director Twitter',
                            'Total de Estudiantes Matriculados'
//                        'cantidad_estudiante_inicial',
//                        'cantidad_estudiante_primaria',
//                        'cantidad_estudiante_otros',
//                        'cantidad cantidad_estudiante_total'
                        );



                        $colDef = array(
                            'cod_plantel' => array(),
                            'cod_estadistico' => array(),
                            'denominacion' => array(),
                            'nombre' => array(),
                            'zona_educativa' => array(),
                            'tipo_dependencia' => array(),
                            'estatus' => array(),
                            'fundacion' => array(),
                            'estado' => array(),
                            'municipio' => array(),
                            'parroquia' => array(),
                            'direccion' => array(),
                            'correo' => array(),
                            'telefono_fijo' => array(),
                            'telefono_otro' => array(),
                            'zona_ubicacion' => array(),
                            'clase_plantel' => array(),
                            'categoria' => array(),
                            'condicion_estudio' => array(),
                            'tipo_matricula' => array(),
                            'turno' => array(),
                            'modalidad' => array(),
                            'dir_cedula' => array(),
                            'dir_nombre' => array(),
                            'dir_apellido' => array(),
                            //'dir_usuario' => array(),
                            'dir_telefono' => array(),
                            'dir_celular' => array(),
                            'dir_email' => array(),
                            'dir_twitter' => array(),
                            'cantidad_estudiante_total' => array() //Cantidad Total de estudiantes matriculados
//                      'cantidad_estudiante_inicial' => array(), // Cantidad detallada de matricula por nivel
//                        'cantidad_estudiante_primaria' => array(),
//                        'cantidad_estudiante_otros' => array(),
//                        'cantidad_estudiante_total' => array()
                        );
                    }

                    if ($col == $caso3) {
                        $headers = array(
                            'Cod Plantel',
                            'Cod Estadistico',
                            'Denominación',
                            'Nombre',
                            'Zona Educativa',
                            'Dependencia',
                            'Estatus',
                            'Año de Fundación',
                            'Estado',
                            'Municipio',
                            'Parroquia',
                            'Dirección',
                            'Correo',
                            'Teléfono Fijo',
                            'Teléfono Otro',
                            'Zona Ubicación',
                            'Clase de Plantel',
                            'Categoria',
                            'Condición Estudio',
                            'Tipo Matrícula',
                            'Turno',
                            'Modalidad',
                            'Director Cédula',
                            'Director Nombre',
                            'Director Apellido',
                            //'Director Usuario',
                            'Director Teléfono',
                            'Director Celular',
                            'Director Email',
                            'Director Twitter',
                            'Total de Estudiantes Otros'
//                        'cantidad_estudiante_inicial',
//                        'cantidad_estudiante_primaria',
//                        'cantidad_estudiante_otros',
//                        'cantidad cantidad_estudiante_total'
                        );



                        $colDef = array(
                            'cod_plantel' => array(),
                            'cod_estadistico' => array(),
                            'denominacion' => array(),
                            'nombre' => array(),
                            'zona_educativa' => array(),
                            'tipo_dependencia' => array(),
                            'estatus' => array(),
                            'fundacion' => array(),
                            'estado' => array(),
                            'municipio' => array(),
                            'parroquia' => array(),
                            'direccion' => array(),
                            'correo' => array(),
                            'telefono_fijo' => array(),
                            'telefono_otro' => array(),
                            'zona_ubicacion' => array(),
                            'clase_plantel' => array(),
                            'categoria' => array(),
                            'condicion_estudio' => array(),
                            'tipo_matricula' => array(),
                            'turno' => array(),
                            'modalidad' => array(),
                            'dir_cedula' => array(),
                            'dir_nombre' => array(),
                            'dir_apellido' => array(),
                            //'dir_usuario' => array(),
                            'dir_telefono' => array(),
                            'dir_celular' => array(),
                            'dir_email' => array(),
                            'dir_twitter' => array(),
                            //        'cantidad_estudiante' => array() //Cantidad Total de estudiantes matriculados
//                      'cantidad_estudiante_inicial' => array(), // Cantidad detallada de matricula por nivel
//                        'cantidad_estudiante_primaria' => array(),
                            'cantidad_estudiante_otros' => array(),
//                        'cantidad_estudiante_total' => array()
                        );
                    }

                    if ($col == $caso2) {
                        $headers = array(
                            'Cod Plantel',
                            'Cod Estadistico',
                            'Denominación',
                            'Nombre',
                            'Zona Educativa',
                            'Dependencia',
                            'Estatus',
                            'Año de Fundación',
                            'Estado',
                            'Municipio',
                            'Parroquia',
                            'Dirección',
                            'Correo',
                            'Teléfono Fijo',
                            'Teléfono Otro',
                            'Zona Ubicación',
                            'Clase de Plantel',
                            'Categoria',
                            'Condición Estudio',
                            'Tipo Matrícula',
                            'Turno',
                            'Modalidad',
                            'Director Cédula',
                            'Director Nombre',
                            'Director Apellido',
                            //'Director Usuario',
                            'Director Teléfono',
                            'Director Celular',
                            'Director Email',
                            'Director Twitter',
                            'Total de Estudiantes Primaria'
//                        'cantidad_estudiante_inicial',
//                        'cantidad_estudiante_primaria',
//                        'cantidad_estudiante_otros',
//                        'cantidad cantidad_estudiante_total'
                        );



                        $colDef = array(
                            'cod_plantel' => array(),
                            'cod_estadistico' => array(),
                            'denominacion' => array(),
                            'nombre' => array(),
                            'zona_educativa' => array(),
                            'tipo_dependencia' => array(),
                            'estatus' => array(),
                            'fundacion' => array(),
                            'estado' => array(),
                            'municipio' => array(),
                            'parroquia' => array(),
                            'direccion' => array(),
                            'correo' => array(),
                            'telefono_fijo' => array(),
                            'telefono_otro' => array(),
                            'zona_ubicacion' => array(),
                            'clase_plantel' => array(),
                            'categoria' => array(),
                            'condicion_estudio' => array(),
                            'tipo_matricula' => array(),
                            'turno' => array(),
                            'modalidad' => array(),
                            'dir_cedula' => array(),
                            'dir_nombre' => array(),
                            'dir_apellido' => array(),
                            //'dir_usuario' => array(),
                            'dir_telefono' => array(),
                            'dir_celular' => array(),
                            'dir_email' => array(),
                            'dir_twitter' => array(),
//                        'cantidad_estudiante' => array() //Cantidad Total de estudiantes matriculados
                            //        'cantidad_estudiante_inicial' => array(), // Cantidad detallada de matricula por nivel
                            'cantidad_estudiante_primaria' => array()
//                        'cantidad_estudiante_otros' => array(),
//                        'cantidad_estudiante_total' => array()
                        );
                    }

                    if ($col == $caso1) {
                        $headers = array(
                            'Cod Plantel',
                            'Cod Estadistico',
                            'Denominación',
                            'Nombre',
                            'Zona Educativa',
                            'Dependencia',
                            'Estatus',
                            'Año de Fundación',
                            'Estado',
                            'Municipio',
                            'Parroquia',
                            'Dirección',
                            'Correo',
                            'Teléfono Fijo',
                            'Teléfono Otro',
                            'Zona Ubicación',
                            'Clase de Plantel',
                            'Categoria',
                            'Condición Estudio',
                            'Tipo Matrícula',
                            'Turno',
                            'Modalidad',
                            'Director Cédula',
                            'Director Nombre',
                            'Director Apellido',
                            //'Director Usuario',
                            'Director Teléfono',
                            'Director Celular',
                            'Director Email',
                            'Director Twitter',
                            //                 'Total de Estudiantes'
                            'Cantidad Estudiante Inicial',
//                        'cantidad_estudiante_primaria',
//                        'cantidad_estudiante_otros',
//                        'cantidad cantidad_estudiante_total'
                        );



                        $colDef = array(
                            'cod_plantel' => array(),
                            'cod_estadistico' => array(),
                            'denominacion' => array(),
                            'nombre' => array(),
                            'zona_educativa' => array(),
                            'tipo_dependencia' => array(),
                            'estatus' => array(),
                            'fundacion' => array(),
                            'estado' => array(),
                            'municipio' => array(),
                            'parroquia' => array(),
                            'direccion' => array(),
                            'correo' => array(),
                            'telefono_fijo' => array(),
                            'telefono_otro' => array(),
                            'zona_ubicacion' => array(),
                            'clase_plantel' => array(),
                            'categoria' => array(),
                            'condicion_estudio' => array(),
                            'tipo_matricula' => array(),
                            'turno' => array(),
                            'modalidad' => array(),
                            'dir_cedula' => array(),
                            'dir_nombre' => array(),
                            'dir_apellido' => array(),
                            //'dir_usuario' => array(),
                            'dir_telefono' => array(),
                            'dir_celular' => array(),
                            'dir_email' => array(),
                            'dir_twitter' => array(),
                            //      'cantidad_estudiante' => array(), //Cantidad Total de estudiantes matriculados
                            'cantidad_estudiante_inicial' => array() // Cantidad detallada de matricula por nivel
//                        'cantidad_estudiante_primaria' => array(),
//                        'cantidad_estudiante_otros' => array(),
//                        'cantidad_estudiante_total' => array()
                        );
                    }

                    $planteles = new Matriculacion();
                   // $dataReport = $planteles->reporteDetalladoPlantelMatricula($col, $lev, $dep);//Caso previo con id_estado
                    $dataReport = $planteles->reporteDetalladoPlantelMatricula($col, $lev, $estado_nombre);
                         //     var_dump($dataReport);die();
//                    if (Yii::app()->user->pbac('admin')) { // Si tienes permisos de administrador puedes descargar el CSV/EXCEL
                    $fileName = 'planteles-' . date('YmdHis') . '.csv';

                    CsvExport::export(
                            $dataReport, // a CActiveRecord array OR any CModel array
                            $colDef, $headers, true, $fileName);

                    //                    } else { // Si no eres administrador puedes es descargar el PDF
//
//                        $mPDF = Yii::app()->ePdf->mpdf('','A4-L', 0, '', 5, 5, 5, 5, 5, 5, 'L');
//                        $mPDF->useSubstitutions=false;
//                        $mPDF->simpleTables = true;
//                        $mPDF->WriteHTML($this->renderPartial('application.modules.planteles.views.consultar._pdfHeader', array(), true));
//                        $mPDF->WriteHTML($this->renderPartial('application.modules.planteles.views.consultar._reporteListaPlanteles', array('model' => $dataReport, 'headers'=>$headers), true));
//                        $mPDF->Output( 'planteles-' . date('YmdHis') . '.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
//
//                    }
                } else {
                    throw new CHttpException(404, 'Recurso no encontrado. Recargue la página e intentelo de nuevo.');
                }
            } else {
                throw new CHttpException(404, 'Recurso no encontrado. Recargue la página e intentelo de nuevo.');
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionReporteCSV() {


        $csvFileName = 'reporte_matricula_13_14_' . date('Y-m-d') . '.csv';
        $ruta = yii::app()->basePath . '/../public/reportes/reporte_matricula_13.csv';

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=" . $csvFileName);
        header("Content-Type: text/csv");
        header("Content-Transfer-Encoding: UTF-8");
        header('Pragma: no-cache');
        header('Expires: 0');

        readfile($ruta);
    }

}
