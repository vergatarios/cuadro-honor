<link rel="stylesheet" href="/public/js/jquery.upload/css/jquery.fileupload.css">
<link rel="stylesheet" href="/public/js/jquery.upload/css/jquery.fileupload-ui.css">
<div class="col-md-12">
<div class="col-md-3">
<span class="btn btn-success smaller-90 fileinput-button">
    <i class="fa fa-upload"></i>
    <span> Seleccione un archivo...</span>
    <!-- The file input field used as target for the file upload widget -->
    <input id="fileupload" type="file" name="files[]" >
</span>
</div>
<div class="col-md-9">
<div class="infoDialogBox">
   <p>Solo los siguientes tipo de archivos son validos: <b>PDF, JPG, PNG, DOC, OPT</b>.<p>
</div>
</div>
    </div>
<br>
<br>
<!-- The container for the uploaded files -->
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>


<div id="dialogPantalla" class="hide"></div>
<div id="files" class="files"></div>

<div id="listado">
    <?php
//    $lista = ArchivoFundamentoJuridico::model()->findAllByAttributes(array('fundamento_juridico_id' => $model->id), array('order' => 'id DESC'));
//    $dataProvider = new CArrayDataProvider($lista, array(
//        'pagination' => array(
//            'pageSize' => 5,
//        )
//    ));

  $this->widget('zii.widgets.grid.CGridView', array(
  'itemsCssClass' => 'table table-striped table-bordered table-hover',
  'id' => 'archivo-fundamento-grid',
  'dataProvider' => $modelArchivo->search($llave),
    'summaryText' => false,
  'pager' => array(
  'header' => '',
  'htmlOptions' => array('class' => 'pagination'),
  'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
  'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
  'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
  'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
  ),
  'columns' => array(
  array(
  'class' => 'CLinkColumn',
  'header' => '<center>Nombre del Archivo</center>',
  'labelExpression' => '$data->nombre',
  'urlExpression' => '"/fundamentoJuridico/fundamentoJuridico/descargar?id=".base64_encode($data->ruta)'
  ),
  array(
  'type' => 'raw',
  'header' => '<center>Acciones</center>',
  'value' => array($this, 'columna'),
  ),
  ),
  ));


?>
</div>
<br>
</div>
<!-- The template to display files available for upload -->

<div class="row-fluid ">

    <a class="btn btn-danger" href="/fundamentoJuridico/fundamentoJuridico">

        <i class="icon-arrow-left bigger-110"></i>
        Volver
    </a>
</div>



<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="/public/js/jquery.upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="/public/js/jquery.upload/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="/public/js/jquery.upload/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<!-- blueimp Gallery script -->
<script src="/public/js/jquery.upload/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/public/js/jquery.upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload validation plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/public/js/jquery.upload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script>



    $(document).ready(function() {
        var id = $("#id").val();
        $('#fileupload').fileupload({
            url: '/fundamentoJuridico/fundamentoJuridico/upload?id=' + id,
            acceptFileTypes: /(\.|\/)(jpe?g|png|pdf|doc|opt)$/i,
            maxFileSize: 5000000,
            singleFileUploads: true,
            autoUpload: true,
            process: [
                {
                    action: 'load',
                    fileTypes: /(\.|\/)(jpe?g|png|pdf|doc|opt)$/i,
                    maxFileSize: 20000000 // 20MB
                },
                {
                    action: 'resize',
                    maxWidth: 1440,
                    maxHeight: 900
                },
                {
                    action: 'save'
                }
            ],
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Se ha producido un error en la carga del archivo.");//ALEXIS ERROR DE CARGA
            }

        });
        $('#fileupload').bind('fileuploaddone', function(e, data) {
            var archivos = data.jqXHR.responseJSON.files;
            //  console.debug(archivos=);
            //var ruta=file.url;
            //$('<p/>').text(file.name).appendTo('#files');
            $.each(archivos, function(index, file) {


                nombre = file.name;
                //alert("sd");
                $("#dialogPantalla").html('<div class="alertDialogBox">\n\
                <p class="bolder center grey"> \n\
¿Desea Cambiar el Nombre del Archivo?</p>\n\</div>\n\
<div class="row">\n\
<div class="col-md-12">\n\
<label class="col-md-12">Nombre:</label>\n\
<input class="col-md-12" required="required" id="nuevoNombre" name="nombre" type="text" value="' + nombre + '"/> \n\
</div>');
                var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                    modal: true,
                    width: '450px',
                    dragable: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-upload'></i>Modificar Archivo</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                            "class": "btn btn-danger btn-xs",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                            html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                            "class": "btn btn-primary btn-xs",
                            click: function() {
                                var ext = getFileExtension(nombre);
                                nombreBD = $("#nuevoNombre").val();
                                datos = {id: nombre, nombreBD: nombreBD, fundamento_id: id}
                                $.ajax({
                                    url: '/fundamentoJuridico/fundamentoJuridico/guardarArchivo',
                                    type: 'POST',
                                    data: datos,
                                    success: function(result)
                                    {
                                        $("#files").html(result);


                                        $('#archivo-fundamento-grid').yiiGridView('update', {
                                            data: {fundamento_id:id}
                                        });


                                    }

                                });
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
//                               $('#archivo-fundamento-grid').yiiGridView('update', {
//                                    data: $(this).serialize()
//                                });


//                var divResult = "files";
//                var urlDir = '/fundamentoJuridico/fundamentoJuridico/guardarArchivo?id=' + nombre + '&fundamento_id=' + $("#id").val();
//                var conEfecto = true;
//                var showHTML = true;
//                var callback = function() {
//                    alert("sd");
//
//
//                };
//                executeAjax(divResult, urlDir, conEfecto, showHTML, callback);

            });
        });
    });
    function getFileExtension(name)
    {
        var found = name.lastIndexOf('.') + 1;
        return (found > 0 ? name.substr(found) : "");
    }
</script>