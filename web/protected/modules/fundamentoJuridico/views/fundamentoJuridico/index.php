<?php
/* @var $this FundamentoJuridicoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fundamento Jurídicos',
);

$this->menu=array(
	array('label' => 'Create FundamentoJurídico', 'url' => array('create')),
    array('label' => 'Manage FundamentoJurídico', 'url' => array('admin')),
);
?>

<h1>Fundamento Juridicos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
