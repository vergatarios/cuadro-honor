<?php
/* @var $this FundamentoJuridicoController */
/* @var $model FundamentoJuridico */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fundamento-juridico-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <div class="tabbable">

        <ul class="nav nav-tabs">

            <li class="active"><a data-toggle="tab" href="#datosGenerales">Datos Generales</a></li>
            <li><a data-toggle="tab" href="#archivos">Archivos</a></li>

        </ul>

        <div class="tab-content">


            <div id="datosGenerales" class="tab-pane active">


                <div class="widget-main form" style="overflow:hidden;">


                    <div>

                        <?php
                        if ($form->errorSummary($model)):
                            ?>
                            <div id ="div-result-message" class="errorDialogBox" >
                                <?php echo $form->errorSummary($model); ?>
                            </div>
                            <?php
                        endif;
                        if ($key == 1) {

                            $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => $msj));
                        }
                        ?>



                        <div class="row">

                            <input type="hidden" id='id' name="id"  value="<?php echo $model->id ?>" />

                            <div class="col-md-12">
                                <label class="col-md-12"  ><b>Nombre </b></label>

                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12">

                                <?php
                                echo
                                $form->textField($model, 'nombre', array('maxlength' => 160, 'class
																				' => 'col-md-12', 'required' => 'required'));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="space-10"></div>
                            <div class="col-md-6">
                                <label class="col-md-12" ><b>Fecha de Emisión:</b></label>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar bigger-100"></i>
                                    </span>

                                    <?php
                                    if ($model->fecha_emision != "") {
                                        $model->fecha_emision = date("d-m-Y", strtotime($model->fecha_emision));
                                    } else {
                                        $model->fecha_emision = "";
                                    }



                                    echo

                                    $form->textField($model, 'fecha_emision', array('size' => 30, 'maxlength' => 30, 'id
					' => 'date-picker', 'required' => 'required', 'id' => 'fecha_emision', 'readonly' => 'readonly'));
                                    ?>


                                </div>


                            </div>






                            <div class="col-md-6">

                                <label class="col-md-12" ><b>Tipo de Fundamentos:</b></label>

                                <div class="col-md-12">

                                    <?php
                                    echo $form->dropDownList($model, 'tipo_fundamento_id', CHtml::listData(TipoFundamento::model()->findAllByAttributes(array('estatus' => 'A'), array('order' => 'nombre ASC')), 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7', 'required' => 'required')
                                    );
                                    ?>



                                </div>


                            </div>
                        </div>
                    </div>


                    <div class="space-7"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="col-md-12"><b>Descripción:</b></label>

                            <div id="editor1" class="wysiwyg-editor" style="border:1px solid #ccc;" contenteditable="true">
                            </div>
                            <?php
                            if ($model->descripcion) {
                                $model->descripcion = CHtml::decode($model->descripcion);
                            }

                            echo $form->textArea($model, 'descripcion', array('id' => 'descripcion', 'readonly' => 'true', 'hidden' => 'hidden'));
                            ?>


                        </div>
                    </div>




                    <div class="row">

                        <input type="hidden" id='id' name="id"  value="<?php echo $model->id ?>" />


                    </div>




                    <hr>
                    <div class="row-fluid wizard-actions">


                        <!--<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Guardar', array('class' => 'btn btn-primary')); ?>-->
                        <a class="btn btn-danger" href="/fundamentoJuridico/fundamentoJuridico">

                            <i class="icon-arrow-left bigger-110"></i>
                            Volver
                        </a>

                        <button class="btn btn-primary btn-next" data-last="Finish ">
                            Guardar
                            <i class=" icon-save"></i>
                        </button>
                    </div>
                </div>


                <?php
                $this->endWidget();
                echo CHtml::scriptFile('/public/js/modules/fundamentoJuridico/fundamentoJuridico.js');
                ?>

            </div>


            <div id="archivos" class="tab-pane">

                <div class="widget-main form" style="overflow:hidden;">
                    <?php if ($llave == ""): ?>
                        <div class="row">
                            <div class="infoDialogBox" > <p class="bolder center grey">Para agregar archivos primero debe registrar un fundamento juridico</p></div>                        
                        </div>
                    <?php endif; ?>

                    <?php
                    if ($llave != "") {
                        $this->renderPartial('_archivo', array('model' => $model, 'modelArchivo' => $modelArchivo, 'key' => $key, 'msj' => $msj, 'llave' => $llave, 'subtitulo' => "Nuevo Fundamento Juridico"));
                    }
                    ?>
                </div>


            </div>
        </div>
    </div>
</div>










        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
<script src="/public/js/modules/fundamentoJuridico/jquery.ui.widget.js"></script>
<script src="/public/js/modules/fundamentoJuridico/jquery.iframe-transport.js"></script>

<script src="/public/js/ace-elements.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/typeahead-bs2.min.js"></script>
<script src="/public/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/public/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/public/js/jquery.ui.touch-punch.min.js"></script>
<script src="/public/js/markdown/markdown.min.js"></script>
<script src="/public/js/markdown/bootstrap-markdown.min.js"></script>
<script src="/public/js/jquery.hotkeys.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/bootbox.min.js"></script>
</div><!-- form -->
