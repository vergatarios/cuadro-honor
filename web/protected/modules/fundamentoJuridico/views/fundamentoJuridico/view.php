<?php
/* @var $this FundamentoJuridicoController */
/* @var $model FundamentoJuridico */

$this->breadcrumbs=array(
	'Fundamento Jurídicos' => array('index'),
    $model->nombre,
);
/*
$this->menu=array(
	array('label'=>'List FundamentoJuridico', 'url'=>array('index')),
	array('label'=>'Create FundamentoJuridico', 'url'=>array('create')),
	array('label'=>'Update FundamentoJuridico', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FundamentoJuridico', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FundamentoJuridico', 'url'=>array('admin')),
);
*/
?>

<h1></h1>

<?php /*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'descripcion',
		'fecha_emision',
		'tipo_fundamento_id',
		'usuario_ini_id',
		'usuario_act_id',
		'fecha_ini',
		'fecha_act',
		'fecha_elim',
		'estatus',
	),
)); */ 

$this->renderPartial('_view', array('model' => $model, 'modelArchivo' => $modelArchivo, 'subtitulo' => 'Ver Fundamento Juridico'));
?>
