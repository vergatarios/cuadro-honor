<?php

class FundamentoJuridicoController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    static $_permissionControl = array(
        'read' => 'Consulta de Fundamentos Juridicos',
        'write' => 'Escritura de Fundamentos Juridicos',
        'admin' => 'Admin de Fundamentos Juridicos',
        'label' => 'Consulta de Fundamentos Juridicos'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('index', 'view', 'admin', 'listarArchivo'),
                'pbac' => array('read', 'write'),
            ),
            // en este array sólo van los action de actualizacion a BD
            array('allow',
                'actions' => array('create',
                    'update',
                    'borrar',
                    'habilitar',
                    'server',
                    'upload',
                    'guardarArchivo',
                    'eliminar',
                    'accionesArchivos',
                    'descargar',
                    'activar'),
                'pbac' => array('write'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $id = $_GET["id"];
        $id = base64_decode($id);
        $modelArchivo = new ArchivoFundamentoJuridico;
        $this->render('view', array(
            'model' => $this->loadModel($id),
            'modelArchivo' => $modelArchivo
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new FundamentoJuridico;
        $key = 0;

        $llave = "";
        if ($this->getQuery('fundamento_id')) {
            $llave = $this->getQuery('fundamento_id');
            //$model = $this->loadModel($llave);

        }
        $msj = "";
        $modelArchivo = new ArchivoFundamentoJuridico;
        if (isset($_POST['FundamentoJuridico'])) {
            $nombre = trim($_POST['FundamentoJuridico']['nombre']);
            $nombre = strtoupper($nombre);

            $descripcion = $_POST['FundamentoJuridico']['descripcion'];
            //$descripcion=strtoupper($descripcion);

            $model->attributes = $_POST['FundamentoJuridico'];
            if ($_POST['FundamentoJuridico']['fecha_emision'] != "") {
                $model->fecha_emision = date("m-d-Y", strtotime($_POST['FundamentoJuridico']['fecha_emision']));
            } else {

                $model->fecha_emision = $_POST['FundamentoJuridico']['fecha_emision'];
            }

            $model->nombre = $nombre;
            $model->descripcion = $descripcion;
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");

            $model->estatus = "A";
            if (isset($model->descripcion)) {
                $filter = new InputFilter(array('em', 'i', 'br', 'b', 'strong', 'ol', 'ul', 'li', 'font', 'div', 'strike'));
                $model->descripcion = $filter->process($model->descripcion);
            }

            if ($model->validate()) {
                if ($model->save()) {
                    $this->registerLog(
                        "ESCRITURA", "create", "Exitoso", "Se creo un Fundamento Juridico"
                    );
                    $key = 1;
                    $llave = $model->id;
                    //$model = new FundamentoJuridico;

                    $msj = "¡Exito! ya puede subir los archivos";
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            }
        }


        $this->render('create', array(
            'model' => $model,
            'modelArchivo' => $modelArchivo,
            'key' => $key,
            'msj' => $msj,
            'llave' => $llave
        ));

    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $id = $_GET["id"];
        $id = base64_decode($id);
        $key = 0;
        $llave = $id;
        $msj = "";
        $model = $this->loadModel($id);
        $modelArchivo = new ArchivoFundamentoJuridico;

        //var_dump($modelArchivo);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['FundamentoJuridico'])) {
            $model->attributes = $_POST['FundamentoJuridico'];
            $model->descripcion = $_POST['FundamentoJuridico']['descripcion'];
            $model->fecha_emision = date("m-d-Y", strtotime($_POST['FundamentoJuridico']['fecha_emision']));
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = "A";
            if (isset($model->descripcion)) {
                $filter = new InputFilter(array('em', 'i', 'br', 'b', 'strong', 'ol', 'ul', 'li', 'font', 'div', 'strike'));
                $model->descripcion = $filter->process($model->descripcion);
            }

            if ($model->save())
                if ($model->validate()) {
                    if ($model->save()) {
                        $key = 1;
                        $msj = "Actualizado con exito.";
                        $llave = $model->id;
                        //$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Actualizado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                }
        }

        $this->render('update', array(
            'model' => $model,
            'modelArchivo' => $modelArchivo,
            'key' => $key,
            'msj' => $msj,
            'llave' => $llave
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {/*
      $dataProvider=new CActiveDataProvider('FundamentoJuridico');
      $this->render('index',array(
      'dataProvider'=>$dataProvider,
      )); */

        $groupId = Yii::app()->user->group;

        $model = new FundamentoJuridico('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['FundamentoJuridico']))
            $model->attributes = $_GET['FundamentoJuridico'];

        $usuarioId = Yii::app()->user->id;
        $dataProvider = new CActiveDataProvider('FundamentoJuridico');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new FundamentoJuridico('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['FundamentoJuridico']))
            $model->attributes = $_GET['FundamentoJuridico'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return FundamentoJuridico the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = FundamentoJuridico::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function columnaAcciones($data) {
        $id = $data["id"];
        if ($data->estatus == "E") {
            $columna = CHtml::link("", "/fundamentoJuridico/fundamentoJuridico/view?id=" . base64_encode($id) . "", array("class" => "fa fa-search", "title" => "Buscar este Fundamento Juridico")) . '&nbsp;&nbsp;';
            if (Yii::app()->user->pbac('fundamentoJuridico.fundamentoJuridico.admin')) {
                $columna .= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar este Fundamento Juridico", "onClick" => "VentanaDialog('" . base64_encode($id) . "','/fundamentoJuridico/fundamentoJuridico/activar','Activar Fundamento Juridico','activar')")) . '&nbsp;&nbsp;';
            }
        } else {
            $columna = CHtml::link("", "/fundamentoJuridico/fundamentoJuridico/view?id=" . base64_encode($id) . "", array("class" => "fa fa-search", "title" => "Buscar este Fundamento Juridico")) . '&nbsp;&nbsp;';
            if(Yii::app()->user->pbac('fundamentoJuridico.fundamentoJuridico.write')){
                $columna .= CHtml::link("", "/fundamentoJuridico/fundamentoJuridico/update?id=" . base64_encode($id) . "", array("class" => "fa fa-pencil green", "title" => "Modificar esta Fundamento Juridico")) . '&nbsp;&nbsp;';
            }
            if(Yii::app()->user->pbac('fundamentoJuridico.fundamentoJuridico.admin')){
                $columna .= CHtml::link("", "", array("class" => "fa fa-trash-o red", "title" => "Inhabilitar esta Fundamento Juridico", "onClick" => "VentanaDialog($id,'/catalogo/fundamentoJuridico/borrar','Inhabilitar Fundamento Juridico','borrar')")) . '&nbsp;&nbsp;';
            }

        }
        return $columna;
    }

    public function columna($data) {
        $id = $data["id"];


        // $columna = CHtml::link("", "/fundamentoJuridico/fundamentoJuridico/update?id=" . base64_encode($id) . "", array("class" => "fa fa-pencil green", "title" => "Modificar esta Fundamento Juridico")) . '&nbsp;&nbsp;';
        $columna = CHtml::link("", "", array("class" => "fa fa-trash-o red", "title" => "Eliminar Archivo", "onClick" => "VentanaDialog($id,'/fundamentoJuridico/fundamentoJuridico/eliminar','Eliminar Archivo','eliminarArchivo')")) . '&nbsp;&nbsp;';

        return $columna;
    }

    /* convertir la id foranea en estatus */

    public function estatus($data) {
        $estatus = $data["estatus"];

        if ($estatus == "E") {
            $columna = "Inactivo";
        } else if ($estatus == "A") {
            $columna = "Activo";
        }
        return $columna;
    }

    public function fechaEmision($data) {
        $fecha_emision = $data["fecha_emision"];
        if (empty($fecha_emision)) {
            $fecha_emision = "";
        } else {
            $fecha_emision = date("d-m-Y", strtotime($fecha_emision));
        }
        return $fecha_emision;
    }

    public function actionBorrar() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = new FundamentoJuridico();
            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_elim = date("Y-m-d H:i:s");
                $model->estatus = 'E';
                if ($model->update()) {
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Inhabiliatado con exito.'));
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
    }

    /**
     * Sube el archivo
     */
    public function actionUpload() {
//        $options = null, $initialize = true, $error_messages = null, $filename=null, $id_model=''
        $upload_handler = new UploadHandler(null, true, null, date('YmdHis') . 'FJ');
    }

    public function actionListarArchivo() {
        $id = $_GET["id"];
        $model = new ArchivoFundamentoJuridico;
        $lista = ArchivoFundamentoJuridico::model()->findAllByAttributes(array('fundmento_id' => $id), array('order' => 'id DESC'));


        if ($model) {
            return $model;
        }
    }

    public function actionGuardarArchivo() {
        $id = $_POST["id"];
        $model = new ArchivoFundamentoJuridico;
        $model->ruta = "/public/uploads/fundamentoJuridico/" . $id;
        $model->nombre = $_POST["nombreBD"];
        $model->fundamento_juridico_id = $_POST["fundamento_id"];
        $model->usuario_ini_id = Yii::app()->user->id;
        $model->fecha_ini = date("Y-m-d H:i:s");
        if ($model->save()) {
            $this->registerLog(
                "ESCRITURA", "guardarArchivo", "Exitoso", "Se creo un archivo relacionado a un fundamento Juridico con el id" . $id, Yii::app()->request->userHostAddress, Yii::app()->user->id, date("Y-m-d H:i:s")
            );
            $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Exito! ya puede subir otro archivo'));
        }
    }
    /**
     * Elimina un archivo en particular
     * @throws CHttpException
     */
    public function actionEliminar() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = new ArchivoFundamentoJuridico();
            $model = ArchivoFundamentoJuridico::model()->findByPk($id);
            if ($model) {

                $basePath = dirname(Yii::app()->basePath . '../');
                //var_dump($basePath);
                $upload_handler = new UploadHandler(null, false);

                $respuesta = $upload_handler->deleteOneByFileName($basePath . $model->ruta);
                if ($respuesta) {

                    if ($model->delete()) {
                        $this->registerLog(
                            "ELIMINACION", "eliminar", "Exitoso", "Se elimino un archivo relacionado a un fundamento Juridico con el id", Yii::app()->request->userHostAddress, Yii::app()->user->id, date("Y-m-d H:i:s")
                        );
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Eliminado con exito.'));
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
    }

    public function actionActivar() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $id = base64_decode($id);

            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->estatus = "A";
                if ($model->update()) {
                    $this->registerLog(
                        "ACTIVAR", "activar", "Exitoso", "Se activo  un fundamento Juridico con el id");
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Habilitado con exito.'));
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
    }
    /**
     * Descarga un archivo en particular
     */
    public function actionDescargar() {
        if (isset($_GET["id"])) {

            $ruta = dirname(Yii::app()->basePath . '../');
            $id = base64_decode($_GET["id"]);
            $nombre = explode("/", $id, 5);

            $archivo = $ruta . $id;
            header("Content-disposition: attachment;filename=$nombre[4]");
            header("Content-type: application/octet-stream");
            readfile($archivo);
        }
    }

    public function actionServer() {

    }

    /**
     * Performs the AJAX validation.
     * @param FundamentoJuridico $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'fundamento-juridico-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
