<?php

class AtencionSolicitudController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Atención de Solicitud de Título',
        'write' => 'Registro de Solicitud de Título',
        'label' => 'Atención de Solicitud de Título'
    );

    const MODULO = "Titulo.AtencionSolicitud";

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'mostrarCantidadEstudiante', 'buscarCedula', 'mostrarConsultaPlantel', 'datosPlantel', 'mostrarAgregarLote', 'agregarLote', 'verificarSerial', 'mostrarAtencionSolicitudPorLotes'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('guardarLoteSeriales', 'guardarAsignacionSerialesPorPlantel'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /* ---------     Muestro la vista principal --------- */

    public function actionIndex() {
        $_SESSION['seriales'] = null;
        $this->render('index', array());
    }

    /* ---------    Fin --------- */


    /* ---------  Muestro el formulario de agregar lote ------- */

    public function actionMostrarAgregarLote() {

        $plantel_id = $_REQUEST['plantel_id'];
        $periodo_actual_id = $_REQUEST['periodo_actual_id'];
        $cantidadGraduando = SeguimientoPapelMoneda::model()->obtenerCantidadGraduando($plantel_id, $periodo_actual_id);
        if ($cantidadGraduando == false) {
            $cantidadGraduando = 0;
        }

        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
        $this->renderPartial('agregarLote', array('cantidadGraduando' => $cantidadGraduando, 'plantel_id' => $plantel_id, 'periodo_actual_id' => $periodo_actual_id), false, true);
        yii::app()->end();
    }

    /* ---------  Fin --------- */


    /* ---------  Muestro la cantidad de estudiantes según el  ultimo serial del lote y verifico  que ese lote este disponible ------- */

    public function actionMostrarCantidadEstudiante() {

        $primer_serial = $_REQUEST['primer_serial'];
        $ultimo_serial = $_REQUEST['ultimo_serial'];
        $plantel_id = $_REQUEST['plantel_id'];
        $periodo_actual_id = $_REQUEST['periodo_actual_id'];
        $mensaje_alert = '';

        if ($ultimo_serial < $primer_serial) {
            $mensaje_alert.='Por favor el dato que introdujo en el campo ultimo serial no puede ser menor al primer serial, intente nuevamente <br>';
            echo json_encode(array('statusCode' => 'alert', 'mensaje' => $mensaje_alert));
        } else {

            $cantidadSeriales = $ultimo_serial - $primer_serial + 1;

            $transaction = Yii::app()->db->beginTransaction();

            try {


                $seriales = Yii::app()->getSession()->get('seriales');
                //  var_dump($seriales);
                if ($seriales != array()) { // Entra aqui cuando ya hay un lote de seriales agregado.
                    //echo "entre aqui";
                    $sumaArray = array();
                    $existe = false;
                    $existeEntre = false;
                    $existeUltimo = false;
                    foreach ($seriales as $keyy => $value) {
                        $primerSerial = $value['primer_serial'];
                        $ultimoSerial = $value['ultimo_serial'];
                        $total = (int) $value['cantidad_serial'];
                        $sumaArray[] = $total;

                        if ($primer_serial == $primerSerial) {
                            $existe = true;
                            break;
                        }
                        if ($primer_serial > $primerSerial && $primer_serial < $ultimoSerial) {
                            $existeEntre = true;
                            break;
                        }
                        if ($primer_serial == $ultimoSerial) {
                            $existeUltimo = true;
                            break;
                        }
                    }

                    if ($existeUltimo == false) {
                        if ($existeEntre == false) {
                            if ($existe == false) {
                                $totalSumaSeriales = array_sum($sumaArray);
                                $resultado = SeguimientoPapelMoneda::model()->verificarSerial($primer_serial, $cantidadSeriales);

                                $transaction->commit();

                                echo json_encode(array(
                                    'statusCode' => "cantidadTitulo",
                                    'cantidadTitulo' => $cantidadSeriales
                                ));
                            } else {

                                $respuesta['statusCode'] = 'alert';
                                $respuesta['mensaje'] = 'Por favor verifique el primer serial que ingreso, debido a que no se encuentra disponible por que ya fue agregado';
                                echo json_encode($respuesta);
                            }
                        } else {
                            $respuesta['statusCode'] = 'alert';
                            $respuesta['mensaje'] = 'Por favor verifique el primer serial que ingreso, debido a que no se encuentra disponible por que ya fue agregado entre un lote';
                            echo json_encode($respuesta);
                        }
                    } else {
                        $respuesta['statusCode'] = 'alert';
                        $respuesta['mensaje'] = 'Por favor verifique el primer serial que ingreso, debido a que no se encuentra disponible por que es el ultimo serial de un lote agregado';
                        echo json_encode($respuesta);
                    }
                } else { // Entra la primera vez que no contiene nada la variable de sesión.
                    //   echo "entre abajo";
                    $resultado = SeguimientoPapelMoneda::model()->verificarSerial($primer_serial, $cantidadSeriales);

                    $transaction->commit();

                    echo json_encode(array(
                        'statusCode' => "cantidadTitulo",
                        'cantidadTitulo' => $cantidadSeriales
                    ));
                }


                //      $resultado = SeguimientoPapelMoneda::model()->verificarSerial($primer_serial, $cantidadSeriales);
                //      $transaction->commit();
//                $ultimo_serial = $primer_serial + $cantidadTitulo - 1;
//                $ultimoSerial = $ultimo_serial;
                //         echo json_encode(array('statusCode' => "cantidadTitulo", 'cantidadTitulo' => $cantidadSeriales));
            } catch (Exception $ex) {
                $transaction->rollback();

                $respuesta['statusCode'] = "alert";
                $error = $ex->getMessage();
                $capturarCadena = explode("*", $error);
                $mensajeSerial = $capturarCadena[1] . ' ' . $capturarCadena[2];
                $respuesta['mensaje'] = $mensajeSerial;
                //    $respuesta['alert'] = $mensajeSerial;
                echo json_encode($respuesta);
            }
        }
    }

    /* ---------  Fin ------- */







    /* ---------  Muestro la cantidad de estudiantes y el ultimo serial según el  primer serial introducido y verifico  que ese lote este disponible------- */

//    public function actionVerificarSerial() {
//
//        if (is_numeric($_REQUEST['primer_serial']) && $_REQUEST['primer_serial'] != '') {
//            $primer_serial = $_REQUEST['primer_serial'];
//        } else {
//            $mensaje_alert.='Por favor ingrese el dato correcto en el campo primer serial, recuerde que el serial solo contiene números, intente nuevamente <br>';
//            echo json_encode(array('statusCode' => 'alert', 'mensaje' => $mensaje_alert));
//        }
//        $ultimo_serial = $_REQUEST['ultimo_serial'];
//        ///   if (is_numeric($_REQUEST['cantidad_serial']))
//        //     $cantidad_serial = $_REQUEST['cantidad_serial'];
//        var_dump($ultimo_serial . ' $ultimo_serial');
//        $transaction = Yii::app()->db->beginTransaction();
//
//        try {
//            $seriales = Yii::app()->getSession()->get('seriales');
//            var_dump($seriales);
//            if ($seriales != array()) {
//                echo "entre aqui";
//                $sumaArray = array();
//                $existe = false;
//                $existeEntre = false;
//                $existeUltimo = false;
//                foreach ($seriales as $keyy => $value) {
//                    $primerSerial = $value['primer_serial'];
//                    $ultimoSerial = $value['ultimo_serial'];
//                    $total = (int) $value['cantidad_serial'];
//                    $sumaArray[] = $total;
//
//                    if ($primer_serial == $primerSerial) {
//                        $existe = true;
//                        break;
//                    }
//                    if ($primer_serial > $primerSerial && $primer_serial < $ultimoSerial) {
//                        $existeEntre = true;
//                        break;
//                    }
//                    if ($primer_serial == $ultimoSerial) {
//                        $existeUltimo = true;
//                        break;
//                    }
//                }
//
//                if ($existeUltimo == false) {
//                    if ($existeEntre == false) {
//                        if ($existe == false) {
//                            $sumaPermitida = array_sum($sumaArray);
//                            $serialesDisponiblesNuevo = $cantidad_serial - $sumaPermitida;
//                            $resultado = SeguimientoPapelMoneda::model()->verificarSerial($primer_serial, $serialesDisponiblesNuevo);
//
//                            $transaction->commit();
//                            $ultimo_serial = $primer_serial + $serialesDisponiblesNuevo - 1;
//                            $ultimoSerial = $ultimo_serial;
//
//                            $respuesta['statusCode'] = 'ultimoSerial';
//                            $respuesta['mensaje'] = $ultimoSerial;
//                            echo json_encode($respuesta);
//                        } else {
//
//                            $respuesta['statusCode'] = 'alert';
//                            $respuesta['mensaje'] = 'Por favor verifique el primer serial que ingreso, debido a que no se encuentra disponible por que ya fue agregado';
//                            echo json_encode($respuesta);
//                        }
//                    } else {
//                        $respuesta['statusCode'] = 'alert';
//                        $respuesta['mensaje'] = 'Por favor verifique el primer serial que ingreso, debido a que no se encuentra disponible por que ya fue agregado entre un lote';
//                        echo json_encode($respuesta);
//                    }
//                } else {
//                    $respuesta['statusCode'] = 'alert';
//                    $respuesta['mensaje'] = 'Por favor verifique el primer serial que ingreso, debido a que no se encuentra disponible por que es el ultimo serial de un lote agregado';
//                    echo json_encode($respuesta);
//                }
//            } else { // Entra la primera vez que no contiene nada la variable de sesión.
//                echo "entre abajo";
//                $cantidadSeriales = $resultado = SeguimientoPapelMoneda::model()->verificarSerial($primer_serial, $ultimo_serial);
//
//                $transaction->commit();
//                $ultimo_serial = $primer_serial + $totalGraduando - 1;
//                $ultimoSerial = $ultimo_serial;
//
//                $respuesta['statusCode'] = 'ultimoSerial';
//                $respuesta['mensaje'] = $ultimoSerial;
//                echo json_encode($respuesta);
//            }
//
//
////            $respuesta['statusCode'] = 'ultimoSerial';
////            $respuesta['mensaje'] = $ultimoSerial;
////            echo json_encode($respuesta);
////   echo json_encode(array('statusCode' => 'ultimoSerial', 'mensaje' => $ultimoSerial));
//        } catch (Exception $ex) {
//            $transaction->rollback();
//
////  $this->registerLog('ESCRITURA', self::MODULO . 'InscribirEstudiantes', 'FALLIDO', 'Ha intentado matricular la Seccion Plantel ' . $seccion_plantel_id);
//            $respuesta['statusCode'] = 'alert';
//            $error = $ex->getMessage();
//            $capturarCadena = explode("*", $error);
//            $mensajeSerial = $capturarCadena[1] . ' ' . $capturarCadena[2];
//            $respuesta['mensaje'] = $mensajeSerial;
//            $respuesta['alert'] = $mensajeSerial;
//            echo json_encode($respuesta);
//        }
//    }

    /* ---------  Fin ------- */



    /* ---------  Guardo los lotes de seriales ------- */

    public function actionGuardarLoteSeriales() {

        $plantel_id = (int) $_REQUEST['plantel_id'];
        $origen_responsable = $_REQUEST['origen_responsable'];
        $cedula_responsable = (int) $_REQUEST['cedula_responsable'];
        $nombre_responsable = $_REQUEST['nombre_responsable'];
        $apellido_responsable = $_REQUEST['apellido_responsable'];
        $cargo_responsable_id = (int) $_REQUEST['cargo_responsable_id'];
        $observacion = $_REQUEST['observacion'];
        //   $cargo_calificacion = $_REQUEST['cargo_calificacion'];
        $sumaTotal = (int) $_REQUEST['suma_total'];
        $mensaje = '';

//        if ($cargo_calificacion == '') {
//            $mensaje.= "El campo Registró Calificaciones no debe estar vacio <br>";
//        }
//        if ($cargo_calificacion != '1' && $cargo_calificacion != '0') {
//            $mensaje.= "El campo Registró Calificaciones fue alterado por favor verifique los datos que ingresó <br>";
//        }

        if ($origen_responsable == '') {
            $mensaje.= "El campo Origen no debe estar vacio <br>";
        } else {
            $origen_responsable = $_REQUEST['origen_responsable'];
            $permitidos = "EV";
            for ($i = 0; $i < strlen($origen_responsable); $i++) {
                if (strpos($permitidos, substr($origen_responsable, $i, 1)) === false) {
// echo $apellidos . " no es válido<br>";
                    $resultadoOrigen = false;
                } else {
//echo $apellidos . " es válido<br>";
                    $origen_responsable = $origen_responsable;
                    $resultadoOrigen = true;
                }
            }
            if ($resultadoOrigen == false) {
                $mensaje.='Por favor ingrese los datos correctos para el campo origen, el origen solo puede contener V-venezolano(a) ó E-Extranjero(a)<br>';
            }
        }
        if ($cedula_responsable == null) {
            $mensaje.= "El campo Cédula no debe estar vacio <br>";
        }
        if ($nombre_responsable == '') {
            $mensaje.= "El campo Nombre no debe estar vacio <br>";
        } else {
            $nombre_responsable = $_REQUEST['nombre_responsable'];
            $permitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÑÄËÏÖÜ ";
            for ($i = 0; $i < strlen($nombre_responsable); $i++) {
                if (strpos($permitidos, substr($nombre_responsable, $i, 1)) === false) {
// echo $apellidos . " no es válido<br>";
                    $resultadoNombre = false;
                    break;
                } else {
//echo $apellidos . " es válido<br>";
                    $nombre_responsable = $nombre_responsable;
                    $resultadoNombre = true;
                }
            }
            if ($resultadoNombre == false) {
                $mensaje.='Por favor ingrese los datos correctos para el campo nombre, el nombre solo puede contener letras<br>';
            }
        }
        if ($apellido_responsable == '') {
            $mensaje.= "El campo Apellido no debe estar vacio <br>";
        } else {
            $apellido_responsable = $_REQUEST['apellido_responsable'];
            $permitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÑÄËÏÖÜ ";
            for ($i = 0; $i < strlen($apellido_responsable); $i++) {
                if (strpos($permitidos, substr($apellido_responsable, $i, 1)) === false) {
// echo $apellidos . " no es válido<br>";
                    $resultadoApellido = false;
                    break;
                } else {
//echo $apellidos . " es válido<br>";
                    $apellido_responsable = $apellido_responsable;
                    $resultadoApellido = true;
                }
            }
            if ($resultadoApellido == false) {
                $mensaje.='Por favor ingrese los datos correctos para el campo apellido, el apellido solo puede contener letras<br>';
            }
        }
        if ($cargo_responsable_id == null) {
            $mensaje.= "El campo Cargo del responsable no debe estar vacio <br>";
        }

        if ($mensaje == '') { //valido por yii los campos.
            $seriales = array();
            $seriales = Yii::app()->getSession()->get('seriales');
            if (is_numeric($plantel_id) && is_numeric($cedula_responsable) && is_numeric($cargo_responsable_id) && is_array($seriales)) {

                foreach ($seriales as $key => $value) {
                    $primer_serial[] = (int) $value['primer_serial'];
                    $cantidad_serial[] = (int) $value['cantidad_serial'];
                }
                $cantidadTotal = array_sum($cantidad_serial);
                $primer_serial = Utiles::toPgArray($primer_serial);
                $cantidad_serial = Utiles::toPgArray($cantidad_serial);

                $transaction = Yii::app()->db->beginTransaction();

                //   if ($cantidadTotal == $sumaTotal) {
                $modulo = self::MODULO;
                $ip = Yii::app()->request->userHostAddress;
                $username = Yii::app()->user->name;

                try {

                    $resultadoGuardarLoteSeriales = SeguimientoPapelMoneda::model()->atencionSolicitud($plantel_id, $origen_responsable, $cedula_responsable, $nombre_responsable, $apellido_responsable, $cargo_responsable_id, $observacion, $primer_serial, $cantidad_serial, $modulo, $ip, $username);
                    //  $this->registerLog('ESCRITURA', $modulo, $resultado_transaccion, $descripcion . $ip, $user_id, $username, $fecha_hora);

                    $transaction->commit();
                    $seriales = Yii::app()->getSession()->get('seriales');
                    $respuesta['statusCode'] = 'success';
                    $respuesta['mensaje'] = 'Estimado Usuario, el proceso de distribución de título se ha realizado exitosamente.';
                    $_SESSION['seriales'] = null;
                    echo json_encode($respuesta);
                } catch (Exception $ex) {
                    $transaction->rollback();

                    $respuesta['statusCode'] = 'error';
                    $respuesta['error'] = $ex;
                    $respuesta['mensaje'] = 'Estimado Usuario, ha ocurrido un error durante el proceso de distribución de título. Intente nuevamente.';
                    echo json_encode($respuesta);
                }
//                } else {
//                    $mensaje.="Por favor debe ingresar la cantidad total de solicitudes para este plantel";
//                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
//                }
            } else {
                throw new CHttpException(404, 'No se ha encontrado el recurso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
            }
        } else { // si ingresa datos erroneos muestra mensaje de error.
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        }
    }

    /* ---------  Fin ------- */


    /* --------- Agrego el lote de seriales que introdujo el usuario y vuelvo a pintar la vista con el lote de serial que se agrego ------- */

    public function actionAgregarLote() {

        $mensaje = '';
        $mensaje_cantidadSeriales = '';
        $serialAsignados = false;
        $primer_serial = $_REQUEST['primer_serial'];
        $ultimo_serial = $_REQUEST['ultimo_serial'];
        // $cantidad_serial = (int) $_REQUEST['cantidad_serial']; // Cantidad solicitada por el usuario.
        $fecha_entrega = $_REQUEST['fecha_entrega'];
        $asignado = $_REQUEST['asignado'];
        $plantel_id = $_REQUEST['plantel_id'];
        $periodo_actual_id = $_REQUEST['periodo_actual_id'];
        $codigoe = $_REQUEST['codigo_estadistico'];
        $codigop = $_REQUEST['codigo_plantel'];
        $tipoBusqueda = $_REQUEST['tipoBusqueda'];
        $dataProviderAsignacionSerial = array();
        $dataProviderAsignacionSerialListo = array();
        $dataProviderSerialesAsignad = array();
        $tienenEstudiantes = false;
        $model = new SeguimientoPapelMoneda;



        $resultadoDatosPlantel = SeguimientoPapelMoneda::model()->datosPlantel($tipoBusqueda, $codigoe, $codigop);

        if ($resultadoDatosPlantel == false) {
            $mensaje.= 'El plantel que consulto no se encontro, por favor si el plantel existe comuniquese con los administradores de este sistema';
            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
        } else {
            $plantel_id = $resultadoDatosPlantel[0]['id'];

            if ($plantel_id != '') {
                $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
                $periodo_actual_id = $periodo_escolar_actual_id['id'];

                //    $estatus_solicitud_id = 1; // El estatus_solicitud_id=1 es el estatus solicitado de la tabla estatus_solicitud.
                //    $estatus_actual_id = 11; // El estatus_actual_id=11 es el estatus en solicitud de la tabla estatus_titulo.
                $resultadoPlanes = SeguimientoPapelMoneda::model()->planesDelPlantel($plantel_id, $periodo_actual_id, $estatus_solicitud_id = null, $estatus_actual_id = null, $a = 'N');
                $resultadoAsignacionSerial = Titulo::model()->asignacionSerial($plantel_id, $periodo_actual_id, $a = 'D');
                if ($resultadoPlanes != false) {
                    $tienePlanes = true;
                    $planesObtenidos = $this->dataProviderPlanes($resultadoPlanes);
                    $suma = array();
                    foreach ($resultadoPlanes as $value) {
                        $total = (int) $value['cantidadestu'];
                        $suma[] = $total;
                    }
                    $sumaTotal = array_sum($suma);
                } else {
                    $tienePlanes = false;
                }
                //       if ($cantidad_serial <= $cantidadGraduando) {


                if ($resultadoAsignacionSerial != false && $resultadoPlanes != false) {
                    $planesObtenidos = $this->dataProviderPlanes($resultadoPlanes);
                    $dataProviderAsignacionSerial = $this->dataProviderAsignacionSerial($resultadoAsignacionSerial);
                    $dataProviderAsignacionSerialListo = array();
                    echo 'vacio';
                } else {

                    $verificarEstatusAsignacionEstudiante = Titulo::model()->verificarEstatusAsignacionEstudiante($plantel_id, $periodo_actual_id);

                    if ($verificarEstatusAsignacionEstudiante == $sumaTotal) {
                        $serialAsignados = true;
                    }
                    $resultadoAsignacionSerialListo = Titulo::model()->asignacionSerial($plantel_id, $periodo_actual_id, $a = 'N');
//                var_dump($resultadoAsignacionSerialListo);
//                die();
                    $dataProviderAsignacionSerialListo = $this->dataProviderAsignacionSerialListo($resultadoAsignacionSerialListo);
                    $dataProviderAsignacionSerial = array();
                    echo 'aqui';
                }


                $cantidadSeriales = $ultimo_serial - $primer_serial + 1;
                $transaction = Yii::app()->db->beginTransaction();

                try {

                    $resultado = SeguimientoPapelMoneda::model()->verificarSerial($primer_serial, $cantidadSeriales);

                    $transaction->commit();


                    if ($ultimo_serial < $primer_serial) {
                        $mensaje_alert.='Por favor el dato que introdujo en el campo ultimo serial no puede ser menor al primer serial, intente nuevamente <br>';
                        echo json_encode(array('statusCode' => 'alert', 'mensaje' => $mensaje_alert));
                    }
                    //else {
//        $cantidadTitulo = $ultimo_serial - $primer_serial + 1;
//                                if ($cantidad_serial > $cantidadGraduando) {
//                                    $mensaje_alert.='Por favor verifique los datos que ingreso ya que no puede solicitar esta cantidad de títulos, intente nuevamente <br>';
//                                    echo json_encode(array('statusCode' => 'alert', 'mensaje' => $mensaje_alert));
//                                }
                    //   }
//     $_SESSION['seriales'] = null;
                    $seriales = Yii::app()->getSession()->get('seriales') !== null ? Yii::app()->getSession()->get('seriales') : array();

                    if ($seriales == array()) {

                        $seriales
                                [] = array(
                            'primer_serial' => $primer_serial,
                            'ultimo_serial' => $ultimo_serial,
                            'cantidad_serial' => $cantidadSeriales,
                            'fecha_entrega' => $fecha_entrega,
                            'asignado' => $asignado
                        );

                        $sumaArray = array();
                        foreach ($seriales as $keyy => $value) {
                            $total = (int) $value['cantidad_serial'];
                            $sumaArray[] = $total;
                        }
                        $sumaPermitida = array_sum($sumaArray);
                    } else {

                        $seriales
                                [] = array(
                            'primer_serial' => $primer_serial,
                            'ultimo_serial' => $ultimo_serial,
                            'cantidad_serial' => $cantidadSeriales,
                            'fecha_entrega' => $fecha_entrega,
                            'asignado' => $asignado
                        );

                        $incrementar = 0;

                        foreach ($seriales as $key => $value) {
                            $primerSerial = $value['primer_serial'];
                            $ultimoSerial = $value['ultimo_serial'];

                            if (isset($seriales[$key + 1])) {

                                $incrementar = ++$incrementar;
                                $contar = count($seriales);

                                for ($x = $incrementar; $x < $contar; $x++) {

                                    if (isset($seriales[$x]['primer_serial']))
                                        $primerSerial_nuevo = $seriales[$x]['primer_serial'];
                                    else
                                        break;

                                    if (isset($seriales[$x]['ultimo_serial']))
                                        $ultimoSerial_nuevo = $seriales[$x]['ultimo_serial'];
                                    else
                                        break;

                                    if ($primerSerial == $primerSerial_nuevo || $ultimoSerial == $ultimoSerial_nuevo) {

                                        unset($seriales[$x]);
                                        $seriales = array_values($seriales);
                                    }
                                    if ($ultimoSerial == $primerSerial_nuevo || $primerSerial == $ultimoSerial_nuevo) {

                                        unset($seriales[$x]);
                                        $seriales = array_values($seriales);
                                    }

                                    if ($primerSerial_nuevo > $primerSerial && $ultimoSerial_nuevo <= $ultimoSerial || $primerSerial_nuevo < $primerSerial && $ultimoSerial_nuevo >= $ultimoSerial) {

                                        unset($seriales[$x]);
                                        $seriales = array_values($seriales);
                                    }
                                }
                            } else {

                                break;
                            }
                        }
//                        $sumaArray = array();
//                        foreach ($seriales as $keyy => $value) {
//                            $total = (int) $value['cantidad_serial'];
//                            $sumaArray[] = $total;
//                        }
//                        $sumaPermitida = array_sum($sumaArray);
//
//                        if ($sumaPermitida > $cantidadGraduando) {
//                            unset($seriales[$keyy]);
//                            $seriales = array_values($seriales);
//                            $mensaje_cantidadSeriales.='Por favor los seriales que introdujo no corresponden a la cantidad de títulos que puede solicitar, usted esta intentando ingresar una cantidad mayor a la permitida, intente nuevamente <br>';
////    echo json_encode(array('statusCode' => 'alert', 'mensaje' => $mensaje_alert));
//                        }
                    }

                    $dataProviderMostrarSeriales = $this->dataProviderMostrarSeriales($seriales, $mostrarSerialesAsignados = false);

                    Yii::app()->getSession()->add('seriales', $seriales);

                    $serialesAsignados = SeguimientoPapelMoneda::model()->serialesAsignados($plantel_id);

                    $arraySeriales = array();
                    $lote = 0;
                    $cantidad = 0;
                    for ($x = 0; $x < count($serialesAsignados); $x++) {
                        $comparar = $serialesAsignados[$x]['serial'];
                        if ($x != count($serialesAsignados) - 1)
                            $primer = $serialesAsignados[$x + 1]['serial'];

                        if (isset($serialesAsignados[$x] ['serial'])) {

                            if ((int) ($comparar + 1) == (int) $primer) {

                                if ($x == 0) {
                                    $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
                                    $cantidad = $cantidad + 1;
                                }
                                if ($x != 0) {
                                    $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
                                    $cantidad = $cantidad + 1;
                                }
                            } else {
                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
                                $lote = $lote + 1;
                                $cantidad = 0;
                                if ($x != count($serialesAsignados) - 1) {
                                    $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x + 1]['serial'];
                                    $cantidad = $cantidad + 1;
                                    $x = $x + 1;
                                }
                            }
                        } else {

                        }
                    }

                    if ($serialesAsignados != false) {
                        $seriales = array();
                        for ($y = 0; $y < count($arraySeriales); $y++) {
                            for ($a = 0; $a < count($arraySeriales[$y]); $a++) {
                                if ($a == 0) {
                                    $seriales[$y]['primer_serial'] = $arraySeriales[$y][$a];
                                }
                                if ($a == (count($arraySeriales[$y]) - 1)) {
                                    $seriales[$y]['ultimo_serial'] = $arraySeriales[$y][$a];
                                }
                                $seriales[$y]['cantidad_serial'] = count($arraySeriales[$y]);
                            }
                        }
                        $dataProviderSerialesAsignad = $this->dataProviderMostrarSeriales($seriales, $mostrarSerialesAsignados = true);
                    }
                    $estudiantesConSeriales = Titulo::model()->verificarEstudiantesConSeriales($plantel_id, $periodo_actual_id);

                    foreach ($estudiantesConSeriales as $value) {
                        $estudiantes_con_serial = $value['estudiantes_con_serial'];
                        $estudiantes_total = $value['estudiantes_total'];
                    }
                    if ($estudiantes_con_serial == $estudiantes_total) {
                        $tienenEstudiantes = true;
                    }

                    $cargo = Cargo::model()->findAll(array('order' => 'nombre ASC', 'condition' => 'id=20 or id=6'));
                    $this->renderPartial('atenderSolicitud', array(
                        'resultadoDatosPlantel' => $resultadoDatosPlantel,
                        'dataProviderMostrarSeriales' => $dataProviderMostrarSeriales,
                        'planesObtenidos' => $planesObtenidos,
                        'tienePlanes' => $tienePlanes,
                        'sumaTotal' => $sumaTotal,
                        'plantel_id' => $plantel_id,
                        'periodo_actual_id' => $periodo_actual_id,
                        'codigoe' => $codigoe,
                        'codigop' => $codigop,
                        'tipoBusqueda' => $tipoBusqueda,
                        'cargo' => $cargo,
                        'model' => $model,
                        'primer_serial' => $primer_serial,
                        'ultimo_serial' => $ultimo_serial,
                        //              'sumaPermitida' => $sumaPermitida,
                        'mensaje' => $mensaje,
                        'mensaje_cantidadSeriales' => $mensaje_cantidadSeriales,
                        'serialAsignados' => $serialAsignados,
                        'dataProviderAsignacionSerial' => $dataProviderAsignacionSerial,
                        'dataProviderAsignacionSerialListo' => $dataProviderAsignacionSerialListo,
                        'dataProviderSerialesAsignad' => $dataProviderSerialesAsignad,
                        'tienenEstudiantes' => $tienenEstudiantes
                    ));
                } catch (Exception $ex) {
                    $transaction->rollback();

//  $this->registerLog('ESCRITURA', self::MODULO . 'InscribirEstudiantes', 'FALLIDO', 'Ha intentado matricular la Seccion Plantel ' . $seccion_plantel_id);
                    $respuesta['statusCode'] = 'alert';
                    $respuesta['alert'] = $ex;
                    $mensaje_alert = 'Por favor ingrese otro serial ya que este lote de seriales no estan disponibles, intente nuevamente <br>';
                    $respuesta['mensaje'] = $mensaje_alert;
                    echo json_encode($respuesta);
                }
//                } else {
//                    $mensaje.='Por favor verifique los datos que ingreso, los datos fueron alterados verifique e intente nuevamente <br>';
//                    $cargo = Cargo::model()->findAll(array('order' => 'nombre ASC', 'condition' => 'id=20 or id=6'));
//                    $this->renderPartial('atenderSolicitud', array(
//                        'resultadoDatosPlantel' => $resultadoDatosPlantel,
//                        'dataProviderMostrarSeriales' => $dataProviderMostrarSeriales,
//                        'planesObtenidos' => $planesObtenidos,
//                        'tienePlanes' => $tienePlanes,
//                        'sumaTotal' => $sumaTotal,
//                        'plantel_id' => $plantel_id,
//                        'periodo_actual_id' => $periodo_actual_id,
//                        'codigoe' => $codigoe,
//                        'codigop' => $codigop,
//                        'tipoBusqueda' => $tipoBusqueda,
//                        'cargo' => $cargo,
//                        'model' => $model,
//                        'primer_serial' => $primer_serial,
//                        'ultimo_serial' => $ultimo_serial,
//                        'sumaPermitida' => $sumaPermitida,
//                        'mensaje' => $mensaje,
//                        'mensaje_cantidadSeriales' => $mensaje_cantidadSeriales,
//                        'serialAsignados' => $serialAsignados
//                    ));
//                }
            }
        }
        //    }
    }

    /* ---------  Fin ------- */



    /* ---------  Muestro la vista de agregar lotes con los datos del plantel, planes, seriales y formulario para ingresar los datos del funcionarios que van a retirar el lote de seriales. ------- */

    public function actionMostrarConsultaPlantel() {

        $mensaje = '';
        $mensaje_cantidadSeriales = '';
        $codigoestad = '';
        $codigoplantel = '';
        $serialAsignados = false;
        $dataProviderMostrarSeriales = array();
        $model = new SeguimientoPapelMoneda;
        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_actual_id = $periodo_escolar_actual_id['id'];
        $dataProviderSerialesAsignad = array();
        $tienenEstudiantes = false;

        if (($_REQUEST['tipoBusqueda'] == 'ce' || $_REQUEST['tipoBusqueda'] == 'cp') && $_REQUEST['tipoBusqueda'] != '') {
            $tipoBusqueda = $_REQUEST['tipoBusqueda'];
        } else {
            $mensaje_alert.='Por favor seleccione el tipo de búsqueda correcta para consultar un plantel, intente nuevamente <br>';
            echo json_encode(array('statusCode' => 'alert', 'mensaje' => $mensaje_alert));
        }

        if ($_REQUEST['tipoBusqueda'] == 'cp' && $_REQUEST['codigop'] == '') {
            $mensaje_alert.='Por favor ingrese el código del plantel que desea consultar, intente nuevamente <br>';
            echo json_encode(array('statusCode' => 'alert', 'mensaje' => $mensaje_alert));
        }

        if ($_REQUEST['tipoBusqueda'] == 'ce' && $_REQUEST['codigoe'] == '') {
            $mensaje_alert.='Por favor ingrese el código del plantel que desea consultar, intente nuevamente <br>';
            echo json_encode(array('statusCode' => 'alert', 'mensaje' => $mensaje_alert));
        }
        if ($_REQUEST['codigop'] != '' && $_REQUEST['codigoe'] != '') {
            $mensaje.='Por favor ingrese los datos correctos que desea consultar, los datos del formulario han sido alterados, intente nuevamente <br>';
//      echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
        }

        if ($_REQUEST['tipoBusqueda'] == 'cp' && $_REQUEST['codigop'] != '') {
            if ($_REQUEST['codigop'] != '') {
                $codigop = strtoupper($_REQUEST['codigop']);
                $codigoe = '';

                if ($codigop != '' && $tipoBusqueda == 'cp') {
                    $permitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    for ($i = 0; $i < strlen($codigop); $i++) {
                        if (strpos($permitidos, substr($codigop, $i, 1)) === false) {
// echo $apellidos . " no es válido<br>";
                            $resultado2 = false;
                            break;
                        } else {
//echo $apellidos . " es válido<br>";
                            $codigoplantel = $codigop;
                            $resultado2 = true;
                        }
                    }

                    if ($resultado2 == false) {
                        $mensaje.='Por favor ingrese los datos correctos para el campo código, el código DEA de un plantel puede contener números y letras<br>';
//         echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                    }
                }
            }
        } elseif ($_REQUEST['tipoBusqueda'] == 'ce' && $_REQUEST['codigoe'] != '') {
//    echo "jjjj";
            if ($_REQUEST['codigoe'] != '') {
                $codigoe = $_REQUEST['codigoe'];
                $codigop = '';

                if ($codigoe != '' && $tipoBusqueda == 'ce') {
                    $permitidos = "0123456789";
                    for ($i = 0; $i < strlen($codigoe); $i++) {
                        if (strpos($permitidos, substr($codigoe, $i, 1)) === false) {
//echo $codigoe . " no es válido<br>";
                            $resultado = false;
                            break;
                        } else {
//     echo $codigoe . " es válido<br>";
                            $codigoestad = $codigoe;
                            $resultado = true;
                        }
                    }

                    if ($resultado == false) {
                        $mensaje.='Por favor ingrese los datos correctos para el campo código, el código estadístico de un plantel puede contener solo números<br>';
//     echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                    }
                }
            }
        }

        if (($tipoBusqueda == 'ce' && $codigoestad != '' && $codigoplantel == '') || ($tipoBusqueda == 'cp' && $codigoestad == '' && $codigoplantel != '')) {
            $resultadoDatosPlantel = SeguimientoPapelMoneda::model()->datosPlantel($tipoBusqueda, $codigoe, $codigop);

            if ($resultadoDatosPlantel == false) {
                $mensaje.= 'El plantel que consulto no se encontro, por favor si el plantel existe comuniquese con los administradores de este sistema';
            } else {
                $plantel_id = $resultadoDatosPlantel[0]['id'];

                if ($plantel_id != '') {

                    //  $estatus_solicitud_id = 1; // El estatus_solicitud_id=1 es el estatus solicitado de la tabla estatus_solicitud.
                    //   $estatus_actual_id = 11; // El estatus_actual_id=11 es el estatus en solicitud de la tabla estatus_titulo.
                    $resultadoPlanes = SeguimientoPapelMoneda::model()->planesDelPlantel($plantel_id, $periodo_actual_id, $estatus_solicitud_id = null, $estatus_actual_id = null, $a = 'N');

                    if ($resultadoPlanes != false) {
                        $tienePlanes = true;
                        $planesObtenidos = $this->dataProviderPlanes($resultadoPlanes);
                        $suma = array();
                        foreach ($resultadoPlanes as $value) {
                            $total = (int) $value['cantidadestu'];
                            $suma[] = $total;
                        }
                        $sumaTotal = array_sum($suma);
                    } else {
                        $sumaTotal = 0;
                        $planesObtenidos = array();
                        $tienePlanes = false;
                    }
                }
            }
        }

        if ($mensaje != '') {
            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
        } else {


            //        $resultadoExistenciaLoteAsignado = SeguimientoPapelMoneda::model()->verificarExistenciaLote($periodo_actual_id, $plantel_id);
            //          if ($resultadoExistenciaLoteAsignado > 0) {
            //           $sumaPermitida = 0;
            $cargo = Cargo::model()->findAll(array('order' => 'nombre ASC', 'condition' => 'id=20 or id=6'));
            $estatus_solicitud_id = 1;
            $estatus_actual_id = 11;
            $resultadoAsignacionSerial = Titulo::model()->asignacionSerial($plantel_id, $periodo_actual_id, $a = 'D');

            $resultadoPlanes = SeguimientoPapelMoneda::model()->planesDelPlantel($plantel_id, $periodo_actual_id, $estatus_solicitud_id, $estatus_actual_id, $a = 'N');

            $primer_serial = '';
            $ultimo_serial = '';
            $estudiantesConSeriales = Titulo::model()->verificarEstudiantesConSeriales($plantel_id, $periodo_actual_id);

            foreach ($estudiantesConSeriales as $value) {
                $estudiantes_con_serial = $value['estudiantes_con_serial'];
                $estudiantes_total = $value['estudiantes_total'];
            }
            if ($estudiantes_con_serial == $estudiantes_total) {
                $tienenEstudiantes = true;
            }

            if ($resultadoAsignacionSerial != false && $resultadoPlanes != false) {
                $planesObtenidos = $this->dataProviderPlanes($resultadoPlanes);
                $dataProviderAsignacionSerial = $this->dataProviderAsignacionSerial($resultadoAsignacionSerial);
                $dataProviderAsignacionSerialListo = array();

                echo 'vacio';
                $this->renderPartial('atenderSolicitud', array(
                    'resultadoDatosPlantel' => $resultadoDatosPlantel,
                    'planesObtenidos' => $planesObtenidos,
                    'tienePlanes' => $tienePlanes,
                    'sumaTotal' => $sumaTotal,
                    'plantel_id' => $plantel_id,
                    'periodo_actual_id' => $periodo_actual_id,
                    'dataProviderMostrarSeriales' => $dataProviderMostrarSeriales,
                    'codigoe' => $codigoe,
                    'codigop' => $codigop,
                    'tipoBusqueda' => $tipoBusqueda,
                    'cargo' => $cargo,
                    'model' => $model,
                    'primer_serial' => $primer_serial,
                    'ultimo_serial' => $ultimo_serial,
                    //       'sumaPermitida' => $sumaPermitida,
                    'mensaje' => $mensaje,
                    'mensaje_cantidadSeriales' => $mensaje_cantidadSeriales,
                    'dataProviderAsignacionSerial' => $dataProviderAsignacionSerial,
                    'serialAsignados' => $serialAsignados,
                    'dataProviderAsignacionSerialListo' => $dataProviderAsignacionSerialListo,
                    'dataProviderSerialesAsignad' => $dataProviderSerialesAsignad,
                    'tienenEstudiantes' => $tienenEstudiantes
                ));
            } else {

                $verificarEstatusAsignacionEstudiante = Titulo::model()->verificarEstatusAsignacionEstudiante($plantel_id, $periodo_actual_id);
                //     var_dump($verificarEstatusAsignacionEstudiante);
                if ($verificarEstatusAsignacionEstudiante == $sumaTotal) {
                    $serialAsignados = true;
                }
                $resultadoAsignacionSerialListo = Titulo::model()->asignacionSerial($plantel_id, $periodo_actual_id, $a = 'N');
//                var_dump($resultadoAsignacionSerialListo);
//                die();
                $dataProviderAsignacionSerialListo = $this->dataProviderAsignacionSerialListo($resultadoAsignacionSerialListo);
                $dataProviderAsignacionSerial = array();

                $serialesAsignados = SeguimientoPapelMoneda::model()->serialesAsignados($plantel_id, $array = 'SI');

                $arraySeriales = array();
                $lote = 0;
                $cantidad = 0;
                for ($x = 0; $x < count($serialesAsignados); $x++) {
                    $comparar = $serialesAsignados[$x]['serial'];
                    if ($x != count($serialesAsignados) - 1)
                        $primer = $serialesAsignados[$x + 1]['serial'];

                    if (isset($serialesAsignados[$x] ['serial'])) {

                        if ((int) ($comparar + 1) == (int) $primer) {

                            if ($x == 0) {
                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
                                $cantidad = $cantidad + 1;
                            }
                            if ($x != 0) {
                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
                                $cantidad = $cantidad + 1;
                            }
                        } else {
                            $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
                            $lote = $lote + 1;
                            $cantidad = 0;
                            if ($x != count($serialesAsignados) - 1) {
                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x + 1]['serial'];
                                $cantidad = $cantidad + 1;
                                $x = $x + 1;
                            }
                        }
                    } else {

                    }
                }

                if ($serialesAsignados != false) {
                    $seriales = array();
                    for ($y = 0; $y < count($arraySeriales); $y++) {
                        for ($a = 0; $a < count($arraySeriales[$y]); $a++) {
                            if ($a == 0) {
                                $seriales[$y]['primer_serial'] = $arraySeriales[$y][$a];
                            }
                            if ($a == (count($arraySeriales[$y]) - 1)) {
                                $seriales[$y]['ultimo_serial'] = $arraySeriales[$y][$a];
                            }
                            $seriales[$y]['cantidad_serial'] = count($arraySeriales[$y]);
                        }
                    }
                    $dataProviderSerialesAsignad = $this->dataProviderMostrarSeriales($seriales, $mostrarSerialesAsignados = true);
                }

                echo 'aqui';
                $this->renderPartial('atenderSolicitud', array(
                    'resultadoDatosPlantel' => $resultadoDatosPlantel,
                    'planesObtenidos' => $planesObtenidos,
                    'tienePlanes' => $tienePlanes,
                    'sumaTotal' => $sumaTotal,
                    'plantel_id' => $plantel_id,
                    'periodo_actual_id' => $periodo_actual_id,
                    'dataProviderMostrarSeriales' => $dataProviderMostrarSeriales,
                    'codigoe' => $codigoe,
                    'codigop' => $codigop,
                    'tipoBusqueda' => $tipoBusqueda,
                    'cargo' => $cargo,
                    'model' => $model,
                    'primer_serial' => $primer_serial,
                    'ultimo_serial' => $ultimo_serial,
                    //       'sumaPermitida' => $sumaPermitida,
                    'mensaje' => $mensaje,
                    'mensaje_cantidadSeriales' => $mensaje_cantidadSeriales,
                    'dataProviderAsignacionSerial' => $dataProviderAsignacionSerial,
                    'serialAsignados' => $serialAsignados,
                    'dataProviderAsignacionSerialListo' => $dataProviderAsignacionSerialListo,
                    'dataProviderSerialesAsignad' => $dataProviderSerialesAsignad,
                    'tienenEstudiantes' => $tienenEstudiantes
                ));
            }
            //           }
//            elseif ($resultadoExistenciaLoteAsignado == 0) {
//
//                $estatus_solicitud_id = 2; // El estatus_solicitud_id=2 es el estatus en proceso de la tabla estatus_solicitud.
//                $estatus_actual_id = 3; // El estatus_actual_id=3 es el estatus asignado a plantel de la tabla estatus_titulo.
//                $resultadoPlanes = SeguimientoPapelMoneda::model()->planesDelPlantel($plantel_id, $periodo_actual_id, $estatus_solicitud_id, $estatus_actual_id, $a = 'N');
//
//                if ($resultadoPlanes == false) {
//                    $planesObtenidos = array();
//                    $sumaTotal = 0;
//                } else {
//                    $planesObtenidos = $this->dataProviderPlanes($resultadoPlanes);
//
//                    $suma = array();
//                    foreach ($resultadoPlanes as $value) {
//                        $total = (int) $value['cantidadestu'];
//                        $suma[] = $total;
//                    }
//                    $sumaTotal = array_sum($suma);
//                }
//
//                $serialesAsignados = SeguimientoPapelMoneda::model()->serialesAsignados($plantel_id);
//
//                $arraySeriales = array();
//                $lote = 0;
//                $cantidad = 0;
//                for ($x = 0; $x < count($serialesAsignados); $x++) {
//                    $comparar = $serialesAsignados[$x]['serial'];
//                    if ($x != count($serialesAsignados) - 1)
//                        $primer = $serialesAsignados[$x + 1]['serial'];
//
//                    if (isset($serialesAsignados[$x] ['serial'])) {
//
//                        if ((int) ($comparar + 1) == (int) $primer) {
//
//                            if ($x == 0) {
//                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
//                                $cantidad = $cantidad + 1;
//                            }
//                            if ($x != 0) {
//                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
//                                $cantidad = $cantidad + 1;
//                            }
//                        } else {
//                            $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x]['serial'];
//                            $lote = $lote + 1;
//                            $cantidad = 0;
//                            if ($x != count($serialesAsignados) - 1) {
//                                $arraySeriales[$lote][$cantidad] = $serialesAsignados[$x + 1]['serial'];
//                                $cantidad = $cantidad + 1;
//                                $x = $x + 1;
//                            }
//                        }
//                    } else {
//
//                    }
//                }
//
//                if ($serialesAsignados != false) {
//                    $seriales = array();
//                    for ($y = 0; $y < count($arraySeriales); $y++) {
//                        for ($a = 0; $a < count($arraySeriales[$y]); $a++) {
//                            if ($a == 0) {
//                                $seriales[$y]['primer_serial'] = $arraySeriales[$y][$a];
//                            }
//                            if ($a == (count($arraySeriales[$y]) - 1)) {
//                                $seriales[$y]['ultimo_serial'] = $arraySeriales[$y][$a];
//                            }
//                            $seriales[$y]['cantidad_serial'] = count($arraySeriales[$y]);
//                        }
//                    }
//
//
//                    $dataProviderMostrarSeriales = $this->dataProviderMostrarSeriales($seriales, $mostrarSerialesAsignados = true);
//                }
//                $datosFuncionario = SeguimientoPapelMoneda::model()->datosFuncionario($plantel_id);
//
//                if ($datosFuncionario != false) {
//                    $this->renderPartial('mostrarLote', array(
//                        'resultadoDatosPlantel' => $resultadoDatosPlantel,
//                        'planesObtenidos' => $planesObtenidos,
//                        'dataProviderMostrarSeriales' => $dataProviderMostrarSeriales,
//                        'datosFuncionario' => $datosFuncionario
//                    ));
//                } else {
//                    $datosFuncionario = 0;
//                    if (($_REQUEST['tipoBusqueda'] == 'cp' && $_REQUEST['codigop'] != '') || ($_REQUEST['tipoBusqueda'] == 'ce' && $_REQUEST['codigoe'] != '')) {
//                        $this->renderPartial('mostrarLote', array(
//                            'resultadoDatosPlantel' => $resultadoDatosPlantel,
//                            'planesObtenidos' => $planesObtenidos,
//                            'dataProviderMostrarSeriales' => $dataProviderMostrarSeriales,
//                            'datosFuncionario' => $datosFuncionario
//                        ));
//                    }
//                }
//            }
        }
    }

    /* ---------  Fin ------- */


    /* --------- Permite guardar los seriales segun la matricula por el plantel que ingreso------- */

    public function actionGuardarAsignacionSerialesPorPlantel() {

        $plantel_id = $_REQUEST['plantel_id'];
        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_actual_id = $periodo_escolar_actual_id['id'];
        $resultadoAsignacionSerial = Titulo::model()->asignacionSerial($plantel_id, $periodo_actual_id, $a = 'D');

        if ($resultadoAsignacionSerial != false) {

            $transaction = Yii::app()->db->beginTransaction();

            foreach ($resultadoAsignacionSerial as $key => $value) {
                $estudiante_id[] = (int) $value['estudiante_id'];
                $seccion_plantel_periodo_id[] = (int) $value['seccion_plantel_periodo_id'];
                $grado_id[] = (int) $value['grado_id'];
                $seccion_id[] = (int) $value['seccion_id'];
                $plan_id[] = (int) $value['plan_id'];
            }


            $estudiante_id = Utiles::toPgArray($estudiante_id);
            $seccion_plantel_periodo_id = Utiles::toPgArray($seccion_plantel_periodo_id);
            $grado_id = Utiles::toPgArray($grado_id);
            $seccion_id = Utiles::toPgArray($seccion_id);
            $plan_id = Utiles::toPgArray($plan_id);


            $modulo = self::MODULO;
            $ip = Yii::app()->request->userHostAddress;
            $username = Yii::app()->user->name;
            $nombre = Yii::app()->user->nombre;
            $apellido = Yii::app()->user->apellido;
            $cedula = Yii::app()->user->cedula;
            $usuario_id = Yii::app()->user->id;

            try {

                $resultadoGuardarAsignacionSeriales = Titulo::model()->asignacionSerialesPorPlantel($estudiante_id, $periodo_actual_id, $plantel_id, $cedula, $nombre, $apellido, $modulo, $ip, $username, $usuario_id);
                //  $this->registerLog('ESCRITURA', $modulo, $resultado_transaccion, $descripcion . $ip, $user_id, $username, $fecha_hora);

                $transaction->commit();
                $respuesta['statusCode'] = 'success';
                $respuesta['mensaje'] = 'Estimado Usuario, el proceso de asignación de seriales por plantel se ha realizado exitosamente.';
                echo json_encode($respuesta);
            } catch (Exception $ex) {
                $transaction->rollback();

                $respuesta['statusCode'] = 'error';
                $respuesta['error'] = $ex;
                $respuesta['mensaje'] = 'Estimado Usuario, ha ocurrido un error durante el proceso de asignación de seriales por plantel. Intente nuevamente.';
                echo json_encode($respuesta);
            }
        }
    }

    /* ---------  Fin ------- */




    /* ---------  Busca si la cedula ingresada en el formulario del  funcionario responsable existe en la base de datos del saime------- */

    public function actionBuscarCedula() {
        if (isset($_REQUEST['cedula'])) {
            $cedula = $_REQUEST['cedula'];
            $existe = false;
            $cedulaArrayDecoded = array();
            $cedulaDecoded = "";
            if (strpos($cedula, "-")) {
                $cedulaArrayDecoded = explode("-", $cedula);
                if (count($cedulaArrayDecoded) == 2) {
                    $origen = $cedulaArrayDecoded[0];
                    $cedulaDecoded = $cedulaArrayDecoded[1];
                    if (!(is_string($origen) && strlen($origen) == 1 && is_numeric($cedulaDecoded) && strlen($cedulaDecoded) > 1 && strlen($cedulaDecoded) <= 8)) {
// MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                        $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                } else {
// MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                    $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
// MENSAJE DE ERROR NO POSEE EL FORMATO CORRECTO V-99999999
                $mensaje = "La Cedula de Identidad no posee el formato correcto, Ej: V-99999999 ó E-99999999";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje));
                Yii::app()->end();
            }

            $this->validarUsuario($origen, $cedulaDecoded);
        } else {
            throw new CHttpException(404, 'No se han especificado los datos necesarios. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    /* ---------  Fin ------- */


    /* ---------  Valida que el usuario que exista en el saime ------- */

    public function validarUsuario($origen, $cedula) {

        $busquedaCedula = AutoridadPlantel::model()->busquedaSaime($origen, $cedula); // valida si existe la cedula en la tabla saime
        if (!$busquedaCedula) {
            $mensaje = "Esta Cedula de Identidad no se encuentra registrada en nuestro sistema, "
                    . "por favor contacte al personal de soporte mediante "
                    . "<a href='mailto:soporte.gescolar@me.gob.ve'>soporte.gescolar@me.gob.ve</a>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); // NO EXISTE EN SAIME
            Yii::app()->end();
        } else {

            $nombreEstudianteSaime = $busquedaCedula['nombre'];
            $apellidoEstudianteSaime = $busquedaCedula['apellido'];
            $nombreFiltrado = Utiles::onlyAlphaNumericWithSpace($nombreEstudianteSaime);
            $apellidoFiltrado = Utiles::onlyAlphaNumericWithSpace($apellidoEstudianteSaime);

            if ($nombreEstudianteSaime != $nombreFiltrado || $apellidoEstudianteSaime != $apellidoFiltrado) {

                echo json_encode(array('statusCode' => 'successU', 'nombre' => strtoupper(trim($busquedaCedula['nombre'])), 'apellido' => strtoupper(trim($busquedaCedula['apellido'])), 'bloqueo' => false));
            } else {

                echo json_encode(array('statusCode' => 'successU', 'nombre' => strtoupper($busquedaCedula['nombre']), 'apellido' => strtoupper($busquedaCedula['apellido']), 'bloqueo' => true));
            }
        }
    }

    /* ---------  Fin ------- */


    /* ---------  Genero la tabla de los planes ------- */

    public function dataProviderPlanes($resultadoPlanes) {

        foreach ($resultadoPlanes as $key => $value) {
            $plan = $value['nombreplan'];
            $mencion = $value['nombremencion'];
            $cantidad = $value['cantidadestu'];

            $rawData[] = array(
                'id' => $key,
                'plan' => '<center>' . $plan . '</center>',
                'mencion' => '<center>' . $mencion . '</center>',
                'cantidad' => '<center>' . $cantidad . '</center>'
            );
        }

        return new CArrayDataProvider($rawData, array(
            'pagination' => false
                )
        );
    }

    /* ---------  Fin ------- */

    public function dataProviderAsignacionSerial($resultadoAsignacionSerial) {

        foreach ($resultadoAsignacionSerial as $key => $value) {
            $apellido_estud = $value['apellido_estud'];
            $nombre_estud = $value['nombre_estud'];
            $cedula_estud = $value['cedula_estud'];
            $plan_estud = $value['plan_estud'];
            $mencion_estud = $value['mencion_estud'];
            $grado_estud = $value['grado_estud'];
            $seccion_estud = $value['seccion_estud'];

            $plan_mencion_estud = $plan_estud . '[' . $mencion_estud . ']';
            $grado_seccion_estud = $grado_estud . ' ' . '"' . $seccion_estud . '"';
            $keyy = $key + 1;
            $rawData[] = array(
                'id' => $key,
                'contador' => '<center>' . $keyy . '</center>',
                'apellido_estud' => '<center>' . $apellido_estud . '</center>',
                'nombre_estud' => '<center>' . $nombre_estud . '</center>',
                'cedula_estud' => '<center>' . $cedula_estud . '</center>',
                'plan_mencion_estud' => '<center>' . $plan_mencion_estud . '</center>',
                'grado_seccion_estud' => '<center>' . $grado_seccion_estud . '</center>'
            );
        }

        return new CArrayDataProvider($rawData, array(
            'pagination' => false
                )
        );
    }

    public function dataProviderAsignacionSerialListo($resultadoAsignacionSerialListo) {

        foreach ($resultadoAsignacionSerialListo as $key => $value) {
            $apellido_estud = $value['apellido_estud'];
            $nombre_estud = $value['nombre_estud'];
            $cedula_estud = $value['cedula_estud'];
            $plan_estud = $value['plan_estud'];
            $mencion_estud = $value['mencion_estud'];
            $grado_estud = $value['grado_estud'];
            $seccion_estud = $value['seccion_estud'];
            $serial = $value['serial'];
            $keyy = $key + 1;

            $plan_mencion_estud = $plan_estud . '[' . $mencion_estud . ']';
            $grado_seccion_estud = $grado_estud . ' ' . '"' . $seccion_estud . '"';

            $rawData[] = array(
                'id' => $key,
                'contador' => '<center>' . $keyy . '</center>',
                'apellido_estud' => '<center>' . $apellido_estud . '</center>',
                'nombre_estud' => '<center>' . $nombre_estud . '</center>',
                'cedula_estud' => '<center>' . $cedula_estud . '</center>',
                'serial' => '<center>' . $serial . '</center>',
                'plan_mencion_estud' => '<center>' . $plan_mencion_estud . '</center>',
                'grado_seccion_estud' => '<center>' . $grado_seccion_estud . '</center>'
            );
        }

        return new CArrayDataProvider($rawData, array(
            'pagination' => false
                )
        );
    }

    /* ---------  Genero la tabla de los seriales con el lote que ingreso ------- */

    public function dataProviderMostrarSeriales($seriales, $mostrarSerialesAsignados) {

        foreach ($seriales as $key => $value) {
            $primer_serial = $value['primer_serial'];
            $ultimo_serial = $value['ultimo_serial'];
            $cantidad_serial = $value['cantidad_serial'];


            if ($mostrarSerialesAsignados == true) {
                $rawData[] = array(
                    'id' => $key,
                    'primer_serial' => '<center>' . $primer_serial . '</center>',
                    'ultimo_serial' => '<center>' . $ultimo_serial . '</center>',
                    'cantidad_serial' => '<center>' . $cantidad_serial . '</center>'
                );
            } else {
                $fecha_entrega = $value['fecha_entrega'];
                $asignado = $value ['asignado'];
                $rawData[] = array(
                    'id' => $key,
                    'primer_serial' => '<center>' . $primer_serial . '</center>',
                    'ultimo_serial' => '<center>' . $ultimo_serial . '</center>',
                    'cantidad_serial' => '<center>' . $cantidad_serial . '</center>',
                    'fecha_entrega' => '<center>' . $fecha_entrega . '</center>',
                    'asignado' => '<center>' . $asignado . '</center>'
                );
            }
        }

        return new CArrayDataProvider($rawData, array(
            'pagination' => false
                )
        );
    }

    /* ---------  Fin ------- */



    /* ---------  generarLetraFromCedula ------- */

    public static function generarLetraFromCedula($cedula) {

        if (is_numeric($cedula)) {
            $numero = $cedula;
        } else {
            $numero = substr($cedula, 2);
        }

        $letra = substr("TRWAGMYFPDXBNJZSQVHLCKE", strtr($numero, "XYZ", "012") % 23, 1);

        return $letra;
    }

    public function loadModel($id) {
        $model = SeccionPlantel::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /* ---------  Fin ------- */

    /**
     * Performs the AJAX validation.
     * @param SeccionPlantel $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'seccion-plantel-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
