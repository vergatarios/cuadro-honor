<?php

class RegistroProgramadoSerialController extends Controller
{

    const A_ACTIVO = 'A';
    const A_EJECUTADO = 'X';
    const A_ELIMINADO = 'E';
    const A_ANULADO = 'D';
    const A_PROCESO = 'P';

    static $_permissionControl = array(
        'read' => 'Consulta Lista de Registro Programado de Seriales',
        'write' => 'Solicitud de Registro Programado de Seriales',
        'admin' => 'Anulación y Reactivación de Registro Programado de Seriales',
        'label' => 'Módulo de Gestión de Registro Programado de Seriales'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index',),
                'pbac' => array('read', 'write', 'admin'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('crear', 'editar'),
                'pbac' => array('write', 'admin'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('anular', 'activar'),
                'pbac' => array('admin',),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCrear()
    {
        $model=new RegistroProgramadoSerial();
        
        $model->zona_educativa_id = 26;
        
        $this->csrfTokenName = 'tokenSerialProgramado';
        
        if(isset($_POST['RegistroProgramadoSerial']))
        {
            if($this->validateCsrfToken()){
                $model->attributes=$_POST['RegistroProgramadoSerial'];
                $model->usuario_ini_id = Yii::app()->user->id;
                $model->fecha_ini = date('Y-m-d H:i:s');
                $model->direccion_ip = $this->getRealIP();
                $model->estatus = self::A_ACTIVO;
                $model->checksum = md5($model->prefijo.$model->serial_inicial.$model->serial_final.$model->fecha_ini.$model->usuario_ini_id.$model->direccion_ip);
                if($model->validate()){ // se valida que las Rules del Modelo se cumplan
                    if($model->save()){ // Se valida que se haya efectuado la operación con éxito
                        $this->registroProgramadoExecuteCommand($model->prefijo, $model->serial_inicial, $model->serial_final);
                        $this->redirect(array('editar', 'id'=>  base64_encode($model->id), 'mensaje'=>'Se ha efectuado la Solicitud de Registro Programado de Seriales de forma exitosa. Prefijo: '.$model->prefijo.'. Serial Inicial: '.$model->serial_inicial.'. Serial Final: '.$model->serial_final.'.'));
                    }
                    else{ // Se ha presentado un error en el sistema
                        $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Ha ocurrido un error en el Sistema. Intentelo más tarde.'));
                        Yii::app()->end();
                    }
                }else{ // Validaciones de las Rules no han sido cumplidas
                    $token = $this->getCsrfToken('*RegistroProgramadoSerial*'); // Se genera un nuevo Token y se Renderiza de nuevo el Formulario el cual presentará los errores que se presentaron en la validación
                }
            }
            else{ // Token del Formulario No Válido
                $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'No se ha podido verificar la autenticidad del Formulario. Recargue la página e intentelo de nuevo.'));
                Yii::app()->end();
            }

        }
        else{
            $token = $this->getCsrfToken('*RegistroProgramadoSerial*');
        }

        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery-ui.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery-ui.min.js'] = false;

        $this->renderPartial('_form_carga_programada', array('registroProgramado'=>$model, 'token'=>$token, 'proceso'=>'crear'), false, true);
    }

    public function actionEditar($id, $mensaje=null){

        $idDecoded = base64_decode($id);

        if(is_numeric($idDecoded)){ // Se valida que el parámetro id es numérico

        $model = $this->loadModel((int)$idDecoded);

            if($model){ // Se valida que el registro solicitado se encuentre registrado

                if($model->estatus==self::A_ACTIVO){ // Solo se puede editar la Solicitud de Registro Programado solo cuando Este se encuentra en Estatus Activo

                    $this->csrfTokenName = 'tokenSerialProgramado';

                    if(isset($_POST['RegistroProgramadoSerial']))
                    {
                        if($this->validateCsrfToken()){
                            $model->attributes=$_POST['RegistroProgramadoSerial'];
                            $model->usuario_act_id = Yii::app()->user->id;
                            $model->fecha_act = date('Y-m-d H:i:s');
                            $direccion_ip_ini = $model->direccion_ip;
                            $model->direccion_ip = $this->getRealIP();
                            $model->estatus = self::A_ACTIVO;
                            $model->checksum = md5($model->prefijo.$model->serial_inicial.$model->serial_final.$model->fecha_ini.$model->fecha_act.$model->usuario_ini_id.$model->usuario_act_id.$direccion_ip_ini.$model->direccion_ip);
                            if($model->validate()){ // se valida que las Rules del Modelo se cumplan
                                if($model->update()){ // Se valida que se haya efectuado la operación con éxito
                                    $mensaje = 'Se han efectuado las modificaciones en la Solicitud de Registro Programado de Seriales de forma exitosa. Prefijo: '.$model->prefijo.'. Serial Inicial: '.$model->serial_inicial.'. Serial Final: '.$model->serial_final.'.';
                                    $token = $this->getCsrfToken('*RegistroProgramadoSerial*');
                                }
                                else{ // Se ha presentado un error en el sistema
                                    $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Ha ocurrido un error en el Sistema. Intentelo más tarde.'));
                                    Yii::app()->end();
                                }
                            }else{ // Validaciones de las Rules no han sido cumplidas
                                $token = $this->getCsrfToken('*RegistroProgramadoSerial*'); // Se genera un nuevo Token y se Renderiza de nuevo el Formulario el cual presentará los errores que se presentaron en la validación
                            }
                        }
                        else{ // Token del Formulario No Válido
                             $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'No se ha podido verificar la autenticidad del Formulario. Recargue la página e intentelo de nuevo.'));
                            Yii::app()->end();
                        }

                    }
                    else{ // Si no viene ningún dato de formulario solo se genera el token para luego renderizar el formulario
                        $token = $this->getCsrfToken('*RegistroProgramadoSerial*');
                    }

                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                    Yii::app()->clientScript->scriptMap['jquery-ui.js'] = false;
                    Yii::app()->clientScript->scriptMap['jquery-ui.min.js'] = false;

                    $this->renderPartial('_form_carga_programada', array('registroProgramado'=>$model, 'token'=>$token, 'proceso'=>'editar', 'mensaje'=>$mensaje), false, true);

                }
                else{ // Si el Registro no se encuentra en estatus activo no se puede editar
                    $this->renderPartial("//msgBox", array('class' => 'alertDialogBox', 'message' => 'No se ha podido completar la operación de edición de la Solicitud de registro Programado. Debido a que este registro se encuentra en Estatus: '.  strtr($modelIni->estatus, array(self::A_ACTIVO=>'Activo', self::A_ANULADO=>'Anulado', self::A_PROCESO=>'En Proceso', self::A_EJECUTADO=>'Ejecutado', self::A_ELIMINADO=>'Eliminado')).'.'));
                }
            }
            else{ // Si no se encuentra el registro por el ID
                throw new CHttpException(404, 'No se ha encontrado el Registro Solicitado');
            }
        }
        else{ // Si el ID no es numérico
            throw new CHttpException(400, 'Los datos suministrados no son suficientes para efectuar esta operación');
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionAnular($id)
    {
        
        $idDecoded = base64_decode($id);
        
        if(is_numeric($idDecoded)){
        
            $model = $this->loadModel($idDecoded);

            if($model){

                if($model->estatus == 'A'){

                    $model->estatus = 'E';
                    $model->usuario_eli_id = Yii::app()->user->id;
                    $model->fecha_elim = date('Y-m-d H:i:s');

                    if($model->validate()){ // se valida que las Rules del Modelo se cumplan
                        if($model->update()){ // Se valida que se haya efectuado la operación con éxito
                            $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Se han efectuado la Anulación de la Solicitud de Registro Programado de Seriales de forma exitosa. Prefijo: '.$model->prefijo.'. Serial Inicial: '.$model->serial_inicial.'. Serial Final: '.$model->serial_final.'. Ha sido Anulado.'));

                        }
                        else{ // Se ha presentado un error en el sistema
                            $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Ha ocurrido un error en el Sistema. Intentelo más tarde.'));
                        }    
                    }
                    else{
                        $this->renderPartial("//errorSumMsg", array('model' => $model));
                    }

                }else{
                    $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'El Registro Programado no se encuentra Activo en estos momentos por lo que no puede ser modificado ni anulado. Este registro se encuentra en Estatus: '.  strtr($model->estatus, array(self::A_ACTIVO=>'Activo', self::A_ANULADO=>'Anulado', self::A_PROCESO=>'En Proceso', self::A_EJECUTADO=>'Ejecutado', self::A_ELIMINADO=>'Eliminado'))));
                }

            }else{
                throw new CHttpException(404, 'No se ha encontrado el Registro Solicitado');
            }

        }
        else
        {
            throw new CHttpException(400, 'Los datos suministrados no son suficientes para efectuar esta operación');
        }
    }
    
    /**
     * Reactivate a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionActivar($id)
    {
        $idDecoded = base64_decode($id);
        
        if(is_numeric($idDecoded)){
        
            $model = $this->loadModel($idDecoded);

            if($model){

                if($model->estatus == 'E'){

                    $model->estatus = 'A';
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_act = date('Y-m-d H:i:s');

                    if($model->validate()){ // se valida que las Rules del Modelo se cumplan
                        if($model->update()){ // Se valida que se haya efectuado la operación con éxito
                            $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Se han efectuado la Reactivación de la Solicitud de Registro Programado de Seriales de forma exitosa. Prefijo: '.$model->prefijo.'. Serial Inicial: '.$model->serial_inicial.'. Serial Final: '.$model->serial_final.'. Ha sido Reactivado.'));
                        }
                        else{ // Se ha presentado un error en el sistema
                            $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'Ha ocurrido un error en el Sistema. Intentelo más tarde.'));
                        }
                    }
                    else{
                        $this->renderPartial("//errorSumMsg", array('model' => $model));
                    }

                }else{
                    $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'El Registro Programado no se encuentra Eliminado en estos momentos por lo que no puede ser modificado ni Reactivado. Este registro se encuentra en Estatus: '.  strtr($model->estatus, array(self::A_ACTIVO=>'Activo', self::A_ANULADO=>'Anulado', self::A_PROCESO=>'En Proceso', self::A_EJECUTADO=>'Ejecutado', self::A_ELIMINADO=>'Eliminado'))));
                }

            }else{
                throw new CHttpException(404, 'No se ha encontrado el Registro Solicitado');
            }
        }
        else
        {
            throw new CHttpException(400, 'Los datos suministrados no son suficientes para efectuar esta operación');
        }

    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('RegistroProgramadoSerial');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new RegistroProgramadoSerial('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['RegistroProgramadoSerial']))
            $model->attributes=$_GET['RegistroProgramadoSerial'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }
    
    
    public function registroProgramadoExecuteCommand($prefijo, $serialInicial, $serialFinal){

        $ruta = yii::app()->basePath;
        $ruta = $ruta . '/yiic';
        $grupoId = Yii::app()->user->group;

        $resultado_comando = shell_exec("nohup /usr/bin/php $ruta RegistroProgramadoSeriales RegistroProgramado --prefijo=$prefijo --serialInicial=$serialInicial --serialFinal=$serialFinal --grupoId=$grupoId > /tmp/REG_PROG_SERIALES_$(date +%Y%m%d%H%M%S).log  & echo $!");
        $respuesta['statusCode'] = 'success';
        $respuesta['resultado'] = $resultado_comando;
        $jsonResponse = json_encode($respuesta);
        
        Utiles::enviarCorreo(Yii::app()->params['adminEmailSend'], 'Notificación de Registro Programado de Seriales', 'Se está ejecutando el Proceso Nro. '.$resultado_comando." correspondiente al proceso de Registro Programado de Títulos (Prefijo: $prefijo, Serial Inicial: $serialInicial y Serial Final: $serialFinal).");
        
        echo $jsonResponse;
    }
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return RegistroProgramadoSerial the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=RegistroProgramadoSerial::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param RegistroProgramadoSerial $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='registro-programado-serial-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
