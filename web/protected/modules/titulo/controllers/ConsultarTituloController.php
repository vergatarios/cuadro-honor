<?php

class ConsultarTituloController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consulta de Título',
        'write' => 'Consulta de Título',
        'label' => 'Consulta de Título'
    );

    const MODULO = "titulo.ConsultarTitulo";

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
//'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'BusquedaEstudianteEgresado', 'dataProviderConsultaTitulo'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('guardarLiquidacionSeriales'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

            $this->render('index', array(
                ));
            
    }

    public function actionBusquedaEstudianteEgresado() {
        if (Yii::app()->request->isAjaxRequest) {

            $tipoBusqueda = $_POST['tipoNacionalidad'];
            $EstuadianteEgresado = $_POST['EstuadianteEgresado'];




            if (($tipoBusqueda == 'E' || $tipoBusqueda == 'P' || $tipoBusqueda == 'V')) {
                $permitidos = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ";
                for ($i = 0; $i < strlen($EstuadianteEgresado); $i++) {
                    if (strpos($permitidos, substr($EstuadianteEgresado, $i, 1)) === false) {
                        //  echo $nombres . " no es válido<br>";
                        $resultado = false;
                        break;
                    } else {
                        //echo $nombres . " es válido<br>";
                        //$nombres = $nombres;
                        $resultado = true;
                    }
                }
                if ($resultado == false) {
                    $respuesta['statusCode'] = 'alert';
                    $respuesta['mensaje'] = 'Estimado Usuario, solo se permite ingresar letras y números.';
                    echo json_encode($respuesta);
                    Yii::app()->end();
                }

                $resultadoBusqueda = Titulo::model()->buscarEstTitulo($tipoBusqueda, $EstuadianteEgresado);


                if ($resultadoBusqueda) {
                    $estudianteID = $resultadoBusqueda[0]['estudiante_id'];
                    if ($estudianteID) {
                        $EstuadianteEgresado = (int) $EstuadianteEgresado;
                         $compara = false;
                        for ($i = 1; $i < 5; $i++) {
                            $coincidir = $i;
                            if ($i == 4) {
                                $coincidir = 'P';
                            }

                            $buscarNotas = Titulo::model()->buscarNotaEstudiante($EstuadianteEgresado, $coincidir);
                          
                            
                            if ($buscarNotas) {

                                $dataConsultarNota[] = $this->dataProviderConsultaNota($buscarNotas);
                                $compara = TRUE;
                            } 

                                
                            
                        }
//                        var_dump($dataConsultarNota[3]);
//                        die();
                        if ($compara == false) {
                            $dataConsultarNota = array();
                        }
//                        var_dump($dataConsultarNota);
//                        die();
                        $historialEstudiante = Estudiante::model()->historicoEstudiante($EstuadianteEgresado, $estudianteID);
                        if (!$historialEstudiante) {
                            $historialEstudiante = array();
                        }

                        $dataConsultaTitulo = $this->dataProviderConsultaTitulo($resultadoBusqueda);


//
//                        var_dump($dataConsultarNota);
//                        var_dump($dataConsultaTitulo);
                        // die();
                        $this->renderPartial('mostrarResultadoBusqueda', array('dataProvider' => $dataConsultaTitulo,
                        'resultadoBusqueda' => $resultadoBusqueda,
                        'historicoEstudiante' => $historialEstudiante,
                        'dataConsultarNota' => $dataConsultarNota,
                            //'plantelPK' => $plantelPK
                        ), FALSE, TRUE);
//                $respuesta['statusCode'] = 'alert';
//                echo json_encode($respuesta);
                } else {
                        $respuesta['statusCode'] = 'alert';
                        $respuesta['mensaje'] = 'Ha ocurrido un error por favor reportarlo al administrador del sistema.';
                        echo json_encode($respuesta);
                        Yii::app()->end();
                    }
                } else {

                    $respuesta['statusCode'] = 'alert';
                $respuesta['mensaje'] = 'Estimado Usuario, no se encontro al estudiante egresado que esta buscando.';
                    echo json_encode($respuesta);
                    Yii::app()->end();
                }

//            var_dump($resultadoBusqueda);
//            die();
        } else {
                $respuesta['statusCode'] = 'alert';
                $respuesta['mensaje'] = 'Estimado Usuario,se ha alterado los datos por favor intente de nuevo.';
                echo json_encode($respuesta);
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    function dataProviderConsultaTitulo($resultadoBusqueda) {
        foreach ($resultadoBusqueda as $key => $value) {
//            var_dump($resultadoBusqueda);
//            die();
            $cedula_identidad = $value['documento_identidad'];
            $nombres = $value['nombres'];
            $apellidos = $value['apellidos'];
            $plantel = $value['nombreplantel'];
            $plan_nombre = $value['nombre_plan'];
            $nombre_mencion = $value['nombre_mencion'];
            $cod_estadistico = $value['cod_estadistico'];
            $serial = $value['serial'];
           // $anoEgreso = $value['ano_egreso'];
            (array_key_exists('ano_egreso',$value))?$anoEgreso = $value['ano_egreso']:$anoEgreso = null;

            $fechaOtorgacion = $value['fecha_otorgamiento'];

            $fechaOtorgacion = date("d-m-Y", strtotime($fechaOtorgacion));

            $plantelCode_estadi = $plantel . ' ' . $cod_estadistico;
            $nombreApellido = $nombres . ' ' . $apellidos;
//            $botones = "<div class='center'>" . CHtml::checkBox('EstSolicitud[]', "false", array('value' => base64_encode($value['id']),
////                   'onClick' => "Estudiante('')",
//                        "title" => "solicitud")
//                    ) .
//                    "</div>";
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'cedula_identidad' => '<center>' . $cedula_identidad . '</center>',
                'nombreApellido' => '<center>' . $nombreApellido . '</center>',
                'plan_nombre' => '<center>' . $plan_nombre . '</center>',
                'nombre_mencion' => "<center>" . $nombre_mencion . '</center>',
                'serial' => "<center>" . $serial . "</center>",
                'plantelCode_estadi' => "<center>" . $plantelCode_estadi . '</center>',
                'anoEgreso' => "<center>" . $anoEgreso . '</center>',
                'fechaOtorgacion' => "<center>" . $fechaOtorgacion . '</center>'
                    //  'botones' => '<center>' . $botones . '</center>'
            );
        }
        // var_dump($rawData);
        //  die();
        return new CArrayDataProvider($rawData, array(
            'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
                )
        );
    }
    function dataProviderConsultaNota($buscarNotas) {
//        var_dump($buscarNotas);
//        die();
        foreach ($buscarNotas as $key => $value) {
//            var_dump($buscarNotas['materia']);
//            die();
            $materia = $value['materia'];
            $mencion = $value['mencion'];
            $nota = $value['nota'];
//            $botones = "<div class='center'>" . CHtml::checkBox('EstSolicitud[]', "false", array('value' => base64_encode($value['id']),
////                   'onClick' => "Estudiante('')",
//                        "title" => "solicitud")
//                    ) .
//                    "</div>";
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'materia' => '<center>' . $materia . '</center>',
                'mencion' => '<center>' . $mencion . '</center>',
                'nota' => '<center>' . $nota . '</center>',
                    //  'botones' => '<center>' . $botones . '</center>'
            );
        }

        //  die();
//        var_dump($rawData);
//        die();
        return new CArrayDataProvider($rawData, array(
            'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
                )
        );
    }

}
