<div id="index">
    <?php
    $this->breadcrumbs = array(
        'Validación de Entrega de Seriales',
    );
    ?>

    <div id="error" class="hide errorDialogBox" >
        <p></p>
    </div>

    <div id="alerta" class="hide alertDialogBox" >
        <p></p>
    </div>
    <div id="seleccionPlantel">
    </div>

    <div id="exitoso" class="hide successDialogBox" >
        <p></p>

        <div class="col-md-6" style="padding-top: 4%">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("titulo/seguimientoTitulo/indexZonaEdu"); ?>"class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
    </div>

    <div id="zonaEdu">
        <div class = "widget-box">

            <div class = "widget-header">
                <h5>Validación de Entrega de Seriales a los Planteles</h5>

                <div class = "widget-toolbar">
                    <a href = "#" data-action = "collapse">
                        <i class = "icon-chevron-down"></i>
                    </a>
                </div>

            </div>

            <div class = "widget-body">
                <div style = "display: block;" class = "widget-body-inner">
                    <div class = "widget-main">
                        <div id="informacion" style="padding-top: 4px">
                            <?php if (isset($resultadoPlantel) && $resultadoPlantel !== array()) { ?>
                                <div class="infoDialogBox">
                                    <p>
                                        <b> Por favor seleccione los planteles que le entregó los seriales.</b>
                                    </p>
                                </div>
                            <?php } ?>
                            <?php if (isset($mostrarEntregadoPlantel) && $mostrarEntregadoPlantel !== array()) { ?>
                                <?php if ($verificarControlPlantel == 4) { ?>
                                    <div class="successDialogBox">
                                        <p>
                                            <b>El proceso de validación de lotes de papel moneda  a los planteles ha culminado con exito. <br>
                                                A continuación se le muestra los planteles que verificó la entrega de lotes de seriales.</b>
                                        </p>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <div class="row row-fluid" style="padding-bottom: 4px; padding-top: 4px">


                            <div id="1eraFila" class="col-md-12">

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div class="col-md-12">
                                    <?php
                                    if (isset($resultadoPlantel) && $resultadoPlantel !== array()) {
                                        $this->widget(
                                                'zii.widgets.grid.CGridView', array(
                                            'id' => 'seriales_plantel',
                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                            'dataProvider' => $resultadoPlantel,
                                            'summaryText' => false,
                                            'columns' => array(
                                                array(
                                                    'name' => 'key',
                                                    'type' => 'raw',
                                                    'header' => '<center></center>'
                                                ),
                                                array(
                                                    'name' => 'plantel',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Plantel</b></center>'
                                                ),
                                                array(
                                                    'name' => 'cant_seriales_asignado_por_plantel',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Seriales Asignados al Plantel</b></center>'
                                                ),
                                                array(
                                                    "name" => "botones",
                                                    "type" => "raw",
                                                    'header' => '<div class="center">' . '<b>Entregado al Plantel</b>' . '<br> ' . CHtml::checkBox('selec_todoPlantel', false, array('id' => 'selec_todoPlantel', 'title' => 'Seleccionar Todos Los Seriales Entregados', 'class' => 'tooltipMatricula')) . "</div>"
                                                ),
                                            ),
                                            'pager' => array(
                                                'header' => '',
                                                'htmlOptions' => array('class' => 'pagination'),
                                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                            ),
                                                )
                                        );
                                    }
                                    ?>
                                    <?php
                                    if (isset($resultadoPlantel) && $resultadoPlantel !== array() && isset($mostrarEntregadoPlantel) && $mostrarEntregadoPlantel !== array() && ($verificarControlPlantel == 0)) {
                                        echo '<hr>';
                                        echo '<div class="successDialogBox">
                                        <p>
                                            <b>
                                                Planteles que ya verificó la entrega de lotes de seriales.
                                            </b>
                                        </p>
                                    </div>';
                                    }
                                    ?>
                                    <?php
                                    if (isset($mostrarEntregadoPlantel) && $mostrarEntregadoPlantel !== array() && ($verificarControlPlantel == 4 || $verificarControlPlantel == 0)) {
                                        $this->widget(
                                                'zii.widgets.grid.CGridView', array(
                                            'id' => 'mostrar_seriales_Plantel',
                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                            'dataProvider' => $mostrarEntregadoPlantel,
                                            'summaryText' => false,
                                            'columns' => array(
                                                array(
                                                    'name' => 'key',
                                                    'type' => 'raw',
                                                    'header' => '<center></center>'
                                                ),
                                                array(
                                                    'name' => 'plantel',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Plantel</b></center>'
                                                ),
                                                array(
                                                    'name' => 'cant_seriales_asignado_por_plantel',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Seriales Asignados al Plantel</b></center>'
                                                ),
                                            ),
                                            'pager' => array(
                                                'header' => '',
                                                'htmlOptions' => array('class' => 'pagination'),
                                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                            ),
                                                )
                                        );
                                    }
                                    ?>


                                    <?php if ($mostrarPlantel == false) { ?>
                                        <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                            <thead>

                                                <tr>
                                                    <th>
                                                        <b></b>
                                                    </th>
                                                </tr>

                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="alertDialogBox">
                                                            <p>
                                                                Aun no se ha asignado ningún serial a un plantel en específico.
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    <?php } ?>
                                    <?php if ($verificarControlPlantel == 2) { ?>
                                        <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                            <thead>

                                                <tr>
                                                    <th>
                                                        <b></b>
                                                    </th>
                                                </tr>

                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="alertDialogBox">
                                                            <p>
                                                                El proceso de validación de lotes de papel moneda al plantel aun no ha culminado, cuando finalice se le notificara por un correo.
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    <?php } ?>

                                </div>

                            </div>

                            <div class = "col-md-12"><div class = "space-6"></div></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12" style="padding-top: 1%">
                <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("../../"); ?>"class="btn btn-danger">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
                <?php if ($verificarControlPlantel == 0) { ?>
                    <button id="btnAsignarPlantel" type="button" class="btn btn-primary btn-next pull-right" role="button" aria-disabled="false"><span class="ui-button-text"><i class="icon-save info bigger-110"></i>&nbsp; Entregado al Plantel</span>
                    </button>
                <?php } ?>
            </div>



        </div>
    </div>
    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
</div>


<script>
    $(document).ready(function() {

        $("html, body").animate({scrollTop: 0}, "fast");

        $("#selec_todoPlantel").unbind('click');
        $("#selec_todoPlantel").click(function() {


            var select = $("#selec_todoPlantel").is(':checked') ? true : false;
            if (select == true) {

                $('input[name="OtorgarPlantel[]"]').each(function() {
                    $(this).attr('checked', 'checked');
                });
            }
            else {

                $('input[name="OtorgarPlantel[]"]').each(function() {
                    $(this).attr('checked', false);
                });
            }

        });





        var style = 'alerta';
        var msgasignado = "<p>Estimado usuario, debe seleccionar por lo menos un Plantel para realizar esta acción.</p>";
        $("#btnAsignarPlantel").unbind('click');
        $("#btnAsignarPlantel").click(function(e) {

            var plantel_select = new Array();
            $('input[name="OtorgarPlantel[]"]:checked').each(function() {
                if ($(this).val() != '') {
                    plantel_select.push($(this).val());
                }
            });
            if (plantel_select.length > 0 && !jQuery.isEmptyObject(plantel_select)) {

                var datos = {
                    plantel_select: plantel_select
                };
                var method = 'POST';
                var urlDir = '/titulo/seguimientoTitulo/asignarPlantel/';
                Loading.show();

                $.ajax({
                    url: urlDir,
                    data: datos,
                    dataType: 'html',
                    type: method,
                    success: function(resp, resp2, resp3) {

                        try {

                            var json = jQuery.parseJSON(resp3.responseText);

                            if (json.statusCode === "success") {

                                $("#seleccionPlantel").addClass('hide');
                                $("#seleccionPlantel p").html('');
                                $("#error").addClass('hide');
                                $("#error p").html('');
                                $("#zonaEdu").addClass('hide');
                                $("#exitoso").removeClass('hide');
                                $("#exitoso p").html(json.mensaje);
                                $("html, body").animate({scrollTop: 0}, "fast");
                                Loading.hide();

                            }
                            else if (json.statusCode === "error") {

                                $("#seleccionPlantel").addClass('hide');
                                $("#seleccionPlantel p").html('');
                                $("#exitoso").addClass('hide');
                                $("#exitoso p").html('');
                                $("#error").removeClass('hide');
                                $("#error p").html(json.mensaje);
                                $("html, body").animate({scrollTop: 0}, "fast");
                                Loading.hide();
                            }

                        } catch (e) {
                            $("#seleccionPlantel").addClass('hide');
                            $("#seleccionPlantel p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                            Loading.hide();
                        }
                        Loading.hide();
                    }
                });
            }
            else {

                displayDialogBox('seleccionPlantel', style, msgasignado);
                $("html, body").animate({scrollTop: 0}, "fast");
                Loading.hide();
            }

        });



    });
</script>


