<div id="index">
    <?php
    $this->breadcrumbs = array(
        'Validación de Entrega de Seriales',
    );
    ?>

    <div id="error" class="hide errorDialogBox" >
        <p></p>
    </div>

    <div id="alerta" class="hide alertDialogBox" >
        <p></p>
    </div>
    <div id="seleccionZonaEdu">
    </div>

    <div id="exitoso" class="hide successDialogBox" >
        <p></p>

        <div class="col-md-6" style="padding-top: 4%">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("titulo/seguimientoTitulo/indexDrcee"); ?>"class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
    </div>

    <div id="drcee">
        <div class = "widget-box">

            <div class = "widget-header">
                <h5>Validación de Entrega de Seriales a las Zona Educativas</h5>

                <div class = "widget-toolbar">
                    <a href = "#" data-action = "collapse">
                        <i class = "icon-chevron-down"></i>
                    </a>
                </div>

            </div>

            <div class = "widget-body">
                <div style = "display: block;" class = "widget-body-inner">
                    <div class = "widget-main">
                        <div id="informacion" style="padding-top: 4px">
                            <?php if (isset($resultadoDrcee) && $resultadoDrcee !== array()) { ?>
                                <div class="infoDialogBox">
                                    <p>
                                        <b> Por favor seleccione las zonas educativas a las que le entregó los seriales.</b>
                                    </p>
                                </div>
                            <?php } ?>
                            <?php if (isset($mostrarEntregadoZona) && $mostrarEntregadoZona !== array()) { ?>
                                <?php if ($verificarControlZonaEdu == 4) { ?>
                                    <div class="successDialogBox">
                                        <p>
                                            <b>El proceso de validación de lotes de papel moneda  a las zonas educativas ha culminado con exito. <br>
                                                A continuación se le muestra las zonas educativas a las que verificó la entrega de lotes de seriales.</b>
                                        </p>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <div class="row row-fluid" style="padding-bottom: 4px; padding-top: 4px">


                            <div id="1eraFila" class="col-md-12">

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div class="col-md-12">
                                    <?php
                                    if (isset($resultadoDrcee) && $resultadoDrcee !== array()) {
                                        $this->widget(
                                                'zii.widgets.grid.CGridView', array(
                                            'id' => 'seriales_zona_educativa',
                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                            'dataProvider' => $resultadoDrcee,
                                            'summaryText' => false,
                                            'columns' => array(
                                                array(
                                                    'name' => 'key',
                                                    'type' => 'raw',
                                                    'header' => '<center></center>'
                                                ),
                                                array(
                                                    'name' => 'zona_educativa',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Zona Educativa</b></center>'
                                                ),
                                                array(
                                                    'name' => 'cant_seriales_asignado',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Seriales Asignados a Estudiantes</b></center>'
                                                ),
                                                array(
                                                    "name" => "botones",
                                                    "type" => "raw",
                                                    'header' => '<div class="center">' . '<b>Entregado a la Zona Educativa</b>' . '<br> ' . CHtml::checkBox('selec_todoZona', false, array('id' => 'selec_todoZona', 'title' => 'Seleccionar Todos Los Seriales Entregados', 'class' => 'tooltipMatricula')) . "</div>"
                                                ),
                                            ),
                                            'pager' => array(
                                                'header' => '',
                                                'htmlOptions' => array('class' => 'pagination'),
                                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                            ),
                                                )
                                        );
                                    }
                                    ?>
                                    <?php
                                    if (isset($resultadoDrcee) && $resultadoDrcee !== array() && isset($mostrarEntregadoZona) && $mostrarEntregadoZona !== array() && ($verificarControlZonaEdu == 0)) {
                                        echo '<hr>';
                                        echo '<div class="successDialogBox">
                                        <p>
                                            <b>
                                                Zonas educativas que ya verificó la entrega de lotes de seriales.
                                            </b>
                                        </p>
                                    </div>';
                                    }
                                    ?>

                                    <?php
                                    if (isset($mostrarEntregadoZona) && $mostrarEntregadoZona !== array() && ($verificarControlZonaEdu == 4 || $verificarControlZonaEdu == 0)) {
                                        $this->widget(
                                                'zii.widgets.grid.CGridView', array(
                                            'id' => 'mostrar_seriales_zona_educativa',
                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                            'dataProvider' => $mostrarEntregadoZona,
                                            'summaryText' => false,
                                            'columns' => array(
                                                array(
                                                    'name' => 'key',
                                                    'type' => 'raw',
                                                    'header' => '<center></center>'
                                                ),
                                                array(
                                                    'name' => 'zona_educativa',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Zona Educativa</b></center>'
                                                ),
                                                array(
                                                    'name' => 'cant_seriales_asignado',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Seriales Asignados a Estudiantes</b></center>'
                                                ),
                                            ),
                                            'pager' => array(
                                                'header' => '',
                                                'htmlOptions' => array('class' => 'pagination'),
                                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                            ),
                                                )
                                        );
                                    }
                                    ?>


                                    <?php if ($mostrarZonas == false) { ?>
                                        <table style="width:650%; " class="table table-striped table-bordered table-hover" class="align-center">
                                            <thead>

                                                <tr>
                                                    <th>
                                                        <b></b>
                                                    </th>
                                                </tr>

                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="alertDialogBox">
                                                            <p>
                                                                Aun no se ha asignado ningún serial a una zona educativa en específico, Por favor diríjase a "asignación seriales por estado".
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    <?php } ?>
                                    <?php if ($verificarControlZonaEdu == 2) { ?>
                                        <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                            <thead>

                                                <tr>
                                                    <th>
                                                        <b></b>
                                                    </th>
                                                </tr>

                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="alertDialogBox">
                                                            <p>
                                                                El proceso de validación de lotes de papel moneda  a las zonas educativas aun no ha culminado, cuando finalice se le notificara por un correo.
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    <?php } ?>

                                </div>

                            </div>

                            <div class = "col-md-12"><div class = "space-6"></div></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12" style="padding-top: 1%">
                <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("../../"); ?>"class="btn btn-danger">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
                <?php if ($verificarControlZonaEdu == 0) { ?>
                    <button id="btnAsignarZonaEdu" type="button" class="btn btn-primary btn-next pull-right" role="button" aria-disabled="false"><span class="ui-button-text"><i class="icon-save info bigger-110"></i>&nbsp; Entregado a Zona Educativa</span>
                    </button>
                <?php } ?>
            </div>



        </div>
    </div>
    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
</div>


<script>
    $(document).ready(function() {

        $("html, body").animate({scrollTop: 0}, "fast");

        $("#selec_todoZona").unbind('click');
        $("#selec_todoZona").click(function() {


            var select = $("#selec_todoZona").is(':checked') ? true : false;
            if (select == true) {

                $('input[name="OtorgarZonaEdu[]"]').each(function() {
                    $(this).attr('checked', 'checked');
                });
            }
            else {

                $('input[name="OtorgarZonaEdu[]"]').each(function() {
                    $(this).attr('checked', false);
                });
            }

        });





        var style = 'alerta';
        var msgasignado = "<p>Estimado usuario, debe seleccionar por lo menos una Zona Educativa para realizar esta acción.</p>";
        $("#btnAsignarZonaEdu").unbind('click');
        $("#btnAsignarZonaEdu").click(function(e) {

            var zona_educativa_select = new Array();
            $('input[name="OtorgarZonaEdu[]"]:checked').each(function() {
                if ($(this).val() != '') {
                    zona_educativa_select.push($(this).val());
                }
            });
            if (zona_educativa_select.length > 0 && !jQuery.isEmptyObject(zona_educativa_select)) {

                var datos = {
                    zona_educativa_select: zona_educativa_select
                };
                var method = 'POST';
                var urlDir = '/titulo/seguimientoTitulo/asignarZonaEducativa/';
                Loading.show();

                $.ajax({
                    url: urlDir,
                    data: datos,
                    dataType: 'html',
                    type: method,
                    success: function(resp, resp2, resp3) {

                        try {

                            var json = jQuery.parseJSON(resp3.responseText);

                            if (json.statusCode === "success") {

                                $("#seleccionZonaEdu").addClass('hide');
                                $("#seleccionZonaEdu p").html('');
                                $("#error").addClass('hide');
                                $("#error p").html('');
                                $("#drcee").addClass('hide');
                                $("#exitoso").removeClass('hide');
                                $("#exitoso p").html(json.mensaje);
                                $("html, body").animate({scrollTop: 0}, "fast");
                                Loading.hide();

                            }
                            else if (json.statusCode === "error") {

                                $("#exitoso").addClass('hide');
                                $("#exitoso p").html('');
                                $("#seleccionZonaEdu").addClass('hide');
                                $("#seleccionZonaEdu p").html('');
                                $("#error").removeClass('hide');
                                $("#error p").html(json.mensaje);
                                $("html, body").animate({scrollTop: 0}, "fast");
                                Loading.hide();
                            }

                        } catch (e) {
                            $("#seleccionZonaEdu").addClass('hide');
                            $("#seleccionZonaEdu p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                            Loading.hide();
                        }
                        Loading.hide();
                    }
                });
            }
            else {

                displayDialogBox('seleccionZonaEdu', style, msgasignado);
                $("html, body").animate({scrollTop: 0}, "fast");
                Loading.hide();
            }

        });



    });
</script>


