<style type="text/css">

    .estudiantes table, .estudiantes th, .estudiantes td {
        border: 1px solid grey;
    }
    .estudiantes table {
        border-collapse: collapse;
        border-spacing: 0px;
        text-align:center;
    }
</style>

<table class="estudiantes" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
       border-spacing: 0px;
       text-align:center; border: 1px solid grey; ">

    <?php
    foreach ($datosReporte as $key => $value) {
        $nombre_plantel = isset($value['nombre_plantel']) ? $value['nombre_plantel'] : '';
        $documento_identidad = isset($value['documento_identidad']) ? $value['documento_identidad'] : '';
        $cedula_escolar = isset($value['cedula_escolar']) ? $value['cedula_escolar'] : '';
        $serial = isset($value['serial']) ? $value['serial'] : '';
        $nombres = isset($value['nombres']) ? $value['nombres'] : '';
        $apellidos = isset($value['apellidos']) ? $value['apellidos'] : '';
        $cod_plantel = isset($value['cod_plantel']) ? $value['cod_plantel'] : '';
        $grado = (isset($value['grado'])) ? $value['grado'] : '';
        $cod_plan = (isset($value['codigo_plan'])) ? $value['codigo_plan'] : '';
        $seccion = isset($value['seccion']) ? $value['seccion'] : '';
        $periodo = isset($value['periodo']) ? $value['periodo'] : '';
        $nacionalidad = isset($value['nacionalidad']) ? $value['nacionalidad'] : '';
        $tdocumento_identidad = isset($value['tdocumento_identidad']) ? $value['tdocumento_identidad'] : '';
        $grado_seccion = '';

        if ($documento_identidad != '') {
            if ($tdocumento_identidad == "") {
                $documento_identidad = $documento_identidad;
            } else {
                $documento_identidad = $tdocumento_identidad . '-' . $documento_identidad;
            }
        } elseif ($documento_identidad == 'No Asignado') {
            $documento_identidad = 'No Asignado';
        }

        if ($grado == 'No Asignado' && $seccion == 'No Asignado') {
            $grado_seccion = 'No Asignado';
        } elseif ($grado != 'No Asignado' && $seccion != 'No Asignado') {
            $grado_seccion = $grado . '   ' . ' " ' . $seccion . ' " ';
        }
        ?>


        <tr>

            <td width="25px" align="center"  >
                <?php echo $key + 1; ?>
            </td>
            <td  width="80px" align="center">
                <?php echo $serial; ?>
            </td>
            <td width="60px" align="center"  >
                <?php echo $nacionalidad; ?>
            </td>
            <td  width="80px"  align="center">
                <?php echo $documento_identidad; ?>
            </td>
            <td  width="80px"  align="center">
                <?php echo $cedula_escolar; ?>
            </td>
            <td  width="120px"  align="center">
                <?php echo $apellidos; ?>
            </td>
            <td  width="120px"  align="center">
                <?php echo $nombres; ?>
            </td>
            <td  width="70px"  align="center">
                <?php echo $grado_seccion; ?>
            </td>
            <td width="70px"  align="center">
                <?php echo $cod_plan; ?>
            </td>
            <td width="140px"  align="center">
                <?php echo $nombre_plantel; ?>
            </td>
            <td  width="70px" align="center">
                <?php echo $cod_plantel; ?>
            </td>
            <td  width="70px" align="center">
                <?php echo $periodo; ?>
            </td>
        </tr>

        <?php
    }
    echo "</table>";
    ?>
<!--    <tr>
    <th colspan="6" class="center">
    </th>


    <th  class="center">
    </th>

</tr>-->






    <br>









