
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'form-registro-programado-serial',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>
    <div class="row-fluid">
        <div class="widget-box">

            <div class="widget-header">
                <h5>Carga Programada de Seriales por Rangos</h5>

                <div class="widget-toolbar">
                    <a data-action="collapse" href="#">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>

            </div>

            <!-- SOY PUTO -->

            <div class="widget-body">
                <div class="widget-body-inner" style="display:block;">
                    <div class="widget-main form">
                        <div class="row row-fluid" id="resultCargaProgramada">
                            <?php if($registroProgramado->hasErrors()): ?>
                            <div class="errorDialogBox">
                                <p>
                                    <?php echo CHtml::errorSummary($registroProgramado); ?>
                                </p>
                            </div>
                            <?php elseif(isset($mensaje) && strlen($mensaje)>0): ?>
                            <div class="successDialogBox">
                                <p>
                                    <?php echo $mensaje; ?> Si lo requiere. Puede editar esta Solicitud. Indique el Prefijo y el Rango de Seriales para solicitar el Registro Programado. <!-- Una vez que el registro se haya completado será informado a su correo Electrónico. --> Todos los campos son obligatorios.
                                </p>
                            </div>
                            <?php else: ?>
                            <div class="infoDialogBox">
                                <p>
                                    Indique el Prefijo y el Rango de Seriales para solicitar el Registro Programado. <!-- Una vez que el registro se haya completado será informado a su correo Electrónico. --> Todos los campos son obligatorios.
                                </p>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="row row-fluid">
                            <div class="col-md-2">
                                <label for="FormRegistroProgramadoSerial_prefijo" class="col-md-12 required">Prefijo <span class="required">*</span></label>
                                <?php echo $form->textField($registroProgramado,'prefijo',array('class'=>'span-7','maxlength'=>2, 'value'=>'AA', 'id'=>'FormRegistroProgramadoSerial_prefijo',)); ?>
                            </div>
                            <div class="col-md-5">
                                <label for="FormRegistroProgramadoSerial_serial_inicial" class="col-md-12 required">Serial Inicial <span class="required">*</span></label>
                                <?php echo $form->textField($registroProgramado,'serial_inicial',array('class'=>'span-7','maxlength'=>20, 'min'=>'0', 'step'=>'1', 'pattern'=>'\d+', 'id'=>'FormRegistroProgramadoSerial_serial_inicial',)); ?>
                            </div>
                            <div class="col-md-5">
                                <label for="FormRegistroProgramadoSerial_serial_final" class="col-md-12 required">Serial Final <span class="required">*</span></label>
                                <?php echo $form->textField($registroProgramado,'serial_final',array('class'=>'span-7','maxlength'=>20, 'id'=>'FormRegistroProgramadoSerial_serial_final',)); ?>
                            </div>
                            <div class="col-md-12">
                                <div class="space-6">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="FormRegistroProgramadoSerial_zona_educativa_id" class="col-md-12 required">Zona Educativa Asignada <span class="required">*</span></label>
                                <?php
                                        $lista = ZonaEducativa::model()->getZonasEducativasActivas();

                                        echo $form->dropDownList($registroProgramado, 'zona_educativa_id', CHtml::listData($lista, 'id', 'nombre'), array(
                                            'empty' => '- SELECCIONE -',
                                            'id' => 'FormRegistroProgramadoSerial_zona_educativa_id',
                                            'class' => 'span-12',
                                            'empty' => array('' => '- SELECCIONE -'),
                                        ));
                                ?>
                            </div>
                            <input type="hidden" name="tokenSerialProgramado" id="FormRegistroProgramadoSerial_tokenProgramado" value="<?php echo $token; ?>" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php $this->endWidget(); ?>

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/titulo/registro/form_programado.js',CClientScript::POS_END); ?>