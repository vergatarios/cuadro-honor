    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'registro-programado-serial-grid',
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'dataProvider'=>$registroProgramado->search(),
        'filter'=>$registroProgramado,
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                    function(){

                        $('#RegistroProgramadoSerial_fecha_ini').datepicker();
                        $.datepicker.setDefaults($.datepicker.regional = {
                                dateFormat: 'dd-mm-yy',
                                showOn:'focus',
                                showOtherMonths: false,
                                selectOtherMonths: true,
                                changeMonth: true,
                                changeYear: true,
                                minDate: new Date(1800, 1, 1),
                                maxDate: 'today'
                            });

                        $('#RegistroProgramadoSerial_fecha_carga').datepicker();
                        $.datepicker.setDefaults($.datepicker.regional = {
                                dateFormat: 'dd-mm-yy',
                                showOn:'focus',
                                showOtherMonths: false,
                                selectOtherMonths: true,
                                changeMonth: true,
                                changeYear: true,
                                minDate: new Date(1800, 1, 1),
                                maxDate: 'today'
                            });

                        $('#RegistroProgramadoSerial_serial_inicial').bind('keyup blur', function () {
                            keyNum(this, false);
                        });

                        $('#RegistroProgramadoSerial_serial_inicial').bind('blur', function () {
                            clearField(this);
                        });

                        $('#RegistroProgramadoSerial_serial_final').bind('keyup blur', function () {
                            keyNum(this, false);
                        });

                        $('#RegistroProgramadoSerial_serial_final').bind('blur', function () {
                            clearField(this);
                        });
                    }
                ",
        'columns'=>array(
            array(
                'header' => '<center>Serial Inicial</center>',
                'name' => 'serial_inicial',
                'filter' => CHtml::textField('RegistroProgramadoSerial[serial_inicial]', $registroProgramado->serial_inicial, array('title' => 'Serial Inicial', 'maxlength' => '20')),
            ),
            array(
                'header' => '<center>Serial Final</center>',
                'name' => 'serial_final',
                'filter' => CHtml::textField('RegistroProgramadoSerial[serial_final]', $registroProgramado->serial_final, array('title' => 'Serial Final', 'maxlength' => '20')),
            ),
            array(
                'header' => '<center>Fecha de Registro</center>',
                'name' => 'fecha_ini',
                'value' => array($this, 'fechaIni'),
                'filter' => CHtml::textField('RegistroProgramadoSerial[fecha_ini]', Utiles::transformDate($registroProgramado->fecha_ini, '-', 'ymd', 'dmy'), array('placeHolder' => 'DD-MM-AAAA',)),
            ),
            array(
                'header' => '<center>Registrado por</center>',
                'name' => 'username_ini',
                'value' => '(is_object($data->usuarioIni))?$data->usuarioIni->username.": ".$data->usuarioIni->nombre." ".$data->usuarioIni->apellido:""',
                'filter' => CHtml::textField('RegistroProgramadoSerial[username_ini]', (is_object($registroProgramado->usuarioIni))?$registroProgramado->usuarioIni->username:"", array('title' => 'Usuario que ha efectuado el Registro', 'maxlength' => '15')),
            ),
            array(
                'header' => '<center>Fecha de Ejecución</center>',
                'name' => 'fecha_carga',
                'value' => array($this, 'fechaCarga'),
                'filter' => CHtml::textField('RegistroProgramadoSerial[fecha_carga]', Utiles::transformDate($registroProgramado->fecha_carga, '-', 'ymd', 'dmy'), array('placeHolder' => 'DD-MM-AAAA',)),
            ),
            array(
                'header' => '<center> Estatus </center>',
                'name' => 'estatus',
                'filter' =>CHtml::listData($this->estatus_asig(),'id','nombre'),
                'value' => array($this, 'estatus'),
            ),
            array(
                'type' => 'raw',
                'header' => '<center>Acción</center>',
                'value' => array($this, 'columnaAccionesRegistroProgramado'),
                'htmlOptions' => array('nowrap'=>'nowrap'),
            ),
        ),
        'emptyText' => 'No se han encontrado Solicitudes de Registro Programado de Seriales!',
    )); ?>