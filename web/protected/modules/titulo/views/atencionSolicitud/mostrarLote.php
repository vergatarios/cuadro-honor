<div class = "widget-box collapsed">

    <div class = "widget-header">
        <h5>Identificación del Plantel <?php echo '"' . $resultadoDatosPlantel[0]['nombre'] . '"'; ?></h5>

        <div class = "widget-toolbar">
            <a href = "#" data-action = "collapse">
                <i class = "icon-chevron-down"></i>
            </a>
        </div>

    </div>

    <div class = "widget-body">
        <div style = "display: none;" class = "widget-body-inner">
            <div class = "widget-main">

                <div class="row row-fluid center">
                    <div id="1eraFila" class="col-md-12">
                        <div class="col-md-4" >

                            <?php echo CHtml::label('<b>Código del Plantel</b>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['cod_plantel'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                        <div class="col-md-4" >
                            <?php echo CHtml::label('<b>Código Estadistico</b>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['cod_estadistico'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                        <div class="col-md-4" >
                            <?php echo CHtml::label('<b>Denominación</b>', '', array("class" => "col-md-12")); ?>
                            <?php
                            echo CHtml::textField('', $resultadoDatosPlantel[0]['denominacion'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                            ?>
                        </div>

                    </div>

                    <div class = "col-md-12"><div class = "space-6"></div></div>

                    <div id="2daFila" class="col-md-12">
                        <div class="col-md-4" >
                            <?php echo CHtml::label('<b>Nombre del Plantel</b>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['nombre'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                        <div class="col-md-4" >
                            <?php echo CHtml::label('<b>Zona Educativa</b>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['zona_educativa'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                        <div class="col-md-4" >
                            <?php echo CHtml::label('<b>Estado</b>', '', array("class" => "col-md-12")); ?>
                            <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['estado'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                        </div>

                    </div>

                    <div class = "col-md-12"><div class = "space-6"></div></div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php if ($planesObtenidos !== array()) { ?>

    <div class = "widget-box">

        <div class = "widget-header">
            <h5>Planes</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row align-center">
                        <div class="col-md-12" id ="plan">
                            <?php
                            if (isset($planesObtenidos) && $planesObtenidos !== array()) {
                                $this->widget(
                                        'zii.widgets.grid.CGridView', array(
                                    'id' => 'planes',
                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                    'dataProvider' => $planesObtenidos,
                                    'summaryText' => false,
                                    'columns' => array(
                                        array(
                                            'name' => 'plan',
                                            'type' => 'raw',
                                            'header' => '<center><b>Plan</b></center>'
                                        ),
                                        array(
                                            'name' => 'mencion',
                                            'type' => 'raw',
                                            'header' => '<center><b>Mención</b></center>'
                                        ),
                                        array(
                                            'name' => 'cantidad',
                                            'type' => 'raw',
                                            'header' => '<center><b>Cantidad de Estudiantes</b></center>'
                                        ),
                                    ),
                                    'pager' => array(
                                        'header' => '',
                                        'htmlOptions' => array('class' => 'pagination'),
                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                    ),
                                        )
                                );
                            }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

<?php } else { ?>

    <div class = "widget-box">

        <div class = "widget-header">
            <h5></h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row align-center">
                        <div class="col-md-12">

                            <div class="col-md-12" id="1eraFila">
                                <div id="1eraFila" class="col-md-12">

                                    <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                        <thead>

                                            <tr>
                                                <th>
                                                    <b></b>
                                                </th>
                                            </tr>

                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="alertDialogBox">
                                                        <p>
                                                            Para poder asignar los seriales correspondientes a este plantel, el director debe primero solicitar los seriales seg&uacute;n lo establecido en el per&iacute;odo actual.
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>

                                    </table>

                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


<?php } ?>


<?php if ($dataProviderMostrarSeriales != array()) { ?>
    <!-- Muestro la parte de agregar seriales por lotes -->

    <div class = "widget-box">

        <div class = "widget-header">
            <h5>Seriales de Facsímiles</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row align-center">
                        <div class="col-md-12" id ="serial">
                            <div class="col-md-12">

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div id='mostarSeriales' class="col-md-12">

                                    <div class = "col-md-12"><div class = "space-6"></div></div>
                                    <div class="col-md-12">
                                        <?php
                                        if (isset($dataProviderMostrarSeriales) && $dataProviderMostrarSeriales !== array()) {
                                            $this->widget(
                                                    'zii.widgets.grid.CGridView', array(
                                                'id' => 'planes',
                                                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                                'dataProvider' => $dataProviderMostrarSeriales,
                                                'summaryText' => false,
                                                'columns' => array(
                                                    array(
                                                        'name' => 'primer_serial',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Primer Serial</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'ultimo_serial',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Ultimo Serial</b></center>'
                                                    ),
                                                    array(
                                                        'name' => 'cantidad_serial',
                                                        'type' => 'raw',
                                                        'header' => '<center><b>Cantidad de Estudiantes</b></center>'
                                                    ),
                                                ),
                                                'pager' => array(
                                                    'header' => '',
                                                    'htmlOptions' => array('class' => 'pagination'),
                                                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                                ),
                                                    )
                                            );
                                        }
                                        ?>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php } ?>



<?php if ($datosFuncionario != 0) { ?>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'datosFuncionario_form'
    ));
    ?>
    <div class = "widget-box">

        <div class = "widget-header">
            <h5>Datos del Funcionario Responsable de la Recepción de Títulos</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row align-center">
                        <div class="col-md-12">

                            <div class="col-md-12" id="1eraFila">
                                <div id="1eraFila" class="col-md-12">
                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('Cédula Responsable', '', array("class" => "col-md-12")); ?>
                                        <?php // echo '<input type="text" ata-toggle="tooltip" data-placement="bottom" placeholder="V-0000000" title="Ej: V-99999999 ó E-99999999" id="SeguimientoPapelMoneda_cedula_responsable"  maxlength="10" size="10" class="span-7" name="SeguimientoPapelMoneda_cedula_responsable" onkeypress = "return CedulaFormat(this, event)" />'; ?>
                                        <?php echo CHtml::textField('', $datosFuncionario[0]['cedula_responsable'], array('class' => 'span-7', 'style' => 'width: 90%', 'readonly' => true)); ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('Nombres', '', array("class" => "col-md-12")); ?>
                                        <?php echo CHtml::textField('', $datosFuncionario[0]['nombre_responsable'], array('class' => 'span-7', 'style' => 'width: 90%', 'readonly' => true)); ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('Apellidos', '', array("class" => "col-md-12")); ?>
                                        <?php echo CHtml::textField('', $datosFuncionario[0]['apellido_responsable'], array('class' => 'span-7', 'style' => 'width: 90%', 'readonly' => true)); ?>
                                    </div>

                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div id="2eraFila" class="col-md-12">
                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('Origen', '', array("class" => "col-md-12")); ?>
                                        <?php if ($datosFuncionario[0]['origen_responsable'] != null) { ?>
                                            <?php if ($datosFuncionario[0]['origen_responsable'] == 'V') { ?>
                                                <?php echo CHtml::textField('', 'Venezolano(a)', array('class' => 'span-7', 'readonly' => true, 'style' => 'width: 90%')); ?>
                                            <?php } elseif ($datosFuncionario[0]['origen_responsable'] == 'E') { ?>
                                                <?php echo CHtml::textField('', 'Extranjero(a)', array('class' => 'span-7', 'readonly' => true, 'style' => 'width: 90%')); ?>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <?php echo CHtml::textField('', '', array('class' => 'span-7', 'readonly' => true, 'style' => 'width: 90%')); ?>
                                        <?php } ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('Cargo', '', array("class" => "col-md-12")); ?>
                                        <?php
                                        $cargo = Cargo::model()->findByPk($datosFuncionario[0]['cargo_responsable_id']);
                                        if ($cargo['nombre'] != '') {
                                            echo CHtml::textField('', $cargo['nombre'], array('class' => 'span-7', 'readonly' => true, 'style' => 'width: 90%'));
                                        } else {
                                            ?>
                                            <?php
                                            echo CHtml::textField('', '', array('class' => 'span-7', 'readonly' => true, 'style' => 'width: 90%'));
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-4" >
                                        <?php echo CHtml::label('Observación', 'observacion', array("class" => "col-md-12")); ?>
                                        <?php echo CHtml::textArea('observacion', $datosFuncionario[0]['observacion'], array('class' => 'span-7', 'readonly' => true, 'style' => 'width: 90%')); ?>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php $this->endWidget(); ?>
<?php } ?>
<div class="row">
    <div class="col-md-6" style="padding-top: 1%">
        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("titulo/atencionSolicitud"); ?>"class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>
</div>
