<div id="listaSolicitudPorPlantel">
    <div class = "widget-box collapsed">

        <div class = "widget-header">
            <h5>Identificación del Plantel <?php echo '"' . $resultadoDatosPlantel[0]['nombre'] . '"'; ?></h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: none;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row row-fluid center">
                        <div id="1eraFila" class="col-md-12">
                            <div class="col-md-4" >

                                <?php echo CHtml::label('<b>Código del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['cod_plantel'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Código Estadistico</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['cod_estadistico'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Denominación</b>', '', array("class" => "col-md-12")); ?>
                                <?php
                                echo CHtml::textField('', $resultadoDatosPlantel[0]['denominacion'], array('class' => 'span-7', 'readOnly' => 'readOnly'));
                                ?>
                            </div>

                        </div>

                        <div class = "col-md-12"><div class = "space-6"></div></div>

                        <div id="2daFila" class="col-md-12">
                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Nombre del Plantel</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['nombre'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Zona Educativa</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['zona_educativa'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('<b>Estado</b>', '', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::textField('', $resultadoDatosPlantel[0]['estado'], array('class' => 'span-7', 'readOnly' => 'readOnly')); ?>
                            </div>

                        </div>

                        <div class = "col-md-12"><div class = "space-6"></div></div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <div class = "widget-box">

        <div class = "widget-header">
            <h5>Planes</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row align-center">
                        <div class="col-md-12" id ="plan">

                            <?php
                            if (isset($planesObtenidos) && $planesObtenidos !== array()) {
                                $this->widget(
                                        'zii.widgets.grid.CGridView', array(
                                    'id' => 'planes',
                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                    'dataProvider' => $planesObtenidos,
                                    'summaryText' => false,
                                    'columns' => array(
                                        array(
                                            'name' => 'plan',
                                            'type' => 'raw',
                                            'header' => '<center><b>Plan</b></center>'
                                        ),
                                        array(
                                            'name' => 'mencion',
                                            'type' => 'raw',
                                            'header' => '<center><b>Mención</b></center>'
                                        ),
                                        array(
                                            'name' => 'cantidad',
                                            'type' => 'raw',
                                            'header' => '<center><b>Cantidad de Estudiantes</b></center>'
                                        ),
                                    ),
                                    'pager' => array(
                                        'header' => '',
                                        'htmlOptions' => array('class' => 'pagination'),
                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                    ),
                                        )
                                );
                            }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>




    <div class = "widget-box">

        <div class = "widget-header">
            <h5>Asignación de Seriales</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">

                    <div class="row align-center">
                        <div class="col-md-12" id ="asignacionSerial">


                            <div class = "col-md-12"><div class = "space-6"></div></div>

                            <div class="col-md-12">


                                <?php
                                if ($serialAsignados == true) {
                                    ?>
                                    <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                        <thead>

                                            <tr>
                                                <th>
                                                    <b></b>
                                                </th>
                                            </tr>

                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="successDialogBox">
                                                        <p>
                                                            Este plantel ya realizo la asignación de seriales correspondientes.
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>

                                    </table>
                                <?php } ?>
                                <div id="scrolltable" style='border: 0;background: #ffffff; overflow:auto;padding-right: 15px; padding-top: 15px; padding-left: 15px;
                                     padding-bottom: 15px;border-right: #6699CC 0px solid; border-top: #999999 0px solid;border-left: #6699CC 0px solid; border-bottom: #6699CC 0px solid;
                                     scrollbar-arrow-color : #999999; scrollbar-face-color : #666666;scrollbar-track-color :#3333333 ;height:120%; left: 28%; top: 300; width: 100%'>
                                     <?php
                                     if (isset($dataProviderAsignacionSerial) && $dataProviderAsignacionSerial !== array()) {
                                         $this->widget(
                                                 'zii.widgets.grid.CGridView', array(
                                             'id' => 'planes',
                                             'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                             'dataProvider' => $dataProviderAsignacionSerial,
                                             'summaryText' => false,
                                             'columns' => array(
                                                 array(
                                                     'name' => 'contador',
                                                     'type' => 'raw',
                                                     'header' => '<center><b> </b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'apellido_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Apellido</b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'nombre_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Nombre</b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'cedula_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Cédula</b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'plan_mencion_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Plan</b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'grado_seccion_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Grado y Sección</b></center>'
                                                 ),
                                             ),
                                             'pager' => array(
                                                 'header' => '',
                                                 'htmlOptions' => array('class' => 'pagination'),
                                                 'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                 'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                 'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                 'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                             ),
                                                 )
                                         );
                                     } //else {
                                     ?>
                                </div>
                                <div id="scrolltable" style='border: 0;background: #ffffff; overflow:auto;padding-right: 15px; padding-top: 15px; padding-left: 15px;
                                     padding-bottom: 15px;border-right: #6699CC 0px solid; border-top: #999999 0px solid;border-left: #6699CC 0px solid; border-bottom: #6699CC 0px solid;
                                     scrollbar-arrow-color : #999999; scrollbar-face-color : #666666;scrollbar-track-color :#3333333 ;height:120%; left: 28%; top: 300; width: 100%'>
                                     <?php
                                     if (isset($dataProviderAsignacionSerialListo) && $dataProviderAsignacionSerialListo !== array()) {
                                         $this->widget(
                                                 'zii.widgets.grid.CGridView', array(
                                             'id' => 'seriales_listo',
                                             'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                             'dataProvider' => $dataProviderAsignacionSerialListo,
                                             'summaryText' => false,
                                             'columns' => array(
                                                 array(
                                                     'name' => 'contador',
                                                     'type' => 'raw',
                                                     'header' => '<center><b> </b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'cedula_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Cédula</b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'serial',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Serial</b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'apellido_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Apellido</b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'nombre_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Nombre</b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'plan_mencion_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Plan</b></center>'
                                                 ),
                                                 array(
                                                     'name' => 'grado_seccion_estud',
                                                     'type' => 'raw',
                                                     'header' => '<center><b>Grado y Sección</b></center>'
                                                 ),
                                             ),
                                             'pager' => array(
                                                 'header' => '',
                                                 'htmlOptions' => array('class' => 'pagination'),
                                                 'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                 'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                 'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                 'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                             ),
                                                 )
                                         );
                                     }
                                     ?>
                                </div>
                                                              <!--  <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                                                    <thead>

                                                                        <tr>
                                                                            <th>
                                                                                <b></b>
                                                                            </th>
                                                                        </tr>

                                                                    </thead>

                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="alertDialogBox">
                                                                                    <p>
                                                                                        Este plantel no tiene estudiantes matriculados, para asignar seriales deben matricular primero.
                                                                                    </p>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>

                                                                </table>-->

                                <?php
                                //      }
                                ?>




                            </div>
                            <div class = "col-md-12"><div class = "space-6"></div></div>
                        </div>
                        <?php if ($dataProviderAsignacionSerialListo == array()) { ?>
                            <div  class="pull-right" style="padding-left: 20px; padding: 20px" >
                                <button  id = "btnAsignacionSerialPorPlantel"  class="btn btn-primary btn-sm" type="submit" data-last="Finish">
                                    <i class="fa fa-plus icon-on-right"></i>
                                    Asignar Seriales
                                </button>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6" style="padding-top: 2%">
    <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("titulo/atencionSolicitud"); ?>"class="btn btn-danger">
        <i class="icon-arrow-left"></i>
        Volver
    </a>
</div>