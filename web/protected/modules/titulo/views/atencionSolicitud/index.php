<div id="exitoAtenderSolicitud" class="hide successDialogBox">
    <p></p>

    <div class="col-md-12" style="padding-top: 4%">
        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("titulo/atencionSolicitud"); ?>"class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>

</div>


<div id="index">
    <?php
    /* @var $this AtencionSolicitudController */

    $this->breadcrumbs = array(
        'Asignación de Seriales' => array("../../titulo/atencionSolicitud"),
        'Solicitud'
    );
    ?>

    <div class = "widget-box">

        <div class = "widget-header">
            <h5>Consultar Solicitudes del Plantel</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">
                    <div id="respuestaBuscar" class="hide errorDialogBox" ><p></p> </div>
                    <div id="busqueda" class="hide alertDialogBox" ><p></p> </div>
                    <div id="campos_vacios" class="hide alertDialogBox" ><p></p> </div>
                    <div id="informacion">
                        <div class="infoDialogBox">
                            <p>
                                Por favor ingrese el código dea o código estadistico del plantel para consultar las solicitudes de títulos.
                            </p>
                        </div>
                    </div>

                    <div class="row row-fluid" style="padding-bottom: 20px; padding-top: 20px">

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'busqueda_form',
                                // 'action' => 'atencionSolicitud/mostrarConsultaPlantel'
                        ));
                        ?>
                        <div id="1eraFila" class="col-md-12">

                            <div class="col-md-4" >
                                <?php echo CHtml::label('Tipo de Búsqueda <span class="required">*</span>', '', array("class" => "col-md-12", 'id' => 'tipo_busqueda')); ?><span ></span>
                                <?php echo CHTML::dropDownList('tipoBusqueda', 'tipoBusqueda', array('ce' => 'Código Estadístico', 'cp' => 'Código del Plantel'), array('empty' => '-SELECCIONE-', 'class' => 'span-7', 'onChange' => 'tipoBusquedas()', 'style' => 'width: 90%')); ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('Código <span class="required">*</span>', '', array("class" => "col-md-12", 'id' => 'codigo')); ?><span ></span>
                                <div id="codigoEsta" >
                                    <?php echo CHtml::textField('codigoe', '', array('class' => 'span-7', 'maxlength' => 10, 'style' => 'width: 90%')); ?>
                                </div>
                                <div id="codigoPlant" class="hide" >
                                    <?php echo CHtml::textField('codigop', '', array('class' => 'span-7', 'maxlength' => 10, 'style' => 'width: 90%')); ?>
                                </div>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('&nbsp;&nbsp;', '', array("class" => "col-md-12")); ?>
                              <!--  <a id="btnConsultar" href="<?php //echo Yii::app()->createUrl("titulo/atencionSolicitud/mostrarConsultaPlantel/tipoBusqueda");          ?>"class="btn btn-primary btn-sm">
                                    <i class="icon-arrow-left"></i>
                                    Consultar
                                </a> -->
                                <button  id = "btnConsultar"  class="btn btn-primary btn-sm" type="submit" data-last="Finish">
                                    <i class="fa fa-search icon-on-right"></i>
                                    Consultar
                                </button>
                            </div>

                        </div>

                        <div class = "col-md-12"><div class = "space-6"></div></div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" style="padding-top: 1%">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("../../"); ?>"class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
    </div>

    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
</div>

<script>
    $(document).ready(function() {
        $("#codigoe").attr('readonly', true);
        $("html, body").animate({scrollTop: 0}, "fast");
    });

    function tipoBusquedas() {

        $("#codigoe").attr('readonly', false);
        $("#codigop").attr('readonly', false);
        var tipoBusqueda = $("#tipoBusqueda").val();
        //alert(tipoBusqueda);

        if (tipoBusqueda == 'ce') {
            $("#codigop").val('');
            $("#codigoPlant").addClass('hide');
            $("#codigoEsta").removeClass('hide');
            $('#codigoe').bind('keyup blur', function() {
                keyNum(this, false);
                makeUpper(this);
            });
        }

        if (tipoBusqueda == 'cp') {
            $("#codigoe").val('');
            $("#codigoEsta").addClass('hide');
            $("#codigoPlant").removeClass('hide');
            $('#codigop').bind('keyup blur', function() {
                keyAlphaNum(this, false);
                makeUpper(this);
            });
            $('#codigop').bind('keyup blur', function() {
                keyAlphaNum(this, false);
                clearField(this);
            });
        }

        if (tipoBusqueda == '') {
            $("#codigoe").val('');
            $("#codigop").val('');
            $("#codigoe").attr('readonly', true);
            $("#codigop").attr('readonly', true);
        }

    }
</script>

<div id="css_js">
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/titulo/atencionSolicitud.js', CClientScript::POS_END);
    ?>
</div>

<div id="dialog_agregar_lote" class="hide">

</div>
