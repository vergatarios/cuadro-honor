<?php
/**
 * Este controlador permite mediante una API REST administrar los datos de los usuarios del Sistema.
 *
 * @author gabriel
 */
class UserController extends Controller {
    
    private $errorCode;
    
    private $serviceName;
    
    /**
     * these constants rappresent new possible errors
     * @var int
     */
    const ERROR_NONE = 0;
    const ERROR_USERNAME_INVALID = 1;
    const ERROR_PASSWORD_INVALID = 2;
    const ERROR_USER_BANNED = 3;
    const ERROR_USER_INACTIVE = 4;
    const ERROR_USER_APPROVAL = 5;
    const ERROR_PASSWORD_REQUESTED = 6;
    const ERROR_USER_ACTIVE = 7;
    const ERROR_ACTIVATION_CODE = 8;
    
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }
 
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('login',),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionLogin(){
        
        $this->serviceName = 'loginGescolar';
        
        if(Yii::app()->request->isPostRequest){
            $response = array('status'=>404, 'login'=>0, 'Recurso no encontrado.', 'user'=>null,);
            $headers = $this->getRequestHeaders(true);
            $username = trim((isset($headers['X_USERNAME']))?$headers['X_USERNAME']:'');
            $password = trim((isset($headers['X_USERNAME']))?$headers['X_PASSWORD']:'');
            if(strlen($username)==0 OR strlen($password)==0){
                $response = array('status'=>400, 'login'=>0, 'message'=>'Petición inválida: debe indicar los datos de autenticación del Cliente Web Service.', 'user'=>null,);
            }
            else{
                $isValidClient = AplicationAuth::hasPermission($username, $password, $this->serviceName);
                if($isValidClient){
                    $usernameGescolar = $this->getPost("username");
                    $passwordGescolar = $this->getPost("password");
                    $userLoged = $this->loginGescolarUser($usernameGescolar, $passwordGescolar);
                    if($userLoged){
                        $response = array('status'=>200, 'login'=>1, 'message'=>'El Usuario se ha autenticado exitosamente.', 'user'=>$userLoged,);
                    }
                    else{
                        $response = array('status'=>200, 'login'=>0, 'message'=>'El Usuario no corresponde con la Clave ingresada.', 'user'=>array('username'=>$usernameGescolar, 'password'=>$passwordGescolar),);
                    }
                }
                else{
                    $response = array('status'=>401, 'login'=>0, 'message'=>'Usted no se encuentra Autorizado para efectuar esta acción.', 'user'=>array('username'=>$usernameGescolar, 'password'=>$passwordGescolar),);
                }
            }
        }
        else{
            $response = array('status'=>403, 'login'=>0, 'message'=>'No está permitido el envío de datos mediante este método.', 'user'=>null,);
        }
        if(Yii::app()->params['testing']){
            $response['header_info'] = $headers;
            $response['data_post'] = $_POST;
        }
        $contentType = trim((isset($headers['Accept']))?$headers['Accept']:'application/json');
        if($contentType==='text/xml'){
            $this->xmlResponse($response);
        }
        else{
            $this->jsonResponse($response);
        }
    }
    
    private function loginGescolarUser($username, $password){
        
        $model = UserGroupsUser::model()->findByAttributes(array('username' => $username));
        
        //var_dump($model);
        
        if (!count($model))
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if ((int) $model->status === UserGroupsUser::WAITING_ACTIVATION)
            $this->errorCode = self::ERROR_USER_INACTIVE;
        else if ($model->password !== md5($password.$model->getSalt()))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else if ((int) $model->status === UserGroupsUser::WAITING_APPROVAL)
            $this->errorCode = self::ERROR_USER_APPROVAL;
        else if ((int) $model->status === UserGroupsUser::BANNED)
            $this->errorCode = self::ERROR_USER_BANNED;
        else if ((int) $model->status === UserGroupsUser::PASSWORD_CHANGE_REQUEST)
            $this->errorCode = self::ERROR_PASSWORD_REQUESTED;
        else {
            $this->errorCode = self::ERROR_NONE;
        }
        
        if($this->errorCode===0){
            return array('usuario'=>$model->username,'origen'=>$model->origen, 'cedula'=>$model->cedula, 'nombre'=>$model->nombre, 'apellido'=>$model->apellido);
        }
        return false;
    }

}
