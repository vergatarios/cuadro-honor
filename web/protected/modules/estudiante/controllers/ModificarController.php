<?php

class ModificarController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Modificación de Estudiantes',
        'write' => 'Modificación de Estudiantes',
        'admin' => 'Modificación la Cedula Escolar',
        'label' => 'Modificación de Estudiantes'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {

        //LECTURA O CONSULTA
        return array(
            array('allow',
                'actions' => array(
                    'seleccionarMunicipio',
                    'seleccionarParroquia',
                    'seleccionarLocalidad',
                    'seleccionarPoblacion',
                    'seleccionarUrbanizacion',
                    'buscarAutoridad',
                    'buscarEstudiante',
                    'index',),
                'pbac' => array('read'),
            ),
            //en esta seccion colocar todos los action del modulo
            array('allow',
                'actions' => array(
                    'datos',
                    'guardarDatosAntropometricos',
                    'datosTitulo',
                ),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    const MODULO = "Estudiante.ModificarController.";



    public function actionBuscarEstudiante() {
        $documento_identidad = $this->getRequest('documentoIdentidad');
        $tdocumento_identidad = $this->getRequest('tdocumentoIdentidad');
        $estudiante_id = $this->getRequest('estudiante_id');
        $busquedaCedula = null;
        if ($documento_identidad AND $tdocumento_identidad) {
            if (in_array($tdocumento_identidad, array('V', 'E', 'P'))) {
                $estudiantes = new Estudiante;
                $busquedaCedula = AutoridadPlantel::model()->busquedaSaimeMixta($tdocumento_identidad, $documento_identidad); // valida si existe la cedula en la tabla saime
                if (!$busquedaCedula) {
                    $mensaje = "Esta Cedula de Identidad no se encuentra registrada en nuestro sistema, si cree que esto puede ser un error "
                        . "por favor contacte al personal de soporte mediante "
                        . "<a href='mailto:soporte_gescolar@me.gob.ve'>soporte_gescolar@me.gob.ve</a>";
                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); // NO EXISTE EN SAIME
                    Yii::app()->end();
                } else { /* SI LA CEDULA EXISTE EN EL SAIME */
                    // echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'fecha' =>  date("d/m/Y", strtotime($busquedaCedula['fecha'])), 'error' => true, 'mensaje'=>$mensaje  ));
                    /* SI EL ESTUDIANTE YA SE ENCUENTRA REGISTRADO EN LA TABLA DE ESTUDIANTE */
                    $validaCedula = Estudiante::model()->findByAttributes(array('documento_identidad' => $documento_identidad, 'tdocumento_identidad' => $tdocumento_identidad));
                    if (isset($validaCedula)) {
                        $estudiante_id_confirmacion = (is_object($validaCedula) AND isset($validaCedula->estudiante_id)) ? $validaCedula->estudiante_id : null;
                        if ($estudiante_id == $estudiante_id_confirmacion) {
                            $nombreEstudianteSaime = $busquedaCedula['nombre'];
                            $apellidoEstudianteSaime = $busquedaCedula['apellido'];
                            $nombreFiltrado = Utiles::onlyAlphaNumericWithSpace($nombreEstudianteSaime);
                            $apellidoFiltrado = Utiles::onlyAlphaNumericWithSpace($apellidoEstudianteSaime);
                            if ($nombreEstudianteSaime != $nombreFiltrado || $apellidoEstudianteSaime != $apellidoFiltrado) {
                                $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                                echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                            } else {
                                $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                                echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'bloqueo' => true, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                            }
                        } else {
                            $mensaje = "<p>El estudiante asociado a este número de cédula ya está registrado.";
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                            echo json_encode(array('statusCode' => 'mensaje', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento'])), 'edad' => $edad, 'mensaje' => $mensaje)); //'error' => true,
                            Yii::app()->end();
                        }
                    } else { /* SI EL ESTUDIANTE NO SE ENCUENTRA REGISTRADO EN LA TABLA DE ESTUDIANTE */
                        $nombreEstudianteSaime = $busquedaCedula['nombre'];
                        $apellidoEstudianteSaime = $busquedaCedula['apellido'];
                        $nombreFiltrado = Utiles::onlyAlphaNumericWithSpace($nombreEstudianteSaime);
                        $apellidoFiltrado = Utiles::onlyAlphaNumericWithSpace($apellidoEstudianteSaime);
                        if ($nombreEstudianteSaime != $nombreFiltrado || $apellidoEstudianteSaime != $apellidoFiltrado) {
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                            echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                        } else {
                            $edad = Estudiante::model()->calcularEdad(date("Y-m-d", strtotime($busquedaCedula['fecha_nacimiento'])));
                            echo json_encode(array('statusCode' => 'successU', 'nombre' => $busquedaCedula['nombre'], 'apellido' => $busquedaCedula['apellido'], 'edad' => $edad, 'bloqueo' => true, 'fecha_nacimiento' => date("d-m-Y", strtotime($busquedaCedula['fecha_nacimiento']))));
                        }
                    }
                }
            } else {
                $mensaje = "ERROR!!!!! No ha ingresado los parámetros necesarios para cumplir con la respuesta a su petición. La Cédula debe contener solo caracteres numéricos.";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); //
            }
        } else {
            $mensaje = "ERROR!!!!! No ha ingresado los parámetros necesarios para cumplir con la respuesta a su petición. La Cédula debe contener solo caracteres numéricos.";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            echo json_encode(array('statusCode' => 'mensaje', 'mensaje' => $mensaje)); //
        }
    }

    public function actionIndex($id, $bc=null) {
        /**
         * IDENTIFICO SI ES DIRECTOR Y DE SER ASI QUE ESTE CONSULTANDO UNO DE SUS PLANTELES.
         */
        /* CONSULTO SI EL PLANTEL ES DEL DIRECTOR */
        $plantelPK = '';
        $plantel_id = '';
        $nombrePlantel = '';
        $estudiante_id = $id;
        if(strlen($estudiante_id)>0 && is_numeric($this->getIdDecoded($estudiante_id))) {
            $plantel_id = Estudiante::model()->findAll(array('condition' => 'id = :id', 'params'=>array(':id'=>$this->getIdDecoded($estudiante_id))));

            $plantel_anterior_id = $plantel_id[0]['plantel_anterior_id'];
            $plantel_id = $plantel_id[0]['plantel_actual_id'];

            $plantelPK = Plantel::model()->findByPk($plantel_id);
            $plantelAnteriorPK = Plantel::model()->findByPk($plantel_anterior_id);
            $datosPlantel = Plantel::model()->DatosPlantel($plantel_id);
//            var_dump($datosPlantel);die();
            if ($datosPlantel != NULL) {
                $nombrePlantel = $datosPlantel[0]['id'];
            }
//            else if($datosPlantel == NULL && $plantelPK == NULL){
//                $this->redirect('/estudiante/');
//            }
            else if ($datosPlantel == NULL && $plantelPK != NULL && Yii::app()->user->group != UserGroups::DIRECTOR) {
                $nombrePlantel = $plantelPK['id'];
            } else if ($datosPlantel == NULL && $plantelPK != NULL && Yii::app()->user->group == UserGroups::DIRECTOR) {
                $this->redirect('/estudiante/');
            }
        }
        /* EMPIEZA EL MODULO DE MODIFICACION ESTUDIANTE */
        if(strlen($estudiante_id)>0 && is_numeric($this->getIdDecoded($estudiante_id))) {
            $estudiante_id = $this->getIdDecoded($id);
            if (is_numeric($estudiante_id)) {
                $model = $this->loadModel($estudiante_id);

                if (!$model->tdocumento_identidad) {
                    $model->tdocumento_identidad = '';

                    //$model->setScenario('gestionEstudiante');
//                    if (strlen($model->documento_identidad) > 2) {
//                        $model->documento_identidad = 'V-' . $model->documento_identidad;
//                    }
                    if ($model) {
                        /* BUSQUEDA POR INSCRIPCION_ESTUDIANTE Y POR TALUMNOS_ACAD */
//                    var_dump($model->id);die();
                        if(is_numeric($model->documento_identidad)){
                            $identificacion = $model->documento_identidad;

                        }
                        else {
                            if(is_numeric($model->cedula_escolar)){
                                $identificacion = $model->cedula_escolar;
                            }
                            else {
                                $identificacion=null;
                            }
                        }
                        if(is_numeric($identificacion))
                            $historicoEstudiante = Estudiante::model()->historicoEstudiante($identificacion, $model->id);
                        else
                            $historicoEstudiante=array();
                        if ($model->representante_id) {
                            $modelRepresentante = $this->loadModelRepresentante($model->representante_id);
//                            $modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                        } else {
                            $modelRepresentante = new Representante;
                        }
                    }
                } else {

                    //$model->setScenario('gestionEstudiante');
//                if (strlen($model->documento_identidad) > 2) {
//                    $model->documento_identidad = 'V-' . $model->documento_identidad;
//                }
                    if ($model) {
                        /* BUSQUEDA POR INSCRIPCION_ESTUDIANTE Y POR TALUMNOS_ACAD */

                        if(is_numeric($model->documento_identidad)){
                            $identificacion = $model->documento_identidad;

                        }
                        else {
                            if(is_numeric($model->cedula_escolar)){
                                $identificacion = $model->cedula_escolar;
                            }
                            else {
                                $identificacion=null;
                            }
                        }
                        if(is_numeric($identificacion)){
                            $historicoEstudiante = Estudiante::model()->historicoEstudiante($identificacion, $model->id);
                        }
                        else
                            $historicoEstudiante=array();
                        if ($model->representante_id) {
                            $modelRepresentante = $this->loadModelRepresentante($model->representante_id);
//                        $modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                        } else {
                            $modelRepresentante = new Representante;
                        }
                    }
                }

                $modelHistorialMedico = new HistorialMedico;
                $modelDatosAntropometricos = new DatosAntropometricos;
//                var_dump($modelDatosAntropometricos);
//                die();
//                var_dump('hola');

                $modelDatosAntropometricos = $this->loadModelDatosAntropometrico($model->id);
                $estadoCivil = EstadoCivil::model()->findAll();
                $genero = Genero::model()->findAll();
                $estado = Estado::model()->findAll(array('order' => 'nombre ASC'));
                $pais = $model->obtenerPaises();
                $zonaUbicacion = ZonaUbicacion::model()->findAll();
                $condicionVivienda = $model->obtenerCondicionVivienda();
                $tipoVivienda = $model->obtenerTipoVivienda();
                $ubicacionVivienda = $model->obtenerUbicacionVivienda();
                $condicionInfraestructura = CondicionInfraestructura::model()->findAll();
                $etnia = $model->obtenerEtnia();
                $afinidad = Afinidad::model()->findAll(); /* AFINIDAD OTRO REPRESENTANTE LEGAL */
                $afinidadMadre = Afinidad::model()->findAll(array('condition' => "nombre ILIKE 'Madre'")); /* AFINIDAD OTRO REPRESENTANTE MADRE */
                $afinidadPadre = Afinidad::model()->findAll(array('condition' => "nombre ILIKE 'Padre'")); /* AFINIDAD OTRO REPRESENTANTE MADRE */
                $diversidadFuncional = $model->obtenerDiversidadFuncional();
                $tipoSangre = $model->obtenerTipoSangre();
                $profesion = $model->obtenerProfesion();

                $this->render('modificar', array(
                    'model' => $model,
                    'modelRepresentante' => $modelRepresentante,
                    'modelHistorialMedico' => $modelHistorialMedico,
                    'modelDatosAntropometricos' => $modelDatosAntropometricos,
                    'estadoCivil' => $estadoCivil,
                    'genero' => $genero,
                    'estado' => $estado,
                    'pais' => $pais,
                    'zonaUbicacion' => $zonaUbicacion,
                    'condicionVivienda' => $condicionVivienda,
                    'tipoVivienda' => $tipoVivienda,
                    'ubicacionVivienda' => $ubicacionVivienda,
                    'condicionInfraestructura' => $condicionInfraestructura,
                    'etnia' => $etnia,
                    'afinidad' => $afinidad,
                    'afinidadMadre' => $afinidadMadre,
                    'afinidadPadre' => $afinidadPadre,
                    'diversidadFuncional' => $diversidadFuncional,
                    'tipoSangre' => $tipoSangre,
                    'profesion' => $profesion,
                    'nombrePlantel' => $nombrePlantel,
                    'plantel_id' => $plantel_id,
                    'plantelPK' => $plantelPK,
                    'plantelAnteriorPK' => $plantelAnteriorPK,
                    'historicoEstudiante' => $historicoEstudiante,
                    'estudiante_id'=>$id
                ));
            } else {
                throw new CHttpException(404, 'No se ha especificado el Estudiante que desea modificar. Vuelva a la página anterior e intentelo de nuevo.'); // esta vacio el request
            }
        }
    }

    public function actionDatos() {
        /* DATOS REPRESENTANTE */
        if (isset($_POST['Representante'])) {
            $id = $_REQUEST['estudiante_id'];
            $estudiante_id = base64_decode($id);
            if (is_numeric($estudiante_id)) {
                $model = $this->loadModel($estudiante_id);
                if ($model) {
                    if ($model->representante_id) {
                        /* BUSCA EL REPRESENTANTE QUE ESTA ASOCIADO CON EL ESTUDIANTE ACTUAL */
                        $estudiante = Estudiante::model()->findAll(array('condition' => 'id = ' . $estudiante_id));
                        $representante_id_actual = $estudiante[0]['representante_id'];
                        $documento_identidad_representante = $_POST['Representante']['documento_identidad'];
                        $tdocumento_identidad_representante = $_POST['Representante']['tdocumento_identidad'];
                        $representante = Representante::model()->findByAttributes(array('documento_identidad' => $documento_identidad_representante, 'tdocumento_identidad' => $tdocumento_identidad_representante));
                        if ($representante) {
                            if(isset($representante[0])){
                                $representante_id_request = $representante[0]['id'];
                            }
                            if(isset($representante['id'])){
                                $representante_id_request = $representante['id'];
                            }
                        } else {
                            $representante_id_request = NULL;
                        }
                        if ($representante_id_actual == $representante_id_request) {
                            $modelRepresentante = $this->loadModelRepresentante($representante_id_actual);
                            //$modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                        } else {
                            if ($representante_id_request != NULL) {
                                $modelRepresentante = $this->loadModelRepresentante($representante_id_request);
                                //$modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                            } else {
                                $modelRepresentante = new Representante;
                            }
                        }
                    } else {
                        $modelRepresentante = new Representante;
                    }
//                    var_dump($modelRepresentante);die();
                }
            }
            $modelRepresentante->attributes = $_POST['Representante'];
            $datosRepresentante[] = $_POST['Representante'];
            $datosRepresentante[0]['nombres'] = strtoupper($_POST['nombreRepresentante']);
            $datosRepresentante[0]['apellidos'] = strtoupper($_POST['apellidoRepresentante']);
            $datosRepresentante[0]['fecha_nacimiento'] = Utiles::transformDate($_POST['fecha_nacimiento_representante']);
            $datosRepresentante[0]['telefono_habitacion'] = Utiles::onlyNumericString($_POST['Representante']['telefono_habitacion']);
            $datosRepresentante[0]['telefono_movil'] = Utiles::onlyNumericString($_POST['Representante']['telefono_movil']);
            $datosRepresentante[0]['telefono_empresa'] = Utiles::onlyNumericString($_POST['Representante']['telefono_empresa']);

            $modelRepresentante->documento_identidad = $documento_identidad_representante = $datosRepresentante[0]['documento_identidad'];
            $modelRepresentante->tdocumento_identidad = $tdocumento_identidad_representante = $datosRepresentante[0]['tdocumento_identidad'];
            //$numeroCedula = $datosRepresentante[0]['documento_identidad'];
            $representante = Representante::model()->findByAttributes(array('documento_identidad' => $documento_identidad_representante, 'tdocumento_identidad' => $tdocumento_identidad_representante));
            if ($representante) {
                $datosRepresentante[0]['existe'] = true;
                $datosRepresentante[0]['representante_id'] = $representante['id'];
            } else {
                $datosRepresentante[0]['existe'] = false;
            }

            $pais = $model->obtenerDatosPais($datosRepresentante[0]['pais_id']);
            if ($pais[0]['nombre'] == 'Venezuela' || $pais[0]['nombre'] == 'VENEZUELA') {
                $datosRepresentante[0]['estado_nac_id'] = $_POST['Representante']['estado_nac_id'];
            }
//            if ($_POST['Representante']['documento_identidad']) {
//                $datosRepresentante[0]['documento_identidad'] = substr($_POST['Representante']['documento_identidad'], 2);
//            }
            $modelRepresentante->attributes = $datosRepresentante[0];
            $modelRepresentante->nombres = $datosRepresentante[0]['nombres'];
            $modelRepresentante->apellidos = $datosRepresentante[0]['apellidos'];
            $modelRepresentante->sexo = $datosRepresentante[0]['sexo'];
            $modelRepresentante->telefono_habitacion = $datosRepresentante[0]['telefono_habitacion'];
            $modelRepresentante->telefono_movil = $datosRepresentante[0]['telefono_movil'];
            $modelRepresentante->telefono_empresa = $datosRepresentante[0]['telefono_empresa'];
            $modelRepresentante->setScenario('gestionRepresentante');

            if (!$modelRepresentante->validate()) {
                $this->renderPartial('//errorSumMsg', array('model' => $modelRepresentante));
                Yii::app()->end();
            } else {

                if ($modelRepresentante->save()) {
                    $estudiante_id = base64_decode($_REQUEST['estudiante_id']);
                    if (is_numeric($estudiante_id)) {
                        $cedula_escolar = Estudiante::model()->findAll(array('condition' => 'id = ' . $estudiante_id));
                        $representante_id = $model->obtenerYAsignarRepresentanteAlEstudiante($cedula_escolar[0]['documento_identidad'],$cedula_escolar[0]['tdocumento_identidad'], $modelRepresentante->documento_identidad, $modelRepresentante->tdocumento_identidad);
                        $this->registerLog('LECTURA', self::MODULO . 'Index', 'EXITOSO', 'Se modifico un representante con éxito.');
                    }
                    Yii::app()->end();
                }
            }
        }
        /* DATOS ESTUDIANTE */

        if (isset($_POST['Estudiante'])) {
            $id = $_REQUEST['estudiante_id'];
            $estudiante_id = base64_decode($id);
            if (is_numeric($estudiante_id)) {
                $model = $this->loadModel($estudiante_id);
                /*if (strlen($model->documento_identidad) > 2) {
                    $model->documento_identidad = 'V-' . $model->documento_identidad;
                 }*/
                if ($model) {
                    if ($model->representante_id) {
                        $modelRepresentante = $this->loadModelRepresentante($model->representante_id);
                        //$modelRepresentante->documento_identidad = 'V-' . $modelRepresentante->documento_identidad;
                    } else {
                        $modelRepresentante = new Representante;
                    }
                }
            }
            $modeloFormulario = $_POST['Estudiante'];
            $model->attributes = $_POST['Estudiante'];
            if(Yii::app()->user->group==UserGroups::JEFE_DRCEE){
                $model->setScenario('moddrcee');
            }else {
                $model->setScenario('gestionEstudiante');
            }


            $model->tdocumento_identidad = $modeloFormulario['tdocumento_identidad'];
            $model->documento_identidad = $_POST['cedula_hidden'];
            $model->nombres = strtoupper($_POST['nombres_hidden']);
            $model->apellidos = strtoupper($_POST['apellidos_hidden']);
            if ($model->ingreso_familiar == null) {
                $model->ingreso_familiar = 0;
            }

            if (Yii::app()->user->pbac('estudiante.modificar.admin')) {
                if ($_POST['Estudiante']['documento_identidad'] > 0) {
                    $model->documento_identidad = $cedulaEstudiante = $_POST['Estudiante']['documento_identidad'];
                    $model->cedula_escolar = $cedulaEstudiante = trim($_POST['Estudiante']['cedula_escolar']);
                }

                $model->nombres = strtoupper($_POST['Estudiante']['nombres']);
                $model->apellidos = strtoupper($_POST['Estudiante']['apellidos']);
            }

            $model->telefono_movil = Utiles::onlyNumericString($_POST['Estudiante']['telefono_movil']);
            $model->telefono_habitacion = Utiles::onlyNumericString($_POST['Estudiante']['telefono_habitacion']);
            $model->fecha_nacimiento = Utiles::transformDate($_POST['Estudiante']['fecha_nacimiento']);

            $model->documento_identidad = $_POST['Estudiante']['documento_identidad'];
            $model->sexo = $_POST['Estudiante']['sexo'];
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->estatus = "A";


            if (!$model->validate()) {

                $this->renderPartial('//errorSumMsg', array('model' => $model));
                Yii::app()->end();
            } else {
                /* SE REALIZA EL GUARDAR */
                $datosRepresentante = Yii::app()->getSession()->get('datosRepresentante');
                $datosHistorialMedico = Yii::app()->getSession()->get('datosHistorialMedico');
                if ($datosRepresentante[0]['existe'] == true) {
                    /* MODIFICAR LOS DATOS DEL REPRESENTANTE LEGAL */
                    $modelRepresentante = $modelRepresentante->findByPk($datosRepresentante[0]['representante_id']);
                    $modelRepresentante->attributes = $datosRepresentante[0];
                    $modelRepresentante->fecha_nacimiento = Utiles::transformDate($datosRepresentante[0]['fecha_nacimiento']);
                    $modelRepresentante->nombres = $datosRepresentante[0]['nombres'];
                    $modelRepresentante->apellidos = $datosRepresentante[0]['apellidos'];
                    $modelRepresentante->sexo = $datosRepresentante[0]['sexo'];
                    $modelRepresentante->usuario_ini_id = Yii::app()->user->id;
                    $modelRepresentante->fecha_ini = date("Y-m-d H:i:s");
                    $modelRepresentante->estatus = "A";
                }

                /* SI EL ESTUDIANTE NO POSEE CEDULA */
                if ($model->documento_identidad == '') {
                    $model->documento_identidad = NULL;
                }

                if ($model->save()) {
                    if ($model->representante_id == '') {
                        $representante_id = 'NULL';
                    } else {
                        $representante_id = $model->representante_id;
                    }
                    $documento_identidad_representante = Representante::model()->findAll(array('condition' => 'id = ' . $representante_id));
                    $is_empty = empty($documento_identidad_representante);
                    if ($is_empty) {
                        $cedula_representante = 'NULL';
                        $tdocumento_identidad = 'NULL';
                    } else {
                        $cedula_representante = $documento_identidad_representante[0]['documento_identidad'];
                        $tdocumento_identidad = $documento_identidad_representante[0]['tdocumento_identidad'];
                    }
                    $representante_id = $model->obtenerYAsignarRepresentanteAlEstudiante($model->documento_identidad, $model->tdocumento_identidad, $cedula_representante, $tdocumento_identidad);



                    $this->registerLog('ESCRITURA', self::MODULO . 'DATOS', 'EXITOSO', 'Se MODIFICO un estudiante con éxito.');
                }
                Yii::app()->end();
            }
        }

    }

    public function actionGuardarDatosAntropometricos(){
        /* DATOS ANTROPOMETRICOS */
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['DatosAntropometricos']) AND isset($_POST['estudiante_id_mod'])) {
                $estudiante_id = base64_decode($_POST['estudiante_id_mod']);

                $datosAntropometricos = $this->loadModelDatosAntropometrico($estudiante_id);

                if($datosAntropometricos==null){
                    $datosAntropometricos = new DatosAntropometricos();
                    $datosAntropometricos->attributes = $_POST['DatosAntropometricos'];
                }
                else {
                    $postDatosAntropometricos = $this->getPost('DatosAntropometricos');
                    (isset($postDatosAntropometricos['peso']))?$peso =$postDatosAntropometricos['peso'] :$peso = '';
                    (isset($postDatosAntropometricos['talla_camisa']))?$talla_camisa =$postDatosAntropometricos['talla_camisa'] :$talla_camisa = '';
                    (isset($postDatosAntropometricos['talla_zapato']))?$talla_zapato =$postDatosAntropometricos['talla_zapato'] :$talla_zapato = '';
                    (isset($postDatosAntropometricos['talla_pantalon']))?$talla_pantalon =$postDatosAntropometricos['talla_pantalon'] :$talla_pantalon = '';
                    (isset($postDatosAntropometricos['estatura']))?$estatura =$postDatosAntropometricos['estatura'] :$estatura= '';

                    $datosAntropometricos->peso=$peso;
                    $datosAntropometricos->talla_camisa=$talla_camisa;
                    $datosAntropometricos->talla_zapato=$talla_zapato;
                    $datosAntropometricos->talla_pantalon=$talla_pantalon;
                    $datosAntropometricos->estatura=$estatura;
                    $datosAntropometricos->estudiante_id = $estudiante_id;
                }
                $datosAntropometricos->usuario_ini_id = Yii::app()->user->id;
                $datosAntropometricos->estatus = 'A';


                if($datosAntropometricos->validate()){
                    if(!$datosAntropometricos->save()){
                        $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                        Yii::app()->user->setFlash('mensajeError', "$mensaje");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                    }
                }
                else {
                    $this->renderPartial('//errorSumMsg', array('model' => $datosAntropometricos));
                    Yii::app()->end();
                }
            } else
                throw new CHttpException(404, 'No se ha encontrado el recurso que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        } else
            throw new CHttpException(404, 'Estimado Usuario, usted no esta autorizado para acceder mediante esta via.');
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Estudiante the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Estudiante::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadModelRepresentante($id) {
        $model = Representante::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadModelDatosAntropometrico($idEstudiante) {
//        Representante::model()->findByAttributes(array('documento_identidad' => $documento_identidad_repre, 'tdocumento_identidad' => $tdocumento_identidad_repre));
        $model = DatosAntropometricos::model()->findByAttributes(array('estudiante_id' => $idEstudiante));
        if ($model === null)
            $model = new DatosAntropometricos ();
        return $model;
    }

    /**
     *
     * @param base64 $id Id del Estudiante
     * @throws CHttpException
     */
    public function actionDatosTitulo($id){
        $idDecoded = $this->getIdDecoded($id);
        if($this->hasPost('EstudianteVerif') && Yii::app()->request->isAjaxRequest){
            $datosEstudiante = $this->getPost('EstudianteVerif');
            $origenCedula = $this->getPost('origenCedulaEstudiante');
            $nroCedula = $this->getPost('nroCedulaEstudiante');
            if(is_numeric($idDecoded)){
                $model = $this->loadModel($idDecoded);
                if($model){
                    $atributosIniciales = $model->attributes;
                    $model->attributes = $datosEstudiante;
                    $model->fecha_nacimiento = Utiles::transformDate($model->fecha_nacimiento);
                    if($atributosIniciales!=$model->attributes){
                        if($model->save()){
                            $titulos = EstudianteTitulo::model()->getTitulosEstudiante($idDecoded, $origenCedula, $nroCedula);
                            if(!is_null($titulos) && count($titulos)>0){
                                $this->renderPartial("_listaTitulos", array('titulos'=>$titulos));
                            }
                            else{
                                $this->renderPartial("//msgBox", array('class'=>'alertDialogBox', 'message'=>'Esta persona no posee credenciales o títulos registrados en el sistema.'));
                            }
                        }
                        else{
                            $errores = CHtml::errorSummary($model);
                            if(strlen($errores)>1){
                                $this->renderPartial("//errorSumMsg", array('model'=>$model));
                            }else{
                                $this->renderPartial("//msgBox", array('class'=>'errorDialogBox', 'message'=>'Ha ocurrido un error, comuniquese con el administrador del sistema y notifique lo sucedido.'));
                            }
                        }
                    }else{
                        $titulos = EstudianteTitulo::model()->getTitulosEstudiante($idDecoded, $origenCedula, $nroCedula);
                        if(!is_null($titulos) && count($titulos)>0){
                            $this->renderPartial("_listaTitulos", array('titulos'=>$titulos));
                        }
                        else{
                            $this->renderPartial("//msgBox", array('class'=>'alertDialogBox', 'message'=>'Esta persona no posee credenciales o títulos registrados en el sistema.'));
                        }
                    }
                }else{
                    throw new CHttpException(404, 'El estudiante indicado no se ha encontrado.');
                }
            }
            else{
                throw new CHttpException(401, 'No ha proporcionado los datos necesarios para efectuar esta acción.');
            }
        }
        else{
            throw new CHttpException(403, 'No está permitido efectuar esta acción mediante esta vía.');
        }

    }

    /**
     * Performs the AJAX validation.
     * @param Estudiante $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'estudiante-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
