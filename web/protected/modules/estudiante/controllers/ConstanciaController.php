<?php

class ConstanciaController extends Controller {
    public $defaultAction = 'buscar';

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    static $_permissionControl = array(
        'read' => 'Consulta de Constancias',
        'write' => 'Gestion de Constancias',
        'label' => 'Módulo de Constancias'
    );

    /**
     * @Return array filtros de acción
     */

    /**
     * Especifica las reglas de control de acceso.
     * Este método es utilizado por el filtro 'AccessControl'.
     * Reglas de control de acceso a una matriz @ return
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'activar', 'delete', 'buscar', 'prueba'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionBuscar() {
        $error = 4;
        $model = new Constancia();
        if ($this->hasPost('cedula') || $this->hasPost('pasaporte')) {
            Yii::import('ext.qrcode.QRCode');
            $cedula = $this->getPost('cedula');
            $nacionalidad = $this->getPost('nacionalidad');
            $pasaporte = $this->getPost('pasaporte');
            //$comprobar_documento=$this->getPost('comprobar_documento');
            $resultado = Constancia::model()->buscarEstudiante($cedula, $nacionalidad);
            $verificarConstancion=$model->verificarQrCedula($cedula);
           // var_dump($resultado); die();
            if ($resultado){
                if(empty($verificarConstancion)){
                //$id_estudiante = $resultado['estudiante_id'];
                $fecha_nacimiento = $resultado['fecha_nacimiento'];
                $implode=  implode("", $resultado);
                $codigo_qr= md5($implode);
                $url="gescolar.dev/verificacion/ConstanciaVerificar/verificar/verificacion/".$codigo_qr;
                $code = new QRCode($url);
                $code->create(Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $cedula . '.png');
                $command = 'chmod 777 -R . ' . Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $cedula . '.png';
                exec($command);
                $model->codigo_qr=$codigo_qr;
                $model->usuario_act_id=Yii::app()->user->id;
                $model->inscripcion_estudiante_id=$resultado['id_inscripcion'];
                $model->nombre_estudiante=$resultado['nombre_estudiante'];
                $model->apellido_estudiante=$resultado['apellido_estudiante'];
                $model->nombre_plantel=$resultado['nombre_plantel'];
                $model->codigo_estadistico=$resultado['codigo_estadistico'];
                $model->nombre_seccion=$resultado['nombre_seccion'];
                $model->nombre_grado=$resultado['nombre_grado'];
                $model->documento_identidad=$resultado['documento_identidad'];
                $model->nombre_estado=$resultado['estado'];
                $model->nombre_capital=$resultado['capital'];
                $model->nombre_nivel=$resultado['nivel'];
                $model->fecha_nacimiento=$resultado['fecha_nacimiento'];
                $model->url=$url;
                //var_dump($model); die();
                $model->save();
                $mPDF = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 15, 15, 16, 16, 9, 9, 'M');
                $mPDF->WriteHTML($this->renderPartial('_pdfHeader', array(), true));
                $mPDF->WriteHTML($this->renderPartial('view_pdf_estudiante', array('cedula' => $cedula, 'url' => $url, 'nacionalidad' => $nacionalidad, 'fecha_nacimiento' => $fecha_nacimiento), true, false));
                $mPDF->Output('Pdf' . '.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
                }else{
                    //$resultado = Constancia::model()->buscarEstudiante($cedula, $nacionalidad);
                 $url=$verificarConstancion['url'];
                $mPDF = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 15, 15, 16, 16, 9, 9, 'M');
                $mPDF->WriteHTML($this->renderPartial('_pdfHeader', array(), true));
                $mPDF->WriteHTML($this->renderPartial('view_pdf_estudiante', array('cedula' => $cedula, 'url' => $url, 'nacionalidad' => $nacionalidad, 'fecha_nacimiento' => $fecha_nacimiento), true, false));
                $mPDF->Output('Pdf' . '.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
                }
            } else {
                $error = 1;
            }
        }
        $this->render('_form', array('model' => $model, 'error' => $error));
    }


        public function actionVerificar() {
        if (isset($_REQUEST['verificacion'])) {
            $codigo_verificacion = $_REQUEST['verificacion'];
            $verificarQr = TituloDigital::model()->verificarQr($codigo_verificacion);
            if ($verificarQr) {
                $this->render('_verificacion', array('verificarQr' => $verificarQr));
            } else {
                throw new CHttpException(404, 'El Requerimiento no Existe');
            }
        } else {
            throw new CHttpException(404, 'El Requerimiento no Existe');
        }
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Constancia;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Constancia'])) {
            $model->attributes = $_POST['Constancia'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Constancia'])) {
            $model->attributes = $_POST['Constancia'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $groupId = Yii::app()->user->group;
        $model = new Constancia('search');
        if (isset($_GET['Constancia']))
            $model->attributes = $_GET['Constancia'];
        $usuarioId = Yii::app()->user->id;
        $dataProvider = new CActiveDataProvider('Constancia');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
        ));
    }

    public function loadModel($id) {
        $model = Constancia::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    /**
     * Performs the AJAX validation.
     * @param Constancia $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'constancia-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPrueba() {
        Yii::import('ext.qrcode.QRCode');
        $code = new QRCode("data to encode");
        $code->create(Yii::app()->basePath . '/qr/file.png');
    }

    public function columnaAcciones($datas) {
        $id = $datas["id"];
        $id2 = $datas["estatus"];
        $id = base64_encode($id);
        if ($id2 == "A") {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "title" => "Buscar Esta Constancia", "onClick" => "VentanaDialog('$id','/catalogo/mencion/view','Mención','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-pencil green", "title" => "Modificar Constancia", "onClick" => "VentanaDialog('$id','/catalogo/mencion/update','Modificar Mención','update')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa icon-trash red", "title" => "Eliminar Constancia", "onClick" => "VentanaDialog('$id','/catalogo/mencion/borrar','Eliminar Mención','borrar')")) . '&nbsp;&nbsp;';
        } else {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "title" => "Buscar Mencion", "onClick" => "VentanaDialog('$id','/catalogo/mencion/view','Mención','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-check", "title" => "Activar Mencion", "onClick" => "VentanaDialog('$id','/catalogo/mencion/activar','activar Mención','activar')")) . '&nbsp;&nbsp;';
        }
        return $columna;
    }

}
