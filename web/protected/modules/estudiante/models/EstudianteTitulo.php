<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EstudianteTitulo
 *
 * @author gabriel
 */
class EstudianteTitulo extends CActiveRecord {
    
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'matricula.estudiante';
    }
    
    /**
     * 
     * @param integer $id
     * @param string $origenCedula
     * @param string $nroCedula
     */
    public function getTitulosEstudiante($id, $origenCedula, $nroCedula){
        $result = null;
        if(is_numeric($id) && in_array($origenCedula, array('V', 'E', 'P')) && strlen($nroCedula)>0){
            $sql = "
                   SELECT DISTINCT 
                        t.id AS titulo_id,
                        t.ntitulo AS serial,
                        t.cdea AS cod_plantel,
                        t.femision AS fecha_otorgamiento,
                        t.anoagreso AS anio_egreso,
                        cr.nombre AS credencial,
                        m.nombre AS nombre_mencion,
                        t.cplan AS cod_plan,
                        pl.nombre AS nombre_plan,
                        e.id AS estudiante_id,
                        e.tdocumento_identidad AS origen_estudiante,
                        e.documento_identidad AS cedula_estudiante,
                        t.en_revision,
                        1 AS legacy
                      FROM legacy.titulos t
                      LEFT JOIN gplantel.plantel p ON (p.cod_estadistico = t.cestadistico)
                     INNER JOIN gplantel.plan pl ON (pl.cod_plan = t.cplan)
                      LEFT JOIN gplantel.credencial cr ON pl.credencial_id = cr.id
                      LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
                     INNER JOIN matricula.estudiante e ON (e.documento_identidad=t.calumno::CHARACTER VARYING)
                      LEFT JOIN estado es ON ( es.id=e.estado_nac_id)
                      LEFT JOIN municipio mu ON (mu.id=e.municipio_nac_id)
                      LEFT JOIN estado ese ON ( p.estado_id=ese.id)
                   WHERE calumno = :nroCedula and e.tdocumento_identidad = :origenCedula
                UNION
                   SELECT DISTINCT 
                       t.id AS titulo_id,
                       pm.prefijo||' '||pm.serial AS serial,
                       p.cod_plantel,
                       t.fecha_otorgamiento,
                       t.anio_egreso,
                       cr.nombre AS credencial,
                       me.nombre AS nombre_mencion,
                       pl.cod_plan AS cod_plan,
                       pl.nombre AS nombre_plan,
                       e.id AS estudiante_id,
                       e.tdocumento_identidad AS origen_estudiante,
                       e.documento_identidad AS cedula_estudiante,
                       0 AS en_revision,
                       0 AS legacy
                     FROM matricula.estudiante e
                    INNER JOIN titulo.titulo t ON (t.estudiante_id = e.id)
                    INNER JOIN gplantel.plantel p ON (p.id = e.plantel_actual_id)
                     LEFT JOIN estado es ON ( es.id=e.estado_nac_id)
                     LEFT JOIN estado ese ON ( p.estado_id=ese.id)
                    INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id)
                    INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
                     LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
                     LEFT JOIN municipio mu ON (mu.id=e.municipio_nac_id)
                    INNER JOIN gplantel.zona_educativa ze ON ze.id = pm.zona_educativa_id
                     LEFT JOIN gplantel.credencial cr ON pl.credencial_id = cr.id
                     LEFT JOIN gplantel.mencion me ON (me.id=pl.mencion_id)
                    INNER JOIN seguridad.usergroups_user u ON (u.group_id = 45) -- Funcionarios designados
                    INNER JOIN estado est ON (est.id = p.estado_id AND est.id = u.estado_id)
                    INNER JOIN gplantel.autoridad_plantel au ON (t.plantel_id = au.plantel_id and au.cargo_id= 3) -- Funcionarios designados
                    INNER JOIN seguridad.usergroups_user us ON (au.usuario_id = us.id)
                     LEFT JOIN gplantel.autoridad_plantel aut ON (t.plantel_id = aut.plantel_id and aut.cargo_id= 20) -- Funcionarios designados
                     LEFT JOIN seguridad.usergroups_user use ON (aut.usuario_id = use.id)
                 WHERE e.id IS NOT NULL AND e.id = :idEstudiante AND t.estatus_actual_id = 2 -- t.estatus_actual_id = 2 -> Titulo Entregado
                    ";
            //echo "<pre>$sql (id:$id, ci:$nroCedula, o:$origenCedula)</pre>";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(":idEstudiante", $id, PDO::PARAM_INT);
            $command->bindParam(":nroCedula", $nroCedula, PDO::PARAM_INT);
            $command->bindParam(":origenCedula", $origenCedula, PDO::PARAM_STR);
            $result = $command->queryAll();
        }
        return $result;
    }
    
     /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ColaboradorPlantel the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
}
