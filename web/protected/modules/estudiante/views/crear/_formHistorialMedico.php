<?php
/**
 * Created by PhpStorm.
 * User: ignacio
 * Date: 11/09/14
 * Time: 9:09
 */
//if(isset($existeCongreso) AND $existeCongreso == array()){
?>


<div class="infoDialogBox">
    <p>
        En el momento del registro de la enfermeda y las vacunas debera introducir primero la fecha y luego la enfermedad o vacuna.
    </p>
</div>

<div class="widget-box" id="historial-medico">
    <div class="widget-header">
        <h5>Historial de Enfermedades</h5>
        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
                <div class="row">

                    <div class="widget-box" id="historia_enfermedad_box" >
                        <div class="widget-body">
                            <div class="widget-body-inner" style="display: block;">
                                <div class="widget-main">
                                    <div class="row">
                                        <div class="form">
                                            <form id="historial-enfermedad-form">
                                                <?php
                                                $this->widget('ext.yii-playground.widgets.tabularinput.XTabularInput', array(
                                                    'models' => $modelEnfermedadEstudiante,
                                                    /* 'containerTagName'=>'div',
                                                      'headerTagName'=>'div',
                                                      'header'=>'
                                                      <div class="row">
                                                      <div class="col-md-3">'.CHtml::activeLabelEX($congreso_docente,'tdocumento_identidad').'</div>
                                                      <div class="col-md-3">'.CHtml::activeLabelEX($congreso_docente,'documento_identidad').'</div>
                                                      <div class="col-md-3">'.CHtml::activeLabelEX($congreso_docente,'nombres').'</div>
                                                      <div class="col-md-3"></div>
                                                      </div>
                                                      ',
                                                      'inputContainerTagName'=>'div ',
                                                      'inputTagName'=>'div class="row"', */
                                                    //'inputTagName'=>'div class="row"',
                                                    'inputView' => 'tabular/_tabularInput',
                                                    'inputUrl' => $this->createUrl('Crear/addTabularInputs'),
                        'removeTemplate' => '<div class="col-md-2">{link}</div></div>',
                                                    'removeLabel' => 'Eliminar enfermedad',
    'removeHtmlOptions' => array('class' => 'btn btn-sm btn-danger', 'style' => 'margin-top:20.5px'),
                                                    'addLabel' => 'Agregar enfermedad',
    'addTemplate' => '<div class="row">{link}</div>',
                                                    'addHtmlOptions' => array('class' => 'btn btn-sm btn-success full-width'),

                                                ));
                                                ?>
                                                <button id="enfermedadHistorial" type="submit" data-last="Finish" title="Pasar al siguiente paso del estudiante" class="btn btn-primary btn-next pull-right enfer_idX">
                                                    Actualizar cambios
                        <i class="fa fa-mail-forward"></i>
                    </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    <!--                    <div class="row"><div class="space-6"></div></div>
                    
                    <div class="widget-box" id="congreso_datos_box" >
                        <div class="widget-header">
                            <h5>Datos Generales</h5>
                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-body-inner" style="display: block;">
                                <div class="widget-main">
                                    <div class="row">
                                        <div class="form">
                                            
                                                            </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
                        </div>-->


</div>
<hr>
<!--<div class="row">
    <div class="col-md-6 wizard-actions pull-right">
        <button type="submit" data-last="Finish" id="btnGuardarCongreso" class="btn btn-primary btn-next">
            Guardar
            <i class="icon-save icon-on-right"></i>
        </button>
    </div>

</div>-->

<div id="dialog_error" class="hide">
</div>
<div class="widget-box" id="historial-vacuna">
    <div class="widget-header">
        <h5>Historial de vacuna</h5>
        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="row">

        <div class="widget-box" id="historia_medico_vacuna_box" >
            <div class="widget-body">
                <div class="widget-body-inner" style="display: block;">
                    <div class="widget-main">
                        <div class="row">
                            <div class="form">
                                <form id="historial-vacuna-form">
                                    <?php
                                    $this->widget('ext.yii-playground.widgets.tabularinput.XTabularInput', array(
                                        'models' => $modelVacunaEstudiante,
    /* 'containerTagName'=>'div',
                                          'headerTagName'=>'div',
                                          'header'=>'
                                          <div class="row">
                                          <div class="col-md-3">'.CHtml::activeLabelEX($congreso_docente,'tdocumento_identidad').'</div>
                                          <div class="col-md-3">'.CHtml::activeLabelEX($congreso_docente,'documento_identidad').'</div>
                                          <div class="col-md-3">'.CHtml::activeLabelEX($congreso_docente,'nombres').'</div>
                                          <div class="col-md-3"></div>
                                          </div>
                                          ',
                                          'inputContainerTagName'=>'div ',
                                          'inputTagName'=>'div class="row"', */
                                        //'inputTagName'=>'div class="row"',
                                        'inputView' => 'tabular/_tabularInputVacuna',
    'inputUrl' => $this->createUrl('Crear/addTabularInputsVacuna'),
    'removeTemplate' => '<div class="col-md-2">{link}</div></div>',
                                        'removeLabel' => 'Eliminar Vacuna',
    'removeHtmlOptions' => array('class' => 'btn btn-sm btn-danger', 'style' => 'margin-top:20.5px'),
                                        'addLabel' => 'Agregar Vacuna',
    'addTemplate' => '<div class="row">{link}</div>',
                                        'addHtmlOptions' => array('class' => 'btn btn-sm btn-success full-width'),
                                    ));
                                    ?>
                    <button type="submit" data-last="Finish" title="Pasar al siguiente paso del estudiante" class="btn btn-primary btn-next pull-right">
                        Actualizar cambios
                        <i class="fa fa-mail-forward"></i>
                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--                    <div class="row"><div class="space-6"></div></div>

    <div class="widget-box" id="congreso_datos_box" >
        <div class="widget-header">
            <h5>Datos Generales</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main">
                    <div class="row">
                        <div class="form">

                                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>
        </div>-->


</div>
<?php
// }
//
//else {
?>
<!--    <div class="row">
        <div class="infoDialogBox">
            <p>
                Estimado usuario, ya ha registrado los datos generales del Congreso Pedagógico. Proximamente podrá consultar dicha información.
            </p>
        </div>
    </div>-->
<?php //}?>




