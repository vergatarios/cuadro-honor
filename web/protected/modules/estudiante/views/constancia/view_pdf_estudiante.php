<?php $resultado = Constancia::model()->buscarestudiante($cedula,$nacionalidad); ?>
<div style="margin-left: 80px;">
    <p align="center"> REPUBLICA BOLIVARIANA DE VENEZUELA </p>
    <p align="center"> MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN </p>
    <p align="center"> <?php echo $resultado['nombre_plantel']; ?> </p>
    <p align="center"> <?php echo $resultado['estado'] . ' -ESTADO' . ' . ' . $resultado['capital']; ?> </p>
</div>
<div align="center">
    <p align="center" style="font-weight:bold;"> Constancia de Estudio </p>
</div>
<div>
    <p>Quién suscribe, Dirección de Control de Estudios de la <?php echo $resultado['nombre_plantel']; ?>,
        por medio de la presente hace constar que el(la)
        Ciudadano(a) <?php echo $resultado['apellido_estudiante'] . ' ' . $resultado['nombre_estudiante'] ?>, titular de la Cédula de Identidad
        Número <?php echo $resultado['documento_identidad']; ?> , es alumno(a) regular de <?php echo $resultado['nivel']; ?>
    </p>
</div>
<?php
$fecha = date('Y-m-d');
$mes = explode("-", $fecha);
$limite = 3;
$total = $mes[1] + $limite;
?>
<?php $fecha_nacimiento_e=$fecha_nacimiento;
$valor=  explode('-', $fecha_nacimiento_e);
?>
<br>
<br>
<br>
<div>
    <p> Constancia que se expide, a petición de parte interesada en la ciudad de <?php echo  $resultado['estado']; ?>,
        el día <?php echo $mes[2] . '-' . $total . '-' . $mes[0] ?>
    </p>
</div>

<div>
<?php $imagen = Yii::app()->basePath . '/../' . '/public/downloads/constancia/' . $cedula . '.png';?>
     <img style="width: 20%; margin-left: 500px;" src="<?php echo $imagen ?>">
</div>
<br>
<div align="center">
    Url del QR:
</div>
<div>
    <a href="<?php echo $url;?>"> <?php echo $url;?> </a>
</div>