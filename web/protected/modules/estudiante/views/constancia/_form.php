
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'constancia-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <div class="widget-box">
        <div class="widget-header">
            <h5>B&uacute;squeda de Constancia</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main">
                    <div id="validaciones">
              <div id="mensajesphp">
                  <?php
                    if (isset($error)):
                        if($error == 1):
                        $this->renderPartial('//msgBox', array('class' => 'alertDialogBox', 'message' => 'Pasaporte o Cédula no Existe'));
                        endif;
                    endif;
                    ?>
                        </div>
                    </div>
                    <a href="#" class="search-button"></a>
                    <div style="display:block" class="search-form">
                        <div class="widget-main form">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-12" for="grupo" style="font-weight: bold">Nacionalidad <span class="required">*</span> </label>
                                    <?php echo Chtml::dropDownList('nacionalidad', '', array('V' => 'V', 'E' => 'E'), array('style' => 'width:20%;')); ?>
                                </div>
                                <div>
                                    <div class="col-md-6">
                                        <label class="col-md-12" for="grupo" style="font-weight: bold">Tipo de Documento <span class="required">*</span> </label>
                                        <?php
                                        echo CHtml::radioButtonList('comprobar_documento', '', array('cedula' => 'Cédula', 'pasaporte' => 'Pasaporte'), array(
                                            'labelOptions' => array('style' => 'display:inline', 'id' => 'comprobar'), // add this code
                                            'separator' => '&nbsp &nbsp &nbsp',
                                        ));
                                        ?>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="col-md-12"><label for="cedula" style="font-weight: bold">Cédula<span id="cedula_req" class="hide"> <span class="required">*</span></span> </label></div>
                                    <?php echo CHtml::textField('cedula', '', array('disabled' => 'disabled', 'id' => 'cedula')); ?>
                                </div>

                                <div class="col-md-4">
                                    <div class="col-md-12"><label for="pasaporte" style="font-weight: bold">Pasaporte<span id="pasaporte_req" class="hide"> <span class="required">*</span> </span></label></div>
                                    <?php echo CHtml::textField('pasaporte', '', array('disabled' => 'disabled', 'id' => 'pasaporte')); ?>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <?php echo CHtml::button('Buscar', array('class' => 'btn btn-info btn-xs', 'id' => 'buscar', 'style' => 'width:110px;heigth:30', 'submit' => array('constancia/buscar'))); ?>

                            </div>
                            <br>
                        </div>
                    </div>
                </div><!-- search-form -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#buscar").on("click", function() {
            var cedula = $.trim($('#cedula').val());
            var pasaporte = $.trim($('#pasaporte').val());
            if (cedula==="" && pasaporte==="") {
                $("#mensajesphp").addClass("class", "hide");
                displayDialogBox('validaciones', 'error', 'DATOS FALTANTES: Cédula o pasaporte no pueden ser vacios');
                return false;
            }else{
                return true;
            }
        });
        $("#comprobar_documento_0").click(function() {
            if ($("#comprobar_documento_0").val() === 'cedula') {
                $("#cedula").removeAttr("disabled");
                $("#pasaporte").attr("disabled", "disabled");
                $("#cedula_req").removeAttr("class", "hide");
                $("#pasaporte_req").attr("class", "hide");

            }
        });
        $("#comprobar_documento_1").click(function() {
            if ($("#comprobar_documento_1").val() === 'pasaporte') {
                $("#pasaporte").removeAttr("disabled");
                $("#cedula").attr("disabled", "disabled");
                $("#pasaporte_req").removeAttr("class", "hide");
                $("#cedula_req").attr("class", "hide");
            }
        });

        $('#cedula').bind('keyup blur', function() {
            keyNum(this, true);
        });
        $('#cedula').bind('blur', function() {
            clearField(this);
        });
        $('#pasaporte').bind('blur', function() {
            clearField(this);
        });
        });
</script>
<?php $this->endWidget(); ?>
</div><!-- form -->