<?php
/* @var $this ConstanciaController */
/* @var $model Constancia */

$this->breadcrumbs=array(
	'Constancias'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Constancia', 'url'=>array('index')),
	array('label'=>'Manage Constancia', 'url'=>array('admin')),
);
?>

<h1>Create Constancia</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>