<?php
        $this->breadcrumbs = array(
            'Constancia',
        );
        ?>

        <div class="widget-box">
            <div class="widget-header">
                <h4>Lista de Constancias</h4>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>



            <div class="widget-body">
                <div style="display:block;" class="widget-body-inner">
                    <div class="widget-main">
                        <div>
                            <div id="resultadoOperacion">
                                <div class="infoDialogBox">
                                    <p>
                                        En este módulo se observaran todas las constancias que se generan.
                                    </p>
                                </div>
                            </div>
                            <?php
                            // if (($groupId == 1) || ($groupId == 18)) {
                            ?>

                            <div class="pull-right" style="padding-left:10px;">
                                <a  type="submit" id='apertura_ticket' data-last="Finish" class="btn btn-success btn-next btn-sm">
                                    <i class="fa fa-plus icon-on-right"></i>
                                    Crear Nueva Constancia
                                </a>
                            </div>
                        </div><!-- search-form -->
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                            'id' => 'clase-plantel-grid',
                            'filter' => $model,
                            'dataProvider' => $model->search(),
                            'summaryText' => 'Mostrando {start}-{end} de {count}',
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                            'afterAjaxUpdate' => "

                                    function(){

                                        $('#date-picker').datepicker();
                                        $.datepicker.setDefaults($.datepicker.regional = {
                                                dateFormat: 'dd-mm-yy',
                                                showOn:'focus',
                                                showOtherMonths: false,
                                                selectOtherMonths: true,
                                                changeMonth: true,
                                                changeYear: true,
                                                minDate: new Date(1800, 1, 1),
                                                maxDate: 'today'
                                            });

                                        $('#Ticket_codigo').bind('keyup blur', function () {
                                            keyNum(this, false);
                                            clearField(this);
                                        });


                                        $('#Ticket_observacion').bind('keyup blur', function () {
                                            keyText(this, true);
                                        });

                                        $('#Ticket_observacion').bind('blur', function () {
                                            clearField(this);
                                        });
                                    }
                                ",
                            'columns' => array(
                                array(
                                    'header' => '<center> Nombre del Archivo </center>',
                                    'name' => 'nombre_archivo',
                                ),
                                array(
                                    'header' => '<center> Url </center>',
                                    'name' => 'url',
                                ),
                                array(
                                    'header' => '<center>Fecha</center>',
                                    'name' => 'codigo_qr',
                                ),
                                array(
                                    'header' => '<center> Estatus </center>',
                                    'name' => 'estatus',
                                    'filter' => array('A' => 'Activo', 'E' => 'Eliminado'),
                                ),
                                array(
                                    'type' => 'raw',
                                    'header' => '<center>Acción</center>',
                                    'value' => array($this, 'columnaAcciones'),
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                ),
                            ),
                            'emptyText' => 'No se han encontrado Registros',
                        ));
                        ?>
                        <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<?php ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('.search-form form').submit(function(){
            $('#tipo-fundamento-grid').yiiGridView('update', {
                    data: $(this).serialize()
            });
            return false;
    });
    ");
        ?>


<script type="text/javascript">
 $(document).click(function() {
 $("#comprobar_documento_0").click(function () {
if($("#comprobar_documento_0").val()=='cedula'){
$("#cedula").removeAttr("disabled");
$("#pasaporte").attr("disabled","disabled");
$("#cedula_req").removeAttr("class","hide");
$("#pasaporte_req").attr("class","hide");
}
});
$("#comprobar_documento_1").click(function () {
if($("#comprobar_documento_1").val()=='pasaporte'){
  $("#pasaporte").removeAttr("disabled");
  $("#cedula").attr("disabled","disabled");
  $("#pasaporte_req").removeAttr("class","hide");
  $("#cedula_req").attr("class","hide");
}
});

$('#cedula').bind('keyup blur', function() {
   keyNum(this, true);
});
    $('#cedula').bind('blur', function() {
            clearField(this);
        });
        $('#pasaporte').bind('blur', function() {
            clearField(this);
        });
    });
</script>

