<?php
/* @var $this EstudianteController */
/* @var $data Estudiante */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_escolar')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_escolar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_identidad')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_identidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombres')); ?>:</b>
	<?php echo CHtml::encode($data->nombres); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellidos')); ?>:</b>
	<?php echo CHtml::encode($data->apellidos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_nacimiento')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_nacimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('correo')); ?>:</b>
	<?php echo CHtml::encode($data->correo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_movil')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_movil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_habitacion')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_habitacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lateralidad_mano')); ?>:</b>
	<?php echo CHtml::encode($data->lateralidad_mano); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('identificacion_extranjera')); ?>:</b>
	<?php echo CHtml::encode($data->identificacion_extranjera); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ciudad_nacimiento')); ?>:</b>
	<?php echo CHtml::encode($data->ciudad_nacimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pais_id')); ?>:</b>
	<?php echo CHtml::encode($data->pais_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('etnia_id')); ?>:</b>
	<?php echo CHtml::encode($data->etnia_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_civil_id')); ?>:</b>
	<?php echo CHtml::encode($data->estado_civil_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('diversidad_funcional_id')); ?>:</b>
	<?php echo CHtml::encode($data->diversidad_funcional_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('genero_id')); ?>:</b>
	<?php echo CHtml::encode($data->genero_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('condicion_vivienda_id')); ?>:</b>
	<?php echo CHtml::encode($data->condicion_vivienda_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zona_ubicacion_id')); ?>:</b>
	<?php echo CHtml::encode($data->zona_ubicacion_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_vivienda_id')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_vivienda_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ubicacion_vivienda_id')); ?>:</b>
	<?php echo CHtml::encode($data->ubicacion_vivienda_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ingreso_familiar')); ?>:</b>
	<?php echo CHtml::encode($data->ingreso_familiar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('condicion_infraestructura_id')); ?>:</b>
	<?php echo CHtml::encode($data->condicion_infraestructura_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('beca')); ?>:</b>
	<?php echo CHtml::encode($data->beca); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('canaima')); ?>:</b>
	<?php echo CHtml::encode($data->canaima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('serial_canaima')); ?>:</b>
	<?php echo CHtml::encode($data->serial_canaima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nacionalidad')); ?>:</b>
	<?php echo CHtml::encode($data->nacionalidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_nac_id')); ?>:</b>
	<?php echo CHtml::encode($data->estado_nac_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('municipio_nac_id')); ?>:</b>
	<?php echo CHtml::encode($data->municipio_nac_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parroquia_nac_id')); ?>:</b>
	<?php echo CHtml::encode($data->parroquia_nac_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion_nac')); ?>:</b>
	<?php echo CHtml::encode($data->direccion_nac); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_id')); ?>:</b>
	<?php echo CHtml::encode($data->estado_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('municipio_id')); ?>:</b>
	<?php echo CHtml::encode($data->municipio_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parroquia_id')); ?>:</b>
	<?php echo CHtml::encode($data->parroquia_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion_dom')); ?>:</b>
	<?php echo CHtml::encode($data->direccion_dom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('poblacion_id')); ?>:</b>
	<?php echo CHtml::encode($data->poblacion_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('urbanizacion_id')); ?>:</b>
	<?php echo CHtml::encode($data->urbanizacion_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_via_id')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_via_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('via')); ?>:</b>
	<?php echo CHtml::encode($data->via); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('representante_id')); ?>:</b>
	<?php echo CHtml::encode($data->representante_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion_afinidad')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion_afinidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('otro_represent_id')); ?>:</b>
	<?php echo CHtml::encode($data->otro_represent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_ini_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_ini_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ini')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ini); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_act_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_act_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_act')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_act); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_elim')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_elim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?>:</b>
	<?php echo CHtml::encode($data->estatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('carnet_diplomatico')); ?>:</b>
	<?php echo CHtml::encode($data->carnet_diplomatico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plantel_actual_id')); ?>:</b>
	<?php echo CHtml::encode($data->plantel_actual_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plantel_anterior_id')); ?>:</b>
	<?php echo CHtml::encode($data->plantel_anterior_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estatus_id')); ?>:</b>
	<?php echo CHtml::encode($data->estatus_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sigue_id')); ?>:</b>
	<?php echo CHtml::encode($data->sigue_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_nuevo_id')); ?>:</b>
	<?php echo CHtml::encode($data->estado_nuevo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_nac_nuevo_id')); ?>:</b>
	<?php echo CHtml::encode($data->estado_nac_nuevo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('municipio_nuevo_id')); ?>:</b>
	<?php echo CHtml::encode($data->municipio_nuevo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('municipio_nac_nuevo_id')); ?>:</b>
	<?php echo CHtml::encode($data->municipio_nac_nuevo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('afinidad_id')); ?>:</b>
	<?php echo CHtml::encode($data->afinidad_id); ?>
	<br />

	*/ ?>

</div>