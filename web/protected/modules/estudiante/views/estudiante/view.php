<?php
/* @var $this EstudianteController */
/* @var $model Estudiante */

$this->breadcrumbs=array(
	'Estudiantes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Estudiante', 'url'=>array('index')),
	array('label'=>'Create Estudiante', 'url'=>array('create')),
	array('label'=>'Update Estudiante', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Estudiante', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Estudiante', 'url'=>array('admin')),
);
?>

<h1>View Estudiante #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cedula_escolar',
		'cedula_identidad',
		'nombres',
		'apellidos',
		'fecha_nacimiento',
		'correo',
		'telefono_movil',
		'telefono_habitacion',
		'lateralidad_mano',
		'identificacion_extranjera',
		'ciudad_nacimiento',
		'pais_id',
		'etnia_id',
		'estado_civil_id',
		'diversidad_funcional_id',
		'genero_id',
		'condicion_vivienda_id',
		'zona_ubicacion_id',
		'tipo_vivienda_id',
		'ubicacion_vivienda_id',
		'ingreso_familiar',
		'condicion_infraestructura_id',
		'beca',
		'canaima',
		'serial_canaima',
		'nacionalidad',
		'estado_nac_id',
		'municipio_nac_id',
		'parroquia_nac_id',
		'direccion_nac',
		'estado_id',
		'municipio_id',
		'parroquia_id',
		'direccion_dom',
		'poblacion_id',
		'urbanizacion_id',
		'tipo_via_id',
		'via',
		'representante_id',
		'descripcion_afinidad',
		'otro_represent_id',
		'usuario_ini_id',
		'fecha_ini',
		'usuario_act_id',
		'fecha_act',
		'fecha_elim',
		'estatus',
		'carnet_diplomatico',
		'plantel_actual_id',
		'plantel_anterior_id',
		'estatus_id',
		'sigue_id',
		'estado_nuevo_id',
		'estado_nac_nuevo_id',
		'municipio_nuevo_id',
		'municipio_nac_nuevo_id',
		'afinidad_id',
	),
)); ?>
