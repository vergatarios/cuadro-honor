<?php
/* @var $this EstudianteController */
/* @var $model Estudiante */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'estudiante-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cedula_escolar'); ?>
		<?php echo $form->textField($model,'cedula_escolar'); ?>
		<?php echo $form->error($model,'cedula_escolar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cedula_identidad'); ?>
		<?php echo $form->textField($model,'cedula_identidad'); ?>
		<?php echo $form->error($model,'cedula_identidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombres'); ?>
		<?php echo $form->textField($model,'nombres',array('size'=>60,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'nombres'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellidos'); ?>
		<?php echo $form->textField($model,'apellidos',array('size'=>60,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'apellidos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
		<?php echo $form->textField($model,'fecha_nacimiento'); ?>
		<?php echo $form->error($model,'fecha_nacimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'correo'); ?>
		<?php echo $form->textField($model,'correo',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'correo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_movil'); ?>
		<?php echo $form->textField($model,'telefono_movil',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'telefono_movil'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_habitacion'); ?>
		<?php echo $form->textField($model,'telefono_habitacion',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'telefono_habitacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lateralidad_mano'); ?>
		<?php echo $form->textField($model,'lateralidad_mano',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'lateralidad_mano'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'identificacion_extranjera'); ?>
		<?php echo $form->textField($model,'identificacion_extranjera',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'identificacion_extranjera'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ciudad_nacimiento'); ?>
		<?php echo $form->textField($model,'ciudad_nacimiento',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ciudad_nacimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pais_id'); ?>
		<?php echo $form->textField($model,'pais_id'); ?>
		<?php echo $form->error($model,'pais_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'etnia_id'); ?>
		<?php echo $form->textField($model,'etnia_id'); ?>
		<?php echo $form->error($model,'etnia_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado_civil_id'); ?>
		<?php echo $form->textField($model,'estado_civil_id'); ?>
		<?php echo $form->error($model,'estado_civil_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'diversidad_funcional_id'); ?>
		<?php echo $form->textField($model,'diversidad_funcional_id'); ?>
		<?php echo $form->error($model,'diversidad_funcional_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'genero_id'); ?>
		<?php echo $form->textField($model,'genero_id'); ?>
		<?php echo $form->error($model,'genero_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'condicion_vivienda_id'); ?>
		<?php echo $form->textField($model,'condicion_vivienda_id'); ?>
		<?php echo $form->error($model,'condicion_vivienda_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zona_ubicacion_id'); ?>
		<?php echo $form->textField($model,'zona_ubicacion_id'); ?>
		<?php echo $form->error($model,'zona_ubicacion_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_vivienda_id'); ?>
		<?php echo $form->textField($model,'tipo_vivienda_id'); ?>
		<?php echo $form->error($model,'tipo_vivienda_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ubicacion_vivienda_id'); ?>
		<?php echo $form->textField($model,'ubicacion_vivienda_id'); ?>
		<?php echo $form->error($model,'ubicacion_vivienda_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ingreso_familiar'); ?>
		<?php echo $form->textField($model,'ingreso_familiar'); ?>
		<?php echo $form->error($model,'ingreso_familiar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'condicion_infraestructura_id'); ?>
		<?php echo $form->textField($model,'condicion_infraestructura_id'); ?>
		<?php echo $form->error($model,'condicion_infraestructura_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'beca'); ?>
		<?php echo $form->textField($model,'beca'); ?>
		<?php echo $form->error($model,'beca'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'canaima'); ?>
		<?php echo $form->textField($model,'canaima'); ?>
		<?php echo $form->error($model,'canaima'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'serial_canaima'); ?>
		<?php echo $form->textField($model,'serial_canaima'); ?>
		<?php echo $form->error($model,'serial_canaima'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nacionalidad'); ?>
		<?php echo $form->textField($model,'nacionalidad',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'nacionalidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado_nac_id'); ?>
		<?php echo $form->textField($model,'estado_nac_id'); ?>
		<?php echo $form->error($model,'estado_nac_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'municipio_nac_id'); ?>
		<?php echo $form->textField($model,'municipio_nac_id'); ?>
		<?php echo $form->error($model,'municipio_nac_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parroquia_nac_id'); ?>
		<?php echo $form->textField($model,'parroquia_nac_id'); ?>
		<?php echo $form->error($model,'parroquia_nac_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion_nac'); ?>
		<?php echo $form->textField($model,'direccion_nac',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'direccion_nac'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado_id'); ?>
		<?php echo $form->textField($model,'estado_id'); ?>
		<?php echo $form->error($model,'estado_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'municipio_id'); ?>
		<?php echo $form->textField($model,'municipio_id'); ?>
		<?php echo $form->error($model,'municipio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parroquia_id'); ?>
		<?php echo $form->textField($model,'parroquia_id'); ?>
		<?php echo $form->error($model,'parroquia_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion_dom'); ?>
		<?php echo $form->textArea($model,'direccion_dom',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'direccion_dom'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'poblacion_id'); ?>
		<?php echo $form->textField($model,'poblacion_id'); ?>
		<?php echo $form->error($model,'poblacion_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'urbanizacion_id'); ?>
		<?php echo $form->textField($model,'urbanizacion_id'); ?>
		<?php echo $form->error($model,'urbanizacion_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_via_id'); ?>
		<?php echo $form->textField($model,'tipo_via_id'); ?>
		<?php echo $form->error($model,'tipo_via_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'via'); ?>
		<?php echo $form->textArea($model,'via',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'via'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'representante_id'); ?>
		<?php echo $form->textField($model,'representante_id'); ?>
		<?php echo $form->error($model,'representante_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion_afinidad'); ?>
		<?php echo $form->textField($model,'descripcion_afinidad',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'descripcion_afinidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'otro_represent_id'); ?>
		<?php echo $form->textField($model,'otro_represent_id'); ?>
		<?php echo $form->error($model,'otro_represent_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_ini_id'); ?>
		<?php echo $form->textField($model,'usuario_ini_id'); ?>
		<?php echo $form->error($model,'usuario_ini_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_ini'); ?>
		<?php echo $form->textField($model,'fecha_ini',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'fecha_ini'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_act_id'); ?>
		<?php echo $form->textField($model,'usuario_act_id'); ?>
		<?php echo $form->error($model,'usuario_act_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_act'); ?>
		<?php echo $form->textField($model,'fecha_act',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'fecha_act'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_elim'); ?>
		<?php echo $form->textField($model,'fecha_elim',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'fecha_elim'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estatus'); ?>
		<?php echo $form->textField($model,'estatus',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'estatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'carnet_diplomatico'); ?>
		<?php echo $form->textField($model,'carnet_diplomatico',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'carnet_diplomatico'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plantel_actual_id'); ?>
		<?php echo $form->textField($model,'plantel_actual_id'); ?>
		<?php echo $form->error($model,'plantel_actual_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plantel_anterior_id'); ?>
		<?php echo $form->textField($model,'plantel_anterior_id'); ?>
		<?php echo $form->error($model,'plantel_anterior_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estatus_id'); ?>
		<?php echo $form->textField($model,'estatus_id'); ?>
		<?php echo $form->error($model,'estatus_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sigue_id'); ?>
		<?php echo $form->textField($model,'sigue_id'); ?>
		<?php echo $form->error($model,'sigue_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado_nuevo_id'); ?>
		<?php echo $form->textField($model,'estado_nuevo_id'); ?>
		<?php echo $form->error($model,'estado_nuevo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado_nac_nuevo_id'); ?>
		<?php echo $form->textField($model,'estado_nac_nuevo_id'); ?>
		<?php echo $form->error($model,'estado_nac_nuevo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'municipio_nuevo_id'); ?>
		<?php echo $form->textField($model,'municipio_nuevo_id'); ?>
		<?php echo $form->error($model,'municipio_nuevo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'municipio_nac_nuevo_id'); ?>
		<?php echo $form->textField($model,'municipio_nac_nuevo_id'); ?>
		<?php echo $form->error($model,'municipio_nac_nuevo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'afinidad_id'); ?>
		<?php echo $form->textField($model,'afinidad_id'); ?>
		<?php echo $form->error($model,'afinidad_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


