<?php
/* @var $this EstudianteController */
/* @var $model Estudiante */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cedula_escolar'); ?>
		<?php echo $form->textField($model,'cedula_escolar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cedula_identidad'); ?>
		<?php echo $form->textField($model,'cedula_identidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombres'); ?>
		<?php echo $form->textField($model,'nombres',array('size'=>60,'maxlength'=>120)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apellidos'); ?>
		<?php echo $form->textField($model,'apellidos',array('size'=>60,'maxlength'=>120)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_nacimiento'); ?>
		<?php echo $form->textField($model,'fecha_nacimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'correo'); ?>
		<?php echo $form->textField($model,'correo',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telefono_movil'); ?>
		<?php echo $form->textField($model,'telefono_movil',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telefono_habitacion'); ?>
		<?php echo $form->textField($model,'telefono_habitacion',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lateralidad_mano'); ?>
		<?php echo $form->textField($model,'lateralidad_mano',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'identificacion_extranjera'); ?>
		<?php echo $form->textField($model,'identificacion_extranjera',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ciudad_nacimiento'); ?>
		<?php echo $form->textField($model,'ciudad_nacimiento',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pais_id'); ?>
		<?php echo $form->textField($model,'pais_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'etnia_id'); ?>
		<?php echo $form->textField($model,'etnia_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado_civil_id'); ?>
		<?php echo $form->textField($model,'estado_civil_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'diversidad_funcional_id'); ?>
		<?php echo $form->textField($model,'diversidad_funcional_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'genero_id'); ?>
		<?php echo $form->textField($model,'genero_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'condicion_vivienda_id'); ?>
		<?php echo $form->textField($model,'condicion_vivienda_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'zona_ubicacion_id'); ?>
		<?php echo $form->textField($model,'zona_ubicacion_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_vivienda_id'); ?>
		<?php echo $form->textField($model,'tipo_vivienda_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ubicacion_vivienda_id'); ?>
		<?php echo $form->textField($model,'ubicacion_vivienda_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ingreso_familiar'); ?>
		<?php echo $form->textField($model,'ingreso_familiar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'condicion_infraestructura_id'); ?>
		<?php echo $form->textField($model,'condicion_infraestructura_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'beca'); ?>
		<?php echo $form->textField($model,'beca'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'canaima'); ?>
		<?php echo $form->textField($model,'canaima'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'serial_canaima'); ?>
		<?php echo $form->textField($model,'serial_canaima'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nacionalidad'); ?>
		<?php echo $form->textField($model,'nacionalidad',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado_nac_id'); ?>
		<?php echo $form->textField($model,'estado_nac_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'municipio_nac_id'); ?>
		<?php echo $form->textField($model,'municipio_nac_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parroquia_nac_id'); ?>
		<?php echo $form->textField($model,'parroquia_nac_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'direccion_nac'); ?>
		<?php echo $form->textField($model,'direccion_nac',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado_id'); ?>
		<?php echo $form->textField($model,'estado_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'municipio_id'); ?>
		<?php echo $form->textField($model,'municipio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parroquia_id'); ?>
		<?php echo $form->textField($model,'parroquia_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'direccion_dom'); ?>
		<?php echo $form->textArea($model,'direccion_dom',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'poblacion_id'); ?>
		<?php echo $form->textField($model,'poblacion_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'urbanizacion_id'); ?>
		<?php echo $form->textField($model,'urbanizacion_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_via_id'); ?>
		<?php echo $form->textField($model,'tipo_via_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'via'); ?>
		<?php echo $form->textArea($model,'via',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'representante_id'); ?>
		<?php echo $form->textField($model,'representante_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcion_afinidad'); ?>
		<?php echo $form->textField($model,'descripcion_afinidad',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'otro_represent_id'); ?>
		<?php echo $form->textField($model,'otro_represent_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario_ini_id'); ?>
		<?php echo $form->textField($model,'usuario_ini_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_ini'); ?>
		<?php echo $form->textField($model,'fecha_ini',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario_act_id'); ?>
		<?php echo $form->textField($model,'usuario_act_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_act'); ?>
		<?php echo $form->textField($model,'fecha_act',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_elim'); ?>
		<?php echo $form->textField($model,'fecha_elim',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estatus'); ?>
		<?php echo $form->textField($model,'estatus',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'carnet_diplomatico'); ?>
		<?php echo $form->textField($model,'carnet_diplomatico',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'plantel_actual_id'); ?>
		<?php echo $form->textField($model,'plantel_actual_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'plantel_anterior_id'); ?>
		<?php echo $form->textField($model,'plantel_anterior_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estatus_id'); ?>
		<?php echo $form->textField($model,'estatus_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sigue_id'); ?>
		<?php echo $form->textField($model,'sigue_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'afinidad_id'); ?>
		<?php echo $form->textField($model,'afinidad_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->