<?php
/* @var $this EstudianteController */
/* @var $model Estudiante */

$this->breadcrumbs=array(
	'Estudiantes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Estudiante', 'url'=>array('index')),
	array('label'=>'Create Estudiante', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#estudiante-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Estudiantes</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'estudiante-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'cedula_escolar',
		'cedula_identidad',
		'nombres',
		'apellidos',
		'fecha_nacimiento',
		/*
		'correo',
		'telefono_movil',
		'telefono_habitacion',
		'lateralidad_mano',
		'identificacion_extranjera',
		'ciudad_nacimiento',
		'pais_id',
		'etnia_id',
		'estado_civil_id',
		'diversidad_funcional_id',
		'genero_id',
		'condicion_vivienda_id',
		'zona_ubicacion_id',
		'tipo_vivienda_id',
		'ubicacion_vivienda_id',
		'ingreso_familiar',
		'condicion_infraestructura_id',
		'beca',
		'canaima',
		'serial_canaima',
		'nacionalidad',
		'estado_nac_id',
		'municipio_nac_id',
		'parroquia_nac_id',
		'direccion_nac',
		'estado_id',
		'municipio_id',
		'parroquia_id',
		'direccion_dom',
		'poblacion_id',
		'urbanizacion_id',
		'tipo_via_id',
		'via',
		'representante_id',
		'descripcion_afinidad',
		'otro_represent_id',
		'usuario_ini_id',
		'fecha_ini',
		'usuario_act_id',
		'fecha_act',
		'fecha_elim',
		'estatus',
		'carnet_diplomatico',
		'plantel_actual_id',
		'plantel_anterior_id',
		'estatus_id',
		'sigue_id',
		'estado_nuevo_id',
		'estado_nac_nuevo_id',
		'municipio_nuevo_id',
		'municipio_nac_nuevo_id',
		'afinidad_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
