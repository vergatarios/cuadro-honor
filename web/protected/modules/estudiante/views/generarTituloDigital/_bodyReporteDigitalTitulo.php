<?php ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <table width="100%" style="vert-align: middle; text-align: center; font-size: 16px; font-family: Arial;">

                <tr>
                    <td style=" text-align: center; font-size: 19px;"><strong>REPÚBLICA BOLIVARIANA DE VENEZUELA</strong></td>
                </tr>

                <tr>
<!--                    <td style=" text-align: center; "><strong>MINISTERIO DE EDUCACIÓN, CULTURA Y DEPORTES</strong></td>-->
                    <td style=" text-align: center; font-size: 16px;"><strong>MINISTERIO DEL PODER POPULAR PARA LA EDUCACIÓN</strong></td>
                </tr>

                <tr>
                    <td style=" text-align: center; font-size: 15px;"><strong>Viceministerio de Participación y Apoyo Académico</strong></td>
                </tr>

                <tr>
                    <td style=" text-align: center; font-size: 15px;"><strong>Dirección General de Registro y Control Académico</strong></td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td   style=" text-align: left; "></br><strong>Zona Educativa / Plantel :</strong> <?php echo $resultadoBusqueda['plantel']; ?></td>
                </tr>

                <tr>
                    <td   style=" text-align: left; "><strong>Código :</strong> <?php echo $resultadoBusqueda['codigo_plantel']; ?></td>
                </tr>

                <tr>
                    <td   style=" text-align: left; "><strong>Título de  :</strong> <strong><?php echo $resultadoBusqueda['nombre_plan']; ?></strong></td>
                </tr>

                <tr>
                    <td  style=" text-align: left; "><strong>Plan de estudio, Código Nro: </strong> <?php echo $resultadoBusqueda['cod_plan']; ?></td>
                </tr>

                <tr>
                    <td  style=" text-align: left; "><strong>Que se otorga a: </strong> <?php echo $resultadoBusqueda['nombre_estudiante']; ?></td>
                </tr>

                <tr>
                    <td  style=" text-align: left; "><strong>Cédula de Identidad Nro.: </strong> <?php echo $resultadoBusqueda['origen_estudiante'].'-'.$resultadoBusqueda['cedula_estudiante']; ?></td>
                </tr>

                <tr>
                    <td  style=" text-align: left; "><strong>Nacido(a) en : </strong> <?php echo $resultadoBusqueda['nacido_en'] . ' el ' . $fechaNacimiento['2'].' de '.$monthName.' de '.$fechaNacimiento['0']; ?></td>
                </tr>

                <tr>
                    <td  style=" text-align: left; "><strong>  <?php echo 'Previo el cumplimiento de los requisitos exigidos por la ley'; ?></strong></td>
                </tr>

                <tr>
                    <td  style=" text-align: left; "><strong>Lugar y Fecha de expedición: </strong>
                         <?php
                            //$fechaOtorgamiento=array('0'=>'3','1'=>'01','2'=>'2006');
                            if(isset($resultadoBusqueda['fecha_otorgamiento']) && ($resultadoBusqueda['fecha_otorgamiento'])!=null){
                            if(isset ($resultadoBusqueda['lugar_expedicion']) && $resultadoBusqueda['lugar_expedicion']!=null){
                                echo $resultadoBusqueda['lugar_expedicion'].', ';
                            }
                            echo $fechaOtorgamiento[2].' de '.$mesNombreExpendicion.' de '.$fechaOtorgamiento[0];
                        }
                        ?>
                    </td>
                </tr>
        </table>
    </div>


<!--<div style="position: relative;">-->
<!--    <table width="100%" style="vert-align: middle; text-align: center; font-size: 7px; font-family: Arial;">-->
<!--        <tr>-->
<!--            <td width="10%" style="text-align: center;"><strong> Director del Plantel</strong></td>-->
<!--            <td width="10%" style="text-align: center;"><strong> Profesor Jefe de Evaluación y Acreditación o <br> Representante del Consejo General de Docentes</strong></td>-->
<!--            <td width="10%" style="text-align: center;"><strong> Funcionario designado por el <br> Ministro de Educación, Cultura y Deportes</strong></td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td></td>-->
<!--            <td></td>-->
<!--            <td></td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td   style=" "><strong> --><?php //echo "Nombre: ".$resultadoBusqueda['nombre_director_plantel']; ?><!--</strong></td>-->
<!--            <td   style=" "><strong> --><?php //echo "Nombre: ".$resultadoBusqueda['nombre_drcee_zona'];?><!--</strong></td>-->
<!--            <td   style=" "><strong> --><?php //echo "Nombre: ".$resultadoBusqueda['nombre_funcionario_designado']; ?><!--</strong></td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td   style=" "><strong> --><?php //echo "CI: ".$resultadoBusqueda['cedula_director_plantel']; ?><!--</strong></td>-->
<!---->
<!--            <td   style=" "><strong> --><?php //echo "CI: ".$resultadoBusqueda['cedula_drcee_zona']; ?><!--</strong></td>-->
<!---->
<!--            <td   style=" "><strong> --><?php //echo "CI: ".$resultadoBusqueda['cedula_funcionario_designado']; ?><!--</strong></td>-->
<!---->
<!--        </tr>-->
<!---->
<!--        <tr>-->
<!--            <td style=""><strong>Sello del Plantel</strong></td>-->
<!--            <td style=""><strong>Sello de la Autoridad <br> Educativa</strong></td>-->
<!--            <td  style=" text-align: left; "><strong>Año de Egreso : </strong> --><?php //echo $resultadoBusqueda[0]['anio_egreso'] ?><!--</td>-->
<!--        </tr>-->
<!--    </table>-->
<!---->
<!--</div>-->
