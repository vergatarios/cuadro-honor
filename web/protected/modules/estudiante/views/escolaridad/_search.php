<?php
/* @var $this EscolaridadController */
/* @var $model TalumnoAcad */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'calumno'); ?>
		<?php echo $form->textField($model,'calumno'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cestadistico'); ?>
		<?php echo $form->textField($model,'cestadistico'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cgradoano'); ?>
		<?php echo $form->textField($model,'cgradoano'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cseccion'); ?>
		<?php echo $form->textField($model,'cseccion',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cplan'); ?>
		<?php echo $form->textField($model,'cplan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cescolaridad1'); ?>
		<?php echo $form->textField($model,'cescolaridad1',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cescolaridad2'); ?>
		<?php echo $form->textField($model,'cescolaridad2',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fperiodoescolar'); ?>
		<?php echo $form->textField($model,'fperiodoescolar',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fmatricula'); ?>
		<?php echo $form->textField($model,'fmatricula'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dobservacion'); ?>
		<?php echo $form->textField($model,'dobservacion',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fegreso'); ?>
		<?php echo $form->textField($model,'fegreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lapso'); ?>
		<?php echo $form->textField($model,'lapso',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cdea'); ?>
		<?php echo $form->textField($model,'cdea',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cestado'); ?>
		<?php echo $form->textField($model,'cestado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cregimen'); ?>
		<?php echo $form->textField($model,'cregimen',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cnivel'); ?>
		<?php echo $form->textField($model,'cnivel',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cestatusegreso'); ?>
		<?php echo $form->textField($model,'cestatusegreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'observacione'); ?>
		<?php echo $form->textField($model,'observacione',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'carea_atencion'); ?>
		<?php echo $form->textField($model,'carea_atencion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_matri'); ?>
		<?php echo $form->textField($model,'tipo_matri'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'calumno2'); ?>
		<?php echo $form->textField($model,'calumno2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_cedula_escolar'); ?>
		<?php echo $form->textField($model,'s_cedula_escolar'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_cedula_identidad'); ?>
		<?php echo $form->textField($model,'s_cedula_identidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_carnet_diplomatico'); ?>
		<?php echo $form->textField($model,'s_carnet_diplomatico'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_alumno_id'); ?>
		<?php echo $form->textField($model,'s_alumno_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_plantel_id'); ?>
		<?php echo $form->textField($model,'s_plantel_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_nivel_nombre'); ?>
		<?php echo $form->textField($model,'s_nivel_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_grado_nombre'); ?>
		<?php echo $form->textField($model,'s_grado_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_periodo_nombre'); ?>
		<?php echo $form->textField($model,'s_periodo_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_modalidad_nombre'); ?>
		<?php echo $form->textField($model,'s_modalidad_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_escolaridad_nombre'); ?>
		<?php echo $form->textField($model,'s_escolaridad_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'historico'); ?>
		<?php echo $form->textField($model,'historico'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_plan_id'); ?>
		<?php echo $form->textField($model,'s_plan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gplantel_plantel_id'); ?>
		<?php echo $form->textField($model,'gplantel_plantel_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->