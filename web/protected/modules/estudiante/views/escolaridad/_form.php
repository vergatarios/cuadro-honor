<?php
/* @var $this EscolaridadController */
/* @var $model TalumnoAcad */
/* @var $form CActiveForm */
?>
<div class = "widget-box">

<div class = "widget-header" style="border-width: thin;">

    <h5>Datos del estudiante</h5>
    <div class="widget-toolbar">
        <a data-action="collapse" href="#">
            <i class="icon-chevron-up"></i>

        </a>
    </div>

</div>

<div class = "widget-body">
    <div style = "display: block;" class = "widget-body-inner">
        <div class = "widget-main">
            <div class="row">
                <div class="infoDialogBox" id="infoEstudiante">
                    <p> Por Favor Ingrese los datos correspondientes, Los campos marcados con <b><span class="required">*</span></b> son requeridos.</p>
                </div>
            </div>


            <div class="row row-fluid">
                <div class="form">

                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'talumno-acad-form',
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation' => false,
                    ));
                    ?>
                    <?php
                    if ($form->errorSummary($model)):
                        ?>
                        <div id ="div-result-message" class="errorDialogBox" >
                            <?php echo $form->errorSummary($model); ?>
                        </div>
                    <?php
                    endif;
                    ?>



                    <div id="1eraFila" class="col-md-12">
                        <div class="col-md-4" >
                            <label class="col-sm-12" for="tdocumento_identidad"> Tipo de Identificación <span class="required">*</span></label>
                            <?php
                            echo $form->dropDownList($model, 'tdocumento_identidad', array(
                                'V' => 'Venezolana',
                                'E' => 'Extranjera',
                                'P' => 'Pasaporte',
                            ), array('required' => 'required' , 'class' => 'span-7', 'id'=>'tipo_documento'));
                            ?>
                            <?php echo $form->error($model, 'tdocumento_identidad'); ?>
                        </div>

                        <div class="col-md-4" >
                            <label class="col-sm-12" for="calumno"> Documento de Identidad <span class="required">*</span></label>
                            <?php echo $form->textField($model, 'calumno',array('required' => 'required' ,'maxlength' => 10, 'placeholder' => 'Ejm: 12345678','class' => 'span-7', 'id'=>'cedula'));?>
                            <?php echo $form->error($model, 'calumno'); ?>
                        </div>

                        <div class="col-md-4" >
                            <label class="col-sm-12" for="cestadistico"> Código Estadístico <span class="required">*</span></label>
                            <?php echo $form->textField($model, 'cestadistico', array('required' => 'required' , 'maxlength' => 10,'placeholder' => 'Ejm: 54321','class' => 'span-7', 'id'=>'cod_estadistico')); ?>
                            <?php echo $form->error($model, 'cestadistico'); ?>
                        </div>

                    </div>

                    <div class = "col-md-12"><div class = "space-6"></div></div>

                    <div id="2daFila" class="col-md-12">
                        <div class="col-md-4" >
                            <label class="col-sm-12" for="cdea"> Código del Plantel <span class="required">*</span</label>
                            <?php echo $form->textField($model, 'cdea', array('maxlength' => 80,'placeholder' =>'Còdigo del Plantel','class' => 'span-7', 'id'=>'cdea')); ?>
                            <?php echo $form->error($model, 'cdea'); ?>
                        </div>
                        <div class="col-md-4" >
                            <label class="col-sm-12" for="cseccion"> Sección <span class="required">*</span></label>
                            <?php
                            echo $form->dropDownList(
                                $model, 'cseccion', CHtml::listData(Seccion::model()->findAll(array('order' => 'nombre ASC')), 'nombre', 'nombre'), array('empty' => array('' => '- SELECCIONE -'), 'required' => 'required','class' => 'span-7', 'id'=>'seccion')
                            );
                            ?>
                            <?php echo $form->error($model, 'cseccion'); ?>
                        </div>



                        <div class="col-md-4" >

                            <label class="col-sm-12" for="cplan"> Código de Plan <span class="required">*</span></label>
                            <?php echo $form->textField($model, 'cplan', array('required' => 'required' , 'maxlength' => 10,'placeholder' => 'Ejm: 54321','class' => 'span-7', 'id'=>'cod_plan'));  ?>
                            <?php echo $form->error($model, 'cplan'); ?>

                        </div>


                    </div>

                    <div class = "col-md-12"><div class = "space-6"></div></div>

                    <div id="3raFila" class = "col-md-12">
                        <div class="col-md-4" >
                            <label class="col-sm-12" for="fmatricula"> Fecha de Matrícula <span class="required">*</span></label>
                            <?php echo $form->textField($model, 'fmatricula', array('required' => 'required' ,'placeholder' => '','class' => 'span-7', 'id'=>'fecha_matricula', 'readOnly' => 'readOnly'));  ?>
                            <?php echo $form->error($model, 'fmatricula'); ?>
                        </div>

                        <div class="col-md-4" >
                            <label class="col-sm-12" for="fegreso"> Fecha de Egreso <span class="required">*</span></label>
                            <?php echo $form->textField($model, 'fegreso', array('required' => 'required' ,'placeholder' => '','class' => 'span-7', 'id'=>'fecha_egreso', 'readOnly' => 'readOnly')); ?>
                            <?php echo $form->error($model, 'fegreso'); ?>
                        </div>
                        <div class="col-md-4" >
                            <label class="col-sm-12" for="fperiodoescolar"> Periodo Escolar <span class="required">*</span></label>
                            <?php
                            echo $form->dropDownList($model, 'fperiodoescolar', array(
                                10 => 2010,
                                11 => 2011,
                                12 => 2012,
                                13 => 2013,
                            ), array('required' => 'required' , 'maxlength' => 10,'placeholder' => 'Ejm: 54321','class' => 'span-7', 'id'=>'periodo'));
                            ?>
                            <?php // echo $form->dropDownList($model,'fperiodoescolar', CHtml::listData(array('10' => '10')));  ?>
                            <?php // echo $form->textField($model,'fperiodoescolar',array('size'=>3,'maxlength'=>3));  ?>
                            <?php echo $form->error($model, 'fperiodoescolar'); ?>
                        </div>



                    </div>

                    <div id="4taFila" class = "col-md-12">

                        <div class="col-md-4" >
                            <label class="col-sm-12" for="tdocumento_identidad"> Grado <span class="required">*</span></label>
                            <?php
                            echo $form->dropDownList(
                                $model, 's_grado_nombre', CHtml::listData(Grado::model()->findAll(array('order' => 'nombre ASC')), 'nombre', 'nombre'), array('empty' => array('' => '- SELECCIONE -'), 'required' => 'required', 'class' => 'span-7', 'id'=>'grado')
                            );
                            ?>
                            <?php // echo $form->textField($model,'s_grado_nombre'); ?>
                            <?php echo $form->error($model, 's_grado_nombre'); ?>
                        </div>

                        <div class="col-md-4" >
                            <label class="col-sm-12" for="tdocumento_identidad"> Modalidad <span class="required">*</span></label>
                            <?php
                            echo $form->dropDownList(
                                $model, 's_modalidad_nombre', CHtml::listData(Modalidad::model()->findAll(array('order' => 'nombre ASC')), 'nombre', 'nombre'), array('empty' => array('' => '- SELECCIONE -'), 'required' => 'required', 'class' => 'span-7', 'id'=>'modalidad')
                            );
                            ?>
                            <?php echo $form->error($model, 's_modalidad_nombre'); ?>
                        </div>

                        <div class="col-md-4" >
                            <label class="col-sm-12" for="tdocumento_identidad"> Escolaridad <span class="required">*</span></label>
                            <?php
                            echo $form->dropDownList(
                                $model, 's_escolaridad_nombre', CHtml::listData(Escolaridad::model()->findAll(array('order' => 'nombre ASC')), 'nombre', 'nombre'), array('empty' => array('' => '- SELECCIONE -'), 'required' => 'required', 'class' => 'span-7', 'id'=>'escolaridad')
                            );
                            ?>
                            <?php echo $form->error($model, 's_escolaridad_nombre'); ?>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="col-sm-12" for="curso"> Permanencia <span class="required">*</span></label>
                            <?php
                            echo $form->dropDownList($model, 'curso', array(
                                'CURSO' => 'CURSO',
                                'NO CURSO' => 'NO CURSO',
                            ), array('required' => 'required' , 'maxlength' => 10,'placeholder' => 'Ejm: 54321','class' => 'span-7', 'id'=>'curso'));
                            ?>
                            <?php // echo $form->dropDownList($model,'fperiodoescolar', CHtml::listData(array('10' => '10')));  ?>
                            <?php // echo $form->textField($model,'fperiodoescolar',array('size'=>3,'maxlength'=>3));  ?>
                            <?php echo $form->error($model, 'curso'); ?>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-xs-6">
                            <a id="btnRegresar" class="btn btn-danger" href="/estudiante">
                                <i class="icon-arrow-left"></i>
                                Volver
                            </a>
                        </div>
                        <div class="col-xs-6">
                            <button class="btn btn-primary btn-next pull-right" title="Guardar Datos de Escolaridad" data-last="Finish" type="submit">
                                Guardar
                                <i class="icon-save icon-on-right"></i>
                            </button>
                        </div>
                    </div>

                </div>

                <?php $this->endWidget(); ?>


            </div>
        </div>
    </div>
</div>

</div>



<script>

    $(document).ready(function() {

        $("#cedula").bind('keyup blur', function() {
            keyNum(this, true);
        });

        $("#cod_estadistico").bind('keyup blur', function() {
            keyNum(this, true);
        });

        $("#cod_plan").bind('keyup blur', function() {
            keyNum(this, true);
        });

        $("#observacion").bind('keyup blur', function() {
            keyAlphaNum(this, true);
            makeUpper(this);
        });

        /*****************Fecha Matricula***************************/
        $('#fecha_matricula').unbind('click');
        $('#fecha_matricula').unbind('focus');
        $('#fecha_matricula').datepicker();
        var fecha = new Date();
        var anoActual = fecha.getFullYear();

        $.datepicker.setDefaults($.datepicker.regional['es']);
        $.datepicker.setDefaults($.datepicker.regional = {
            dateFormat: 'yy-mm-dd',
            'showOn': 'focus',
            'showOtherMonths': false,
            'selectOtherMonths': true,
            'changeMonth': true,
            'changeYear': true,
            'yearRange': '1920:' + anoActual,
            minDate: new Date(1996, 1, 1),
            maxDate: 'today',

        });

        /*****************Fecha Egreso***************************/
        $('#fecha_egreso').unbind('click');
        $('#fecha_egreso').unbind('focus');
        $('#fecha_egreso').datepicker();
        var fecha = new Date();
        var anoActual = fecha.getFullYear();

        $.datepicker.setDefaults($.datepicker.regional['es']);
        $.datepicker.setDefaults($.datepicker.regional = {
            dateFormat: 'yy-mm-dd',
            'showOn': 'focus',
            'showOtherMonths': false,
            'selectOtherMonths': true,
            'changeMonth': true,
            'changeYear': true,
            'yearRange': '1920:' + anoActual,
            minDate: new Date(1996, 1, 1),
            maxDate: 'today',

        });

    });//------------FIN DOCUMENT

</script>



