<?php
/* @var $this EscolaridadController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Talumno Acads',
);

$this->menu=array(
	array('label'=>'Create TalumnoAcad', 'url'=>array('create')),
	array('label'=>'Manage TalumnoAcad', 'url'=>array('admin')),
);
?>

<h1>Talumno Acads</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
