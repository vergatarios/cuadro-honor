<?php
/* @var $this EscolaridadController */
/* @var $data TalumnoAcad */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('calumno')); ?>:</b>
	<?php echo CHtml::encode($data->calumno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cestadistico')); ?>:</b>
	<?php echo CHtml::encode($data->cestadistico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cgradoano')); ?>:</b>
	<?php echo CHtml::encode($data->cgradoano); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cseccion')); ?>:</b>
	<?php echo CHtml::encode($data->cseccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cplan')); ?>:</b>
	<?php echo CHtml::encode($data->cplan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cescolaridad1')); ?>:</b>
	<?php echo CHtml::encode($data->cescolaridad1); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cescolaridad2')); ?>:</b>
	<?php echo CHtml::encode($data->cescolaridad2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fperiodoescolar')); ?>:</b>
	<?php echo CHtml::encode($data->fperiodoescolar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fmatricula')); ?>:</b>
	<?php echo CHtml::encode($data->fmatricula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dobservacion')); ?>:</b>
	<?php echo CHtml::encode($data->dobservacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fegreso')); ?>:</b>
	<?php echo CHtml::encode($data->fegreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lapso')); ?>:</b>
	<?php echo CHtml::encode($data->lapso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cdea')); ?>:</b>
	<?php echo CHtml::encode($data->cdea); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cestado')); ?>:</b>
	<?php echo CHtml::encode($data->cestado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cregimen')); ?>:</b>
	<?php echo CHtml::encode($data->cregimen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cnivel')); ?>:</b>
	<?php echo CHtml::encode($data->cnivel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cestatusegreso')); ?>:</b>
	<?php echo CHtml::encode($data->cestatusegreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observacione')); ?>:</b>
	<?php echo CHtml::encode($data->observacione); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('carea_atencion')); ?>:</b>
	<?php echo CHtml::encode($data->carea_atencion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_matri')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_matri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('calumno2')); ?>:</b>
	<?php echo CHtml::encode($data->calumno2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_cedula_escolar')); ?>:</b>
	<?php echo CHtml::encode($data->s_cedula_escolar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_cedula_identidad')); ?>:</b>
	<?php echo CHtml::encode($data->s_cedula_identidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_carnet_diplomatico')); ?>:</b>
	<?php echo CHtml::encode($data->s_carnet_diplomatico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_alumno_id')); ?>:</b>
	<?php echo CHtml::encode($data->s_alumno_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_plantel_id')); ?>:</b>
	<?php echo CHtml::encode($data->s_plantel_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_nivel_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->s_nivel_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_grado_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->s_grado_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_periodo_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->s_periodo_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_modalidad_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->s_modalidad_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_escolaridad_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->s_escolaridad_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('historico')); ?>:</b>
	<?php echo CHtml::encode($data->historico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_plan_id')); ?>:</b>
	<?php echo CHtml::encode($data->s_plan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gplantel_plantel_id')); ?>:</b>
	<?php echo CHtml::encode($data->gplantel_plantel_id); ?>
	<br />

	*/ ?>

</div>