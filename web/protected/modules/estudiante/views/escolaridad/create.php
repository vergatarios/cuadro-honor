<?php
/* @var $this EscolaridadController */
/* @var $model TalumnoAcad */

$this->breadcrumbs=array(
	'Estudiantes'=>array('/estudiante/'),
	'Crear',
);
?>

<h1>Crear escolaridad</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>