<?php
/* @var $this DatosAntropometricosController */
/* @var $model DatosAntropometricos */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'datos-antropometricos-consultar-form',
    // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>
    
    <div id="resultadoDatosAntropometricos"></div>

    <div class="widget-box">

        <div class="widget-header">
            <h5>Datos Antropometricos </h5>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">

            <div class="widget-main">

                <div class="row">
                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'estatura', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'estatura', array("readonly" => "readonly", "class" => "span-7", 'data-toggle' => "tooltip", "data-placement" => "bottom", "placeholder" => "0.00 m ", "title" => "Ej: 0.40 - 2.00", "maxlength" => "4", "size" => "10", "min" => "0.80", "max" => "2.00", "required" => "required", "onkeypress" => "return(currencyFormat(this,'','.',event));")); ?>
                        <?php echo $form->error($model, 'estatura'); ?>
                    </div>

                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'peso', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'peso', array("class" => "span-7", "readonly" => "readonly")); ?>
                        <?php echo $form->error($model, 'peso'); ?>
                    </div>

                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'talla_camisa', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'talla_camisa', array("class" => "span-7", "readonly" => "readonly")); ?>
                        <?php echo $form->error($model, 'talla_camisa'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'talla_pantalon', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'talla_pantalon', array("class" => "span-7", "readonly" => "readonly")); ?>
                        <?php echo $form->error($model, 'talla_pantalon'); ?>
                    </div>

                    <div class="col-md-4">
                        <?php echo $form->labelEx($model, 'talla_zapato', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'talla_zapato', array("class" => "span-7", "readonly" => "readonly")); ?>
                        <?php echo $form->error($model, 'talla_zapato'); ?>
                    </div>
                </div>



            </div>
        </div>
    </div>


<br>

<div class="row">

    <div class="col-xs-6">
        <a class="btn btn-danger" href="/estudiante/consultar" id="btnRegresar">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>

    <div class="col-xs-6">
        <!--        <button class="btn btn-primary btn-next pull-right" title="Pasar al siguiente paso del estudiante" data-last="Finish" type="submit">
                    Actualizar cambios
            <i class="fa fa-mail-forward"></i>
                </button>-->
    </div>

</div>



<br><br>


    <?php $this->endWidget(); ?>
</div><!-- form -->





