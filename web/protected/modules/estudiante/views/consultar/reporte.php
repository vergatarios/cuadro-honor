<b>Identificaci&oacute;n del Estudiante</b>
<table border width="800px">
    <tr>
        <td width="17%">
            C&eacute;dula
        </td>
        <td width="17%">
            <?php echo $model->cedula_identidad; ?>
        </td>
        <td width="17%">
            Nombres
        </td>
        <td width="17%">
            <?php echo $model->nombres; ?>
        </td>
        <td width="17%">
            Apellidos
        </td>
        <td width="17%">
            <?php echo $model->apellidos; ?>
        </td>
    </tr>
    
    <tr>
        <td width="17%">
            Fecha de nacimiento
        </td>
        <td width="17%">
            <?php echo $model->fecha_nacimiento; ?>
        </td>
        <td width="17%">
            Lateralidad
        </td>
        <td width="17%">
            <?php
                $mano = $model->lateralidad_mano;
                if($mano == 'IZQ') {
                    echo 'IQZQUIERDO';
                }
                else if($mano == 'DER') {
                    echo 'DERECHO';
                }
                else if($mano == 'AMB') {
                    echo 'AMBIDIESTRO';
                }
                else {
                    echo '';
                }
            
            ?>
        </td>
        <td width="17%">
            Genero
        </td>
        <td width="17%">
            <?php echo $model->genero_id; ?>
        </td>
    </tr>
    
    <tr>
        <td width="17%">
            C&eacute;dula Escolar
        </td>
        <td width="17%">
            <?php echo $model->cedula_escolar; ?>
        </td>
        <td width="17%">
            Estado civil
        </td>
        <td width="17%">
            <?php echo $model->estadoCivil->nombre; ?>
        </td>
        <td width="17%">
            Nacionalidad
        </td>
        <td width="17%">
            <?php
                $nacionalidad = $model->nacionalidad;
                if($nacionalidad = 'V') {
                    echo 'VENEZOLANA';
                }
                else if($nacionalidad = 'E') {
                    echo 'EXTRANGERA';
                }
                else {
                    echo '';
                }
            ?>
        </td>
    </tr>
    
    <tr>
        <td width="17%">
            Pais de nacimiento
        </td>
        <td width="17%">
            <?php
                $pais = $model->obtenerDatosPais($model->pais_id);
                echo $pais[0]['nombre'];
            ?>
        </td>
        <td width="17%">
            Estado de nacimiento
        </td>
        <td width="17%">
            <?php echo $model->estado->nombre; ?>
        </td>
        <td width="17%">
            Etnia
        </td>
        <td width="17%">
            <?php
                $etnia = $model->obtenerDatosEtnia($model->etnia_id);
                echo $etnia[0]['nombre'];
            ?>
        </td>
    </tr>
</table>