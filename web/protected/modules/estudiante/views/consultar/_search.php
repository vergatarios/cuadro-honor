<?php
/* @var $this ConsultarController */
/* @var $model Estudiante */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row">
        <input type="hidden" value="<?php echo $plantel_id; ?>" name="Estudiante[plantel_id]">
        <input type="hidden" value="<?php //echo base64_encode($plantel_id);  ?>" name="id">
        
        <div class="col-md-6">
            <div class="col-md-12"><b>C&oacute;digo del Plantel</b></div>
            <div class="col-md-12">
                <?php echo $form->textField($model, 'plantel_actual_id', array('size' => 10, 'maxlength' => 15, 'class' => 'span-7')); ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="col-md-12"><b>Documento de Identidad del Representante</b></div>
            <div class="col-md-12">
            <?php echo $form->textField($model, 'representante_id', array('size' => 10, 'maxlength' => 15, 'class' => 'span-7', 'id' => 'Estudiante_documento_identidad_representante_search')); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-12"><b>Documento Identidad del Estudiante</b></div>
            <div class="col-md-12">
            <?php echo $form->textField($model, 'documento_identidad', array('size' => 10, 'maxlength' => 10, 'class' => 'span-7', 'id' => 'Estudiante_documento_identidad_search')); ?>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="col-md-12"><b>C&eacute;dula Escolar del Estudiante</b></div>
            <div class="col-md-12">
                <?php echo $form->textField($model, 'cedula_escolar', array('class' => 'span-7', 'id' => 'Estudiante_cedula_escolar_search')); ?>
            </div>
        </div>
    </div>

   <!-- <div class="row">
        
        <div class="col-md-6">
            <div class="col-md-12"><b>Nombres del Estudiante</b></div>
            <div class="col-md-12">
                <?php /*echo $form->textField($model, 'nombres', array('size' => 10, 'maxlength' => 100, 'class' => 'span-7', 'id' => 'Estudiante_nombres_search')); */?>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="col-md-12"><b>Apellidos del Estudiante</b></div>
            <div class="col-md-12">
                <?php /*echo $form->textField($model, 'apellidos', array('size' => 10, 'maxlength' => 100, 'class' => 'span-7', 'id' => 'Estudiante_apellidos_search')); */?>
            </div>
        </div>


    </div>-->

    <div class="row">
        <div class="text-right">
            <div style="padding-right: 13px;" class="col-md-12">
                <button id="buscarPlantel" type="submit" data-last="Finish" class="btn btn-primary btn-next btn-sm">
                    Buscar
                    <i class="fa fa-search icon-on-right"></i>
                </button>
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
