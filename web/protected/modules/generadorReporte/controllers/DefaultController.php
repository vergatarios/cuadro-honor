<?php
/**
 * Author: Maikel Ortega Hernandez
 * Email: maikeloh@gmail.com
 * Date: 30/10/14
 * Time: 22:48
 */

class DefaultController extends Controller {

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                //'ips' => Yii::app()->controller->module->ipFilters
                'ips' => $this->module->ipFilters
            ),
            array(
                'deny'
            )
        );
    }

    public function actionIndex()
    {
        $query = (isset($_POST['query'])) ? $_POST['query'] : null;
        Yii::app()->log->routes[0]->enabled = false;

        if(is_null($query)){
            echo ('You must provide a query param');
            Yii::app()->end();
        }


        /**
        $jsonQuery = array(
            'select' => array(
                'ie.id',
                'ie.plantel_id',
                'p.estado_id',
                'p.municipio_id',
                'ie.estudiante_id',
                'ie.inscripcion_regular',
                'ie.repitiente',
                'ie.repitiente_completo',
                'ie.materia_pendiente',
                'ie.fecha_ini',
                'ie.periodo_id',
                'spp.seccion_plantel_id',
                'ie.seccion_plantel_periodo_id',
                'sp.nivel_id'

            ),
            'from'   => array(
                'gplantel.plantel', 'p'
            ),
            'left_join' => array(
                 array(
                    array('matricula.inscripcion_estudiante', 'ie'),
                    'ie.plantel_id = p.id'
                )
            ),
            'inner_join' => array(
                array(
                    array('gplantel.seccion_plantel_periodo', 'spp'),
                    'ie.seccion_plantel_periodo_id = spp.id'
                ),
                array(
                    array('gplantel.seccion_plantel', 'sp'),
                    'sp.id=spp.seccion_plantel_id'
                ),
            ),
            'output' => 'csv'
        );
        **/

        $jsonQuery = json_decode($query, true);

        if($jsonQuery === null){
            echo "Error parsing json data, incorrect json string.\r\n";
            var_dump($query);
            var_dump($jsonQuery);
            Yii::app()->end();
        }



        try {
            Yii::app()->generadorReporte->setJsonQuery($jsonQuery);
        } catch(Exception $e){
            echo $e->getMessage();
            Yii::app()->end();
        }

        try {
            $data = Yii::app()->generadorReporte->getData();
        } catch(Exception $e ){
            echo $e->getMessage();
            Yii::app()->end();
        } catch(InvalidArgumentException $e){
            echo $e->getMessage();
            Yii::app()->end();
        }

        echo $data;
        Yii::app()->end();
    }

} 
