<?php

/* @var $this AplicacionController */
/* @var $model Aplicacion */

$this->breadcrumbs=array(
	'Aplicacions'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Aplicacions';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Aplicacions</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Aplicacions.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/aplicacion/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Aplicacions                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'aplicacion-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>nombre</center>',
            'name' => 'nombre',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[nombre]', $model->nombre, array('title' => '',)),
        ),
        array(
            'header' => '<center>codigo</center>',
            'name' => 'codigo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[codigo]', $model->codigo, array('title' => '',)),
        ),
        array(
            'header' => '<center>key</center>',
            'name' => 'key',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[key]', $model->key, array('title' => '',)),
        ),
        array(
            'header' => '<center>password</center>',
            'name' => 'password',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[password]', $model->password, array('title' => '',)),
        ),
        array(
            'header' => '<center>encriptacion_id</center>',
            'name' => 'encriptacion_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[encriptacion_id]', $model->encriptacion_id, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>plataforma_id</center>',
            'name' => 'plataforma_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[plataforma_id]', $model->plataforma_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>url</center>',
            'name' => 'url',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[url]', $model->url, array('title' => '',)),
        ),
        array(
            'header' => '<center>datos_consultados</center>',
            'name' => 'datos_consultados',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[datos_consultados]', $model->datos_consultados, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),
        array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[estatus]', $model->estatus, array('title' => '',)),
        ),
        array(
            'header' => '<center>cedula_contacto</center>',
            'name' => 'cedula_contacto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[cedula_contacto]', $model->cedula_contacto, array('title' => '',)),
        ),
        array(
            'header' => '<center>nombre_apellido_contacto</center>',
            'name' => 'nombre_apellido_contacto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[nombre_apellido_contacto]', $model->nombre_apellido_contacto, array('title' => '',)),
        ),
        array(
            'header' => '<center>telefono_fijo_contacto</center>',
            'name' => 'telefono_fijo_contacto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[telefono_fijo_contacto]', $model->telefono_fijo_contacto, array('title' => '',)),
        ),
        array(
            'header' => '<center>telefono_celular_contacto</center>',
            'name' => 'telefono_celular_contacto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[telefono_celular_contacto]', $model->telefono_celular_contacto, array('title' => '',)),
        ),
        array(
            'header' => '<center>email_contacto</center>',
            'name' => 'email_contacto',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[email_contacto]', $model->email_contacto, array('title' => '',)),
        ),
        array(
            'header' => '<center>rif_institucion</center>',
            'name' => 'rif_institucion',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Aplicacion[rif_institucion]', $model->rif_institucion, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>