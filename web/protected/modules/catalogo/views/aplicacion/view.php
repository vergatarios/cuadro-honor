<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Aplicacions'=>array('lista'),
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'aplicacion-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos de la Aplicación</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Nombre de la Aplicación:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->nombre); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Codigo:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->codigo); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Key:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->key); ?></label>					
                                                    </div>
                                                </div>
                                                 <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Password:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->password); ?></label>					
                                                    </div>
                                                </div>
                                                 <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Tipo de Encriptación:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->encriptacion->nombre); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Plataforma:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->plataforma->nombre); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Dirección URL :</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->url); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Datos Consultados por la App:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->datos_consultados); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row" >
                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Fecha de Creación:</b></label>		
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_ini))); ?></label>				
                                                    </div>
                                                </div>
                                                
                                                <div class="row">  
                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Creado por:</b></label>			
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->usuarioIni->nombre . " " . $model->usuarioIni->apellido); ?></label>
                                                    </div>

                                                </div>
                                                <div class="row" >
                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Estatus:</b></label>		
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="col-md-12" >
                                                            <?php
                                                            if ($model->estatus == "A") {
                                                                echo "Activo";
                                                            } else if ($model->estatus == "I") {
                                                                echo "Inactivo";
                                                            } else if ($model->estatus == "E") {
                                                                echo "Eliminado";
                                                            }
                                                            ?>			
                                                        </label>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>    
                                
                                                 <div class="space-6"></div>

                        <div class="widget-box">

                            <div class="widget-header">
                                <h5>Datos del Contacto</h5>

                                <div class="widget-toolbar">
                                    <a data-action="collapse" href="#">
                                        <i class="icon-chevron-up"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="widget-body">
                                <div class="widget-body-inner">
                                    <div class="widget-main">
                                        <div class="widget-main form">                                                                                              
                                                <div class="row">  
                                                    <?php if ($model->estatus == "I") { ?>
                                                        <div class="col-md-6">
                                                            <label class="col-md-12" ><b>Modificado por:</b></label>			
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label class="col-md-12" ><?php echo CHtml::encode($model->usuarioAct->nombre . " " . $model->usuarioAct->apellido); ?></label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="col-md-12"><b>Inhabilitado el:</b></label>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label class="col-md-12"><?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_elim))); ?></label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="col-md-12"><b>Fecha de Actualización:</b></label>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label class="col-md-12">
                                                                <?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_act))); ?>	
                                                            </label>
                                                        </div>

                                                    <?php } ?>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Cedula:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->cedula_contacto); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Nombre y Apellido:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->nombre_apellido_contacto); ?></label>					
                                                    </div>
                                                </div>
                                                 <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Telefono Fijo:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->telefono_fijo_contacto); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Telefono Movil:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->telefono_celular_contacto); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>Correo Electronico:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->email_contacto); ?></label>					
                                                    </div>
                                                </div>
                                                <div class="row">                                                    

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><b>RIF de la Institución que Representa:</b></label>					
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label class="col-md-12" ><?php echo CHtml::encode($model->rif_institucion); ?></label>					
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="row">
                                                   

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                                                                              

                                                  </div>

                                                    <div class="space-6"></div>

                                                    

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                                                                            

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">                                                
                                                        

                                                  </div>

                                                    <div class="space-6"></div>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                             
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>
            
        </div>

    </div>
</div>