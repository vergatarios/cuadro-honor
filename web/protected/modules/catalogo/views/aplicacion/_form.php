<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <!--            <ul class="nav nav-tabs">
                            <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                            <li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>
                        </ul>-->

            <!--            <div class="tab-content">-->
            <div class="tab-pane active" id="datosGenerales">
                <div class="form">

                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'aplicacion-form',
                        //'htmlOptions' => array('data-form-type' => $formType,), // for inset effect
                        // Please note: When you enable ajax validation, make sure the corresponding
                        // controller action is handling ajax validation correctly.
                        // There is a call to performAjaxValidation() commented in generated controller code.
                        // See class documentation of CActiveForm for details on this.
                        'enableAjaxValidation' => false,
                    ));
                    ?>
                    <div id="validaciones">

                    </div>
                    <div id="div-result">
                        <?php
                        if ($model->hasErrors()) {
                            $this->renderPartial('//errorSumMsg', array('model' => $model));
                        } else {
                            if (Yii::app()->user->hasFlash('exito')) {
                                ?>
                                <div class="successDialogBox">
                                    <p><?php echo Yii::app()->user->getFlash('exito'); ?></p>
                                </div>
                                <?php
                            } else {
                                ?>                        
                                <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                                <?php
                            }
                        }
                        ?>
                    </div>

                    <div id="div-datos-generales">

                        <div class="widget-box">

                            <div class="widget-header">
                                <h5>Datos Generales</h5>

                                <div class="widget-toolbar">
                                    <a data-action="collapse" href="#">
                                        <i class="icon-chevron-up"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="widget-body">
                                <div class="widget-body-inner">
                                    <div class="widget-main">
                                        <div class="widget-main form">
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="col-md-6">
                                                        <?php echo $form->labelEx($model, 'nombre'); ?>
                                                        <?php echo $form->textField($model, 'nombre', array('size' => 60, 'maxlength' => 100, 'class' => 'span-12', "required" => "required",)); ?>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <?php echo $form->labelEx($model, 'codigo'); ?>
                                                        <?php echo $form->textField($model, 'codigo', array('size' => 60, 'maxlength' => 100, 'class' => 'span-12', "required" => "required",)); ?>
                                                    </div>

                                                    <!--                                                    <div class="col-md-4">
                                                    <?php
                                                    $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
                                                    $numerodeletras = 23; //numero de letras para generar el texto
                                                    $key = ""; //variable para almacenar la cadena generada
                                                    for ($i = 0; $i < $numerodeletras; $i++) {
                                                        $key .= substr($caracteres, rand(0, strlen($caracteres)), 1); /* Extraemos 1 caracter de los caracteres 
                                                          entre el rango 0 a Numero de letras que tiene la cadena */
                                                    }
                                                    //echo $password;
                                                    $model->key = $key;
                                                    ?>
                                                    <?php echo $form->labelEx($model, 'key'); ?>
                                                    <?php echo $form->textField($model, 'key', array('size' => 60, 'maxlength' => 300, 'class' => 'span-12', "required" => "required",)); ?>
                                                                                                        </div>-->

                                                </div>

                                                <div class="space-6"></div>

                                                <div class="col-md-12">

                                                    <!--                                                    <div class="col-md-4">
                                                    <?php
                                                    $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
                                                    $numerodeletras = 23; //numero de letras para generar el texto
                                                    $password = ""; //variable para almacenar la cadena generada
                                                    for ($i = 0; $i < $numerodeletras; $i++) {
                                                        $password .= substr($caracteres, rand(0, strlen($caracteres)), 1); /* Extraemos 1 caracter de los caracteres 
                                                          entre el rango 0 a Numero de letras que tiene la cadena */
                                                    }
                                                    //echo $password;
                                                    $model->password = $password;
                                                    ?>
                                                    <?php echo $form->labelEx($model, 'password'); ?>
                                                    <?php echo $form->textField($model, 'password', array('size' => 60, 'maxlength' => 300, 'class' => 'span-12', "required" => "required",)); ?>
                                                                                                        </div>-->


                                                    <div class="col-md-6">
<?php echo $form->labelEx($model, 'encriptacion_id'); ?>
                                                        <?php echo $form->dropDownList($model, 'encriptacion_id', CHtml::listData(Encriptacion::model()->findAll(array('limit' => 50)), 'id', 'nombre'), array('prompt' => '- - -', 'class' => 'span-12', "required" => "required",)); ?>
                                                    </div>


                                                    <div class="col-md-6">
<?php echo $form->labelEx($model, 'plataforma_id'); ?>
                                                        <?php echo $form->dropDownList($model, 'plataforma_id', CHtml::listData(Plataforma::model()->findAll(array('limit' => 50)), 'id', 'nombre'), array('prompt' => '- - -', 'class' => 'span-12', "required" => "required",)); ?>
                                                    </div>

                                                </div>

                                                <div class="space-6"></div>

                                                <div class="col-md-12">

                                                    <div class="col-md-6">
<?php echo $form->labelEx($model, 'url'); ?>
                                                        <?php echo $form->textField($model, 'url', array('size' => 60, 'maxlength' => 255, 'class' => 'span-12', "required" => "required",)); ?>
                                                    </div>

                                                    <div class="col-md-6">
<?php echo $form->labelEx($model, 'estatus'); ?>
                                                        <?php echo $form->dropDownList($model, 'estatus', array('A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado', 'B' => 'Baneado'), array('prompt' => '- - -', 'class' => 'span-12',)); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-12">
<?php echo $form->labelEx($model, 'datos_consultados'); ?>
                                                        <?php echo $form->textArea($model, 'datos_consultados', array('rows' => 6, 'cols' => 12, 'class' => 'span-12', "required" => "required",)); ?>
                                                    </div>
                                                </div>


                                                <div class="space-6"></div>

                                                <div class="col-md-12">



                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                        </div>

                        <div class="space-6"></div>

                        <div class="widget-box">

                            <div class="widget-header">
                                <h5>Datos del Contacto</h5>

                                <div class="widget-toolbar">
                                    <a data-action="collapse" href="#">
                                        <i class="icon-chevron-up"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="widget-body">
                                <div class="widget-body-inner">
                                    <div class="widget-main">
                                        <div class="widget-main form">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3">
<?php echo $form->labelEx($model, 'tdocumento_identidad'); ?>                                                            
                                                        <?php echo $form->dropDownList($model, 'tdocumento_identidad', array('V' => 'Venezolano', 'E' => 'Extranjero', 'P' => 'Pasaporte'), array('prompt' => '- - -', 'class' => 'span-12',)); ?>
                                                    </div>
                                                    <div class="col-md-3">
<?php echo $form->labelEx($model, 'cedula_contacto'); ?>
                                                        <?php echo $form->textField($model, 'cedula_contacto', array('class' => 'span-12', "required" => "required",)); ?>
                                                    </div>


                                                    <div class="col-md-3">
<?php echo $form->labelEx($model, 'nombre_apellido_contacto'); ?>
                                                        <?php echo $form->textField($model, 'nombre_apellido_contacto', array('size' => 60, 'maxlength' => 80, 'class' => 'span-12',)); ?>
                                                    </div>
                                                    <div class="col-md-3">
<?php echo $form->labelEx($model, 'email_contacto'); ?>
                                                        <?php echo $form->emailField($model, 'email_contacto', array('size' => 60, 'maxlength' => 100, 'class' => 'span-12', "required" => "required", 'placeholder' => 'user@email.com')); ?>
                                                    </div>

                                                </div>

                                                <div class="space-6"></div>

                                                <div class="col-md-12">
                                                    <div class="col-md-4">
<?php echo $form->labelEx($model, 'rif_institucion'); ?>
                                                        <?php echo $form->textField($model, 'rif_institucion', array('class' => 'span-12', "required" => "required",'placeholder' => 'X-999999999-9')); ?>
                                                    </div>

                                                    <div class="col-md-4">
<?php echo $form->labelEx($model, 'telefono_fijo_contacto'); ?>
                                                        <?php echo $form->textField($model, 'telefono_fijo_contacto', array('size' => 11, 'maxlength' => 11, 'class' => 'span-12', "required" => "required",'placeholder' => '(0299)9999999')); ?>
                                                    </div>


                                                    <div class="col-md-4">
<?php echo $form->labelEx($model, 'telefono_celular_contacto'); ?>
                                                        <?php echo $form->textField($model, 'telefono_celular_contacto', array('size' => 11, 'maxlength' => 11, 'class' => 'span-12', "required" => "required",'placeholder' => '(0499)9999999')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

<?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>


            </div>
            <!--            </div>-->

            <div id="resultDialog" class="hide"></div>

<?php
Yii::app()->clientScript->registerScriptFile(
        Yii::app()->request->baseUrl . '/public/js/modules/catalogo/Aplicacion/form.js', CClientScript::POS_END
);
?>
            <?php
            Yii::app()->clientScript->registerScriptFile(
                    Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END
            );
            ?>
        </div>
    </div>
    <script src="/public/js/jquery.maskedinput.min.js"></script>
    <script>
$(document).ready(function(){
    // Filters
    $("#Aplicacion_nombre").on('keyup blur', function(){
        keyText(this, true);
    });
    
    $("#Aplicacion_cedula_contacto").on('keyup blur', function(){
        keyNum(this, false, false);
    });
    
    $("#Aplicacion_nombre_apellido_contacto").on('keyup blur', function(){
        keyText(this, true);
    });
    
    $("#Aplicacion_datos_consultados").on('keyup blur', function(){
        keyText(this, true);
    });
    
    $("#Aplicacion_email_contacto").on('keyup blur', function(){
        keyEmail(this, false);
    });
    
    // Clear Fields
    
    $("#Aplicacion_nombre").on('blur', function(){
        clearField(this);
    });
    $("#Aplicacion_cedula_contacto").on('blur', function(){
        clearField(this);
    });
    
    $("#Aplicacion_nombre_apellido_contacto").on('blur', function(){
        clearField(this);
    });
    
    $("#Aplicacion_datos_consultados").on('blur', function(){
        clearField(this);
    });
    
    $("#Aplicacion_email_contacto").on('blur', function(){
        clearField(this);
    });
    
    // Masks

    $.mask.definitions['L'] = '[1-2]';
    $.mask.definitions['X'] = '[2|4|6]';
    
    $('#Aplicacion_telefono_fijo_contacto').mask('(0299)999-9999');
    $('#Aplicacion_telefono_celular_contacto').mask('(04LX)999-9999');      
});
</script>
</div>
