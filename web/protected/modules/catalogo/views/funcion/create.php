<?php
/* @var $this FuncionController */
/* @var $model Funcion */

$this->pageTitle = 'Registro de Funcions';

      $this->breadcrumbs=array(
	'Funcions'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>