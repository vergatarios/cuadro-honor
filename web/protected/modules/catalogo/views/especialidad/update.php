<?php
/* @var $this EspecialidadController */
/* @var $model Especialidad */

$this->pageTitle = 'Actualización de Datos de Especialidads';

$this->breadcrumbs=array(
    'Catalogo'=>array('/catalogo/'),
    'Especialidades'=>array('/catalogo/especialidad/lista'),
    'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>