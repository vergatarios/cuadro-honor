<?php
/* @var $this EspecialidadController */
/* @var $model Especialidad */

$this->pageTitle = 'Registro de Especialidades';

$this->breadcrumbs=array(
	'Catalogo'=>array('/catalogo/'),
	'Especialidades'=>array('/catalogo/especialidad/lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>