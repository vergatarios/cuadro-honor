<?php
/* @var $this EstudioController */
/* @var $model Estudio */

$this->pageTitle = 'Registro de Estudios';

$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Estudios'=>array('lista'),
    'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro','estatus'=>$estatus)); ?>