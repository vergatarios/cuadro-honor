<?php
/* @var $this TiempoDedicacionController */
/* @var $model TiempoDedicacion */

$this->pageTitle = 'Registro de Tiempo Dedicacions';

$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Tiempo de Dedicación'=>array('/catalogo/tiempoDedicacion'),
    'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro','exito'=>$exito)); ?>