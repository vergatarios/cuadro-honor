<?php
/* @var $this TiempoDedicacionController */
/* @var $model TiempoDedicacion */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Tiempo de Dedicación'=>array('/catalogo/tiempoDedicacion'),
    'Consulta',
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'tiempo-dedicacion-form',
                            'htmlOptions' => array('onSubmit' => 'return false;',), // for inset effect
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation' => false,
                        ));
                        ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Vista de Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label class="col-md-12" ><b>Nombre:</b></label>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="col-md-12" ><?php echo CHtml::encode($model->nombre); ?></label>
                                                            </div>

                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label class="col-md-12" ><b>Estatus:</b></label>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="col-md-12" ><?php echo CHtml::encode($model->estatus); ?></label>
                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label class="col-md-12" ><b>Creado Por:</b></label>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="col-md-12" ><?php echo CHtml::encode($model->usuarioIni->nombre . " " . $model->usuarioIni->apellido); ?></label>
                                                            </div>

                                                        </div>
                                                        <div class="row" >
                                                            <div class="col-md-6">
                                                                <label class="col-md-12" ><b>Fecha de Creación:</b></label>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="col-md-12" ><?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_ini))); ?></label>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/catalogo/tiempoDedicacion"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>
            </div>

        </div>
    </div>

</div>
</div>