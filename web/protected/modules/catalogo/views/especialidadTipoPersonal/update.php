<?php
/* @var $this EspecialidadTipoPersonalController */
/* @var $model EspecialidadTipoPersonal */

$this->pageTitle = 'Actualización de Datos de Especialidad Tipo Personals';

      $this->breadcrumbs=array(
	'Especialidad Tipo Personals'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>