<?php

/* @var $this EspecialidadTipoPersonalController */
/* @var $model EspecialidadTipoPersonal */

$this->breadcrumbs=array(
    'Catalogos'=>array('/catalogo/'),
    'Especialidad por Tipo de Personal',
    'Administración',
);
$this->pageTitle = 'Administración de Especialidad por Tipo de Personal';

?>
    <div class="widget-box">
        <div class="widget-header">
            <h5>Lista de Especialidad por Tipo de Personal</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">

                    <div class="row space-6"></div>
                    <div>
                        <div id="resultadoOperacion">
                            <div class="infoDialogBox">
                                <p>
                                    En este módulo podrá registrar y/o actualizar los datos de Especialidad Tipo Personals.
                                </p>
                            </div>
                        </div>

                        <div class="pull-right" style="padding-left:10px;">
                            <a href="<?php echo $this->createUrl("/catalogo/especialidadTipoPersonal/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar Nueva Especialidad por Tipo de Personal                        </a>
                        </div>


                        <div class="row space-20"></div>

                    </div>

                    <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'especialidad-tipo-personal-grid',
                        'dataProvider'=>$dataProvider,
                        'filter'=>$model,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'summaryText' => 'Mostrando {start}-{end} de {count}',
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                        'afterAjaxUpdate' => "
                function(){

                }",
                        'columns'=>array(
                            array(
                                'header' => '<center>Especialidad</center>',
                                'name' => 'especialidad_id',
                                'htmlOptions' => array(),
                                'value' => '(is_object($data->especialidad) && isset($data->especialidad->nombre))? $data->especialidad->nombre: ""',
                                //'value'=>'(is_object($data->especialidad) AND isset($data->especialidad->nombre)?$data->especialidad->nombre:"")',
                                'filter' => CHtml::textField('EspecialidadTipoPersonal[especialidad_nom]', $model->especialidad_nom, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>Tipo de Personal</center>',
                                'name' => 'tipo_personal_id',
                                'htmlOptions' => array(),
                                'value' => '(is_object($data->tipoPersonal) && isset($data->tipoPersonal->nombre))? $data->tipoPersonal->nombre: ""',
                                'filter' => CHtml::textField('EspecialidadTipoPersonal[tipo_personal_nom]', $model->tipo_personal_nom, array('title' => '',)),
                            ),
                            /*
                            array(
                                'header' => '<center>fecha_act</center>',
                                'name' => 'fecha_act',
                                'htmlOptions' => array(),
                                //'filter' => CHtml::textField('EspecialidadTipoPersonal[fecha_act]', $model->fecha_act, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>usuario_elim_id</center>',
                                'name' => 'usuario_elim_id',
                                'htmlOptions' => array(),
                                //'filter' => CHtml::textField('EspecialidadTipoPersonal[usuario_elim_id]', $model->usuario_elim_id, array('title' => '',)),
                            ),
                            array(
                                'header' => '<center>fecha_elim</center>',
                                'name' => 'fecha_elim',
                                'htmlOptions' => array(),
                                //'filter' => CHtml::textField('EspecialidadTipoPersonal[fecha_elim]', $model->fecha_elim, array('title' => '',)),
                            ),*/
                            array(
                                'header' => '<center>Estatus</center>',
                                'name' => 'estatus',
                                'htmlOptions' => array(),
                                'value' => array($this, 'estatus'),
                                'filter' => array('A' => 'Activo', 'E' => 'Eliminado', 'I' => 'Inactivo'),
                            ),

                            array(
                                'type' => 'raw',
                                'header' => '<center>Acción</center>',
                                'value' => array($this, 'getActionButtons'),
                                'htmlOptions' => array('nowrap'=>'nowrap'),
                            ),
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/modules/catalogo/especialidadTipoPersonal/form.js',CClientScript::POS_END);
?>