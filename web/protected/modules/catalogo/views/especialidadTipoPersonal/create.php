<?php
/* @var $this EspecialidadTipoPersonalController */
/* @var $model EspecialidadTipoPersonal */

$this->pageTitle = 'Registro de Especialidad Tipo Personals';

$this->breadcrumbs=array(
    'Catálogo'=>array('/catalogo'),
    'Especialidad del Tipo de Personal'=>array('lista'),
    'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>