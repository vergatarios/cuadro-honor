<?php
/* @var $this ClasePlantelController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Catálogo'=>'/catalogo/',
	'Clase de Planteles'
);
echo CHtml::scriptFile('/public/js/modules/catalogo/clasePlantel.js');
?>



<div class="widget-box">

    <?php
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $('#plantel-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
        return false;
    });
    ");
    ?>

    <div class="widget-header">
        <h4>Clases de Planteles</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
                <div class="row col-sm-8" id="resultadoOperacion"></div>
                <div class="row space-6"></div>
                <div>
                    <?php
                        if(($groupId == 1) || ($groupId == 18))
                        {
                        ?>
                            <div class="pull-right" style="padding-left:10px;">
                                <a  type="submit" onclick="VentanaDialog('','/catalogo/clasePlantel/create','Nueva Clase de Plantel','create','')" data-last="Finish" class="btn btn-success btn-next btn-sm">
                                    <i class="fa fa-plus icon-on-right"></i>
                                    Registrar nueva clase de plantel
                                </a>
                           
                            </div>
                 
                        <?php
                        }
                    ?>

                    <div class="row space-20">


                    </div>

                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                    	'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'id'=>'clase-plantel-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'summaryText'=>false,
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                        'columns' => array(
                            array(
                                'header' => '<center>Nombre de la clase de plantel</center>',
                                'name' => 'nombre',

                            ),
                            array(
                                'header' => '<center>Estatus</center>',
                                'name' => 'estatus',
                                'value' => array($this, 'estatus'),
                                'filter'=>array('A'=>'Activo','E'=>'Inactivo'),
                                
                                
                            ),
                        
                            array(
                                'type' => 'raw',
                                'header' => '<center>Acciones</center>',
                                'value' => array($this, 'columnaAcciones'),
                            ),
                        ),
                    ));
                    ?>
                    <div class="row-fluid-actions">
                                <a class="btn btn-danger" href="/catalogo">
                                <i class="icon-arrow-left bigger-110"></i>
                                Volver
                                </a>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>








<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */?>
