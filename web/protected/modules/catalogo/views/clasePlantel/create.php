<?php
/* @var $this ClasePlantelController */
/* @var $model ClasePlantel */
/*
$this->breadcrumbs=array(
	'Clase Plantels'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ClasePlantel', 'url'=>array('index')),
	array('label'=>'Manage ClasePlantel', 'url'=>array('admin')),
);
*/
?>

<div class="widget-box">

        <div class="widget-header">
            <h5>Crear nueva Clase de Plantel</h5>

            <div class="widget-toolbar">
                <a>
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

