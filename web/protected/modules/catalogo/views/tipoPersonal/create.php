<?php
/* @var $this TipoPersonalController */
/* @var $model TipoPersonal */

$this->pageTitle = 'Registro de Tipo Personals';

      $this->breadcrumbs=array(
	'Tipo Personal'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>