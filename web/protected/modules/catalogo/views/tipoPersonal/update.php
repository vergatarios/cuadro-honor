<?php
/* @var $this TipoPersonalController */
/* @var $model TipoPersonal */

$this->pageTitle = 'Actualización de Datos de Tipo Personals';

      $this->breadcrumbs=array(
	'Tipo Personals'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>