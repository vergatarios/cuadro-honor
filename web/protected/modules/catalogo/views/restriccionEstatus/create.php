<?php
/* @var $this RestriccionEstatusController */
/* @var $model RestriccionEstatus */

$this->pageTitle = 'Registro de Restricción Estatus';

      $this->breadcrumbs=array(
	'Restricción Estatus'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>