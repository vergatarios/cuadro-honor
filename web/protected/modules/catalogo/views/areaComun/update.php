<?php
/* @var $this AreaComunController */
/* @var $model AreaComun */

$this->pageTitle = 'Actualización de Datos de Área Común';

$this->breadcrumbs=array(
    'Catálogo'=>array('/catalogo/'),
    'Área Común'=>array('/catalogo/areaComun/lista'),
    'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>