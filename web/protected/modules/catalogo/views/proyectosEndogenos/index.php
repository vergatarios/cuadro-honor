<?php
$this->breadcrumbs = array(
    'Catalogo' => array('/catalogo'),
    'Proyectos Endogenos',
);
?>
<div class="widget-box">

    <?php
    Yii::app()->clientScript->registerScript('search', "
	$('.search-form form').submit(function(){
		$('#proyectos-endogenos-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
	");
    ?>

    <div class="widget-header">
        <h5>Proyectos Endogenos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
                <div>
                    <div class="pull-right" style="padding-left:10px;">
                        <a id="btnCrearProyectoEndogeno" data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar nuevo Proyecto Endogeno
                        </a>
                    </div>

                    <div class="col-lg-12"><div class="space-6"></div></div>

                    <?php
                    $nombre = array(
                        'header' => '<center>Nombre Asignado</center>',
                        'name' => 'nombre',
                            //'value' => '(is_object($data->nombre) && isset($data->nombre))? $data->nombre : ""',
                    );
                    $estatus = array(
                        'type' => 'raw',
                        'header' => '<center>Estatus</center>',
                        'name' => 'estatus',
                        'value' => 'strtr("{$data->estatus}", array("A"=>"Activo", "E"=>"Inactivo"))',
                        'filter' => array("A" => "Activo", "E" => "Inactivo"),
                        'htmlOptions' => array('width' => '200px', 'class' => 'center'),
                    );
                    $acciones = array(
                        'type' => 'raw',
                        'header' => '<center>Acciones</center>',
                        'value' => array($this, 'columnaAcciones'),
                        'htmlOptions' => array('width' => '120x', 'class' => 'center'),
                    );


                    $this->widget('zii.widgets.grid.CGridView', array(
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'id' => 'proyectos-endogenos-grid',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        //'pager' => array('pageSize' => 1),
                        'summaryText' => false,
                        'afterAjaxUpdate' => "function(){
                            
                             $('#ProyectosEndogenos_nombre').bind('keyup blur', function() {
                                   keyText(this, false);
                                   makeUpper(this, true);
                                   clearField(this);
                             });
 
                         }",
                        'columns' => array(
                            $nombre,
                            $estatus,
                            $acciones
                        ),
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>

<div class="row">

    <div class="col-md-6">
        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("catalogo"); ?>" class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>

    <!-- <div class="col-md-6 wizard-actions">
         <button type="submit" data-last="Finish" class="btn btn-primary btn-next">
             Guardar
             <i class="icon-save icon-on-right"></i>
         </button>
     </div>
    -->

</div>


<div id="detalleGrado" class="hide"></div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/proyectos_endogenos.js', CClientScript::POS_END);
