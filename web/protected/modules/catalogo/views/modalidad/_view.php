<?php
/* @var $this ModalidadController */
/* @var $data Modalidad */
?>


<div class="view">

    <div class="tabbable">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#datosGenerales">Datos Generales</a></li>
            <li><a data-toggle="tab" href="#niveles">Niveles</a></li>
        </ul>

        <div class="tab-content">

            <div id="datosGenerales" class="tab-pane active">


                <div class="widget-main form">
                    <div class="row">                                                                
                        <div class="col-md-6">
                            <label class="col-md-12" ><b>Nombre:</b></label>

                        </div>
                        <div class="col-md-6">

                            <?php echo CHtml::encode($model->nombre); ?>			
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-md-12" ><b>Creado por:</b></label>
                        </div>
                        <div class="col-md-6">
                            <?php echo CHtml::encode($model->usuarioIni->nombre . " " . $model->usuarioIni->apellido); ?>			
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <label class="col-md-12" ><b>Fecha de Creación:</b></label>

                        </div>
                        <div class="col-md-6">
                            <?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_ini))); ?>			
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <label class="col-md-12" ><b>Estatus:</b></label>

                        </div>	
                        <div class="col-md-6">


                            <?php if ($model->estatus == "A") {
                                echo CHtml::encode("Activo");
                            } else if ($model->estatus == "E") {
                                echo CHtml::encode("Inactivo");
                            }
                            ?>			
                        </div>		

                    </div>
                    <div class="row">
                            <?php if ($model->usuarioAct) { ?>
                            <div class="col-md-6">
                                <label class="col-md-12" ><b>Modificado por:</b></label>
                            </div>
                            <div class="col-md-6">
    <?php echo CHtml::encode($model->usuarioAct->nombre . " " . $model->usuarioAct->apellido); ?>			
                            </div>
<?php } ?>
                    </div>

                    <div class="row">


                            <?php if ($model->usuarioAct) { ?>
                            <div class="col-md-6">
                                <label class="col-md-12"><b>Fecha de Actualización:</b></label>
                            </div>	
                            <div class="col-md-6">
    <?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_act))); ?>
                            </div>				
                        <?php } ?>
                    </div>	

                    <div class="row">
<?php if ($model->estatus == "E") { ?>
                            <div class="col-md-6">
                                <label class="col-md-12"><b>Inhabilitado el:</b></label>

                            </div>
                            <div class="col-md-6">
    <?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime($model->fecha_elim))); ?>			
                            </div>
<?php } ?>
                    </div>


                </div>
            </div>

            <div id="niveles" class="tab-pane">
                <center><h4>Niveles</h4></center>
                <?php
                $l = Modalidad::model()->obtenerNiveles(base64_encode($model->id), Yii::app()->user->id);

                $dataProviderModalidadNivel = new CArrayDataProvider($l, array(
                ));
                ?>

                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'id' => 'modalidad-nivel-view-grid',
    'dataProvider' => $dataProviderModalidadNivel,
    'summaryText' => false,
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'columns' => array(
                        array(
                            'header' => '<center>Nombre</center>',
                            'name' => 'nombre',
                        ),
                    ),
                ));
                ?>


            </div>




        </div>
    </div>

</div>