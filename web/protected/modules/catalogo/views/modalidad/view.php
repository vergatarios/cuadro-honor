<?php
/* @var $this ModalidadController */
/* @var $model Modalidad */

$this->breadcrumbs=array(

	'Modalidades'=>array('index'),

);
/*
$this->menu=array(
	array('label'=>'List Modalidad', 'url'=>array('index')),
	array('label'=>'Create Modalidad', 'url'=>array('create')),
	array('label'=>'Update Modalidad', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Modalidad', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Modalidad', 'url'=>array('admin')),
);
?>

<h1>View Modalidad #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'usuario_ini_id',
		'fecha_ini',
		'usuario_act_id',
		'fecha_act',
		'fecha_elim',
		'estatus',
	),
	

)); */

 $this->renderPartial('_view',array('model'=>$model));
?>
