<?php
/* @var $this ModalidadController */
/* @var $model Modalidad */
/* @var $form CActiveForm */
$this->breadcrumbs = array(
    'Catalogo' => array('/catalogo'),
    'Modalidad' => array('/catalogo/modalidad'),
    'Niveles',
);
echo CHtml::scriptFile('/public/js/modules/catalogo/modalidad.js');
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'modalidad-nivel-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
        ));
?>
<!--Evitar la redireccion-->


<script>
    function noENTER(evt)
    {
        var evt = (evt) ? evt : ((event) ? event : null);
        var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        if ((evt.keyCode == 13) && (node.type == "text"))
        {
            return false;
        }
    }
    document.onkeypress = noENTER;
</script>
<div class="widget-box">

    <div class="widget-header">
        <h5><?php echo CHtml::encode($model->nombre); ?></h5>

        <div class="widget-toolbar">
            <a >
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="form">


        <div class="widget-body">

            <div class="widget-main form">



                <?php
                if ($form->errorSummary($model)):
                    ?>
                    <div id ="div-result-message" class="errorDialogBox" >
                        <?php echo $form->errorSummary($model); ?>
                    </div>
                    <?php
                endif;
                ?>



                <div class="row">


                    <input type="hidden" id='id' name="id" value="<?php echo base64_encode($model->id); ?>" />

                    <div class="col-md-10">
                
                        <label class="col-md-12">Niveles</label>
                        <div class="col-md-12">
                        <?php
                        echo $form->dropDownList($model, 'nivels', CHtml::listData(Nivel::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'")), 'id', 'nombre'), array('required' => 'required', 'empty' => '-SELECCIONE-', 'data-placeholder' => 'Seleccione niveles...', 'class
                            ' => 'span-8')
                        );
                        ?>
                        </div>   
                    </div>
                </div>
                <?php $this->endWidget(); ?>
                <div class="row">

                    <div class="col-md-12">
                        <label  class="col-md-12">Niveles Agregados</label>
                        <div  class="col-md-12">
                            <div id="listaNiveles">			
                                <?php
                                $l = Modalidad::model()->obtenerNiveles(base64_encode($model->id), Yii::app()->user->id);

                                $dataProvider = new CArrayDataProvider($l, array(
                                    'pagination' => array(
                                        'pageSize' => 5,
                                    )
                                ));
                                ?>

                                <?php
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                    'id' => 'modalidad-nivel-grid',
                                    'dataProvider' => $dataProvider,
                                    'summaryText' => false,
                                    'pager' => array(
                                        'header' => '',
                                        'htmlOptions' => array('class' => 'pagination'),
                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                    ),
                                    'columns' => array(
                                        array(
                                            'header' => '<center>Nombre</center>',
                                            'name' => 'nombre',
                                        ),
                                        array(
                                            'type' => 'raw',
                                            'header' => '<center>Acciones</center>',
                                            'value' => array($this, 'columna')
                                        ),
                                    ),
                                ));
                                ?>
                            </div>		
                            <div class="row-fluid-actions">
                                <a class="btn btn-danger" href="/catalogo/modalidad">
                                    <i class="icon-arrow-left bigger-110"></i>
                                    Volver
                                </a>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>

        </div>
    </div>
</div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>



<!-- form -->
<script>

    function quitar(id, modalidad_id) {


        var data =
                {
                    id: id,
                    modalidad_id: modalidad_id
                };

        $("#dialogPantalla").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Esta seguro que desea eliminar este nivel? </p></div>');

        var dialog = $("#dialogPantalla").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Nivel </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-warning btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacion";
                        var urlDir = "/catalogo/modalidad/quitarNivel/";
                        var datos = data;
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function() {
                            $('#modalidad-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };


                        if (datos) {
                            executeAjax('listaNiveles', '/catalogo/modalidad/QuitarNivel', data, true, true, 'GET', '');
                            $(this).dialog("close");
                        }
                    }
                }
            ],
        });


        Loading.hide();




        //VentanaDialog(data,'/catalogo/modalidad/nivel','Eliminar Nivel de Modalidad','borrarNivel');

        //

    }

    function agregar(id_nivel, id_modalidad) {



        $("#dialogPantalla").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Esta seguro que desea Agregar este nivel? </p></div>');

        var dialog = $("#dialogPantalla").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Nivel </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-warning btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                    "class": "btn btn-primary btn-xs",
                    click: function() {
                        var data =
                                {
                                    nivel_id: id_nivel,
                                    id: id_modalidad
                                };


                        if (data) {
                            executeAjax('listaNiveles', '/catalogo/modalidad/cargarNivel', data, true, true, 'GET', '');
                            $(this).dialog("close");
                        }
                    }
                }
            ],
        });


        Loading.hide();




        //VentanaDialog(data,'/catalogo/modalidad/nivel','Eliminar Nivel de Modalidad','borrarNivel');

        //

    }


    $(document).ready(function() {

        $("#Modalidad_nivels").select2({
            allowClear: true
        });


        $("#nombre").keyup(function() {

            $("#nombre").val($("#nombre").val().toUpperCase());

        });

        $("#Modalidad_nivels").bind('change', function() {

            var id_nivel = $("#Modalidad_nivels").val();
            var id_modalidad = $("#id").val();

            if (id_nivel == "" || id_modalidad == "") {
            }
            else {
                id_nivel_encoded = btoa(id_nivel);
                agregar(id_nivel_encoded, id_modalidad);

            }


        });
    });
</script>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/select2.min.js', CClientScript::POS_END);
?>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/select2.css" rel="stylesheet" />