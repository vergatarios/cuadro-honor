

<?php
$this->breadcrumbs = array(
    'Catalogo' => array('../catalogo/'),
    'Asignaturas',
);
?>
<br>
<div class="widget-box">

    <div class="widget-header">
              
                       
        <h5>
           Asignaturas
        </h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
             <i class="icon-chevron-down"></i>  

            </a>
        </div>

    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="">
            <div class="widget-main">
                <div class="col-md-8">
<div id="dialogBoxReg" style="display:none"><?php $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'La Asignatura ha sido inhabilitada exitosamente.'));?></div>
<div id="dialogBoxHab" style="display:none"><?php $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'La Asignatura ha sido Habilitada exitosamente.')); ?></div>
                </div>
                <a href="#" class="search-button"></a>
                <br>
        <div class="pull-right" style="padding-left:10px;">
    <a id="link-new" class="btn btn-success btn-next btn-sm" data-last="Finish" onClick="registrarAsignatura()">
        <i class="fa fa-plus icon-on-right"></i>
        <span class="space-2">Registrar una Nueva Asignatura</span>
    </a>
</div>
                <br>
                  <div class="clearfix margin-5">
<?php
                          
      $data = $model->search('fecha_act DESC, fecha_ini DESC');                          

$this->widget('zii.widgets.grid.CGridView', array(
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'id' => 'asignatura-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => false,
    'pager' => array(
        'header' => '',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
    ),
            'afterAjaxUpdate' => " function(){

                                        $('#Asignatura_abreviatura').bind('keyup blur', function () {
                                             keyAlphaNum(this, true, true);
                                             makeUpper(this);
                                        });
                                        
                                        $('#Asignatura_nombre').bind('keyup blur', function () {
                                             keyDashText(this, true, true);
                                             makeUpper(this);
                                        });
                                    }
                                    
                                ",
    'columns' => array(
        array(
            'header' => '<center>Nombre Asignado</center>',
            'name' => 'nombre',
            'filter' => CHtml::textField('Asignatura[nombre]'),
            
        ),
        
           array(
            'header' => '<center>Abreviatura</center>',
            'name' => 'abreviatura',
            'filter' => CHtml::textField('Asignatura[abreviatura]', null),
        ),
           array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'filter'=> array('A' => 'Activo', 'E' => 'Inactivo'),
            'value'=>array($this, 'columnaEstatus'),
        ),
         
        array('type' => 'raw',
            'header' => '<center>Acciones</center>',
            'value' => array($this, 'columnaAcciones'),
        ),
    ),
));
?>
            </div>
        </div>
        <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div> 


    </div>
</div>

</div>
<div class="row">

    <div class="col-md-6">
        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("catalogo"); ?>" class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>

    <!-- <div class="col-md-6 wizard-actions">
         <button type="submit" data-last="Finish" class="btn btn-primary btn-next">
             Guardar
             <i class="icon-save icon-on-right"></i>
         </button>
     </div>
    -->

</div>

<div id="dialogPantalla" class="hide" ></div>
<div id="dialogEliminar" class="hide" ><?php $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Inhabilitar esta Asignatura? </p>'));?></div>
<div id="dialogReactivar" class="hide" ><?php $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Habilitar Nuevamente esta Asignatura? </p>'));?></div>

<?php 
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/asignatura.js',CClientScript::POS_END);
?>
