<?php

/* @var $this EstatusDocenteController */
/* @var $model EstatusDocente */


$this->breadcrumbs=array(
    'Catalogo'=>array('/catalogo'),
    'Estatus Personal'
);

$this->pageTitle = 'Administración de Estatus Personal';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Estatus Personal</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Estatus Personal.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/estatusDocente/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Estatus Personal                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'estatus-docente-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
$('#EstatusDocente_nombre').unbind('keyup');
    $('#EstatusDocente_nombre').bind('keyup', function() {
        keyAlphaNum(this,true,false);
        makeUpper(this);
    });
    $('#EstatusDocente_nombre').unbind('blur');
    $('#EstatusDocente_nombre').bind('blur', function() {
        clearField(this);
    });
                }",
	'columns'=>array(
//        array(
//            'header' => '<center>idaaaa</center>',
//            'name' => 'id',
//            'htmlOptions' => array(),
//            //'filter' => CHtml::textField('EstatusDocente[id]', $model->id, array('title' => '',)),
//        ),
        array(
            'header' => '<center>Nombre</center>',
            'name' => 'nombre',
            'htmlOptions' => array('readonly'=>true),
            'filter' => CHtml::textField('EstatusDocente[nombre]', $model->nombre, array('title' => '','maxlength'=>'35')),
        ),
        array(
            'header' => '<center>Fecha de Creación</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
          'filter' => CHtml::textField('EstatusDocente[fecha_ini]', $model->fecha_ini, array('title' => '','readonly'=>true)),
        ),
//        array(
//            'header' => '<center>usuario_ini_id</center>',
//            'name' => 'usuario_ini_id',
//            'htmlOptions' => array(),
//            //'filter' => CHtml::textField('EstatusDocente[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
//        ),
//        array(
//            'header' => '<center>usuario_act_id</center>',
//            'name' => 'usuario_act_id',
//            'htmlOptions' => array(),
//            //'filter' => CHtml::textField('EstatusDocente[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
//        ),
        array(
            'header' => '<center>Fecha de Actualización</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('EstatusDocente[fecha_act]', $model->fecha_act, array('title' => '','readonly'=>true)),
        ),
		/*
        array(
            'header' => '<center>fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('EstatusDocente[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),
		*/
        /*array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('EstatusDocente[estatus]', $model->estatus, array('title' => '',)),
        ),*/

        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'value' => array($this, 'estatus'),
            'filter' => array('A' => 'Activo', 'E' => 'Eliminado', 'I' => 'Inactivo'),
            //'filter' => CHtml::textField('Noticia[estatus]', $model->estatus, array('title' => '',)),
        ),
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>
<script src="/public/js/modules/catalogo/estatusDocente/form.js"></script>
