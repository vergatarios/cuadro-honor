<?php
/* @var $this GradoInstruccionController */
/* @var $model GradoInstruccion */

$this->pageTitle = 'Registro de Grado Instruccions';

      $this->breadcrumbs=array(
	'Grado Instruccions'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>