<?php
/* @var $this GradoInstruccionController */
/* @var $model GradoInstruccion */

$this->pageTitle = 'Actualización de Datos de Grado Instruccions';

      $this->breadcrumbs=array(
	'Grado Instruccions'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>