<?php
$this->breadcrumbs = array(
    'Catalogos' => array('/catalogo/'),
    'Planes de Estudio' => array('/catalogo/planEstudio/'),
    'Modificación'
);
?>
<script>
    $(document).ready(function() {
        $("#Plan_credencial_id,#Plan_mencion_id, #Plan_fund_juridico_id, #Plan_nivel_id, #nivel_id").select2({
            allowClear: true
        });
    });
</script>
<div class="form">

    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos generales</a></li>
            <li id="asignaturasGrados"><a href="#asignaturasPlan"  data-toggle="tab">Asignaturas</a></li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="datosGenerales">
                <div class="infoDialogBox">
                    <p>
                        Debe Ingresar los Datos Generales del Plan de Estudio. Los campo marcados con <strong class="required">*</strong> son requeridos.
                    </p>
                </div>
                <div id="errorSummary" class="hide">
                </div>
                <div  id="" class="widget-box">
                    <div class="widget-header" style="border-width: 1px">
                        <h5>Datos Generales</h5>
                        <div class="widget-toolbar">
                            <a  href="#" data-action="collapse">
                                <i class="icon-chevron-up"></i>
                            </a>
                        </div>
                    </div>

                    <div id="" class="widget-body" >
                        <div class="widget-body-inner" >
                            <div class="widget-main form">
                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'plan-estudio-form',
                                    'enableAjaxValidation' => false,
                                    'enableClientValidation' => true,
                                    'clientOptions' => array(
                                        //  'validateOnSubmit' => true,
                                        'validateOnType' => true,
                                        'validateOnChange' => true),
                                ));
                                echo CHtml::hiddenField('plan_id', $plan_id);
                                ?>

                                <div class="row">

                                    <div id="1raFila" class="col-md-12">
                                        <div class="col-md-6" style="margin: 0px;line-height: 155%;">
                                            <?php echo $form->labelEx($model, 'cod_plan', array("class" => "col-md-12")); ?>
                                            <?php echo $form->textField($model, 'cod_plan', array('class' => 'span-4')); ?>

                                            <div class="row col-md-12" ><?php echo $form->error($model, 'cod_plan'); ?> </div>
                                        </div>
                                    </div>

                                    <div id="2daFila" class="col-md-12">
                                        <div class="col-md-6" style="margin: 0px;line-height: 155%;">
                                            <?php echo $form->labelEx($model, 'nombre', array("class" => "col-md-12")); ?>
                                            <?php echo $form->textField($model, 'nombre', array('class' => 'span-7')); ?>
                                            <div class="row col-md-12" ><?php echo $form->error($model, 'nombre'); ?> </div>
                                        </div>

                                        <div class="col-md-6" style="margin: 0px;line-height: 155%;">
                                            <?php echo $form->labelEx($model, 'credencial_id', array("class" => "col-md-12")); ?>
                                            <?php echo $form->dropDownList($model, 'credencial_id', CHtml::listData($dropDownCredencial, 'id', 'nombre'), array('class' => 'span-7', 'empty' => '-Seleccione-', 'style' => "width:91.5%; ")); ?>
                                            <div class="row col-md-12" ><?php echo $form->error($model, 'credencial_id'); ?> </div>
                                        </div>
                                    </div>

                                    <div id="3raFila" class="col-md-12">

                                        <div class="col-md-6" style="margin: 0px;line-height: 155%;">
                                            <?php echo $form->labelEx($model, 'fund_juridico_id', array("class" => "col-md-12")); ?>
                                            <?php echo $form->dropDownList($model, 'fund_juridico_id', CHtml::listData($dropDownFundJuridico, 'id', 'nombre'), array('class' => 'span-7', 'empty' => '-Seleccione-', 'style' => "width:91.5%; ")); ?>
                                            <div class="row col-md-12" ><?php echo $form->error($model, 'fund_juridico_id'); ?> </div>
                                        </div>

                                        <div class="col-md-6" style="margin: 0px;line-height: 155%;">
                                            <?php echo $form->labelEx($model, 'mencion_id', array("class" => "col-md-12")); ?>
                                            <?php echo $form->dropDownList($model, 'mencion_id', CHtml::listData($dropDownMencion, 'id', 'nombre'), array('class' => 'span-7', 'empty' => '-Seleccione-', 'style' => "width:91.5%; ")); ?>
                                            <div class="row col-md-12" ><?php echo $form->error($model, 'mencion_id'); ?> </div>
                                        </div>

                                    </div>
                                    <div id="4taFila" class="col-md-12">


                                        <div class="col-md-6" style="margin: 0px;line-height: 155%;">
                                            <?php echo $form->labelEx($model, 'tipo_documento_id', array("class" => "col-md-12")); ?>
                                            <?php echo $form->dropDownList($model, 'tipo_documento_id', CHtml::listData($dropDownTipoDocumento, 'id', 'nombre'), array('class' => 'span-7', 'empty' => '-Seleccione-', 'style' => "width:91.5%; ")); ?>
                                            <div class="row col-md-12" ><?php echo $form->error($model, 'tipo_documento_id'); ?> </div>
                                        </div>

                                    </div>
                                </div>
                                <?php $this->endWidget(); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <br>
<!--                <div  id="" class="widget-box ">
                    <div class="widget-header" style="border-width: 1px">
                        <h5>Nivel Inicial del Plan de Estudio</h5>
                        <div class="widget-toolbar">
                            <a  href="#" data-action="collapse">
                                <i class="icon-chevron-up"></i>
                            </a>
                        </div>
                    </div>-->

<!--                    <div id="" class="widget-body" >
                        <div class="widget-body-inner" >
                            <div class="widget-main form">
                                <div class="row">
                                    <div class="infoDialogBox">
                                        <p>
                                            Inicialmente debe asociar un Nivel de Educación a este Plan de Estudio para facilitar el registro de asignaturas al mismo. 
                                        </p>
                                    </div>
                                    <div id="" class="col-md-12">
                                        <div class="col-md-6" style="margin: 0px;line-height: 155%;">
                                            <?php echo $form->labelEx($model, 'nivel_id', array("class" => "col-md-12")); ?>
                                            <?php echo $form->dropDownList($model, 'nivel_id', CHtml::listData($dropDownNivel, 'id', 'nombre'), array('class' => 'span-7', 'empty' => '-Seleccione-', 'style' => "width:91.5%; ")); ?>
                                            <div class="row col-md-12" ><?php echo $form->error($model, 'nivel_id'); ?> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
                <br>
                <div  id="wBoxGrados" class="widget-box hide">
                    <div class="widget-header" style="border-width: 1px">
                        <h5>Grados</h5>
                        <div class="widget-toolbar">
                            <a  href="#" data-action="collapse">
                                <i class="icon-chevron-up"></i>
                            </a>
                        </div>
                    </div>

                    <div id="" class="widget-body" >
                        <div class="widget-body-inner" >
                            <div class="widget-main form">
                                <div class="row">
                                    <div id="gridGrados" class="col-md-12">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <a id="btnRegresar" href="  <?php echo Yii::app()->createUrl("catalogo/planEstudio/"); ?>" class="btn btn-danger">
                            <i class="icon-arrow-left"></i>
                            Volver
                        </a>
                    </div>
                    <div class="col-md-6 wizard-actions">
                        <button id="btnModificarPlan" type="submit" data-last="Finish" class="btn btn-primary btn-next">
                            Actualizar
                            <i class="fa fa-exchange icon-on-right"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="asignaturasPlan">

                <div  class="col-md-12">
                    <div class="infoDialogBox">
                        <p>
                            Estimado usuario, seleccione el <strong >Nivel</strong> para desplegar los grados asociados al mismo y asi poder modificar las <strong>Asignaturas</strong>
                        </p>
                    </div>
                    <div  id="wBoxNivelesPlan" class="widget-box">
                        <div class="widget-header" style="border-width: 1px">
                            <h5>Niveles del Plan de Estudio <?php print($model->cod_plan); ?></h5>
                            <?php echo CHtml::hiddenField('codigo_plan',$model->cod_plan);?>
                            <div class="widget-toolbar">
                                <a  href="#" data-action="collapse">
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div id="" class="widget-body" >
                            <div class="widget-body-inner" >
                                <div class="widget-main form">
                                    <div class="row">
                                        <div class="col-md-6" style="margin: 0px;line-height: 155%;">
                                            <?php echo CHtml::label('Nivel', 'nivel_id', array("class" => "col-md-12")); ?>
                                            <?php echo CHtml::dropDownList('nivel_id', '', CHtml::listData($dropDownNivel, 'id', 'nombre'), array('class' => 'span-7', 'empty' => '-Seleccione-', 'style' => "width:91.5%; ")); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="gradosAsignaturas"></div>

                </div>

                <div class="space-6"></div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <a id="btnRegresar" href="  <?php echo Yii::app()->createUrl("catalogo/planEstudio/"); ?>" class="btn btn-danger">
                            <i class="icon-arrow-left"></i>
                            Volver
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dialogos">

    <div id = "dialog_error" class="hide">
        <div class="alertDialogBox bigger-110">
            <p class="bigger-110 bolder center grey">

            </p>
        </div>
    </div>
    <div id = "dialog_success" class="hide">
        <div class="successDialogBox bigger-110">
            <p class="bigger-110 bolder center grey">

            </p>
        </div>
    </div>

</div>
<div id="css_js">
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/planes.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/select2.min.js', CClientScript::POS_END);
    ?>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/select2.css" rel="stylesheet" />
</div>

