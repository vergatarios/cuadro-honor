
<div  id="wBoxAsignaturasGrados" class="widget-box hide">
    <div class="widget-header" style="border-width: 1px">
        <h5>Establecer Asignaturas a Grados</h5>
        <div class="widget-toolbar">
            <a  href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div id="" class="widget-body" >
        <div class="widget-body-inner" >
            <div class="widget-main form">
                <div class="row">
                    <div id="gridAsignaturasGrados" class="col-md-12">
                        <div class="col-md-offset-3 col-md-6">
                            <?php
                            if (isset($dataProvider) && $dataProvider !== array() && $dataProvider !== null)
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                    'id' => 'asignaturasGrados-grid',
                                    'dataProvider' => $dataProvider,
                                    //'filter' => $model,
                                    'pager' => array('pageSize' => 1),
                                    'summaryText' => false,
                                    'columns' => array(
                                        array(
                                            'name' => 'nombre',
                                            'type' => 'raw',
                                            'header' => '<center>Nombre del Grado</center>'
                                        ),
                                        array(
                                            'name' => 'boton',
                                            'type' => 'raw',
                                            'header' => '<center>Acciones</center>',
                                            'htmlOptions' => array('nowrap' => 'nowrap', 'width' => '15%'),
                                        ),
                                    ),
                                    'pager' => array(
                                        'header' => '',
                                        'htmlOptions' => array('class' => 'pagination'),
                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                    ),
                                ));

                            else {
                                ?>
                            <div class="infoDialogBox">
                                    <p>
                                        Estimado usuario, dicho Plan de Estudio no posee Grados o se presento un error al intentar obtener los Grados del Plan de Estudio.
                                    </p>
                                </div>

                                <?php
                            }
                            ?>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

</div>
<div id="dialog_agregarAsignatura" class="hide"></div>
<script type="text/javascript">
    $(document).ready(function() {
        var codigo_plan = "<? (isset($codigo_plan)) ? print($codigo_plan):print('')?>";
        var nivel_id, plan_id, grado_id, divResult, urlDir, conEfecto, showHTML, method, callback;
        $('.edit-data-grado').unbind('click');
        $('.edit-data-grado').on('click',
                function(e) {
                    e.preventDefault();
                    plan_id = $(this).attr('data-plan');
                    nivel_id = $(this).attr('data-nivel');
                    grado_id = $(this).attr('data-id');
                    var data = {
                        nivel_id: nivel_id,
                        plan_id: plan_id,
                        grado_id: grado_id,
                    };

                    divResult = 'dialog_agregarAsignatura';
                    urlDir = '/catalogo/planEstudio/condicionarGrado/';
                    conEfecto = true;
                    showHTML = true;
                    method = 'get';
                    callback = function() {
                        divResultInterno = 'materiasAsignadas';
                        urlDirInterno = 'buscarAsignaturas';
                        datosInterno = {
                            grado_id: grado_id,
                            plan_id: plan_id,
                        };
                        conEfectoInterno = true;
                        showHTMLInterno = true;
                        methodInterno = 'get';
                        executeAjax(divResultInterno, urlDirInterno, datosInterno, conEfectoInterno, showHTMLInterno, methodInterno);
                        $("#" + divResultInterno).removeClass('hide');
                        $("#cod_planSpan").html(' Plan de Estudio '+base64_decode(codigo_plan));
                        
                        
                    };
                    executeAjax(divResult, urlDir, data, conEfecto, showHTML, method, callback);
                    var dialog_agregarAsignatura = $("#dialog_agregarAsignatura").removeClass('hide').dialog({
                        modal: true,
                        width: '900px',
                        position: ['center', 50],
                        draggable: false,
                        resizable: false,
                        title: "<div class='widget-header widget-header-small'><h4 class='smaller orange'><i class='fa fa-book orange'></i> Asignaturas </h4></div>",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                                "class": "btn btn-xs",
                                click: function() {
                                    $("#dialog_agregarAsignatura p").html('');
                                    $(this).dialog("close");

                                }
                            }
                        ]
                    });
                }
        );
    });
</script>
