<div class="widget-box">

    <div class="widget-header" style="border-width: thin">
        <h4 id="nomPlan">Asignaturas<?php echo isset($data) ? ': ' . $data : "" ?></h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>
    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
                <div class="row">  
                    <div class="space-6"></div>
                    <div class="col-md-12">
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                            'id' => 'asignaturas-grid',
                            'dataProvider' => $dataProvider,
                            //'filter' => $model,
                            'pager' => array('pageSize' => 1),
                            'summaryText' => false,
                            'columns' => array(
                                array(
                                    'name' => 'nombre',
                                    'type' => 'raw',
                                    'header' => '<center>Nombre de la Asignatura</center>'
                                ),
                            ),
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

