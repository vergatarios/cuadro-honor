<div class="tab-pane active" id="agregarAutoridad">
    <div id="wBoxAgregarAsignaturas" class="widget-box">
        <div id="div-result-message" class="hide">
        </div>
        <div class="widget-header" style="border-width: thin">
            <h5>Establecer Asignatura</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#" >
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>
        <div id="agregarAsignatura" class="widget-body" >
            <div class="widget-body-inner">
                <div class="widget-main form">

                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'action' => Yii::app()->createUrl($this->route),
                        'method' => 'get',
                    ));
                    ?>
                    <?php echo CHtml::hiddenField('asignatura_nivel_id', $nivel_id); ?>
                    <?php echo CHtml::hiddenField('plan_id', $plan_id); ?>
                    <?php echo CHtml::hiddenField('grado_id', $grado_id);
                    ?>
                    <div class="row">
                        <div id="1eraFila" class="col-md-12 row">
                            <div class="col-md-6" >
                                <?php echo CHtml::label('Asignatura', 'asignatura', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::dropDownList('asignatura', '', CHtml::listData($dropDownAsignatura, 'id', 'nombre'), array('empty' => '-Seleccione-', 'title' => 'Seleccione la Asignatura que desea agregar', 'class' => 'tooltipPlanesAsignaturas span-7'));
                                ?>
                            </div>
                            <div id ="divAvaladoMppe"class="col-md-3 center hide" >
                                <?php echo CHtml::label('¿Avalado por MPPE?', 'avalado', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::checkBox('avalado', true, array('class' => 'tooltipPlanesAsignaturas', 'title' => 'La Asignatura es avalada por el MPPE?'));
                                ?>
                            </div>
                            <div id ="divPredecesor" class="col-md-3 hide center">
                                <?php echo CHtml::label('¿Tiene Prelación?', 'predecesor', array("class" => "col-md-12")); ?>
                                <?php echo CHtml::checkBox('predecesor', false, array('class' => 'tooltipPlanesAsignaturas', 'title' => 'Esta Asignatura requiere que se haya aprobado otra anteriormente?'));
                                ?>
                            </div>
                        </div>
                    </div>
                    <div id="wBoxPrelacion" class="widget-box hide">
                        <div class="widget-header " id="wHeaderPrelacion" style="border-width: thin">
                            <h5>Búsqueda de Asignaturas de Prelación</h5>
                            <div class="widget-toolbar">
                                <a data-action="collapse" href="#" >
                                    <i class="icon-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body" id="wBodyPrelacion" >
                            <div class="widget-body-inner" >
                                <div class="widget-main form">
                                    <div class="row">
                                        <div  class="col-md-12 row ">
                                            <div id="divCod_plan" class="hide">
                                                <div  class="col-md-5 " >
                                                    <?php echo CHtml::label('Código de Plan', 'cod_plan', array("class" => "col-md-12")); ?>
                                                    <?php echo CHtml::telField('cod_plan', '', array('empty' => '-Seleccione-', 'class' => 'span-7 tooltipPlanesAsignaturas', 'title' => 'Ingrese el código del plan en el cual se encuentra la asignatura que es requerida.'));
                                                    ?>
                                                </div>
                                                <div class="col-md-2 ">
                                                    <div class="col-md-12" style="margin-right: 15px; margin-top: 25%;">
                                                        <button class="btn btn-primary btn-next btn-sm" data-last="Finish" type="submit" id="btnBuscarPlan">
                                                            Buscar Plan
                                                            <i class="icon-search  icon-on-right"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div  class="col-md-12 row">
                                            <div  id="divAsignaturaPre" class="hide">
                                                <div id="gradoPredecesor" class="col-md-5 " >
                                                    <?php echo CHtml::label('Grado Prelación', 'gradoPre', array("class" => "col-md-12")); ?>
                                                    <?php echo CHtml::dropDownList('gradoPre', '', CHtml::listData($dropDownGrado, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7 tooltipPlanesAsignaturas', 'title' => 'Seleccione el grado donde se encuentra la asignatura que es requerida.'));
                                                    ?>
                                                </div>
                                                <div id="asignaturaPredecesor" class="col-md-5 " >
                                                    <?php echo CHtml::label('Asignatura Prelación', 'asignaturaPre', array("class" => "col-md-12")); ?>
                                                    <?php
                                                    echo CHtml::dropDownList('asignaturaPre', '', CHtml::listData($dropDownAsignatura, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7 tooltipPlanesAsignaturas', 'title' => 'Seleccione la asignatura que es requerida.'));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="space-6"><div class="row col-md-12"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="space-6"><div class="row col-md-12"></div></div>


                    </div>
                    <div class="space-6"><div class="row col-md-12"></div></div>
                    <div class="row">
                        <div id="divBtnAgregarAsignatura" class="hide">
                            <div class="col-md-12 row">
                                <div class="col-md-4">
                                    <label for="asignatura" class="col-md-12">Horas Te&oacute;ricas <span class="required">*</span></label>
                                    <input id="hora_teorica" value="" type="text" class="form-control" />
                                </div>
                                <div class="col-md-4">
                                    <label for="asignatura" class="col-md-12">Horas Practicas</label>
                                    <input id="hora_practica" value="" type="text" class="form-control" />
                                </div>
                                <div class="pull-right col-md-3">
                                    <div class="col-md-12" style="margin-right: 15px;">
                                        <label for="asignatura" class="col-md-12">&nbsp;</label>
                                        <button class="btn btn-success btn-next btn-sm" data-last="Finish" type="submit" id="btnAgregarAsignatura">
                                            Establecer Asignatura
                                            <i class="icon-plus  icon-on-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>


        </div>
    </div>
    <div id="wBoxPrelacion" class="widget-box">
        <div class="widget-header " id="wHeaderPrelacion" style="border-width: thin">
            <h5>Materias Asignadas<?php (isset($grado)) ? print(': ' . $grado) : print(''); ?> <span id = "cod_planSpan"></span></h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#" >
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="widget-body " id="wBodyPrelacion" >
            <div class="widget-body-inner" >
                <div class="widget-main form">
                    <div class="row">
                        <div id="materiasAsignadas" class=""></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".tooltipPlanesAsignaturas").tooltip({
                show: {
                    effect: "slideDown",
                    delay: 250
                }
            });
            $("#asignatura").on("change", function() {
                var asignatura = $("#asignatura option:selected").val();
                if (asignatura !== null && asignatura !== '') {
                    $("#divPredecesor").removeClass('hide');
                    $("#divAvaladoMppe").removeClass('hide');
                    $("#divBtnAgregarAsignatura").removeClass('hide');
                    $("#predecesor").attr('checked', false);
                    $("#avalado").attr('checked', true);


                }
                else {
                    $("#divBtnAgregarAsignatura").addClass('hide');
                    $("#divAsignaturaPre").addClass('hide');
                    $("#predecesor").attr('checked', false);
                    $("#avalado").attr('checked', false);
                    $("#asignaturaPre").val('');
                    $("#gradoPre").val('');
                    $("#div-result-message").addClass('hide');
                    $("#div-result-message").html('');
                }

            });
            $("#predecesor").on("click", function() {
                var isChecked = $("#predecesor").attr('checked');
                if (isChecked) {
                    $("#divCod_plan").removeClass('hide');
                    $("#wBoxPrelacion").removeClass('hide');
                }
                else {
                    $("#wBoxPrelacion").addClass('hide');
                    $("#divCod_plan").addClass('hide');
                    $("#asignaturaPre").val('');
                    $("#gradoPre").val('');
                    $("#div-result-message").addClass('hide');
                    $("#div-result-message").html('');
                }
            });

            $("#btnBuscarPlan").unbind('click');
            $("#btnBuscarPlan").click(function(e) {
                e.preventDefault();
                var mensaje = 'Estimado Usuario, si tilda la opción <strong>"Tiene Prelación</strong> debe especificar los siguientes campos : <br>';
                var cod_plan = $("#cod_plan").val();
                var cod_plan_encoded = base64_encode(cod_plan);
                var divResult = 'gradoPre';
                var urlDir = '/catalogo/planEstudio/buscarGradosPlan/';
                var datos = {cod_plan: cod_plan_encoded};
                var conEfecto = true;
                var showHTML = true;
                var method = 'POST';
                var isValid = true;
                var mensaje = '';
                var callback = function() {
                };

                if (cod_plan == '' || cod_plan == null) {
                    mensaje = mensaje + '<p> <strong>*</strong> El Campo Código de Plan no puede estar vacio.</p>';
                    isValid = false;
                }

                if (isValid) {
                    $.ajax({
                        type: method,
                        url: urlDir,
                        dataType: "html",
                        data: datos,
                        beforeSend: function() {
                            if (conEfecto) {
                                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                                displayHtmlInDivId(divResult, url_image_load);
                            }
                            // mostrarNotificacion();
                            peticionActiva = true;
                        },
                        afterSend: function() {
                            peticionActiva = false;
                        },
                        success: function(resp, textStatus, jqXHR) {
                            try {
                                var json = jQuery.parseJSON(jqXHR.responseText);
                                //refrescarGrid();
                                if (json.statusCode == 'success') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'exito', json.mensaje);
                                    urlDir = '/catalogo/planEstudio/buscarAsignaturas/';
                                    datos = {grado_id: json.grado, plan_id: json.plan};
                                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method);
                                    $("#" + divResult).removeClass('hide');
                                }
                                else if (json.statusCode == 'error') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'error', json.mensaje);
                                }
                            } catch (e) {
                                if (showHTML) {
                                    $("#div-result-message").addClass('hide');
                                    $("#div-result-message").html('');
                                    $("#divAsignaturaPre").removeClass('hide');
                                    displayHtmlInDivId(divResult, resp, conEfecto);
                                }
                            }
                        },
                        statusCode: {
                            404: function() {
                                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            },
                            400: function() {
                                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
                            },
                            401: function() {
                                displayDialogBox(divResult, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            403: function() {
                                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            500: function() {
                                if (typeof callback == "function")
                                    callback.call();
                                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            },
                            503: function() {
                                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            },
                            999: function(resp) {
                                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            //alert(thrownError);
                            if (xhr.status == '401') {
                                document.location.href = "http://" + document.domain + "/";
                            } else if (xhr.status == '400') {
                                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            } else if (xhr.status == '500') {
                                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            } else if (xhr.status == '503') {
                                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            }
                            else if (xhr.status == '999') {
                                displayDialogBox('dialog_asignarPlan', "error", xhr.status + ': ' + xhr.responseText);
                            }
                        }

                    });
                }
                else {
                    $("#div-result-message").removeClass('hide');
                    displayDialogBox('div-result-message', 'error', mensaje);
                }
            });
            $("#gradoPre").on("change", function() {
                var mensaje = 'Estimado Usuario, si tilda la opción <strong>"Tiene Prelación</strong> debe especificar los siguientes campos : <br>';
                var grado_pre = $("#gradoPre").val();
                var cod_plan = $("#cod_plan").val();
                var grado_pre_encoded = base64_encode(grado_pre);
                var cod_plan_encoded = base64_encode(cod_plan);
                var divResult = 'asignaturaPre';
                var urlDir = '/catalogo/planEstudio/buscarAsignaturasPlan/';
                var datos = {grado_pre: grado_pre_encoded, cod_plan: cod_plan_encoded};
                var conEfecto = true;
                var showHTML = true;
                var method = 'POST';
                var isValid = true;
                var mensaje = '';
                var callback = function() {
                };

                if (grado_pre != '' && grado_pre != null) {
                    $.ajax({
                        type: method,
                        url: urlDir,
                        dataType: "html",
                        data: datos,
                        beforeSend: function() {
                            if (conEfecto) {
                                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                                displayHtmlInDivId(divResult, url_image_load);
                            }
                            // mostrarNotificacion();
                            peticionActiva = true;
                        },
                        afterSend: function() {
                            peticionActiva = false;
                        },
                        success: function(resp, textStatus, jqXHR) {
                            try {
                                var json = jQuery.parseJSON(jqXHR.responseText);
                                //refrescarGrid();
                                if (json.statusCode == 'success') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'exito', json.mensaje);
                                    urlDir = '/catalogo/planEstudio/buscarAsignaturas/';
                                    datos = {grado_id: json.grado, plan_id: json.plan};
                                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method);
                                    $("#" + divResult).removeClass('hide');
                                }
                                else if (json.statusCode == 'error') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'error', json.mensaje);
                                }
                            } catch (e) {
                                if (showHTML) {
                                    $("#div-result-message").addClass('hide');
                                    $("#div-result-message").html('');
                                    $("#divAsignaturaPre").removeClass('hide');
                                    displayHtmlInDivId(divResult, resp, conEfecto);
                                }
                            }
                        },
                        statusCode: {
                            404: function() {
                                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            },
                            400: function() {
                                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
                            },
                            401: function() {
                                displayDialogBox(divResult, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            403: function() {
                                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            500: function() {
                                if (typeof callback == "function")
                                    callback.call();
                                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            },
                            503: function() {
                                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            },
                            999: function(resp) {
                                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            //alert(thrownError);
                            if (xhr.status == '401') {
                                document.location.href = "http://" + document.domain + "/";
                            } else if (xhr.status == '400') {
                                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            } else if (xhr.status == '500') {
                                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            } else if (xhr.status == '503') {
                                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            }
                            else if (xhr.status == '999') {
                                displayDialogBox('dialog_asignarPlan', "error", xhr.status + ': ' + xhr.responseText);
                            }
                        }

                    });
                }
            });
            $("#btnAgregarAsignatura").unbind('click');
            $("#btnAgregarAsignatura").click(function(e) {
                e.preventDefault();
                var divResultMensaje = 'div-result-message';
                if($("#hora_teorica").val() == ''){
//                    alert();
                    var mensaje = 'Estimado Usuario, debe especificar el siguiente campo:<br>';
                    mensaje = mensaje + '<p> <strong>*</strong> El campo Horas Teóricas no puede estar vacio.</p>';
                    isValid = false;
                    $("#" + divResultMensaje).removeClass('hide');
                    displayDialogBox(divResultMensaje, 'error', mensaje);
                }
                else{
                $("#div-result-message").addClass('hide');
                $("#div-result-message").html('');
                var grado_id = $("#grado_id").val();
                var hora_teorica = $("#hora_teorica").val();
                var hora_practica = $("#hora_practica").val();
                var plan_id = $("#plan_id").val();
                var avalado = $("#avalado").val();
                var nivel_id = $("#asignatura_nivel_id").val();
                var cod_plan = $("#cod_plan").val();
                var asignatura = $("#asignatura").val();
                var asignaturaPre = $("#asignaturaPre").val();
                var gradoPre = $("#gradoPre").val();
                var isCheckedPrelacion = $("#predecesor").attr('checked');
                var isCheckedAvalado = $("#avalado").attr('checked');
                var mensaje = 'Estimado Usuario, si tilda la opción <strong>"Tiene Prelación</strong> debe especificar los siguientes campos:<br>';
                var divResult = 'materiasAsignadas';
//                var divResultMensaje = 'div-result-message';
                var urlDir = '/catalogo/planEstudio/agregarAsignatura/';
                var datos = {
                    grado_id: grado_id,
                    hora_teorica: hora_teorica,
                    hora_practica: hora_practica,
                    plan_id: plan_id,
                    nivel_id: nivel_id,
                    asignatura_id: base64_encode(asignatura),
                    asignatura_pre_id: base64_encode(asignaturaPre),
                    grado_pre_id: base64_encode(gradoPre),
                    avalado: base64_encode(isCheckedAvalado),
                    prelacion: base64_encode(isCheckedPrelacion),
                    cod_plan: base64_encode(cod_plan)
                };
                var conEfecto = true;
                var showHTML = true;
                var method = 'POST';
                var isValid = true;
                var callback = function() {
                };
                if (isCheckedPrelacion) {
                    if (gradoPre == '' || gradoPre == null) {
                        mensaje = mensaje + '<p> <strong>*</strong> El Campo Grado Prelación no puede estar vacio.</p>';
                        isValid = false;
                    }
                    if (asignaturaPre == '' || asignaturaPre == null) {
                        mensaje = mensaje + '<p> <strong>*</strong> El Campo Asignatura Prelación no puede estar vacio.</p>';
                        isValid = false;
                    }
                }
                if (isValid) {
                    $.ajax({
                        type: method,
                        url: urlDir,
                        dataType: "html",
                        data: datos,
                        beforeSend: function() {
                            if (conEfecto) {
                                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                                displayHtmlInDivId(divResultMensaje, url_image_load);
                            }
                            // mostrarNotificacion();
                            peticionActiva = true;
                        },
                        afterSend: function() {
                            peticionActiva = false;
                        },
                        success: function(resp, textStatus, jqXHR) {
                            try {
                                var json = jQuery.parseJSON(jqXHR.responseText);
                                $("#hora_teorica").val('');
                                $("#hora_practica").val('');
                                //refrescarGrid();
                                if (json.statusCode == 'success') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox(divResultMensaje, 'exito', json.mensaje);
                                    urlDir = '/catalogo/planEstudio/buscarAsignaturas/';
                                    datos = {grado_id: json.grado, plan_id: json.plan};
                                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method);
                                    $("#" + divResult).removeClass('hide');
                                    urlDirAsignaturas = '/catalogo/planEstudio/buscarAsignaturasDisponibles/';
                                    datos = {asignaturas: json.asignaturas};
                                    executeAjax('asignatura', urlDirAsignaturas, datos, conEfecto, showHTML, method);
                                }
                                else if (json.statusCode == 'error') {
                                    $("#" + divResultMensaje).removeClass('hide');
                                    displayDialogBox(divResultMensaje, 'error', json.mensaje);
                                }
                            } catch (e) {
                                if (showHTML) {
                                    displayHtmlInDivId(divResultMensaje, resp, conEfecto);
                                }
                            }
                        },
                        statusCode: {
                            404: function() {
                                displayDialogBox(divResultMensaje, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            },
                            400: function() {
                                displayDialogBox(divResultMensaje, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
                            },
                            401: function() {
                                displayDialogBox(divResultMensaje, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            403: function() {
                                displayDialogBox(divResultMensaje, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            500: function() {
                                if (typeof callback == "function")
                                    callback.call();
                                displayDialogBox(divResultMensaje, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            },
                            503: function() {
                                displayDialogBox(divResultMensaje, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            },
                            999: function(resp) {
                                displayDialogBox(divResultMensaje, "error", resp.status + ': ' + resp.responseText);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            //alert(thrownError);
                            if (xhr.status == '401') {
                                document.location.href = "http://" + document.domain + "/";
                            } else if (xhr.status == '400') {
                                displayDialogBox(divResultMensaje, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            } else if (xhr.status == '500') {
                                displayDialogBox(divResultMensaje, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            } else if (xhr.status == '503') {
                                displayDialogBox(divResultMensaje, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            }
                            else if (xhr.status == '999') {
                                displayDialogBox('dialog_asignarPlan', "error", xhr.status + ': ' + xhr.responseText);
                            }
                        }

                    });
                }
                else {
                    $("#" + divResultMensaje).removeClass('hide');
                    displayDialogBox(divResultMensaje, 'error', mensaje);
                }
                }
            });
        });
    </script>





