<?php
$this->breadcrumbs = array(
    'Catalogo' => array('/catalogo'),
    'Planes de Estudio' => array('/catalogo/planEstudio'),
    'Consultar Plan de Estudio',
);
?>
<div class="widget-box">

    <?php
    Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function(){
		$('#grados-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
	");
    ?>

    <div class="widget-header" style="border-width: thin">
        <h4>Plan de Estudio</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
                <div class="row">
                    <div class="space-6"></div>
                    <div class="col-lg-5">
                        <div class="row">
                            <?php
                            $credencial = (is_object($data->credencial) && isset($data->credencial->nombre)) ? $data->credencial->nombre : "#No_Posee";
                            $mencion = (is_object($data->mencion) && isset($data->mencion->nombre)) ? $data->mencion->nombre : "#No_Posee";
                            //$fund_juridico = (is_object($data->mencion) && isset($data->mencion->nombre)) ? $data->mencion->nombre : "#No_Posee";
                            ?>
                            <label for="Plan_nombre" class="span-7"><b>Nombre del Plan</b></label>
                            <label class="span-7">
                                <input type="text" value="<?php echo $data->nombre; ?>" class="span-11" disabled="disbled">
                            </label>
                        </div>
                        <div class="row row-fluid">
                            <label for="Plan_cod_plan" class="span-7"><b>Código del Plan</b></label>
                            <label class="span-7">
                                <input type="text" value="<?php echo $data->cod_plan; ?>" class="span-11" disabled="disbled">
                            </label>
                        </div>
                        <div class="row row-fluid">
                            <label for="Plan_cod_plan" class="span-7"><b>Mención</b></label>
                            <label class="span-7">
                                <input type="text" value="<?php echo $mencion; ?>" class="span-11" disabled="disbled">
                            </label>
                        </div>
                        <div class="row row-fluid">
                            <label for="Plan_cod_plan" class="span-7"><b>Credencial</b></label>
                            <input type="text" value="<?php echo $credencial; ?>" class="span-11" disabled="disbled">
                            <label class="span-7">
                            </label>

                        </div>

                    </div>
                    <div class="col-lg-7">

                        <?php
                        if ($dataProvider !== array())
                            $this->widget('zii.widgets.grid.CGridView', array(
                                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                'id' => 'grados-grid',
                                'dataProvider' => $dataProvider,
                                //'filter' => $model,
                                'pager' => array('pageSize' => 1),
                                'summaryText' => false,
                                'afterAjaxUpdate' => "function(){
                            
                             $('#Plan_nombre').bind('keyup blur', function() {
                                 keyText(this, false);
                             });
                             $('#Plan_cod_plan').bind('keyup blur', function() {
                                 keyNum(this, false);
                             });
 
                         }",
                                'columns' => array(
                                    array(
                                        'name' => 'nombre',
                                        'type' => 'raw',
                                        'header' => '<center>Nombre del Grado</center>'
                                    ),
                                    array(
                                        'name' => 'boton',
                                        'type' => 'raw',
                                        'header' => '<center>Acciones</center>',
                                        'htmlOptions' => array('nowrap' => 'nowrap', 'width' => '15%'),
                                    ),
                                ),
                                'pager' => array(
                                    'header' => '',
                                    'htmlOptions' => array('class' => 'pagination'),
                                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                ),
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!isset($renderPartial)): ?>
        <div class="row">

                <div class="col-md-6">
                <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("catalogo/planEstudio"); ?>" class="btn btn-danger">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
            </div>

                <!-- <div class="col-md-6 wizard-actions">
                 <button type="submit" data-last="Finish" class="btn btn-primary btn-next">
                     Guardar
                     <i class="icon-save icon-on-right"></i>
                 </button>
             </div>
            -->

            </div>
    <?php endif; ?>
<div id="detalleGrado" class="hide"></div>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/planes.js', CClientScript::POS_END);
