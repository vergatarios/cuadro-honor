<?php
if (isset($dataProvider) && $dataProvider !== array())
    $this->widget('zii.widgets.grid.CGridView', array(
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'id' => 'asignaturas-grid',
        'dataProvider' => $dataProvider,
        //'filter' => $model,
        'pager' => array('pageSize' => 1),
        'summaryText' => false,
        'columns' => array(
//            array(
//                'name' => 'orden',
//                'type' => 'raw',
//                'header' => '<center>Orden</center>'
//            ),
            array(
                'name' => 'nombre',
                'type' => 'raw',
                'header' => '<center>Nombre de la Asignatura</center>'
            ),
            array(
                'name' => 'hora_teorica',
                'type' => 'raw',
                'header' => '<center>Horas Teóricas</center>'
            ),
            array(
                'name' => 'hora_practica',
                'type' => 'raw',
                'header' => '<center>Horas Practicas</center>'
            ),
            array(
                'name' => 'suma',
                'type' => 'raw',
                'header' => '<center>Horas Totales</center>'
            ),
            array(
                'name' => 'grado_pre',
                'type' => 'raw',
                'header' => '<center>Grado Prelación</center>'
            ),
            array(
                'name' => 'asignatura_pre',
                'type' => 'raw',
                'header' => '<center>Asignatura Prelación</center>'
            ),
            array(
                'name' => 'estatus',
                'type' => 'raw',
                'header' => '<center>Estatus</center>'
            ),
            array(
                'name' => 'acciones',
                'type' => 'raw',
                'header' => '<center>Acciones</center>',
                'htmlOptions' => array('width' => '10%'),
            ),
        ),
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
    ));
else {
    ?>
    <div class="infoDialogBox">
        <p>
            Estimado usuario, no ha establecido asignaturas para dicho grado. 
        </p>
    </div>
    <?php
}
?>
<div id="confirm-status" class="hide">
    <div class="alertDialogBox">
        <p style="text-align: justify">
            Al <strong><span class="confirm-action"></span></strong> la Asignatura "<b id="confirm-description"></b>" se <strong><span class="confirm-action"></span>á</strong> de igual forma el proceso de calificaciones y matricula con dicha Asignatura en el sistema.
        </p>
    </div>
    <div class="alert alert-info bigger-110">
        <p class="bigger-110 center">  Desea usted <strong><span class="confirm-action"></span></strong> la Asignatura?</p>
    </div>
</div>
<div id="update-status" class="hide">
    <div class="alertDialogBox">
        <p style="text-align: justify">
            Se actualizarán los datos de la Asignatura.
        </p>
    </div>
    <div class="alert alert-info bigger-110">
        <p class="bigger-110 center">  Desea usted Actualizar los datos de la Asignatura?</p>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var urlDir = '/catalogo/planEstudio/buscarAsignaturasDisponibles/';
        var asignaturas = '<?php (isset($asignaturas)) ? print_r($asignaturas) : null; ?>';
        var datos = {asignaturas: asignaturas};
        executeAjax('asignatura', urlDir, datos, false, true, 'POST');

        $('.change-status').unbind('click');
        $('.change-status').on('click',
                function(e) {
                    e.preventDefault();
                    var grado = $(this).attr('data-grado');
                    var plan = $(this).attr('data-plan');
                    var asignatura = $(this).attr('data-asignatura');
                    var description = $(this).attr('data-description');
                    var accion = $(this).attr('data-action');
                    cambiarEstatusAsignatura(grado, plan, asignatura, description, accion);
                }
        );
        $('.change-order').unbind('click');
        $('.change-order').on('click',
                function(e) {
                    e.preventDefault();
                    var grado = $(this).attr('data-grado');
                    var plan = $(this).attr('data-plan');
                    var asignatura = $(this).attr('data-asignatura');
                    var description = $(this).attr('data-description');
                    var accion = $(this).attr('data-action');
                    ordenarAsignatura(grado, plan, asignatura, description, accion);
                }
        );
    });
    $('.change-update').unbind('click');
    $('.change-update').on('click',
            function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var grado = $(this).attr('data-grado');
                var plan = $(this).attr('data-plan');
                var asignatura = $(this).attr('data-asignatura');
                var description = $(this).attr('data-description');
                var accion = $(this).attr('data-action');
                var hora_teorica = $("#hora_teorica" + asignatura).val();
                var hora_practica = $("#hora_practica" + asignatura).val();
                if(hora_teorica == ''){
                    $("#div-result-message").removeClass('hide');
                    $("#div-result-message").html('\
                        <div class="errorDialogBox">\n\
                            <button type="button" class="close" onclick="$(this).parent().fadeOut("slow");" style="padding-right: 13px;"><span aria-hidden="true"></span></button>\n\
                            <p>El campo de hora teórica en la Asignatura ' + description + ' esta vacio, es requerido.</p>\n\
                        </div>');
                        $("#hora_teorica").addClass('');
                        scrollUp("fast");
                }
                else{
                    actualizarAsignatura(id, hora_teorica, hora_practica, grado, plan, asignatura, description, accion);
                }
            }
    );
    
    $("#hora *, #hora_teorica, #hora_practica").bind('keyup', function(){
        keyNum(this, false);
    });
</script>
