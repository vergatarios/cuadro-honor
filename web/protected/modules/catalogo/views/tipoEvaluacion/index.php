<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/TipoEvaluacion/index.js', CClientScript::POS_END
);
?>
<?php

/* @var $this TipoEvaluacionController */
/* @var $model TipoEvaluacion */

$this->breadcrumbs=array(
	'Tipo Evaluación'=>array('/catalogo/tipoEvaluacion'),
	'Administración',
);
$this->pageTitle = 'Administración de Tipo Evaluación';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Tipo Evaluación</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
<!--        <div style="display:block;" class="widget-body-inner">-->
            <div class="widget-main">

                <div class="row space-6"></div>
<!--                <div>-->
                    <div id="resultadoOperacion">
                    </div>
                    <div class="row space-6"></div>
                    <?php
                    if (Yii::app()->user->pbac('catalogo.tipopersonal.index')):
                        ?>
                        <div class="pull-right" style="padding-left:10px;">
                            <a  type="submit" onclick="VentanaDialog('', '/catalogo/tipoEvaluacion/registro', 'Tipo De Evaluación', 'create', '')" data-last="Finish" class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar Tipo de Evaluación
                            </a>
                        </div>
                        <?php
                    endif;
                    ?>


                    <div class="row space-20"></div>

<!--                </div>-->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tipo-evaluacion-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        //'summaryText' => 'Mostrando {start}-{end} de {count}',
        'summaryText' => false,
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    $('#TipoEvaluacion_fecha_ini').datepicker();
                    $('#TipoEvaluacion_fecha_act').datepicker();
                    $.datepicker.setDefaults($.datepicker.regional = {
                            dateFormat: 'dd-mm-yy',
                            showOn:'focus',
                            showOtherMonths: false,
                            selectOtherMonths: true,
                            changeMonth: true,
                            changeYear: true,
                            minDate: new Date(1800, 1, 1),
                            maxDate: 'today'
                        });
                    
                    $('#TipoEvaluacion_nombre').unbind('keyup blur');
                    $('#TipoEvaluacion_nombre').on('keyup blur', function () {
                        keyText(this, true);
                    });
                    
                    $('#TipoEvaluacion_nombre').unbind('blur');
                    $('#TipoEvaluacion_nombre').on('blur', function () {
                        clearField(this);
                    });
                    
                    $('#TipoEvaluacion_fecha_act').on('dblclick', function(){
                        $(this).val('');
                        $('#tipo-evaluacion-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    });
                     $('#TipoEvaluacion_fecha_ini').on('dblclick', function(){
                        $(this).val('');
                        $('#tipo-evaluacion-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    });

                }",
	'columns'=>array(
        array(
            'header' => '<center>Nombre</center>',
            'name' => 'nombre',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('TipoEvaluacion[nombre]', $model->nombre, array('title' => '',)),
        ),        
        array(
            'header' => '<center>Fecha de Creación</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),           
            'filter' => CHtml::textField('TipoEvaluacion[fecha_ini]', $model->fecha_ini, array('title' => 'Fecha de Creación', 'readOnly' => 'readOnly')),
        ),
        array(
            'header' => '<center>Fecha de Actualización</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),           
            'filter' => CHtml::textField('TipoEvaluacion[fecha_act]', $model->fecha_act, array('title' => 'Fecha de Actualización al hacer, Para ver lista hacer Doble Click', 'readOnly' => 'readOnly',)),
        ),
	
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'value' => array($this, 'getEstatus'),
            'filter' => CHtml::dropDownList('TipoEvaluacion[estatus]', $model->estatus, array('' => '- - -', 'A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado'), array('title' => '')),
        ),
		
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    //'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
            </div>
<!--        </div>-->
    </div>
<!--</div>-->
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>
<div id="dialogInhabilitar" class="hide">

    <div class="alert alert-info bigger-110">
        <p class="bigger-110 center"> ¿Desea usted Inhabilitar este Tipo de Evaluación?</p>
    </div>
</div>
<div id="dialogActivar" class="hide">

    <div class="alert alert-info bigger-110">
        <p class="bigger-110 center"> ¿Desea usted Activar este Tipo de Evaluación?</p>
    </div>
</div>