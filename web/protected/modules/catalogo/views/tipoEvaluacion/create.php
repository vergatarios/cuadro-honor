<?php
/* @var $this TipoEvaluacionController */
/* @var $model TipoEvaluacion */

$this->pageTitle = 'Registro de Tipo Evaluacions';

      $this->breadcrumbs=array(
	'Tipo Evaluación'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>