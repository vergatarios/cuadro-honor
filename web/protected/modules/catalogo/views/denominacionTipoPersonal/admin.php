<?php

/* @var $this DenominacionTipoPersonalController */
/* @var $model DenominacionTipoPersonal */

$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Denominación por Tipo de Personal'=>array('lista'),
    'Administración',
);
$this->pageTitle = 'Administración de Denominación por Tipo de Personal';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Denominación por Tipo de Personal</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <?php
                        if ($model->hasErrors()) {
                            $this->renderPartial('//errorSumMsg', array('model' => $model));
                        } else {
                            if (Yii::app()->user->hasFlash('exito')) {
                                ?>
                                <div class="successDialogBox">
                                    <p><?php echo Yii::app()->user->getFlash('exito'); ?></p>
                                </div>
                            <?php
                            } else {
                                ?>
                                <div class="infoDialogBox">
                                    <p>
                                        En este módulo podrá registrar y/o actualizar los datos de Denominación por Tipo de Personal.
                                    </p>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/denominacionTipoPersonal/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nueva Denominación por Tipo de Personal                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'denominacion-tipo-personal-grid',
                    'dataProvider'=>$dataProvider,
                    'filter'=>$model,
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'summaryText' => 'Mostrando {start}-{end} de {count}',
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'afterAjaxUpdate' => "
                function(){

                }",
                    'columns'=>array(
                        /* array(
                             'header' => '<center>id</center>',
                             'name' => 'id',
                             'htmlOptions' => array(),
                             //'filter' => CHtml::textField('DenominacionTipoPersonal[id]', $model->id, array('title' => '',)),
                         ),*/
                        array(
                            'header' => '<center>Denominación</center>',
                            'name' => 'denominacion[nombre]',
                            'value' => '(is_object($data->denominacion) && isset($data->denominacion->nombre))? $data->denominacion->nombre: ""',
                            'htmlOptions' => array(),
                            'filter' => CHtml::textField('DenominacionTipoPersonal[denominacion][nombre]', (is_object($model->denominacion) AND isset($model->denominacion->nombre))?$model->denominacion->nombre:'', array('title' => '',)),
                        ),
                        array(
                            'header' => '<center>Tipo de Personal</center>',
                            'name' => 'tipoPersonal[nombre]',
                            'value' => '(is_object($data->tipoPersonal) && isset($data->tipoPersonal->nombre))? $data->tipoPersonal->nombre: ""',
                            'htmlOptions' => array(),
                            'filter' => CHtml::textField('DenominacionTipoPersonal[tipoPersonal][nombre]', (is_object($model->tipoPersonal) AND isset($model->tipoPersonal->nombre))?$model->tipoPersonal->nombre:'', array('title' => '',)),
                        ),
                        /*array(
                           'header' => '<center>usuario_ini_id</center>',
                           'name' => 'usuario_ini_id',
                           'htmlOptions' => array(),
                           //'filter' => CHtml::textField('DenominacionTipoPersonal[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
                       ),
                       array(
                           'header' => '<center>fecha_ini</center>',
                           'name' => 'fecha_ini',
                           'htmlOptions' => array(),
                           //'filter' => CHtml::textField('DenominacionTipoPersonal[fecha_ini]', $model->fecha_ini, array('title' => '',)),
                       ),
                       array(
                           'header' => '<center>usuario_act_id</center>',
                           'name' => 'usuario_act_id',
                           'htmlOptions' => array(),
                           //'filter' => CHtml::textField('DenominacionTipoPersonal[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
                       ),

                       array(
                           'header' => '<center>fecha_act</center>',
                           'name' => 'fecha_act',
                           'htmlOptions' => array(),
                           //'filter' => CHtml::textField('DenominacionTipoPersonal[fecha_act]', $model->fecha_act, array('title' => '',)),
                       ),
                       array(
                           'header' => '<center>usuario_elim_id</center>',
                           'name' => 'usuario_elim_id',
                           'htmlOptions' => array(),
                           //'filter' => CHtml::textField('DenominacionTipoPersonal[usuario_elim_id]', $model->usuario_elim_id, array('title' => '',)),
                       ),
                       array(
                           'header' => '<center>fecha_elim</center>',
                           'name' => 'fecha_elim',
                           'htmlOptions' => array(),
                           //'filter' => CHtml::textField('DenominacionTipoPersonal[fecha_elim]', $model->fecha_elim, array('title' => '',)),
                       ),
                         */
                        array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'htmlOptions' => array(),
                            'value' => array($this, 'estatus'),
                            'filter' => array('A' => 'Activo', 'E' => 'Eliminado','I'=>'Inactivo'),
                            //'filter' => CHtml::textField('DenominacionTipoPersonal[estatus]', $model->estatus, array('title' => '',)),
                        ),

                        array(
                            'type' => 'raw',
                            'header' => '<center>Acción</center>',
                            'value' => array($this, 'getActionButtons'),
                            'htmlOptions' => array('nowrap'=>'nowrap'),
                        ),
                    ),
                )); ?>
            </div>
        </div>
    </div>
</div>