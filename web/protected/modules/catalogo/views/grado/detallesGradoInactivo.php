
<div class="widget-box">

    <div class="widget-header">
              
                       
        <h5>
            Grado <?php echo $model->nombre;?>
        </h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
             <i class="icon-chevron-down"></i>  

            </a>
        </div>

    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="">
            <div class="widget-main">
                <center>
          
                <table class="table table-striped table-bordered table-hover" style="width:900px;">
                    <thead>
                    
                    <tr>
                        <th>
                            <center>
                                <b>Nombre del Grado</b>
                            </center>
                        </th>
                        <th>
                            <center>
                                <b>Nombre de usuario que desactivó el Grado</b>
                            </center>
                        </th>
                          <th>
                            <center>
                                <b>Fecha de Desactivación</b>
                            </center>
                        </th>
                   </tr>
                   
                     </thead>
                     <tbody>
                       
                            <tr class="odd">
                                <td>
                                    <center><b><?php echo $model->nombre;?></b></center>
                                </td>
                                <td>
                                    <center><b><?php echo $model->usuarioIni->nombre;?></b></center>
                                </td>
                                <td>
                                <center><b><?php echo date("d-m-Y H:i:s",strtotime($model->fecha_act));?></b></center>
                                </td>
                            </tr>
                                         
                    </tbody>

                                  </table>
                </center>

                </div><!-- search-form -->
        
</div>

</div>
</div>

            