<?php
/* @var $this AmbitoInvestigacionController */
/* @var $model AmbitoInvestigacion */

$this->pageTitle = 'Actualización de Datos de Ambito Investigacions';

      $this->breadcrumbs=array(
	'Ambito Investigacions'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>