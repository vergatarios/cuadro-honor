<?php
/* @var $this CampoConocimientoController */
/* @var $model CampoConocimiento */

$this->pageTitle = 'Registro de Campo Conocimientos';

      $this->breadcrumbs=array(
	'Campo Conocimientos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>