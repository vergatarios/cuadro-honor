<?php

/* @var $this FuncionTipoPersonalController */
/* @var $model FuncionTipoPersonal */

$this->breadcrumbs=array(
    'Catálogo'=>array('/catalogo'),
	'Función del Tipo de Personal'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Función del Tipo de Personal';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Función del Tipo de Personal</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Función del Tipo de Personal.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/funcionTipoPersonal/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Función del Tipo de Personal                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'funcion-tipo-personal-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
            $('#FuncionTipoPersonal_funcion_id').bind('keyup blur', function () {
                keyAlphaNum(this, true, true);
                makeUpper(this);
        }   );
                }",


    /*'afterAjaxUpdate'=>"
            function(){
                $('#FuncionTipoPersonal_funcion_id').unbind('keyup');
                $('#FuncionTipoPersonal_funcion_id').bind('keyup', function(){
                    keyAlphaNum(this,true,false);
                    makeUpper(this);
                );
                $('#FuncionTipoPersonal_funcion_id').unbind('blur');
                $('#FuncionTipoPersonal_funcion_id').bind('blur',funtion(){
                    clearField(this);
                });
            }
        ",*/

	'columns'=>array(
//        array(
//            'header' => '<center>id</center>',
//            'name' => 'id',
//            'htmlOptions' => array(),
//            //'filter' => CHtml::textField('FuncionTipoPersonal[id]', $model->id, array('title' => '',)),
//        ),



        array(
            'header' => '<center>Función</center>',
            'name' => 'funcion_id',
            'value'=>'$data->funcion->nombre',
            'htmlOptions' => array(),
            'value' => '(is_object($data->funcion) && isset($data->funcion->nombre))? $data->funcion->nombre: ""',
            'filter' => CHtml::textField('FuncionTipoPersonal[funcion_id]', $model->funcion_id, array('title' => '',)),
        ),

        array(
            'header' => '<center>Tipo de Personal</center>',
            'name' => 'tipo_personal_id',
            'value'=>'$data->tipoPersonal->nombre',
            //'id'=>'FuncionTipoPersonal_tipo_personal_id',
            'htmlOptions' => array(),
            'filter' => CHtml::listData($tiposDePersonal, 'id', 'nombre'),
        ),

//        array(
//            'header' => '<center>Fecha de Creación</center>',
//            'name' => 'fecha_ini',
//            'htmlOptions' => array(),
//            'filter' => CHtml::textField('FuncionTipoPersonal[fecha_ini]', $model->fecha_ini, array('title' => '','readonly'=>true)),
//        ),
        /*
        array(
            'header' => '<center>usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('FuncionTipoPersonal[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),*/

//        array(
//            'header' => '<center>Fecha de Actualización</center>',
//            'name' => 'fecha_act',
//            'htmlOptions' => array(),
//            'filter' => CHtml::textField('FuncionTipoPersonal[fecha_act]', $model->fecha_act, array('title' => '','readonly'=>true)),
//        ),
		/*
        array(
            'header' => '<center>usuario_elim_id</center>',
            'name' => 'usuario_elim_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('FuncionTipoPersonal[usuario_elim_id]', $model->usuario_elim_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('FuncionTipoPersonal[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),
        array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('FuncionTipoPersonal[estatus]', $model->estatus, array('title' => '',)),
        ),
		*/
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            'value' => array($this, 'estatus'),
            'filter' => array('A' => 'Activo', 'E' => 'Eliminado', 'I' => 'Inactivo'),
            //'filter' => CHtml::textField('FuncionTipoPersonal[estatus]', $model->estatus, array('title' => '',)),
        ),


		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>

<script src="/public/js/modules/catalogo/funcionTipoPersonal/form.js"></script>