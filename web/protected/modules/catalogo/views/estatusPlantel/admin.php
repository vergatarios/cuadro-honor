<?php
/* @var $this EstatusPlantelController */
/* @var $model EstatusPlantel */

$this->breadcrumbs=array(
	'Catálogo'=>array('/catalogo'),
	'Estatus de Planteles',
);
/*
$this->menu=array(
	array('label'=>'List EstatusPlantel', 'url'=>array('index')),
	array('label'=>'Create EstatusPlantel', 'url'=>array('create')),
);
*/
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#estatus-plantel-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

echo CHtml::scriptFile('/public/js/modules/catalogo/estatusPlantel.js');
?>



<?php /*echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); */?>
</div><!-- search-form -->
<div class="widget-box">
		<div class="widget-header">
        	<h4>Estatus de los Planteles</h4>
		<div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
            	<div class="row col-sm-8" id="resultadoOperacion"></div>
                <div class="row space-6"></div>
                <div>
                    <?php
                        if(($groupId == 1) || ($groupId == 18))
                        {
                        ?>
                            <div class="pull-right" style="padding-left:10px;">
                                <a  type="submit" onclick="VentanaDialog('','/catalogo/estatusPlantel/create','Nuevo Estatus de Plantel','create','')" data-last="Finish" class="btn btn-success btn-next btn-sm">
                                    <i class="fa fa-plus icon-on-right"></i>
                                    Registrar nuevo estado de plantel
                                </a>
                           
                            </div>
                 
                        <?php
                        }
                    ?>

                    <div class="row space-20"></div>

											<?php  $this->widget('zii.widgets.grid.CGridView', array(
                    							'itemsCssClass' => 'table table-striped table-bordered table-hover',
												'id'=>'estatus-plantel-grid',
												'dataProvider' => $model->search(),
						                        'filter' => $model,
						                        'summaryText'=>false,
						                        'pager' => array(
						                            'header' => '',
						                            'htmlOptions' => array('class' => 'pagination'),
						                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
						                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
						                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
						                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
						                        ),
												'columns' => array(
						                            array(
						                                'header' => '<center>Nombre del estatus del plantel</center>',
						                                'name' => 'nombre',

						                            ),
						                            array(
						                                'header' => '<center>Estatus</center>',
						                                'name' => 'estatus',
						                                'value' => array($this, 'estatus'),
						                                'filter'=>array('A'=>'Activo','E'=>'Inactivo'),
						                                
						                                
						                            ),
						                        
						                            array(
						                                'type' => 'raw',
						                                'header' => '<center>Acciones</center>',
						                                'value' => array($this, 'columnaAcciones'),
						                            ),
						                        ),
											)); ?>

											<div class="row-fluid-actions">
								<a class="btn btn-danger" href="/catalogo">
								<i class="icon-arrow-left bigger-110"></i>
								Volver
								</a>
							</div>

                </div>
            </div>
        </div>
    </div>
</div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>


<div id="dialogPantalla" class="hide"></div>

