<?php
/* @var $this EstatusPlantelController */
/* @var $model EstatusPlantel */

$this->breadcrumbs=array(
	'Estatus Plantels'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EstatusPlantel', 'url'=>array('index')),
	array('label'=>'Manage EstatusPlantel', 'url'=>array('admin')),
);
?>


<?php $this->renderPartial('_form', array('model'=>$model, 'subtitulo'=>'Crear Estatus de Plantel')); ?>