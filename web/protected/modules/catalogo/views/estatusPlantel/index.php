<?php
/* @var $this EstatusPlantelController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Estatus Plantels',
);

$this->menu=array(
	array('label'=>'Create EstatusPlantel', 'url'=>array('create')),
	array('label'=>'Manage EstatusPlantel', 'url'=>array('admin')),
);
?>

<h1>Estatus Plantels</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
