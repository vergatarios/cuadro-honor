<?php
/* @var $this DependenciaNominalController */
/* @var $model DependenciaNominal */

$this->breadcrumbs=array(
	'Dependencia Nominals'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DependenciaNominal', 'url'=>array('index')),
	array('label'=>'Create DependenciaNominal', 'url'=>array('create')),
	array('label'=>'View DependenciaNominal', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DependenciaNominal', 'url'=>array('admin')),
);
?>

<h1>Update DependenciaNominal <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>