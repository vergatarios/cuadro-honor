<?php
/* @var $this userGroups/GrupoController */
/* @var $model UserGroupsGroup */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'dependencia-form',
    'enableAjaxValidation'=>true,
)); ?>
  

        <div class="widget-body">

            <div class="widget-body-inner">

                <div class="widget-main form">
                    

                        <div class="row">

                            <input type="hidden" id='id' name="id" value="<?php echo $model->id ?>" />
                            
                                <div class="col-md-6">
                                    <label class="col-md-12" for="nombre">Nombre de la Dependencia <span class="required">*</span></label>
                                   <?php echo $form->textField($model,'nombre', array('required'=>'required', 'maxlength'=>30, 'class'=>'span-12', 'id'=>'nombre_dependencia', 'placeholder'=>'Nombre de Dependencia', 'title'=>'Nombre de la Dependencia ')); ?>
                                </div>

                                </div>

                </div>
            </div>
        </div>
   
<?php $this->endWidget(); ?>