<?php

class AsignaturaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
static $_permissionControl = array(
        'read' => 'Consulta de Asignaturas ',
        'write' => 'Modificacion de Asignaturas ', 
        'label' => 'Consulta de Asignaturas'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'consultarAsignatura', 'consultarAsignaturaInactivo','modificarAsignatura', 'procesarCambio','crear','create', 'eliminar', 'reactivar'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('write'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
        //*------------------Columnas del Index--------------------------------------------------
        //---------------------------------------------------------------------------------------
        
                 
 public function columnaAcciones($data){
        
        $id = $data["id"];
       
        
        if($data->estatus=='A'){
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarAsignatura($id)", "title" => "Consultar esta Asignatura")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green", "onClick" => "modificarAsignatura($id)", "title" => "Modificar Asignatura")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "borrar($data->id)", "class" => "fa fa-trash-o red remove-data", "style" => "color:#555;", "title" => "Eliminar"));
        }
        else{
            //$columna .= CHtml::link("", "#", array("onClick" => "reactivar($data->id)", "class" => "fa fa icon-ok red remove-data", "style" => "color:#555;", "title" => "Reactivar"));
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarAsignatura($id)", "title" => "Consultar esta Asignatura")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "reactivar($data->id)", "class" => "fa icon-ok green remove-data", "style" => "color:#555;", "title" => "Reactivar"));
        }

        return $columna;
    }
     public function columnaEstatus($data){
         
         $estatus='';
         
         if($data['estatus']=="A")
         {
             $estatus='Activo';
         }
         
         if($data['estatus']=="E")
         {
             $estatus='Inactivo';
         }
         
         return $estatus;
         
         
    }
    
    
    //--------------------------------->fin columnas
    
    
     //----------------------------------------------------------------------
        //-----------------------------CREAR NUEVO REGISTRO---------------------
        
        
	public function actionCreate()
	{
            $model = new Asignatura;

        //Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
	}
        
        public function actionCrear() { 
            
            $model = new Asignatura;
              #var_dump($_REQUEST);die();
            if (isset($_POST['nombre'])) {
            $model->nombre=trim($_POST['nombre']);
            $model->abreviatura=trim($_POST['abrev']);
            $mensaje='';
            
            if($model->validate()) {
            
            $nombreA = strtoupper($_POST['nombre']);
            $abrevA=strtoupper( $_POST['abrev']);
            $nombre= trim($nombreA);
            $abrev= trim($abrevA);
            
           
            $result = $model->registrar($nombre,$abrev);
            $this->registerLog('ESCRITURA', 'catalogo.asignatura.crear', 'EXITOSO', 'Se ha creado una asignatura');
                    
            $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Registro Exitoso'));
            $model= new Asignatura;
                        
            }
           
            
            else { // si ingresa datos erroneos muestra mensaje de error.
            $this->renderPartial('//errorSumMsg', array('model' => $model));
            
            
            }
    
        }
        
            }
    
    //--------------------------------------------------------------------------
    //-------------------------------MODIFICAR REGISTRO-------------------------
    
     public function actionModificarAsignatura($id)
	{
           $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

            $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
    }
    
    public function actionProcesarCambio() {
         $id = $_POST['Asignatura']['id'];
        $model= new Asignatura;
        if (isset($_POST['Asignatura'])) {
            $model->nombre=trim($_POST['Asignatura']['nombre']);
            $model->abreviatura=trim($_POST['Asignatura']['abrev']);
            $mensaje='';
            
            if($model->validate()) {
                           
            $id = $_POST['Asignatura']['id'];
            $nombreA = strtoupper($_POST['Asignatura']['nombre']);
            $abrevA=strtoupper( $_POST['Asignatura']['abrev']);
            $nombre= trim($nombreA);
            $abrev= trim($abrevA);
            //var_dump($nombreA);die();
            $result = $model->actualizar($id,$nombre,$abrev);
            $this->registerLog('ESCRITURA', 'catalogo.asignatura.procesarCambio', 'EXITOSO', 'Se ha modificado una Asignatura');
          
         
            }
            else { // si ingresa datos erroneos muestra mensaje de error.
                //$this->renderPartial('//errorSumMsg', array('model' => $model));
         $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Modificación exitosa'));            
         $model = $this->loadModel($id);    
         $this->renderPartial('_form', array(
            'model' => $model,
        ));
        Yii::app()->end();
            }
            $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Modificación exitosa'));
            $model = $this->loadModel($id);
            $this->renderPartial('_form', array(
            'model' => $model,
        ));
    
            
                         
        }
                   
    
    } 
        
        //--------------------------------------------------------------------------
    //---------------------------CONSULTAR DETALLES-----------------------------
    
     public function actionConsultarAsignatura($id) {


        $model = new Asignatura;

        $id = $_GET['id'];
        $model->id = $id;
        $model = $this->loadModel($id);
        //var_dump($model->usuarioAct->nombre);die();
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('detallesAsignatura', array(
        'model'=>Asignatura::model()->view($id),
            //'model'=>$model,
                ), FALSE, TRUE);
     }
     
     public function actionConsultarAsignaturaInactivo($id) {


        $model = new Asignatura;

        $id = $_GET['id'];
        $model->id = $id;
        $model = $this->loadModel($id);
        //var_dump($model->usuarioAct->nombre);die();
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('detallesAsignaturaInactivo', array(
            'model'=>Asignatura::model()->view($id),
            //'model'=>$model,
                ), FALSE, TRUE);
     }
    
    //--------------------------------------------------------------------------
    //------------DESACTIVAR (ELIMINACION LOGICA)-------------------------------
    
    public function actionEliminar() {
        
                    //$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Turno Elimanado con exito.'));
                  $model= new Asignatura;
                    $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Inhabilitar esta Asignatura? </p>'));
                if(isset($_GET['id']))
		{
		$id=$_GET['id'];
                 
        
            $result = $model->desactivar($id);
            $this->registerLog('ESCRITURA', 'catalogo.asignatura.eliminar', 'EXITOSO', 'Se ha eliminado una asignatura');
                }
		
	}

        
        //---------------------------------------------------------
        
        
      ///------------------------------------------------------------------------------------------
    //-----------------------------REACTIVAR-------------------------------------------------------
    
    public function actionReactivar() {
        
           //$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Turno Elimanado con exito.'));
                  $model= new Asignatura;
                   $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Habilitar esta Asignatura? </p>'));
                if(isset($_GET['id']))
		{
		$id=$_GET['id'];
                 
        
            $result = $model->activar($id);
            $this->registerLog('ESCRITURA', 'catalogo.asignatura.reactivar', 'EXITOSO', 'Se ha reactivado una asignatura');
                }
		
	}
               

        
        //___________________________________________________________________
        ///____________________________________________________________________
        
        
        
	public function actionPrincipal()
	{
		$dataProvider=new CActiveDataProvider('Asignatura');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
                
		$model=new Asignatura('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Asignatura']))
			$model->attributes=$_GET['Asignatura'];

		$this->render('index',array(
			'model'=>$model,
                    
		));
                
                $this->registerLog('LECTURA', 'catalogo.asignatura.index', 'EXITOSO', 'Se ha consultado lista de asignaturas');
	}

          static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $modulo = "Catalogo.AsignaturaController." . $accion;
        $Utiles->traza($transaccion, $modulo, date('Y-m-d H:i:s'));
    }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Asignatura the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Asignatura::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Asignatura $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='asignatura-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
