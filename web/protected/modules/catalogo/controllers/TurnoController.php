<?php

class TurnoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
static $_permissionControl = array(
        'read' => 'Consulta de Turnos ',
        'write' => 'Modificacion de Turnos ', 
        'label' => 'Consulta de Turnos'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'consultarTurno', 'consultarTurnoInactivo', 'modificarTurno', 'procesarCambio','crear','create', 'eliminar', 'reactivar'),
                'pbac' => array('write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'consultarTurno'),
                'users' => array('read'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
        
        
        //----------------------------------------------------------------------
        //-----------------------------CREAR NUEVO REGISTRO---------------------
        
        
	public function actionCreate()
	{
         $model = new Turno;

        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
        Yii::app()->end();
        
	}
        
        public function actionCrear() { 
                
        $model = new Turno;
       
        if (isset($_POST['nombre'])) {
            $nombre= trim(strtoupper($_POST['nombre']));
            $model->nombre= $nombre;
            $model->usuario_act_id = Yii::app()->user->id;
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = 'A';
            
                if ($model->save()) {
                   $model->nombre=NULL; 
                   echo $model->nombre;
                  
                   $model= new Turno;
                   $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso.'));
                   
        
                   Yii::app()->end();
                                         
                }else{
                    
                 $this->renderPartial('//errorSumMsg', array('model' => $model));
                 Yii::app()->end();
                }
                
        }
    }
    
    //--------------------------------------------------------------------------
    //-------------------------------MODIFICAR REGISTRO-------------------------
    
     public function actionModificarTurno($id)
	{
           $model = $this->loadModel($id);

      
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
    }
    
    public function actionProcesarCambio() {

        if (isset($_POST['Turno'])) {
            $id = $_POST['Turno']['id'];
            $model = $this->loadModel($id);
            if (is_numeric($id)) {
                $model = Turno::model()->findByPk($id);
                //var_dump($_POST['Turno']['nombre']);die();
                $model->attributes = $_POST['Turno'];
                $model->nombre = trim($_POST['Turno']['nombre']);
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->estatus = "A";
             
             if ($model->save()) {
                    
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
                    $this->registerLog('ESCRITURA', 'catalogo.turno.procesarCambio', 'EXITOSO', 'Se ha Modificado un turno');

                    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    $this->renderPartial('_form', array(
                        'model' => $model
                            ), FALSE, TRUE);
                    
                } else {
       // $this->renderPartial('//errorSumMsg', array('model' => $model));
//        Yii::app()->end();
        $this->renderPartial('_form', array(
                      'model' => $model
                         ), FALSE, TRUE);          
                }
          
        }else{
               throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
        }
        
        
                }
    
        
   
    }          
            
            
      
        
    //--------------------------------------------------------------------------
    //---------------------------CONSULTAR DETALLES-----------------------------
    
     public function actionConsultarTurno($id) {


        $model = new Turno;

        $id = $_GET['id'];
        $model->id = $id;
        $model = $this->loadModel($id);
        //var_dump($model->usuarioAct->nombre);die();
        //Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('detallesTurno', array(
            'model'=>Turno::model()->view($id),
            //'model'=>$model,
                ), FALSE, TRUE);
         //Yii::app()->end();
     }
     
     public function actionConsultarTurnoInactivo($id) {


        $model = new Turno;

        $id = $_GET['id'];
        $model->id = $id;
        $model = $this->loadModel($id);
        //var_dump($model->usuarioAct->nombre);die();
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('detallesTurnoInactivo', array(
            'model'=>Turno::model()->view($id),
            //'model'=>$model,
                ), FALSE, TRUE);
     }
    
    //--------------------------------------------------------------------------
    //------------DESACTIVAR (ELIMINACION LOGICA)-------------------------------
    
    public function actionEliminar() {
                //$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Turno Elimanado con exito.'));
                $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Inhabilitar este Turno? </p>'));
                if(isset($_GET['id']))
		{
		$id=$_GET['id'];
		
		$model=$this->loadModel($id);
		if($model){
		$model->usuario_act_id=Yii::app()->user->id;
		$model->fecha_elim=date("Y-m-d H:i:s");
                $model->fecha_act=date("Y-m-d H:i:s");
		$model->estatus="E";
		if($model->save()){
                        $model=$this->loadModel($id);
			//$this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Inhabilitar este Turno? </p>'));
			 $this->registerLog('ESCRITURA', 'catalogo.turno.eliminar', 'EXITOSO', 'Se ha eliminado un turno');		
                                        
		}
		else{
					throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

				}

		}
		else{

		throw new CHttpException(404, 'Error! Recurso no encontrado!');		
		}
		

		}

	}
        
         ///------------------------------------------------------------------------------------------
    //-----------------------------REACTIVAR-------------------------------------------------------
    
    public function actionReactivar() {
                $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Habilitar este Turno? </p>'));

                if(isset($_GET['id']))
		{
		$id=$_GET['id'];
		
		$model=$this->loadModel($id);
		if($model){
		
                $model->usuario_ini_id= Yii::app()->user->id;
                $model->usuario_act_id=NULL;
                $model->fecha_ini=date("Y-m-d H:i:s");
                $model->fecha_elim=NULL;
                $model->fecha_act=NULL;
		$model->estatus="A";
		if($model->save()){
			
					$model=$this->loadModel($id);
                                        $this->registerLog('ESCRITURA', 'catalogo.turno.reactivar', 'EXITOSO', 'Se ha reactivado un turno');

		}
		else{
					throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

				}

		}
		else{

		throw new CHttpException(404, 'Error! Recurso no encontrado!');		
		}
		

		}

	}


    
    
    
    //--------------------------------------------------------------------------
    //-----------------------INDEX PAGINA PRINCIPAL-----------------------------
    
        
    public function actionIndex(){

         /*
          $url = $_SERVER['REQUEST_URI'];
          if($url[strlen($url)-1] != '/')
          {
                $this->redirect('../catalogo/turno/');
               // throw new CHttpException(404, "Dirección inválida.");
          }
          * 
          */
            $model=new Turno('search');
           // $model->unsetAttributes();  // clear any default values

        
        
            $model=new Turno('search');
            $model->unsetAttributes();  // clear any default values

            if(isset($_GET['Turno']))
                    $model->attributes=$_GET['Turno'];

            $this->render('index',array(
                    'model'=>$model,
            ));
             $this->registerLog('LECTURA', 'catalogo.turno.index', 'EXITOSO', 'Se ha consultado lista de turnos');
    }
    
    
    public function columnaAcciones($data){
 
         $id = $data["id"];
       
        
        if($data->estatus=='A'){
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarTurno($id)", "title" => "Consultar este Turno")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green", "onClick" => "modificarTurno($id)", "title" => "Modificar Turno")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "borrar($data->id)", "class" => "fa fa-trash-o red remove-data", "style" => "color:#555;", "title" => "Eliminar"));
        }
        else{
            //$columna .= CHtml::link("", "#", array("onClick" => "reactivar($data->id)", "class" => "fa fa icon-ok red remove-data", "style" => "color:#555;", "title" => "Reactivar"));
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarTurno($id)", "title" => "Consultar este Turno")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "reactivar($data->id)", "class" => "fa icon-ok green remove-data", "style" => "color:#555;", "title" => "Reactivar"));
        }
         return $columna;
    }    
        
    public function columnaEstatus($data){
         
         $estatus='';
         
         if($data['estatus']=="A")
         {
             $estatus='Activo';
         }
         
         if($data['estatus']=="E")
         {
             $estatus='Inactivo';
         }
         
         return $estatus;
         
         
    }
    
    
    
    
    
    
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Turno']))
		{
			$model->attributes=$_POST['Turno'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionPpal()
	{
		$dataProvider=new CActiveDataProvider('Turno');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
        
              static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $modulo = "Catalogo.TurnoController." . $accion;
        $Utiles->traza($transaccion, $modulo, date('Y-m-d H:i:s'));
    }


	
	public function loadModel($id)
	{
		$model=Turno::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Turno $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='turno-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
