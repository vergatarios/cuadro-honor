<?php

class CredencialController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
 static $_permissionControl = array(
        'read' => 'Consulta de Credenciales ',
        'write' => 'Modificacion de Credenciales ', 
        'label' => 'Consulta de Credenciales'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'consultarCredencial', 'modificarCredencial', 'procesarCambio','crear','create', 'eliminar', 'reactivar'),
                'pbac' => array('write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'consultarCredencial'),
                'users' => array('read'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
      public function actionCreate() {

        $model = new Credencial;

        //Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ));
    }

    public function actionCrear() {

        $model = new Credencial;

                   #var_dump($_REQUEST);die();
        if (isset($_POST['nombre'])) {
            //var_dump($_POST['nombre']);die();
            $nombre=  trim(strtoupper($_POST['nombre']));
           // var_dump($nombre);
            $model->nombre = $nombre; 
           $model->usuario_ini_id = Yii::app()->user->id;
           $model->fecha_ini = date("Y-m-d H:i:s");
           $model->estatus = 'A';
            
            if($model->validate()) {
                if ($model->save()) {
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso.'));
                    $model= new Credencial;
                    $this->registerLog('ESCRITURA', 'catalogo.credencial.crear', 'EXITOSO', 'Se ha creado una nueva credencial'); 
                }
            }else{
           
                   // $model= new Credencial;
                    
                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                   // $this->renderPartial('_form', array(
                     //   'model' => $model,
                   // ));
             }
         }
    }
    
        //------------------------->MODIFICAR----------------------------------------------
        //.--------------------------------------------------------------------------------
      public function actionModificarCredencial($id)
	{
           $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
    }
    
    public function actionProcesarCambio() {

        //$this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
        // Uncomment the following line if AJAX validation is needed
         //$this->performAjaxValidation($model);
         

        if (isset($_POST['Credencial'])) {
            $id = $_POST['Credencial']['id'];
            $model = $this->loadModel($id);
              if (is_numeric($id)) {
            $model = Credencial::model()->findByPk($id);
            
            $model->attributes = $_POST['Credencial'];
            $nombre=$_POST['Credencial']['nombre'];
            $model->nombre=$nombre;
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = "A";

            if ($model->validate()) {

                if ($model->save()) {
                    
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
                    $this->registerLog('ESCRITURA', 'catalogo.credencial.procesarCambio', 'EXITOSO', 'Se ha Modificado una credencial');
                    
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            }
           
            
        }
        }
        
       
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
    }
    
    ///----------------------------------------------------------------------------------------------------------------
    //-------------------------------------------CONSULTAR
    
    
    
    public function actionConsultarCredencial($id) {


        $model = new Credencial;

        $id = $_GET['id'];
        $model->id = $id;
        $model = $this->loadModel($id);
        //var_dump($model->usuarioAct->nombre);die();
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('detallesCredencial', array(
            'model'=>Credencial::model()->view($id),
            //'model'=>$model,
                ), FALSE, TRUE);
        
        
    }
    
    
    
    ///------------------------------------------------------------------------------------------
    //-----------------------------BORRAR--------------------------------------------------------
    
    public function actionEliminar() {
                $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Inhabilitar este Turno? </p>'));

         if(isset($_GET['id']))
	{
		$id=$_GET['id'];
		
		$model=$this->loadModel($id);
		if($model){
		$model->usuario_act_id=Yii::app()->user->id;
		$model->fecha_elim=date("Y-m-d H:i:s");
                $model->fecha_act=date("Y-m-d H:i:s");
		$model->estatus="E";
                    if($model->save()){
			
                    $model=$this->loadModel($id);
                    $this->registerLog('ESCRITURA', 'catalogo.credencial.eliminar', 'EXITOSO', 'Se ha eliminado una credencial');

                    }else{
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

                        }

		}else{
                throw new CHttpException(404, 'Error! Recurso no encontrado!');		
		}
		

	}

}
        
         ///------------------------------------------------------------------------------------------
    //-----------------------------REHABILITAR-------------------------------------------------------
    
    public function actionReactivar() {
               
                $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Habilitar esta Credencial Nuevamente? </p>'));

                if(isset($_GET['id']))
		{
		$id=$_GET['id'];
		
		$model=$this->loadModel($id);
		if($model){
		$model->usuario_act_id=Yii::app()->user->id;
		$model->fecha_elim=date("Y-m-d H:i:s");
                $model->fecha_act=date("Y-m-d H:i:s");
		$model->estatus="A";
		if($model->save()){
			
					$model=$this->loadModel($id);
                                       $this->registerLog('ESCRITURA', 'catalogo.credencial.reactivar', 'EXITOSO', 'Se ha reactivado una credencial');

		}
		else{
					throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

				}

		}
		else{

		throw new CHttpException(404, 'Error! Recurso no encontrado!');		
		}
		

		}

	}


        //---------------------------------------------------------------------------------------
        
        public function columnaEstatus($data){
         
         $estatus='';
         
         if($data['estatus']=="A")
         {
             $estatus='Activo';
         }
         
         if($data['estatus']=="E")
         {
             $estatus='Inactivo';
         }
         
         return $estatus;
         
         
    }
        

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Credencial']))
		{
			$model->attributes=$_POST['Credencial'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionAd()
	{
		$dataProvider=new CActiveDataProvider('Credencial');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Credencial('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Credencial']))
			$model->attributes=$_GET['Credencial'];

		$this->render('index',array(
			'model'=>$model,
		));
                $this->registerLog('LECTURA', 'catalogo.credencial.index', 'EXITOSO', 'Se ha consultado lista de credenciales');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Credencial the loaded model
	 * @throws CHttpException
	 */
         public function columnaAcciones($data){
 
         $id = $data["id"];
       
        
        if($data->estatus=='A'){
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarCredencial($id)", "title" => "Consultar esta Credencial")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green", "onClick" => "modificarCredencial($id)", "title" => "Modificar Credencial")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "borrar($data->id)", "class" => "fa fa-trash-o red remove-data", "style" => "color:#555;", "title" => "Eliminar"));
        }
        else{
            //$columna .= CHtml::link("", "", array("onClick" => "reactivar($data->id)", "class" => "fa fa icon-ok red remove-data", "style" => "color:#555;", "title" => "Reactivar"));
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarCredencial($id)", "title" => "Consultar esta credencial")) . '&nbsp;&nbsp;';
             $columna .= CHtml::link("", "", array("onClick" => "reactivar($data->id)", "class" => "fa icon-ok green remove-data", "style" => "color:#555;", "title" => "Reactivar"));
        }
         return $columna;
    }
    
    
          static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $modulo = "Catalogo.CredencialController." . $accion;
        $Utiles->traza($transaccion, $modulo, date('Y-m-d H:i:s'));
    }

        
	public function loadModel($id)
	{
		$model=Credencial::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Credencial $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='credencial-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
