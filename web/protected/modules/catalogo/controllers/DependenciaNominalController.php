<?php

class DependenciaNominalController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consulta de Dependencias Nominales',
        'write' => 'Modificacion de Dependencias Nominales',
        'label' => 'Consulta de Dependencias Nominales'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'consultarDependencia', 'modificarDependencia', 'procesarCambio', 'crear', 'create', 'eliminar', 'reactivar'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Despliega el pop-up con el formulario para un nuevo registro.
     * @author Meylin Guillén.
     * @param $model Modelo DependenciaNominal.
     */
    public function actionCreate() {

        $model = new DependenciaNominal;

        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
    }

    /**
     * Procesa los datos del formulario del nuevo registro.
     * @author Meylin Guillén.
     * @param $model Modelo DependenciaNominal.
     */
    public function actionCrear() {

        $model = new DependenciaNominal;

        if (isset($_POST['nombre'])) {
            //var_dump($_POST['nombre']);die();
            $model->nombre = trim(strtoupper($_POST['nombre']));
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = 'A';

            if ($model->validate()) {

                //var_dump('ALGO');die();
                if ($model->save()) {
                    //var_dump($model);die();
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso!'));
                    $this->registerLog('ESCRITURA', 'catalogo.dependenciaNominal.crear', 'EXITOSO', 'Se ha registrado una Dependencia');
                }
            } else {

                $this->renderPartial('//errorSumMsg', array('model' => $model));
            }
        }
    }

    /**
     * Despliega el pop-up para nuevo registro.
     * @author Meylin Guillén.
     * @param $model Modelo DependenciaNominal.
     */
    public function actionModificarDependencia($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
    }

    /**
     * Procesa los datos del formulario desde la opción de modificar una dependencia ya creada.
     * @author Meylin Guillén.
     * @param $model Modelo DependenciaNominal.
     * @param integer $id ID asignado a la dependencia que se va a modificar.
     */
    public function actionProcesarCambio() {

        if (isset($_POST['Dependencia'])) {
            $id = $_POST['Dependencia']['id'];
            $model = $this->loadModel($id);
            if (is_numeric($id)) {
                $model = dependenciaNominal::model()->findByPk($id);

                $model->attributes = $_POST['Dependencia'];
                $model->nombre = trim(strtoupper($_POST['Dependencia']['nombre']));
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->estatus = "A";

                if ($model->validate()) {

                    if ($model->save()) {

                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Modificación realizada con exito.'));
                        $this->registerLog('ESCRITURA', 'catalogo.dependenciaNominal.procesarCambio', 'EXITOSO', 'Se ha Modificado una Dependencia');
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                }
            } else {
                Yii::app()->user->setFlash('error', "No se ha podido completar la última operación, ID invalido.");
            }
        }
        $this->renderPartial('_form', array(
            'model' => $model,
        ));
    }

    /**
     * Elimina de manera lógica una dependencia ya creada.
     * @author Meylin Guillén.
     * @param $model Modelo DependenciaNominal.
     * @param integer $id ID asignado a la dependencia que se va a desactivar.
     */
    public function actionEliminar() {
        $this->renderPartial("//msgBox", array('class' => 'alertDialogBox', 'message' => '<p class="bolder center grey"> ¿Desea Inhabilitar esta Dependencia Nominal? </p>'));

        if (isset($_GET['id'])) {
            $id = $_GET['id'];

            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_elim = date("Y-m-d H:i:s");
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->estatus = "E";
                if ($model->save()) {
                    //$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Eliminado con exito.'));
                    $model = $this->loadModel($id);
                    $this->registerLog('ESCRITURA', 'catalogo.grado.eliminar', 'EXITOSO', 'Se ha eliminado una dependencia');
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
    }

    /**
     * Reactiva de manera lógica una dependencia que fue previamente desactivada.
     * @author Meylin Guillén.
     * @param $model Modelo DependenciaNominal.
     * @param integer $id ID asignado a la dependencia que se va a reactivar.
     */
    public function actionReactivar() {
        $this->renderPartial("//msgBox", array('class' => 'alertDialogBox', 'message' => '<p class="bolder center grey"> ¿Desea Habilitar este Turno? </p>'));

        if (isset($_GET['id'])) {
            $id = $_GET['id'];

            $model = $this->loadModel($id);
            if ($model) {

                $model->usuario_ini_id = Yii::app()->user->id;
                $model->usuario_act_id = NULL;
                $model->fecha_ini = date("Y-m-d H:i:s");
                $model->fecha_elim = NULL;
                $model->estatus = "A";
                if ($model->save()) {

                    $model = $this->loadModel($id);
                    $this->registerLog('ESCRITURA', 'catalogo.grado.reactivar', 'EXITOSO', 'Se ha reactivado una dependencia');
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
    }

    ///_____________________________________________________________________________
    //______________________________________________________________________________


    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionInicio() {
        $dataProvider = new CActiveDataProvider('DependenciaNominal');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Muestra la página principal del catálogo, con la lista (Grid) de registros actuales.
     * @author Meylin Guillén.
     * @param $model Modelo DependenciaNominal.
     */
    public function actionIndex() {


        $model = new DependenciaNominal('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['DependenciaNominal']))
            $model->attributes = $_GET['DependenciaNominal'];

        $this->render('index', array(
            'model' => $model,
        ));
        $this->registerLog('Lectura', 'catalogo.grado.index', 'EXITOSO', 'Se han consultado la lista de Dependencias');
    }

    /**
     * Botones de accion (crear, consultar, desactivar o activar) desde el Grid de la vista principal.
     * @author Meylin Guillén.
     * @param integer $id ID asignado a la dependencia Nominal.
     * @return 
     */
    public function columnaAcciones($data) {

        $id = $data["id"];


        if ($data->estatus == 'A') {
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarDependencia($id)", "title" => "Consultar este servicio")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green", "onClick" => "modificarDependencia($id)", "title" => "Modificar Dependencia")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "borrar($data->id)", "class" => "fa fa-trash-o red remove-data", "style" => "color:#555;", "title" => "Eliminar"));
        } else {
            //$columna .= CHtml::link("", "#", array("onClick" => "reactivar($data->id)", "class" => "fa fa icon-ok red remove-data", "style" => "color:#555;", "title" => "Reactivar"));
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarDependencia($id)", "title" => "Consultar este servicio")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "reactivar($data->id)", "class" => "fa icon-ok green remove-data", "style" => "color:#555;", "title" => "Reactivar"));
        }

        return $columna;
    }

    /**
     * Genera la columna del estatus en el Grid principal.
     * @author Meylin Guillén.
     * @param string $data array que trae los datos de la dependencia, en este caso $data['estatus'].
     * @return 
     */
    public function columnaEstatus($data) {

        $estatus = '';

        if ($data['estatus'] == "A") {
            $estatus = 'Activo';
        }

        if ($data['estatus'] == "E") {
            $estatus = 'Inactivo';
        }

        return $estatus;
    }

    /**
     * Despliega pop-up con la información detallada de la dependencia seleccionada.
     * @author Meylin Guillén.
     * @param $model Modelo DependenciaNominal.
     * @param integer $id ID asignado a la dependencia que se va a consultar.
     * @return 
     */
    public function actionConsultarDependencia($id) {


        $model = new DependenciaNominal;

        $id = $_GET['id'];
        $model->id = $id;
        $model = $this->loadModel($id);
        //var_dump($model->usuarioAct->nombre);die();
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('detallesDependencia', array(
            'model' => DependenciaNominal::model()->view($id),
                //'model'=>$model,
                ), FALSE, TRUE);
    }

    static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $modulo = "Catalogo.DependenciaNominalController." . $accion;
        $Utiles->traza($transaccion, $modulo, date('Y-m-d H:i:s'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return DependenciaNominal the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = DependenciaNominal::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param DependenciaNominal $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'dependencia-nominal-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
