<?php

class RestriccionEstatusController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de RestriccionEstatusController',
        'write' => 'Creación y Modificación de RestriccionEstatusController',
        'admin' => 'Administración Completa  de RestriccionEstatusController',
        'label' => 'Módulo de RestriccionEstatusController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion','activar', 'eliminacion', 'admin',),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $model=new RestriccionEstatus('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('RestriccionEstatus')){
            $model->attributes=$this->getQuery('RestriccionEstatus');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new RestriccionEstatus('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('RestriccionEstatus')){
            $model->attributes=$this->getQuery('RestriccionEstatus');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {


         if($this->hasQuery('id') AND !is_numeric($this->getIdDecoded($this->getQuery('id')))){
            throw new CHttpException(404,'Estimado usuario, no se han recibido los parametros validos para realizar esta acción. Regrese a la página anterior e intente nuevamente.');
        }
        $model=new RestriccionEstatus();
        $id = $this->getQuery('id');
        
        $usuario_id=Yii::app()->user->id;
        $fecha_timestamp = date('Y-m-d H:i:s');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if($this->hasPost('RestriccionEstatus'))
        {
            $model->attributes=$this->getPost('RestriccionEstatus');
            $model->usuario_ini_id=$usuario_id;
            $model->fecha_ini=$fecha_timestamp;
           
            $model->estatus = "A";
            if($model->save()){
                Yii::app() -> user -> setFlash("//msgBox", "<p>Registro Exitoso sus Datos son:.</p>");
               //$this->redirect(array('view','id'=>$model->id));              
              // $mensaje=$this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Exito! ya puede realizar otro registro.'));
               
               
            }
            
        }

        $this->render('create',array(
            'model'=>$model,
            
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {

          $idDecoded = $this->getIdDecoded($id);
         $model = $this->loadModel($idDecoded);
         if($this->hasPost('RestriccionEstatus'))
            {               
                $model->attributes=$this->getPost('RestriccionEstatus');
                $model->usuario_act_id=Yii::app()->user->id;
                $model->fecha_act=date('Y-m-d H:i:s');
               
                
                if($model->save()){
                  
                     Yii::app() -> user -> setFlash("//msgBox", "<p>Actualizado con Exito sus datos son: <br></p>");
     
            }
            
                }

            $this->render('update',array(
                    'model'=>$model,
               
                    
            ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {

         if (isset($_POST['id'])) {
                $id = $_POST['id'];
                $id = base64_decode($id);
                $model = $this->loadModel($id);
                if ($model) {
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_elim = date("Y-m-d H:i:s");
                    $model->estatus = "I";
                    if ($model->save()) {
                        //$this->registerLog('ESCRITURA', 'catalogo.aplicacion.borrar', 'EXITOSO', 'Se ha eliminado un Servicio');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Inhabilitado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } else {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            }
    }
    /**
        * Activate a particular model.
        * If activation is successful, the browser will be redirected to the 'admin' page.
        * @param integer $id the ID of the model to be deleted
        */
     
    public function actionActivar($id) 
    {

            if (isset($_POST['id'])) 
            {
                $id = $_POST['id'];
                $id = base64_decode($id);
                $model = $this->loadModel($id);
                if ($model) 
                {
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_act = date("Y-m-d H:i:s");
                    $model->estatus = "A";
                    if ($model->save()) 
                    {
  
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activado con éxito.'));
                        $model = $this->loadModel($id);
                    } 
                    else 
                    {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } 
                else 
                {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            } // fin de if si se recibe el id via post

        }  // f

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return RestriccionEstatus the loaded model
     * @throws CHttpException
     */
    public function getEstatus($data) {
        $estatus = $data["estatus"];
        $columna = strtr($estatus, array('A'=>'Activo', 'I'=>'Inactivo',));
        return $columna;
    }
    
    public function loadModel($id)
    {
        $model=RestriccionEstatus::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param RestriccionEstatus $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='restriccion-estatus-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {

        $id = $data["id"];
		$id=base64_encode($id);
                 
                $botones = '<div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">';
                
		if($data->estatus=="I")
                {           
                           $botones.= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver Datos", 'href' => '/catalogo/restriccionEstatus/consulta/id/'.$id)) . '&nbsp;&nbsp;';
                           $botones.= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", "onClick" => "VentanaDialog('$id','/catalogo/restriccionEstatus/activar','Activar','activar')")) . '&nbsp;&nbsp;';
                           
								
                   
                }
		else
                {
                    
		$botones.= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver Datos", 'href' => '/catalogo/restriccionEstatus/consulta/id/'.$id)) . '&nbsp;&nbsp;';
		$botones.= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/catalogo/restriccionEstatus/edicion/id/'.$id)) . '&nbsp;&nbsp;';
		$botones.= CHtml::link("","",array("class"=>"fa fa-trash-o red","title"=>"Inactivar", "onClick"=>"VentanaDialog('$id','/catalogo/restriccionEstatus/eliminacion','Inactivar','borrar')")).'&nbsp;&nbsp;';
                }
                
                $botones.="</div>";
		return $botones;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}