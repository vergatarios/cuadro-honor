<?php

class SeccionController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Agregar Secciones',
        'write' => 'Agregar Secciones',
        'label' => 'Agregar Secciones'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('confirmarEliminacion', 'mostrarSeccionForm', 'activarSeccion', 'vizualizar', 'mostrarSeccion', 'index'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('eliminarSeccion', 'modificarSeccion', 'guardarSeccion'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function columnaAcciones($data) {

        $id = $data->id;
        $estatus = $data->estatus;
        if (($estatus == 'A') || ($estatus == '')) {
            $columna = '<div class="left" class="action-buttons">';
            $columna .= CHtml::link("", "", array('onClick' => "vizualizar('" . base64_encode($id) . "')", "class" => "fa fa-search", "title" => "Consultar la Sección")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array('onClick' => "mostrarSeccion('" . base64_encode($id) . "')", "class" => "fa fa-pencil green", "title" => "Modificar la Sección")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array('onClick' => "confirmarEliminacion('" . base64_encode($id) . "')", "class" => "icon-trash red remove-data", "title" => "Inactivar la Sección"));
            $columna .= '</div>';
        } else if ($estatus == 'E') {
            $columna = '<div class="left" class="action-buttons"> ';
            $columna .= CHtml::link("", "", array('onClick' => "vizualizar('" . base64_encode($id) . "')", "class" => "fa fa-search", "title" => "Consultar la Sección")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array('onClick' => "activarSeccion('" . base64_encode($id) . "')", "class" => "fa icon-ok green", "title" => "Activar la Sección")) . '&nbsp;&nbsp;';
            $columna .= '</div>';
        }

        return $columna;
    }

    public function estatusSeccion($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'E') {
            return 'Inactivo';
        }
    }

    public function actionActivarSeccion($id) {

        $model = new Seccion;
        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "No se ha encontrado la sección que ha solicitado para Activar. Recargue la página e intentelo de nuevo.";
            Yii::app()->user->setFlash('mensajeError', "$mensaje");
            $this->renderPartial('//flashMsgv2');
            // Yii::app()->end();
        } else {
            $model = Seccion::model()->findByPk($seccionId);
            $idSeccion = $model->id;
            //  $nombre = trim(strtoupper($_REQUEST['Seccion']['nombre']));


            $resultadoEliminacion = $model->activarSeccion($idSeccion);

            if ($resultadoEliminacion == 1) {
                $this->registerLog('ACTIVAR', 'catalogo.seccion.ActivarSeccion', 'EXITOSO', 'Permite activar un registro de una sección');
                Yii::app()->user->setFlash('mensajeExitoso', "Activación Exitosa de la Sección:" . '&nbsp;&nbsp;' . $model['nombre']);
                $this->renderPartial('//flashMsg');
            } else { // error que no guardo
                Yii::app()->user->setFlash('mensajeError', "Registro invalido, por favor intente nuevamente");
                $this->renderPartial('//flashMsgv2');
                Yii::app()->end();
            }
        }
    }

    public function actionEliminarSeccion($id) {

        $model = new Seccion;
        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "No se ha encontrado la sección que ha solicitado para Inactivar. Recargue la página e intentelo de nuevo.";
            Yii::app()->user->setFlash('mensajeError', "$mensaje");
            $this->renderPartial('//flashMsgv2');
            // Yii::app()->end();
        } else {
            $model = Seccion::model()->findByPk($seccionId);
            $idSeccion = $model->id;
            //  $nombre = trim(strtoupper($_REQUEST['Seccion']['nombre']));


            $resultadoEliminacion = $model->eliminarSeccion($idSeccion);

            if ($resultadoEliminacion == 1) {
                $this->registerLog('INACTIVAR', 'catalogo.seccion.EliminarSeccion', 'EXITOSO', 'Permite inactivar un registro de una sección');
                Yii::app()->user->setFlash('mensajeExitoso', "Inactivación Exitosa de la Sección:" . '&nbsp;&nbsp;' . $model['nombre']);
                $this->renderPartial('//flashMsg');
            } else { // error que no guardo
                Yii::app()->user->setFlash('mensajeError', "Registro invalido, por favor intente nuevamente");
                $this->renderPartial('//flashMsgv2');
                Yii::app()->end();
            }
        }
    }

    public function actionVizualizar($id) {
        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 45px'>" . "<b> No se ha encontrado la sección que ha solicitado para consultar. Recargue la página e intentelo de nuevo. </b>" . "</div>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
            // Yii::app()->end();
        } else {
            $model = Seccion::model()->findByPk($seccionId);
            $this->renderPartial('vizualizar', array(
                'model' => $model,
            ));
        }
    }

    public function actionMostrarSeccion($id) {

        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 45px'>" . "<b> No se ha encontrado la sección que ha solicitado para modificar. Recargue la página e intentelo de nuevo. </b>" . "</div>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
            // Yii::app()->end();
        } else {
            $model = Seccion::model()->findByPk($seccionId);
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('_form', array('model' => $model), false, true);
        }
    }

    public function actionMostrarSeccionForm() {

        $model = new Seccion;
        $this->renderPartial('_form', array('model' => $model));
    }

    public function actionGuardarSeccion() {

        $model = new Seccion;
        if (isset($_POST['Seccion'])) {

            $nombre = trim(strtoupper($_REQUEST['Seccion']['nombre']));
            $model->nombre = $nombre;
            $mensaje = "";

            if ($model->validate()) {

                if (is_numeric($nombre)) {

                    $mensaje.= "El campo nombre de sección solo puede contener Letras <br>";
                } else {
                    $nombre = trim(strtoupper($_REQUEST['Seccion']['nombre']));
                }

                //compruebo que los caracteres sean los permitidos
                if ($nombre != '') {
                    $permitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    for ($i = 0; $i < strlen($nombre); $i++) {
                        if (strpos($permitidos, substr($nombre, $i, 1)) === false) {
                            // echo $nombre . " no es válido<br>";
                            $resultado = false;
                        } else {
                            // echo $nombre . " es válido<br>";
                            $nombreValido = $nombre;
                            $resultado = true;
                        }
                    }
                }

                if ($mensaje != null) {
                    Yii::app()->user->setFlash('mensajeError', "$mensaje");
                    $this->renderPartial('//flashMsgv2');
                    Yii::app()->end();
                }

                if ($resultado == true) {

                    $usuario_id = Yii::app()->user->id;
                    $estatus = 'A';
                    $model->nombre = $nombreValido;
                    $model->estatus = $estatus;
                    $model->usuario_ini_id = $usuario_id;

                    if ($model->save()) {
                        $seccionn_id = $model->id; //obtengo id de la seccion del registro que acabo de guardar.
                        $this->registerLog('ESCRITURA', 'catalogo.seccion.GuardarSeccion', 'EXITOSO', 'Permite guardar un registro de una sección');

                        $mensaje = "$seccionn_id";
                        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                        $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
                        // Yii::app()->end();
                    } else { // error que no guardo
                        Yii::app()->user->setFlash('mensajeError', "Registro invalido, por favor intente nuevamente");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                    }
                } else { // si ingresa datos erroneos muestra mensaje de error.
                    Yii::app()->user->setFlash('mensajeError', "Por favor ingrese los datos correctos el nombre de la sección solo puede contener letras");
                    $this->renderPartial('//flashMsgv2');
                    Yii::app()->end();
                }
            } else { // si ingresa datos erroneos muestra mensaje de error.
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                Yii::app()->end();
            }
        } else {

            throw new CHttpException(404, 'No se ha especificado la sección que desea agregar. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionModificarSeccion($id) {

        $model = new Seccion;
        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 45px'>" . "No se ha encontrado la sección que ha solicitado para modificar. Recargue la página e intentelo de nuevo." . "</div>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
            // Yii::app()->end();
        } else {
            $model = Seccion::model()->findByPk($seccionId);

            if (isset($_POST['id'])) {
                $nombre = $_REQUEST['nombre'];

                $model->nombre = $nombre;
                $mensaje = "";

                if ($model->validate()) {

                    if (is_numeric($_REQUEST['nombre'])) {

                        $mensaje.= "El campo nombre de sección solo puede contener Letras <br>";
                    } else {
                        $nombre = trim(strtoupper($_REQUEST['nombre']));
                    }

                    /*  VALIDO QUE LA SECCION NO SE REPITA  */
                    $validaSeccion = $model->validaNombreSeccion($nombre);

                    /*     FIN      */


                    foreach ($validaSeccion as $value) {
                        $seccionId = $value["id"];
                    }
                    if ($seccionId != $model->id) {

                        if ($validaSeccion != array()) {
                            $mensaje.="El nombre de sección no se puede repetir";
                        }
                    }

                    //compruebo que los caracteres sean los permitidos
                    if ($nombre != '') {
                        $permitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        for ($i = 0; $i < strlen($nombre); $i++) {
                            if (strpos($permitidos, substr($nombre, $i, 1)) === false) {
                                // echo $nombre . " no es válido<br>";
                                $resultado = false;
                            } else {
                                // echo $nombre . " es válido<br>";
                                $nombreValido = $nombre;
                                $resultado = true;
                            }
                        }
                    }

                    if ($mensaje != null) {
                        Yii::app()->user->setFlash('mensajeError', "$mensaje");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                    }
                    if ($resultado == true) {
                        if ($model != null) {

                            $usuario_id = Yii::app()->user->id;
                            $estatus = 'A';
                            $fecha = date('Y-m-d H:i:s');
                            $model->nombre = $nombreValido;
                            $model->estatus = $estatus;
                            $model->usuario_act_id = $usuario_id;
                            $model->fecha_act = $fecha;
                            //  var_dump($model);
                            if ($model->save()) {
                                $seccionn_id = $model->id; //obtengo id de la seccion del registro que acabo de guardar.
                                // var_dump($seccionn_id);
                                $this->registerLog('ACTUALIZACION', 'catalogo.seccion.ModificarSeccion', 'EXITOSO', 'Permite modificar un registro de una sección');
                                $mensaje = "$seccionn_id";
                                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                                $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
                                // Yii::app()->end();
                            } else { // error que no guardo
                                Yii::app()->user->setFlash('mensajeError', "Registro invalido, por favor intente nuevamente");
                                $this->renderPartial('//flashMsgv2');
                                Yii::app()->end();
                            }
                        } else {

                            Yii::app()->user->setFlash('mensajeError', "Por favor seleccione un registro para modificar");
                            $this->renderPartial('//flashMsgv2');
                            Yii::app()->end();
                        }
                    } else { // si ingresa datos erroneos muestra mensaje de error.
                        Yii::app()->user->setFlash('mensajeError', "Por favor ingrese los datos correctos el nombre de la sección solo puede contener letras");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                    }
                } else { // si ingresa datos erroneos muestra mensaje de error.
                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                    Yii::app()->end();
                }
            } else {

                throw new CHttpException(404, 'No se ha especificado la sección que desea agregar. Recargue la página e intentelo de nuevo.'); // esta vacio el request
            }
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex() {

        $model = new Seccion('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Seccion']) || isset($_GET['ajax'])) {
            if (isset($_GET['Seccion']))
                $model->attributes = $_GET['Seccion'];
            $this->render('admin', array(
                'model' => $model,
            ));
        } else {
            $url = $_SERVER['REQUEST_URI'];
            $validaUrl = '/catalogo/seccion/';
            if ($url != $validaUrl) {
                $this->redirect('../catalogo');
                //throw new CHttpException(404, "Dirección inválida.");
            } else {
                $this->render('admin', array(
                    'model' => $model,
                ));
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Seccion the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Seccion::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Seccion $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'seccion-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
