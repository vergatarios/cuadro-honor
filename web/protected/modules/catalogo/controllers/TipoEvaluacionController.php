<?php

class TipoEvaluacionController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de TipoEvaluacionController',
        'write' => 'Creación y Modificación de TipoEvaluacionController',
        'admin' => 'Administración Completa  de TipoEvaluacionController',
        'label' => 'Módulo de TipoEvaluacionController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'admin','activar'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $model=new TipoEvaluacion('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('TipoEvaluacion')){
            $model->attributes=$this->getQuery('TipoEvaluacion');
        }
        $dataProvider = $model->search();
        $this->render('index',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {     
        $groupId = Yii::app()->user->group;
        $model = new TipoEvaluacion('search');
        if (isset($_GET['TipoEvaluacion']))
            $model->attributes = $_GET['TipoEvaluacion'];
        $usuarioId = Yii::app()->user->id;
        $dataProvider = new CActiveDataProvider('TipoEvaluacion');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {      
         $idDecode = base64_decode($id);
            if (is_numeric($idDecode)) {
            $this->renderPartial('view', array(
                'model' => $this->loadModel($idDecode),
            ));
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {   
         $model = new TipoEvaluacion;
        if ($model) {

            if (isset($_POST['TipoEvaluacion'])) {
                $model->attributes = $_POST['TipoEvaluacion'];
                $nombre = trim($_POST['TipoEvaluacion']['nombre']);
                $nombre = strtoupper($nombre);
                $model->nombre = $nombre;
                $model->usuario_ini_id = Yii::app()->user->id;
                $model->fecha_ini = date("Y-m-d H:i:s");
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->estatus = "A";

                if ($model->validate()) {
                    if ($model->save()) {
                        $this->registerLog(
                                "ESCRITURA", "registro", "Exitoso", "Se creo un Tipo de Evaluación"
                        );
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Exito! ya puede realizar otro registro.'));
                        $model = new TipoEvaluacion;
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                }
            }
        } else {

            throw new CHttpException(404, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
        }

        $this->renderPartial('_form', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {
        $id = $_REQUEST['id'];
        $id = base64_decode($id);
        $model = $this->loadModel($id);
       if (isset($_POST['TipoEvaluacion'])) {
           $model->attributes = $_POST['TipoEvaluacion'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            if ($model->save())
                if ($model->validate()){
                    if ($model->save()){
                        $this->registerLog('ESCRITURA', 'catalogo.tipoEvaluacion.create', 'EXITOSO', 'Se ha creado un servicio');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }

                  } else {
                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                }
                }
         $this->renderPartial('_form', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id)
    {
          if (isset($_POST['id'])) {
                $id = $_POST['id'];
                $id = base64_decode($id);
                $model = $this->loadModel($id);
                if ($model) {
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_elim = date("Y-m-d H:i:s");
                    $model->estatus = "I";
                    if ($model->save()) {
                        //$this->registerLog('ESCRITURA', 'catalogo.especificacionestatus.borrar', 'EXITOSO', 'Se ha eliminado un Servicio');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Inhabilitado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } else {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            }
    }
    public function actionActivar($id)
        {
            if (isset($_POST['id'])) {
                $id = $_POST['id'];
                $id = base64_decode($id);
                $model = $this->loadModel($id);
                if ($model) {
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_elim = date("Y-m-d H:i:s");
                    $model->estatus = "A";
                    if ($model->save()) {
                        //$this->registerLog('ESCRITURA', 'catalogo.especificacionestatus.activar', 'EXITOSO', 'Se ha activado una Servicio');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } else {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            }
        }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return TipoEvaluacion the loaded model
     * @throws CHttpException
     */
    public function getEstatus($data) {
        $estatus = $data["estatus"];
        $columna = strtr($estatus, array('A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado',));
        return $columna;
    }

    public function getFechaIni($data) {
        $result = $data['fecha_ini'];

        $result = Utiles::transformDate($result, '-', 'y-m-d', 'd-m-y');

        return $result;
    }

    public function getFechaAct($data) {
        $result = $data['fecha_act'];

        $result = Utiles::transformDate($result, '-', 'y-m-d', 'd-m-y');

        return $result;
    }

    public function loadModel($id) {
        $model = TipoEvaluacion::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param TipoEvaluacion $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='tipo-evaluacion-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id = $data["id"];
        $id = base64_encode($id);
        $estatus = $data['estatus'];
        if ($estatus == 'I') {
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "title" => "Buscar Tipo de Evaluación", "onClick" => "VentanaDialog('$id','/catalogo/tipoEvaluacion/consulta','Vista de Tipo de Evaluación','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-check", "title" => "Activar Tipo de Evaluación", "onClick" => "VentanaDialog('$id','/catalogo/tipoEvaluacion/activar','Activar Tipo de Evaluación','activar')")) . '&nbsp;&nbsp;';
            
        } else {
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "title" => "Buscar Tipo de Evaluación", "onClick" => "VentanaDialog('$id','/catalogo/tipoEvaluacion/consulta','Vista de Tipo de Evaluación','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green", "title" => "Modificar Tipo de Evaluación", "onClick" => "VentanaDialog('$id','/catalogo/tipoEvaluacion/edicion','Modificar Tipo de Evaluación','update')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Inhabilitar Tipo de Evaluación", "onClick" => "VentanaDialog('$id','/catalogo/tipoEvaluacion/eliminacion','Inhabilitar Tipo de Evaluación','borrar')")) . '&nbsp;&nbsp;';
        }
        return $columna;
    }

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
//    public function getIdDecoded($id){
//        if(is_numeric($id)){
//            return $id;
//        }
//        else{
//            $idDecodedb64 = base64_decode($id);
//            if(is_numeric($idDecodedb64)){
//                return $idDecodedb64;
//            }
//        }
//        return null;
//    }
}