<?php

class NivelController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Gestion de Nivel',
        'write' => 'Gestion de Nivel',
        'label' => 'Gestion de Nivel'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'activarNivel', 'view', 'informacion', 'modificar', 'procesarCambio', 'registrar', 'crear', 'eliminarNivel', 'grado', 'cargarGrado', 'quitarGrado', 'agregarPlan', 'cargarPlan', 'quitarPlan'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionInformacion($id) {
        $this->renderPartial('informacion', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionModificar($id) {
        //$tipoPeriodo = TipoPeriodo::model()->findAll(array('order' => 'id ASC'));

        /* SELECCION DE PERMITE MATERIA PENDIENTE 1 = SI 0 = NO */
        $tipoPeriodo = TipoPeriodo::model()->findAll(array('order' => 'id ASC'));
        $model = $this->loadModel($id);
        if ($model->permite_materia_pendiente == 0)/* NO */ {
            $dropDownList = CHtml::dropDownList('permite_materia_pendiente', $model, array('' => '- SELECCIONE -', '1' => 'Si', '0' => 'No'), array('options' => array('0' => array('selected' => true)), 'style' => 'width:100%'));
        }
        if ($model->permite_materia_pendiente == 1)/* SI */ {
            $dropDownList = CHtml::dropDownList('permite_materia_pendiente', $model, array('' => '- SELECCIONE -', '1' => 'Si', '0' => 'No'), array('options' => array('1' => array('selected' => true)), 'style' => 'width:100%'));
        }

        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model,
            'dropDownList' => $dropDownList,
            'tipoPeriodo' => $tipoPeriodo,
                ), FALSE, TRUE);
    }

    public function actionEliminarNivel($id) {

        $model = Nivel::model()->findByPk($id);

        if ($model != null) {
            $nivel_id = $model->id;
            $resultadoEliminar = Nivel::model()->eliminarNivel($nivel_id);
            //var_dump($resultadoActivar);die();
            if ($resultadoEliminar == 1) {
                $this->registrarTraza('Inactivo el Nivel', 'EliminarNivel');
                $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Eliminado con exito.'));
            } else {
                throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
            }
        } else {
            throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
        }
    }

    public function actionActivarNivel($id) {
        // $model = new Nivel;
        $model = Nivel::model()->findByPk($id);

        if ($model != null) {
            $nivel_id = $model->id;

            $resultadoActivar = Nivel::model()->activarNivel($nivel_id);
            if ($resultadoActivar == 1) {
                $this->registrarTraza('Activo el Nivel', 'ActivarNivel');
                $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activación con exito.'));
            } else {
                throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
            }
        } else {
            throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
        }
    }

    public function actionCrear() {
        $model = new Nivel;
        $tipoPeriodo = TipoPeriodo::model()->findAll(array('order' => 'id ASC'));

        //var_dump($_REQUEST);die();
        if (isset($_POST['Nivel'])) {
            //var_dump($_REQUEST);die();
            $model->nombre = $_POST['Nivel']['nombre'];
            $model->tipo_periodo_id = $_POST['Nivel']['nivel'];
            $model->cantidad = $_POST['Nivel']['cantidad'];
            $model->cant_lapsos = $_POST['Nivel']['cant_lapsos'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = 'A';


            //var_dump('ALGO');die();
            if ($model->validate()) {
                if ($model->save()) {
                    $this->registrarTraza('Guardo en Nivel', 'Crear');
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso.'));
                    //$this->registrarTraza('Registro un nivel ' . $plantel_id, 'CrearNivel');
                }
            } else { // si ingresa datos erroneos muestra mensaje de error.
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                //Yii::app()->end();
            }
        }

        $dropDownList = CHtml::dropDownList('permite_materia_pendiente', $model, array('' => '- SELECCIONE -', '1' => 'Si', '0' => 'No'));

        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model,
            'dropDownList' => $dropDownList,
            'tipoPeriodo' => $tipoPeriodo,
        ));
    }

    public function actionProcesarCambio() {

        $model = new Nivel;
        $tipoPeriodo = TipoPeriodo::model()->findAll(array('order' => 'id ASC'));
        if (isset($_POST['Nivel'])) {
            $id = (int) $_POST['Nivel']['id'];
            $model = $this->loadModel($id);
            $model = Nivel::model()->findByPk($id);
            $model->nombre = $_POST['Nivel']['nombre'];
            $model->tipo_periodo_id = $_POST['Nivel']['nivel'];
            $model->cantidad = $_POST['Nivel']['cantidad'];
            $model->cant_lapsos = $_POST['Nivel']['cant_lapsos'];
            $model->permite_materia_pendiente = $_POST['Nivel']['permite_materia_pendiente'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = 'A';

            $nombre = $model->nombre;
            $nombre = trim(strtoupper($nombre));
            $model->nombre = $nombre;

            //var_dump(trim(strtoupper($nombre)));die();

            if ($model->validate()) {

                if ($model->save()) {
                    $this->registrarTraza('Modifico el Nivel', 'ProcesarCambio');
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
                    //$this->registrarTraza('Eliminó un servicio al Plantel ' . $plantel_id, 'EliminarServicio');
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else { // si ingresa datos erroneos muestra mensaje de error.
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                //Yii::app()->end();
            }
        } else {
            Yii::app()->user->setFlash('error', "No se ha podido completar la última operación, ID invalido.");
        }

        if ($model->permite_materia_pendiente == 0)/* NO */ {
            $dropDownList = CHtml::dropDownList('permite_materia_pendiente', $model, array('' => '- SELECCIONE -', '1' => 'Si', '0' => 'No'), array('options' => array('0' => array('selected' => true)), 'style' => 'width:100%'));
        }
        if ($model->permite_materia_pendiente == 1)/* SI */ {
            $dropDownList = CHtml::dropDownList('permite_materia_pendiente', $model, array('' => '- SELECCIONE -', '1' => 'Si', '0' => 'No'), array('options' => array('1' => array('selected' => true)), 'style' => 'width:100%'));
        }
        $this->renderPartial('_form', array(
            'model' => $model,
            'dropDownList' => $dropDownList,
            'tipoPeriodo' => $tipoPeriodo,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistrar() {
        $model = new Nivel;
        //Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $tipoPeriodo = TipoPeriodo::model()->findAll(array('order' => 'id ASC'));
        $dropDownList = CHtml::dropDownList('permite_materia_pendiente', $model, array('' => '- SELECCIONE -', '1' => 'Si', '0' => 'No'), array('style' => 'width:100%'));
        $this->renderPartial('_form', array(
            'model' => $model,
            'dropDownList' => $dropDownList,
            'tipoPeriodo' => $tipoPeriodo,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

        $model = new Nivel('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Nivel']) || isset($_GET['ajax'])) {
            if (isset($_GET['Nivel']))
                $model->attributes = $_GET['Nivel'];
            $this->render('index', array(
                'model' => $model,
            ));
        } else {
            $url = $_SERVER['REQUEST_URI']; // validar que la url sea la correcta
            $validaUrl = '/catalogo/nivel/';
            if ($url != $validaUrl) {
                $this->redirect('../catalogo');
                //throw new CHttpException(404, "Dirección inválida.");
            } else {
                $this->render('index', array(
                    'model' => $model,
                ));
            }
        }
        $this->registrarTraza('Entro en Nivel', 'Index');
    }

    public function actionGrado() {
        $id = $_REQUEST['id'];
        $id = base64_decode($id);
        $model = $this->loadModel($id);
        $grado = Grado::model()->findAll(array('order' => 'modalidad_id ASC, consecutivo ASC'));
        $this->render('_grado', array(
            'model' => $model,
            'grado' => $grado,
        ));
    }

    public function actionCargarGrado() {
        if (!empty($_GET['id']) && !empty($_GET['grado_id'])) {
            $resultado = Grado::model()->registroGrado($_GET['id'], $_GET['grado_id'], Yii::app()->user->id);
            $this->registrarTraza('Asigno un grado al Nivel', 'CargarGrado');
            $lista = Grado::model()->obtenerGrado($_GET['id'], Yii::app()->user->id);

            /* $id = $_REQUEST['id'];
              $id = base64_decode($id);
              $model = $this->loadModel($id);
              $grado = Grado::model()->findAll();

              $this->renderPartial('_grado',array(
              'model' => $model,
              'grado' => $grado)); */
            $dataProvider = new CArrayDataProvider($lista, array(
                'pagination' => array(
                    'pageSize' => 5,
                )
            ));
            $this->widget('zii.widgets.grid.CGridView', array(
                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                'id' => 'modalidad-nivel-grid',
                'dataProvider' => $dataProvider,
                'summaryText' => false,
                'pager' => array(
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                ),
                'columns' => array(
                    array(
                        'header' => '<center>Nombre</center>',
                        'name' => 'nombre',
                    ),
                    array(
                        'type' => 'raw',
                        'header' => '<center>Acciones</center>',
                        'value' => array($this, 'columna')
                    ),
                ),
            ));
        }
    }

    public function actionQuitarGrado() {
        if (!empty($_GET['id'])) {

            $resultado = Grado::model()->eliminarGrado($_GET['id']);
            $this->registrarTraza('Elimino un grado de Nivel', 'QuitarGrado');
            $lista = Grado::model()->obtenerGrado(base64_encode($_GET['nivel_id']), Yii::app()->user->id);
            $dataProvider = new CArrayDataProvider($lista, array(
                'pagination' => array(
                    'pageSize' => 5,
                )
            ));
            $this->widget('zii.widgets.grid.CGridView', array(
                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                'id' => 'modalidad-nivel-grid',
                'dataProvider' => $dataProvider,
                'summaryText' => false,
                'pager' => array(
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                ),
                'columns' => array(
                    array(
                        'header' => '<center>Nombre</center>',
                        'name' => 'nombre',
                    ),
                    array(
                        'type' => 'raw',
                        'header' => '<center>Acciones</center>',
                        'value' => array($this, 'columna')
                    ),
                ),
            ));
        }
    }

    public function actionAgregarPlan($id) {

        $nivel_id = base64_decode($id);
        // var_dump($id);die();

        if (is_numeric($nivel_id)) {

            $model = $this->loadModel($nivel_id);

            $planesDisponibles = new Plan();

            if ($this->getQuery('ajax') == 'plan-grid') {
                $planesDisponibles->attributes = $this->getQuery('Plan');
            }

            $dataProviderPlanesDisp = $planesDisponibles->searchPlanDisponiblesNivel($nivel_id);

            $this->render('_plan', array(
                'model' => $model,
                'dataProviderPlanesDisp' => $dataProviderPlanesDisp,
                'planesDisponibles' => $planesDisponibles,
            ));
        } else {
            throw new CHttpException(404, "No se ha encontrado el nivel que ha solicitado. Recargue la página e intentelo de nuevo.");
        }
    }

    public function actionCargarPlan() {

        if (!empty($_GET['id_plan']) && !empty($_GET['id_nivel'])) {
            $id_nivel = base64_decode($_GET['id_nivel']);
            $id_plan = base64_decode($_GET['id_plan']);


            if (is_numeric($id_nivel) && is_numeric($id_plan)) {
                // var_dump($id_plan);die();
                $model = new NivelPlan;
                $model->nivel_id = $id_nivel;
                $model->plan_id = $id_plan;
                $model->usuario_ini_id = Yii::app()->user->id;
                $model->fecha_ini = date("Y-m-d H:i:s");
                $model->estatus = "A";
                if (NivelPlan::model()->findByAttributes(array('nivel_id' => $id_nivel, 'plan_id' => $id_plan))) {
                    echo '<div class="alertDialogBox col-md-6"><p class="bolder center grey"> ¡Ya se ha asignado este plan!</p></div>';
                } else {
                    if ($model->save()) {

                        $descripcion = 'se asigno el plan :' . $id_plan . ' al nivel :' . $id_nivel;
                        $this->registerLog('ESCRITURA', 'catalogo.nivel.cargarPlan', 'EXITOSO', $descripcion);
                    }
                }


                $NivelPlan = NivelPlan::model()->obtenerPlanNivel($id_nivel);

                $dataProviderNivelPlan = new CArrayDataProvider($NivelPlan, array(
                    'pagination' => array(
                        'pageSize' => 15,
                    )
                ));
                $this->widget('zii.widgets.grid.CGridView', array(
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'id' => 'nivel-plan-grid',
                    'dataProvider' => $dataProviderNivelPlan,
                    'summaryText' => false,
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'columns' => array(
                        array(
                            'header' => '<center>Codigo</center>',
                            'name' => 'cod_plan',
                        ),
                        array(
                            'header' => '<center>Nombre</center>',
                            'name' => 'nombre',
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Acciones</center>',
                            'value' => array($this, 'quitarPlan')
                        ),
                    ),
                ));
            } else {
                throw new CHttpException(404, "No se ha encontrado el plan o el nivel que ha solicitado. Recargue la página e intentelo de nuevo.");
            }
        } else {
            throw new CHttpException(500, "No se han cargado los datos necesarios. Recargue la página e intentelo de nuevo.");
        }
    }

    public function actionQuitarPlan() {

        if (!empty($_GET['id_plan_nivel']) && !empty($_GET['id_nivel'])) {
            $id_nivel = base64_decode($_GET['id_nivel']);
            $plan_nivel_id = base64_decode($_GET['id_plan_nivel']);

            if (is_numeric($id_nivel) && is_numeric($plan_nivel_id)) {
                $resultado = NivelPlan::model()->eliminarPlanNivel($plan_nivel_id, $id_nivel);
                if ($resultado) {
                    $descripcion = 'se Elimino el planNivel :' . $plan_nivel_id;
                    $this->registerLog('ELIMINAR', 'catalogo.nivel.quitarPlan', 'EXITOSO', $descripcion);
                }
                $NivelPlan = NivelPlan::model()->obtenerPlanNivel($id_nivel);

                $dataProviderNivelPlan = new CArrayDataProvider($NivelPlan, array(
                    'pagination' => array(
                        'pageSize' => 15,
                    )
                ));
                $this->widget('zii.widgets.grid.CGridView', array(
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'id' => 'nivel-plan-grid',
                    'dataProvider' => $dataProviderNivelPlan,
                    'summaryText' => false,
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'columns' => array(
                        array(
                            'header' => '<center>Codigo</center>',
                            'name' => 'cod_plan',
                        ),
                        array(
                            'header' => '<center>Nombre</center>',
                            'name' => 'nombre',
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Acciones</center>',
                            'value' => array($this, 'quitarPlan')
                        ),
                    ),
                ));
            } else {//no es numerico
                throw new CHttpException(404, "No se ha encontrado el plan o el nivel que ha solicitado. Recargue la página e intentelo de nuevo.");
            }
        } else {//no existe
            throw new CHttpException(500, "No se han cargado los datos necesarios. Recargue la página e intentelo de nuevo.");
        }
    }

    public function columna($l) {
        $columna = CHtml::link("", "", array("class" => "fa fa-trash-o red", "onClick" => 'quitar(' . $l["id"] . ',' . $l["nivel_id"] . ');', "value" => '$l["id"]')) . '&nbsp;&nbsp;';
        return $columna;
    }
    
        public function mencion($l) {
      
            if($l['mencion_id']!=NULL){
                $valor=$l['mencion']['nombre'];
            }else{
                 $valor="";
            }
            
        return $valor;
    }

    public function asignarPlan($data) {
        if (is_numeric($data["id"])) {
            $id = base64_encode($data["id"]);
            $columna = CHtml::link("", "", array("class" => "fa fa-plus green", "title" => "Asignar Plan " . $data["cod_plan"] . "", "onClick" => 'agregarPlan("' . $id . '");', "value" => '$data["id"]')) . '&nbsp;&nbsp;';
            return $columna;
        } else {
            throw new CHttpException(404, "No se ha encontrado el plan o el nivel que ha solicitado. Recargue la página e intentelo de nuevo.");
        }
    }

    public function quitarPlan($data) {
        if (is_numeric($data["id"])) {
            $id = base64_encode($data["id"]);
            $columna = CHtml::link("", "", array("class" => "fa fa-trash-o red", "title" => "Quitar Plan ", "onClick" => 'quitarPlan("' . $id . '");', "value" => '$data["id"]')) . '&nbsp;&nbsp;';
            return $columna;
        } else {
            throw new CHttpException(404, "No se ha encontrado el plan o el nivel que ha solicitado. Recargue la página e intentelo de nuevo.");
        }
    }

    public function columnaAcciones($data)
    /*
     * Botones del accion (crear, consultar)
     */ {
        $id = $data["id"];
        $estatus = $data["estatus"];
        
        $columna = '<div class="btn-group">
                        <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
                            Seleccione
                            <span class="icon-caret-down icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-yellow pull-right">';
        
        if (($estatus == 'A') || ($estatus == '')) {
            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Consultar nivel</span>", '', array("class" => "fa fa-search-plus", "title" => "Consultar nivel", "onClick" => "consultarNivel($id)")) . '</li>';
            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Modificar nivel</span>", '', array("class" => "fa fa-pencil green", "title" => "Modificar nivel", "onClick" => "modificarNivel($id)")) . '</li>';
            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Asignar Planes</span>", "/catalogo/nivel/agregarPlan/id/" . base64_encode($id), array("class" => "fa fa-book red", "title" => "Asignar Planes a este Nivel")) . '</li>';
            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Asignar Grados</span>", "/catalogo/nivel/grado?id=" . base64_encode($id), array("class" => "fa fa-sitemap orange", "title" => "Asignar Grados a este Nivel")) . '</li>';
            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Inactivar nivel</span>", '', array("class" => "fa fa-trash-o red", "title" => "Inactivar nivel", "onClick" => "eliminarNivel($id)")) . '</li>';
        } else if ($estatus == 'E') {
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarNivel($id)", "title" => "Consultar este nivel")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array('onClick' => "activarNivel($id)", "class" => "fa icon-ok green", "title" => "Activar este nivel")) . '&nbsp;&nbsp;';
        }
        $columna .= '</ul></div>';

        return $columna;
    }

    public function columnaEstatus($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'E') {
            return 'Inactivo';
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Nivel('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Nivel']))
            $model->attributes = $_GET['Nivel'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $modulo = "Planteles.CrearController." . $accion;
        $Utiles->traza($transaccion, $modulo, date('Y-m-d H:i:s'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Nivel the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Nivel::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'ERROR (LoadModel): No se encontró el nivel suministrado.');
        return $model;
    }

    /*     * ******************************************************************************* */
    /*     * ****************************Alexis Editand************************************* */
    /*     * ******************************************************************************* */

    /**
     * Performs the AJAX validation.
     * @param Nivel $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'nivel-form') {
            echo CActiveForm::validate($model);


            Yii::app()->end();
        }
    }

}
