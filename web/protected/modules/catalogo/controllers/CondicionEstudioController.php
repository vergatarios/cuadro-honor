<?php

class CondicionEstudioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */

	 static $_permissionControl = array(
        'read' => 'Consulta de Condicion de Estudio',
        'write' => 'Gestion de Condicion de Estudio',
        'label' => 'Módulo de Condicion de estudio'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index','view'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'activar', 'delete'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
            $id=base64_decode($id);
		$this->renderPartial('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CondicionEstudio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

                
              


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CondicionEstudio']))
		{
                        $model->usuario_ini_id=Yii::app()->user->name;
                       
			$model->attributes=($_POST['CondicionEstudio']);
                         $model->nombre=strtoupper($model->nombre);
                        //$nombre->$model($nombre);
			$model->usuario_ini_id=Yii::app()->user->id;
			$model->fecha_ini=date("Y-m-d H:i:s");
			$model->estatus="A";
                        $r=strlen($model->nombre);
                        
                        
                        
                         //$id=base64_decode($id);
			if($model->validate()){
				if($model->save()){
                                    
                                    if($r<5){
                                         $this->renderPartial("//msgBox",array('class'=>'errorDialogBox','message'=>'El registro debe contener al menos 5 caracteres.'));  
                                    }else if($r>=5){
                                       $this->registerLog('ESCRITURA', 'catalogo.condicionEstudio.create', 'EXITOSO', 'Se ha creado una Mencion');
					$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Exito! ya puede realizar otro registro.'));
					$model=new CondicionEstudio;
					
				}
				else{
					throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

				}
			}
			
				
			//var_dump($_POST['ClasePlantel']);
	
		}
                }

		$this->renderPartial('create',array(
			'model'=>$model,
                  
		));
	}
        
        
                
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{ 
            
            
            $id=$_REQUEST['id'];
            $id=base64_decode($id);
		$model=$this->loadModel($id);
                //var_dump($id);
               


                // Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CondicionEstudio']))
		{
			$model->attributes=$_POST['CondicionEstudio'];
			$model->usuario_act_id=Yii::app()->user->id;
			$model->fecha_act=date("Y-m-d H:i:s");
		

			if($model->save())
				if($model->validate()){
				if($model->save()){
                                       $this->registerLog('ESCRITURA', 'catalogo.condicionEstudio.update', 'EXITOSO', 'Se ha actualizado una Mencion');
					$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Actualizado con exito.'));
					$model=$this->loadModel($id);
					
				}
				else{
					throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

				}
			}
				
		}

		$this->renderPartial('update',array(
			'model'=>$model,
		));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(isset($_POST['id']))
		{
		$id=$_POST['id'];
		//$id=base64_decode($id);
                //var_dump($id);
                $id=base64_decode($id);
		$model=$this->loadModel($id);
		if($model){
		$model->usuario_act_id=Yii::app()->user->id;
		$model->fecha_elim=date("Y-m-d H:i:s");
		$model->estatus="E";
		if($model->save()){
                   $this->registerLog('ESCRITURA', 'catalogo.condicionEstudio.delete', 'EXITOSO', 'Se ha eliminado una Mencion');
			$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Eliminado con exito.'));
					$model=$this->loadModel($id);

		}
		else{
					throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

				}

		}
		else{

		throw new CHttpException(404, 'Error! Recurso no encontrado!');		
		}
		

		}
//$this->render('borrar',array('model'=>$model,));

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
			//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		
	}


	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            
              $groupId = Yii::app()->user->group;
              
            $model=new CondicionEstudio('search');
                	
               if(isset($_GET['CondicionEstudio']))
			$model->attributes=$_GET['CondicionEstudio'];
			
        $usuarioId = Yii::app()->user->id;
		$dataProvider=new CActiveDataProvider('CondicionEstudio');
		$this->render('admin',array(
			'model'=>$model,
			'groupId'=>$groupId,
			'usuarioId'=>$usuarioId,
			'dataProvider'=>$dataProvider,
            
            
     
     
      
		
		));
	}
        
	
	public function actionAdmin()
	{
              $groupId = Yii::app()->user->group;
		$model=new CondicionEstudio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CondicionEstudio']))
			$model->attributes=$_GET['CondicionEstudio'];

		$this->render('index',array(
			'model'=>$model,
		));
           
                
                
		
                
                
	}
        
          public function estatus($data)
{
	$id = $data["estatus"];
            if($id == 'A')
            {
                $columna = 'Activo';
            }
            
            if($id == 'E')
            {
                $columna = 'Inactivo';
            }
            
		return $columna;
	}
        
        
        public function columnaAcciones($datas)
	{
  
            
		$id = $datas["id"];
                $id2= $datas["estatus"];
                $id=base64_encode($id);
                //if($id=="A"){
                if($id2=="A"){
		$columna = CHtml::link("","#",array("class"=>"fa fa-search","title"=>"Buscar esta Clase de plantel" ,"onClick"=>"VentanaDialog('$id','/catalogo/condicionEstudio/view','Condición De Estudio','view')")).'&nbsp;&nbsp;';
                $columna .= CHtml::link("","#",array("class"=>"fa fa-check","title"=>"Activar Mencion" ,"onClick"=>"VentanaDialog('$id','/catalogo/mencion/activar','Activar Condicion De Estudio','activar')")).'&nbsp;&nbsp;';

                }
                else {
                $columna = CHtml::link("","#",array("class"=>"fa fa-search","title"=>"Buscar esta Clase de plantel" ,"onClick"=>"VentanaDialog('$id','/catalogo/condicionEstudio/view','Condición De Estudio','view')")).'&nbsp;&nbsp;';
		$columna .= CHtml::link("","#",array("class"=>"fa fa-pencil green","title"=>"Modificar esta Clase de plantel", "onClick"=>"VentanaDialog('$id','/catalogo/condicionEstudio/update','Modificar Condición De Estudio','update')")).'&nbsp;&nbsp;';
		$columna .= CHtml::link("","#",array("class"=>"fa icon-trash red","title"=>"Eliminar esta Condicion de Estudio", "onClick"=>"VentanaDialog('$id','/catalogo/condicionEstudio/borrar','Eliminar Condición De Estudio','borrar')")).'&nbsp;&nbsp;';
                }
		
		return $columna;
               
	}
        
        
        
        public function actionActivar(){
	 if(isset($_POST['id']))
		{
		$id=$_POST['id'];
		$id=base64_decode($id);
		$model=$this->loadModel($id);
		if($model){
		$model->usuario_act_id=Yii::app()->user->id;
		$model->fecha_elim=date("Y-m-d H:i:s");
		$model->estatus="A";
		if($model->save()){
                    $this->registrarTraza('Se activo una condicion De estudio', 'Guardar Condicion de Estudio');
			$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Activado con exito.'));
					$model=$this->loadModel($id);

		}
		else{
					throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

				}

		}
		else{

		throw new CHttpException(404, 'Error! Recurso no encontrado!');		
		}
		

		}
//$this->render('borrar',array('model'=>$model,));

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
			//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
	
        

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CondicionEstudio the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CondicionEstudio::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CondicionEstudio $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='condicion-estudio-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
          static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $modulo = "catalogo.condicionEstudioController" . $accion;
        $Utiles->traza($transaccion, $modulo, date('Y-m-d H:i:s'));
    }

}
