<?php

class ModalidadController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';


    /* Default action */

    /**
     * @return array action filters
     */
    static $_permissionControl = array(
        'read' => 'Consulta de Modalidad',
        'write' => 'Consulta de Modalidad',
        'label' => 'Consulta de Modalidad'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('index', 'view', 'admin', 'nivel'),
                'pbac' => array('read', 'write'),
            ),
            // en este array sólo van los action de actualizacion a BD
            array('allow',
                'actions' => array('create', 'update', 'borrar', 'habilitar', 'cargarNivel', 'quitarNivel', 'activar'),
                'pbac' => array('write'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $id = base64_decode($id);
        if (isset($id)) {
            $this->renderPartial('view', array(
                'model' => $this->loadModel($id),
            ));
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Modalidad;

        if ($model) {

            if (isset($_POST['Modalidad'])) {
                $model->attributes = $_POST['Modalidad'];
                $nombre = trim($_POST['Modalidad']['nombre']);
                $nombre = strtoupper($nombre);
                $model->nombre = $nombre;
                $model->usuario_ini_id = Yii::app()->user->id;
                $model->fecha_ini = date("Y-m-d H:i:s");
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->estatus = "A";

                if ($model->validate()) {
                    if ($model->save()) {
                        $this->registerLog(
                                "ESCRITURA", "create", "Exitoso", "Se creo una Modalidad"
                        );
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Exito! ya puede realizar otro registro.'));
                        $model = new Modalidad;
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                }
            }
        } else {

            throw new CHttpException(404, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
        }

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);



        $this->renderPartial('create', array(
            'model' => $model,
        ));
    }

    public function actionCargarNivel() {
        if (!empty($_GET['id']) && !empty($_GET['nivel_id'])) {
            $nivel_id_decoded = base64_decode($_GET['nivel_id']);
            $resultado = Modalidad::model()->registroNivel($_GET['id'], $nivel_id_decoded, Yii::app()->user->id);
            $log = "Se asigno el nivel " . $nivel_id_decoded . " a la modalidad " . $_GET["id"] . "";
            $this->registerLog(
                    "ESCRITURA", "cargarNivel", "Exitoso", $log
            );

            $lista = Modalidad::model()->obtenerNiveles($_GET['id'], Yii::app()->user->id);
            $modalidad_id = base64_decode($_GET['id']);
            $dataProvider = new CArrayDataProvider($lista, array(
                'pagination' => array(
                    'pageSize' => 5,
                )
            ));
            $this->widget('zii.widgets.grid.CGridView', array(
                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                'id' => 'modalidad-nivel-grid',
                'dataProvider' => $dataProvider,
                'summaryText' => false,
                'pager' => array(
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                ),
                'columns' => array(
                    array(
                        'header' => '<center>Nombre</center>',
                        'name' => 'nombre',
                    ),
                    array(
                        'type' => 'raw',
                        'header' => '<center>Acciones</center>',
                        'value' => array($this, 'columna')
                    ),
                ),
            ));
        }
    }

    /**
     * Elimina un nivel relacionado a una modalidad en particular
     * recibe un id por GET
     * @author Alexis Moreno
     */
    public function actionQuitarNivel() {
        if (!empty($_GET['id'])) {
            $nivel_id_decoded = base64_decode($_GET['id']);
            $modalidadEncoded = $_GET['modalidad_id'];

            $resultado = Modalidad::model()->dropNivel($nivel_id_decoded);
            $log = "Se Elimino el nivel " . $nivel_id_decoded . " de la modalidad " . $modalidadEncoded . "";
            $this->registerLog(
                    "ELIMINACION", "quitarNivel", "Exitoso", $log
            );
            $lista = Modalidad::model()->obtenerNiveles($modalidadEncoded, Yii::app()->user->id);
            $modalidad_id = base64_decode($_GET['modalidad_id']);

            $dataProvider = new CArrayDataProvider($lista, array(
                'pagination' => array(
                    'pageSize' => 5,
                )
            ));
            $this->widget('zii.widgets.grid.CGridView', array(
                'itemsCssClass' => 'table table-striped table-bordered table-hover',
                'id' => 'modalidad-nivel-grid',
                'dataProvider' => $dataProvider,
                'summaryText' => false,
                'pager' => array(
                    'header' => '',
                    'htmlOptions' => array('class' => 'pagination'),
                    'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                    'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                    'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                    'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                ),
                'columns' => array(
                    array(
                        'header' => '<center>Nombre</center>',
                        'name' => 'nombre',
                    ),
                    array(
                        'type' => 'raw',
                        'header' => '<center>Acciones</center>',
                        'value' => array($this, 'columna')
                    ),
                ),
            ));
        }
    }

    /**
     * Muestra la vista donde se asignan los niveles
     * @author Alexis Moreno
     */
    public function actionNivel() {
        $id = $_REQUEST['id'];
        $id = base64_decode($id);
        $model = $this->loadModel($id);
        $this->renderPartial('niveles', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate() {
        $id = $_REQUEST['id'];
        $id = base64_decode($id);
        $model = $this->loadModel($id);


        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Modalidad'])) {
            $model->attributes = $_POST['Modalidad'];
            $nombre = trim($_POST['Modalidad']['nombre']);
            $nombre = strtoupper($nombre);
            $model->nombre = $nombre;
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = "A";

            if ($model->save())
                if ($model->validate()) {
                    if ($model->save()) {
                        $this->registerLog(
                                "ACTUALIZACION", "update", "Exitoso", "actualizo una modalidad"
                        );
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                }
        }

        $this->renderPartial('update', array(
            'model' => $model,
        ));
    }

    /**
     * Inactiva una modalidad en particular.
     * @param integer $id de la modalidad a inactivar
     */
    public function actionBorrar() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $id = base64_decode($id);

            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_elim = date("Y-m-d H:i:s");
                $model->estatus = "E";
                if ($model->save()) {
                    $this->registerLog(
                            "INACTIVAR", "borrar", "Exitoso", "Inactivo una modalidad"
                    );
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Inhabiliatado con exito.'));
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
    }

    /**
     * Reactiva una modalidad en particular
     * @params integer $id del modelo que se va a reactivar
     */
    public function actionActivar() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $id = base64_decode($id);

            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->estatus = "A";
                if ($model->save()) {
                    $this->registerLog(
                            "ACTIVAR", "activar", "Exitoso", "activo una modalidad"
                    );
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Habilitado con exito.'));
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        /* $dataProvider=new CActiveDataProvider('Modalidad');
          $this->render('index',array(
          'dataProvider'=>$dataProvider,
          )); */

        #$model=new Modalidad('search');
        $groupId = Yii::app()->user->group;

        $model = new Modalidad('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Modalidad']))
            $model->attributes = $_GET['Modalidad'];

        $usuarioId = Yii::app()->user->id;
        $this->registerLog(
                "LECTURA", "index", "Exitoso", "Ingreso a modalidad"
        );
        $dataProvider = new CActiveDataProvider('Modalidad');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Modalidad('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Modalidad']))
            $model->attributes = $_GET['Modalidad'];
        var_dump($model->attributes);
        die();
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Modalidad the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Modalidad::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Devuelve las columnas con las acciones posibles
     * @param type $data
     * @return string
     */
    public function columnaAcciones($data) {
        $id = $data["id"];
        $id = base64_encode($id);

        $columna = '<div class="btn-group">
                        <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
                            Seleccione
                            <span class="icon-caret-down icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-yellow pull-right">';


        if ($data->estatus == "E") {
            $columna = CHtml::link("", "", array("class" => "fa fa-search blue", "title" => "Buscar esta Modalidad", "onClick" => "VentanaDialog('$id','/catalogo/modalidad/view','Modalidad','view')")) . '&nbsp;&nbsp;';
            if (Yii::app()->user->group == 1) {
                $columna .= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar esta Modalidad", "onClick" => "VentanaDialog('$id','/catalogo/modalidad/activar','Activar Modalidad','activar')")) . '&nbsp;&nbsp;';
            }
        } else {

            $columna .= '<li>' .CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Visualizar </span>", "#", array("class" => "fa fa-search blue", "title" => "Buscar esta Modalidad", "onClick" => "VentanaDialog('$id','/catalogo/modalidad/view','Modalidad','view')")) . '</li>';
            $columna .= '<li>' .CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Modificar</span>", "#", array("class" => "fa fa-pencil green", "title" => "Modificar esta Modalidad", "onClick" => "VentanaDialog('$id','/catalogo/modalidad/update','Modificar Modalidad','update')")) . '</li>';
            $columna .= '<li>' .CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Niveles</span>", "/catalogo/modalidad/nivel?id=$id", array("class" => "fa fa-sitemap orange", "title" => "Niveles Relacionados a esta Modalidad")) . '</li>';
            $columna .= '<li>' .CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Eliminar</span>", "#", array("class" => "fa fa-trash-o red", "title" => "Inactivar esta Modalidad", "onClick" => "VentanaDialog('$id','/catalogo/modalidad/borrar','Inhabilitar Modalidad','borrar')")) . '</li>';
            
        }

        $columna .= '</ul></div>';
        return $columna;
    }

    /**
     *  funcion para devolver el esatatus segun las iniciales A y E
     * @param type $data registro del modelo
     * @return string el estatus del registro
     */
    public function estatus($data) {
        $estatus = $data["estatus"];

        if ($estatus == "E") {
            $columna = "Inactivo";
        } else if ($estatus == "A") {
            $columna = "Activo";
        }
        return $columna;
    }

    /**
     * Devuelve las columnas con las acciones para los niveles de una modalidad
     * @param type $l
     * @return string
     */
    public function columna($l) {
        $nivel_id = base64_encode($l["id"]);
        $modalidad_id = base64_encode($l["modalidad_id"]);
        $columna = CHtml::link("", "", array("class" => "fa fa-trash-o red", "onClick" => 'quitar("' . $nivel_id . '","' . $modalidad_id . '");', "value" => '$l["id"]')) . '&nbsp;&nbsp;';
        return $columna;
    }

    /**
     * Performs the AJAX validation.
     * @param Modalidad $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'modalidad-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
