<?php

class GradoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	 static $_permissionControl = array(
        'read' => 'Consulta de Grados',
        'write' => 'Modificacion de Grados', // no lleva etiqueta write
        'label' => 'Consulta de Grados'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'consultarGrado', 'modificarGrado', 'procesarCambio','crear','create', 'eliminar'),
                'pbac' => array('read'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update','consultarGrado', 'modificarGrado', 'procesarCambio','crear','create', 'eliminar', 'reactivar'),
                'users' => array('write'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
        
        //*------------------Columnas del Index--------------------------------------------------
        //---------------------------------------------------------------------------------------
        
                 
 public function columnaAcciones($data){
        
        $id = $data["id"];
       
        
        if($data->estatus=='A'){
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarGrado($id)", "title" => "Consultar este Grado")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa fa-pencil green", "onClick" => "modificarGrado($id)", "title" => "Modificar Grado")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "borrar($data->id)", "class" => "fa fa-trash-o red remove-data", "style" => "color:#555;", "title" => "Eliminar"));
        }
        else{
            //$columna .= CHtml::link("", "", array("onClick" => "reactivar($data->id)", "class" => "fa fa icon-ok red remove-data", "style" => "color:555;", "title" => "Reactivar"));
            $columna = CHtml::link("", "", array("class" => "fa fa-search", "onClick" => "consultarGrado($id)", "title" => "Consultar este Grado")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "reactivar($data->id)", "class" => "fa icon-ok green remove-data", "style" => "color:#555;", "title" => "Reactivar"));
        }

        return $columna;
    }
     public function columnaEstatus($data){
         
         $estatus='';
         
         if($data['estatus']=="A")
         {
             $estatus='Activo';
         }
         
         if($data['estatus']=="E")
         {
             $estatus='Inactivo';
         }
         
         return $estatus;
         
         
    }
    
    
    //--------------------------------->fin columnas
    
    
     //----------------------------------------------------------------------
        //-----------------------------CREAR NUEVO REGISTRO---------------------
        
        
	public function actionCreate()
	{
            $model = new Grado;

        //Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
	}
        
        public function actionCrear() { 
            
        $model = new Grado;
        if (isset($_POST['nombre'])) {
            //var_dump($_POST['nombre']);die();
            $nombre=  trim($_POST['nombre']);
            $model->nombre = $nombre;
            $model->usuario_act_id = Yii::app()->user->id;
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = 'A';
            
                //var_dump('ALGO');die();
                if ($model->save()) {
                   
                    //var_dump($model);die();
                     $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Registro Exitoso.'));
                     $this->registerLog('ESCRITURA', 'catalogo.grado.crear', 'EXITOSO', 'Se ha creado un Grado');
                }else{
             
           
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                }
        }
    }
    
    //--------------------------------------------------------------------------
    //-------------------------------MODIFICAR REGISTRO---------$model= new Grado;----------------
    
     public function actionModificarGrado($id)
	{
           $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('_form', array(
            'model' => $model
                ), FALSE, TRUE);
    }
    
    public function actionProcesarCambio() {
        
         if (isset($_POST['Grado'])) {
            $id = $_POST['Grado']['id'];
            $model = $this->loadModel($id);
              if (is_numeric($id)) {
            $model = Grado::model()->findByPk($id);
            
            $model->attributes = $_POST['Grado'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            $model->estatus = "A";

            if ($model->validate()) {

                if ($model->save()) {
                    
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
                    $this->registerLog('ESCRITURA', 'catalogo.grado.procesarCambio', 'EXITOSO', 'Se ha Modificado un Grado');
                    
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            }
            
            
        }
        else {
                  Yii::app()->user->setFlash('error', "No se ha podido completar la última operación, ID invalido.");
        }
        }
        $this->renderPartial('_form', array(
            'model' => $model,
        ));
    }

    //--------------------------------------------------------------------------
    //---------------------------CONSULTAR DETALLES-----------------------------
    
     public function actionConsultarGrado($id) {


        $model = new Grado;

        $id = $_GET['id'];
        $model->id = $id;
        $model = $this->loadModel($id);
        //var_dump($model->usuarioAct->nombre);die();
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('detallesGrado', array(
        'model'=>Grado::model()->view($id),
            //'model'=>$model,
                ), FALSE, TRUE);
     }
     
     public function actionConsultarGradoInactivo($id) {


        $model = new Grado;

        $id = $_GET['id'];
        $model->id = $id;
        $model = $this->loadModel($id);
        //var_dump($model->usuarioAct->nombre);die();
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        $this->renderPartial('detallesGradoInactivo', array(
            'model'=>Grado::model()->view($id),
            //'model'=>$model,
                ), FALSE, TRUE);
     }
    
    //--------------------------------------------------------------------------
    //------------DESACTIVAR (ELIMINACION LOGICA)-------------------------------
    
    public function actionEliminar() {
                //$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Grado Elimanado con exito.'));
                               $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Inhabilitar este Grado? </p>'));
                if(isset($_GET['id']))
		{
		$id=$_GET['id'];
		
		$model=$this->loadModel($id);
		if($model){
		$model->usuario_act_id=Yii::app()->user->id;
		$model->fecha_elim=date("Y-m-d H:i:s");
                $model->fecha_act=date("Y-m-d H:i:s");
		$model->estatus="E";
		if($model->save()){
			//$this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Grado desactivado'));
					$model=$this->loadModel($id);
                                        $this->registerLog('ESCRITURA', 'catalogo.grado.eliminar', 'EXITOSO', 'Se ha eliminado un Grado');

		}
		else{
					throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

				}

		}
		else{

		throw new CHttpException(404, 'Error! Recurso no encontrado!');		
		}
		

		}

	}
///------------------------------------------------------------------------------------------
    //-----------------------------REACTIVAR-------------------------------------------------------
    
    public function actionReactivar() {
                $this->renderPartial("//msgBox",array('class'=>'alertDialogBox','message'=>'<p class="bolder center grey"> ¿Desea Habilitar este Turno? </p>'));

                if(isset($_GET['id']))
		{
		$id=$_GET['id'];
		
		$model=$this->loadModel($id);
		if($model){
		
                $model->usuario_ini_id= Yii::app()->user->id;
                $model->usuario_act_id=NULL;
                $model->fecha_ini=date("Y-m-d H:i:s");
                $model->fecha_elim= NULL;
		$model->estatus="A";
		if($model->save()){
			
					$model=$this->loadModel($id);
                                       $this->registerLog('ESCRITURA', 'catalogo.grado.reactivar', 'EXITOSO', 'Se ha reactivado un Grado');

		}
		else{
					throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

				}

		}
		else{

		throw new CHttpException(404, 'Error! Recurso no encontrado!');		
		}
		

		}

	}

    
    
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Grado']))
		{
			$model->attributes=$_POST['Grado'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
        
        static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $modulo = "Catalogo.GradoController." . $accion;
        $Utiles->traza($transaccion, $modulo, date('Y-m-d H:i:s'));
    }

	public function actionX()
	{
		$dataProvider=new CActiveDataProvider('Grado');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Grado('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Grado']))
			$model->attributes=$_GET['Grado'];

		$this->render('index',array(
			'model'=>$model,
		));
                $this->registerLog('LECTURA', 'catalogo.grado.index', 'EXITOSO', 'Se ha consultado la lista de grados');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Grado the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Grado::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Grado $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='grado-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
