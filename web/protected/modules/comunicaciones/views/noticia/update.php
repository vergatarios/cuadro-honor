<?php
/* @var $this NoticiaController */
/* @var $model Noticia */

$this->pageTitle = 'Actualización de Datos de Noticias';

$this->breadcrumbs=array(
	'Noticias'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion','estatus'=>$estatus)); ?>
<?php //$this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>