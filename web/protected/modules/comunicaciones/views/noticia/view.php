<?php
/* @var $this NoticiaController */
/* @var $model Noticia */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Noticias'=>array('lista'),
    'Consulta',
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

<!--            <ul class="nav nav-tabs">-->
<!--                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>-->
<!--                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>--a>-->
<!--            </ul>-->

<!--            <div class="tab-content">-->
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'noticia-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Vista de Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-12">
                                                            <label class="col-md-12" >Título</label>
                                                            <?php echo $form->textField($model,'titulo',array('size'=>40, 'maxlength'=>40, 'required' => 'required', 'class' => 'span-12','disabled'=>'disabled' )); ?>

                                                        </div>


                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="space-6"></div>
                                                        <div class="col-md-4">

                                                            <div class="col-md-6">
                                                                <label class="col-md-12" >Fecha de Clausura</label>

                                                                <div class="input-group">
                                                                     <span class="input-group-addon">
                                                                     <i class="icon-calendar bigger-100"></i>
                                                                     </span>

                                                                    <?php
                                                                    if ($model->fecha_de_clausura != "") {
                                                                        echo $form->textField($model,'fecha_de_clausura', array('size' => 30, 'maxlength' => 30,
                                                                            'id' => 'date-picker', 'required' => 'required', 'readonly' => 'readonly',
                                                                            'value'=>Utiles::transformDate($model->fecha_de_clausura, '-', 'Y-m-d', 'd-m-Y'),'disabled'=>'disabled'));

                                                                    }

                                                                    ?>

                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="col-md-4">
                                                            <label class="col-md-12" >Importante o Fija</label>
                                                            <?php echo $form->dropDownList($model,'importante_o_fija',array('I'=>'Importante', 'F'=>'Fija'), array( 'class' => 'span-12', "required"=>"required",'disabled'=>'disabled')); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <label class="col-md-12" >Estatus</label>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array( 'class' => 'span-12', "required"=>"required",'disabled'=>'disabled')); ?>
                                                        </div>



                                                    </div>
                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">



                                                    </div>
                                                    <div class="space-6"></div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="col-md-12">Cuerpo</label>
                                                            <div class="col-md-12" style="border:1px solid #ccc; min-height:100px;">
                                                                <?=$model->cuerpo?>
                                                                </div>
<!--                                                            --><?php //echo $form->textArea($model,'cuerpo',array('rows'=>6, 'cols'=>12, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>

                                                            </div>


                                                        </div>
                                                    </div>

                                                <div class="space-6"></div>
                                                <?php if ($model->usuario_elim_id != "") {?>
                                                <div class="row">
                                                    <div class="col-md-4">

                                                        <div class="col-md-6">
                                                            <label class="col-md-12" >Fecha de Eliminación</label>

                                                            <div class="input-group">
                                                                     <span class="input-group-addon">
                                                                     <i class="icon-calendar bigger-100"></i>
                                                                     </span>

                                                                <?php
                                                                if ($model->fecha_elim != "") {

                                                                    echo $form->textField($model,'fecha_elim', array('size' => 30, 'maxlength' => 30,
                                                                        'id' => 'date-picker', 'required' => 'required', 'readonly' => 'readonly',
                                                                        'value'=>Utiles::transformDate($model->fecha_elim, '-', 'Y-m-d', 'd-m-Y'),'disabled'=>'disabled'));

                                                                }

                                                                ?>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="col-md-4">
                                                        <label class="col-md-12" >Usuario que Eliminó</label>
                                                        <?php echo $form->textField($model,'usuario_elim_id', array('class' => 'span-12', 'disabled'=>'disabled','value'=>$model->usuarioIni->nombre." ".$model->usuarioIni->apellido,)); ?>
                                                    </div>


                                                    <!--                                                            --><?php //echo $form->textArea($model,'cuerpo',array('rows'=>6, 'cols'=>12, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>


                                                </div>
                                                <?php }?>
                                            </div>


                                                    <div class="space-6"></div>

                                                </div>
<!--                                                fin de row-->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/comunicaciones/noticia"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

            </div>
        </div>

    </div>
</div>


<script src="/public/js/modules/fundamentoJuridico/jquery.ui.widget.js"></script>
<script src="/public/js/modules/fundamentoJuridico/jquery.iframe-transport.js"></script>
<!---------------------------------------------------------------------------------------->
<script src="/public/js/modules/comunicaciones/noticia.js"></script>

<script src="/public/js/ace-elements.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/typeahead-bs2.min.js"></script>
<script src="/public/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/public/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/public/js/jquery.ui.touch-punch.min.js"></script>
<script src="/public/js/markdown/markdown.min.js"></script>
<script src="/public/js/markdown/bootstrap-markdown.min.js"></script>
<script src="/public/js/jquery.hotkeys.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/bootbox.min.js"></script>