<?php
/* @var $this NoticiaController */
/* @var $model Noticia */

$this->pageTitle = 'Registro de Noticias';

$this->breadcrumbs=array(
	'Noticias'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'formType'=>'registro',
    'fechaClausuraDefecto'=>$fechaClausuraDefecto,
    'estatus'=>$estatus
    )); ?>