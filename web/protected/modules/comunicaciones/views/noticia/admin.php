<?php

/* @var $this NoticiaController */
/* @var $model Noticia */

$this->breadcrumbs=array(
	'Noticias'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Noticias';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Noticias</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <?php

                        foreach(Yii::app()->user->getFlashes() as $key => $message) {
                            $this->renderPartial('//msgBox',array('class'=>'successDialogBox','message'=>$message));
                        }
                        ?>
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Noticias.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/comunicaciones/noticia/publicoNoticia"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Vista Principal                        </a>
                        <a href="<?php echo $this->createUrl("/comunicaciones/noticia/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nueva Noticias                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'noticia-grid',
        'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
//        'afterAjaxUpdate' => "
//                function(){
//
//                }",
    'afterAjaxUpdate' => 'function(){$("#date-picker-CGridView").datepicker();
        var fecha = new Date();
var anoActual = fecha.getFullYear();
        var fecha = new Date();
var anoActual = fecha.getFullYear();
$.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: "yy-mm-dd",
        showOn:"focus",
            showOtherMonths: false,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            closeText: "Cerrar",
            prevText: "<-Ant",
            nextText: "Sig->",
            currentText: "Hoy",
            monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthNamesShort: ["Ene","Feb","Mar","Abr", "May","Jun","Jul","Ago","Sep", "Oct","Nov","Dic"],
            dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
            dayNamesShort: ["Dom","Lun","Mar","Mié","Juv","Vie","Sáb"],
            dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
            weekHeader: "Sm"



    });
    }',

	'columns'=>array(
        array(
            'header' => '<center>Título</center>',
            'name' => 'titulo',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Noticia[titulo]', $model->titulo, array('title' => '',)),
        ),
        array(
            'header' => '<center>Importante o Fija</center>',
            'name' => 'importante_o_fija',
            'value' => array($this, 'importanteFija'),
            'filter' => array('I' => 'Importante', 'F' => 'Fija'),
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[importante_o_fija]', $model->importante_o_fija, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha de Clausura</center>',
            'name' => 'fecha_de_clausura',
            'htmlOptions' => array(),

            //'filter' => CHtml::textField('Noticia[fecha_de_clausura]', $model->fecha_de_clausura, array('title' => '',)),

            'filter' => CHtml::activeTextField($model, 'fecha_de_clausura', array('id' => "date-picker-CGridView",
                'placeHolder' => 'aaaa-mm-dd',
                'readonly' => 'readonly'
            )),
        ),
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'value' => array($this, 'estatus'),
            'filter' => array('A' => 'Activo', 'E' => 'Eliminado', 'I' => 'Inactivo'),
            //'filter' => CHtml::textField('Noticia[estatus]', $model->estatus, array('title' => '',)),
        ),

        array(
            'type' => 'raw',
            'header' => '<center>Nombre de Usuario</center>',
            'value'=>'$data->usuarioIni->nombre',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[usuario_ini_id]',
            //    $model->usuarioIni->nombre, array('title' => '',)),
        ),
        array(
            'type' => 'raw',
            'header' => '<center>Apellido de Usuario</center>',
            'value'=>'$data->usuarioIni->apellido',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[usuario_ini_id]',
            //    $model->usuarioIni->nombre, array('title' => '',)),
        ),
		/*
		array(
            'header' => '<center>Cuerpo</center>',
            'name' => 'cuerpo',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[cuerpo]', $model->cuerpo, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_elim_id</center>',
            'name' => 'usuario_elim_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[usuario_elim_id]', $model->usuario_elim_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),
        array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Noticia[estatus]', $model->estatus, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>

<!--<script src="/public/js/modules/fundamentoJuridico/jquery.ui.widget.js"></script>
<script src="/public/js/modules/fundamentoJuridico/jquery.iframe-transport.js"></script>-->
<!---------------------------------------------------------------------------------------->
<script src="/public/js/modules/comunicaciones/noticia.js"></script>

<script src="/public/js/ace-elements.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/typeahead-bs2.min.js"></script>
<script src="/public/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/public/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/public/js/jquery.ui.touch-punch.min.js"></script>
<script src="/public/js/markdown/markdown.min.js"></script>
<script src="/public/js/markdown/bootstrap-markdown.min.js"></script>
<script src="/public/js/jquery.hotkeys.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/bootbox.min.js"></script>
<!--<script src="/public/js/date-time/daterangepicker.min.js"></script>
<script src="public/js/date-time/moment.min.js"></script>-->

<!--<script src="public/js/date-time/bootstrap-timepicker.min.js"></script>
<script src="public/js/date-time/daterangepicker.min.js"></script>-->