<?php
/* @var $this TituloDigitalController */
/* @var $model TituloDigital */

$this->breadcrumbs=array(
	'Titulo Digitals'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TituloDigital', 'url'=>array('index')),
	array('label'=>'Create TituloDigital', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#titulo-digital-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Titulo Digitals</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'titulo-digital-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'codigo_verificacion',
		'titulo_id',
		'origen_estudiante',
		'cedula_estudiante',
		'nombre_estudiante',
		/*
		'codigo_titulo',
		'zona_educativa',
		'plantel',
		'codigo_plantel',
		'nivel',
		'mencion',
		'nacido_en',
		'fecha_emision',
		'lugar_expedicion',
		'fecha_expedicion',
		'anio_egreso',
		'origen_director_zona_educativa',
		'cedula_director_zona_educativa',
		'nombre_director_zona_educativa',
		'firma_digital_director_zona_educativa',
		'origen_director_plantel',
		'cedula_director_plantel',
		'nombre_director_plantel',
		'firma_digital_director_plantel',
		'origen_coordinador_drcee',
		'cedula_coordinador_drcee',
		'nombre_coordinador_drcee',
		'firma_digital_coordinador_drcee',
		'origen_funcionario_designado',
		'cedula_funcionario_designado',
		'nombre_funcionario_designado',
		'firma_digital_funcionario_designado',
		'datos_verificacion',
		'observacion',
		'estudiante_id',
		'usuario_ini_id',
		'fecha_ini',
		'usuario_act_id',
		'fecha_act',
		'fecha_elim',
		'estatus',
		'periodo_escolar_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
