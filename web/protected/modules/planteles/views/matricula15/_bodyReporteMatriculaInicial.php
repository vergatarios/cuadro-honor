<style type="text/css">

    .estudiantes table, .estudiantes th, .estudiantes td {
        border: 1px solid grey;
    }  
    .estudiantes table {
        border-collapse: collapse;
        border-spacing: 0px;
        text-align:center;
    }
</style>

<?php
if (isset($estudiantes) AND $estudiantes != array()) {
    ?>
    <table class="estudiantes" style="font-size:8px; font-family:Helvetica Neue,Arial,Helvetica,sans-serif;width:800px;border-collapse: collapse;
           border-spacing: 0px;
           text-align:center; border: 1px solid grey; ">

        <?php
        foreach ($estudiantes as $key => $value) {
            $nacionalidad = isset($value[0]['nacionalidad']) ? $value[0]['nacionalidad'] : null;
            $documento_identidad = isset($value[0]['documento_identidad']) ? $value[0]['documento_identidad'] : null;
            $tDocumento_identidad = isset($value[0]['tdocumento_identidad']) ? $value[0]['tdocumento_identidad'] : null;
            $cedula_escolar = isset($value[0]['cedula_escolar']) ? $value[0]['cedula_escolar'] : null;
            $nombres = isset($value[0]['nombres']) ? $value[0]['nombres'] : null;
            $apellidos = isset($value[0]['apellidos']) ? $value[0]['apellidos'] : null;
            $sexo = isset($value[0]['sexo']) ? $value[0]['sexo'] : null;
            $fecha_nacimiento = (isset($value[0]['fecha_nacimiento'])) ? $value[0]['fecha_nacimiento'] : null;
            $inscripcion_regular = (isset($value[0]['inscripcion_regular'])) ? $value[0]['inscripcion_regular'] : null;
            $materia_pendiente = isset($value[0]['materia_pendiente']) ? $value[0]['materia_pendiente'] : null;
            $repitiente = (isset($value[0]['repitiente'])) ? $value[0]['repitiente'] : null;
            $doble_inscripcion = (isset($value[0]['doble_inscripcion'])) ? $value[0]['doble_inscripcion'] : null;
            $observacion = isset($value[0]['observacion']) ? $value[0]['observacion'] : null;
            $documentoIdentidad = null;
            $rg_check = null;
            $mp_check = null;
            $rp_check = null;
            $di_check = null;
            $fecha_array = explode('-', $fecha_nacimiento);
            $anio = isset($fecha_array[0]) ? $fecha_array[0] : null;
            $mes = isset($fecha_array[1]) ? $fecha_array[1] : null;
            $dia = isset($fecha_array[2]) ? $fecha_array[2] : null;

            if ($documento_identidad != null) {
                if ($tDocumento_identidad != null) {
                    $documentoIdentidad = $tDocumento_identidad . '-' . $documento_identidad;
                } else {
                    $documentoIdentidad = $documento_identidad;
                }
            } else {
                if ($cedula_escolar != null)
                    $documentoIdentidad = 'C.E: ' . $cedula_escolar;
                else {
                    $documentoIdentidad = 'NO POSEE';
                }
            }
            if (!($observacion != null))
                $observacion = '**********';
            $rg_check = CHtml::checkBox('RG', (bool) $inscripcion_regular);
            $mp_check = CHtml::checkBox('RG', (bool) $materia_pendiente);
            $rp_check = CHtml::checkBox('RG', (bool) $repitiente);
            $di_check = CHtml::checkBox('RG', (bool) $doble_inscripcion);
            ?>

            <tr>

                <td width="15px" align="center" style="font-family: Arial;">
                    <?php echo $key + 1; ?>
                </td>
                <td  width="30px" align="center" style="font-family: Arial;">
                    <?php echo $nacionalidad; ?>
                </td>
                <td  width="80px"  align="center" style="font-family: Arial;">
                    <?php echo $documentoIdentidad; ?>
                </td>
                <td  width="149px"  align="center" style="font-family: Arial;">
                    <?php echo $apellidos; ?>
                </td>
                <td  width="149px"  align="center" style="font-family: Arial;">
                    <?php echo $nombres; ?>
                </td>
                <td  width="40px"  align="center" style="font-family: Arial;">
                    <?php echo $sexo; ?>
                </td>
                <td width="30px"  align="center" style="font-family: Arial;">
                    <?php echo $dia; ?>
                </td>
                <td width="30px"  align="center" style="font-family: Arial;">
                    <?php echo $mes; ?>
                </td>
                <td  width="30px" align="center" style="font-family: Arial;">
                    <?php echo $anio; ?>
                </td>
                <td width="20px" align="center" style="font-family: Arial;">
                    <?php echo $rg_check; ?>
                </td>
                <td width="20px" align="center" style="font-family: Arial;">
                    <?php echo $rp_check; ?>
                </td>
                <td width="20px" align="center" style="font-family: Arial;">
                    <?php echo $mp_check; ?>
                </td>
                <td width="20px" align="center" style="font-family: Arial;">
                    <?php echo $di_check; ?>
                </td>
                <td  width="160px"  align="center" style="font-family: Arial; font-size:9px;">
                    <?php echo $observacion; ?>
                </td>

            </tr>

            <?php
        }
        echo "</table>";
    }
    ?>
<!--    <tr>
    <th colspan="6" class="center">
    </th>
    
    
    <th  class="center">
    </th>

</tr>-->






    <br>









