<div class="form" id="_formEndogeno"> 

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'plantelEndogeno-form',
        'action' => 'guardarEndogeno',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>
    <?php //echo CHtml::hiddenField('ids_proyectosEndoP', $ids_proyectosEndoP, array('id' => 'ids_proyectosEndoP')); ?>
    <?php echo CHtml::hiddenField('plantel_id', $plantel_id, array('id' => 'plantel_id')); ?>
    <div id="_widget-endogeno" class="widget-box">


        <div id="resultadoEndogeno" class="infoDialogBox">
            <p>
                Debe Seleccionar los Proyectos de Desarrollo Endógeno que se llevan a cabo en el Plantel.
            </p>
        </div>
        <?php if (isset($estatusMod)) { ?>
            <div id ="guardoEndogeno" class="successDialogBox" >
                <p>
                    <?php echo "Actualicación Exitosa"; ?>
                </p>
            </div> 
        <?php } ?>
        <div class="widget-header">
            <h5>Proyectos de Desarrollo Endógeno</h5>
            <div class="widget-toolbar">
                <a  href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div id="desarrolloEndogeno" class="widget-body" >
            <div class="widget-body-inner" >
                <div class="widget-main">                      

                    <div class="row">

                        <div class="col-md-4">

                            <label for="servicios">Proyectos de Desarrollo Endógeno<span class="required"></span></label><br>
                            <?php
                            echo CHtml::dropDownList('proyectos_endogenos', 'id', $listProyectosEndo, array(
                                'empty' => 'Seleccione un proyecto',
                                'onChange' => 'agregarProyecto()',
                                'class' => 'span-9'
                                    )
                            );
                            ?>
                        </div>
                        <div class="col-md-8" id ="proyectosUsados">
                            <?php
                            if (isset($dataProvider) && $dataProvider !== array()) {
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'dataProvider' => $dataProvider,
                                    'id' => 'proyectos_endogenos_usados',
                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                    'enableSorting' => true,
                                    'summaryText' => false,
                                    //'selectionChanged' => 'function(id) { getPermission("' . Yii::app()->baseUrl . '", "' . UserGroupsAccess::GROUP . '", $.fn.yiiGridView.getSelection(id))}',
                                    'columns' => array(
                                        array(
                                            'name' => 'nombre',
                                            'type' => 'raw',
                                            'header' => '<center>Proyecto Endogeno</center>'
                                        ),
                                        array(
                                            'name' => 'boton',
                                            'type' => 'raw',
                                            'header' => '<center>Acciones</center>',
                                        ),
                                    ),
                                    'pager' => array(
                                        'header' => '',
                                        'htmlOptions' => array('class' => 'pagination'),
                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                    ),
                                ));
                            }
                            ?>
                        </div>
                        <br>
                        <br>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">

<!--        <div class="col-md-6">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("planteles/consultar/"); ?>" class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>-->

    </div>

    <?php
    $this->endWidget();
    ?>
</div>







