<script type="text/javascript">
    //$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "seleccionarMunicipio",
        data: {estado_id:<?php echo $model->estado_id; ?>},
        success: function(data) {

            $("#Plantel_municipio_id").html(data);
            $("#Plantel_municipio_id").val(<?php echo $model->municipio_id; ?>);
        },
    });
    $.ajax({
        type: "GET",
        url: "seleccionarParroquia",
        data: {municipio_id:<?php echo $model->municipio_id; ?>},
        success: function(data) {
            $("#Plantel_parroquia_id").html(data);
            $("#Plantel_parroquia_id").val(<?php echo $model->parroquia_id; ?>);

        },
    });

    //});
</script>
<div  id="identificacionP" class="widget-box collapsed">

    <div id="resultadoPlantel">
    </div>

    <div id="resultado">
        <div class="infoDialogBox">
            <p>
                Debe Ingresar los Datos Generales del Plantel, los campos marcados con <span class="required">*</span> son requeridos.
            </p>
        </div>
    </div>
    <div id ="guardo" class="successDialogBox" style="display: none">
        <p>
            Registro exitoso
        </p>
    </div>
    <br>
    <div class="widget-header" style="border-width: 1px">
        <h5>Identificaci&oacute;n Del Plantel</h5>
        <div class="widget-toolbar">
            <a  href="#" data-action="collapse">
                <i class="icon-chevron-down"></i>
            </a>
        </div>
    </div>

    <div id="identificacionPlantel" class="widget-body" >
        <div class="widget-body-inner" >
            <div class="widget-main form">

                <div class="row">
                    <div class="col-md-2" style="height: 250px">
                        <p>
                            <img id="img_thumb_logo" class="img-thumbnail" alt="..." src="<?php echo Yii::app()->baseUrl . '/public/images/indice.svg'; ?>"  />
                        </p>
                        <?php
                        $this->widget('xupload.XUpload', array(
                            'url' => Yii::app()->createUrl("planteles/modificar/upload", array("parent_id" => 1)),
                            'model' => $xupload, //An instance of our model
                            'attribute' => 'file',
                            'autoUpload' => true,
                            'multiple' => true,
                            'htmlOptions' => array('id' => 'form-form'),
                            //Our custom upload template
                            // 'uploadView' => 'application.views.site.upload',
                            //our custom download template
                            //'downloadView' => 'application.views.site.download',
                            'options' => array(//Additional javascript options
                                //This is the submit callback that will gather
                                //the additional data  corresponding to the current file
                                'submit' => "js:function (e, data) {
                              var inputs = data.context.find(':input');
                              data.formData = inputs.serializeArray();
                              return true;
                              }",
                                'success' => "js:function(data){
                              var json = data[0];
                              $('#img_thumb_logo').attr('src', json.thumbnail_url);
                              $.ajax({
                              url: '../../agregarLogo',
                              type: 'get',
                              dataType: 'html',
                              data: {plantel_id : $('#plantel_id').val(),
                              filename : json.name},
                              success: function(datahtml) {
                              //console.log(datahtml);

                              }
                              });
                              }"
                            ),
                        ));
                        ?>
                    </div>

                    <div id="divNER" class="col-md-3">
                        <label for="ner">NER</label>
                        <input type="checkbox" id="ner" name="ner" value="" onchange="mostrarNer()" onclick="mostrarNer()">
                    </div>

                    <div id="divNombreNer" class="col-md-3" style="display: none">
                        <?php echo $form->labelEx($model, 'codigo_ner', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'codigo_ner', array('class' => 'span-4', 'style' => 'width: 160px')); ?>
                        <?php //echo $form->error($model, 'codigo_ner');  ?>
                    </div>

                    <div id="divNoInscrito" class="col-md-3">
                        <label class="col-md-12" for="noinscrito" title="No Inscrito">No Inscrito</label>
                        <input type="checkbox" id="Plantel_no_inscrito" name="Plantel[no_inscrito]" value="false" style="margin-left: 15px;">
                    </div>

                    <div class="col-md-10">
                    </div>
                    <div  id="divCod_plantel" class="col-md-3" >
                        <?php echo $form->labelEx($model, 'cod_plantel', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'cod_plantel', array('size' => 10, 'maxlength' => 10, 'class' => 'span-7')); ?><br>
                        <?php //echo $form->error($model, 'cod_plantel');  ?>
                    </div>

                    <div  id="divCod_plantelNER" class="col-md-3" style="display: none">
                        <?php echo $form->labelEx($model, 'cod_plantel', array("class" => "col-md-12")); ?>
                        <input type="text" id="cod_plantelNer" name="cod_plantelNer" maxlength="4" size="4" class="span-7">
                    </div>

                    <div id="divCodEstadistico" class="col-md-3">
                        <?php echo $form->labelEx($model, 'cod_estadistico', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'cod_estadistico', array('class' => 'span-7', 'size' => 10, 'maxlength' => 10, 'onkeyup' => "var reg = /[^0-9 ]/gi;
                                                                   if(reg.test(this.value))this.value = this.value.replace(reg,'');")); ?>

                        <?php //echo $form->error($model, 'cod_estadistico');  ?>
                    </div>

                    <div id="divDenominacion" class="col-md-4">
                        <?php echo $form->labelEx($model, 'denominacion_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'denominacion_id', CHtml::listData($denominacion, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7')); ?>
                        <?php //echo $form->error($model, 'denominacion_id');  ?>
                    </div>

                    <div id="divNombre"  class="col-md-3">
                        <?php echo $form->labelEx($model, 'nombre', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'nombre', array('size' => 60, 'maxlength' => 150, 'class' => 'span-7')); ?>
                        <?php //echo $form->error($model, 'nombre');  ?>
                    </div>

                    <div id="divZonaEducativa" class="col-md-3">
                        <?php echo $form->labelEx($model, 'zona_educativa_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'zona_educativa_id', CHtml::listData($zonaEducativa, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7')); ?>
                        <?php //echo $form->error($model, 'zona_educativa_id');  ?>
                    </div>

                    <div id="divTipoDependencia" class="col-md-4">
                        <?php echo $form->labelEx($model, 'tipo_dependencia_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'tipo_dependencia_id', CHtml::listData($tipoDependencia, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7')); ?>
                        <?php //echo $form->error($model, 'tipo_dependencia_id');  ?>
                    </div>

                    <div id="divDistrito" class="col-md-3">
                        <?php echo $form->labelEx($model, 'distrito_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'distrito_id', CHtml::listData($distrito, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7')); ?>
                        <?php //echo $form->error($model, 'distrito_id');  ?>
                    </div>

                    <div id="divEstatusPlantel"  class="col-md-3">
                        <?php echo $form->labelEx($model, 'estatus_plantel_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'estatus_plantel_id', CHtml::listData($estatusPlantel, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7')); ?>
                        <?php //echo $form->error($model, 'estatus_plantel_id');  ?>
                    </div>

                    <div id="divAnnioFundado"  class="col-md-4">
                        <?php echo $form->labelEx($model, 'annio_fundado', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList($model, 'annio_fundado', array('1970' => '1970', '1971' => '1971', '1972' => '1972',
                            '1973' => '1973', '1974' => '1974', '1975' => '1975', '1976' => '1976', '1977' => '1977', '1978' => '1978', '1979' => '1979',
                            '1980' => '1980', '1981' => '1981', '1982' => '1982', '1983' => '1983', '1984' => '1984', '1985' => '1985', '1986' => '1986',
                            '1987' => '1987', '1988' => '1988', '1989' => '1989', '1990' => '1990', '1991' => '1991', '1992' => '1992', '1993' => '1993',
                            '1994' => '1994', '1995' => '1995', '1996' => '1996', '1997' => '1997', '1998' => '1998', '1999' => '1999', '2000' => '2000',
                            '2001' => '2001', '2002' => '2002', '2003' => '2003', '2004' => '2004', '2005' => '2005', '2006' => '2006', '2007' => '2007',
                            '2008' => '2008', '2009' => '2009', '2010' => '2010', '2011' => '2011', '2012' => '2012', '2013' => '2013', '2014' => '2014'
                                ), array('empty' => '-Seleccione-', 'class' => 'span-7'));
                        ?>
                        <?php //echo $form->error($model, 'annio_fundado');  ?>
                    </div>
                </div>

                <hr>
            </div>
        </div>
    </div>
</div>
<br>

<div id="datosUbicacionP" class="widget-box collapsed">

    <div class="widget-header">
        <h5>Datos de Ubicaci&oacute;n</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-down"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-body-inner">
            <div class="widget-main form">

                <div class="row">

                    <div id="divEstado" class="col-md-4">
                        <?php echo $form->labelEx($model, 'estado_id', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'estado_id', CHtml::listData($estado, 'id', 'nombre'), array(
                            'ajax' => array(
                                'type' => 'GET',
                                'update' => '#Plantel_municipio_id',
                                'url' => CController::createUrl('crear/seleccionarMunicipio'),
                            ),
                            'empty' => array('' => '-Seleccione-'), 'class' => 'span-8',
                                )
                        );
                        ?>
                        <?php //echo $form->error($model, 'estado_id');  ?>
                    </div>

                    <div id="divMunicipio" class="col-md-4">
                        <?php echo $form->labelEx($model, 'municipio_id', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList($model, 'municipio_id', array(), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-8',
                            'ajax' => array(
                                'type' => 'GET',
                                'update' => '#Plantel_parroquia_id',
                                'url' => CController::createUrl('crear/seleccionarParroquia'),
                            ),
                            'empty' => array('' => '-Seleccione-'), 'class' => 'span-8',
                        ));
                        ?>
                        <?php //echo $form->error($model, 'municipio_id');  ?>
                    </div>

                    <!-- <div id="divParroquia" class="col-md-4">
                    <?php echo $form->labelEx($model, 'parroquia_id', array("class" => "col-md-12")); ?>
                    <?php
                    echo $form->dropDownList($model, 'parroquia_id', array(), array(
                        'empty' => '-Seleccione-',
                        'id' => 'Plantel_parroquia_id',
                        'class' => 'span-8',
                        'ajax' => array(
                            'type' => 'GET',
                            'update' => '#Plantel_localidad_id',
                            'url' => CController::createUrl('crear/seleccionarLocalidad'),
                        ),
                        'empty' => array('' => '-Seleccione-'),
                    ));
                    ?>
                    <?php //echo $form->error($model, 'parroquia_id');  ?>
                     </div>-->

                    <!--ALEXIS EDITANDO-->
                    <div id="divParroquia" class="col-md-4">
                        <?php echo $form->labelEx($model, 'parroquia_id', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList($model, 'parroquia_id', array(), array(
                            'empty' => '- SELECCIONE -',
                            'id' => 'Plantel_parroquia_id',
                            'class' => 'span-7',
                            'ajax' => array(
                                'type' => 'GET',
                                'update' => '#Plantel_urbanizacion_id',
                                'url' => CController::createUrl('crear/seleccionarUrbanizacion'),
                                'success' => 'function(resutl) {
                                                $("#Plantel_urbanizacion_id").html(resutl);
                                                var parroquia_id=$("#Plantel_parroquia_id").val();

                                                var data=
                                                        {
                                                            parroquia_id: parroquia_id,

                                                        };
                                                $.ajax({
                                                    type:"GET",
                                                    data:data,
                                                    url:"/planteles/crear/seleccionarPoblacion",
                                                    update:"#Plantel_poblacion_id",
                                                    success:function(result){  $("#Plantel_poblacion_id").html(result);}


                                                });

                                            }',
                            ),
                            'empty' => array('' => '- SELECCIONE -'),
                        ));
                        ?>

                    </div>

                    <div id="divPoblacion" class="col-md-4">
                        <?php echo $form->labelEx($model, 'poblacion_id', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList($model, 'poblacion_id', array(), array(
                            'empty' => '- SELECCIONE -',
                            'id' => 'Plantel_poblacion_id',
                            'class' => 'span-7',
                            'empty' => array('' => '- SELECCIONE -'),
                        ));
                        ?>

                    </div>

                    <div id="divUrbanizacion" class="col-md-4">
                        <?php echo $form->labelEx($model, 'urbanizacion_id', array("class" => "col-md-12")); ?>

                        <?php
                        echo $form->dropDownList($model, 'urbanizacion_id', array(), array(
                            'empty' => '- SELECCIONE -',
                            'id' => 'Plantel_urbanizacion_id',
                            'class' => 'span-7',
                            'empty' => array('' => '- SELECCIONE -'),
                        ));
                        ?>
                    </div>

                    <div id="divTipoVia" class="col-md-4">
                        <?php echo $form->labelEx($model, 'tipo_via_id', array("class" => "col-md-12")); ?>

                        <?php
                        $lista = Plantel::model()->obtenerTipoVia();

                        echo $form->dropDownList($model, 'tipo_via_id', CHtml::listData($lista, 'id', 'nombre'), array(
                            'empty' => '- SELECCIONE -',
                            'id' => 'Plantel_tipo_via_id',
                            'class' => 'span-7',
                            'empty' => array('' => '- SELECCIONE -'),
                        ));
                        ?>
                    </div>



                    <div id="divVia" class="col-md-4">


                        <?php echo $form->labelEx($model, 'via', array("class" => "col-md-12")); ?>
                        <div class="autocomplete-w1">
                            <?php echo $form->textField($model, 'via', array('size' => 160, 'maxlength' => 160, 'class' => 'span-7', 'id' => 'query', 'onkeyup' => 'makeUpper("#query");')); ?>
                            <div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content" hidden="hidden"></div>
                        </div>



                    </div>


                    <!--FIN ALEXIS-->


                    <!--<div id="divLocalidad" class="col-md-4">
                    <?php echo $form->labelEx($model, 'localidad_id', array("class" => "col-md-12")); ?>
                    <?php
                    echo $form->dropDownList($model, 'localidad_id', array(), array(
                        'empty' => '-Seleccione-',
                        'class' => 'span-8',
                    ));
                    ?>
                    <?php //echo $form->error($model, 'localidad_id');  ?>
                    </div>-->

                    <div id="divDireccion" class="col-md-4">
                        <?php echo $form->labelEx($model, 'direccion', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'direccion', array('size' => 6, 'maxlength' => 100, 'class' => 'span-8')); ?>
                        <?php //echo $form->error($model, 'direccion');  ?>
                    </div>

                    <div id="divTelefonoFijo" class="col-md-4">
                        <?php echo $form->labelEx($model, 'telefono_fijo', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'telefono_fijo', array('size' => 11, 'maxlength' => 11, 'class' => 'span-8', 'onkeyup' => "var reg = /[^0-9 ]/gi;
                                                                   if(reg.test(this.value))this.value = this.value.replace(reg,'');")); ?>
                        <?php //echo $form->error($model, 'telefono_fijo');  ?>
                    </div>

                    <div  id="divTelefonoOtro"class="col-md-4">
                        <?php echo $form->labelEx($model, 'telefono_otro', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'telefono_otro', array('size' => 11, 'maxlength' => 11, 'class' => 'span-8', 'onkeyup' => "var reg = /[^0-9 ]/gi;
                                                                   if(reg.test(this.value))this.value = this.value.replace(reg,'');")); ?>
                        <?php //echo $form->error($model, 'telefono_otro');  ?>
                    </div>

                    <div id="divNFax" class="col-md-4">
                        <?php echo $form->labelEx($model, 'nfax', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'nfax', array('size' => 11, 'maxlength' => 11, 'class' => 'span-8', 'onkeyup' => "var reg = /[^0-9 ]/gi;
                                                                   if(reg.test(this.value))this.value = this.value.replace(reg,'');")); ?>
                        <?php //echo $form->error($model, 'correo');  ?>
                    </div>

                    <div id="divCorreo" class="col-md-4">
                        <?php echo $form->labelEx($model, 'correo', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'correo', array('size' => 60, 'maxlength' => 100, 'class' => 'span-8')); ?>
                        <?php //echo $form->error($model, 'correo');  ?>
                    </div>

                    <div id="divZonaUbicacion" class="col-md-4">
                        <?php echo $form->labelEx($model, 'zona_ubicacion_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'zona_ubicacion_id', CHtml::listData($zona_ubicacion, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-8')); ?>
                        <?php //echo $form->error($model, 'correo');  ?>
                    </div>


                    <div class="row row-fluid">
                        <div class="col-md-2" style="width: 140px">
                            <label>Tipo Ubicación</label>
                            <div class="row">
                                <div id="divFronteriza" class="col-md-2" style="width: 140px">
                                    <label for="fronteriza">Fronteriza</label>
                                    <input type="checkbox" name="fronteriza" id="fronteriza" value="1">
                                    <?php //echo $form->checkBox($tipoUbicacion,'id');   ?>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 140px">
                            <label>&nbsp;</label>
                            <div class="row">
                                <div id="divIndigena" class="col-md-2" style="width: 140px">
                                    <label for="indigena">Indígena</label>
                                    <input type="checkbox" name="indigena" id="indigena" value="2">
                                    <?php //echo $form->checkBox($tipoUbicacion,'id');   ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2" style="width: 140px">
                            <label>&nbsp;</label>
                            <div class="row">
                                <div id="divDificilAcceso" class="col-md-2" style="width: 140px">
                                    <label for="dificil_acceso">Dificil Acceso</label>
                                    <input type="checkbox" name="dificil_acceso" id="dificil_acceso" value="3">
                                    <?php //echo $form->checkBox($tipoUbicacion,'id');   ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>


                <div class="widget-header">
                    <h5>Coordenadas Geogr&aacute;ficas</h5>
                </div><br>

                <div class="row">
                    <div id="divLogitud" class="col-md-4">
                        <?php echo $form->labelEx($model, 'longitud', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'longitud', array('class' => 'span-8')); ?>
                        <?php echo $form->error($model, 'longitud'); ?>
                    </div>

                    <div id="divLatitud" class="col-md-4">
                        <?php echo $form->labelEx($model, 'latitud', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'latitud', array('style' => 'width:317px')); ?>
                        <?php //echo $form->error($model, 'latitud');  ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<br>

<div id="otrosDatosP" class="widget-box collapsed">
    <div class="widget-header">
        <h5>Otros Datos</h5>

        <div class="widget-toolbar">
            <a  href="#" data-action="collapse" >
                <i class="icon-chevron-down"></i>
            </a>
        </div>
    </div>

    <div id="infoGeneral" class="widget-body" >
        <div  class="widget-body-inner" >
            <div class="widget-main form">

                <div class="row">
                    <div id="divClasePlantel" class="col-md-4">
                        <?php echo $form->labelEx($model, 'clase_plantel_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'clase_plantel_id', CHtml::listData($clasePlantel, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-8')); ?>
                        <?php //echo $form->error($model, 'clase_plantel_id');  ?>
                    </div>

                    <div id="divTipoUbicacion" class="col-md-4">
                        <?php echo $form->labelEx($model, 'zona_ubicacion_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'zona_ubicacion_id', CHtml::listData($zona_ubicacion, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-8')); ?>
                        <?php //echo $form->error($model, 'zona_ubicacion_id');  ?>
                    </div>
                    <div id="divCategoria" class="col-md-4">
                        <?php echo $form->labelEx($model, 'categoria_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'categoria_id', CHtml::listData($categoria, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-8')); ?>
                        <?php //echo $form->error($model, 'categoria_id');  ?>
                    </div>

                    <div id="divCondicionesEstudio" class="col-md-4">
                        <?php echo $form->labelEx($model, 'condicion_estudio_id', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'condicion_estudio_id', CHtml::listData($condicionEstudio, 'id', 'nombre'), array(
                            'ajax' => array(
                                'type' => 'GET',
                                'update' => '#turno_nuevo_id',
                                'url' => CController::createUrl('crear/asignacionTurno'),
                            ),
                            'empty' => array('' => '-Seleccione-'), 'class' => 'span-8',
                                )
                        );
                        // echo $form->dropDownList($model, 'condicion_estudio_id', CHtml::listData($condicionEstudio, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-8', 'onChange' => 'asignacionTurno('.$plantel_id.')'));
                        ?>
                        <?php //echo $form->error($model, 'condicion_estudio_id');   ?>
                    </div>

                    <div id="divGenero" class="col-md-4">
                        <?php echo $form->labelEx($model, 'genero_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'genero_id', CHtml::listData($genero, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-8')); ?>
                        <?php //echo $form->error($model, 'genero_id');   ?>
                    </div>

                    <div id="divTurno" class="col-md-4">
                        <?php echo $form->labelEx($model, 'turno_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'turno_id', CHtml::listData($turno, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-8', 'id' => 'turno_nuevo_id')); ?>
                        <?php //echo $form->error($model, 'turno_id');   ?>
                    </div>

                    <div id="divRegimen" class="col-md-4">
                        <?php echo $form->labelEx($model, 'regimen_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model, 'regimen_id', CHtml::listData($regimenEstudio, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-8')); ?>
                        <?php //echo $form->error($model, 'regimen_id');   ?>
                    </div>

                </div>
                <hr>


            </div>
        </div>
    </div>
</div>

<br>
<hr>
<div class="row">

    <div class="col-md-6">
        <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("planteles/consultar/admin"); ?>"class="btn btn-danger">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>



    <div class="col-md-6 wizard-actions">
        <button type="submit" data-last="Finish" class="btn btn-primary btn-next">
            Guardar
            <i class="icon-save icon-on-right"></i>
        </button>
    </div>

</div>
<hr>

<br><br>
<?php $this->endWidget(); ?>

<script>
    $("#plantelMod-form").submit(function(evt) {
        evt.preventDefault();
        $.ajax({
            url: "../../actualizarDatosGenerales",
            data: $("#plantelMod-form").serialize(),
            dataType: 'html',
            type: 'post',
            success: function(resp) {
                if (isNaN(resp)) {
                    //document.getElementById("resultado").style.display = "none";
                    document.getElementById("resultadoPlantel").style.display = "block";
                    $("html, body").animate({scrollTop: 0}, "fast");
                    $("#resultadoPlantel").html(resp);

                } else {
                    window.location.reload();
                    //document.getElementById("resultado").style.display = "none";
                    //   document.getElementById("success").setAttribute("class","successDialogBox");
                    document.getElementById("guardo").style.display = "block";
                }

            }
        });
    });


</script>


