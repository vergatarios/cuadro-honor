<?php
$tdocumento_identificacion = array(
    array('id'=>'V',
        'nombre'=>'Venezolana'),
    array('id'=>'E',
        'nombre'=>'Extranjera'),
    array('id'=>'P',
        'nombre'=>'Pasaporte'),
)
?>
<div class="col-md-12">
    <div class="col-md-2">
        <?php echo CHtml::activeLabelEx($modelo,"tdocumento_identidad",array('class'=>"span-7")); ?>
        <?php echo CHtml::activeDropDownList($modelo,"[$index]tdocumento_identidad", CHtml::listData($tdocumento_identificacion, 'id', 'nombre'),array('class'=>"span-7"))?>
    </div>
    <div class="col-md-2">
        <?php echo CHtml::activeLabelEx($modelo,"documento_identidad",array('class'=>"span-7")); ?>
        <?php echo CHtml::activeTextField($modelo,"[$index]documento_identidad",array('class'=>"span-7 data-doc-ident", 'data-id'=>base64_encode($index))); ?>
    </div>
    <div class="col-md-3">
        <?php echo CHtml::activeLabelEx($modelo,"nombres",array('class'=>"span-7")); ?>
        <?php echo CHtml::activeTextField($modelo,"[$index]nombres",array('class'=>"span-7",'readOnly'=>'readOnly',)); ?>
    </div>
    <div class="col-md-3">
        <?php echo CHtml::activeLabelEx($modelo,"apellidos",array('class'=>"span-7")); ?>
        <?php echo CHtml::activeTextField($modelo,"[$index]apellidos",array('class'=>"span-7",'readOnly'=>'readOnly')); ?>
    </div>