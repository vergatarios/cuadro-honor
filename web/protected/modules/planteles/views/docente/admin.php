<?php

/* @var $this DocenteController */
/* @var $model Docente */

$this->breadcrumbs=array(
	'Docentes'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Docentes';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Docentes</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Docentes.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/planteles/docente/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Docentes                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'docente-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>nombres</center>',
            'name' => 'nombres',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[nombres]', $model->nombres, array('title' => '',)),
        ),
        array(
            'header' => '<center>apellidos</center>',
            'name' => 'apellidos',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[apellidos]', $model->apellidos, array('title' => '',)),
        ),
        array(
            'header' => '<center>tdocumento_identidad</center>',
            'name' => 'tdocumento_identidad',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[tdocumento_identidad]', $model->tdocumento_identidad, array('title' => '',)),
        ),
        array(
            'header' => '<center>documento_identidad</center>',
            'name' => 'documento_identidad',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[documento_identidad]', $model->documento_identidad, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_id</center>',
            'name' => 'usuario_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[usuario_id]', $model->usuario_id, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>nivel_id</center>',
            'name' => 'nivel_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[nivel_id]', $model->nivel_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>grado_instruccion_id</center>',
            'name' => 'grado_instruccion_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[grado_instruccion_id]', $model->grado_instruccion_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>estatus_docente_id</center>',
            'name' => 'estatus_docente_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[estatus_docente_id]', $model->estatus_docente_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>especificacion_estatus_id</center>',
            'name' => 'especificacion_estatus_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[especificacion_estatus_id]', $model->especificacion_estatus_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_elim</center>',
            'name' => 'fecha_elim',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[fecha_elim]', $model->fecha_elim, array('title' => '',)),
        ),
        array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[estatus]', $model->estatus, array('title' => '',)),
        ),
        array(
            'header' => '<center>escalafon_id</center>',
            'name' => 'escalafon_id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('Docente[escalafon_id]', $model->escalafon_id, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>