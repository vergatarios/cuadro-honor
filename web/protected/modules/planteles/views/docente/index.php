<?php

/* @var $this DocenteController */
/* @var $model AsignaturaDocente */

$this->breadcrumbs=array(
    'Planteles'=>array('/planteles/consultar/'),
    'Secciones'=>array('/planteles/seccionPlantel15/admin/id/'.$plantel_id),
    'Docentes',
);
$this->pageTitle = 'Administración de Docentes';

?>
    <div id="resultadoElim"></div>
    <div class = "widget-box collapsed">
        <div class = "widget-header">
            <h5>Identificación del Plantel <?php echo '"' . $datosPlantel['nom_plantel'] . '"'; ?></h5>
            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>
        </div>
        <div class = "widget-body">
            <div style = "display: none;" class = "widget-body-inner">
                <div class = "widget-main">
                    <div class="row" style="max-height:140px;" >
                        <?php $this->renderPartial('_informacionPlantel', array('datosPlantel' => $datosPlantel)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class = "widget-box collapsed">
        <div class = "widget-header">
            <h5>Identificación de la Sección</h5>
            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>
        </div>
        <div class = "widget-body">
            <div style = "display: none;" class = "widget-body-inner">
                <div class = "widget-main">
                    <div class="row" style="max-height:140px;" >
                        <?php $this->renderPartial('_informacionSeccion', array('datosSeccion' => $dataSeccion)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box">
        <div class="widget-header">
            <h5>Lista de Docentes</h5>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">

                    <div class="row space-6"></div>
                    <div>
                        <div id="resultadoOperacion">
                            <div class="infoDialogBox">
                                <p>
                                    En este módulo podrá registrar y/o actualizar los datos de Docentes.
                                </p>
                            </div>
                        </div>
                        <div class="row col-sm-8" id="resultadoOperacion"></div>


                        <div class="row space-20"></div>

                    </div>
                    <?php echo CHtml::hiddenField('plantel_id',$plantel_id);?>
                    <?php echo CHtml::hiddenField('seccion_plantel_id',$seccion_plantel_id);?>
                    <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'docente-grid',
                        'dataProvider'=>$model->search(),
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'summaryText' => 'Mostrando {start}-{end} de {count}',
                        'emptyText' => 'No se han encontrado Asignaturas asociadas a esta sección.',
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                        'afterAjaxUpdate' => "
                        function(){
                            $('.edit-docente').unbind('click');
    $('.edit-docente').bind('click', function (e) {
        var id= $(this).attr('data-id');
        editarAsignaturaDocente(e,id);
    });
                        }",
                        'columns'=>array(
                            array(
                                'header' => '<center>Asignatura</center>',
                                'name' => 'asignatura',
                                'value' => '(is_object($data->asignatura) AND isset($data->asignatura->nombre))?$data->asignatura->nombre:""',
                                'htmlOptions' => array('nowrap'=>'nowrap','width'=>'50%'),
                            ),
                            array(
                                'header' => '<center>Docente</center>',
                                'name' => 'docente',
                                'value' => '(isset($data) AND is_object($data->personalPlantel) AND is_object($data->personalPlantel->personal) AND isset($data->personalPlantel->personal->nombres))?$data->personalPlantel->personal->nombres." ".$data->personalPlantel->personal->apellidos:"SIN ASIGNAR"',
                                //'htmlOptions' => array('id'=>'DocentePlantel_apellidos'),
                                'htmlOptions' => array('nowrap'=>'nowrap','width'=>'45%'),
                                'filter' => CHtml::textField('DocentePlantel[apellidos]', '', array('title' => '',)),
                            ),
                            /* array(
                                 'header' => '<center>Fecha de Creación</center>',
                                 'name' => 'fecha_ini',
                                 'htmlOptions' => array(),
                                 //'filter' => CHtml::textField('Docente[fecha_ini]', $model->fecha_ini, array('title' => '',)),
                             ),*/
                            array(
                                'type' => 'raw',
                                'header' => '<center>Acción</center>',
                                'value' => array($this, 'getActionButtons'),
                                'htmlOptions' => array('nowrap'=>'nowrap','width'=>'5%'),
                            ),
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="space-6"><div class="col-md-12"></div></div>
    <div class="col-md-12">
        <div class="col-md-6">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("/planteles/seccionPlantel15/admin/id/" .$plantel_id); ?>" class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
            <?php
            $this->renderPartial('_accionesSobreSeccion', array('plantel_id' => base64_decode($plantel_id),'seccion_plantel_id'=>base64_decode($seccion_plantel_id),'estatus_asig_doc'=>$estatus_asig_doc)) ?>

            <?php   if (Yii::app()->user->pbac('write')) { ?>
                <?php   if ($estatus_asig_doc==false OR  $estatus_asig_doc==null OR $estatus_asig_doc=='E') { ?>
                    <a id="btnTerminarMatriculacion" style="height:42px;" href="#" data-action ='A' class="btn btn-warning change-status">
                        <i class="fa fa-lock"></i>
                        Cerrar Proceso de Asignación de Docentes
                    </a>
                <?php
                } else {
                    if (Yii::app()->user->pbac('admin')) {
                        ?>            <a id="btnActivarMatriculacion"style="height:42px;" href="#" data-action ='E' class="btn btn-warning change-status">
                            <i class="fa fa-unlock"></i>
                            Aperturar Proceso de Asignación de Docentes
                        </a>
                    <?php
                    }
                }
            }
            ?>
        </div>




    </div>

    <div id="dialog_docente" class="hide"></div>
    <div id="dialog_success" class="hide">
        <p></p>
    </div>
    <div id="confirm-status" class="hide">
        <div class="alert alert-info bigger-110">
            <p class="bigger-110 center">  Desea usted <strong><span class="confirm-action"></span></strong> el Proceso de Asignación de Docentes?</p>
        </div>
    </div>
    <div id="dialogPantalla" class="hide ">
        <div id="mensaje-confirm" class="alertDialogBox">
        </div>
    </div>

<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/modules/plantel/docente/index.js',CClientScript::POS_END
);
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/modules/plantel/docente/eliminar.js',CClientScript::POS_END
);

?>