<?php
/* @var $this DocenteController */
/* @var $model Docente */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
    'Planteles'=>array('/planteles/consultar/'),
    'Docentes',//=>array('/planteles/docente/lista/id/'.$plantel_id),
    'Consulta'
);
?>
<div class="col-xs-12">
<div class="row-fluid">

<div class="tabbable">

<ul class="nav nav-tabs">
    <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
    <li class=""><a href="#otrosDatos" data-toggle="tab">Planteles Asociados</a></li>
</ul>

<div class="tab-content">
<div class="tab-pane active" id="datosGenerales">
    <div class="view">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'docente-form',
            'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
        )); ?>

        <div id="div-datos-generales">

            <div class="widget-box">

                <div class="widget-header">
                    <h5>Vista de Datos Generales</h5>

                    <div class="widget-toolbar">
                        <a data-action="collapse" href="#">
                            <i class="icon-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-body-inner">
                        <div class="widget-main">
                            <div class="widget-main form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php
                                        $identificacion = '';
                                        $gradoInstruccion = '';
                                        $escalafon = '';
                                        $creadoPor = '';
                                        $estatusDocente = '';
                                        (isset($model->tdocumento_identidad) AND isset($model->documento_identidad) AND !is_null($model->tdocumento_identidad) AND !is_null($model->documento_identidad))? $identificacion = $model->tdocumento_identidad.'-'.$model->documento_identidad:'';
                                        (is_object($model->escalafon) AND isset($model->escalafon->nombre))? $escalafon = $model->escalafon->nombre:'';
                                        (is_object($model->usuarioIni) AND isset($model->usuarioIni->nombre) AND isset($model->usuarioIni->apellido))? $creadoPor = strtoupper($model->usuarioIni->nombre).' '.strtoupper($model->usuarioIni->apellido):'';
                                        (is_object($model->gradoInstruccion) AND isset($model->gradoInstruccion->nombre))? $gradoInstruccion = $model->gradoInstruccion->nombre:'';
                                        (is_object($model->estatusDocente) AND isset($model->estatusDocente->nombre))? $estatusDocente = $model->estatusDocente->nombre:'';
                                        ?>
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'identificacion'); ?>
                                            <?php echo $form->textField($model,'identificacion',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', 'disabled'=>'disabled', 'value'=>$identificacion)); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'nombres'); ?>
                                            <?php echo $form->textField($model,'nombres',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                        </div>


                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'apellidos'); ?>
                                            <?php echo $form->textField($model,'apellidos',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>

                                    <div class="col-md-12">

                                        <!--<div class="col-md-4">
                                                            <?php /*echo $form->labelEx($model,'nivel_id'); */?>
                                                            <?php /*echo $form->textField($model,'nivel_id', array('class' => 'span-12', 'disabled'=>'disabled',)); */?>
                                                        </div>-->
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'grado_instruccion_id'); ?>
                                            <?php echo $form->textField($model,'grado_instruccion_id', array('class' => 'span-12', 'disabled'=>'disabled','value'=>$gradoInstruccion)); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'estatus_docente_id'); ?>
                                            <?php echo $form->textField($model,'estatus_docente_id', array('class' => 'span-12', 'disabled'=>'disabled','value'=>$estatusDocente)); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'escalafon_id'); ?>
                                            <?php echo $form->textField($model,'escalafon_id', array('class' => 'span-12', 'disabled'=>'disabled','value'=>$escalafon)); ?>
                                        </div>

                                    </div>

                                    <div class="space-6"></div>
                                    <div class="col-md-12">
                                        <!-- <div class="col-md-4">
                                                            <?php /*echo $form->labelEx($model,'especificacion_estatus_id'); */?>
                                                            <?php /*echo $form->textField($model,'especificacion_estatus_id', array('class' => 'span-12', 'disabled'=>'disabled',)); */?>
                                                        </div>-->
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'usuario_ini_id'); ?>
                                            <?php echo $form->textField($model,'usuario_ini_id', array('class' => 'span-12', 'disabled'=>'disabled','value'=>$creadoPor)); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'fecha_ini'); ?>
                                            <?php echo $form->textField($model,'fecha_ini', array('class' => 'span-12', 'disabled'=>'disabled')); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $form->labelEx($model,'estatus'); ?>
                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                        </div>
                                    </div>
                                    <div class="space-6"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">

                    <div class="col-md-6">
                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/docente"); ?>" id="btnRegresar">
                            <i class="icon-arrow-left"></i>
                            Volver
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div>

<div class="tab-pane" id="otrosDatos">
    <div class="widget-box">

        <div class="widget-header">
            <h5>Vista de Datos Generales</h5>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-body-inner">
                <div class="widget-main">
                    <div class="widget-main form">
                        <div class="row">
                            <div class="col-md-12">
                                <?php $this->widget('zii.widgets.grid.CGridView', array(
                                    'id'=>'docente-grid',
                                    'dataProvider'=>$modelDocente->searchPlantelDocente(),
                                    'filter'=>false,
                                    'enableSorting'=>false,
                                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                    'summaryText' => 'Mostrando {start}-{end} de {count}',
                                    'emptyText' => 'El Docente no esta asociado a ningun plantel.',
                                    'pager' => array(
                                        'header' => '',
                                        'htmlOptions' => array('class' => 'pagination'),
                                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                    ),
                                    'columns'=>array(
                                        array(
                                            'header' => '<center>Plantel</center>',
                                            //'name' => 'plantel',
                                            'htmlOptions' => array(),
                                            'value' => '(isset($data) AND is_object($data->plantel) AND isset($data->plantel->nombre))?$data->plantel->nombre:""',
                                            //'filter' => CHtml::textField('Docente[estatus]', $model->estatus, array('title' => '',)),
                                        ),
                                        array(
                                            'header' => '<center>Código del Plantel</center>',
                                            //'name' => 'cod_plantel',
                                            'htmlOptions' => array(),
                                            'value' => '(isset($data) AND is_object($data->plantel) AND isset($data->plantel->cod_plantel))?$data->plantel->cod_plantel:""',
                                            //'filter' => CHtml::textField('Docente[estatus]', $model->estatus, array('title' => '',)),
                                        ),
                                        array(
                                            'header' => '<center>Estado</center>',
                                            //'name' => 'cod_plantel',
                                            'htmlOptions' => array(),
                                            'value' => '(isset($data) AND is_object($data->plantel) AND is_object($data->plantel->estado) AND isset($data->plantel->estado->nombre))?$data->plantel->estado->nombre:""',
                                            //'filter' => CHtml::textField('Docente[estatus]', $model->estatus, array('title' => '',)),
                                        ),
                                        array(
                                            'header' => '<center>Modalidad</center>',
                                            //'name' => 'cod_plantel',
                                            'htmlOptions' => array(),
                                            'value' => '(isset($data) AND is_object($data->plantel) AND is_object($data->plantel->modalidad) AND isset($data->plantel->modalidad->nombre))?$data->plantel->modalidad->nombre:""',
                                            //'filter' => CHtml::textField('Docente[estatus]', $model->estatus, array('title' => '',)),
                                        ),
                                    ),
                                )); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">

            <div class="col-md-6">
                <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/docente"); ?>" id="btnRegresar">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
            </div>

        </div>
    </div>
</div>

</div>
</div>

</div>
</div>