<?php
/* @var $this DocenteController */
/* @var $model Docente */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'docente-form',
                            'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-result">
                            <?php
                            if($model->hasErrors()):
                                $this->renderPartial('//errorSumMsg', array('model' => $model));
                            else:
                                ?>
                                <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                            <?php
                            endif;
                            ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-4">
                                                            <?php
                                                            $tdocumento_identificacion = array(
                                                                array('id'=>'V',
                                                                    'nombre'=>'Venezolana'),
                                                                array('id'=>'E',
                                                                    'nombre'=>'Extranjera'),
                                                                array('id'=>'P',
                                                                    'nombre'=>'Pasaporte'),
                                                            )
                                                            ?>
                                                            <?php echo $form->labelEx($model,'tdocumento_identidad'); ?>
                                                            <?php echo $form->dropDownList($model,'tdocumento_identidad',CHtml::listData($tdocumento_identificacion, 'id', 'nombre'),array('class'=>'span-12')); ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'documento_identidad'); ?>
                                                            <?php echo $form->textField($model,'documento_identidad',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'nombres'); ?>
                                                            <?php echo $form->textField($model,'nombres',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>
                                                    </div>
                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">
                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'apellidos'); ?>
                                                            <?php echo $form->textField($model,'apellidos',array('size'=>60, 'maxlength'=>60, 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                        <!-- <div class="col-md-4">
                                                            <?php /*echo $form->labelEx($model,'nivel_id'); */?>
                                                            <?php /*echo $form->textField($model,'nivel_id', array('class' => 'span-12',"required"=>"required",)); */?>
                                                        </div>-->


                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'grado_instruccion_id'); ?>
                                                            <?php echo $form->dropDownList($model, 'grado_instruccion_id', CHtml::listData(GradoInstruccion::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estatus_docente_id'); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus_docente_id', CHtml::listData(EstatusDocente::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-6"></div>
                                                    <div class="col-md-12">
                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'escalafon_id'); ?>
                                                            <?php echo $form->dropDownList($model, 'escalafon_id', CHtml::listData(TipoDocente::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'estatus'); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>
                                                    </div>
                                                    <div class="space-6"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/docente"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions">
                                        <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>
        <div id="dialog_error" class="hide"></div>

        <?php
          Yii::app()->clientScript->registerScriptFile(
            Yii::app()->request->baseUrl . '/public/js/modules/plantel/docente/form.js',CClientScript::POS_END
         );

        ?>
    </div>
</div>