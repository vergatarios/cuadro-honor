
<div class="widget-header">
    <h5>Colecci&oacute;n Bicentenaria</h5>

    <div class="widget-toolbar">
        <a data-action="collapse" href="#">
            <i class="icon-chevron-up"></i>
        </a>
    </div>

</div>
<div id="coleccionBicentenaria"></div>
<div id="msgAlerta"></div>
<div class="widget-body">
    <div class="widget-body-inner" style="display: block;">
        <div class="widget-main">
            <div style="display:block" class="search-form">
                <div class="widget-main form">

                            <?php
                            /* @var $this ServicioPlantelPeriodoController */
                            /* @var $model ServicioPlantelPeriodo */
                            /* @var $form CActiveForm */
                            ?>

                            <div class="form">

                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'servicio-plantel-periodo-form',
                                    'enableAjaxValidation' => false,
                                ));
                                ?>

                                <div id="divCantidad" class="col-md-3">
                                    <?php echo $form->labelEx($model, 'Cantidad de libros Recibido', array("class" => "col-md-12")); ?>
                                    <?php echo $form->textField($model, 'cantidad', array('class' => 'span-7')); ?>
                                    <?php echo $form->error($model, 'cantidad'); ?>
                                </div>
                                <div id="divCondicionServicion" class="col-md-3">
                                    <?php echo $form->labelEx($model, 'Condición de servicio', array("class" => "col-md-12")); ?>
                                    <?php echo $form->dropDownList($model, 'condicion_servicio_id', CHtml::listData($CondicionServicion, 'id', 'nombre'), array('empty' => '-Seleccione-', 'class' => 'span-7')); ?>
                                </div>
                                <div id="divCondicionServicion" class="col-md-3">
                                    <button id="guardarColeccionB" class="btn btn-success btn-next btn-sm" type="button" style="margin-top: 20px;" >
                                        <i class="icon-save icon-on-right"></i>
                                        Guardar
                                    </button>
                                </div>
                                <!--                                <div id="divguardarColeccionB" class="col-md-3">
                                                                    <button id="guardarColeccionB" class="btn btn-success btn-next btn-sm" type="button" style="padding-top: 3px; padding-bottom: 2px;">
                                    <i class="icon-save icon-on-right"></i>
                                    Guardar
                                </button>
                                                                </div>-->
<div id="divplantel" class="col-md-3">
                                    <?php echo $form->hiddenField($model, 'plantel_id', array('value' => $plantel_id)); ?>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>

                                <?php $this->endWidget(); ?>
                            </div><!-- form -->
                            <br>


                    <?php
//                    $this->renderPartial('_search', array(
//                        'model' => $model,
//                        'plantel_id' => $plantel_id,
//                    ));
                    ?>

                </div><!-- search-form -->
            </div><!-- search-form -->
        </div>
    </div>
    
</div>
<br>
<a class="btn btn-danger" href="/planteles/consultar" id="btnRegresar">
    <i class="icon-arrow-left"></i>
    Volver
</a>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/plantel/coleccionBicentenaria.js', CClientScript::POS_END);
?>