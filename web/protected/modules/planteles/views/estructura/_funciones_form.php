<?php

//Yii::app()->clientScript->registerCoreScript('yiiactiveform');
echo CHtml::scriptFile('/public/js/modules/plantel/estructura-plantel/funciones_form.js');

 $html_options=array('prompt'=>'-- Seleccione --',
                               'style' => 'width: 80%;',
                               'class'=>'span-12',
                                  //'onChange'=>'llamada_funcion_js(this.value);',
                                  //'options'=>array('valor_numerico'=>array('selected'=>'selected')) 
                                                  );


?>


<div class="widget-box">
    
        <div class="widget-header">
        
            <h5> <strong>Procesamiento de las Funciones del Personal </strong>   </h5>
            <!--<div class="widget-toolbar">

            </div>-->
        </div>
    
        <div class="widget-body">

        <div class="widget-body-inner">

        <div class="widget-main form">
 
                <div class="row-fluid" id="resultado">
                    
                        <div class="infoDialogBox">
                            <p>
                                Todos los campos con <span class="required">*</span> son obligatorios. 
                            </p>
                        </div>
                 </div>
            

    

         
<?php

        
        
                       $form=$this->beginWidget('CActiveForm', array(
                                                                        'id' => 'funcion-personal-form',
                                                                        'enableAjaxValidation' => false,
                                                                        'enableClientValidation' => true,
                                                                        'clientOptions' => array(
                                                                                                //  'validateOnSubmit' => true,
                                                                                                'validateOnType' => true,
                                                                                                'validateOnChange' => true
                                                                                                ),
                                                                    )
                                            );
        
                    ?> 
            
                     
         <div class="row">

                           

                         
             
             
             
       
            

         <div class="col-md-12" style="margin: 0px;line-height: 155%;">

                        <? if($form->errorSummary($model)): ?>


                                <div id ="div-result-message" class="errorDialogBox" >
                                        <?php echo $form->errorSummary($model); ?>

                                </div>



                        <?php endif; ?>
          </div>

          <div class="col-md-12"> <div class="space-6"> </div> </div> <!-- salto de linea -->
            
                    
            

         
         <div class="col-md-12" style="margin: 0px;line-height: 155%;">
                  
                               
                  <div class="col-md-4">
                      
                      <?php echo $form->labelEx($model, 'funcion_id', array("class" => "col-md-12")); ?>
                        <?php echo $form->dropDownList($model,'funcion_id',CHtml::listData($funciones,'id','nombre'),$html_options); ?>
                       <?php //echo $form->textField($model, 'funcion_id', array('style' => 'width: 80%;', 'maxlength' => '30','class'=>'span-12','placeholder'=>'','title'=>'')); ?>
                       <div class="row col-md-12" ><?php echo $form->error($model, 'funcion_id'); ?> </div>
              
                  </div>
             
                  <div class="col-md-4">
                      
                     &nbsp;
                     
                     <?php echo $form->hiddenField($model, 'personal_plantel_id', array('style' => 'width: 80%;', 'maxlength' => '30','class'=>'span-12','placeholder'=>'','title'=>'')); ?>  
                    
              
                  </div>
             
                  <div class="col-md-4">
                  
                     &nbsp;
              
                  </div>
                  
                 
                  
                  
                                            
          </div> <!-- primera fila -->
         

         

          
          
          <div class="col-md-12"> <div class="space-6"> </div> </div> <!-- salto de linea -->   
        
          <div class="row col-md-12" >  <!-- campos hidden -->
              <?php echo CHtml::hiddenField('id', base64_encode($model->id), array()); ?>
               
              
          
          
          
   
          </div>
          
         

    <?php $this->endWidget(); ?>

<div class="col-md-12"> <div class="space-6"> </div> </div> <!-- salto de linea -->  




</div> <!-- div class row -->
    

    
    
    </div>  <!-- div form -->
            
         
            
    </div>
            
     </div>



</div>

