

<?php 




$parametros_array=array(  
                        'modelPersonal'=>$modelPersonal,
                        'modelPersonalPlantel'=>$modelPersonalPlantel,  
                        'modelPlantel'=>$modelPlantel,
                        'modelFuncionPersonal'=>$modelFuncionPersonal,
                        'modelPersonalPlantelEspacios'=>$modelPersonalPlantelEspacios,
                        'modelEspecificacionEstatus'=>$modelEspecificacionEstatus,
                        'modelEstatusDocente'=>$modelEstatusDocente,
                        'modelPersonalEstudio'=>$modelPersonalEstudio,
                        'lista_tdocumento_identificacion'=>$lista_tdocumento_identificacion,
                        'lista_tipo_personal'=>$lista_tipo_personal,
                        'lista_grado_instruccion'=>$lista_grado_instruccion,
                        'lista_estatus_docente'=>$lista_estatus_docente,
                        'lista_especificacion_estatus'=>$lista_especificacion_estatus,
                        'lista_especialidad'=>$lista_especialidad, 
                        'lista_funcion'=>$lista_funcion,
                        'lista_denominacion'=>$lista_denominacion,
                        'lista_situacion_cargo'=>$lista_situacion_cargo,
                        'lista_tiempo_dedicacion'=>$lista_tiempo_dedicacion,
                        'display_div_atributo_espec'=>$display_div_atributo_espec, 
                        'display_div_seccion_dependencia'=>$display_div_seccion_dependencia,
                        'display_div_atributo_docente'=>$display_div_atributo_docente, 
                        'display_div_atributo_denom_espec'=>$display_div_atributo_denom_espec,
                        'etiqueta_dependencia'=>$etiqueta_dependencia,
                        'mensaje'=>$mensaje,
                        'formType'=>$formType,
                        'plantel_id'=>$plantel_id,
                        'indicador'=>$indicador,
                                        );


?>






    <div class="col-xs-12">
        <div class="row-fluid">
            
            <div>
                
                  <?php $this->renderPartial('_viewPlantel', array('modelPlantel' => $modelPlantel)); ?>
                
            </div>

            <div class="tabbable">

                <ul class="nav nav-tabs">
                    
                    <?php if($indicador==""&&$formType=="registro"):  ?>
                    <li class="active"><a href="#datosGenerales" data-toggle="tab">Personal: Datos Generales</a></li>           
                    <?php endif; ?>
                    
                    
                    <?php if($indicador==""&&$formType=="edicion"):  ?>                       
                
                    
                    <?php  if($modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO   ): ?>
                     <li class="active" ><a href="#datosGenerales" data-toggle="tab">Personal: Datos Generales</a></li>
                     <li><a href="#estudiosSuperiores" data-toggle="tab">Personal: Estudios Superiores </a></li>
                     <li><a href="#espacios" data-toggle="tab">Personal: Espacios</a></li>                    
                    
                    <?php endif; ?>
                     
                     
                     <?php  if($modelPersonalPlantel->tipo_personal_id!=Constantes::OBRERO   ): ?>
                     
                     <li class="active" > <a href="#datosGenerales" data-toggle="tab">Personal: Datos Generales</a> </li>
                     
                     <li> <a href="#estudiosSuperiores" data-toggle="tab">Personal: Estudios Superiores </a> </li>
                                       
                    
                    <?php endif; ?>
                     
                  
                    
                    
                    <?php endif; ?>
                    
                    
                    <?php if($indicador!=""&&$formType=="edicion"):  ?>
                    <li><a href="#datosGenerales" data-toggle="tab">Personal: Datos Generales </a></li>

                     <?php  if($modelPersonalPlantel->tipo_personal_id!=Constantes::OBRERO   ): ?>
                    
                      <li class="active"><a href="#estudiosSuperiores" data-toggle="tab">Personal: Estudios Superiores </a></li>
                    
                    <?php endif; ?>
                     
                     
                    
                    
                    <?php  if($modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO   ): ?>
                    
                     <li> <a href="#estudiosSuperiores" data-toggle="tab">Personal: Estudios Superiores </a> </li>
                     <li class="active"> <a href="#espacios" data-toggle="tab">Personal: Espacios </a></li>
                     
                    
                    <?php endif; ?>
                     
                     
                     
                     
                    
                    
                    
                    
                    <?php endif; ?>
                    
                    
                    
                    
                    
                   
                </ul>

                <div class="tab-content">
                    
                    <?php if($indicador==""&&$formType=="registro"):  ?>
                    
                    <div class="tab-pane active" id="datosGenerales">
                        
                        <?php $this->renderPartial('_datosGenerales',$parametros_array ); ?>
                        
                      
                    </div>
                    
                    
                    
                    <?php endif; ?>
                    
                    
                    
                    
                     <?php if($indicador==""&&$formType=="edicion"):  ?>
                    
                    
                    

                    
                    
                    <?php  if($modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO   ): ?>
                    
                    
                    <div class="tab-pane active" id="datosGenerales">
                        
                        <?php $this->renderPartial('_datosGenerales', $parametros_array ); ?>
                        
                      
                    </div>
                       
                       
                    <div class="tab-pane" id="estudiosSuperiores">
                        
                        <?php $this->renderPartial('_estudiosSuperiores', $parametros_array ); ?>
                        
                      
                    </div>
                    
                     </div>   
                    <div class="tab-pane" id="espacios">
                        
                        <?php $this->renderPartial('_espacios', $parametros_array ); ?>
                        
                      
                    </div> 
                        
                        
                    
                    <?php endif; ?>
                        
                        
                        
                    <?php  if($modelPersonalPlantel->tipo_personal_id!=Constantes::OBRERO   ): ?>
                    
                    
                    <div class="tab-pane active" id="datosGenerales">
                        
                        <?php $this->renderPartial('_datosGenerales', $parametros_array ); ?>
                        
                      
                    </div>
                      
                     
                    <div class="tab-pane" id="estudiosSuperiores">
                        
                        <?php $this->renderPartial('_estudiosSuperiores', $parametros_array ); ?>
                        
                      
                    </div>
                        
                    
                    <?php endif; ?>
                        
                        
                    
                    
                    
                    
                    
                    <?php endif; ?>
                    
                    
                     <?php if($indicador!=""&&$formType=="edicion"):  ?>
                    
                    <div class="tab-pane" id="datosGenerales">
                        
                        <?php $this->renderPartial('_datosGenerales', $parametros_array ); ?>
                        
                      
                    </div> 
                    
                    
                    <?php  if($modelPersonalPlantel->tipo_personal_id!=Constantes::OBRERO   ): ?>
                    
                     <div class="tab-pane active" id="estudiosSuperiores">
                        
                        <?php $this->renderPartial('_estudiosSuperiores', $parametros_array ); ?>
                        
                      
                    </div>                   
                    
                    <?php endif; ?>
                    

                    
                    <?php  if($modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO   ): ?>
                    
                        
                    <div class="tab-pane" id="estudiosSuperiores">
                        
                        <?php $this->renderPartial('_estudiosSuperiores', $parametros_array ); ?>
                        
                      
                    </div> 
                        
                     </div> 
                     <div class="tab-pane active" id="espacios">
                        
                        <?php $this->renderPartial('_espacios', $parametros_array ); ?>
                        
                      
                    </div>   
                        
                      
                        
                    
                    <?php endif; ?>
                    
                    
                    
                    
                    
                    
                    

                    
                    
                    
                    <?php endif; ?>
                    
                    
                    
                    
                    

                    

                </div> <!-- tab-content -->
            </div> <!-- tab-content -->
            

            
            
        </div>  <!-- tabbable -->
    </div> <!-- row-fluid -->
        
   </div> <!-- col-xs-12 -->
    <div id="resultDialog" class="hide"></div>
    <div id="dialog_error" class="hide"></div>

<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(
 Yii::app()->request->baseUrl . '/public/js/modules/plantel/estructura-plantel/form.js',CClientScript::POS_END
);

?>

