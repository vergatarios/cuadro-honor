
<div id="dialogPantallaPersonalEstudio" class="hide"></div>


<?php 

echo CHtml::scriptFile('/public/js/modules/plantel/estructura-plantel/estudiosSuperiores.js');








Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#personal-estudio-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

//Yii::app()->clientScript->registerCoreScript('yiiactiveform');

?>







<div class="widget-box">
 <div class="widget-header">
         <h5> Gesti&oacute;n de los estudios a nivel superior del personal: <b> <?php echo  $modelPersonal->nombres." ". $modelPersonal->apellidos; ?> </b>   </h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
               
                <div class="row space-6"></div>
                
                 
                 
                 <div class="row">
                     <div class="row col-sm-8" id="resultadoOperacionPersonalEstudio"> </div>
                     
                     
                     
                 </div>
                 <div class="col-md-12"> <div class="space-6"> </div> </div>
                
                 <div class="row">
                 
                 
                                            
            

                
                <div>
                    
                    
                    
                    
                        
                                <div class="pull-right" style="padding-left:10px;">
                                    <a  type="submit" onclick="VentanaDialogEstSup('','/planteles/estructura/registroPersonalEstudio','Registrar los Estudios Superiores del Personal','registroPersonalEstudio','')" data-last="Finish" class="btn btn-success btn-next btn-sm">
                                        <i class="fa fa-plus icon-on-right"></i>
                                        Registrar los Estudios del Personal
                                    </a>

                                </div>

                            <?php
                           
                        ?>

                        <div class="row space-20"></div>
							
		</div><!-- search-form -->
                
                </div> <!-- class-row -->
                
               <div>

                                            
                                            <?php 
                                            
                                            
                                            
                                              
                                            
                                            $this->widget('zii.widgets.grid.CGridView', array(
                                            'id'=>'personal-estudio-grid',                                            
                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                            'dataProvider'=>$modelPersonalEstudio->search_personal_estudio($modelPersonal->id),
                                            'filter'=>$modelPersonalEstudio,
                                            'summaryText'=> '<br/><h5> Cantidad Total de Registros: {count} / Mostrando: del {start} al {end} </h5>', 
                                            
                                            'pager' => array(
                                                            'header' => "",
                                                            'htmlOptions' => array('class' => 'pagination'),
                                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                                            ),
                                                
                                            'columns'=>array(
                                             array('name'=>'estudio_id',                                                    
                                                   'header' => '<center>  Estudios </center>',
                                                   'value' =>'$data->estudio->nombre',
                                                   'filter' => false,
                                                    /*'filter'=>CHtml::activeHiddenField($modelFuncionPersonal, 'funcion_id',array('id'=>"funcion_id",                                                                                                          
                                                                    'readonly'=>'readonly'

                                                                                                            )),*/
                                                ),                                                
                                             
                                             array('name'=>'especifique_estudio',                                                    
                                                   'header' => '<center> Estudio Detalle </center>',
                                                   //'value' =>'$data->estudio->nombre',
                                                   'filter' => false,
                                                    /*'filter'=>CHtml::activeHiddenField($modelFuncionPersonal, 'funcion_id',array('id'=>"funcion_id",                                                                                                          
                                                                                                           'readonly'=>'readonly'

                                                                                                            )),*/
                                                ),
                                                
                                                 array(
                                                        'header' => '<center>Estatus</center>',
                                                        'name' => 'estatus',
                                                        'value' => array($this, 'estatus'),
                                                        'filter' => false,
                                                         /* 'filter'=>CHtml::activeHiddenField($modelFuncionPersonal, 'estatus',array('id'=>"estatus",                                                                                                          
                                                                                                           'readonly'=>'readonly'

                                                                                                            )),*/
                                                        //'filter'=>array('A'=>'Activo','E'=>'Eliminado'),


                                                    ),
                                            
                                                
                                                array(                                                    
                                                        'type' => 'raw',
                                                        'header'=>'Acciones',        
                                                        'value'=>array($this,'columnaAccionesPersonalEstudio'), 
                                                     ),

                                                
                                            
                                                
                                                
                                            ),
                                            ));  ?>


                                        </div>
                
                
                
                
            </div>
        </div>
    </div>
    
    <div>
          <?php echo CHtml::hiddenField('_est_sup_personal_id',$modelPersonal->id, array()); ?>
                
    </div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>

    <hr>
    <div class="row">
        <div class="col-md-6">
            <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/estructura/lista/id/".base64_encode($plantel_id)); ?>" id="btnRegresar">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
    </div>






 
