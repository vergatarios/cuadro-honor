<?php
/* @var $this ConsultarController */
/* @var $model Plantel */

$this->breadcrumbs=array(
	'Plantels'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Plantel', 'url'=>array('index')),
	array('label'=>'Create Plantel', 'url'=>array('create')),
	array('label'=>'Update Plantel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Plantel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Plantel', 'url'=>array('admin')),
);
?>

<h1>View Plantel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cod_estadistico',
		'cod_plantel',
		'planta_fisica_id',
		'nombre',
		'denominacion_id',
		'tipo_dependencia_id',
		'estado_id',
		'municipio_id',
		'parroquia_id',
		'localidad_id',
		'direccion',
		'distrito_id',
		'zona_educativa_id',
		'modalidad_id',
		'nivel_id',
		'condicion_estudio_id',
		'correo',
		'telefono_fijo',
		'telefono_otro',
		'director_actual_id',
		'director_supl_actual_id',
		'subdirector_actual_id',
		'subdirector_supl_actual_id',
		'coordinador_actual_id',
		'coordinador_supl_actual_id',
		'clase_plantel_id',
		'condicion_infra_id',
		'categoria_id',
		'regimen_id',
		'posee_electricidad',
		'posee_edificacion',
		'logo',
		'observacion',
		'es_tecnica',
		'especialidad_tec_id',
		'usuario_ini_id',
		'fecha_ini',
		'usuario_act_id',
		'fecha_act',
		'fecha_elim',
		'estatus',
		'estatus_plantel_id',
		'latitud',
		'longitud',
		'annio_fundado',
		'turno_id',
		'genero_id',
		'tipo_ubicacion_id',
	),
)); ?>
