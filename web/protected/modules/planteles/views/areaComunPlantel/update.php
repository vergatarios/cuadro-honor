<?php
/* @var $this AreaComunPlantelController */
/* @var $model AreaComunPlantel */

$this->pageTitle = 'Actualización de Datos de Area Comun Plantels';

      $this->breadcrumbs=array(
	'Area Comun Plantels'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('edicionArea', array('model'=>$model, 'formType'=>'edicion', 'id_plantel'=>$id_plantel,)); ?>
