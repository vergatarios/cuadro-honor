<?php

/* @var $this AreaComunPlantelController */
/* @var $model AreaComunPlantel */

$this->breadcrumbs=array(
        
        //'Estructura del Plantel'=>array('/planteles/estructura/lista/id/'.$plantel_id),
	//'Área Común Plantel'=>array('/planteles/areaComunPlantel/lista/id/'.$plantel_id),
        'Área Común Plantel'=>array('/planteles/areaComunPlantel'),
	'Administración',
);
$this->pageTitle = 'Administración de Area Comun Plantel';

?>
<div class="widget-box">
    <div class="widget-header">

        <h5>Lista de Área Común Plantel</h5> 

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <?php if(Yii::app() -> user -> hasFlash('//msgBox')):?>
        <div class="successDialogBox">
        <?php echo Yii::app() -> user -> getFlash('//msgBox'); ?>
        </div>
        <?php endif; ?>
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Area Comun Plantels.
                            </p>
                        </div>
                    </div>
                  
                     <?php
                if (Yii::app()->user->pbac('planteles.areacomunplantel.admin')):
                    ?>
                     <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/planteles/"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Volver a <b>Planteles</b> para <b>Registrar otra Área Común</b></a>
                    </div>
                                        <?php
                                    endif;
                                    ?>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'area-comun-plantel-grid',
	'dataProvider'=>$model->search(),
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
               $('#AreaComunPlantel_fecha_ini').datepicker();
                    $('#AreaComunPlantel_fecha_act').datepicker();
                    $.datepicker.setDefaults($.datepicker.regional = {
                            dateFormat: 'dd-mm-yy',
                            showOn:'focus',
                            showOtherMonths: false,
                            selectOtherMonths: true,
                            changeMonth: true,
                            changeYear: true,
                            minDate: new Date(1800, 1, 1),
                            maxDate: 'today'
                        });
                    
                    $('#AreaComunPlantel_fecha_act').on('dblclick', function(){
                        $(this).val('');
                        $('#area-comun-plantel-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    });
                     $('#AreaComunPlantel_fecha_ini').on('dblclick', function(){
                        $(this).val('');
                        $('#area-comun-plantel-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    });

                }",
	'columns'=>array(
//                 
        array(
           
            'header' => '<center>Area Común</center>',
            'name' => 'area_comun_id',
            'value' => '(is_object($data->areaComun) && isset($data->areaComun->nombre))? $data->areaComun->nombre: ""',
            //'htmlOptions' => array(),
            //'filter' => CHtml::textField('AreaComunPlantel[area_comun_id]', $model->area_comun_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Nombre del Plantel</center>',
            'name' => 'plantel_id',
            'value' => '(is_object($data->plantel) && isset($data->plantel->nombre))? $data->plantel->nombre: ""',
            //'htmlOptions' => array(),            
            //'filter' => CHtml::textField('AreaComunPlantel[plantel_id]', $model->plantel_id, array('title' => '',)),
        ),
//         array(
//            'header' => '<center>Fecha de Creación</center>',
//            'name' => 'fecha_ini',
//            'htmlOptions' => array(),
//            'value' => array($this, 'getFechaIni'),
//            'filter' => CHtml::textField('AreaComunPlantel[fecha_ini]', $model->fecha_ini, array('title' => 'Fecha de Creación', 'readOnly' => 'readOnly')),
//        ),
//        array(
//            'header' => '<center>Fecha de Actualización</center>',
//            'name' => 'fecha_act',
//            'htmlOptions' => array(),
//            'value' => array($this, 'getFechaAct'),
//            'filter' => CHtml::textField('AreaComunPlantel[fecha_act]', $model->fecha_act, array('title' => 'Fecha de Actualización al hacer, Para ver lista hacer Doble Click', 'readOnly' => 'readOnly',)),
//        ),        
//        array(
//           'header' => '<center>Estatus</center>',
//            'name' => 'estatus',
//            'value' => array($this, 'getEstatus'),
//            'filter' => CHtml::dropDownList('AreaComunPlantel[estatus]', $model->estatus, array('' => '- - -', 'A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado'), array('title' => '')),
//        ),
             array(
             'name' => 'estatus',
             'header' => 'Estatus',
             'value' => 'strtr($data->estatus,array("A"=>"Activo", "E"=>"Inactivo"))',
             'filter' => array('A' => 'Activo', 'E' => 'Inactivo'),
                            ),
        array(
            'header' => '<center>Cantidad</center>',
            'name' => 'cantidad',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('AreaComunPlantel[cantidad]', $model->cantidad, array('title' => '',)),
        ),
		
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    //'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
    </div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>


<!--<div class="row">
    <div class="col-md-6">
        <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/estructura/lista/"); ?>" id="btnRegresar">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>
</div>-->