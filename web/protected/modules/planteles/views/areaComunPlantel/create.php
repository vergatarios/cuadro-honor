<?php
/* @var $this AreaComunPlantelController */
/* @var $model AreaComunPlantel */

$this->pageTitle = 'Registro de Area Comun Plantel';

      $this->breadcrumbs=array(
	'Planteles'=> array('/planteles/'),
        'Estructura del Plantel'=>array('/planteles/estructura/lista/id/'.$plantel_id),
	//'Área Común Plantel'=>array('/planteles/areaComunPlantel/lista/id/'.$plantel_id),
        'Área Común Plantel'=>array('/planteles/areaComunPlantel'),  
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro', 'plantel_id'=>$plantel_id)); ?>