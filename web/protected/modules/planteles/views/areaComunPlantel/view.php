<?php
/* @var $this AreaComunPlantelController */
/* @var $model AreaComunPlantel */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Area Comun Plantels'=>array('lista'),
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'area-comun-plantel-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Vista de Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        
                                                         <div class="row">  
                                                          <div class="col-md-6">
                                                          <label class="col-md-12" ><b>Nombre del Área Común:</b></label>			
                                                          </div>

                                                          <div class="col-md-6">
                                                          <label class="col-md-12" ><?php echo CHtml::encode($model->areaComun->nombre); ?></label>
                                                          </div>
                                                          </div>
                                                        
                                                         <div class="row">  
                                                          <div class="col-md-6">
                                                          <label class="col-md-12" ><b>Nombre del Plantel:</b></label>			
                                                          </div>

                                                          <div class="col-md-6">
                                                          <label class="col-md-12" ><?php echo CHtml::encode($model->plantel->nombre); ?></label>
                                                          </div>
                                                          </div>
                                                        <div class="row" >
                                                                    <div class="col-md-6">
                                                                    <label class="col-md-12" ><b>Fecha de Creación:</b></label>		
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                    <label class="col-md-12" ><?php echo CHtml::encode(date("d-m-Y H:i:s",strtotime($model->fecha_ini)));?></label>				
                                                                    </div>
                                                                </div>
                                                        <div class="row" >
                                                                    <div class="col-md-6">
                                                                        <label class="col-md-12" ><b>Estatus:</b></label>		
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="col-md-12" >
                                                                            <?php
                                                                            if ($model->estatus == "A") {
                                                                                echo "Activo";
                                                                            } else if ($model->estatus == "I") {
                                                                                echo "Inactivo";
                                                                            } else if ($model->estatus == "E") {
                                                                                echo "Eliminado";
                                                                            }
                                                                            ?>			
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                        <div class="row">                                                    

                                                                        <div class="col-md-6">
                                                                            <label class="col-md-12" ><b>Cantidad:</b></label>					
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <label class="col-md-12" ><?php echo CHtml::encode($model->cantidad); ?></label>					
                                                                        </div>
                                                                    </div>
                                                        

                                                  </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/areaComunPlantel/lista/id/".base64_encode($id_plantel)); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>