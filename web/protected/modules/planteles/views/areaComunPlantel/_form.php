
<?php
/* @var $this AreaComunPlantelController */
/* @var $model AreaComunPlantel */
/* @var $form CActiveForm */
?>

 
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'area-comun-plantel-form',
                                //'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>                                                                                      
                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>
                                    
                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">  
                                                 <?php
                                     if($model->hasErrors())
                                       $this->renderPartial('//errorSumMsg', array('model' => $model));
                                        else
                                      ?>
                                      
                                    <?php if(Yii::app() -> user -> hasFlash('//msgBox')):?>
                                    <div class="successDialogBox">
                                    <?php echo Yii::app() -> user -> getFlash('//msgBox'); ?>
                                    </div>
                                    
                                    <?php endif; ?>                                  
                                    
<!--                                    <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>-->
                                    <?php
                            
                            
                            ?>
                                    
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">                                          
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model,'area_comun_id'); ?>
                                                            <?php echo $form->dropDownList($model, 'area_comun_id',CHtml::listData(CAreaComun::getData('estatus', 'A'), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


<!--                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model,'plantel_id'); ?>
                                                            <?php echo $form->dropDownList($model, 'plantel_id', CHtml::listData(Plantel::model()->findAll(array('limit'=>50)), 'id', 'nombre'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>-->
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model,'plantel_id'); ?>
                                                            <br>
                                                            <?php $nombre=$model->plantel->nombre;?>
                                                             <?php echo CHtml::textField('plantel',"$nombre", array('class' => 'span-12', 'readonly'=>'readonly')); ?>
                                                            
                                                        </div>


<!--                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_elim'); ?>
                                                            <?php echo $form->textField($model,'fecha_elim', array('class' => 'span-12',)); ?>
                                                        </div>-->

                                                  </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model,'estatus'); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo',), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                                        </div>


                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model,'cantidad'); ?>
                                                            <?php echo $form->textField($model,'cantidad', array('class' => 'span-12',"required"=>"required",)); ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                        <div class="col-md-6">
                                            <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/areaComunPlantel/lista/id/".$plantel_id); ?>" id="btnRegresar">
                                                <i class="icon-arrow-left"></i>
                                                Volver
                                            </a>
                                        </div>
                                        
                                        <div class="col-md-6 wizard-actions">
                                            <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                                Guardar
                                                <i class="icon-save icon-on-right"></i>
                                            </button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

            </div>
        </div>

    </div>
</div>
<div id="dialogPantalla" class="hide"></div>
        <div id="resultDialog" class="hide"></div> 
        <div id="dialog_error" class="hide"></div>
