<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/AreaComunPlantel/admin.js', CClientScript::POS_END
);
?>

<?php

/* @var $this AreaComunPlantelController */
/* @var $model AreaComunPlantel */

$this->breadcrumbs=array(
    'Planteles'=> array('/planteles/'),
    'Estructura del Plantel'=>array('/planteles/estructura/lista/id/'.$plantel_id),
    //'Área Común Plantel'=>array('/planteles/areaComunPlantel/lista/id/'.$plantel_id),
    'Área Común Plantel'=>array('/planteles/areaComunPlantel'),
    'Administración',
);
$this->pageTitle = 'Administración de Area Comun Plantel';

?>
<div>

    <?php $this->renderPartial('_viewPlantel', array('model' => $model)); ?>

</div>
<div class="widget-box">
    <div class="widget-header">

        <h5>Lista de Área Común Plantel</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <?php if(Yii::app() -> user -> hasFlash('//msgBox')):?>
            <div class="successDialogBox">
                <?php echo Yii::app() -> user -> getFlash('//msgBox'); ?>
            </div>
        <?php endif; ?>
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Área Común de los Planteles.
                            </p>
                        </div>
                    </div>

                    <?php
                    if (Yii::app()->user->pbac('planteles.areaComunPlantel.admin')):
                        ?>
                        <div class="pull-right" style="padding-left:10px;">
                            <a href="<?php echo $this->createUrl("/planteles/areaComunPlantel/registro/id/".$plantel_id); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registro de Área Común</a>
                        </div>
                    <?php
                    endif;
                    ?>


                    <div class="row space-20"></div>

                </div>

                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'id'=>'area-comun-plantel-grid',
                    'dataProvider'=>$dataProvider,
                    'filter'=>$model,
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'summaryText' => 'Mostrando {start}-{end} de {count}',
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'afterAjaxUpdate' => "
                function(){
            

                }",
                    'columns'=>array(
//       
                        array(

                            'header' => '<center>Area Común</center>',
                            'name' => 'area_comun_id',
                            'value' => '(is_object($data->areaComun) && isset($data->areaComun->nombre))? $data->areaComun->nombre: ""',
                            'htmlOptions' => array(),
                            'filter' => CHtml::textField('AreaComunPlantel[area_comun_id]', $model->area_comun_id, array('title' => '',)),
                        ),
//        array(
//            'header' => '<center>Nombre del Plantel</center>',
//            'name' => 'plantel_id',
//            'value' => '(is_object($data->plantel) && isset($data->plantel->nombre))? $data->plantel->nombre: ""',
//            //'htmlOptions' => array(),            
//            'filter' => CHtml::textField('AreaComunPlantel[plantel_id]', $model->plantel_id, array('title' => '',)),
//        ),

                        array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'value' => array($this, 'getEstatus'),
                            'filter' => CHtml::dropDownList('AreaComunPlantel[estatus]', $model->estatus, array('' => '- - -', 'A' => 'Activo', 'I' => 'Inactivo'), array('title' => '')),
                        ),
                        array(
                            'header' => '<center>Cantidad</center>',
                            'name' => 'cantidad',
                            'htmlOptions' => array(),
                            //'filter' => CHtml::textField('AreaComunPlantel[cantidad]', $model->cantidad, array('title' => '',)),
                        ),

                        array(
                            'type' => 'raw',
                            'header' => '<center>Acción</center>',
                            'value' => array($this, 'getActionButtons'),
                            //'htmlOptions' => array('nowrap'=>'nowrap'),
                        ),
                    ),
                )); ?>
            </div>
        </div>
    </div>
</div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>
<div id="dialogInhabilitar" class="hide">

    <div class="alert alert-warning">
        <p class="bigger-110 center"> ¿Desea usted Inhabilitar esta Área Común del Plantel?</p>
    </div>
</div>
<div id="dialogActivar" class="hide">

    <div class="alertDialogBox">

        <p class="bolder center grey"> ¿Desea usted Activar esta Área Común del Plantel?</p>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <a class="btn btn-danger" href="<?php echo $this->createUrl("/planteles/estructura/lista/id/".$plantel_id); ?>" id="btnRegresar">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>
</div>