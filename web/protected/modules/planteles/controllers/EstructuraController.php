<?php
// actioncerrar 
class EstructuraController extends Controller
{

    /**   // en el presente codigo se encuentra seteado la variable $periodo_id=15; en caso de que cierren el periodo 14, se debe buscar y comentar donde se encuentre esto
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning periodo
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    
    const MODULO="EstructuraController";    
    const PLANTEL_COBRA="¿En qu&eacute; plantel cobra?";  // en que plantel cobra
    const PLANTEL_LABORA="¿En qu&eacute; plantel labora?"; // en que plantel labora    
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    
    
    
    public static $_permissionControl = array(
        'read' => 'Consulta de EstructuraController',
        'write' => 'Creación y Modificación de EstructuraController',
        'admin' => 'Administración Completa  de EstructuraController',
        'label' => 'Módulo de EstructuraController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules. loadmodel actionlista
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array( 'lista', 'consulta', 'registro', 'edicion', 'borrarPersonalPlantel','activarPersonalPlantel', 'admin','actualizarCampoCantidad','cerrarProcesoRegistroPersonal',
                                    'edicionFuncionPersonal','borrarFuncionPersonal','registroFuncionPersonal','activarFuncionPersonal','consultaFuncionPersonal', 'aperturarProcesoRegistroPersonal',
                                    'edicionPersonalPlantelEspacios','borrarPersonalPlantelEspacios','registroPersonalPlantelEspacios','activarPersonalPlantelEspacios','consultaPersonalPlantelEspacios',
                                    'registroPersonalEstudio','edicionPersonalEstudio','consultaPersonalEstudio','borrarPersonalEstudio','activarPersonalEstudio',
                                    
                               
                                    ),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin','actualizarCampoCantidad','cerrarProcesoRegistroPersonal', 'aperturarProcesoRegistroPersonal',
                                   'edicionFuncionPersonal','borrarFuncionPersonal','registroFuncionPersonal','activarFuncionPersonal','consultaFuncionPersonal',
                                   'edicionPersonalPlantelEspacios','borrarPersonalPlantelEspacios','registroPersonalPlantelEspacios','activarPersonalPlantelEspacios','consultaPersonalPlantelEspacios',
                                   'registroPersonalEstudio','edicionPersonalEstudio','consultaPersonalEstudio','borrarPersonalEstudio','activarPersonalEstudio',
                                  ),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta','obtenerDatosPersona','actualizarCampoCantidad','actualizarComboDenominacion',
                                   'actualizarComboEspecificacionEstatus','actualizarComboEspecialidadPersonal','actualizarComboFuncionPersonal',
                                   'ReporteEmpleados'),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    public function actionReporteEmpleados() {


        if ($this->hasRequest('tipo_personal')) {
            $personal=$this->getRequest('tipo_personal');
            $plantel=$this->getRequest('plantel');
            $periodo=$this->getRequest('periodo');
         }
        else {
            throw new CHttpException(404, "Fallo al recibir los datos.");
        }
        $plantel_deco = base64_decode($plantel);
        $periodo_deco = base64_decode($periodo);
        $personal_deco = base64_decode($personal);
        $modulo = "Planteles.Estructura.ReporteEmpleados";
        $ip = Yii::app()->request->userHostAddress;
        $usuario_id = Yii::app()->user->id;
        $username = Yii::app()->user->name;
        if (isset($plantel_deco)) {
            $plantel = Plantel::model()->findByPk($plantel_deco);
            $datosPersonal = PersonalPlantel::model()->obtenerDatosPersonal($plantel_deco, $periodo_deco, $personal_deco);
            $tipoPersonal = PersonalPlantel::model()->obtenerTipoPersonal($personal_deco);
            $nombre_pdf = $plantel['nombre'] . ' - ' . $plantel['cod_plantel'];
            $mpdf = new mpdf('', 'LEGAL', 0, '', 15, 15, 54, 50);            //$mpdf->SetMargins(3,69.1,70);
            $header = $this->renderPartial('_headerReportePersonal', array('model' => $plantel, 'tipoPersonal' => $tipoPersonal), true);
            $body = $this->renderPartial('_bodyReportePersonal', array('datosPlantel' => $datosPlantel, 'datosPersonal' => $datosPersonal), true);
            $mpdf->SetFont('sans-serif');
            $mpdf->SetHTMLHeader($header);          
            $mpdf->WriteHTML($body);
            $mpdf->Output($nombre_pdf . '.pdf', 'D');
            $this->registerLog('REPORTES', $modulo . 'PDF', 'EXITOSO', 'Descarga del pdf con el nombre: ' . $nombre_pdf . ' del plantel ' . $plantel['nombre'], $ip, $usuario_id, $username);
        } else
            throw new CHttpException(404, 'Estimado Usuario, no se ha encontrado el recurso solicitado. Vuelva a la página anterior e intente de nuevo');
    }
    
    
    
    public function actionActualizarComboEspecificacionEstatus()
    {

        
      
        
            if(isset($_POST['estatusDocente_id']))
            {   
                    $modelEspecificacionEstatus=new EspecificacionEstatus();
                    //Pinto el opcion de seleccione
                    echo CHtml::tag('option',array('value' =>''),'-- Seleccione --',true);

                    if($_POST['estatusDocente_id']=='')
                    {
                            echo CHtml::tag('option',array('value' =>''),'Debe seleccionar un elemento de la lista',true);
                            Yii::app()->end();
                    }
                    // traer la informacion a ser filtrada
                    $resultado=$modelEspecificacionEstatus->getEspecificacionEstatusFiltro($_POST['estatusDocente_id']); 

                    foreach($resultado as $key=>$data)
                    {
                            echo CHtml::tag('option',array('value' => $data->id),$data->nombre,true);
                    }

            } // fin de if
            else
            {
                echo CHtml::tag('option',array('value' =>''),'-- Seleccione --',true);
                Yii::app()->end();
            }

    } // fin del action: actionActualizarComboEspecificacionEstatus


    public function actionActualizarComboFuncionPersonal()
    {

        
      
        
            if(isset($_POST['personalPlantel_tipo_personal_id']))
            {   
                    $model=new PersonalPlantel();
                    //Pinto el opcion de seleccione
                    echo CHtml::tag('option',array('value' =>''),'-- Seleccione --',true);

                    if($_POST['personalPlantel_tipo_personal_id']=='')
                    {
                            echo CHtml::tag('option',array('value' =>''),'Debe seleccionar un elemento de la lista',true);
                            Yii::app()->end();
                    }
                    // traer la informacion a ser filtrada
                    $resultado=$model->getFuncionPersonalFiltro($_POST['personalPlantel_tipo_personal_id']); 

                    foreach($resultado as $key=>$data)
                    {
                           echo CHtml::tag('option',array('value' => $data["id"]),$data["nombre"],true);
                    }

            } // fin de if
            else
            {
                echo CHtml::tag('option',array('value' =>''),'-- Seleccione --',true);
                Yii::app()->end();
            }

    } // fin del action: actualizarComboFuncionPersonal

  
    public function actionActualizarComboEspecialidadPersonal()
    {

        
      
        
            if(isset($_POST['personalPlantel_tipo_personal_id']))
            {   
                    $model=new PersonalPlantel();
                    //Pinto el opcion de seleccione
                    echo CHtml::tag('option',array('value' =>''),'-- Seleccione --',true);

                    if($_POST['personalPlantel_tipo_personal_id']=='')
                    {
                            echo CHtml::tag('option',array('value' =>''),'Debe seleccionar un elemento de la lista',true);
                            Yii::app()->end();
                    }
                    // traer la informacion a ser filtrada
                    $resultado=$model->getEspecialidadPersonalFiltro($_POST['personalPlantel_tipo_personal_id']); 

                    foreach($resultado as $key=>$data)
                    {
                            echo CHtml::tag('option',array('value' => $data["id"]),$data["nombre"],true);
                    }

            } // fin de if
            else
            {
                echo CHtml::tag('option',array('value' =>''),'-- Seleccione --',true);
                Yii::app()->end();
            }

    } // fin del action: actualizarComboEspecialidadPersonal

   
    
    
      
    public function actionActualizarComboDenominacion()
    {

        
      
        
            if(isset($_POST['personalPlantel_tipo_personal_id']))
            {   
                    $model=new PersonalPlantel();
                    //Pinto el opcion de seleccione
                    echo CHtml::tag('option',array('value' =>''),'-- Seleccione --',true);

                    if($_POST['personalPlantel_tipo_personal_id']=='')
                    {
                            echo CHtml::tag('option',array('value' =>''),'Debe seleccionar un elemento de la lista',true);
                            Yii::app()->end();
                    }
                    // traer la informacion a ser filtrada
                    $resultado=$model->getDenominacionFiltro($_POST['personalPlantel_tipo_personal_id']); 

                    foreach($resultado as $key=>$data)
                    {
                            echo CHtml::tag('option',array('value' => $data["id"]),$data["nombre"],true);
                    }

            } // fin de if
            else
            {
                echo CHtml::tag('option',array('value' =>''),'-- Seleccione --',true);
                Yii::app()->end();
            }

    } // fin del action: actualizarComboEspecialidadPersonal

    
    

    public function actionLista()
    {
        if($this->hasQuery('id') AND !is_numeric($this->getIdDecoded($this->getQuery('id')))){
            throw new CHttpException(404,'Estimado usuario, no se han recibido los parametros validos para realizar esta acción. Regrese a la página anterior e intente nuevamente.');
        }
        
        
        $modelPeriodoEscolar=new PeriodoEscolar();
        $modelCierreRegistroPersonal=new CierreRegistroPersonal();
        $periodo_id_array=$modelPeriodoEscolar->getPeriodoActivo();
        $periodo_id=$periodo_id_array["id"];
        $periodo_id=15; // borrar cuando den la autorizacion de borrar el periodo anterior 
        $estatus_proceso_registro=Constantes::ESTATUS_PROCESO_ACTIVO;        
        
        $model=new PersonalPlantel('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('PersonalPlantel'))
        {
            $model->attributes=$this->getQuery('PersonalPlantel');
        }
        $id = $this->getQuery('id');
        $model->plantel_id=$this->getIdDecoded($id);
        
        $modelPlantel=$this->loadModelPlantel($model->plantel_id);
        
        
        
        $cargar_periodo_registrado=$modelCierreRegistroPersonal->getPeriodoRegistroPersonal($model->plantel_id, $periodo_id);
        
        if(count($cargar_periodo_registrado)>0)
        {
           $estatus_proceso_registro=$cargar_periodo_registrado[0]["estatus"]; 
            
        }
        
        Yii::app()->session["estatus_proceso_registro"]=$estatus_proceso_registro;
        
        
        
        
        
        
        //$dataProvider = $model->search();
        //echo $model->plantel_id;
        $this->render('admin',array(
            'model'=>$model,
            'estatus_proceso_registro'=>$estatus_proceso_registro,
            'plantel_id'=>$id,
            'periodo_id'=>$periodo_id,            
            'modelPlantel'=>$modelPlantel,
        ));
    }


    
    // action utilizado para actualizar el campo cantidad en la interfaz de espacios
    
    public function actionActualizarCampoCantidad()
    {
        
        $modelPersonalPlantelEspacios=new PersonalPlantelEspacios();        
        $area_comun_plantel_id="";     
        
        if(isset($_REQUEST["area_comun_plantel_id"]))
        {
           $area_comun_plantel_id=trim($_REQUEST["area_comun_plantel_id"]);
           
           if(!is_numeric($area_comun_plantel_id))
           {
               echo "Valor no válido";
               Yii::app()->end();
           }
           
           $resultado_cantidad=$modelPersonalPlantelEspacios->getCantidadEspacios($area_comun_plantel_id);  
           
           if(count($resultado_cantidad)==0)
           {
                echo "No registrado";
               Yii::app()->end();
               
           }
           
           echo trim($resultado_cantidad[0]["cantidad"]);
           Yii::app()->end();
  
        } // fin del if si existe la variable via request
        
        echo "Valores Inválidos";
        Yii::app()->end();
        
        
    } // fin del action: actionActualizarCampoCantidad 
    
    

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    
    ////////////// INICIO DE ACTIOS PARA LA ADMINISTRACION GENERAL DE PERSONALPLANTEL
    
    public function actionConsulta($id)
    {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    public function actionRegistro()
    {   
        
        if($this->hasQuery('id') AND !is_numeric($this->getIdDecoded($this->getQuery('id'))))
        {
            throw new CHttpException(404,'Estimado usuario, no se han recibido los parametros validos para realizar esta acción. Regrese a la página anterior e intente nuevamente.');
            
        } 
        
        
        
        
               
        // inicializacion de variables 
        $nombres="";
        $apellidos="";
        $fecha_nacimiento="";
        $sexo="";
        $datosPersona="";
        $estatus= Constantes::ESTATUS_ACTIVO;        
        $plantel_id = $this->getIdDecoded($this->getQuery('id')); 
        $usuario_id=Yii::app()->user->id;
        $fecha_hora_actual = date('Y-m-d H:i:s');
        $display_div_atributo_espec="display: none;";        
        $display_div_seccion_dependencia="display: none;";
        $display_div_atributo_docente="display: none;";
        $display_div_atributo_denom_espec="display: none;";
        $etiqueta_dependencia="";
        $pers_cond_nom_id=  Constantes::PERS_COND_ACT_EN_PLANTEL; 
        $indicador_dependencia=false;
        
        $mensaje="";
        $indicador="";
        $labora="S";
        $cobra="S";
        // fin de inicializacion de variables
       
        
        
        // inicio de inicialiacion / carga de los modelos
        $modelPersonal=new Personal();
        $modelPersonalPlantel=new PersonalPlantel();
        $modelAutoridadPlantel = new AutoridadPlantel();
        $modelPeriodoEscolar=new PeriodoEscolar();
        $modelPlantel=$this->loadModelPlantel($plantel_id);
        $modelFuncionPersonal=new FuncionPersonal();
        $modelPersonalPlantelEspacios=new PersonalPlantelEspacios();
        $modelEspecificacionEstatus=new EspecificacionEstatus();
        $modelEstatusDocente=new EstatusDocente();
        $modelPersonalEstudio=new PersonalEstudio();
        // fin de inicialiacion / carga de los modelos
        
        // inicio de variables para las listas desplegables
        $lista_tdocumento_identificacion=$modelPersonal->getNacionalidad();
        $lista_tipo_personal=$modelPersonal->getTipoPersonal();
        $lista_grado_instruccion=$modelPersonal->getGradoInstruccion(); 
        $lista_estatus_docente=$modelPersonal->getEstatusDocente();
        $lista_especificacion_estatus=$modelPersonal->getEspecificacionEstatus();
        $lista_especialidad=array();
        $lista_funcion=array();
        $lista_denominacion=array();
        $lista_situacion_cargo=$modelPersonalPlantel->getSituacionCargo();
        $lista_tiempo_dedicacion=$modelPersonalPlantel->getTiempoDedicacion();
        
        
       
        
        
        // fin  de variables para las listas desplegables
        
        //Inicio: otros tributos y validaciones 
        $periodo_id_array=$modelPeriodoEscolar->getPeriodoActivo();
        $periodo_id=$periodo_id_array["id"];
        $modelPersonalPlantel->cobra=$cobra;
        $modelPersonalPlantel->labora=$labora;
        $modelPersonalPlantel->pers_cond_nom_id=$pers_cond_nom_id;
        
        $periodo_id=15; // borrar despues de las pruebas
        
        //Fin: otros atributos y validaciones
        
        // Uncomment the following line if AJAX validation is needed
        //
        // $this->performAjaxValidation(array($modelPersonal, $modelPersonalPlantel ));
          
        // Inicio: establecer los escenarios de los modelos
        $modelPersonal->setScenario("personalRegistro");
        $modelPersonalPlantel->setScenario("personalRegistro");
        // Fin: establecer los escenarios de los modelos
            
            

         if( $this->hasPost('Personal') && $this->hasPost('PersonalPlantel') )      
         {  

                // Inicio: Inicializar todos los atributos del modelo con los del formulario
                $modelPersonal->attributes=$this->getPost('Personal');
                $modelPersonalPlantel->attributes=$this->getPost('PersonalPlantel');
                $modelEstatusDocente->id=$_POST['EstatusDocente']['id'];
                $indicador_dependencia=false;
                $lista_especialidad=$modelPersonal->getEspecialidad();
                $lista_funcion=$modelPersonal->getFuncion();
                $lista_denominacion=$modelPersonalPlantel->getDenominacion();
                // Fin: Inicializar todos los atributos del modelo con los del formulario
                

                $datosPersona = $modelAutoridadPlantel->busquedaSaimeMixta($modelPersonal->tdocumento_identidad, $modelPersonal->documento_identidad);
                if($datosPersona)
                {
                    (isset($datosPersona['nombre']))? $nombres=$datosPersona['nombre']:$nombres='';
                    (isset($datosPersona['apellido']))? $apellidos=$datosPersona['apellido']:$apellidos='';
                    (isset($datosPersona['fecha_nacimiento']))? $fecha_nacimiento=$datosPersona['fecha_nacimiento']:$fecha_nacimiento='';
                    (isset($datosPersona['sexo']))? $sexo=$datosPersona['sexo']:$sexo='';

                }
                
                

                
                
                
                // Inicio: Inicializar atributos restantes del modelo, y dar formato a datos que correspondan
                $modelPersonal->nombres=trim($nombres);
                $modelPersonal->apellidos=trim($apellidos);
                $modelPersonal->correo=trim($modelPersonal->correo);
                $modelPersonal->fecha_nacimiento=date("Y-m-d",  strtotime($fecha_nacimiento));
                $modelPersonal->sexo=trim($sexo);
                $modelPersonal->telefono_fijo=trim($modelPersonal->telefono_fijo);
                $modelPersonal->telefono_celular=trim($modelPersonal->telefono_celular);
                $modelPersonal->usuario_id=$usuario_id;
                $modelPersonal->usuario_ini_id=$usuario_id;
                $modelPersonal->fecha_ini=$fecha_hora_actual;
                $modelPersonal->estatus=$estatus;
                
                if(isset($modelPersonalPlantel->denominacion_especificacion))
                {
                    $modelPersonalPlantel->denominacion_especificacion=trim($modelPersonalPlantel->denominacion_especificacion);
                    $modelPersonalPlantel->denominacion_especificacion=  strtoupper($modelPersonalPlantel->denominacion_especificacion);
                    
                }
                

                // Fin: Inicializar atributos restantes del modelo, y dar formato a datos que correspondan




                
                
                if($modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE)
                {
                    $display_div_atributo_docente="display: block;";        
                    
                }
                else
                {
                     $display_div_atributo_docente="display: none;";
                }
                
                
                if($modelPersonalPlantel->denominacion_id!="")
                {
                    if($modelPersonalPlantel->denominacion_id!=Constantes::PERS_DENOM_DOC_AULA)
                    {
                        $display_div_atributo_denom_espec="display: block;";  
                    }
                    else 
                    {
                        $display_div_atributo_denom_espec="display: none;";

                    }
                              
                    
                }
                else
                {
                     $display_div_atributo_denom_espec="display: none;";
                }
                
                
                
               
                
                
                if( $modelPersonalPlantel->tipo_personal_id!="" && is_numeric($modelPersonalPlantel->tipo_personal_id) &&  $modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE)
                {
                    $display_div_atributo_espec="display: block;";
                }
                
                // validaciones para desplegar el la seccion de dependencia o no para los casos que aplique
                if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="S" )
                {
                    $display_div_seccion_dependencia="display: none;";
                    $etiqueta_dependencia="";                      

                }

                if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="N" )
                {
                    $display_div_seccion_dependencia="display: block;";
                    $etiqueta_dependencia= self::PLANTEL_LABORA;                    

                }

                if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="S" )
                {
                    $display_div_seccion_dependencia="display: block;";
                    $etiqueta_dependencia=self::PLANTEL_COBRA;
                    

                }


                if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="N" )
                {
                    $display_div_seccion_dependencia="display: none;";
                    $etiqueta_dependencia="";
                    $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_OTROS;

                    

                }
                
                
                
                    

                if( $modelPersonal->validate() &&  $modelPersonalPlantel->validate()   )
                {          
                    
                    
                    
                     $transaction=$modelPersonal->dbConnection->beginTransaction();         
                    
                    
                    try
                    {        
                             // validaciones para los casos en que el trabajador cobra o labore en el plantel
                             
                             if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="S" )
                             {
                                 $display_div_seccion_dependencia="display: none;";
                                 $etiqueta_dependencia="";
                                 $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_ACT_EN_PLANTEL; 
                                 
                             }

                             if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="N" )
                             {
                                 $display_div_seccion_dependencia="display: block;";
                                 $etiqueta_dependencia= self::PLANTEL_LABORA;
                                 $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_COBRA_PLANTEL_LAB_OTR;
                                 $indicador_dependencia=true;

                             }

                             if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="S" )
                             {
                                 $display_div_seccion_dependencia="display: block;";
                                 $etiqueta_dependencia=self::PLANTEL_COBRA;
                                 $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_LABORA_PLANTEL_COB_OTR;
                                 $indicador_dependencia=true;

                             }


                             if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="N" )
                             {
                                 $display_div_seccion_dependencia="display: none;";
                                 $etiqueta_dependencia="";
                                 $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_OTROS;

                                 throw new Exception('Las opciones de cobra y labora no pueden ser NO y NO respectivamente');

                             }
                        
                        
                        
                             // validando el correo que sea obligatorio solo para cuando el tipo de personal sea docente
                             if($modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE || $modelPersonalPlantel->tipo_personal_id==Constantes::ADMINISTRATIVO  )  
                             {
                                 
                                  
                                 
                                   if($modelPersonal->correo=="" || $modelPersonal->correo==null )
                                   {
                                        throw new Exception('Estimado usuario, para el tipo de personal Docente o Administrativo el correo debe ser obligatorio');  
                                         
                                   }
                                                                      
                             } 
                         
                            // validacion para cuando el tipo de personal es obrero

                             if($modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO )  //if( (int) $modelPersonalPlantel->tipo_personal_id==Constantes::ADMINISTRATIVO || $modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO )
                             {
                                   $areaComunPlantel=count($modelPersonal->getBuscarAreaComunPlantel($plantel_id)); 
                                   if($areaComunPlantel==0)
                                   throw new Exception('Estimado usuario, para el tipo de personal Obrero debe cargar previamente las áreas comunes del plantel. Regrese a la página anterior e intente nuevamente.');
                             }  
                             // validacion para cuando el tipo de personal es docente
                             // solicitado y mandado a quitar, si lo piden de nuevo descomentar
                            /* if($modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE)
                             {
                               if ($modelPersonalPlantel->denominacion_id=="")
                               {
                                  throw new Exception('Estimado usuario, para el tipo de personal Docente el campo denominaci&oacute;n es obligatorio'); 
                               }
                               
                             } */
                             

                             $validar_restriccion=$modelPersonalPlantel->getRestriccionesEstatus($modelPersonalPlantel->tipo_personal_id, $modelPersonal->especificacion_estatus_id);
                             
                             if(count($validar_restriccion)>0)
                             {
                                  throw new Exception('Estimado usuario, para el Tipo de Personal seleccionado no se permite la cla&uacute;sula indicada en la Especificaci&oacute;n del Estatus. '); 
                                 
                             }
                             


                             
                             

                             if($modelPersonalPlantel->denominacion_id!="")
                             {
                                    if($modelPersonalPlantel->denominacion_id!=Constantes::PERS_DENOM_DOC_AULA)
                                    {
                                        if($modelPersonalPlantel->denominacion_especificacion=="")
                                        {
                                            throw new Exception('Estimado usuario, para la Denominaci&oacute;n seleccionada debe indicar el campo Especifique denominaci&oacute;n. '); 
                                        } 
                                    }
                                    
                             }
                             

                                   
                             // obteniendo y validando la dependencia del plantel para los casos que aplique

                             
                             
                             if($indicador_dependencia)
                             {
                                 
                                 if($modelPersonalPlantel->plantel_id_dep_pers!="" || $modelPersonalPlantel->plantel_id_dep_pers!=null )
                                 {
                                    $codigo_plantel=$modelPersonalPlantel->plantel_id_dep_pers;
                                    $plantelDependencia = $modelPersonalPlantel->getPlantelDependenciaFiltro($modelPersonalPlantel->plantel_id_dep_pers);
                                     
                                    if( count($plantelDependencia) > 0 )
                                    {
                                        
                                        $modelPersonalPlantel->plantel_id_dep_pers=$plantelDependencia[0]["id"];
                                        
                                        if($plantel_id==$modelPersonalPlantel->plantel_id_dep_pers)
                                        {
                                            $modelPersonalPlantel->plantel_id_dep_pers=$codigo_plantel;
                                            throw new Exception('El c&oacute;digo de plantel que usted esta intentando ingresar, pertenece al plantel actual');
                                        }
                                        
                                        
                                        
                                        

                                    }
                                    else
                                    {
                                        throw new Exception('El c&oacute;digo de plantel ingresado no existe en la base de datos'); 
                                    }
                                     
                                     
                                     
                                 }
                                 else
                                 {
                                    throw new Exception('El c&oacute;digo de plantel es obligatorio para los casos en el que el personal no cobre o no labore en el plantel'); 
                                 }
                                 

                             }
                             
                             if(!$this->loadModelEspecialidadTipoPersonal($modelPersonalPlantel->tipo_personal_id, $modelPersonalPlantel->especialidad_id))
                             {
                                 throw new Exception('La especialidad del personal seleccionada no corresponde con el tipo de personal indicado'); 
                             }
                             
                             if(!$this->loadModelFuncionTipoPersonal($modelPersonalPlantel->tipo_personal_id, $modelPersonalPlantel->funcion_id))
                             {
                                  throw new Exception('La funci&oacute;n del personal seleccionada no corresponde con el tipo de personal indicado');
                             }
                             
                             if(!$this->loadModelDenominacionTipoPersonal($modelPersonalPlantel->tipo_personal_id, $modelPersonalPlantel->denominacion_id))
                             {
                                  throw new Exception('La Denominaci&oacute;n del personal seleccionada no corresponde con el tipo de personal indicado');
                             }
                             
                             
                             
                             
                             
                             

                             /*  Para evitar que el nombre y apellido sean modificados en el formulario a traves del firebug, capturamos la
                                 nacionalidad y la cedula, y buscamos nuevamente los nombres y apellidos del saime y damos formato            
                              */              


                             /*  Para evitar que el nombre y apellido sean modificados en el formulario a traves del firebug, capturamos la
                                 nacionalidad y la cedula, y buscamos nuevamente los nombres y apellidos del saime y damos formato            
                              */              


                        
                            // buscar a ver si la cedula esta registrada en la tabla personal, sino esta registrada se debe guardar
                            // en dos tablas en caso contrario se guarda en una, se valida que en la tabla personal_plantel no se duplique el registro
                            // al igual que en la tabla personal no se debe guardar doble
                            $buscarPersonal = $modelPersonal->getBuscarPersonal($modelPersonal->tdocumento_identidad,$modelPersonal->documento_identidad);

                            
                             /*  Para evitar que el nombre y apellido sean modificados en el formulario a traves del firebug, capturamos la
                                 nacionalidad y la cedula, y buscamos nuevamente los nombres y apellidos del saime y damos formato            
                              */
                            
                            if(count($buscarPersonal)==0)
                            {
                                
                                
                                
                               

                                if($modelPersonal->save())
                                {
                                    // si guarda en la tabla personal, entonces se captura el id, se establecer los valores del modelo PersonalPlantel se guarda en la tabla personalPlantel
                                                                        
                                    $modelPersonalPlantel->personal_id=$modelPersonal->id;
                                    $modelPersonalPlantel->plantel_id=$plantel_id;
                                    $modelPersonalPlantel->periodo_id=$periodo_id;                             
                                    $modelPersonalPlantel->usuario_ini_id=$usuario_id;
                                    $modelPersonalPlantel->fecha_ini=$fecha_hora_actual;
                                    $modelPersonalPlantel->estatus=$estatus;


                                    if($modelPersonalPlantel->save())
                                    {
                                        //guardarmos el log, cargamos los modelos previamente guardados y redireccionamos a la vista de modificar
                                        $this->registerLog('ESCRITURA', self::MODULO.".registro ", 'EXITOSO', 'Registro de personal ingresado exitosamente, con el id: '.$modelPersonal->id." y en el plantel con id: $plantel_id");
                                                                                 
                                        $transaction->commit(); 
                                        // hacemos comit y luego redireccionamos 
                                         $mensaje="Registro de personal ingresado exitosamente";
                                                                                                                         
                                        // //$this->redirect(array('pantalla','id'=>$model->id)); colocar a la pantalla a la que se dirije enviando los parametros
                                        

                                    }
                                    else
                                    {
                                        throw new Exception('Hubo un problema al momento de guardar el registro en la tabla PersonalPlantel');
                                    }

                                    //
                                    
                                }
                                else
                                {
                                    throw new Exception('Hubo un problema al momento de guardar el registro en la tabla Personal');
                                }
                                


                            }
                            else 
                            { 
                                    // si la persona esta registrada en el sistema validamos, si ya tiene un registro en la tabla personal_plantel para evitar duplicidad
                                    // si hay duplicidad enviamos mensaje de error, en caso contrario capturamos el id del personal y guardamos en la tabla personal plantel    
                                                       
                               
                                    $modelPersonalPlantel->personal_id=$buscarPersonal[0]["id"];
                                    $modelPersonalPlantel->plantel_id=$plantel_id;
                                    $modelPersonalPlantel->periodo_id=$periodo_id;                              
                                    $modelPersonalPlantel->usuario_ini_id=$usuario_id;
                                    $modelPersonalPlantel->fecha_ini=$fecha_hora_actual;
                                    $modelPersonalPlantel->estatus=$estatus;
                                    
                                                               
                                    if( $modelPersonalPlantel->getBuscarPersonalPlantel($buscarPersonal[0]["id"],$plantel_id,$periodo_id,$estatus) )
                                    {                                            
                                        throw new Exception('Ya se encuentra registrado en el sistema este personal y se encuentra en estatus "Activo" en este plantel y en el periodo actual');
                                    }
                                    
                                    
                                    if($modelPersonalPlantel->save())
                                    {
                                        //guardarmos el log, cargamos los modelos previamente guardados y redireccionamos a la vista de modificar
                                        
                                        $this->registerLog('ESCRITURA', self::MODULO.".registro ", 'EXITOSO', 'EL personal con el  id: '.$modelPersonal->id.", ha sido ingresado en el plantel con id: $plantel_id, exitosamente");
                                         
                                        $transaction->commit(); 
                                        // hacemos comit y luego redireccionamos 
                                         $mensaje="Registro de personal ingresado exitosamente";
                                                                                                                         
                                        // //$this->redirect(array('pantalla','id'=>$model->id)); colocar a la pantalla a la que se dirije enviando los parametros
                                        

                                    }
                                    else
                                    {
                                        throw new Exception('Hubo un problema al momento de guardar el registro en la tabla PersonalPlantel');
                                    }
                                    
                                    
                                    
                                    //$transaction->commit();


                                  



                            } // FIN DEL ELSE


                      //

                    } // fin del bloque try                  
                    catch(Exception $errores)
                    {
                            $transaction->rollBack();
                            //var_dump($errores->getMessage());
                            $modelPersonal->getErroresAdicionales($errores->getMessage());
                            //Yii::app()->user->setFlash('error','Ha ocurrido un error, por favor comuniquese con el administrador y reporte el siguiente mensaje: '.$errores->getMessage());
                           
                            
                            
                            
                    //$this->render('//error');
                    //Yii::app()->end();
                             //Yii::app()->end();

                            //Yii::app()->end();

                    } // fin del bloque try*/




                } // fin del if de la validacion de los formularios con el modelo








            } // fin del if de si todos los datos del formulario son enviados via post
           
        
        
        
           
        $this->render('create',array(
            'modelPersonal'=>$modelPersonal,
            'modelPersonalPlantel'=>$modelPersonalPlantel,
            'modelPlantel'=>$modelPlantel,
            'modelFuncionPersonal'=>$modelFuncionPersonal,
            'modelPersonalPlantelEspacios'=>$modelPersonalPlantelEspacios,
            'modelEspecificacionEstatus'=>$modelEspecificacionEstatus,
            'modelEstatusDocente'=>$modelEstatusDocente,
            'modelPersonalEstudio'=>$modelPersonalEstudio,
            'lista_tdocumento_identificacion'=>$lista_tdocumento_identificacion,
            'lista_tipo_personal'=>$lista_tipo_personal,
            'lista_grado_instruccion'=>$lista_grado_instruccion,
            'lista_estatus_docente'=>$lista_estatus_docente,
            'lista_especificacion_estatus'=>$lista_especificacion_estatus,
            'lista_especialidad'=>$lista_especialidad, 
            'lista_funcion'=>$lista_funcion,
            'lista_denominacion'=>$lista_denominacion,
            'lista_situacion_cargo'=>$lista_situacion_cargo,
            'lista_tiempo_dedicacion'=>$lista_tiempo_dedicacion,
            'plantel_id'=>$plantel_id,
            'display_div_atributo_espec'=>$display_div_atributo_espec,
            'display_div_atributo_docente'=>$display_div_atributo_docente,
            'display_div_seccion_dependencia'=>$display_div_seccion_dependencia,
            'display_div_atributo_denom_espec'=>$display_div_atributo_denom_espec, 
            'etiqueta_dependencia'=>$etiqueta_dependencia,
            'indicador'=>$indicador,
            'mensaje'=>$mensaje,
            
                      
            
        ));
        Yii::app()->end();
        
        
        
        
        
    } // fin del action para mostrar la pantalla de registro del personal, y la creacion del mismo 


    public function actionEdicion($id)
    {
        $indicador="";
        $array_indicador=array();
        
        /*if($this->hasQuery('id') AND !is_numeric($this->getIdDecoded($this->getQuery('id'))))
        {
            throw new CHttpException(404,'Estimado usuario, no se han recibido los parametros validos para realizar esta acción. Regrese a la página anterior e intente nuevamente.');
            
        } */
        
        // inicializacion de variables 
       
        $personal_plantel_id = base64_decode($id);        
        
        if(!is_numeric($personal_plantel_id))
        {
            $array_indicador=explode("-", $personal_plantel_id);           
            $personal_plantel_id=$array_indicador[0];
            $indicador=$array_indicador[1];
            
        }
        
        $nombres="";
        $apellidos="";
        $fecha_nacimiento="";
        $sexo="";
        $datosPersona="";
        $estatus=Constantes::ESTATUS_ACTIVO;        
        //$plantel_id = $this->getIdDecoded($this->getQuery('id')); 
        $usuario_id=Yii::app()->user->id;
        $fecha_hora_actual = date('Y-m-d H:i:s');
        $display_div_atributo_espec="display: none;";
        $mensaje="";        
        $display_div_seccion_dependencia="";
        $display_div_atributo_docente="";
        $display_div_atributo_denom_espec="";
        $etiqueta_dependencia="";       
       
        
        // fin de inicializacion de variables
       
        
        
        // inicio de inicialiacion / carga de los modelos
        $modelPersonalPlantel=$this->loadModelPersonalPlantel($personal_plantel_id);
        $modelPersonal=$this->loadModelPersonal($modelPersonalPlantel->personal_id);
        $modelPlantel=$this->loadModelPlantel($modelPersonalPlantel->plantel_id);
        $modelEspecificacionEstatus=$this->loadModelEspecificacionEstatus($modelPersonal->especificacion_estatus_id);
        $modelEstatusDocente=$this->loadModelEstatusDocente($modelEspecificacionEstatus->estatus_docente_id);
        $modelAutoridadPlantel = new AutoridadPlantel(); // busqueda de la persona en el saime
        $modelPeriodoEscolar=new PeriodoEscolar();
        $modelFuncionPersonal=new FuncionPersonal();
        $modelPersonalPlantelEspacios=new PersonalPlantelEspacios();
        $modelPersonalEstudio=new PersonalEstudio();
        
        if( $modelPersonalPlantel->plantel_id_dep_pers!="" || $modelPersonalPlantel->plantel_id_dep_pers!=null )
        {
            $modelPlantelDependencia=$this->loadModelPlantel($modelPersonalPlantel->plantel_id_dep_pers);
            $modelPersonalPlantel->plantel_id_dep_pers=$modelPlantelDependencia->cod_plantel;
            
        }
        
        // fin de inicialiacion / carga de los modelos
        
        // inicio de variables para las listas desplegables
        $lista_tdocumento_identificacion=$modelPersonal->getNacionalidad();
        $lista_tipo_personal=$modelPersonal->getTipoPersonal();
        $lista_grado_instruccion=$modelPersonal->getGradoInstruccion(); 
        $lista_estatus_docente=$modelPersonal->getEstatusDocente();
        $lista_especificacion_estatus=$modelPersonal->getEspecificacionEstatus();
        $lista_especialidad=$modelPersonal->getEspecialidad();
        $lista_funcion=$modelPersonal->getFuncion();
        $lista_denominacion=$modelPersonalPlantel->getDenominacion();
        $lista_situacion_cargo=$modelPersonalPlantel->getSituacionCargo();
        $lista_tiempo_dedicacion=$modelPersonalPlantel->getTiempoDedicacion();
        // fin de variables para las listas desplegables
        
        
        //Inicio: otros tributos y validaciones 
        $periodo_id_array=$modelPeriodoEscolar->getPeriodoActivo();        
        $periodo_id=$periodo_id_array["id"]; 
        $periodo_id=15;   // borrar despues de las pruebas  
        $plantel_id=$modelPersonalPlantel->plantel_id;
        $modelPersonal->fecha_nacimiento=date("Y-m-d",  strtotime($modelPersonal->fecha_nacimiento));
        $modelPersonalPlantel->horas_asignadas=number_format($modelPersonalPlantel->horas_asignadas, 2, '.', '');
        $modelPersonalPlantel->horas_asignadas=str_pad( (string) $modelPersonalPlantel->horas_asignadas, "5", "0", STR_PAD_LEFT );
        
        //Fin: otros atributos y validaciones
        
        // Uncomment the following line if AJAX validation is needed
        //
        // $this->performAjaxValidation(array($modelPersonal, $modelPersonalPlantel ));
          
        // Inicio: establecer los escenarios de los modelos
        $modelPersonal->setScenario("personalRegistro");
        $modelPersonalPlantel->setScenario("personalRegistro");
        // Fin: establecer los escenarios de los modelos
        
        // controlar la visualizacion del atributo docente
        if($modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE)
        {
            $display_div_atributo_docente="display: block;";
        }
        else
        {
             $display_div_atributo_docente="display: none;";
        }
        // controlar la visualizacion del atributo denominacion especificacion
        if($modelPersonalPlantel->denominacion_id!="")
        {
            if($modelPersonalPlantel->denominacion_id!=Constantes::PERS_DENOM_DOC_AULA)
            {
                $display_div_atributo_denom_espec="display: block;";  
            }
            else 
            {
                $display_div_atributo_denom_espec="display: none;";

            }


        }
        else
        {
             $display_div_atributo_denom_espec="display: none;";
        }
        
        
        
        if( $modelPersonalPlantel->tipo_personal_id!="" && is_numeric($modelPersonalPlantel->tipo_personal_id) &&  $modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE)
        {
            $display_div_atributo_espec="display: block;";
        }
        
         // validaciones para los casos en que el trabajador cobra o labore en el plantel

        if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="S" )
        {
            
            
            $display_div_seccion_dependencia="display: none;";
            $etiqueta_dependencia="";
            $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_ACT_EN_PLANTEL; 

        }

        if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="N" )
        {
            
            $display_div_seccion_dependencia="display: block;";
            $etiqueta_dependencia= self::PLANTEL_LABORA;
            $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_COBRA_PLANTEL_LAB_OTR;
            

        }

        if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="S" )
        {
            
            $display_div_seccion_dependencia="display: block;";
            $etiqueta_dependencia=self::PLANTEL_COBRA;
            $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_LABORA_PLANTEL_COB_OTR;
            

        }


        if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="N" )
        {
           
            $display_div_seccion_dependencia="display: none;";
            $etiqueta_dependencia="";
            $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_OTROS;

            throw new Exception('Las opciones de cobra y labora no pueden ser NO y NO respectivamente');

        }
        
        
        
            
            

         if( $this->hasPost('Personal') && $this->hasPost('PersonalPlantel') )      
         {  

                // Inicio: Inicializar todos los atributos del modelo con los del formulario
                $modelPersonal->attributes=$this->getPost('Personal');
                $modelPersonalPlantel->attributes=$this->getPost('PersonalPlantel');
                $indicador_dependencia=false;
                // Fin: Inicializar todos los atributos del modelo con los del formulario
                

                $datosPersona = $modelAutoridadPlantel->busquedaSaimeMixta($modelPersonal->tdocumento_identidad, $modelPersonal->documento_identidad);
                if($datosPersona)
                {
                    
                   
                    (isset($datosPersona['nombre']))? $nombres=$datosPersona['nombre']:$nombres='';
                    (isset($datosPersona['apellido']))? $apellidos=$datosPersona['apellido']:$apellidos='';
                    (isset($datosPersona['fecha_nacimiento']))? $fecha_nacimiento=$datosPersona['fecha_nacimiento']:$fecha_nacimiento='';
                    (isset($datosPersona['sexo']))? $sexo=$datosPersona['sexo']:$sexo='';

                }
                
                

                // Inicio: Inicializar atributos restantes del modelo, y dar formato a datos que correspondan
                $modelPersonal->nombres=trim($nombres);
                $modelPersonal->apellidos=trim($apellidos);
                $modelPersonal->correo=trim($modelPersonal->correo);
                $modelPersonal->fecha_nacimiento=date("Y-m-d",  strtotime($fecha_nacimiento));
                $modelPersonal->telefono_fijo=trim($modelPersonal->telefono_fijo);
                $modelPersonal->telefono_celular=trim($modelPersonal->telefono_celular);
                $modelPersonal->usuario_id=$usuario_id;
                $modelPersonal->usuario_act_id=$usuario_id;
                $modelPersonal->fecha_act=$fecha_hora_actual;
                $modelPersonal->estatus=$estatus;                
                $modelPersonal->sexo=trim($sexo);
                
                if(isset($modelPersonalPlantel->denominacion_especificacion))
                {
                    $modelPersonalPlantel->denominacion_especificacion=trim($modelPersonalPlantel->denominacion_especificacion);
                    $modelPersonalPlantel->denominacion_especificacion=  strtoupper($modelPersonalPlantel->denominacion_especificacion);
                    
                }
                

                // Fin: Inicializar atributos restantes del modelo, y dar formato a datos que correspondan


               // controlar la visualizacion del atributo docente despues de que llegue solicitud via post
                if($modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE)
                {
                    $display_div_atributo_docente="display: block;";
                }
                else
                {
                     $display_div_atributo_docente="display: none;";
                }
                
                // controlar la visualizacion del atributo denominacion especificacion despues de que llegue solicitud via post
                if($modelPersonalPlantel->denominacion_id!="")
                {
                    if($modelPersonalPlantel->denominacion_id!=Constantes::PERS_DENOM_DOC_AULA)
                    {
                        $display_div_atributo_denom_espec="display: block;";  
                    }
                    else 
                    {
                        $display_div_atributo_denom_espec="display: none;";

                    }


                }
                else
                {
                     $display_div_atributo_denom_espec="display: none;";
                }
                
                     
                if( $modelPersonalPlantel->tipo_personal_id!="" && is_numeric($modelPersonalPlantel->tipo_personal_id) &&  $modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE)
                {
                    $display_div_atributo_espec="display: block;";
                }
                
                               // validaciones para desplegar el la seccion de dependencia o no para los casos que aplique
                if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="S" )
                {
                    $display_div_seccion_dependencia="display: none;";
                    $etiqueta_dependencia="";                      

                }
                //validaciones para desplegar la seccion de dependencia para los casos que aplique
                if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="N" )
                {
                    $display_div_seccion_dependencia="display: block;";
                    $etiqueta_dependencia= self::PLANTEL_LABORA;                    

                }

                if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="S" )
                {
                    $display_div_seccion_dependencia="display: block;";
                    $etiqueta_dependencia=self::PLANTEL_COBRA;
                    

                }


                if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="N" )
                {
                    $display_div_seccion_dependencia="display: none;";
                    $etiqueta_dependencia="";
                    $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_OTROS;

                    

                }
                
                
                //var_dump($modelPersonalPlantel); plantel_id_dep_pers
                
                          
                if( $modelPersonal->validate() &&  $modelPersonalPlantel->validate()   )
                {
                            
                    
                    $transaction=$modelPersonal->dbConnection->beginTransaction();         
                    
                    
                    try
                    {           
                                // validaciones para los casos en que el trabajador cobra o labore en el plantel

                                if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="S" )
                                {
                                    $display_div_seccion_dependencia="display: none;";
                                    $etiqueta_dependencia="";
                                    $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_ACT_EN_PLANTEL;                                     
                                    $modelPersonalPlantel->plantel_id_dep_pers=null;

                                }

                                if($modelPersonalPlantel->cobra=="S" &&  $modelPersonalPlantel->labora=="N" )
                                {
                                    $display_div_seccion_dependencia="display: block;";
                                    $etiqueta_dependencia= self::PLANTEL_LABORA;
                                    $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_COBRA_PLANTEL_LAB_OTR;
                                    $indicador_dependencia=true;

                                }

                                if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="S" )
                                {
                                    $display_div_seccion_dependencia="display: block;";
                                    $etiqueta_dependencia=self::PLANTEL_COBRA;
                                    $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_LABORA_PLANTEL_COB_OTR;
                                    $indicador_dependencia=true;
                                    

                                }


                                if($modelPersonalPlantel->cobra=="N" &&  $modelPersonalPlantel->labora=="N" )
                                {
                                    $display_div_seccion_dependencia="display: none;";
                                    $etiqueta_dependencia="";
                                    $modelPersonalPlantel->pers_cond_nom_id= Constantes::PERS_COND_OTROS;
                                    $modelPersonalPlantel->plantel_id_dep_pers=null;

                                    throw new Exception('Las opciones de cobra y labora no pueden ser NO y NO respectivamente');

                                }
                        
                                // validando el correo que sea obligatorio solo para cuando el tipo de personal sea docente
                                if($modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE || $modelPersonalPlantel->tipo_personal_id==Constantes::ADMINISTRATIVO  )  
                                {



                                      if($modelPersonal->correo=="" || $modelPersonal->correo==null )
                                      {
                                           throw new Exception('Estimado usuario, para el tipo de personal Docente o Administrativo el correo debe ser obligatorio');  
                                      }

                                }
                        
                                    
                                // validando para el caso de personal obrero que se ingresen las areas comunes
                                if( $modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO )
                                //if( (int) $modelPersonalPlantel->tipo_personal_id==Constantes::ADMINISTRATIVO || $modelPersonalPlantel->tipo_personal_id==Constantes::OBRERO )
                                {
                                      $areaComunPlantel=count($modelPersonal->getBuscarAreaComunPlantel($plantel_id)); 
                                      if($areaComunPlantel==0)
                                      throw new Exception('Estimado usuario, para el tipo de personal Obrero, debe cargar previamente las áreas comunes del plantel. Regrese a la página anterior e intente nuevamente.');
                                }
                                
                                // validacion para cuando el tipo de personal es docente
                                // solicitado y mandado a quitar, si lo piden de nuevo descomentar
                                /* if($modelPersonalPlantel->tipo_personal_id==Constantes::DOCENTE)
                                {
                                  if ($modelPersonalPlantel->denominacion_id=="")
                                  {
                                     throw new Exception('Estimado usuario, para el tipo de personal Docente el campo denominaci&oacute;n es obligatorio'); 
                                  }

                                } */
                                

                                $validar_restriccion=$modelPersonalPlantel->getRestriccionesEstatus($modelPersonalPlantel->tipo_personal_id, $modelPersonal->especificacion_estatus_id);
                             
                                if(count($validar_restriccion)>0)
                                {
                                     throw new Exception('Estimado usuario, para el Tipo de Personal seleccionado no se permite la cla&uacute;sula indicada en la Especificaci&oacute;n del Estatus. '); 

                                }


                                
                                
                                if($modelPersonalPlantel->denominacion_id!="")
                                {
                                    if($modelPersonalPlantel->denominacion_id!=Constantes::PERS_DENOM_DOC_AULA)
                                    {
                                        if($modelPersonalPlantel->denominacion_especificacion=="")
                                        {
                                            throw new Exception('Estimado usuario, para la Denominaci&oacute;n seleccionada debe indicar el campo Especifique denominaci&oacute;n. '); 
                                        } 
                                    }
                                    
                                }
                                
                                

                                
                                

                                // obteniendo y validando la dependencia del plantel para los casos que aplique
                                
                                if($indicador_dependencia)
                                {
                                    
                                   

                                    if($modelPersonalPlantel->plantel_id_dep_pers!="" || $modelPersonalPlantel->plantel_id_dep_pers!=null )
                                    {
                                       $codigo_plantel=$modelPersonalPlantel->plantel_id_dep_pers;

                                       $plantelDependencia = $modelPersonalPlantel->getPlantelDependenciaFiltro($modelPersonalPlantel->plantel_id_dep_pers);

                                       if( count($plantelDependencia) > 0 ) 
                                       {

                                           $modelPersonalPlantel->plantel_id_dep_pers=$plantelDependencia[0]["id"];
                                           
                                           if($plantel_id==$modelPersonalPlantel->plantel_id_dep_pers)
                                           {
                                                $modelPersonalPlantel->plantel_id_dep_pers= $codigo_plantel;
                                                throw new Exception('El c&oacute;digo de plantel que usted esta intentando ingresar, pertenece al plantel actual');
                                           }
                                           

                                       }
                                       else
                                       {
                                           throw new Exception('El c&oacute;digo de plantel ingresado no existe en la base de datos'); 
                                       }



                                    }
                                    else
                                    {
                                       throw new Exception('El c&oacute;digo de plantel es obligatorio para los casos en el que el personal no cobre o no labore en el plantel'); 
                                    }


                                }
                                
                                if(!$this->loadModelEspecialidadTipoPersonal($modelPersonalPlantel->tipo_personal_id, $modelPersonalPlantel->especialidad_id))
                                {
                                    throw new Exception('La especialidad del personal seleccionada no corresponde con el tipo de personal indicado'); 
                                }

                                if(!$this->loadModelFuncionTipoPersonal($modelPersonalPlantel->tipo_personal_id, $modelPersonalPlantel->funcion_id))
                                {
                                     throw new Exception('La funci&oacute;n del personal seleccionada no corresponde con el tipo de personal indicado');
                                }
                                
                                if(!$this->loadModelDenominacionTipoPersonal($modelPersonalPlantel->tipo_personal_id, $modelPersonalPlantel->denominacion_id))
                                {
                                     throw new Exception('La Denominaci&oacute;n del personal seleccionada no corresponde con el tipo de personal indicado');
                                }
                                
                                // al momento de actualizar se debe validar si existe un registro en la tabla personal la cedula registrada
                                // en caso de ser asi enviar un mensaje de error
                                $validar_persona_por_actualizar = $modelPersonal->getValidarPersonaPorActualizar($modelPersonal->id,$modelPersonal->tdocumento_identidad,$modelPersonal->documento_identidad);
                                     
                                if( count($validar_persona_por_actualizar) > 0 )
                                {
                                    throw new Exception('El personal que esta tratando de ingresar, ya se encuentra en el sistema');
                                    
                                }
                             

                                if($modelPersonal->save())
                                {
                                    
                                   

                                  // si guarda en la tabla personal, entonces se captura el id, se establecer los valores del modelo PersonalPlantel se guarda en la tabla personalPlantel
                                                                        
                                    //$modelPersonalPlantel->personal_id=$modelPersonal->id;                                   
                                    //$modelPersonalPlantel->periodo_id=$periodo_id;                             
                                    $modelPersonalPlantel->usuario_ini_id=$usuario_id;
                                    $modelPersonalPlantel->fecha_ini=$fecha_hora_actual;
                                    $modelPersonalPlantel->estatus=$estatus;


                                    if($modelPersonalPlantel->save())
                                    {
                                        //guardarmos el log, cargamos los modelos previamente guardados y redireccionamos a la vista de modificar
                                        //$this->registerLog
                                        $this->registerLog('ESCRITURA', self::MODULO.".edicion ", 'EXITOSO', 'EL personal con el  id: '.$modelPersonal->id.", ha sido modificado en el plantel con id: $plantel_id, exitosamente");
                                         
                                        $transaction->commit(); 
                                        // hacemos comit y luego redireccionamos 
                                         $mensaje="Registro de personal modificado exitosamente";
                                         //$indicador="ind";
                                         $indicador="";
                                                                                                                         
                                        // //$this->redirect(array('pantalla','id'=>$model->id)); colocar a la pantalla a la que se dirije enviando los parametros
                                        

                                    }
                                    else
                                    {
                                        throw new Exception('Hubo un problema al momento de modificar el registro en la tabla PersonalPlantel');
                                    }

                                    //
                                    
                                }
                                else
                                {
                                    throw new Exception('Hubo un problema al momento de modificar el registro en la tabla Personal');
                                }
                                


                    } // fin del bloque try                  
                    catch(Exception $errores)
                    {
                            $indicador="";
                            $transaction->rollBack();
                            //var_dump($errores->getMessage());
                            $modelPersonal->getErroresAdicionales($errores->getMessage());
                            //Yii::app()->user->setFlash('error','Ha ocurrido un error, por favor comuniquese con el administrador y reporte el siguiente mensaje: '.$errores->getMessage());


                    } // fin del bloque try*/




                } // fin del if de la validacion de los formularios con el modelo
                else 
                {
                   $indicador=""; 
                }


            } // fin del if de si todos los datos del formulario son enviados via post
           
        
        
        
           
        $this->render('update',array(
            'modelPersonal'=>$modelPersonal,
            'modelPersonalPlantel'=>$modelPersonalPlantel,
            'modelPlantel'=>$modelPlantel,
            'modelFuncionPersonal'=>$modelFuncionPersonal,
            'modelPersonalPlantelEspacios'=>$modelPersonalPlantelEspacios,
            'modelEspecificacionEstatus'=>$modelEspecificacionEstatus,
            'modelEstatusDocente'=>$modelEstatusDocente,
            'modelPersonalEstudio'=>$modelPersonalEstudio,
            'lista_tdocumento_identificacion'=>$lista_tdocumento_identificacion,
            'lista_tipo_personal'=>$lista_tipo_personal,
            'lista_grado_instruccion'=>$lista_grado_instruccion,
            'lista_estatus_docente'=>$lista_estatus_docente,
            'lista_especificacion_estatus'=>$lista_especificacion_estatus,
            'lista_especialidad'=>$lista_especialidad, 
            'lista_funcion'=>$lista_funcion,
            'lista_denominacion'=>$lista_denominacion,
            'lista_situacion_cargo'=>$lista_situacion_cargo,
            'lista_tiempo_dedicacion'=>$lista_tiempo_dedicacion,
            'plantel_id'=>$plantel_id,
            'display_div_atributo_espec'=>$display_div_atributo_espec,
            'display_div_atributo_docente'=>$display_div_atributo_docente,
            'display_div_atributo_denom_espec'=>$display_div_atributo_denom_espec,
            'mensaje'=>$mensaje,
            'indicador'=>$indicador,
            'display_div_seccion_dependencia'=>$display_div_seccion_dependencia,
            'etiqueta_dependencia'=>$etiqueta_dependencia,
            
            
           
        ));
        Yii::app()->end();  
        
       
        
        
        
        
    } // fin del action para editar los datos del personal

  
    public function actionObtenerDatosPersona(){
        
        
       
       
        
        if (isset($_REQUEST)) 
        {
           
            
            $autoridadPlantel = '';
            $datosPersona = '';
            $nombresPersona = '';
            $apellidosPersona='';
            $fecha_nacimiento='';
            $sexo='';
            $tdocumento_identidad=trim($_POST['tdocumento_identidad']);
            $documento_identidad=trim($_POST['documento_identidad']);
           
            
            
           
            if(in_array($tdocumento_identidad, array('V','E','P')))
            {
                if(in_array($tdocumento_identidad, array('V','E')))
                {
                    if(strlen((string)$documento_identidad)>10)
                    {
                        $mensaje = 'Estimado usuario el campo Documento de Identidad no puede superar los diez (10) caracteres.';
                        $title='Notificación de Error';
                        echo json_encode(array('statusCode'=>'ERROR','mensaje'=>$mensaje,'title'=>$title));
                        Yii::app()->end();
                    }
                    else 
                    {
                        if(!is_numeric( $documento_identidad ))
                        {
                            $mensaje = 'Estimado usuario el campo Documento de Identidad debe poseer solo caracteres numericos.';
                            $title='Notificación de Error';
                            echo json_encode(array('statusCode'=>'ERROR','mensaje'=>$mensaje,'title'=>$title));
                            Yii::app()->end();
                        }
                    }
                }
                
                // buscamos en la tabla personal y personal plantel, sino hay registros procedemos a busca en el saime
                
                $modelPersonal=new Personal();
                $buscarPersonal=$modelPersonal->getBuscarPersonal($tdocumento_identidad, $documento_identidad);
                
                 if(count($buscarPersonal)==0)
                 {
                    // si no hay registros en la tabla personal buscamos en saime y actualizamos los datos del formulario
                    $autoridadPlantel = new AutoridadPlantel();
                    $datosPersona = $autoridadPlantel->busquedaSaimeMixta($tdocumento_identidad, $documento_identidad);
                    if($datosPersona)
                    {
                        
                        (isset($datosPersona['nombre']))? $nombresPersona=$datosPersona['nombre']:$nombresPersona='';
                        (isset($datosPersona['apellido']))? $apellidosPersona=$datosPersona['apellido']:$apellidosPersona='';
                        (isset($datosPersona['fecha_nacimiento']))? $fecha_nacimiento=date("d-m-Y",strtotime($datosPersona['fecha_nacimiento'])):$fecha_nacimiento='';
                        (isset($datosPersona['sexo']))? $sexo=$datosPersona['sexo']:$sexo='';
                        echo json_encode(array('statusCode'=>'SUCCESS','nombres'=>$nombresPersona,'apellidos'=>$apellidosPersona,'fecha_nacimiento'=>$fecha_nacimiento,'sexo'=>$sexo));
                        Yii::app()->end();
                    }
                    else 
                    {
                        $mensaje = 'El Documento de Identidad '.$tdocumento_identidad.'-'.$documento_identidad.' no se encuentra registrada en nuestro sistema, por favor contacte al personal de soporte mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>.';
                        $title='Notificación de Error';
                        echo json_encode(array('statusCode'=>'ERROR','mensaje'=>$mensaje,'title'=>$title));
                        Yii::app()->end();
                    }
                     
                 }
                 else
                 {
                    // en caso contrario si existen datos en la tabla personal, actualizamos los datos del formulario con los datos que se encuentran almacenados 
                        (isset($buscarPersonal[0]['nombres']))? $nombres=$buscarPersonal[0]['nombres']:$nombres='';
                        (isset($buscarPersonal[0]['apellidos']))? $apellidos=$buscarPersonal[0]['apellidos']:$apellidos='';
                        (isset($buscarPersonal[0]['correo']))? $correo=$buscarPersonal[0]['correo']:$correo='';
                        (isset($buscarPersonal[0]['telefono_fijo']))? $telefono_fijo=$buscarPersonal[0]['telefono_fijo']:$telefono_fijo='';
                        (isset($buscarPersonal[0]['telefono_celular']))? $telefono_celular=$buscarPersonal[0]['telefono_celular']:$telefono_celular='';
                        (isset($buscarPersonal[0]['grado_instruccion_id']))? $grado_instruccion_id=$buscarPersonal[0]['grado_instruccion_id']:$grado_instruccion_id='';
                        (isset($buscarPersonal[0]['especificacion_estatus_id']))? $especificacion_estatus_id=$buscarPersonal[0]['especificacion_estatus_id']:$especificacion_estatus_id='';
                        (isset($buscarPersonal[0]['fecha_nacimiento']))? $fecha_nacimiento=date("d-m-Y",strtotime($buscarPersonal[0]['fecha_nacimiento'])):$fecha_nacimiento='';
                        (isset($buscarPersonal[0]['sexo']))? $sexo=$buscarPersonal[0]['sexo']:$sexo='';
                        
                        
                        echo json_encode(array('statusCode'=>'SUCCESS_2','nombres'=>$nombres,
                                                                         'apellidos'=>$apellidos,
                                                                         'correo'=>$correo,
                                                                         'telefono_fijo'=>$telefono_fijo,
                                                                         'telefono_celular'=>$telefono_celular,
                                                                         'grado_instruccion_id'=>$grado_instruccion_id,
                                                                         'especificacion_estatus_id'=>$especificacion_estatus_id,
                                                                         'fecha_nacimiento'=>$fecha_nacimiento,
                                                                         'sexo'=>$sexo,
                                                                        
                                
                                                                                                ));
                        Yii::app()->end();
                     
                     
                     
                     
                     
                 }
                
                
                    
                
                
            }
            else 
            {
                $mensaje = 'Estimado usuario, se han enviado valores no permitidos en el campo Tipo de Documento de Identidad.';
                $title='Notificación de Error';
                echo json_encode(array('statusCode'=>'ERROR','mensaje'=>$mensaje,'title'=>$title));
                Yii::app()->end();
            }
        }
        else 
        {
            throw new CHttpException(403, 'No se han recibido solicitudes via post.');
        }
    }  // fin del action para capturar los datos del personal

  
    
    public function actionBorrarPersonalPlantel()
    {
		
		if(isset($_POST['id']))
		{
                    $id=$_POST['id'];
                    $id=base64_decode($id);
                    $model=$this->loadModelPersonalPlantel($id);
                    if($model)
                    {
                        $model->usuario_act_id=Yii::app()->user->id;
                        $model->fecha_elim=date("Y-m-d H:i:s");
                        $model->estatus=  Constantes::ESTATUS_INACTIVO;
                        if($model->save())
                        {
                            $this->registerLog('ESCRITURA', self::MODULO.".borrarPersonalPlantel ", 'EXITOSO', 'EL personal con el  id: '.$model->personal_id.", ha sido inactivado en el plantel con id: ".$model->plantel_id.", exitosamente");
                            //$this->registerLog();  
                            $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Inactivado con éxito.'));
                            $model=$this->loadModelPersonalPlantel($id);

                        } // fin de if si es guardado exitoso
                        else
                        {
                             throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

                        }

                    } // fin de if si existe el modelo
                    else
                    {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');		
                    }
		

		} // fin de if si es enviado el id via post

	} // fin del action actionBorrarPersonalPlantel
        
        
    public function actionActivarPersonalPlantel() 
    {

            if (isset($_POST['id'])) 
            {
                $id = $_POST['id'];
                $id = base64_decode($id);
                $model = $this->loadModelPersonalPlantel($id);
                if ($model) 
                {
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_act = date("Y-m-d H:i:s");
                    $model->estatus = Constantes::ESTATUS_ACTIVO;
                    if ($model->save()) 
                    {

                        //$this->registerLog(); 
                        $this->registerLog('ESCRITURA', self::MODULO.".activarPersonalPlantel ", 'EXITOSO', 'EL personal con el  id: '.$model->personal_id.", ha sido inactivado en el plantel con id: ".$model->plantel_id.", exitosamente");
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activado con éxito.'));
                        $model = $this->loadModelPersonalPlantel($id);
                    } 
                    else 
                    {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } 
                else 
                {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            } // fin de if si se recibe el id via post

        }  // fin de action actionActivarPersonalPlantel

    
        //  
        
        
        public function actionCerrarProcesoRegistroPersonal() 
        {

            if (isset($_POST) ) 
            {
                $plantel_id=$_POST['plantel_id'];
                $plantel_id=base64_decode($plantel_id);
                $periodo_id=$_POST['periodo_id'];                
                $model = new CierreRegistroPersonal();                
                $model->usuario_ini_id = Yii::app()->user->id;
                $model->fecha_ini = date("Y-m-d H:i:s");
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->fecha_elim = date("Y-m-d H:i:s");
                $model->estatus=Constantes::ESTATUS_PROCESO_CERRADO;
                $model->periodo_id = $periodo_id;
                $model->plantel_id = $plantel_id;                  
               
                
               
                
                if($model->validate())
                {
                    
                    if( !$model->getValidarPersonalPlantelExistente($model->plantel_id, $model->periodo_id) )
                    {
                            throw new CHttpException(999, "Error! Debe existir al menos un personal registrado en el plantel con estatus de activo.");
                    }
                    else
                    {
                        
                        if ($model->save()) 
                        {

                            //$this->registerLog(); 
                            $this->registerLog('ESCRITURA', self::MODULO.".cerrarProcesoRegistroPersonal ", 'EXITOSO', 'EL proceso con el  id: '.$model->id.", ha sido cerrado en el plantel con id: ".$model->plantel_id.", y el id de periodo: ".$model->periodo_id." exitosamente");
                            $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'El proceso de registro de personal ha sido cerrado exitosamente'));

                        } 
                        else 
                        {
                            throw new CHttpException(500, "Error! no se ha completa la operación comuniquelo al administrador del sistema.");
                        }
                        
                    }





                }

                
            } // fin de if si se recibe el id via post
            else
            {                 
                throw new CHttpException(999, 'Error! no se han recibido las variables via post, en caso de persistir el error comuniquelo al administrador del sistema');
                
            }

        }  // fin de action actionActivarPersonalPlantel

    
        public function actionAperturarProcesoRegistroPersonal() 
        {

            if (isset($_POST) ) 
            {
                $plantel_id=$_POST['plantel_id'];
                $plantel_id=base64_decode($plantel_id);
                $periodo_id=$_POST['periodo_id'];                
                $model = new CierreRegistroPersonal();                
                $model->usuario_ini_id = Yii::app()->user->id;
                $model->fecha_ini = date("Y-m-d H:i:s");
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_act = date("Y-m-d H:i:s");
                $model->fecha_elim = NULL;
                $model->estatus=Constantes::ESTATUS_PROCESO_ACTIVO;
                $model->periodo_id = $periodo_id;
                $model->plantel_id = $plantel_id;                  

                if($model->validate())
                {
                
                    if ($model->save()) 
                    {
                        
                        $this->registerLog('ESCRITURA', self::MODULO.".aperturarProcesoRegistroPersonal ", 'EXITOSO', 'EL proceso con el  id: '.$model->id.", ha sido aperturado en el plantel con id: ".$model->plantel_id.", y el id de periodo: ".$model->periodo_id." exitosamente");
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'El proceso de registro de personal ha sido aperturado exitosamente'));

                    } 
                    else 
                    {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                
                }
                
                
            } // fin de if si se recibe el id via post
            else
            {                 
                throw new CHttpException(500, 'Error! no se han recibido las variables via post, en caso de persistir el error comuniquelo al administrador del sistema');
                
            }

        }  // fin de action actionActivarPersonalPlantel

    
        
    
    
    /////////////////// FIN DE ACTIOS PARA LA ADMIINSTRACIO GENERAL DE PERSONALPLANTEL
    
  
        
        
            ///////////////////////////////// INICIO DE ACTIONS PARA LAS FUNCIONALIDADES DE GESTION DE ESTUDIOS SUPERIORES DEL PERSONAL /////////////////
    
    
        public function actionRegistroPersonalEstudio()
	{
		
                
                // inicio de modelos a instanciar
		$model=new PersonalEstudio();      
                // fin de modelos a instanciar
                
                $lista_estudios=$model->getEstudios();
                
                $model->setScenario("gestionPersonalEstudio");
                
                                
                if($model)
                {  
                    if(isset($_POST['PersonalEstudio']))
                    {
                        
                        
                        
                            $model->attributes=$_POST['PersonalEstudio'];
                            
                                                                         
                            
                            
                            // Inicio: Inicializar atributos y dando formato a datos que correspondan
                            $model->usuario_ini_id=Yii::app()->user->id;                           
                            $model->fecha_ini=date("Y-m-d H:i:s");                           
                            $model->estatus=Constantes::ESTATUS_ACTIVO;
                            $model->especifique_estudio=trim($model->especifique_estudio);
                            $model->especifique_estudio=  strtoupper($model->especifique_estudio);                           

                            // Fin: Inicializar atributos y dando formato a datos que correspondan
                            
                            
                            

                            

                            if($model->validate())
                            {
                                
                                
                                
                                 $transaction=$model->dbConnection->beginTransaction();
                    
                                 try                                  
                                 {
                                     
                                     $validar_personal_estudio=$model->getValidarPersonalEstudio("",$model->personal_id,$model->estudio_id,"registro");
                                     if(count($validar_personal_estudio) >0 )
                                     {
                                         throw new Exception('El registro que usted desea ingresar ya se encuentra registrado en el sistema');
                                     }
                                     
                                     
                                    if($model->save())
                                    {

                                        //$this->registerLog(); 
                                        $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Registro ingresado exitosamente, puede registrar otro registro.'));
                                        $model=new PersonalEstudio();
                                        $transaction->commit(); 

                                    }
                                    else
                                    {
                                             throw new Exception('Hubo un problema al momento de guardar el registro ');

                                    }
                                     
                                     
                                 } 
                                 
                                 
                                catch(Exception $errores)
                                {
                                        $transaction->rollBack();
                                        //var_dump($errores->getMessage());
                                        $model->getErroresAdicionales($errores->getMessage());
                                        

                                }









                               
                            }  // fin del validate




                    }  // fin de solicitud recibida via post

                } // fin de if, si existe el modelo
                

		$this->renderPartial('_estudiosSuperiores_form',array(
                    'model'=>$model,  
                    'lista_estudios'=>$lista_estudios,   
                    
		));
                
               
	} // fin del action RegistroFuncionPersonal
        
        
        
        
        
        public function actionEdicionPersonalEstudio()
	{
                // inicio de modelos a instanciar
		$model=new PersonalEstudio();                            
                // fin de modelos a instanciar
                
                
                $lista_estudios=$model->getEstudios();
		$id=$_REQUEST['id'];
		$id=base64_decode($id);
		$model=$this->loadModelPersonalEstudio($id);      
                
                $model->setScenario("gestionPersonalEstudio"); 
		
		if($model)
		{ 

			if(isset($_POST['PersonalEstudio']))
			{
                                $model->attributes=$_POST['PersonalEstudio'];                    

                                // Inicio: Inicializar atributos y dando formato a datos que correspondan
                                $model->usuario_act_id=Yii::app()->user->id;                                
                                $model->fecha_act=date("Y-m-d H:i:s");
                                $model->estatus=Constantes::ESTATUS_ACTIVO;
                                $model->especifique_estudio=trim($model->especifique_estudio);
                                $model->especifique_estudio=strtoupper($model->especifique_estudio);                             
                                // Fin: Inicializar atributos y dando formato a datos que correspondan
	
				if($model->validate())
                                {
                                    
                                    
                                    $transaction=$model->dbConnection->beginTransaction();
                    
                                 try                                  
                                 {
                                     
                                     $validar_personal_estudio=$model->getValidarPersonalEstudio($model->id,$model->personal_id,$model->estudio_id,"edicion");
                                     if(count($validar_personal_estudio) >0 )
                                     {
                                         throw new Exception('El registro que usted desea modificar ya se encuentra registrado en el sistema');
                                     }
                                     
                                     
                                    if($model->save())
                                    {
                                      
                                       // $this->registerLog(); 
                                        $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Actualizado exitosamente.'));
                                        $model=$this->loadModelPersonalEstudio($id);
                                        $transaction->commit();

                                    }
                                    else
                                    {
                                            throw new Exception(500, 'Hubo un problema al modificar el registro');

                                    }
                                     
                                     
                                     
                                     
                                     
                                 }
                                     
                                  catch(Exception $errores)
                                  {
                                            $transaction->rollBack();
                                            //var_dump($errores->getMessage());
                                            $model->getErroresAdicionales($errores->getMessage());


                                  }
                                    
                                    
                                    
				} // fin de validacion del modelo
						
			} // fin de if via post

                } // fin de if si existe el modelo
                else
                {
                    throw new CHttpException(403, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

                }

		$this->renderPartial('_estudiosSuperiores_form',array(
                                                    'model'=>$model,
                                                    'lista_estudios'=>$lista_estudios,
                                                   
                                                   
                                                   
		));
                
                
	} // fin del action edicionFuncionPersonal
        
        
        
        public function actionConsultaPersonalEstudio()
	{
                // inicio de modelos a instanciar
		$model=new PersonalEstudio();                 
                // fin de modelos a instanciar
                
                
                
                
		$id=$_REQUEST['id'];
		$id=base64_decode($id);
		$model=$this->loadModelPersonalEstudio($id); 
		$this->renderPartial('_estudiosSuperiores_view',array(
                                                    'model'=>$model,
                                                    
                                                   
                                                   
                                                   
		));
                
                
	} // fin del action consultaFuncionPersonal
        
        
        
        
        
        public function actionBorrarPersonalEstudio()
	{
		
		if(isset($_POST['id']))
		{
                    $id=$_POST['id'];
                    $id=base64_decode($id);
                    $model=$this->loadModelPersonalEstudio($id);
                    if($model)
                    {
                        $model->usuario_act_id=Yii::app()->user->id;
                        $model->fecha_elim=date("Y-m-d H:i:s");
                        $model->estatus=Constantes::ESTATUS_INACTIVO;
                        if($model->save())
                        {
                            
                            //$this->registerLog();  
                            $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Inactivado con éxito.'));
                            $model=$this->loadModelPersonalEstudio($id);

                        } // fin de if si es guardado exitoso
                        else
                        {
                             throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

                        }

                    } // fin de if si existe el modelo
                    else
                    {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');		
                    }
		

		} // fin de if si es enviado el id via post

	} // fin del action BorrarFuncionPersonal
        
        
        public function actionActivarPersonalEstudio() 
        {

            if (isset($_POST['id'])) 
            {
                $id = $_POST['id'];
                $id = base64_decode($id);
                $model = $this->loadModelPersonalEstudio($id);
                if ($model) 
                {
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_act = date("Y-m-d H:i:s");
                    $model->estatus = Constantes::ESTATUS_ACTIVO;
                    if ($model->save()) 
                    {

                        //$this->registerLog(); 
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activado con éxito.'));
                        $model = $this->loadModelPersonalEstudio($id);
                    } 
                    else 
                    {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } 
                else 
                {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            } // fin de if si se recibe el id via post

        }  // fin de action ActivarFuncionPersonal

        
        
        ///////////////////////////////// FIN DE ACTIONS PARA LAS FUNCIONALIDADES DE GESTION DE ESTUDIOS SUPERIORES DEL PERSONAL /////////////////

        
        
        
        
        
        
    
    ///////////////////////////////// INICIO ACTIONS PARA LAS FUNCIONALIDADES DE LA CARGA DE LAS FUNCIONES DEL PERSONAL /////////////////
    
    
        public function actionRegistroFuncionPersonal()
	{
		
                
                // inicio de modelos a instanciar
		$model=new FuncionPersonal();      
                // fin de modelos a instanciar
                
                $funciones=$model->getFunciones();
                

                
            
                $model->setScenario("gestionFuncionPersonal");
                
                if($model)
                {  
                    if(isset($_POST['FuncionPersonal']))
                    {
                            $model->attributes=$_POST['FuncionPersonal'];
                            
                                              
                            $model->usuario_ini_id=Yii::app()->user->id;                           
                            $model->fecha_ini=date("Y-m-d H:i:s");                           
                            $model->estatus=Constantes::ESTATUS_ACTIVO;
                            
                            
                            

                            

                            if($model->validate())
                            {
                                
                                
                                
                                 $transaction=$model->dbConnection->beginTransaction();
                    
                                 try                                  
                                 {
                                     
                                     $validar_funcion_personal=$model->getValidarFuncionPersonal("",$model->personal_plantel_id,$model->funcion_id,"registro");
                                     if(count($validar_funcion_personal) >0 )
                                     {
                                         throw new Exception('El registro que usted desea ingresar ya se encuentra registrado en el sistema');
                                     }
                                     
                                     
                                    if($model->save())
                                    {

                                        //$this->registerLog(); 
                                        $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Registro ingresado exitosamente, puede registrar otro registro.'));
                                        $model=new FuncionPersonal();
                                        $transaction->commit(); 

                                    }
                                    else
                                    {
                                             throw new Exception('Hubo un problema al momento de guardar el registro ');

                                    }
                                     
                                     
                                 } 
                                 
                                 
                                catch(Exception $errores)
                                {
                                        $transaction->rollBack();
                                        //var_dump($errores->getMessage());
                                        $model->getErroresAdicionales($errores->getMessage());
                                        

                                }









                               
                            }  // fin del validate




                    }  // fin de solicitud recibida via post

                } // fin de if, si existe el modelo
                

		$this->renderPartial('_funciones_form',array(
                    'model'=>$model,  
                    'funciones'=>$funciones,   
                    
		));
                
               
	} // fin del action RegistroFuncionPersonal
        
        
        
        
        
        public function actionEdicionFuncionPersonal()
	{
                // inicio de modelos a instanciar
		$model=new FuncionPersonal();                            
                // fin de modelos a instanciar
                
                
                $funciones=$model->getFunciones();
		$id=$_REQUEST['id'];
		$id=base64_decode($id);
		$model=$this->loadModelFuncionPersonal($id);      
                
                $model->setScenario("gestionFuncionPersonal"); 
		
		if($model)
		{ 

			if(isset($_POST['FuncionPersonal']))
			{
                                $model->attributes=$_POST['FuncionPersonal'];                               
                               

                               
                                $model->usuario_act_id=Yii::app()->user->id;                                
                                $model->fecha_act=date("Y-m-d H:i:s");
                                $model->estatus=Constantes::ESTATUS_ACTIVO;
                                
                                

					
				if($model->validate())
                                {
                                    
                                    
                                    $transaction=$model->dbConnection->beginTransaction();
                    
                                 try                                  
                                 {
                                     
                                     $validar_funcion_personal=$model->getValidarFuncionPersonal($model->id,$model->personal_plantel_id,$model->funcion_id,"edicion");
                                     if(count($validar_funcion_personal) >0 )
                                     {
                                         throw new Exception('El registro que usted desea modificar ya se encuentra registrado en el sistema');
                                     }
                                     
                                     
                                    if($model->save())
                                    {
                                      
                                       // $this->registerLog(); 
                                        $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Actualizado exitosamente.'));
                                        $model=$this->loadModelFuncionPersonal($id);
                                        $transaction->commit();

                                    }
                                    else
                                    {
                                            throw new Exception(500, 'Hubo un problema al modificar el registro');

                                    }
                                     
                                     
                                     
                                     
                                     
                                 }
                                     
                                  catch(Exception $errores)
                                  {
                                            $transaction->rollBack();
                                            //var_dump($errores->getMessage());
                                            $model->getErroresAdicionales($errores->getMessage());


                                  }
                                    
                                    
                                    
				} // fin de validacion del modelo
						
			} // fin de if via post

                } // fin de if si existe el modelo
                else
                {
                    throw new CHttpException(403, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

                }

		$this->renderPartial('_funciones_form',array(
                                                    'model'=>$model,
                                                    'funciones'=>$funciones,
                                                   
                                                   
                                                   
		));
                
                
	} // fin del action edicionFuncionPersonal
        
        
        
        public function actionConsultaFuncionPersonal()
	{
                // inicio de modelos a instanciar
		$model=new FuncionPersonal();                 
                // fin de modelos a instanciar
                
                
                
                
		$id=$_REQUEST['id'];
		$id=base64_decode($id);
		$model=$this->loadModelFuncionPersonal($id); 
		$this->renderPartial('_funciones_view',array(
                                                    'model'=>$model,
                                                    
                                                   
                                                   
                                                   
		));
                
                
	} // fin del action consultaFuncionPersonal
        
        
        
        
        
        public function actionBorrarFuncionPersonal()
	{
		
		if(isset($_POST['id']))
		{
                    $id=$_POST['id'];
                    $id=base64_decode($id);
                    $model=$this->loadModelFuncionPersonal($id);
                    if($model)
                    {
                        $model->usuario_act_id=Yii::app()->user->id;
                        $model->fecha_elim=date("Y-m-d H:i:s");
                        $model->estatus=Constantes::ESTATUS_INACTIVO;
                        if($model->save())
                        {
                            
                            //$this->registerLog();  
                            $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Inactivado con éxito.'));
                            $model=$this->loadModelFuncionPersonal($id);

                        } // fin de if si es guardado exitoso
                        else
                        {
                             throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

                        }

                    } // fin de if si existe el modelo
                    else
                    {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');		
                    }
		

		} // fin de if si es enviado el id via post

	} // fin del action BorrarFuncionPersonal
        
        
        public function actionActivarFuncionPersonal() 
        {

            if (isset($_POST['id'])) 
            {
                $id = $_POST['id'];
                $id = base64_decode($id);
                $model = $this->loadModelFuncionPersonal($id);
                if ($model) 
                {
                    $model->usuario_act_id = Yii::app()->user->id;
                    $model->fecha_act = date("Y-m-d H:i:s");
                    $model->estatus = Constantes::ESTATUS_ACTIVO;
                    if ($model->save()) 
                    {

                        //$this->registerLog(); 
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activado con éxito.'));
                        $model = $this->loadModelFuncionPersonal($id);
                    } 
                    else 
                    {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                } 
                else 
                {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');
                }
            } // fin de if si se recibe el id via post

        }  // fin de action ActivarFuncionPersonal

        
        
        ///////////////////////////////// FIN DE ACTIONS PARA LAS FUNCIONALIDADES DE LA CARGA DE LAS FUNCIONES DEL PERSONAL /////////////////

    
            
            
            
            
                
    ///////////////////////////////// INICIO ACTIONS PARA LAS FUNCIONALIDADES DE LA CARGA DE LOS ESPACIOS DEL PERSONAL DEL PLANTEL /////////////////
    
    
        public function actionRegistroPersonalPlantelEspacios()
	{
		
                
                // inicio de modelos a instanciar
		$model=new PersonalPlantelEspacios(); 
                $plantel_id=Yii::app()->session["sess_esp_plantel_id"];                
                $espacios=$model->getEspacios($plantel_id);                
                
               


                
                // fin de modelos a instanciar
                
            
                $model->setScenario("gestionPersonalPlantelEspacios");
                
                if($model)
                {  
                    if(isset($_POST['PersonalPlantelEspacios']))
                    {
                            $model->attributes=$_POST['PersonalPlantelEspacios'];
                            
                                              
                            $model->usuario_ini_id=Yii::app()->user->id;                           
                            $model->fecha_ini=date("Y-m-d H:i:s");                           
                            $model->estatus=Constantes::ESTATUS_ACTIVO;
                            

                            if($model->validate())
                            {
                                
                                
                                
                                 $transaction=$model->dbConnection->beginTransaction();
                    
                                 try                                  
                                 {
                                     
                                     $validar=$model->getValidarPersonalPlantelEspacios("",$model->personal_plantel_id,$model->area_comun_plantel_id,"registro");
                                     if(count($validar) >0 )
                                     {
                                         throw new Exception('El registro que usted desea ingresar ya se encuentra registrado en el sistema');
                                     }
                                     
                                     
                                     if( $model->cantidad < 1 )
                                     {
                                         throw new Exception(' La cantidad debe ser un número mayor o igual que cero ');
                                     }
                                     
                                     
                                     
                                     $resultado_cantidad=$model->getCantidadEspacios($model->area_comun_plantel_id);  

                                     if(count($resultado_cantidad)>0)
                                     {
                                         
                                        if( $model->cantidad > $resultado_cantidad[0]["cantidad"] )
                                        {
                                            throw new Exception('La cantidad debe ser menor o igual que: '.$resultado_cantidad[0]["cantidad"].", para el tipo de espacio seleccionado");
                                        }
     
                                     } //fin de if
                                     else
                                     {
                                         throw new Exception(' Por favor verifique si la cantidad de espacios para este plantel se encuentra cargado en la pantalla anterior ');
                                     }
                                     
                                     
                                     
                                     
                                     
                                    if($model->save())
                                    {

                                        //$this->registerLog(); 
                                        $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Registro ingresado exitosamente, puede registrar otro registro.'));
                                        $model=new PersonalPlantelEspacios();
                                        $transaction->commit(); 

                                    }
                                    else
                                    {
                                             throw new Exception('Hubo un problema al momento de guardar el registro ');

                                    }
                                     
                                     
                                 } 
                                 
                                 
                                catch(Exception $errores)
                                {
                                        $transaction->rollBack();
                                        //var_dump($errores->getMessage());
                                        $model->getErroresAdicionales($errores->getMessage());
                                        

                                }









                               
                            }  // fin del validate




                    }  // fin de solicitud recibida via post

                } // fin de if, si existe el modelo
                

		$this->renderPartial('_espacios_form',array(
                    'model'=>$model, 
                    'espacios'=>$espacios,
                    
                    
		));
	} // fin del action actionRegistroPersonalPlantelEspacios
        
        
        
        
        
        public function actionEdicionPersonalPlantelEspacios()
	{
                // inicio de modelos a instanciar
		$model=new PersonalPlantelEspacios();                            
                // fin de modelos a instanciar
                $plantel_id=Yii::app()->session["sess_esp_plantel_id"];                
                $espacios=$model->getEspacios($plantel_id);
                
		$id=$_REQUEST['id'];
		$id=base64_decode($id);
		$model=$this->loadModelPersonalPlantelEspacios($id);      
                
                $model->setScenario("gestionPersonalPlantelEspacios"); 
		
		if($model)
		{ 

			if(isset($_POST['PersonalPlantelEspacios']))
			{
                                $model->attributes=$_POST['PersonalPlantelEspacios'];                               
                               

                               
                                $model->usuario_act_id=Yii::app()->user->id;                                
                                $model->fecha_act=date("Y-m-d H:i:s");
                                $model->estatus=Constantes::ESTATUS_ACTIVO;
                                
                                

					
				if($model->validate())
                                {
                                    
                                    
                                    $transaction=$model->dbConnection->beginTransaction();
                    
                                 try                                  
                                 {
                                     
                                     $validar=$model->getValidarPersonalPlantelEspacios($model->id,$model->personal_plantel_id,$model->area_comun_plantel_id,"edicion");
                                     if(count($validar) >0 )
                                     {
                                         throw new Exception('El registro que usted desea modificar ya se encuentra registrado en el sistema');
                                     }
                                     
                                     
                                     if( $model->cantidad < 1 )
                                     {
                                         throw new Exception(' La cantidad debe ser un número mayor o igual que cero ');
                                     }
                                     
                                     
                                     
                                     $resultado_cantidad=$model->getCantidadEspacios($model->area_comun_plantel_id);  

                                     if(count($resultado_cantidad)>0)
                                     {
                                         
                                        if( $model->cantidad > $resultado_cantidad[0]["cantidad"] )
                                        {
                                            throw new Exception('La cantidad debe ser menor o igual que: '.$resultado_cantidad[0]["cantidad"].", para el tipo de espacio seleccionado");
                                        }
     
                                     } //fin de if
                                     else
                                     {
                                         throw new Exception(' Por favor verifique si la cantidad de espacios para este plantel se encuentra cargado en la pantalla anterior ');
                                     }
                                     
                                     
                                    if($model->save())
                                    {
                                      
                                       // $this->registerLog(); 
                                        $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Actualizado exitosamente.'));
                                        $model=$this->loadModelPersonalPlantelEspacios($id);
                                        $transaction->commit();

                                    }
                                    else
                                    {
                                            throw new Exception(500, 'Hubo un problema al modificar el registro');

                                    }
                                     
                                     
                                     
                                     
                                     
                                 }
                                     
                                  catch(Exception $errores)
                                  {
                                            $transaction->rollBack();
                                            //var_dump($errores->getMessage());
                                            $model->getErroresAdicionales($errores->getMessage());


                                  }
                                    
                                    
                                    
				} // fin de validacion del modelo
						
			} // fin de if via post

                } // fin de if si existe el modelo
                else
                {
                    throw new CHttpException(403, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

                }

		$this->renderPartial('_espacios_form',array(
                                                    'model'=>$model,
                                                    'espacios'=>$espacios,
                                                   
                                                   
                                                   
		));
	} // fin del action actionEdicionPersonalPlantelEspacios
        
        
        public function actionBorrarPersonalPlantelEspacios()
	{
		
		if(isset($_POST['id']))
		{
                    $id=$_POST['id'];
                    $id=base64_decode($id);
                    $model=$this->loadModelPersonalPlantelEspacios($id);
                    if($model)
                    {
                        $model->usuario_act_id=Yii::app()->user->id;
                        $model->fecha_elim=date("Y-m-d H:i:s");
                        $model->estatus=Constantes::ESTATUS_INACTIVO;
                        if($model->save())
                        {
                            
                            //$this->registerLog();  
                            $this->renderPartial("//msgBox",array('class'=>'successDialogBox','message'=>'Eliminado con éxito.'));
                            $model=$this->loadModelPersonalPlantelEspacios($id);

                        } // fin de if si es guardado exitoso
                        else
                        {
                             throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');

                        }

                    } // fin de if si existe el modelo
                    else
                    {

                    throw new CHttpException(404, 'Error! Recurso no encontrado!');		
                    }
		

		} // fin de if si es enviado el id via post

	} // fin del action actionBorrarPersonalPlantelEspacios
        
        
            public function actionActivarPersonalPlantelEspacios() 
            {

                if (isset($_POST['id'])) 
                {
                    $id = $_POST['id'];
                    $id = base64_decode($id);
                    $model = $this->loadModelPersonalPlantelEspacios($id);
                    if ($model) 
                    {
                        $model->usuario_act_id = Yii::app()->user->id;
                        $model->fecha_act = date("Y-m-d H:i:s");
                        $model->estatus = Constantes::ESTATUS_ACTIVO;
                        if ($model->save()) 
                        {
                           
                            //$this->registerLog(); 
                            $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activado con éxito.'));
                            $model = $this->loadModelPersonalPlantelEspacios($id);
                        } 
                        else 
                        {
                            throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                        }
                    } 
                    else 
                    {

                        throw new CHttpException(404, 'Error! Recurso no encontrado!');
                    }
                } // fin de if si se recibe el id via post
                
            }  // fin de action actionActivarPersonalPlantelEspacios

            
            
        public function actionConsultaPersonalPlantelEspacios()
	{
                // inicio de modelos a instanciar
		$model=new PersonalPlantelEspacios(); 
                $nombre_espacio="";
                // fin de modelos a instanciar
                
                
                
                
		$id=$_REQUEST['id'];
		$id=base64_decode($id);
		$model=$this->loadModelPersonalPlantelEspacios($id); 
                if($model->area_comun_plantel_id!=NULL ||$model->area_comun_plantel_id!="" ) 
                {
                    $datos_espacio=$model->getNombreEspacio($model->area_comun_plantel_id);
                    $nombre_espacio=$datos_espacio[0]["nombre"];
                }
                    
                
		$this->renderPartial('_espacios_view',array(
                                                    'model'=>$model,
                                                    'nombre_espacio'=>$nombre_espacio,
                                                    
                                                   
                                                   
                                                   
		));
                
                
	} // fin del action consultaFuncionPersonal
        
        
            
            
        
        
        ///////////////////////////////// FIN DE ACTIONS PARA LAS FUNCIONALIDADES DE LA CARGA DE LAS FUNCIONES LOS ESPACIOS DEL PERSONAL DEL PLANTEL /////////////////

    
            
            
            
            
       
   /////////////////// INICIO: SECCION DE CARGA DE LOS MODELOS //////////////////////////////         
            
    public function loadModelPersonalPlantelEspacios($id)
    {
        $model=PersonalPlantelEspacios::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'Model PersonalPlantelEspacios: The requested page does not exist.');
        }
        return $model;
    }
    
    
    
    public function loadModelPersonalPlantel($id)
    {
        $model=PersonalPlantel::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'Model PersonalPlantel: The requested page does not exist.');
        }
        return $model;
    }
    
    public function loadModelPersonal($id)
    {
        $model=Personal::model()->findByPk($id);
        if($model===null){
            throw new CHttpException(404,'Model Personal: The requested page does not exist.');
        }
        return $model;
    }
    
    
    public function loadModelPlantel($id)
    {
        $model=  Plantel::model()->findByPk($id);
        if($model===null)
        {
            throw new CHttpException(404,'Model Plantel: The requested page does not exist.');
        }
        return $model;
    }
    
    
    
    public function loadModelFuncionPersonal($id)
    {
        $model= FuncionPersonal::model()->findByPk($id);
        if($model===null)
        {
            throw new CHttpException(404,'Model FuncionPersonal: The requested page does not exist.');
        }
        return $model;
    } 
    
    
    public function loadModelPersonalEstudio($id)
    {
        $model= PersonalEstudio::model()->findByPk($id);
        if($model===null)
        {
            throw new CHttpException(404,'Model PersonalEstudio: The requested page does not exist.');
        }
        return $model;
    }
    
    
    
    public function loadModelEstatusDocente($id)
    {
        $model= EstatusDocente::model()->findByPk($id);
        if($model===null)
        {
            throw new CHttpException(404,'Model EstatusDocente: The requested page does not exist.');
        }
        return $model;
    } // funcion para cargar el modelo de EstatusDocente
    
    
    public function loadModelEspecificacionEstatus($id)
    {
        $model= EspecificacionEstatus::model()->findByPk($id);
        if($model===null)
        {
            throw new CHttpException(404,'Model EspecificacionEstatus: The requested page does not exist.');
        }
        return $model;
    }
    
    public function loadModelEspecialidadTipoPersonal($tipo_personal_id,$especialidad_id)
    {
        $model= EspecialidadTipoPersonal::model()->find("especialidad_id=:especialidad_id and tipo_personal_id=:tipo_personal_id",
                                                        array(
                                                               ':especialidad_id'=>$especialidad_id,
                                                               ':tipo_personal_id'=>$tipo_personal_id, 
                                                            
                                                             ) 
                                                        );
        
        if(count($model)>0)
        {
            return true;
        }
        
        return false;
        
        
    } // fin metodo loadModelEspecialidadTipoPersonal 
    
    public function loadModelFuncionTipoPersonal($tipo_personal_id,$funcion_id)
    {
        $model= FuncionTipoPersonal::model()->find("funcion_id=:funcion_id and tipo_personal_id=:tipo_personal_id",
                                                        array(
                                                               ':funcion_id'=>$funcion_id,
                                                               ':tipo_personal_id'=>$tipo_personal_id, 
                                                            
                                                             ) 
                                                        );
        if(count($model)>0)
        {
            return true;
        }
        
        return false;
        
        
    } // fin metodo loadModelFuncionTipoPersonal    
    

    public function loadModelDenominacionTipoPersonal($tipo_personal_id,$denominacion_id)
    {
        $model= DenominacionTipoPersonal::model()->find("denominacion_id=:denominacion_id and tipo_personal_id=:tipo_personal_id",
                                                        array(
                                                               ':denominacion_id'=>$denominacion_id,
                                                               ':tipo_personal_id'=>$tipo_personal_id, 
                                                            
                                                             ) 
                                                        );
        if(count($model)>0)
        {
            return true;
        }
        
        return false;
        
        
    } // fin metodo loadModelDenominacionTipoPersonal 
    
    

    
    
    
    

    
     /////////////////// FIN: SECCION DE CARGA DE LOS MODELOS //////////////////////////////  

    
    
    /**
     * Performs the AJAX validation.
     * @param PersonalPlantel $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='personal-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) 
    {
        
        
        	$id = $data["id"];
		$id=base64_encode($id);
                $estatus_proceso_registro=Yii::app()->session["estatus_proceso_registro"];
                 
                $botones = '<div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">';
                //$botones.=$columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/planteles/estructura/consulta/id/'.$id)) . '&nbsp;&nbsp;';
		
                if($estatus_proceso_registro==Constantes::ESTATUS_PROCESO_ACTIVO)
                {
                    if($data->estatus==Constantes::ESTATUS_INACTIVO)
                    {           
                        $botones.= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", "onClick" => "VentanaDialog('$id','/planteles/estructura/activarPersonalPlantel','Activar','activarPersonalPlantel')")) . '&nbsp;&nbsp;';
                    }
                    else
                    {

                        $botones.= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/planteles/estructura/edicion/id/'.$id)) . '&nbsp;&nbsp;';
                        $botones.= CHtml::link("","",array("class"=>"fa fa-trash-o red","title"=>"Inactivar", "onClick"=>"VentanaDialog('$id','/planteles/estructura/borrarPersonalPlantel','Inactivar','borrarPersonalPlantel')")).'&nbsp;&nbsp;';
                    }

                }
                
                
                $botones.="</div>";
		return $botones;

    } // fin de la funcion para mostrar los botones en la pantalla admin del modulo de registro personal-plantel
    
    public function getNacionalidad($data) 
    {
        $tdocumento_identidad = $data["tdocumento_identidad"];
        
        if($tdocumento_identidad==Constantes::NAC_VENEZOLANO)
        {
            return Constantes::DESC_NAC_VENEZOLANO;
        }
        if($tdocumento_identidad==Constantes::NAC_EXTRANJERO)
        {
            return Constantes::DESC_NAC_EXTRANJERO;
        }
        if($tdocumento_identidad==Constantes::NAC_PASAPORTE)
        {
            return Constantes::DESC_NAC_PASAPORTE;
        }
        
        return Constantes::DESC_NAC_OTRO;
        
       
    } // FIN DE LA FUNCION PARA OBTENER LA NACIONALIDAD
    
    

    
    
    

    
     public function columnaAccionesFuncionPersonal($data)
     {
		$id = $data["id"];
		$id=base64_encode($id);
                 
                $botones = '<div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">';
                $botones.= CHtml::link("","",array("class"=>"fa icon-zoom-in look-data","title"=>"Ver" ,"onClick"=>"VentanaDialog('$id','/planteles/estructura/consultaFuncionPersonal','Ver','consultaFuncionPersonal')")).'&nbsp;&nbsp;';
		if($data->estatus==Constantes::ESTATUS_INACTIVO)
                {           
                           $botones.= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", "onClick" => "VentanaDialog('$id','/planteles/estructura/activarFuncionPersonal','Activar','activarFuncionPersonal')")) . '&nbsp;&nbsp;';
                           
								
                   
                }
		else
                {
		
		$botones.= CHtml::link("","",array("class"=>"fa fa-pencil green","title"=>"Editar", "onClick"=>"VentanaDialog('$id','/planteles/estructura/edicionFuncionPersonal','Editar','edicionFuncionPersonal')")).'&nbsp;&nbsp;';
		$botones.= CHtml::link("","",array("class"=>"fa fa-trash-o red","title"=>"Inactivar", "onClick"=>"VentanaDialog('$id','/planteles/estructura/borrarFuncionPersonal','Inactivar','borrarFuncionPersonal')")).'&nbsp;&nbsp;';
                }
                
                $botones.="</div>";
		return $botones;
	} // fin de la funcion columnaAccionesFuncionPersonal

        
        
            
     public function columnaAccionesPersonalEstudio($data)
     {
		$id = $data["id"];
		$id=base64_encode($id);
                 
                $botones = '<div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">';
                $botones.= CHtml::link("","",array("class"=>"fa icon-zoom-in look-data","title"=>"Ver" ,"onClick"=>"VentanaDialogEstSup('$id','/planteles/estructura/consultaPersonalEstudio','Ver','consultaPersonalEstudio')")).'&nbsp;&nbsp;';
		if($data->estatus==Constantes::ESTATUS_INACTIVO)
                {           
                           $botones.= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", "onClick" => "VentanaDialogEstSup('$id','/planteles/estructura/activarPersonalEstudio','Activar','activarPersonalEstudio')")) . '&nbsp;&nbsp;';
                           
								
                   
                }
		else
                {
		
		$botones.= CHtml::link("","",array("class"=>"fa fa-pencil green","title"=>"Editar", "onClick"=>"VentanaDialogEstSup('$id','/planteles/estructura/edicionPersonalEstudio','Editar','edicionPersonalEstudio')")).'&nbsp;&nbsp;';
		$botones.= CHtml::link("","",array("class"=>"fa fa-trash-o red","title"=>"Inactivar", "onClick"=>"VentanaDialogEstSup('$id','/planteles/estructura/borrarPersonalEstudio','Inactivar','borrarPersonalEstudio')")).'&nbsp;&nbsp;';
                }
                
                $botones.="</div>";
		return $botones;
	} // fin de la funcion columnaAccionesPersonalEstudio
        
    
        
        
            
     public function columnaAccionesPlantelPersonalEspacios($data)
     {
		$id = $data["id"];
		$id=base64_encode($id);
                 
                $botones = '<div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">';
                $botones.= CHtml::link("","",array("class"=>"fa icon-zoom-in look-data","title"=>"Ver" ,"onClick"=>"VentanaDialog('$id','/planteles/estructura/consultaPersonalPlantelEspacios','Ver','consultaPersonalPlantelEspacios')")).'&nbsp;&nbsp;';
		if($data->estatus==Constantes::ESTATUS_INACTIVO)
                {           
                           $botones.= CHtml::link("", "", array("class" => "fa fa-check green", "title" => "Activar", "onClick" => "VentanaDialog('$id','/planteles/estructura/activarPersonalPlantelEspacios','Activar','activarPersonalPlantelEspacios')")) . '&nbsp;&nbsp;';
                           
								
                   
                }
		else
                {
		
		$botones.= CHtml::link("","",array("class"=>"fa fa-pencil green","title"=>"Editar", "onClick"=>"VentanaDialog('$id','/planteles/estructura/edicionPersonalPlantelEspacios','Editar','edicionPersonalPlantelEspacios')")).'&nbsp;&nbsp;';
		$botones.= CHtml::link("","",array("class"=>"fa fa-trash-o red","title"=>"Inactivar", "onClick"=>"VentanaDialog('$id','/planteles/estructura/borrarPersonalPlantelEspacios','Inactivar','borrarPersonalPlantelEspacios')")).'&nbsp;&nbsp;';
                }
                
                $botones.="</div>";
		return $botones;
	} // fin de la funcion columnaAccionesPlantelPersonalEspacios

    
        
        
    
    
        public function estatus($data)
        {
                $estatus = $data["estatus"];
                $columna="";

                if($estatus==Constantes::ESTATUS_INACTIVO)
                {
                        $columna=Constantes::DESC_ESTATUS_INACT;
                }
                else if($estatus==Constantes::ESTATUS_ACTIVO)
                {
                        $columna=Constantes::DESC_ESTATUS_ACT;	
                }
                return $columna;
        }
        
        
        
        public function getNombreEspacio($data)
        {  
            
                $modelPersonalPlantelEspacios= new PersonalPlantelEspacios();                
                $area_comun_plantel_id = $data["area_comun_plantel_id"];
                $resultado_nombre_espacio=$modelPersonalPlantelEspacios->getNombreEspacio($area_comun_plantel_id);
                
                if( count($resultado_nombre_espacio)  )
                {
                    
                    
                    return $resultado_nombre_espacio[0]["nombre"];
                    
                }
                else 
                {
                    return "No registrado";
                }
                
                
        }
    
    

    /**
     * Obtiene un id Decodificado si un Id es codificado en base64
     *
     * @param mixed $id
     *
     */
    public function getIdDecoded($id){
        if(is_numeric($id)){
            return $id;
        }
        else{
            $idDecodedb64 = base64_decode($id);
            if(is_numeric($idDecodedb64)){
                return $idDecodedb64;
            }
        }
        return null;
    }
}