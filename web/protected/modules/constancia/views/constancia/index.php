<?php
/* @var $this ConstanciaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Constancias',
);

$this->menu=array(
	array('label'=>'Create Constancia', 'url'=>array('create')),
	array('label'=>'Manage Constancia', 'url'=>array('admin')),
);
?>

<h1>Constancias</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
