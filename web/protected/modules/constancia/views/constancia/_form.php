<?php
 //include "phpqrcode/qrlib.php";
/* @var $this ConstanciaController */
/* @var $model Constancia */
/* @var $form CActiveForm */
?>

<?php
if(isset($error)){
$this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => "Esa cedula: $cedula no se encuentra registrada"));
}?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'constancia-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <div class="widget-box">

        <div class="widget-header">
            <h5>B&uacute;squeda de Constancia</h5>
            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>

        </div>

        <div class="widget-body">
            <div class="widget-body-inner" style="display: block;">
                <div class="widget-main">

                    <a href="#" class="search-button"></a>
                    <div style="display:block" class="search-form">
                        <div class="widget-main form">
                            <br> <br> <br>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="col-md-12" for="grupo" style="font-weight: bold">Nacionalidad <span class="required">*</span> </label>
<?php echo Chtml::dropDownList('nacionalidad', '', array('V' => 'V', 'E' => 'E'), array('style' => 'width:20%;')); ?>
                                </div>


                                <div>
                                    <div class="col-md-6">
                                        <label class="col-md-12" for="grupo" style="font-weight: bold">Tipo de Documento <span class="required">*</span> </label>
                                        <?php
                                        echo CHtml::radioButtonList('comprobar_documento', '', array('cedula' => 'cedula', 'pasaporte' => 'pasaporte'), array(
                                            'labelOptions' => array('style' => 'display:inline'), // add this code
                                            'separator' => '',
                                        ) );
                                        ?>


                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="col-md-12"><label for="cedula" style="font-weight: bold">Cedula<span id="cedula_req" class="hide"> <span class="required">*</span></span> </label></div>
<?php echo '<input type="text" disabled="disabled" data-toggle="tooltip" data-placement="bottom" placeholder="" title="Ej: V-99999999 ó E-99999999" id="cedula"  style="padding:3px 4px" maxlength="10" size="10" class="span-6" name="cedula" maxlength=10/>'; ?>
                                </div>

                                <div class="col-md-4">
                                    <div class="col-md-12"><label for="pasaporte" style="font-weight: bold">Pasaporte<span id="pasaporte_req" class="hide"> <span class="required">*</span> </span></label></div>
<?php echo '<input type="text" disabled="disabled" data-toggle="tooltip" data-placement="bottom" placeholder="" title="" id="pasaporte"  style="padding:3px 4px" maxlength="10" size="10" class="span-6" name="pasaporte" maxlength=10 />'; ?>
                                </div>
                            </div>
                            <div class="row">
                                <br>
                                <div class="col-md-5">
                                    <?php echo CHtml::button('Buscar', array('class'=>'btn btn-info btn-xs','submit' => array('constancia/buscar'))); ?>

                                </div>

                            </div>

                        </div>
                    </div>
                </div><!-- search-form -->
            </div>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->