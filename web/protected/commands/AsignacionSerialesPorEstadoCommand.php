<?php

class AsignacionSerialesPorEstadoCommand extends CConsoleCommand {
    const MODULO = "Titulo.AsignacionPorEstado";

    public function actionAsignacionPorEstado($estado_id, $username, $nombre, $apellido, $cedula, $usuario_id, $grupo_id) {
        date_default_timezone_set('America/Caracas');
        if (is_numeric($estado_id)) {

            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];
            $plantel_id = array();
            $mensajeExitoso = '';
            $planteles = Titulo::model()->planteles($estado_id, $periodo_actual_id, $a = 'N');
            $correos = Titulo::model()->obtenerCorreos($grupo_id);

            if ($planteles != false) {

                $guardarControlAsignacionAntes = Titulo::model()->guardarControlAsignacionAntes($estado_id, $periodo_actual_id);

                $transaction = Yii::app()->db->beginTransaction();

                foreach ($planteles as $key => $value) {
                    $plantel_id[] = (int) $value['id'];
                }

                $plantelesTotal = Utiles::toPgArray($plantel_id);
                $modulo = self::MODULO;
                $ip = Yii::app()->request->userHostAddress;

                $estadoPK = Estado::model()->findByPk($estado_id);
                $nombreEstado = $estadoPK['nombre'];

                try {

                    $resultadoGuardarAsignacionSerialesPorEstados = Titulo::model()->asignarSerialesPorEstados($estado_id, $periodo_actual_id, $plantelesTotal, $cedula, $nombre, $apellido, $modulo, $ip, $username, $usuario_id);
//       $transaction->commit();

                    $guardarControlAsignacionDespues = Titulo::model()->guardarControlAsignacionDespues($estado_id, $periodo_actual_id);
                    $mensajeExitoso = "Estimado Usuario, el proceso de asignación de título tardara un tiempo para realizarse. Por favor ingrese en otra oportunidad";
                    $respuesta['statusCode'] = 'success';
                    $respuesta['mensaje'] = $mensajeExitoso;
                    echo json_encode($respuesta);

                    echo "SE INICIO EL PROCESO DE ASIGNACION DE SERIALES EN EL ESTADO " . ' ' . $nombreEstado . PHP_EOL;

                    $msj = "Estimado usuario se le notifica que el proceso de asignacion de seriales para el estado $nombreEstado ha culminado con exito, ya puede ingresar al sistema gescolar y verificar los estudiantes a los que se le asígno los seriales de títulos correspondientes del MPPE.";

//                    foreach ($correos as $key => $data) {
//                        $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
//                        $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
//                        $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                    $destinatario_nombre = 'jean';
                    $destinatario_apellido = 'Barboza';
                    $remitente_correo = 'jbarboza@me.gob.ve';

                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSMTP();
                    $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de la Asignación de Seriales del MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                    $mensaje = '<img src="cid:barra" />'
                            . '<img class="pull-left"  src="cid:sintillo" height="46" />'
                            . '<img class="pull-right"  src="cid:logo" /><br><br>'
                            . '<br><br>'
                            . '<p>'
                            . '<b> ' . $msj . '</b>'
                            . '</p>';
                    $mailer->Body = $mensaje;
                    $mailer->Send();
//                    if ($mailer->Send()) {
//
//                        $generarReporte = $this->actionReportePorEstado($estado_id, $usuario_id);
//                    }
//                    else {
//                        echo $mailer->ErrorInfo;
//                    }
//   }
//      }
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();

                    $borrarControlAsignacion = Titulo::model()->borrarControlAsignacionError($estado_id, $periodo_actual_id);
//   $mensajeError = 'Estimado Usuario, ha ocurrido un error durante el proceso de asignación de título. Intente nuevamente.';
                    $respuesta['statusCode'] = 'error';
                    $respuesta['error'] = $ex;
                    $error = $ex->getMessage();
                    $respuesta['mensaje'] = $error;
                    echo json_encode($respuesta);

                    echo "Ocurrio un error en el proceso de asignacion de seriales en el estado" . ' ' . $nombreEstado . PHP_EOL;

                    $mensaje_error = $error;

                    $destinatario_nombre = 'Gescolar';
                    $destinatario_apellido = 'Gescolar';
                    $remitente_correo = 'soporte_gescolar@me.gob.ve';
//soporte_gescolar@me.gob.ve
                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSMTP();
                    $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Error en la Asignación de Seriales del MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                    $mensaje = '<img src="cid:barra" />'
                            . '<img class="pull-left"  src="cid:sintillo" height="46" />'
                            . '<img class="pull-right"  src="cid:logo" /><br><br>'
                            . '<br><br>'
                            . '<p>'
                            . '<b> ' . $mensaje_error . '</b>'
                            . '</p>';
                    $mailer->Body = $mensaje;
                    if ($mailer->Send()) {

                        $mensaje_error = "Estimado usuario se le notifica que el proceso de asignacion de seriales para el estado $nombreEstado no se puedo culminar con exito, ya que ocurrio un error en el proceso, por favor notifique este inconveniente al departamento de sistema del MPPE, para que obtenga una solución.";

//                        foreach ($correos as $key => $data) {
//                            $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
//                            $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
//                            $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                        $destinatario_nombre = 'Marisela';
                        $destinatario_apellido = 'La Cruz';
                        $remitente_correo = 'mari.lac.mor@gmail.com';

                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSMTP();
                        $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de la Asignación de Seriales del MPPE';
                        $mailer->IsHTML(true);
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                        $mensaje = '<img src="cid:barra" />'
                                . '<img class="pull-left"  src="cid:sintillo" height="46" />'
                                . '<img class="pull-right"  src="cid:logo" /><br><br>'
                                . '<br><br>'
                                . '<p>'
                                . '<b> ' . $mensaje_error . '</b>'
                                . '</p>';
                        $mailer->Body = $mensaje;
                        $mailer->Send();
//      }
                    }
                }
            }
        } else
            echo "\n " . "EL ESTADO_ID :" . $estado_id . " NO ES UN NUMERO \n";
    }

    public function actionReportePorEstado($estado_id, $username, $usuario_id, $grupo_id) {
        date_default_timezone_set('America/Caracas');
//        $estado_id = $this->getRequest('estado');
//        $estado_id_decoded = base64_decode($estado_id);

        if (is_numeric($estado_id)) {

            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];
            $zona_educativa = Titulo::model()->obtenerZonaEducativa($estado_id);
            $datosReporte = Titulo::model()->obtenerDatosReporteSerialesPorEstado($estado_id, $periodo_actual_id, $zona_educativa);
            $estadoPK = Estado::model()->findByPk($estado_id);
            $nombreEstado = $estadoPK['nombre'];
            $fecha = date('Y');
            $modulo = "Titulo.ReportePorEstado";
            $ip = Yii::app()->request->userHostAddress;
            $correos = Titulo::model()->obtenerCorreos($grupo_id);
            echo "SE INICIO EL PROCESO DE GENERACION DEL REPORTE EN EL FORMATO PDF DEL ESTADO" . ' ' . $nombreEstado . PHP_EOL;

            $nombrePdf = 'Distribucion_Seriales_' . $nombreEstado . '_' . $fecha . '.pdf';

            $verificarRegistroPdf = Titulo::model()->verificarExistenciaDeRegistroPDF($estado_id, $periodo_actual_id, $nombrePdf);
            if ($verificarRegistroPdf == 0) {
                $guardarRegistroPdfEstadoAntes = Titulo::model()->guardarRegistroPdfEstadoAntes($estado_id, $periodo_actual_id, $nombrePdf);
            } else {
                $guardarRegistroPdfEstadoDespues = Titulo::model()->guardarRegistroPdfEstadoDespues($estado_id, $periodo_actual_id, $nombrePdf, $a = 'S');
            }
            $ruta = yii::app()->basePath . '/../public/serial_titulo/';
            $nombre_pdf = $ruta . 'Distribucion_Seriales_' . $nombreEstado . '_' . $fecha; //(isset($datosPlantel['cod_plantel']) AND $datosPlantel['cod_plantel'] != '') ? $datosPlantel['cod_plantel'] . '-Matricula' : '-Matricula';
            $mpdf = new mpdf('', 'LEGAL', 0, '', 15, 15, 37, 50);            //$mpdf->SetMargins(3,69.1,70);
            $header = $this->renderPartial('/modules/titulo/views/asignacionSerialesEstados/_headerReporteEstudiantesConSerialesPorEstado', array('nombreEstado' => $nombreEstado), true);
//  $footer = $this->renderPartial('_footerReporteEstudiantesConSeriales', array('datosAutoridades' => $datos_autoridades), true);
            $body = $this->renderPartial('/modules/titulo/views/asignacionSerialesEstados/_bodyReporteEstudiantesConSerialesPorEstado', array('datosReporte' => $datosReporte), true);

            $mpdf->SetFont('sans-serif');
            $mpdf->SetHTMLHeader($header);
//     $mpdf->setHTMLFooter($footer . '<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($body);
//  $this->registerLog('LECTURA', self::MODULO . 'Reporte', 'EXITOSO', 'Entró matricular la Seccion Plantel' . $seccion_plantel_id);
            $mpdf->Output($nombre_pdf . '.pdf', 'F');

            $existe_nombre_pdf = $ruta . 'Distribucion_Seriales_' . $nombreEstado . '_' . $fecha . '.pdf';


            if (file_exists($existe_nombre_pdf)) {

                $resultadoGuardarReporte = Titulo::model()->guardarReporteSerialesEstado($usuario_id, $nombrePdf, $estado_id);

                if ($resultadoGuardarReporte == 1) {

                    $guardarRegistroPdfEstadoDespues = Titulo::model()->guardarRegistroPdfEstadoDespues($estado_id, $periodo_actual_id, $nombrePdf, $a = 'N');


                    //                    foreach ($correos as $key => $data) {
                    //                        $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
                    //                        $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
                    //                        $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;

                    $destinatario_nombre = 'Gestión';
                    $destinatario_apellido = 'Escolar';
                    $remitente_correo = 'mari.lac.mor@gmail.com';

                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSMTP();
                    $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                    //           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Generación satisfactoria del reporte de Seriales del MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                    $mensaje = '<img src="cid:barra" />'
                            . '<img class="pull-left"  src="cid:sintillo" height="46" />'
                            . '<img class="pull-right"  src="cid:logo" /><br><br>'
                            . '<br><br>'
                            . '<p>'
                            . '<b>Se realizó satisfactoriamente el reporte del estado ' . $nombreEstado . ' ya puede ingresar en el sistema para descargar el reporte.</b>'
                            . '</p>';
                    $mailer->Body = $mensaje;
                    if ($mailer->Send()) {

                        $this->registerLog('REPORTES', $modulo . 'PDF', 'EXITOSO', 'Generación del pdf con el nombre: ' . $nombrePdf, $ip, $usuario_id, $username);

                        echo "GUARDO EL REPORTE EN EL FORMATO PDF DEL ESTADO" . ' ' . $nombreEstado . ' EN LA TABLA reporte_seriales_estado' . PHP_EOL;
                    }
                    //      }
                } else {

                    $borrarRegistroPdfEstadoError = Titulo::model()->borrarRegistroPdfEstadoError($estado_id, $periodo_actual_id, $nombrePdf);

                    echo "ERROR NO GUARDO EL NOMBRE DEL PDF EN LA TABLA titulo.reporte_seriales_estado " . PHP_EOL;

                    $destinatario_nombre = 'Gescolar';
                    $destinatario_apellido = 'Gescolar';
                    $remitente_correo = 'mari.lac.mor@gmail.com';

                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSMTP();
                    $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                    $mailer->FromName = 'Gescolar ';
                    $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                    //           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Notificación de Error en la Generación del reporte de Seriales del MPPE';
                    $mailer->IsHTML(true);
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                    $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                    $mensaje = '<img src="cid:barra" />'
                            . '<img class="pull-left"  src="cid:sintillo" height="46" />'
                            . '<img class="pull-right"  src="cid:logo" /><br><br>'
                            . '<br><br>'
                            . '<p>'
                            . '<b>Notificación de error en el proceso de generación del reporte en pdf del estado: ' . '  ' . $nombreEstado . ' Por favor comuniquese con el personal de soporte del sistema gescolar, para obtener una solución' . '</b>'
                            . '</p>';
                    $mailer->Body = $mensaje;
                    if ($mailer->Send()) {
                        $destinatario_nombre = 'Gescolar';
                        $destinatario_apellido = 'Gescolar';
                        $remitente_correo = 'mari.lac.mor@gmail.com';

                        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $mailer->Host = 'mail.me.gob.ve:25';
                        $mailer->IsSMTP();
                        $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                        $mailer->FromName = 'Gescolar ';
                        $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                        //           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                        $mailer->CharSet = 'UTF-8';
                        $mailer->Subject = 'Notificación de Error en la Generación del reporte de Seriales del MPPE';
                        $mailer->IsHTML(true);
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                        $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                        $mensaje = '<img src="cid:barra" />'
                                . '<img class="pull-left"  src="cid:sintillo" height="46" />'
                                . '<img class="pull-right"  src="cid:logo" /><br><br>'
                                . '<br><br>'
                                . '<p>'
                                . '<b>Notificación de error en el proceso de insertar en la tabla titulo.reporte_seriales_estado, con el nombre del pdf de asignación de seriales en el estado' . '  ' . $nombreEstado . '</b>'
                                . '</p>';
                        $mailer->Body = $mensaje;
                        $mailer->Send();
                    }
                }
            } else {

                $borrarRegistroPdfEstadoError = Titulo::model()->borrarRegistroPdfEstadoError($estado_id, $periodo_actual_id, $nombrePdf);

                echo "ERROR NO EXISTE EL REPORTE EN EL FORMATO PDF DEL ESTADO" . ' ' . $nombreEstado . ' CON EL NOMBRE DE ' . $nombrePdf . PHP_EOL;

                $destinatario_nombre = 'Gescolar';
                $destinatario_apellido = 'Gescolar';
                $remitente_correo = 'mari.lac.mor@gmail.com';

                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $mailer->Host = 'mail.me.gob.ve:25';
                $mailer->IsSMTP();
                $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                $mailer->FromName = 'Gescolar ';
                $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                $mailer->CharSet = 'UTF-8';
                $mailer->Subject = 'Notificación de Error en la Generación del reporte de Seriales del MPPE';
                $mailer->IsHTML(true);
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                $mensaje = '<img src="cid:barra" />'
                        . '<img class="pull-left"  src="cid:sintillo" height="46" />'
                        . '<img class="pull-right"  src="cid:logo" /><br><br>'
                        . '<br><br>'
                        . '<p>'
                        . '<b>Notificación de error en el proceso de generación del pdf de asignación de seriales en el estado' . '  ' . $nombreEstado . '</b>'
                        . '</p>';
                $mailer->Body = $mensaje;
                $mailer->Send();
            }
        } else {
            echo "\n " . "EL ESTADO_ID :" . $estado_id . " NO ES UN NUMERO \n";

            $msj = 'Notificación de error en el proceso de generación del pdf de asignación de seriales ya que el estado no es un número ha sido alterado el id que ha recibido es:' . ' ' . $estado_id;
            $destinatario_nombre = 'Gescolar';
            $destinatario_apellido = 'Gescolar';
            $remitente_correo = 'mari.lac.mor@gmail.com';

            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
            $mailer->Host = 'mail.me.gob.ve:25';
            $mailer->IsSMTP();
            $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
            $mailer->FromName = 'Gescolar ';
            $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
//           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
            $mailer->CharSet = 'UTF-8';
            $mailer->Subject = 'Notificación de Error en la Generación del reporte de Seriales del MPPE';
            $mailer->IsHTML(true);
            $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
            $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
            $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
            $mensaje = '<img src="cid:barra" />'
                    . '<img class="pull-left"  src="cid:sintillo" height="46" />'
                    . '<img class="pull-right"  src="cid:logo" /><br><br>'
                    . '<br><br>'
                    . '<p>'
                    . '<b> ' . $msj . '</b>'
                    . '</p>';
            $mailer->Body = $mensaje;
            if ($mailer->Send()) {

                $mensaje_error = "Estimado usuario se le notifica que el proceso de generación del pdf correspondiente, no se puedo generar ya que los datos fueron alterados, por favor notificar este incoveniente al departamento de sistema del MPPE para que obtenga una solución.";

                //                        foreach ($correos as $key => $data) {
                //                            $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
                //                            $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
                //                            $remitente_correo = (isset($data['correo'])) ? $data['correo'] : null;
                $destinatario_nombre = 'Marisela';
                $destinatario_apellido = 'La Cruz';
                $remitente_correo = 'mari.lac.mor@gmail.com';

                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $mailer->Host = 'mail.me.gob.ve:25';
                $mailer->IsSMTP();
                $mailer->From = 'soporte_gescolar@me.gob.ve'; //Es quien lo envia
                $mailer->FromName = 'Gescolar ';
                $mailer->AddAddress($remitente_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                //           $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');//Para hacer una copia oculta
                $mailer->CharSet = 'UTF-8';
                $mailer->Subject = 'Notificación de Error en la Asignación de seriales del MPPE';
                $mailer->IsHTML(true);
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/barra_n.png', 'barra', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/sintillo.png', 'sintillo', 'barra_n.png');
                $mailer->AddEmbeddedImage(yii::app()->basePath . '/../public/images/logo_sistema.png', 'logo', 'barra_n.png');
                $mensaje = '<img src="cid:barra" />'
                        . '<img class="pull-left"  src="cid:sintillo" height="46" />'
                        . '<img class="pull-right"  src="cid:logo" /><br><br>'
                        . '<br><br>'
                        . '<p>'
                        . '<b> ' . $mensaje_error . '</b>'
                        . '</p>';
                $mailer->Body = $mensaje;
                $mailer->Send();
            }
        }
    }

    public function getViewPath() {
        return Yii::app()->getBasePath() . DIRECTORY_SEPARATOR;
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath() . $viewName . '.php';
    }

    /**
     * Modeified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }

}
