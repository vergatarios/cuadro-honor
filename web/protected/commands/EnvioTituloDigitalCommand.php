<?php
/**
 * Created by PhpStorm.
 * User: nelson
 * Date: 16/01/15
 * Time: 11:12 AM
 */

class EnvioTituloDigitalCommand extends CConsoleCommand
{
    const MODULO = "Estudiante.EnvioTituloDigitalCommand";

    private static $testing = true;

    private $module = 'estudiante';

    private $cacheIndex;

    public function actionEnvio() {

        echo Yii::app()->params['adminEmailSend']."\n";

        try {

            $fechaInicio = date('Y-m-d H:i:s');
            echo "\n----------------------------------------------------------------------------------------------\n";
            echo "\n---------------------------------------I  N  I C  I  O----------------------------------------\n";
            echo "\n----------------------------------------------------------------------------------------------\n";
            echo "$fechaInicio: INICIO DEL PROCESO DE ENVÍO DE CORREO DEL LA SOLICITUD DE TITULO DIGITAL. \n";
            //cache
            $this->cacheIndex = 'GESCOLARMAILTITULODIG:'.date('Ymd');

            //$cantidadVecesSinEmailPorEnviar = Yii::app()->cache->get($this->cacheIndex);

            //Yii::app()->cache->delete($this->cacheIndex);

            $correoSinEnviar=TituloDigital::listadoEmailActivos();

            if(is_array($correoSinEnviar) && count($correoSinEnviar)>0){
                echo "LA CANTIDAD DE TITULOS  SIN ENVIAR SON <<".count($correoSinEnviar).">>\n";
            }

            if(is_array($correoSinEnviar) && count($correoSinEnviar)>0){

                self::$testing = Yii::app()->params['testing'];
                if(!self::$testing){


                    $mailHost = Yii::app()->params['mailServer'];
                    //
                    $mailPort = Yii::app()->params['portMailServer'];
                    echo "\n----------------------------------------------------------------------------------------------\n";
                    echo("|                    Se enviará desde el Servidor <<$mailHost:$mailPort>>.                     |\n");

                    $contador = 1;

                    foreach ($correoSinEnviar as $tituloDigital) {
                        echo "----------------------------------------------------------------------------------------------\n";
                        if($contador>=100){
                            sleep(90);
                            /**
                             * OJO: CAMBIAR la direccion es "DELICADO", ya que dentro se borran todos los ".pdf"
                             */
                            //$directorioPdf=Yii::app()->basePath."/../public/downloads/tituloDigital";
                            ////----DESCOMENTA CUANDO DESEES LIMPIAR LOS PDF
                            //$this->depurarCarpeta($directorioPdf);
                            $contador = 1;
                        }
                        $direcionTituloDigital='http://escolar.dev/public/downloads/tituloDigital';


                        $contenido = " <div align='center'>
                                <br>Se le notifica que su Titulo Digital se a generado generado satisfactoriamente,
                                <br>ingrese a <a href=".$direcionTituloDigital.">Este link</a> para Visualisar y Descargar.
                                <br>link: <a href=".$direcionTituloDigital.">Gescolar App</a>
                             </div>";
                        $correDestinatario = (isset($tituloDigital['correo_estudiante'])) ? strtolower($tituloDigital['correo_estudiante']) : null;
                        $estudiante=(isset($tituloDigital['nombre_estudiante'])) ? 'Estudiante: '.$tituloDigital['nombre_estudiante'] : null;
                        $desde = Yii::app()->params['adminEmailSend'];

                        $archivo=$this->actionVisual($tituloDigital);

                        $result = $this->enviarCorreo($correDestinatario, $estudiante, 'GESCOLAR - Gestion Escolar | Titulo de Digital', $contenido, $desde, 'GESCOLAR - Gestion Escolar',$archivo['urlPdf']);

                        if($result){
                            $actualizacion =$this->actualizarEstatus(true,$tituloDigital);
                            if($actualizacion){
                                echo "\n".date('Y-m-d H:i:s').': ENVIO EXITOSO ';
                                echo "\n----------------------------------------------------------------------------------------------";
                                echo "\n|         EXITOSO EN LA ACTUALIZACION DE ESTATUS DE LA SOLICITUD DE TITULO DIGITAL .         |";
                                echo "\n----------------------------------------------------------------------------------------------\n\n";
                            }
                            else{
                                echo "\n---------------------------------------------------------------------------------------------------------";
                                echo "\n|".date('Y-m-d H:i:s').": ERROR EN LA ACTUALIZACION DE BDs - ERROR AL REALIZAR LA ACTUALIZACION DEL ESTATUS |\n|            EL ENVIO DE LA SOLICITUD DE TITULO DIGITAL FUE REALIZADO.            |\n";
                                echo "\n---------------------------------------------------------------------------------------------------------\n\n\n\n";
                            }
                        }
                        else{
                            $this->actualizarEstatus(false,$tituloDigital);
                            echo "\n".date('Y-m-d H:i:s').': ENVIO FALLIDO - '.json_encode($tituloDigital).".\n";

                        }

                        /*                        }
                                                else{
                                                    echo date('Y-m-d H:i:s').': ENVIO FALLIDO - NO EXISTE EL ARCHIVO ADJUNTO - '.json_encode($tituloDigital)."\n";
                                                }*/

                        $contador = $contador + 1;

                    }
                    echo "\n\n\n\n\n----------------------------------------------------------------------------------------------";
                    echo "\n| ".date('Y-m-d H:i:s').": FIN DEL PROCESO DE ENVÍO DE CORREO DE LA SOLICITUD DE TITULO DIGITAL. |";
                    echo "\n----------------------------------------------------------------------------------------------\n\n\n\n";
                    /**
                     * OJO: CAMBIAR la direccion es "DELICADO", ya que dentro se borran todos los ".pdf"
                     */
                    //$directorioPdf=Yii::app()->basePath."/../public/downloads/tituloDigital";
                    ////----DESCOMENTA CUANDO DESEES LIMPIAR LOS PDF
                    //$this->depurarCarpeta($directorioPdf);
                }

            }
            else{
                /*
                if($cantidadVecesSinEmailPorEnviar>13){
                    echo date('Y-m-d H:i:s').": POR EL DÍA DE HOY SE EXCEDIÓ EL NUMERO DE VECES QUE NO EXISTEN CORREOS CON COMPROBANTE CNAE QUE ENVIAR.\n";
                }
                else{
                */

                echo "\n".date('Y-m-d H:i:s').": NO EXISTEN SOLICITUD DE TITULO DIGITAL QUE ENVIAR.\n\n\n";
                //}
                /*
                if(!$cantidadVecesSinEmailPorEnviar){
                    $cantidadVecesSinEmailPorEnviar = 1;
                    Yii::app()->cache->set($this->cacheIndex, $cantidadVecesSinEmailPorEnviar, 43200);
                }else{
                    $cantidadVecesSinEmailPorEnviar = $cantidadVecesSinEmailPorEnviar + 1;
                    Yii::app()->cache->set($this->cacheIndex, $cantidadVecesSinEmailPorEnviar, 43200);
                }
                */
            }
        } catch (Exception $ex) {
            $respuesta['statusCode'] = 'error';
            $respuesta['error'] = $ex->getMessage();
            $respuesta['mensaje'] = "HA OCURRIDO UN ERROR DURANTE EL PROCESO DE ENVÍO DE CORREO DE LA SOLICITUD TITULO DIGITAL. {$respuesta['error']}.";
//            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
//            $mailer->Host = 'mail.me.gob.ve:25';
//            $mailer->IsSMTP();
//            $mailer->From = Yii::app()->params['adminEmailSend']; //Es quien lo envia
//            $mailer->FromName = 'Sistema de Gestión Escolar';
//            $mailer->AddAddress(Yii::app()->params['adminEmailSend'], 'Equipo de Soporte del Sistema de Gestión Escolar');
//            $mailer->CharSet = 'UTF-8';
//            $mailer->Subject = 'Notificación de Registro Programado de Seriales del MPPPE';
//            $mailer->Body = $respuesta['mensaje'];
//            $mailer->Send();
            echo "\n".date('Y-m-d H:i:s').": ERROR - ".$respuesta['mensaje'].'. Linea: Nro. '.$ex->getLine().".\n";
            echo "\n".date('Y-m-d H:i:s').": FIN DEL PROCESO DE ENVÍO DE CORREO DE LA SOLICITUD TITULO DIGITAL - CON ERROR.\n\n\n\n\n\n";
        }
    }






    public function getViewPath($module='') {
        $modulePath = '';
        if(strlen($module)>0){
            $modulePath = '/modules/'.$module;
        }
        return Yii::app()->getBasePath() . $modulePath . DIRECTORY_SEPARATOR . 'views';
    }

    /**
     *
     * @param type $to
     * @param type $subject
     * @param type $msj
     * @param type $from
     * @param type $from_name
     * @param type $archivo
     * @param type $nombre_archivo
     * @return type
     */
    static public function enviarCorreo($to, $to_name, $subject = 'SIR-SWL', $msj = '', $from = '', $from_name = '',$archivo=null,$nombre_archivo=null) {
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        //$mailer->Host = 'mail.me.gob.ve:25';
        $mailer->Host = Yii::app()->params['mailServer'].':'.Yii::app()->params['portMailServer'];
        $mailer->SMTPDebug  = 2;
        $mailer->IsSMTP();

        // $mailer->SMTPAuth   = true;                  // enable SMTP authentication
        // $mailer->SMTPSecure = "tls";                 // sets the prefix to the servier
        // $mailer->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        // $mailer->Port       = 587;                   // set the SMTP port for the GMAIL server

        // //$mailer->Username   = "soporte_gopae_cnae@gmail.com";  // GMAIL username
        // $mailer->Username   = "gescolar.mppe@gmail.com";  // GMAIL username
        // $mailer->Password   = "gescolarpro";            // GMAIL password

        if (is_array($to)) {
            foreach ($to as $sendTo) {
                $mailer->AddAddress($sendTo);
            }
        } else {
            echo "\n".'Email to: '.$to.'. Name To: '.$to_name."\n";
            $mailer->AddAddress($to, $to_name);
        }

        if (isset($from) and $from != '' and $from != null)
            $mailer->From = $from;
        else
            $mailer->From = Yii::app()->params->adminEmail;
        if (isset($from_name) and $from_name != '' and $from_name != null)
            $mailer->FromName = $from_name;
        else
            $mailer->FromName = Yii::app()->params->adminName;
        if($archivo){
            $mailer->AddBCC('soporte_gescolar@me.gob.ve');
            //$mailer->AddBCC('jarojasm@me.gob.ve');
            //$mailer->AddBCC(Yii::app()->params['adminGmail']);
            $mailer->AddAttachment($archivo);
        }

        $mailer->Username = 'soporte_gescolar';
        $mailer->CharSet = 'UTF-8';
        $mailer->Subject = $subject;
        $mailer->MsgHTML($msj);
        return $mailer->Send();

    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath($this->module) . $viewName . '.php';
    }

    /**
     * Modeified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }

    public function actualizarEstatus($operacion,$tituloDigital){
        if($operacion){
            $tituloDigital->estatus='D';
            $tituloDigital->envios_fallidos=0;
        }else{
            $tituloDigital->envios_fallidos=$tituloDigital->envios_fallidos+1;
            if($tituloDigital->envios_fallidos>=10){
                $tituloDigital->estatus='F';
            }
        }
        $resultado= $tituloDigital->save();
        return $resultado;
    }
    public function actionVisual($modelDigitalTitulo) {

        Yii::import('ext.qrcode.QRCode');

        $meses = Utiles::$MESES_STR;
        $mpdf = Yii::app()->ePdf->mpdf('', 'A4-L',0, '', 15, 15, 16, 9, 9, 9, 'L');
        $diaArchivo = date("d-m-Y");


        try{
            $codigo = $modelDigitalTitulo->codigo_verificacion;

            /**
             * ----------------QR----------------------------------------------
             */
            $url = Yii::app()->params['webUrl']."/verificacion/tituloDigital/verificar/codigo/$codigo";
            $qrGenerator = new QRCode($url);
            $directorioQR=Yii::app()->basePath."/../public/downloads/qr/tituloDigital";
            $command = 'sudo chmod 777 -R '.$directorioQR;
            exec($command);
            $srcQr = $directorioQR."/$codigo.png";
            $qrGenerator->create($srcQr);
            $command = 'sudo chmod 777 -R '.$directorioQR;
            exec($command);
            /**
             * --------------------------------------------------------------
             */

            $fechaNacimiento = explode('-', $modelDigitalTitulo->fecha_nacimiento);
            $fechaOtorgamiento = explode('-', $modelDigitalTitulo->fecha_expedicion);
            $monthName = strtoupper($meses[(int) $modelDigitalTitulo->fecha_nacimiento[1] - 1]);
            //no me trae la fecha de otorgamiento en la no legacy
            $mesNombreExpendicion = strtoupper($meses[(int) $fechaOtorgamiento[1] - 1]);

            $EstuadianteEgresado=$modelDigitalTitulo->cedula_estudiante;

            $modelDigitalTitulo->nacido_en=ucwords(Utiles::strtolower_utf8($modelDigitalTitulo->nacido_en));

            //// Obtengo las vistas que escribire en el pdf.
            $header = $this->renderPartial('/generarTituloDigital/_headerReporteDigitalTitulo', array('resultadoBusqueda' => $modelDigitalTitulo, 'fechaNacimiento' => $fechaNacimiento, 'monthName' => $monthName, 'mesNombreExpendicion' => $mesNombreExpendicion, 'fechaOtorgamiento' => $fechaOtorgamiento, 'stringMd5' => $codigo), true);
            $body = $this->renderPartial('/generarTituloDigital/_bodyReporteDigitalTitulo', array('resultadoBusqueda' => $modelDigitalTitulo, 'fechaNacimiento' => $fechaNacimiento, 'monthName' => $monthName, 'mesNombreExpendicion' => $mesNombreExpendicion, 'fechaOtorgamiento' => $fechaOtorgamiento, 'stringMd5' => $codigo), true);
            $footer = $this->renderPartial('/generarTituloDigital/_footerReporteDigitalTitulo', array('stringMd5' => $codigo, 'resultadoBusqueda' => $modelDigitalTitulo,'srcQr'=>$srcQr), true);

            //// Escribo las tres partes del pdf.
            $mpdf->SetHTMLHeader($header);
            $mpdf->WriteHTML($body);
            $mpdf->setHTMLFooter($footer);
            //// Obtengo la Url junto con el nombre de archivo del PDF
            $urlPdf=Yii::app()->params['webDirectoryPath'].Yii::app()->params['urlDownloadSolicitudTituloDigital'].'/'.$EstuadianteEgresado.$diaArchivo.'.pdf';
            $urlPdf = str_replace('//','/',$urlPdf);
            $resultado=$mpdf->Output($urlPdf, 'F');
            if ($resultado){
                echo "\n No se Creo el PDF: [-".$resultado."-] \n";
            }

        }
        catch (Exception $ex) {
            echo "\n-------------------------------------------\n";
            $respuesta['statusCode'] = 'error';
            $respuesta['error'] = $ex->getMessage();
            $respuesta['mensaje'] = "HA OCURRIDO UN ERROR DURANTE EL PROCESO DE ENVÍO DE CORREO DE LA SOLICITUD TITULO DIGITAL. {$respuesta['error']}.\n";
//            $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
//            $mailer->Host = 'mail.me.gob.ve:25';
//            $mailer->IsSMTP();
//            $mailer->From = Yii::app()->params['adminEmailSend']; //Es quien lo envia
//            $mailer->FromName = 'Sistema de Gestión Escolar';
//            $mailer->AddAddress(Yii::app()->params['adminEmailSend'], 'Equipo de Soporte del Sistema de Gestión Escolar');
//            $mailer->CharSet = 'UTF-8';
//            $mailer->Subject = 'Notificación de Registro Programado de Seriales del MPPPE';
//            $mailer->Body = $respuesta['mensaje'];
//            $mailer->Send();
            echo "\n".date('Y-m-d H:i:s').": ERROR - ".$respuesta['mensaje'].'. Linea: Nro. '.$ex->getLine().".\n";
            echo "\n".date('Y-m-d H:i:s').": FIN DEL PROCESO DE ENVÍO DE CORREO DE LA SOLICITUD TITULO DIGITAL - CON ERROR.\n\n\n";
            echo "\n-------------------------------------------\n";

        }
        $resultado= array('resultado'=>$resultado,'urlPdf'=>$urlPdf);
        return $resultado ;
    }

    public function depurarCarpeta($url){
        $url= str_replace('//', '/', $url.'/*.pdf');
        $command = 'sudo rm -rf '.$url;
        exec($command);
    }

    public function run(){
        $this->actionEnvio();
    }
}