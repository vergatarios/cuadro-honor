<?php

class ProcesarFormularioInscripcionCommand extends CConsoleCommand {
    public $archivo_correo ='';
    public $fila =array();
    public $formulario_id ='';
    public $archivos='';
    CONST MODULO='ProcesarFormularioInscripcionCommand';
    public function actionIndex() {
        date_default_timezone_set('America/Caracas');
        $directorio = realpath(Yii::app()->basePath.'/..');
        $directorio_base = $directorio.'/public/uploads/Procesados/';
        $modelFormulario = new FormularioInscripcionPeriodo();
        $existe_proceso_activo = $modelFormulario->existeProcesoActivo();

        if($existe_proceso_activo==0){
            while($proceso_pendiente = $modelFormulario->buscarFormularioPendiente()){
                sleep(40);
                $existe_proceso_activo = 0;
                $esInicialPrimaria = 0;
                $periodo_id=null;
                $seccion_plantel_id=null;
                $plantel_id=null;
                $operacion = false;
                $result = new stdClass();
                $estudiantes=array();
                $log='';
                $id=null;
                $estudiante_errores=array();
                $modelEstudiante= '';

                echo "PROCESANDO ARCHIVO ".$proceso_pendiente['archivo'].PHP_EOL;

                if(file_exists($this->archivos)){
                    unlink($this->archivos);
                }
                //$proceso_pendiente = $modelFormulario->buscarFormularioPendiente();
                if($proceso_pendiente){
                    $datosSeccion= SeccionPlantel::model()->obtenerDatosSeccion($seccion_plantel_id, $plantel_id);
                    $this->formulario_id=$proceso_pendiente['id'];
                    $seccion_plantel_id=$proceso_pendiente['seccion_plantel_id'];
                    $plantel_id=$proceso_pendiente['plantel_id'];
                    $periodo_id=$proceso_pendiente['periodo_id'];
                    $this->archivo_correo=$archivo=$proceso_pendiente['archivo'];
                    $directorio_archivo=$directorio_base.$archivo;
                    $result = new stdClass();
                    $readDataOnly = true;
                    $excelReader = new ExcelReader($directorio_archivo);
                    $result->archivo = $archivo;
                    $operacion = false;
                    try {
                        list($operacion,
                            $result->class_style,
                            $result->message,
                            $objReaderExcel,
                            $objReader) = $excelReader->getReader($readDataOnly);
                    } catch (Exception $e){
                        echo $this->registrarTrazaReporte('LECTURA','NO SE PUDO ABRIR EL ARCHIVO');
                        echo "NO SE PUEDO ABRIR EL ARCHIVO ".$proceso_pendiente['archivo'].PHP_EOL;
                        FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'Z');
                    }

                    //Verifico que las extensiones del archivo sean las requeridas
                    if($operacion){
                        $sheetData = $objReader->getActiveSheet()->toArray(null,true,true,true);
                        $i = 2; //$i sirve para recorrer el excel
                        $j = $i - 2; // $j sirve para crear el arreglo de la respuesta

                        /*
                         *      ESTRUCTURA ARCHIVO
                         * [A] => Origen Del Estudiante -> requerido
                         * [B] => Identificación Del Estudiante -> requerido
                         * [C] => Nombres Del Estudiante -> requerido solo si es nuevo ingreso
                         * [D] => Apellidos Del Estudiante -> requerido solo si es nuevo ingreso
                         * [E] => Cédula Del Representante -> requerido solo si es nuevo ingreso
                         * [F] => Teléfono del Representante
                         * [G] => Fecha_Nacimiento -> requerido
                         * [H] => Sexo -> requerido
                         * [I] => Afinidad -> requerido solo si es nuevo ingreso
                         * [J] => Orden_Nacimiento -> requerido solo si es nuevo ingreso
                         */

                        $log='========================================VALIDANDO ESTRUCTURA DE DATOS========================================'.PHP_EOL;

                        while (array_key_exists($i, $sheetData)) {
                            $c= count($estudiantes);
                            $valido = true;
                            $this->fila=$sheetData[$i];
                            if(trim($sheetData[$i]['A']) != '' AND trim($sheetData[$i]['B']) != '' AND trim($sheetData[$i]['C']) != '' AND trim($sheetData[$i]['D']) != ''
                                AND trim($sheetData[$i]['E']) != ''  AND trim($sheetData[$i]['G']) != '' AND trim($sheetData[$i]['H']) != '' AND trim($sheetData[$i]['I']) != ''
                            ){

                                /*
                                 * VALIDANDO ORIGEN E IDENTIFICACION
                                 */
                                if(in_array($sheetData[$i]['A'],array('V','E','P','C'))){
                                    if(in_array($sheetData[$i]['A'],array('V','E','C'))){
                                        if(!is_numeric($sheetData[$i]['B'])){
                                            $valido=false;
                                            $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'La identificación del estudiante no corresponde segun el origen.');
                                            $log.='Fila Nº '.$i.' | La identificación del estudiante no corresponde segun el origen'.PHP_EOL;
                                            $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | La identificación del estudiante no corresponde segun el origen');
                                        }
                                        else {

                                            if(in_array($sheetData[$i]['A'],array('V','E'))){
                                                if(strlen((string)$sheetData[$i]['B'])!=8){
                                                    $valido=false;
                                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'La identificación del estudiante debe poseer 8 digitos.');
                                                    $log.='Fila Nº '.$i.' | La identificación del estudiante debe poseer 8 digitos cuando el origen seleccionado es V ó E.'.PHP_EOL;
                                                    $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | La identificación del estudiante debe poseer 8 digitos cuando el origen seleccionado es V ó E');

                                                }
                                            }
                                            else {
                                                if(!strlen($sheetData[$i]['B'])==11){
                                                    $valido=false;
                                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'La identificación del estudiante debe poseer 11 digitos.');
                                                    $log.='Fila Nº '.$i.' | La identificación del estudiante debe poseer 11 digitos cuando el origen seleccionado es C'.PHP_EOL;
                                                    $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | La identificación del estudiante debe poseer 11 digitos cuando el origen seleccionado es C');


                                                }
                                            }
                                        }
                                    }
                                }
                                else {
                                    $valido=false;
                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'Origen incorrecto, valores permitidos (V,E,P,C)');
                                    $log.='Fila Nº '.$i.' | Origen incorrecto, valores permitidos (V,E,P,C)'.PHP_EOL;
                                    $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | Origen incorrecto, valores permitidos (V,E,P,C)');

                                }

                                /*
                                 * VALIDANDO NOMBRES Y APELLIDOS
                                 */
                                if(strlen($sheetData[$i]['C'])<3){
                                    $valido=false;
                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'El Campo Nombres del Estudiante debe poseer al menos 3 caracteres');
                                    $log.='Fila Nº '.$i.' | El Campo Nombres del Estudiante debe poseer al menos 3 caracteres'.PHP_EOL;
                                    $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | El Campo Nombres del Estudiante debe poseer al menos 3 caracteres');

                                }
                                if(strlen($sheetData[$i]['D'])<3){
                                    $valido=false;
                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'El Campo Apellidos del Estudiante debe poseer al menos 3 caracteres');
                                    $log.='Fila Nº '.$i.' | El Campo Apellidos del Estudiante debe poseer al menos 3 caracteres'.PHP_EOL;
                                    $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | El Campo Apellidos del Estudiante debe poseer al menos 3 caracteres');

                                }

                                /*
                                 * VALIDANDO FECHA DE NACIMIENTO DEL ESTUDIANTE
                                 */
                                if(!Utiles::isValidDate(date('Y-m-d',self::ExcelToPHP($sheetData[$i]['G'])),'ymd')){
                                    $valido=false;
                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'La Fecha de Nacimiento del Estudiante posee un formato incorrecto. El formato valido es DIA/MES/AÑO -> 05/03/1991 ');
                                    $log.='Fila Nº '.$i.' | La Fecha de Nacimiento del Estudiante posee un formato incorrecto. El formato valido es DIA/MES/AÑO -> 05/03/1991'.PHP_EOL;
                                    $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | La Fecha de Nacimiento del Estudiante posee un formato incorrecto. El formato valido es DIA/MES/AÑO -> 05/03/1991');

                                }

                                /*
                                 * VALIDANDO SEXO DEL ESTUDIANTE
                                 */
                                if(!in_array($sheetData[$i]['H'],array('M','F'))){
                                    $valido=false;
                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'El Sexo del Estudiante posee un valor invalido. Los valores permitidos son : "M" y "F"');
                                    $log.='Fila Nº '.$i.' | El Sexo del Estudiante posee un valor invalido. Los valores permitidos son : "M" y "F"'.PHP_EOL;
                                    $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | El Sexo del Estudiante posee un valor invalido. Los valores permitidos son : "M" y "F"');

                                }

                                /*
                                 * VALIDANDO AFINIDAD
                                 */

                                if(!($sheetData[$i]['I']>=1 AND $sheetData[$i]['I']<=19)){
                                    $valido=false;
                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'La Afinidad posee un valor invalido. Los valores permitidos estan entre 1 y 8 para mas información vea la leyenda en el archivo.');
                                    $log.='Fila Nº '.$i.' | La Afinidad posee un valor invalido. Los valores permitidos estan entre 1 y 19 para mas información vea la leyenda en el archivo'.PHP_EOL;
                                    $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | La Afinidad posee un valor invalido. Los valores permitidos estan entre 1 y 19 para mas información vea la leyenda en el archivo');

                                }

                                /*
                                 * VALIDANDO ORDEN
                                 */

                                /*if(!($sheetData[$i]['I']>=1 AND $sheetData[$i]['I']<=8)){
                                    $valido=false;
                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'El Orden de Nacimiento posee un valor invalido. Los valores permitidos estan entre 1 y 8 para mas información vea la leyenda en el archivo.');
                                    $log.='Fila Nº '.$i.' | El Orden de Nacimiento posee un valor invalido. Los valores permitidos estan entre 1 y 8 para mas información vea la leyenda en el archivo'.PHP_EOL;

                                }*/
                                if($valido){
                                    $log.='Fila Nº '.$i.' | No posee errores en cuanto a la estructura de los datos, se procederá a matricular al estudiante'.PHP_EOL;
                                    $estudiantes[$c]['origen']=strtoupper($sheetData[$i]['A']);
                                    $estudiantes[$c]['identificacion']=$sheetData[$i]['B'];
                                    $estudiantes[$c]['nombres']=strtoupper(Utiles::onlyAlphaNumericWithSpace($sheetData[$i]['C']));
                                    $estudiantes[$c]['apellidos']=strtoupper(Utiles::onlyAlphaNumericWithSpace($sheetData[$i]['D']));
                                    $estudiantes[$c]['documento_identidad_repre']=$sheetData[$i]['E'];
                                    $estudiantes[$c]['fecha_nacimiento']=date('Y-m-d',self::ExcelToPHP($sheetData[$i]['G']));
                                    $estudiantes[$c]['sexo']=strtoupper($sheetData[$i]['H']);
                                    $estudiantes[$c]['afinidad']=$sheetData[$i]['I'];
                                    $estudiantes[$c]['orden']=$sheetData[$i]['J'];
                                }

                            }
                            else {
                                if(!(trim($sheetData[$i]['A']) == '' AND trim($sheetData[$i]['B']) == '' AND trim($sheetData[$i]['C']) == '' AND trim($sheetData[$i]['D']) == ''
                                    AND trim($sheetData[$i]['E']) == ''  AND trim($sheetData[$i]['G']) == '' AND trim($sheetData[$i]['H']) == '' AND trim($sheetData[$i]['I']) == ''
                                    AND trim($sheetData[$i]['J']) == '')){
                                    $estudiante_errores[]=array('fila'=>$i,'mensaje'=>'Existe por lo menos un campo que no fue llenado, descarte el Teléfono del Representante.');
                                    $log.='Fila Nº '.$i.' | Existe por lo menos un campo que no fue llenado, descarte el Teléfono del Representante y el Orden de Nacimiento'.PHP_EOL;
                                    $this->registrarTrazaReporte('ESTRUCTURA DE DATOS','Fila Nº '.$i.' | Existe por lo menos un campo que no fue llenado, descarte el Teléfono del Representante y el Orden de Nacimiento');

                                }

                            }
                            $objCell = $sheetData[$i];
                            /*$prefijoSerial = strtoupper(utf8_decode(trim(Utiles::onlyAlphaNumericString($objCell['A']))));
                            $serial = strtoupper(utf8_decode(trim(Utiles::onlyAlphaNumericString($objCell['B']))));

                            //en este Bloque se empieza a armar el objeto rowi el cual contendrá la respuesta del procesamiento de cada fila procesada del archivo
                            $dataTotalSeriales[$j] = $serial;
                            $dataTotalPrefijo[$j] = (strlen($prefijoSerial)==0)?'AA':substr($prefijoSerial, 0, 2); // Si está vacío por defecto el prefijo es 'AA'


                            $j = $i - 2;*/
                            $i++;
                        }

                        /*
                         * VERIFICO SI EXISTEN ESTUDIANTES CON DATOS CORRECTOS EN CUANTO A ESTRUCTURA
                         */
                        if(count($estudiantes)>0){
                            $modelSeccion= new SeccionPlantel();
                            $grado_id=$modelSeccion->obtenerGrado($seccion_plantel_id);
                            $esInicialPrimaria = $modelSeccion->esInicialPrimaria($seccion_plantel_id);
                            if($esInicialPrimaria>0){
                                $this->procesarInscripcionInicialPrimaria($estudiantes,$estudiante_errores,$plantel_id,$seccion_plantel_id,$periodo_id,$grado_id,$log,$directorio_archivo,$proceso_pendiente,$directorio_base);
                            }
                            else {
                                $this->procesarInscripcionBachillerato($estudiantes,$estudiante_errores,$plantel_id,$seccion_plantel_id,$periodo_id,$grado_id,$log,$directorio_archivo,$proceso_pendiente,$directorio_base);
                            }
                        }
                        else {
                            $this->generarLog($log,$directorio_base);
                            $this->archivos=$directorio_base.'archivo.txt';
                            FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'T');
                            $msj=$this->renderPartial('/correos/formulario_inscripcion',array('archivo'=>$this->archivo_correo,'estado'=>$proceso_pendiente['estado']),true);
                            $this->enviarCorreo($proceso_pendiente['email'], 'Sistema de Gestión Escolar | Formulario de Inscripción Masiva',$msj, 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar',$this->archivos);
                            $this->fila=array();
                            $this->registrarTrazaReporte('RESULTADO','NO HAY ESTUDIANTES CON LOS DATOS CORRECTOS PARA PROCEDER A MATRICULARLO');

                        }

                        /*$archivoObj = new PapelMoneda();

                        try{

                            list($operacion, $result->response) = $archivoObj->loadDataSeriales($sheetData, $fileName);

                            if($operacion){
                                $result->class_style = "success";
                                $result->message = "El proceso se ha completado. Puede ver el resultado detallado del mismo en la tabla siguiente.";
                            }else{
                                $result->class_style = "alert";
                                $result->message = "El proceso ha culminado con algunas advertencias. Puede ver el resultado detallado del mismo en la tabla siguiente.";
                            }

                        }
                        catch(Exception $e){

                            $result->class_style = "error";
                            $result->message = "Ha ocurrido un error en el proceso de carga. Recargue la página e intentelo de nuevo. Si el error se repite notifiquelo a los administradores del sistema. ({$e->getMessage()})";

                        }*/

                    }else{
                        FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'T');
                        $result->class_style = "error";
                        $result->message = "Ha ocurrido un error en el proceso de lectura del archivo. Recargue la página e intentelo de nuevo. Si el error se repite notifiquelo a los administradores del sistema.";
                    }
                }
            }
        }
    }

    public function getViewPath() {
        return Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'views';
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath() . $viewName . '.php';
    }

    /**
     * Modified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }
    public static function ExcelToPHP($dateValue = 0) {
        $myExcelBaseDate = 25569;
        //	Adjust for the spurious 29-Feb-1900 (Day 60)
        if ($dateValue < 60) {
            --$myExcelBaseDate;
        }

        // Perform conversion
        if ($dateValue >= 1) {
            $utcDays = $dateValue - $myExcelBaseDate;
            $returnValue = round($utcDays * 86400);
            if (($returnValue <= PHP_INT_MAX) && ($returnValue >= -PHP_INT_MAX)) {
                $returnValue = (integer) $returnValue;
            }
        } else {
            $hours = round($dateValue * 24);
            $mins = round($dateValue * 1440) - round($hours * 60);
            $secs = round($dateValue * 86400) - round($hours * 3600) - round($mins * 60);
            $returnValue = (integer) gmmktime($hours, $mins, $secs);
        }

        // Return
        return $returnValue;
    }	//	function ExcelToPHP()

    /**
     * @param $estudiantes
     * @param $estudiante_errores
     * @param $plantel_id
     * @param $seccion_plantel_id
     * @param $periodo_id
     */
    public function procesarInscripcionInicialPrimaria($estudiantes,$estudiante_errores,$plantel_id,$seccion_plantel_id,$periodo_id,$grado_id,$log,$directorio_archivo,$proceso_pendiente,$directorio_base){
        /*
         * DESDE INCIAL HASTA PRIMER GRADO NO SE TOMA EN CUENTA LA PROSECUCIÓN SINO QUE UN ESTUDIANTE PUEDE SER MATRICULADO SI ESTA EN UN RANGO DE EDADES
         *    '2006-01-01' <---> '2014-12-31'
         */
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $gradosPrescolar [] = 2;
            $gradosPrescolar [] = 3;
            $gradosPrescolar [] = 4;
            $gradosPrescolar [] = 7;
            $gradosPrescolar [] = 8;
            $gradosPrescolar [] = 11;
            $gradosPrescolar [] = 12;
            $estudiantes_matricular=array();
            $estudiantes_creados = array();
            $estudiantes_sin_matricular=array();
            $inscripcion_regular=array();
            $estudiante_matricular=array();

            $modelEstudiante = new Estudiante();
            $log.='========================ERRORES MIENTRAS SE MATRICULAN Y/O REGISTRAN LOS ESTUDIANTES========================'.PHP_EOL;
            foreach($estudiantes as $index => $valor){
                $existe_estudiante=false;
                $esta_matriculado=0;
                $estudiante_id = 0;
                $fecha_nacimiento = '';
                $this->fila['A']=$estudiantes[$index]['origen'];
                $this->fila['B']=$estudiantes[$index]['identificacion'];
                $this->fila['C']=$estudiantes[$index]['nombres'];
                $this->fila['D']=$estudiantes[$index]['apellidos'];
                $this->fila['E']=$estudiantes[$index]['documento_identidad_repre'];
                $this->fila['H']=$estudiantes[$index]['fecha_nacimiento'];
                $this->fila['H']=$estudiantes[$index]['sexo'];
                $this->fila['I']=$estudiantes[$index]['afinidad'];
                $this->fila['J']=$estudiantes[$index]['orden'];
                $existe_estudiante=$modelEstudiante->existeEstudiante($valor['origen'],$valor['identificacion']);
                if($existe_estudiante){
                    $estudiante_id=$existe_estudiante['id'];
                    $fecha_nacimiento=$existe_estudiante['fecha_nacimiento'];
                    $esta_matriculado = $modelEstudiante->estaMatriculado($estudiante_id,$periodo_id);

                    if(!$esta_matriculado>0){
                        if(in_array($grado_id,$gradosPrescolar)){

                            /*
                             * LA FECHA DE NACIMIENTO DEBE ESTAR ENTRE  2006-01-01' <---> '2014-12-31
                             */
                            if(strtotime($fecha_nacimiento)>= strtotime('2006-01-01') AND strtotime($fecha_nacimiento)<= strtotime('2014-12-31')){
                                $estudiante_matricular[]=$estudiante_id;
                                $inscripcion_regular[]=1;
                            }
                            else {
                                $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' para ser matriculado debe tener maximo 8 años de edad.'.PHP_EOL;
                                $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' para ser matriculado debe tener maximo 8 años de edad');

                            }

                        }
                        else {
                            /*
                             * LA FECHA DE NACIMIENTO DEBE ESTAR ENTRE  '1999-01-01' <---> '2006-12-31'
                             */
                            if(strtotime($fecha_nacimiento)>= strtotime('1999-01-01') AND strtotime($fecha_nacimiento)<= strtotime('2006-12-31')){
                                $estudiante_matricular[]=$estudiante_id;
                                $inscripcion_regular[]=1;
                            }
                            else {
                                $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' para ser matriculado debe tener minimo 8 años y maximo 15 años de edad.'.PHP_EOL;
                                $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' para ser matriculado debe tener minimo 8 años y maximo 15 años de edad');

                            }

                        }
                    }
                    else {
                        $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' ya se encuentra matriculado en este periodo.'.PHP_EOL;
                        $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' ya se encuentra matriculado en este periodo');

                    }
                }
                else {
                    /*
                     * NO EXISTE ESTUDIANTE  VALIDAMOS FECHA DE NACIMIENTO
                     */
                    $fecha_nacimiento = $valor['fecha_nacimiento'];
                    if(in_array($grado_id,$gradosPrescolar)){
                        /*
                         * LA FECHA DE NACIMIENTO DEBE ESTAR ENTRE  2006-01-01' <---> '2014-12-31
                         */
                        if(strtotime($fecha_nacimiento)>= strtotime('2006-01-01') AND strtotime($fecha_nacimiento)<= strtotime('2014-12-31')){
                            $estudiante_id = $this->registrarEstudiante($estudiantes[$index]);
                            if($estudiante_id!=null AND $estudiante_id>0){
                                $estudiante_matricular[]=$estudiante_id;
                                $inscripcion_regular[]=1;
                            }
                            else {
                                $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' no existe en el sistema, se intentó registrar y ocurrio un error durante el proceso. Estaremos Revisando este caso');
                                $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' no existe en el sistema, se intento registrar y ocurrio un error durante el proceso. Estaremos Revisando este caso.'.PHP_EOL;
                                $estudiantes_sin_matricular[]=array('origen'=>$valor['origen'],'identificacion'=>$valor['identificacion']);
                            }
                        }
                        else {
                            $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' para ser matriculado debe tener maximo 8 años de edad.'.PHP_EOL;
                            $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' para ser matriculado debe tener maximo 8 años de edad');

                        }

                    }
                    else {
                        /*
                         * LA FECHA DE NACIMIENTO DEBE ESTAR ENTRE  '1999-01-01' <---> '2006-12-31'
                         */
                        if(strtotime($fecha_nacimiento)>= strtotime('1999-01-01') AND strtotime($fecha_nacimiento)<= strtotime('2006-12-31')){
                            $estudiante_id = $this->registrarEstudiante($estudiantes[$index]);
                            if($estudiante_id!=null AND $estudiante_id>0){
                                $estudiante_matricular[]=$estudiante_id;
                                $inscripcion_regular[]=1;
                            }
                            else {
                                $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' no existe en el sistema, se intento registrar y ocurrio un error durante el proceso. Estaremos Revisando este caso.'.PHP_EOL;
                                $estudiantes_sin_matricular[]=array('origen'=>$valor['origen'],'identificacion'=>$valor['identificacion']);
                                $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' no existe en el sistema, se intento registrar y ocurrio un error durante el proceso. Estaremos Revisando este caso');

                            }
                        }
                        else {
                            $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' para ser matriculado debe tener minimo 8 años y maximo 15 años de edad.'.PHP_EOL;
                            $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' para ser matriculado debe tener minimo 8 años y maximo 15 años de edad');

                        }

                    }

                }
            }
            if(count($estudiante_matricular)>0){
                $estudiantes_pg_array = Utiles::to_pg_array($estudiante_matricular);
                $inscripcionRegular_pg_array = Utiles::to_pg_array($inscripcion_regular);
                $resultadoInscripcion = Estudiante::model()->inscribirEstudiantes($estudiantes_pg_array, $plantel_id, $seccion_plantel_id, $periodo_id, self::MODULO, $inscripcionRegular_pg_array);

                $estudiantesMatriculados=Estudiante::model()->buscarEstudiantesPorId($estudiante_matricular,$periodo_id);
                if($estudiantesMatriculados){
                    $log='==========================================ESTUDIANTES MATRICULADOS==========================================='.PHP_EOL;

                    foreach ($estudiantesMatriculados as $index =>$valor) {
                        if($valor['cedula_escolar']>0){
                            $log.='El Estudiante '.$valor['nombres'].' '. $valor['apellidos'].' - Cedula Escolar '.$valor['cedula_escolar'].' fue matriculado exitosamente'.PHP_EOL;
                            $this->registrarTrazaReporte('MATRICULADO','El Estudiante '.$valor['nombres'].' '. $valor['apellidos'].' - Cedula Escolar '.$valor['cedula_escolar'].' fue matriculado exitosamente');

                        }
                        else {
                            $log.='El Estudiante '.$valor['nombres'].' '. $valor['apellidos'].' - Documento de Identidad '.$valor['tdocumento_identidad'].'-'.$valor['documento_identidad'].' fue matriculado exitosamente'.PHP_EOL;
                            $this->registrarTrazaReporte('MATRICULADO','El Estudiante '.$valor['nombres'].' '. $valor['apellidos'].' - Documento de Identidad '.$valor['tdocumento_identidad'].'-'.$valor['documento_identidad'].' fue matriculado exitosamente');

                        }
                    }

                }
                $transaction->commit();
                $this->generarLog($log,$directorio_base);
                $this->archivos=$directorio_base.'archivo.txt';

                $msj=$this->renderPartial('/correos/formulario_inscripcion',array('archivo'=>$this->archivo_correo,'estado'=>$proceso_pendiente['estado']),true);
                $this->enviarCorreo($proceso_pendiente['email'], 'Sistema de Gestión Escolar | Formulario de Inscripción Masiva',$msj, 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar',$this->archivos);
                FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'T');


            }
            else {
                //$transaction->rollback();
                $transaction->commit();
                $this->generarLog($log,$directorio_base);
                $this->archivos=$directorio_base.'archivo.txt';

                $msj=$this->renderPartial('/correos/formulario_inscripcion',array('archivo'=>$this->archivo_correo,'estado'=>$proceso_pendiente['estado']),true);
                $this->enviarCorreo($proceso_pendiente['email'], 'Sistema de Gestión Escolar | Formulario de Inscripción Masiva',$msj, 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar',$this->archivos);
                FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'Q');

            }
        }
        catch (Exception $e){
            //$transaction->rollback();
            $transaction->commit();
            $this->generarLog($log,$directorio_base);
            $this->archivos=$directorio_base.'archivo.txt';
            FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'W');
            $msj=$this->renderPartial('/correos/formulario_inscripcion',array('archivo'=>$this->archivo_correo,'estado'=>$proceso_pendiente['estado']),true);
            $this->enviarCorreo($proceso_pendiente['email'], 'Sistema de Gestión Escolar | Formulario de Inscripción Masiva',$msj, 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar',$this->archivos);
            $this->enviarCorreo('isalazar@me.gob.ve', 'Error | Formulario de Inscripción Masiva',$e, 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar',null);

        }


        /*
         * FALTA VERIFICAR QUE SE PUEDA MATRICULAR POR LO MENOS UN ESTUDIANTE Y PASAR LOS PASAMETROS AL STORED PROCEDURE
         */
    }
    public function registrarEstudiante($estudiante){
        $origen = $estudiante['origen'];
        $identificacion = $estudiante['identificacion'];
        $nombres = $estudiante['nombres'];
        $apellidos = $estudiante['apellidos'];
        $fecha_nacimiento = $estudiante['fecha_nacimiento'];
        $documento_identidad_repre = $estudiante['documento_identidad_repre'];
        $orden= $estudiante['orden'];
        $sexo= $estudiante['sexo'];
        $afinidad = $estudiante['afinidad'];
        $model_estudiante =new Estudiante();
        $model_representante = new Representante();
        $representante_id = Representante::model()->existeRepresentante($documento_identidad_repre);
        if(!$representante_id){
            $existeEnSaime = AutoridadPlantel::model()->busquedaSaime(null,$documento_identidad_repre);
            if($existeEnSaime){

                $model_representante->tdocumento_identidad=$existeEnSaime['origen'];
                $model_representante->documento_identidad=$existeEnSaime['cedula'];
                $model_representante->nombres=$existeEnSaime['nombre'];
                $model_representante->apellidos=$existeEnSaime['apellido'];
                $model_representante->fecha_nacimiento=$existeEnSaime['fecha_nacimiento'];
                $model_representante->save(false);
                $representante_id=$model_representante->id;
            }
            else {
                $representante_id=null;
            }
        }
        if($representante_id!=null AND $representante_id >0){

            $model_estudiante->nombres=strtoupper($nombres);
            $model_estudiante->apellidos=strtoupper($apellidos);
            $model_estudiante->fecha_nacimiento=$fecha_nacimiento;
            $model_estudiante->afinidad_id=$afinidad;
            $model_estudiante->representante_id=$representante_id;
            // $model_estudiante->orden=$orden;
            if($origen=='C'){
                $model_estudiante->cedula_escolar=$identificacion;
            }
            else{
                $model_estudiante->tdocumento_identidad=$origen;
                $model_estudiante->documento_identidad=$identificacion;
            }
            $model_estudiante->save(false);
        }
        return $model_estudiante->id;

    }

    public function procesarInscripcionBachillerato($estudiantes,$estudiante_errores,$plantel_id,$seccion_plantel_id,$periodo_id,$grado_id,$log,$directorio_archivo,$proceso_pendiente,$directorio_base)
    {

        $estudiantes_matricular=array();
        $estudiantes_creados = array();
        $estudiantes_sin_matricular=array();
        $inscripcion_regular=array();
        $estudiante_matricular=array();


        $modelEstudiante = new Estudiante();
        $log.='========================ERRORES MIENTRAS SE MATRICULAN Y/O REGISTRAN LOS ESTUDIANTES========================'.PHP_EOL;

        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach($estudiantes as $index => $valor){
                $existe_estudiante=false;
                $esta_matriculado=0;
                $estudiante_id = 0;
                $fecha_nacimiento = '';
                $this->fila['A']=$estudiantes[$index]['origen'];
                $this->fila['B']=$estudiantes[$index]['identificacion'];
                $this->fila['C']=$estudiantes[$index]['nombres'];
                $this->fila['D']=$estudiantes[$index]['apellidos'];
                $this->fila['E']=$estudiantes[$index]['documento_identidad_repre'];
                $this->fila['H']=$estudiantes[$index]['fecha_nacimiento'];
                $this->fila['H']=$estudiantes[$index]['sexo'];
                $this->fila['I']=$estudiantes[$index]['afinidad'];
                $this->fila['J']=$estudiantes[$index]['orden'];
                $existe_estudiante=$modelEstudiante->existeEstudiante($valor['origen'],$valor['identificacion']);
                if($existe_estudiante){
                    $estudiante_id=$existe_estudiante['id'];
                    $fecha_nacimiento=$existe_estudiante['fecha_nacimiento'];
                    $esta_matriculado = $modelEstudiante->estaMatriculado($estudiante_id,$periodo_id);
                    $matriculable =false;

                    if(!$esta_matriculado>0){
                        $matriculable = $modelEstudiante->puedeMatricular($valor['origen'],$valor['identificacion'],$periodo_id,$seccion_plantel_id);
                        if(!$matriculable){
                            $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' no cumple con los requisitos para inscribirlo en esta sección. Se le notificará a la Dirección de Registro y Control Académico para su gestión.'.PHP_EOL;
                            $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' no cumple con los requisitos para inscribirlo en esta sección. Se le notificará a la Dirección de Registro y Control Académico para su gestión');

                        }
                        else {
                            $estudiante_matricular[]=$estudiante_id;
                            $inscripcion_regular[]=1;
                        }
                    }
                    else {
                        $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' ya se encuentra matriculado en este periodo.'.PHP_EOL;
                        $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' ya se encuentra matriculado en este periodo');

                    }
                }
                else {
                    /*
                     * NO EXISTE ESTUDIANTE
                     */
                    $log.='El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' no se encuentra registrado en el sistema. Se le Notificará a la Dirección de Registro y Control Académico para su gestión.'.PHP_EOL;
                    $this->registrarTrazaReporte('MATRICULACIÓN','El Estudiante '.$valor['origen'].'-'.$valor['identificacion'].' no se encuentra registrado en el sistema. Se le Notificará a la Dirección de Registro y Control Académico para su gestión');

                }

            }
            if(count($estudiante_matricular)>0){
                $estudiantes_pg_array = Utiles::to_pg_array($estudiante_matricular);
                $inscripcionRegular_pg_array = Utiles::to_pg_array($inscripcion_regular);
                $resultadoInscripcion = Estudiante::model()->inscribirEstudiantes($estudiantes_pg_array, $plantel_id, $seccion_plantel_id, $periodo_id, self::MODULO, $inscripcionRegular_pg_array);
                $estudiantesMatriculados=Estudiante::model()->buscarEstudiantesPorId($estudiante_matricular,$periodo_id);
                if($estudiantesMatriculados){
                    $log='==========================================ESTUDIANTES MATRICULADOS==========================================='.PHP_EOL;

                    foreach ($estudiantesMatriculados as $index =>$valor) {
                        if($valor['cedula_escolar']>0){
                            $log.='El Estudiante '.$valor['nombres'].' '. $valor['apellidos'].' - Cedula Escolar '.$valor['cedula_escolar'].' fue matriculado exitosamente'.PHP_EOL;
                            $this->registrarTrazaReporte('MATRICULADO','El Estudiante '.$valor['nombres'].' '. $valor['apellidos'].' - Cedula Escolar '.$valor['cedula_escolar'].' fue matriculado exitosamente');

                        }
                        else {
                            $log.='El Estudiante '.$valor['nombres'].' '. $valor['apellidos'].' - Documento de Identidad '.$valor['tdocumento_identidad'].'-'.$valor['documento_identidad'].' fue matriculado exitosamente'.PHP_EOL;
                            $this->registrarTrazaReporte('MATRICULADO','El Estudiante '.$valor['nombres'].' '. $valor['apellidos'].' - Documento de Identidad '.$valor['tdocumento_identidad'].'-'.$valor['documento_identidad'].' fue matriculado exitosamente');

                        }
                    }

                }
                $transaction->commit();
                $this->generarLog($log,$directorio_base);
                $this->archivos=$directorio_base.'archivo.txt';
                $msj=$this->renderPartial('/correos/formulario_inscripcion',array('archivo'=>$this->archivo_correo,'estado'=>$proceso_pendiente['estado']),true);
                $this->enviarCorreo($proceso_pendiente['email'], 'Sistema de Gestión Escolar | Formulario de Inscripción Masiva',$msj, 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar',$this->archivos);
                FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'T');

            }
            else {

                //$transaction->rollback();
                $transaction->commit();
                $this->generarLog($log,$directorio_base);
                $this->archivos=$directorio_base.'archivo.txt';

                $msj=$this->renderPartial('/correos/formulario_inscripcion',array('archivo'=>$this->archivo_correo,'estado'=>$proceso_pendiente['estado']),true);
                $this->enviarCorreo($proceso_pendiente['email'], 'Sistema de Gestión Escolar | Formulario de Inscripción Masiva',$msj, 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar',$this->archivos);
                FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'R');

            }
        }
        catch (Exception $e){
            // $transaction->rollback();
            $transaction->commit();
            $this->generarLog($log,$directorio_base);
            $this->archivos=$directorio_base.'archivo.txt';

            $msj=$this->renderPartial('/correos/formulario_inscripcion',array('archivo'=>$this->archivo_correo,'estado'=>$proceso_pendiente['estado']),true);
            $this->enviarCorreo($proceso_pendiente['email'], 'Sistema de Gestión Escolar | Formulario de Inscripción Masiva',$msj, 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar',$this->archivos);
            FormularioInscripcionPeriodo::model()->actualizarFormulario($proceso_pendiente['id'],'Y');
            $this->enviarCorreo('isalazar@me.gob.ve', 'Error | Formulario de Inscripción Masiva',$e, 'soporte_gescolar@me.gob.ve','Sistema de Gestión Escolar',null);

        }


    }
    static public function enviarCorreo($to, $subject = 'SIR-SWL', $msj = '', $from = '', $from_name = '',$archivo=null) {
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $mailer->Host = 'mail.me.gob.ve:25';
        $mailer->IsSMTP();

        if (is_array($to)) {
            foreach ($to as $sendTo) {
                $mailer->AddAddress("{$sendTo}");
            }
        } else {
            $mailer->AddAddress("{$to}");
        }

        if (isset($from) and $from != '' and $from != null)
            $mailer->From = $from;
        else
            $mailer->From = Yii::app()->params->adminEmail;
        if (isset($from_name) and $from_name != '' and $from_name != null)
            $mailer->FromName = $from_name;
        else
            $mailer->FromName = Yii::app()->params->adminName;
        if($archivo){
            $mailer->AddBCC('yguerra@me.gob.ve');
            $mailer->AddBCC('jarojasm@me.gob.ve');
            $mailer->AddBCC('gescolar.mppe@gmail.com');
            $mailer->AddAttachment($archivo,'RESULTADO_FORMULARIO_INSCRIPCIÓN.txt');
        }

        $mailer->CharSet = 'UTF-8';
        $mailer->Subject = $subject;
        $mailer->MsgHTML($msj);
        return $mailer->Send();
    }
    public function generarLog($log,$base){
        $file = fopen($base."archivo.txt", "w+");
        fwrite($file, $log . PHP_EOL);
        fclose($file);
    }
    public function registrarTrazaReporte($tipo_error,$mensaje){
        /*
                         *      ESTRUCTURA ARCHIVO
                         * [A] => Origen Del Estudiante -> requerido
                         * [B] => Identificación Del Estudiante -> requerido
                         * [C] => Nombres Del Estudiante -> requerido solo si es nuevo ingreso
                         * [D] => Apellidos Del Estudiante -> requerido solo si es nuevo ingreso
                         * [E] => Cédula Del Representante -> requerido solo si es nuevo ingreso
                         * [F] => Teléfono del Representante
                         * [G] => Fecha_Nacimiento -> requerido
                         * [H] => Sexo -> requerido
                         * [I] => Afinidad -> requerido solo si es nuevo ingreso
                         * [J] => Orden_Nacimiento -> requerido solo si es nuevo ingreso
                         */
        $model = new ReporteFormularioInscripcion();
        $model->usuario_ini_id=1;
        $model->estatus='A';
        $model->tipo_error=$tipo_error;
        if($this->fila != array()){
            $model->origen=trim(strtoupper($this->fila['A']));
            $model->identificacion=trim($this->fila['B']);
            $model->nombres=trim(strtoupper(Utiles::onlyAlphaNumericWithSpace($this->fila['C'])));
            $model->apellidos=trim(strtoupper(Utiles::onlyAlphaNumericWithSpace($this->fila['D'])));
            $model->documento_identidad=trim($this->fila['E']);
            if(date('Y-m-d',self::ExcelToPHP($this->fila['G'])) == null OR date('Y-m-d',self::ExcelToPHP($this->fila['G']))==''){
                $model->fecha_nacimiento=null;
            }
            else {
                $model->fecha_nacimiento=date('Y-m-d',self::ExcelToPHP($this->fila['G']));
            }

            $model->sexo=strtoupper(trim($this->fila['H']));
            $model->afinidad=trim($this->fila['I']);
            $model->orden_nacimiento=trim($this->fila['J']);
        }

        $model->mensaje=strtoupper($mensaje);
        $model->formulario_id=$this->formulario_id;
        return $model->save(false);


    }


}
