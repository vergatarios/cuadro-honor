<?php

/**
 * This is the model class for table "personal.docente".
 *
 * The followings are the available columns in table 'personal.docente':
 * @property integer $id
 * @property string $nombres
 * @property string $apellidos
 * @property string $tdocumento_identidad
 * @property string $documento_identidad
 * @property integer $usuario_id
 * @property integer $nivel_id
 * @property integer $grado_instruccion_id
 * @property integer $estatus_docente_id
 * @property integer $especificacion_estatus_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $escalafon_id
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuario
 * @property GradoInstruccion $gradoInstruccion
 * @property EstatusDocente $estatusDocente
 * @property EspecificacionEstatus $especificacionEstatus
 * @property TipoDocente $escalafon
 * @property AsignaturaDocente[] $asignaturaDocentes
 * @property FuncionPersonal[] $funcionPersonal
 * @property DocentePlantel[] $docentePlantels
 */
class Docente extends CActiveRecord
{
    public $identificacion;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'personal.docente';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombres, apellidos, tdocumento_identidad, documento_identidad, escalafon_id', 'required'),
            array('usuario_id, nivel_id, grado_instruccion_id, estatus_docente_id, especificacion_estatus_id, usuario_ini_id, usuario_act_id, escalafon_id', 'numerical', 'integerOnly'=>true),
            array('nombres, apellidos', 'length', 'max'=>60),
            array('tdocumento_identidad, estatus', 'length', 'max'=>1),
            array('documento_identidad', 'length', 'max'=>15),
           // array('documento_identidad', 'validarUnicidad'),
            array('fecha_act, fecha_elim', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombres, apellidos, tdocumento_identidad, documento_identidad, usuario_id, nivel_id, grado_instruccion_id, estatus_docente_id, especificacion_estatus_id, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus, escalafon_id', 'safe', 'on'=>'search'),
        );
    }

    public  function validarUnicidad($attribute,$params){
        $sql_extra = " AND id <> :id ";
        $tdocumento_identidad =$this->tdocumento_identidad;
        $documento_identidad =$this->documento_identidad;
        $id =$this->id;

        $sql = " SELECT COUNT(id) FROM personal.docente WHERE tdocumento_identidad = :tdocumento_identidad AND documento_identidad=:documento_identidad ";
        if(!is_null($this->id))
            $sql .= $sql_extra;
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":tdocumento_identidad", $tdocumento_identidad, PDO::PARAM_STR);
        $consulta->bindParam(":documento_identidad", $documento_identidad, PDO::PARAM_STR);
        if(!is_null($id))
            $consulta->bindParam(":id", $id, PDO::PARAM_INT);
        $resultado = $consulta->queryScalar();

        if($resultado > 0) {
            $this->addError('documento_identidad','Ya existe un Docente registrado con la misma Identificación');
        }
    }
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'usuario' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_id'),
            'gradoInstruccion' => array(self::BELONGS_TO, 'GradoInstruccion', 'grado_instruccion_id'),
            'estatusDocente' => array(self::BELONGS_TO, 'EstatusDocente', 'estatus_docente_id'),
            'especificacionEstatus' => array(self::BELONGS_TO, 'EspecificacionEstatus', 'especificacion_estatus_id'),
            'escalafon' => array(self::BELONGS_TO, 'TipoDocente', 'escalafon_id'),
            'asignaturaDocentes' => array(self::HAS_MANY, 'AsignaturaDocente', 'docente_id'),
            'funcionPersonals' => array(self::HAS_MANY, 'FuncionPersonal', 'personal_id'),
            'docentePlantels' => array(self::HAS_MANY, 'DocentePlantel', 'docente_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'tdocumento_identidad' => 'Tipo de Documento',
            'documento_identidad' => 'Documento de Identidad',
            'identificacion' => 'Documento de Identidad',
            'usuario_id' => 'Usuario',
            'nivel_id' => 'Nivel',
            'grado_instruccion_id' => 'Grado de Instrucción',
            'estatus_docente_id' => 'Estatus del Docente',
            'especificacion_estatus_id' => 'Especificacion Estatus',
            'fecha_ini' => 'Fecha de Creación',
            'usuario_ini_id' => 'Creado Por',
            'usuario_act_id' => 'Actualizado Por',
            'fecha_act' => 'Fecha Actualización',
            'fecha_elim' => 'Fecha Eliminación',
            'estatus' => 'Estatus del Registro',
            'escalafon_id' => 'Escalafón',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('nombres',$this->nombres,true);
        $criteria->compare('apellidos',$this->apellidos,true);
        $criteria->compare('tdocumento_identidad',$this->tdocumento_identidad,true);
        $criteria->compare('documento_identidad',$this->documento_identidad,true);
        $criteria->compare('usuario_id',$this->usuario_id);
        $criteria->compare('nivel_id',$this->nivel_id);
        $criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
        $criteria->compare('estatus_docente_id',$this->estatus_docente_id);
        $criteria->compare('especificacion_estatus_id',$this->especificacion_estatus_id);
        $criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('usuario_act_id',$this->usuario_act_id);
        $criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('fecha_elim',$this->fecha_elim,true);
        $criteria->compare('estatus',$this->estatus,true);
        $criteria->compare('escalafon_id',$this->escalafon_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function busquedaDocente($documentoIdentidad,$tDocumentoIdentidad){
        $resultadoCedula = null;
        if (in_array($tDocumentoIdentidad, array('V', 'E','P')) AND is_numeric($documentoIdentidad)) {
            $sql = "SELECT id,escalafon_id"
                . " FROM personal.docente"
                . " WHERE "
                . " documento_identidad= :documento_identidad AND "
                . " tdocumento_identidad= :tdocumento_identidad ";

            $buqueda = Yii::app()->db->createCommand($sql);
            $buqueda->bindParam(":documento_identidad", $documentoIdentidad, PDO::PARAM_STR);
            $buqueda->bindParam(":tdocumento_identidad", $tDocumentoIdentidad, PDO::PARAM_STR);
            $resultadoCedula = $buqueda->queryRow();
        }
        if ($resultadoCedula !== array()) {
            return $resultadoCedula;
        } else {
            return null;
        }
    }

    public function obtenerAsignaturasSeccionPlantel($seccion_plantel_id){
        $resultado= null;
        $sql = "SELECT UPPER(a.nombre) as asignatura, UPPER(d.nombres) as nombres ,UPPER(d.apellidos) as apellidos,d.tdocumento_identidad,d.documento_identidad"
            ." FROM personal.docente d"
            ." INNER JOIN personal.asignatura_docente ad on (ad.docente_id = d.id)"
            ." INNER JOIN gplantel.asignatura a on (a.id= ad.asignatura_id)"
            ." INNER JOIN gplantel.seccion_plantel_periodo spp on (spp.id = ad.seccion_plantel_periodo_id)"
            ." WHERE spp.seccion_plantel_id=:seccion_plantel_id"
            ." ORDER BY a.nombre ASC";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $resultado = $buqueda->queryAll();
        if ($resultado !== array()) {
            return $resultado;
        } else {
            return null;
        }
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Docente the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
