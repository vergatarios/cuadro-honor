<?php

/**
 * This is the model class for table "personal.tipo_personal_cargo".
 *
 * The followings are the available columns in table 'personal.tipo_personal_cargo':
 * @property integer $id
 * @property integer $tipo_personal_id
 * @property integer $cargo_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Cargo $cargo
 * @property TipoPersonal $tipoPersonal
 * @property UsergroupsUser $usuarioAct
 */
class TipoPersonalCargo extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'personal.tipo_personal_cargo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('tipo_personal_id, cargo_id, fecha_ini, usuario_ini_id', 'required'),
            array('tipo_personal_id, cargo_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
            array('estatus', 'length', 'max'=>1),
            array('tipo_personal_id','validarUnicidadPersonalCargo','on'=>'registro_modificacion'),
            array('fecha_act, fecha_elim', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, tipo_personal_id, cargo_id, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cargo' => array(self::BELONGS_TO, 'Cargo', 'cargo_id'),
            'tipoPersonal' => array(self::BELONGS_TO, 'TipoPersonal', 'tipo_personal_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
        );
    }

    public function validarUnicidadPersonalCargo($atributo, $params = null) {
        if ((isset($this->tipo_personal_id) AND (isset($this->cargo_id)))) {

            $existe= $this->existePersonalCargo($this->tipo_personal_id,$this->cargo_id);
            if ($existe>0){
                $this->addError($atributo, 'Esta combinación Tipo de Personal y Cargo ya existe.');
                $this->addError('cargo_id', null);
            }
        }

    }

    public function existePersonalCargo($tipo_personal_id,$cargo_id){
        $id=$this->id;
        //$estatus = 'A';
        $sql="SELECT COUNT(id) FROM personal.tipo_personal_cargo
              WHERE tipo_personal_id=:tipo_personal_id AND cargo_id=:cargo_id";

        if($id>0){
            $sql.=' AND id<>:id';
        }
        $busqueda = Yii::app()->db->createCommand($sql);
        if($id>0){
            $busqueda->bindParam(":id", $id, PDO::PARAM_INT);
        }
        $busqueda->bindParam(":tipo_personal_id", $tipo_personal_id, PDO::PARAM_INT);
        $busqueda->bindParam(":cargo_id", $cargo_id, PDO::PARAM_INT);
        return $busqueda->queryScalar();

    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'tipo_personal_id' => 'Tipo Personal',
            'cargo_id' => 'Cargo',
            'fecha_ini' => 'Fecha Ini',
            'usuario_ini_id' => 'Usuario Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
        $criteria->with=array(
            'tipoPersonal'=>array('alias'=>'tp'),
            'cargo'=>array('alias'=>'c')
        );

        /*if(is_numeric($this->id)){
            $criteria->compare('id',$this->id);
        }*/
        if(isset($this->tipo_personal_id) AND is_string($this->tipo_personal_id)){
            $criteria->addSearchCondition('tp.nombre','%'.$this->tipo_personal_id.'%',false,'AND','ILIKE');

        }
        if(isset($this->cargo_id) AND is_string($this->cargo_id)){
            $criteria->addSearchCondition('c.nombre','%'.$this->cargo_id.'%',false,'AND','ILIKE');

        }

        if(strlen($this->fecha_ini)>0 && Utiles::dateCheck($this->fecha_ini)){
            //$criteria->compare('fecha_ini',$this->fecha_ini,true);
            $criteria->addCondition("TO_CHAR(t.fecha_ini,'DD-MM-YYYY') = :fecha_ini");
            $criteria->params = array(':fecha_ini'=>$this->fecha_ini);
        }

        if(is_numeric($this->usuario_ini_id)){
            $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        }

        if(is_numeric($this->usuario_act_id)){
            $criteria->compare('usuario_act_id',$this->usuario_act_id);
        }

        if(strlen($this->fecha_act)>0 && Utiles::dateCheck($this->fecha_act)){
            //$criteria->compare('fecha_act',$this->fecha_act,true);
            $criteria->addCondition("TO_CHAR(t.fecha_act,'DD-MM-YYYY') = :fecha_act");
            $criteria->params =  array_merge($criteria->params, array(':fecha_act'=>$this->fecha_act));
        }
        $criteria->compare('fecha_elim',$this->fecha_elim,true);

        if(in_array($this->estatus,array('A','I','E'))){
            $criteria->compare('estatus',$this->estatus);
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TipoPersonalCargo the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
