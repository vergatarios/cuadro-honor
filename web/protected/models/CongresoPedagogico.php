<?php

/**
 * This is the model class for table "gplantel.congreso_pedagogico".
 *
 * The followings are the available columns in table 'gplantel.congreso_pedagogico':
 * @property integer $id
 * @property integer $plantel_id
 * @property integer $periodo_id
 * @property integer $campo_conocimiento_id
 * @property string $linea_conocimiento
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $observaciones
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Plantel $plantel
 * @property PeriodoEscolar $periodo
 * @property CampoConocimiento $campoConocimiento
 * @property CongresoPedagogicoDocente[] $congresoPedagogicoDocentes
 */
class CongresoPedagogico extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.congreso_pedagogico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('plantel_id, periodo_id, campo_conocimiento_id, linea_conocimiento, fecha_inicio, fecha_fin, usuario_ini_id', 'required'),
			array('plantel_id, periodo_id, campo_conocimiento_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('linea_conocimiento', 'length', 'max'=>140),
			array('estatus', 'length', 'max'=>1),
            array('fecha_fin','validarCronograma'),
			array('observaciones, fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, plantel_id, periodo_id, campo_conocimiento_id, linea_conocimiento, fecha_inicio, fecha_fin, observaciones, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

    public function validarCronograma($attr, $params){
        $fechaInicioTimestamp = strtotime($this->fecha_inicio);
        $fechaFinTimestamp = strtotime($this->fecha_fin);
        if($fechaFinTimestamp  < $fechaInicioTimestamp) {
            $this->addError('fecha_fin', 'La Fecha de Inicio debe ser menor a la Fecha de Fin');
        }
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
			'periodo' => array(self::BELONGS_TO, 'PeriodoEscolar', 'periodo_id'),
			'campoConocimiento' => array(self::BELONGS_TO, 'CampoConocimiento', 'campo_conocimiento_id'),
			'congresoPedagogicoDocentes' => array(self::HAS_MANY, 'CongresoPedagogicoDocente', 'congreso_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'plantel_id' => 'Plantel',
			'periodo_id' => 'Periodo',
			'campo_conocimiento_id' => 'Campo de Conocimiento',
			'linea_conocimiento' => 'Linea de Conocimiento',
			'fecha_inicio' => 'Fecha de Inicio',
			'fecha_fin' => 'Fecha de Fin',
			'observaciones' => 'Observaciones',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('plantel_id',$this->plantel_id);
		$criteria->compare('periodo_id',$this->periodo_id);
		$criteria->compare('campo_conocimiento_id',$this->campo_conocimiento_id);
		$criteria->compare('linea_conocimiento',$this->linea_conocimiento,true);
		$criteria->compare('fecha_inicio',$this->fecha_inicio,true);
		$criteria->compare('fecha_fin',$this->fecha_fin,true);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CongresoPedagogico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
