<?php

/**
 * This is the model class for table "personal.cierre_asignacion_docente".
 *
 * The followings are the available columns in table 'personal.cierre_asignacion_docente':
 * @property integer $id
 * @property integer $periodo_id
 * @property integer $seccion_plantel_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property SeccionPlantel $seccionPlantel
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 * @property PeriodoEscolar $periodo
 */
class CierreAsignacionDocente extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'personal.cierre_asignacion_docente';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('periodo_id, seccion_plantel_id, usuario_ini_id, fecha_ini,estatus', 'required'),
            array('seccion_plantel_id','validarCantidadDocente'),
            array('periodo_id, seccion_plantel_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
            //array('fecha_ini, fecha_act, fecha_elim', 'length', 'max'=>6),
            array('estatus', 'length', 'max'=>1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, periodo_id, seccion_plantel_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'seccionPlantel' => array(self::BELONGS_TO, 'SeccionPlantel', 'seccion_plantel_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_ini_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_act_id'),
            'periodo' => array(self::BELONGS_TO, 'PeriodoEscolar', 'periodo_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'periodo_id' => 'Periodo',
            'seccion_plantel_id' => 'Seccion Plantel',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
        );
    }

    public function validarCantidadDocente($attr, $params = null) {

        if(isset($this->seccion_plantel_id) AND isset($this->periodo_id) AND $this->estatus=='A'){
            $count = AsignaturaDocente::model()->getCantidadDocentesAsignados($this->seccion_plantel_id,$this->periodo_id);
            if($count<1){
                $this->addError('seccion_plantel_id', 'Estimado Usuario, para Cerrar el Proceso de Asignación de Docentes por lo menos debe haber asociado un Docente.');
            }
        }

    }
    public function getPeriodoAsignacionPersonal($seccion_plantel_id,$periodo_id)
    {

        $criteria = new CDbCriteria();
        $criteria->limit=1;
        $criteria->select='estatus';
        $criteria->condition = "periodo_id='$periodo_id' and seccion_plantel_id='$seccion_plantel_id'";
        $resultado = CierreAsignacionDocente::model()->findAll($criteria);
        return $resultado;

    }
    /**
     * Permite obtener el ID del registro para luego usar loadModel o findByPk
     * @param type $plantel_id
     */
    public function getDatos($seccion_plantel_id, $periodo_id) {
        $resultado = null;
        if ($periodo_id != '' AND ! is_null($periodo_id) AND is_numeric($seccion_plantel_id)) {
            $sql = "SELECT id, estatus FROM personal.cierre_asignacion_docente WHERE periodo_id = :periodo_id AND seccion_plantel_id = :seccion_plantel_id LIMIT 1";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
            $consulta->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
            $resultado = $consulta->queryRow();
        }
        return $resultado;
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('periodo_id',$this->periodo_id);
        $criteria->compare('seccion_plantel_id',$this->seccion_plantel_id);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('usuario_act_id',$this->usuario_act_id);
        $criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('fecha_elim',$this->fecha_elim,true);
        $criteria->compare('estatus',$this->estatus,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CierreAsignacionDocente the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
