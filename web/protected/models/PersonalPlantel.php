<?php


/**
 * This is the model class for table "personal.personal_plantel".
 *
 * The followings are the available columns in table 'personal.personal_plantel':
 * @property integer $id
 * @property integer $personal_id
 * @property integer $plantel_id
 * @property integer $periodo_id
 * @property integer $tipo_personal_id
 * @property integer $especialidad_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property string $cobra
 * @property string $labora
 * @property string $horas_asignadas
 * @property string $plantel_id_dep_pers
 * The followings are the available model relations:
 * @property FuncionPersonal[] $funcionPersonals
 * @property UsergroupsUser $usuarioAct
 * @property Personal $personal
 * @property UsergroupsUser $usuarioIni
 * @property Plantel $plantel
 * @property TipoPersonal $tipoPersonal
 * @property Especialidad $especialidad
 * @property Funcion $funcion_id
 *
 *
 */
class PersonalPlantel extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */


    public $nombres;
    public $apellidos;
    public $tdocumento_identidad;
    public $documento_identidad;
    public $tipo_personal;
    public $personal_id;
    public $periodo;


    public function tableName()
    {
        return 'personal.personal_plantel';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        
       
        
        return array(

//  
            array('horas_asignadas', 'validaRangoHoras','on'=>'personalRegistro'),
            array('plantel_id_dep_pers,denominacion_especificacion', 'caracteresPermitidosNumerosLetras','on'=>'personalRegistro'),
            array('tipo_personal_id', 'validarPersonalEspecialidad','on'=>'personalRegistro'),
            array('especialidad_id, tipo_personal_id,labora,cobra,funcion_id,horas_asignadas,situacion_cargo_id,tiempo_dedicacion_id,denominacion_id', 'required','message' => 'El campo: {attribute}, no debe estar vacio','on'=>'personalRegistro'),
            array('denominacion_id,situacion_cargo_id,tiempo_dedicacion_id,personal_id, plantel_id, periodo_id, tipo_personal_id, especialidad_id, usuario_ini_id, usuario_act_id,pers_cond_nom_id,funcion_id', 'numerical', 'integerOnly'=>true,'on'=>'personalRegistro'),
            array('estatus', 'length', 'max'=>1,'on'=>'personalRegistro'),
            array('cobra,labora', 'length', 'max'=>1,'on'=>'personalRegistro'),
            array('denominacion_especificacion', 'length', 'max'=>250,'on'=>'personalRegistro'),
            
            //array('fecha_act, fecha_elim', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('tipo_personal_id, estatus,nombres,apellidos,tdocumento_identidad,documento_identidad,periodo_id,funcion_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'funcionPersonals' => array(self::HAS_MANY, 'FuncionPersonal', 'personal_plantel_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'personal' => array(self::BELONGS_TO, 'Personal', 'personal_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
            'tipoPersonal' => array(self::BELONGS_TO, 'TipoPersonal', 'tipo_personal_id'),
            'especialidad' => array(self::BELONGS_TO, 'Especialidad', 'especialidad_id'),
            'periodoEscolar' => array(self::BELONGS_TO, 'PeriodoEscolar', 'periodo_id'),             
            

        );
    }



    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'personal_id' => 'Personal',
            'cobra' => 'Cobra en el Plantel',
            'labora' => 'Labora en el Plantel',
            'plantel_id' => 'Plantel',
            'periodo_id' => 'Periodo Escolar',
            'especialidad_id' => 'Especialidad del Personal',
            'tipo_personal_id' => 'Tipo de Personal',
            'fecha_ini' => 'Fecha de Creación',
            'usuario_ini_id' => 'Creado Por',
            'usuario_act_id' => 'Actualizado Por',
            'fecha_act' => 'Fecha Actualización',
            'fecha_elim' => 'Fecha Eliminación',
            'estatus' => 'Estatus del Registro',
            'plantel_id_dep_pers' => 'C&oacute;digo del Plantel',
            'horas_asignadas' => 'Horas Asignadas',            
            'funcion_id' => 'Funci&oacute;n del Personal',
            'denominacion_id' => 'Denominaci&oacute;n',
            'situacion_cargo_id' => 'Situaci&oacute;n del Cargo',
            'tiempo_dedicacion_id' => 'Tiempo de Dedicaci&oacute;n',
            'denominacion_especificacion' => 'Especifique denominaci&oacute;n',
            
            




        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($plantel_id)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;


        $criteria->join= 'INNER JOIN personal.personal p ON (p.id = "t".personal_id) ';
        $criteria->join.= 'INNER JOIN personal.tipo_personal tp ON (tp.id = "t".tipo_personal_id) ';
        $criteria->join.= 'INNER JOIN gplantel.periodo_escolar pe ON (pe.id = "t".periodo_id) ';
        $criteria->distinct=true;
        $criteria->select = '"t".periodo_id, pe.periodo,t.plantel_id plantel_id, t.id id,p.nombres nombres,p.apellidos apellidos ,p.documento_identidad documento_identidad,t.tipo_personal_id tipo_personal_id,t.estatus estatus,tp.nombre tipo_personal,p.tdocumento_identidad tdocumento_identidad ';


        $criteria->compare('plantel_id',$plantel_id);



        if (strlen($this->nombres) > 0)
        {

            $criteria->addSearchCondition('p.nombres', '%' . $this->nombres . '%', false, 'AND', 'ILIKE');

        } // nombres

        if (strlen($this->apellidos) > 0)
        {

            $criteria->addSearchCondition('p.apellidos', '%' . $this->apellidos . '%', false, 'AND', 'ILIKE');

        } // apellidos

        if (strlen($this->documento_identidad) > 0)
        {

            $criteria->addSearchCondition('p.documento_identidad::text', '%' . $this->documento_identidad . '%', false, 'AND', 'ILIKE');

        } // cedula


        if (strlen($this->tdocumento_identidad) > 0)
        {

            $criteria->compare('p.tdocumento_identidad',$this->tdocumento_identidad);

        }


        if (strlen($this->periodo_id) > 0)
        {

            $criteria->compare('"t".periodo_id',$this->periodo_id);

        }


        if (strlen($this->tipo_personal_id) > 0)
        {

            $criteria->compare('t.tipo_personal_id',$this->tipo_personal_id);

        }

        if (strlen($this->estatus) > 0)
        {

            $criteria->compare('t.estatus',$this->estatus);

        }










        $dataProvider=new CActiveDataProvider($this, array(
            'criteria' => $criteria,

            'sort'=>array(
                'attributes'=>array(


                    'nombres'=>array(
                        'asc'=>'"p".nombres',
                        'desc'=>'"p".nombres DESC',
                    ),
                    'apellidos'=>array(
                        'asc'=>'"p".apellidos',
                        'desc'=>'"p".apellidos DESC',
                    ),
                    'tdocumento_identidad'=>array(
                        'asc'=>'"p".tdocumento_identidad',
                        'desc'=>'"p".tdocumento_identidad DESC',
                    ),
                    'documento_identidad'=>array(
                        'asc'=>'"p".documento_identidad',
                        'desc'=>'"p".documento_identidad DESC',
                    ),
                    'tipo_personal_id'=>array(
                        'asc'=>'t.tipo_personal_id',
                        'desc'=>'t.tipo_personal_id DESC',
                    ),

                    '*',
                ),
                'defaultOrder'=>'t.estatus ASC, t.id DESC',
            ),



            'pagination'=>array('pageSize'=>5),
        ));



        return $dataProvider;




    } // fin del search principal

//          -------------Pedro---------------------------

    public function obtenerTipoPersonal($personal_deco){
        $sql = "SELECT nombre
                FROM personal.tipo_personal
                WHERE id = $personal_deco" ;

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":personal", $personal_deco, PDO::PARAM_INT);
        $personal = $consulta->queryRow();
        return $personal;
    }


    public function obtenerDatosPersonal($plantel_deco, $periodo_deco, $personal_deco) {

        $sql = "SELECT nombres,apellidos,tdocumento_identidad,documento_identidad,correo,fecha_nacimiento,c.nombre,d.nombre as estatus
                FROM personal.personal as a 
                right join personal.personal_plantel as b
                on a.id = b.personal_id 
                right join personal.personal_condicion_nomina as c 
                on c.id = b.pers_cond_nom_id
                left join personal.especificacion_estatus as d 
                on d.id = a.especificacion_estatus_id
                WHERE b.tipo_personal_id = :personal
                AND b.plantel_id = :plantel
                AND b.periodo_id = :periodo";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel", $plantel_deco, PDO::PARAM_INT);
        $consulta->bindParam(":periodo", $periodo_deco, PDO::PARAM_INT);
        $consulta->bindParam(":personal", $personal_deco, PDO::PARAM_INT);
        $personalFiltrado = $consulta->queryAll();
        return $personalFiltrado;
    }

    // ---------------Fin Pedro------------------------

    public function existeDocentePlantel($personal_id,$plantel_id,$periodo_id){
        $resultadoCedula = null;
        $sql = "SELECT id"
            . " FROM personal.personal_plantel"
            . " WHERE "
            . " personal_id= :personal_id AND plantel_id=:plantel_id AND periodo_id=:periodo_id ";
        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":personal_id", $personal_id, PDO::PARAM_INT);
        $buqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $buqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $resultadoCedula = $buqueda->queryScalar();
        return $resultadoCedula;
    }



    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PersonalPlantel the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }



    public function validarPersonalEspecialidad($attribute, $params)
    {

        if($this->tipo_personal_id==Constantes::DOCENTE && $this->especialidad_id=="")
        {
            $this->addError('errorPersonalEspecialidad',"El campo Especialidad del Personal es obligatorio cuando el tipo de personal es Docente");
        }



    } // verificar que la cadena sea valida



    public function getBuscarPersonalPlantel($personal_id,$plantel_id,$periodo_id,$estatus)
    {
        $criteria = new CDbCriteria();




        //$criteria->order = 'nombre ASC';
        $criteria->condition = " personal_id='$personal_id' and plantel_id='$plantel_id' and periodo_id='$periodo_id' and estatus='$estatus' ";
        $resultado = PersonalPlantel::model()->findAll($criteria);

        if(count($resultado)==0)
        {
            return false;
        }


        return true;



    } // fin del metodo para verificar si existe un registro duplicado

    public function getPlantelDependenciaFiltro($cod_plantel)
    {

        $criteria = new CDbCriteria();
        $criteria->order = 'nombre ASC';
        $criteria->condition = "cod_plantel='$cod_plantel' ";
        $resultado = Plantel::model()->findAll($criteria);
        return $resultado;

    } // fin de la funcion para obtener los datos del plantel del cual depende el trabajador que no cobre o no labore fisicamente en el plantel original

    
    
     public function getDenominacion()
     {
        
        $criteria = new CDbCriteria();
        $criteria->order = 'nombre ASC';
        $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = DenominacionPersonal::model()->findAll($criteria);
        return $resultado;
        
     } // fin de la funcion para traer el listado de denominacion
    
     public function getSituacionCargo()
     {
        
        $criteria = new CDbCriteria();
        $criteria->order = 'nombre ASC';
        $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = SituacionCargo::model()->findAll($criteria);
        return $resultado;
        
     } // fin de la funcion para traer el listado de situacion del cargo
    
     public function getTiempoDedicacion()
     {
        
        $criteria = new CDbCriteria();
        $criteria->order = 'nombre ASC';
        $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' ";
        $resultado = TiempoDedicacion::model()->findAll($criteria);
        return $resultado;
        
     } // fin de la funcion para traer el tiempo de dedicacion
     

     
     public function getRestriccionesEstatus($tipo_personal_id, $especificacion_estatus_id)
     {
        
        $criteria = new CDbCriteria();
        $criteria->limit = 1;
        $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' "
                             . " and tipo_personal_id='$tipo_personal_id' "
                             . " and especificacion_estatus_id='$especificacion_estatus_id'  ";
        $resultado = RestriccionEstatus::model()->findAll($criteria);
        return $resultado;
        
     } // fin de la funcion para traer el tiempo de dedicacion

    
    public function validaRangoHoras($attribute, $params)
    {
        
        if (  $this->horas_asignadas < Constantes::MIN_HORAS_ASIGNADAS)
        {
            $this->addError('errorMinimoHorasAsignadas',"El campo: Horas Asignadas debe ser mayor o igual que: ".Constantes::MIN_HORAS_ASIGNADAS);
        }
        
        if (  $this->horas_asignadas > Constantes::MAX_HORAS_ASIGNADAS)
        {
            $this->addError('errorMaximoHorasAsignadas',"El campo: Horas Asignadas debe ser menor o igual que: ".Constantes::MAX_HORAS_ASIGNADAS );
        }
         
        
    }

    public function caracteresPermitidosNumerosLetras($attribute, $params)
    {


        $descripcion_atributos=$this->attributeLabels();
        $utiles=new Utiles();
        if(!$utiles->validar_caracteres_numeros_letras($this->$attribute))
        {
            $this->addError('errorCaracteres',"El campo: $descripcion_atributos[$attribute], contiene caracteres inv&aacute;lidos s&oacute;lo se permiten n&uacute;meros y letras");
        }

    } // verificar que la cadena sea valida  


    public function getEspecialidadPersonalFiltro($personal_id)
    {
        $estatus=Constantes::ESTATUS_ACTIVO;

        $sql = "    select esp.*, esp_tp_pers.tipo_personal_id,esp_tp_pers.especialidad_id
                    from personal.especialidad esp
                    INNER JOIN personal.especialidad_tipo_personal esp_tp_pers on esp.id=esp_tp_pers.especialidad_id
                    where esp.estatus=:estatus and esp_tp_pers.tipo_personal_id=:personal_id";
        $resultado = Yii::app()->db->createCommand($sql);
        $resultado->bindParam(":estatus",$estatus, PDO::PARAM_STR);
        $resultado->bindParam(":personal_id", $personal_id, PDO::PARAM_INT);

        $resultado = $resultado->queryAll();


        return $resultado;


    } // retornar la lista de especialidades den función del tipo de personal

    
    public function getDenominacionFiltro($personal_id)
    {
        $estatus=Constantes::ESTATUS_ACTIVO;

        $sql = "    select denom.*, denom_tp_pers.tipo_personal_id,denom_tp_pers.denominacion_id
                    from personal.denominacion denom
                    INNER JOIN personal.denominacion_tipo_personal denom_tp_pers on denom.id=denom_tp_pers.denominacion_id
                    where denom.estatus=:estatus and denom_tp_pers.tipo_personal_id=:personal_id";
        $resultado = Yii::app()->db->createCommand($sql);
        $resultado->bindParam(":estatus",$estatus, PDO::PARAM_STR);
        $resultado->bindParam(":personal_id", $personal_id, PDO::PARAM_INT);

        $resultado = $resultado->queryAll();


        return $resultado;


    } // retornar la lista de especialidades den función del tipo de personal
    
    

    public function getFuncionPersonalFiltro($personal_id)
    {
        $estatus=Constantes::ESTATUS_ACTIVO;

        $sql = "    select func.*, func_tp_pers.tipo_personal_id,func_tp_pers.funcion_id
                    from personal.funcion func
                    INNER JOIN personal.funcion_tipo_personal func_tp_pers on func.id=func_tp_pers.funcion_id
                    where func.estatus=:estatus and func_tp_pers.tipo_personal_id=:personal_id";

        $resultado = Yii::app()->db->createCommand($sql);
        $resultado->bindParam(":estatus",$estatus, PDO::PARAM_STR);
        $resultado->bindParam(":personal_id", $personal_id, PDO::PARAM_INT);
        $resultado = $resultado->queryAll();
        return $resultado;


    } // retornar la lista de especialidades den función del tipo de personal

    public function existePersonalPlantel($documento_identidad,$tdocumento_identidad,$plantel_id,$periodo_id){
        $resultadoCedula = null;
        $tipo_personal = Constantes::ADMINISTRATIVO.','.Constantes::DOCENTE;

        $sql = "SELECT UPPER(p.nombres) as nombre, UPPER(p.apellidos) as apellido
             FROM personal.personal_plantel pp
             INNER JOIN personal.personal p ON (p.id=pp.personal_id)
             WHERE
             pp.plantel_id=:plantel_id AND pp.periodo_id=:periodo_id AND pp.tipo_personal_id IN ($tipo_personal) AND p.documento_identidad=:documento_identidad AND p.tdocumento_identidad=:tdocumento_identidad";
        $busqueda = Yii::app()->db->createCommand($sql);

        $busqueda->bindParam(":documento_identidad", $documento_identidad, PDO::PARAM_STR);
        $busqueda->bindParam(":tdocumento_identidad", $tdocumento_identidad, PDO::PARAM_STR);
        $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $busqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $resultadoCedula = $busqueda->queryRow();
        return $resultadoCedula;
    }



} // FIN DE LA CLASE
