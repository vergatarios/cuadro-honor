<?php

/**
 * This is the model class for table "personal.asignatura_docente".
 *
 * The followings are the available columns in table 'personal.asignatura_docente':
 * @property integer $id
 * @property integer $seccion_plantel_id
 * @property integer $periodo_id
 * @property integer $personal_plantel_id
 * @property integer $asignatura_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property Asignatura $asignatura
 * @property UsergroupsUser $usuarioIni
 * @property SeccionPlantel $seccionPlantel
 */
class AsignaturaDocente extends CActiveRecord
{
    public $tdocumento_identidad;
    public $documento_identidad;
    public $nombre_completo;
    public $escalafon_id;
    public $nombres;
    public $apellidos;
    public $fecha_nacimiento;
    public $valido;
    public $asignatura_nombre;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'personal.asignatura_docente';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            /*	array('seccion_plantel_id, periodo_id, asignatura_id, usuario_ini_id, fecha_ini', 'required'),*/
            array('seccion_plantel_id, periodo_id, personal_plantel_id, asignatura_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
            array('estatus', 'length', 'max'=>1),
            array('fecha_act, fecha_elim', 'safe'),

            array('nombres, apellidos, tdocumento_identidad, documento_identidad', 'required', 'message' => 'El campo: {attribute}, no debe estar vacio', 'on'=>'asignaturaDocente'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, seccion_plantel_id, periodo_id, personal_plantel_id, asignatura_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'asignatura' => array(self::BELONGS_TO, 'Asignatura', 'asignatura_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'seccionPlantel' => array(self::BELONGS_TO, 'SeccionPlantel', 'seccion_plantel_id'),
            'personalPlantel' => array(self::BELONGS_TO, 'PersonalPlantel', 'personal_plantel_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'seccion_plantel_id' => 'Seccion Plantel',
            'periodo_id' => 'Periodo',
            'personal_plantel_id' => 'Personal Plantel',
            'asignatura_id' => 'Asignatura',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'tdocumento_identidad' => 'Tipo de Documento',
            'documento_identidad' => 'Documento de Identidad',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($response = null)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
        //vacio o null = dataprovider;1=QueryAll;
        $criteria=new CDbCriteria;

        $criteria->compare('seccion_plantel_id',$this->seccion_plantel_id);
        $criteria->compare('periodo_id',$this->periodo_id);
        $criteria->compare('estatus',$this->estatus,true);

        if($response == null){
            $respuesta = new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort'=>array(
                    'defaultOrder'=>'id ASC',
                )

            ));

        }elseif($response==1){
            $criteria->select="a.nombre as asignatura_nombre, t.asignatura_id as asignatura_id";

            $criteria->join="INNER JOIN gplantel.asignatura a ON a.id = t.asignatura_id";
            $respuesta = $this->model()->findAll($criteria);
        }else{
            $respuesta=null;
        }

        return $respuesta;


    }

    public function getCantidadDocentesAsignados($seccion_plantel_id,$periodo_id){
        $sql = "SELECT COUNT(1) FROM personal.asignatura_docente WHERE seccion_plantel_id=:seccion_plantel_id AND periodo_id=:periodo_id AND personal_plantel_id IS NOT NULL";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $command->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $command->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $resultado = $command->queryScalar();
        if($resultado== array()){
            $resultado=null;
        }
        return $resultado;

    }
    public function getDatosCuadraturaMedia($plantel_id){
        $sql = "
            SELECT
            p.nombre,
            (
            SELECT count(DISTINCT sp.id)
            FROM gplantel.seccion_plantel sp
            INNER JOIN gplantel.plantel p ON (p.id=sp.plantel_id)
            INNER JOIN gplantel.seccion_plantel_periodo spp ON (spp.seccion_plantel_id=sp.id)
            INNER JOIN matricula.inscripcion_estudiante ie ON (ie.seccion_plantel_periodo_id=spp.id)
            WHERE spp.periodo_id=14 AND ie.periodo_id=14 AND p.id=:plantel_id AND sp.grado_id=1
            HAVING count(ie.estudiante_id)>1
            ) AS primer_ano,
            (
            SELECT count(DISTINCT sp.id)
            FROM gplantel.seccion_plantel sp
            INNER JOIN gplantel.plantel p ON (p.id=sp.plantel_id)
            INNER JOIN gplantel.seccion_plantel_periodo spp ON (spp.seccion_plantel_id=sp.id)
            INNER JOIN matricula.inscripcion_estudiante ie ON (ie.seccion_plantel_periodo_id=spp.id)
            WHERE spp.periodo_id=14 AND ie.periodo_id=14 AND p.id=:plantel_id AND sp.grado_id=5
            HAVING count(ie.estudiante_id)>1
            ) AS segundo_ano,
            (
            SELECT count(DISTINCT sp.id)
            FROM gplantel.seccion_plantel sp
            INNER JOIN gplantel.plantel p ON (p.id=sp.plantel_id)
            INNER JOIN gplantel.seccion_plantel_periodo spp ON (spp.seccion_plantel_id=sp.id)
            INNER JOIN matricula.inscripcion_estudiante ie ON (ie.seccion_plantel_periodo_id=spp.id)
            WHERE spp.periodo_id=14 AND ie.periodo_id=14 AND p.id=:plantel_id AND sp.grado_id=9
            HAVING count(ie.estudiante_id)>1
            ) AS tercero_ano
            FROM gplantel.plantel p
            WHERE p.id=:plantel_id
";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $command->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $resultado = $command->queryRow();

        if($resultado== array()){
            $resultado=null;
        }return $resultado;

    }

    public function getCuadraturaMedia($plantel_id){
        $sql = "
                SELECT g.id,g.nombre,a.nombre,per.nombres,per.nombres,pga.horas
                FROM personal.asignatura_docente ad
                INNER JOIN gplantel.asignatura a ON (ad.asignatura_id=a.id)
                INNER JOIN personal.personal_plantel perp ON (perp.id=ad.personal_plantel_id)
                INNER JOIN personal.personal per ON (per.id=perp.personal_id)
                INNER JOIN gplantel.seccion_plantel sp ON (sp.id=ad.seccion_plantel_id)
                INNER JOIN gplantel.planes_grados_asignaturas pga ON (pga.asignatura_id=a.id AND sp.grado_id=pga.grado_id AND pga.plan_id=sp.plan_id)
                INNER JOIN gplantel.grado g ON (g.id=pga.grado_id)
                INNER JOIN gplantel.plantel p ON (p.id=sp.plantel_id)
                WHERE p.id=:plantel_id
                ORDER BY pga.orden
";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $command->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $resultado = $command->queryAll();

        if($resultado== array()){
            $resultado=null;
        }
        return $resultado;

    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AsignaturaDocente the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
