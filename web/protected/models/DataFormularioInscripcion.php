<?php

/**
 * This is the model class for table "matricula.data_formulario_inscripcion".
 *
 * The followings are the available columns in table 'matricula.data_formulario_inscripcion':
 * @property integer $id
 * @property integer $formulario_id
 * @property string $origen
 * @property string $identificacion
 * @property string $nombres
 * @property string $apellidos
 * @property string $documento_identidad
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property integer $afinidad
 * @property integer $orden_nacimiento
 * @property string $mensaje
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioIni
 * @property FormularioInscripcionPeriodo $formulario
 */
class DataFormularioInscripcion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'matricula.data_formulario_inscripcion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('formulario_id, fecha_ini', 'required'),
			array('formulario_id, afinidad, orden_nacimiento, usuario_ini_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>1),
			array('origen, identificacion, nombres, apellidos, documento_identidad, fecha_nacimiento, sexo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, formulario_id, origen, identificacion, nombres, apellidos, documento_identidad, fecha_nacimiento, sexo, afinidad, orden_nacimiento, mensaje, usuario_ini_id, fecha_ini, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'formulario' => array(self::BELONGS_TO, 'FormularioInscripcionPeriodo', 'formulario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'formulario_id' => 'Formulario',
			'origen' => 'Origen',
			'identificacion' => 'Identificacion',
			'nombres' => 'Nombres',
			'apellidos' => 'Apellidos',
			'documento_identidad' => 'Documento Identidad',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'sexo' => 'Sexo',
			'afinidad' => 'Afinidad',
			'orden_nacimiento' => 'Orden Nacimiento',
			'mensaje' => 'Mensaje',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('formulario_id',$this->formulario_id);
		$criteria->compare('origen',$this->origen,true);
		$criteria->compare('identificacion',$this->identificacion,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('documento_identidad',$this->documento_identidad,true);
		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('afinidad',$this->afinidad);
		$criteria->compare('orden_nacimiento',$this->orden_nacimiento);
		$criteria->compare('mensaje',$this->mensaje,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DataFormularioInscripcion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
