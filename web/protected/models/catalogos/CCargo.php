<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CCargo extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'descripcion',
  3 => 'ente_id',
  4 => 'consecutivo',
  5 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 11,
    'nombre' => 'Administrativo de Zona Educativa',
    'descripcion' => NULL,
    'ente_id' => 4,
    'consecutivo' => NULL,
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 10,
    'nombre' => 'Administrativo de Registro y Control de Estudio',
    'descripcion' => NULL,
    'ente_id' => 5,
    'consecutivo' => NULL,
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 1,
    'nombre' => 'Jefe de Registro y Control de Estudio',
    'descripcion' => NULL,
    'ente_id' => 4,
    'consecutivo' => NULL,
    'estatus' => 'A',
  ),
  3 => 
  array (
    'id' => 3,
    'nombre' => 'Director de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 0,
    'estatus' => 'A',
  ),
  4 => 
  array (
    'id' => 15,
    'nombre' => 'Supervisor de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 13,
    'estatus' => 'I',
  ),
  5 => 
  array (
    'id' => 6,
    'nombre' => 'Jefe de Zona Educativa',
    'descripcion' => NULL,
    'ente_id' => 4,
    'consecutivo' => 1,
    'estatus' => 'A',
  ),
  6 => 
  array (
    'id' => 19,
    'nombre' => 'Coordinador(a) Suplente de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 5,
    'estatus' => 'I',
  ),
  7 => 
  array (
    'id' => 18,
    'nombre' => 'Sub-Director(a) Suplente de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 3,
    'estatus' => 'I',
  ),
  8 => 
  array (
    'id' => 17,
    'nombre' => 'Coordinador(a) de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 4,
    'estatus' => 'I',
  ),
  9 => 
  array (
    'id' => 16,
    'nombre' => 'Director(a) Suplente de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 2,
    'estatus' => 'I',
  ),
  10 => 
  array (
    'id' => 13,
    'nombre' => 'Supervisor de Registro y Control de Estudio',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 7,
    'estatus' => 'I',
  ),
  11 => 
  array (
    'id' => 14,
    'nombre' => 'Supervisor de Zona Educativa',
    'descripcion' => NULL,
    'ente_id' => 4,
    'consecutivo' => NULL,
    'estatus' => 'A',
  ),
  12 => 
  array (
    'id' => 12,
    'nombre' => 'Administrativo de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 6,
    'estatus' => 'I',
  ),
  13 => 
  array (
    'id' => 9,
    'nombre' => 'Profesor(a) de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 12,
    'estatus' => 'I',
  ),
  14 => 
  array (
    'id' => 8,
    'nombre' => 'Maestro(a) de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 11,
    'estatus' => 'I',
  ),
  15 => 
  array (
    'id' => 20,
    'nombre' => 'Funcionario Designado',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 14,
    'estatus' => 'A',
  ),
  16 => 
  array (
    'id' => 7,
    'nombre' => 'Planificación de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 8,
    'estatus' => 'I',
  ),
  17 => 
  array (
    'id' => 5,
    'nombre' => 'Sub-Director(a) de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 1,
    'estatus' => 'I',
  ),
  18 => 
  array (
    'id' => 4,
    'nombre' => 'Secretario(a) de Plantel',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 9,
    'estatus' => 'I',
  ),
  19 => 
  array (
    'id' => 2,
    'nombre' => 'Coordinador(a) Misión Ribas',
    'descripcion' => NULL,
    'ente_id' => 1,
    'consecutivo' => 10,
    'estatus' => 'I',
  ),
)		; 

	}
}