<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CZonaEducativa extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estado_id',
  3 => 'municipio_id',
  4 => 'parroquia_id',
  5 => 'jefe_actual_id',
  6 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 1,
    'nombre' => 'AMAZONAS',
    'estado_id' => 35,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 2,
    'nombre' => 'ANZOATEGUI',
    'estado_id' => 18,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 3,
    'nombre' => 'APURE',
    'estado_id' => 36,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  3 => 
  array (
    'id' => 4,
    'nombre' => 'ARAGUA',
    'estado_id' => 23,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  4 => 
  array (
    'id' => 5,
    'nombre' => 'BARINAS',
    'estado_id' => 37,
    'municipio_id' => 266,
    'parroquia_id' => 2009,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  5 => 
  array (
    'id' => 6,
    'nombre' => 'BOLIVAR',
    'estado_id' => 26,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  6 => 
  array (
    'id' => 7,
    'nombre' => 'CARABOBO',
    'estado_id' => 27,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  7 => 
  array (
    'id' => 8,
    'nombre' => 'COJEDES',
    'estado_id' => 38,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  8 => 
  array (
    'id' => 9,
    'nombre' => 'DELTA AMACURO',
    'estado_id' => 39,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  9 => 
  array (
    'id' => 11,
    'nombre' => 'DISTRITO CAPITAL',
    'estado_id' => 21,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  10 => 
  array (
    'id' => 12,
    'nombre' => 'FALCON',
    'estado_id' => 40,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  11 => 
  array (
    'id' => 13,
    'nombre' => 'GUARICO',
    'estado_id' => 41,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  12 => 
  array (
    'id' => 14,
    'nombre' => 'LARA',
    'estado_id' => 25,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  13 => 
  array (
    'id' => 15,
    'nombre' => 'MERIDA',
    'estado_id' => 30,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  14 => 
  array (
    'id' => 16,
    'nombre' => 'MIRANDA',
    'estado_id' => 28,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  15 => 
  array (
    'id' => 17,
    'nombre' => 'MONAGAS',
    'estado_id' => 24,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  16 => 
  array (
    'id' => 18,
    'nombre' => 'NUEVA ESPARTA',
    'estado_id' => 16,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  17 => 
  array (
    'id' => 19,
    'nombre' => 'PORTUGUESA',
    'estado_id' => 34,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  18 => 
  array (
    'id' => 20,
    'nombre' => 'SUCRE',
    'estado_id' => 42,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  19 => 
  array (
    'id' => 21,
    'nombre' => 'TACHIRA',
    'estado_id' => 22,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  20 => 
  array (
    'id' => 22,
    'nombre' => 'TRUJILLO',
    'estado_id' => 32,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  21 => 
  array (
    'id' => 23,
    'nombre' => 'VARGAS',
    'estado_id' => 44,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  22 => 
  array (
    'id' => 24,
    'nombre' => 'YARACUY',
    'estado_id' => 43,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  23 => 
  array (
    'id' => 25,
    'nombre' => 'ZULIA',
    'estado_id' => 20,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
  24 => 
  array (
    'id' => 10,
    'nombre' => 'DEPENDENCIAS FEDERALES',
    'estado_id' => 45,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'I',
  ),
  25 => 
  array (
    'id' => 26,
    'nombre' => 'CENTRAL MINISTERIO',
    'estado_id' => NULL,
    'municipio_id' => NULL,
    'parroquia_id' => NULL,
    'jefe_actual_id' => NULL,
    'estatus' => 'A',
  ),
)		; 

	}
}