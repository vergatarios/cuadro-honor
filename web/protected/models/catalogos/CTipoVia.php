<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CTipoVia extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'co_stat_data',
  2 => 'co_tipo_via',
  3 => 'nb_tipo_via',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 1,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'AT',
    'nb_tipo_via' => 'AUTOPISTA',
  ),
  1 => 
  array (
    'id' => 2,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'AV',
    'nb_tipo_via' => 'AVENIDA',
  ),
  2 => 
  array (
    'id' => 3,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'BV',
    'nb_tipo_via' => 'BOULEVARD',
  ),
  3 => 
  array (
    'id' => 4,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'CA',
    'nb_tipo_via' => 'CALLE',
  ),
  4 => 
  array (
    'id' => 5,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'CJ',
    'nb_tipo_via' => 'CALLEJON',
  ),
  5 => 
  array (
    'id' => 6,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'CM',
    'nb_tipo_via' => 'CAMINO',
  ),
  6 => 
  array (
    'id' => 7,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'CR',
    'nb_tipo_via' => 'CARRERA',
  ),
  7 => 
  array (
    'id' => 8,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'CT',
    'nb_tipo_via' => 'CARRETERA',
  ),
  8 => 
  array (
    'id' => 9,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'EQ',
    'nb_tipo_via' => 'ESQUINA',
  ),
  9 => 
  array (
    'id' => 10,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'ES',
    'nb_tipo_via' => 'ESCALERA',
  ),
  10 => 
  array (
    'id' => 11,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'MA',
    'nb_tipo_via' => 'MANZANA',
  ),
  11 => 
  array (
    'id' => 12,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'PA',
    'nb_tipo_via' => 'PASEO',
  ),
  12 => 
  array (
    'id' => 13,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'PL',
    'nb_tipo_via' => 'PLAZA',
  ),
  13 => 
  array (
    'id' => 14,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'PQ',
    'nb_tipo_via' => 'PARQUE',
  ),
  14 => 
  array (
    'id' => 15,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'PR',
    'nb_tipo_via' => 'PROLONGACION',
  ),
  15 => 
  array (
    'id' => 16,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'PS',
    'nb_tipo_via' => 'PASAJE',
  ),
  16 => 
  array (
    'id' => 17,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'QE',
    'nb_tipo_via' => 'QUEBRADA',
  ),
  17 => 
  array (
    'id' => 18,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'RM',
    'nb_tipo_via' => 'RAMAL',
  ),
  18 => 
  array (
    'id' => 19,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'RT',
    'nb_tipo_via' => 'RUTA',
  ),
  19 => 
  array (
    'id' => 20,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'SE',
    'nb_tipo_via' => 'SECCION',
  ),
  20 => 
  array (
    'id' => 21,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'TR',
    'nb_tipo_via' => 'TRANSVERSAL',
  ),
  21 => 
  array (
    'id' => 22,
    'co_stat_data' => 'A',
    'co_tipo_via' => 'VE',
    'nb_tipo_via' => 'VEREDA',
  ),
)		; 

	}
}