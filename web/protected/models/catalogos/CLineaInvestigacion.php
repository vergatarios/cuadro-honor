<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CLineaInvestigacion extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 1,
    'nombre' => 'Linea 1',
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 2,
    'nombre' => 'Linea 2',
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 3,
    'nombre' => 'Linea 3 ',
    'estatus' => 'A',
  ),
)		; 

	}
}