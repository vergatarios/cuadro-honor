<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CGrado extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estatus',
  3 => 'consecutivo',
  4 => 'modalidad_id',
  5 => 'es_final',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 71,
    'nombre' => '1ER TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 13,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  1 => 
  array (
    'id' => 72,
    'nombre' => '2DO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 14,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  2 => 
  array (
    'id' => 73,
    'nombre' => '3ER TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 15,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  3 => 
  array (
    'id' => 74,
    'nombre' => '4TO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 16,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  4 => 
  array (
    'id' => 75,
    'nombre' => '5TO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 17,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  5 => 
  array (
    'id' => 76,
    'nombre' => '6TO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 18,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  6 => 
  array (
    'id' => 1,
    'nombre' => '1ER AÑO',
    'estatus' => 'A',
    'consecutivo' => 13,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  7 => 
  array (
    'id' => 2,
    'nombre' => '1ER GRADO',
    'estatus' => 'A',
    'consecutivo' => 7,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  8 => 
  array (
    'id' => 3,
    'nombre' => '1ER NIVEL',
    'estatus' => 'A',
    'consecutivo' => 4,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  9 => 
  array (
    'id' => 4,
    'nombre' => '1ER MATERNAL (3 MESES A 1 AÑO)',
    'estatus' => 'A',
    'consecutivo' => 1,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  10 => 
  array (
    'id' => 5,
    'nombre' => '2DO AÑO',
    'estatus' => 'A',
    'consecutivo' => 14,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  11 => 
  array (
    'id' => 16,
    'nombre' => '5TO GRADO',
    'estatus' => 'A',
    'consecutivo' => 11,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  12 => 
  array (
    'id' => 14,
    'nombre' => '4TO GRADO',
    'estatus' => 'A',
    'consecutivo' => 10,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  13 => 
  array (
    'id' => 13,
    'nombre' => '4TO AÑO',
    'estatus' => 'A',
    'consecutivo' => 16,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  14 => 
  array (
    'id' => 12,
    'nombre' => '3ER MATERNAL (2 AÑOS A 3 AÑOS)',
    'estatus' => 'A',
    'consecutivo' => 3,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  15 => 
  array (
    'id' => 9,
    'nombre' => '3ER AÑO',
    'estatus' => 'A',
    'consecutivo' => 15,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  16 => 
  array (
    'id' => 8,
    'nombre' => '2DO MATERNAL (1 AÑO A 2 AÑOS)',
    'estatus' => 'A',
    'consecutivo' => 2,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  17 => 
  array (
    'id' => 7,
    'nombre' => '2DO NIVEL',
    'estatus' => 'A',
    'consecutivo' => 5,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  18 => 
  array (
    'id' => 6,
    'nombre' => '2DO GRADO',
    'estatus' => 'A',
    'consecutivo' => 8,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  19 => 
  array (
    'id' => 10,
    'nombre' => '3ER GRADO',
    'estatus' => 'A',
    'consecutivo' => 9,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  20 => 
  array (
    'id' => 59,
    'nombre' => '1ER SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 13,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  21 => 
  array (
    'id' => 60,
    'nombre' => '2DO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 14,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  22 => 
  array (
    'id' => 62,
    'nombre' => '4TO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 16,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  23 => 
  array (
    'id' => 63,
    'nombre' => '5TO  SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 17,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  24 => 
  array (
    'id' => 64,
    'nombre' => '6TO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 18,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  25 => 
  array (
    'id' => 65,
    'nombre' => '7MO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 19,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  26 => 
  array (
    'id' => 66,
    'nombre' => '8VO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 20,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  27 => 
  array (
    'id' => 77,
    'nombre' => '7MO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 19,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  28 => 
  array (
    'id' => 78,
    'nombre' => '8VO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 20,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  29 => 
  array (
    'id' => 79,
    'nombre' => '9NO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 21,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  30 => 
  array (
    'id' => 80,
    'nombre' => '10MO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 22,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  31 => 
  array (
    'id' => 81,
    'nombre' => '11VO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 23,
    'modalidad_id' => 3,
    'es_final' => 0,
  ),
  32 => 
  array (
    'id' => 83,
    'nombre' => '1ER TERMINO',
    'estatus' => 'A',
    'consecutivo' => 13,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  33 => 
  array (
    'id' => 84,
    'nombre' => '2DO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 14,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  34 => 
  array (
    'id' => 85,
    'nombre' => '3ER TERMINO',
    'estatus' => 'A',
    'consecutivo' => 15,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  35 => 
  array (
    'id' => 86,
    'nombre' => '4TO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 16,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  36 => 
  array (
    'id' => 87,
    'nombre' => '5TO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 17,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  37 => 
  array (
    'id' => 88,
    'nombre' => '6TO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 18,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  38 => 
  array (
    'id' => 89,
    'nombre' => '7MO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 19,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  39 => 
  array (
    'id' => 90,
    'nombre' => '8VO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 20,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  40 => 
  array (
    'id' => 91,
    'nombre' => '9NO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 21,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  41 => 
  array (
    'id' => 92,
    'nombre' => '10MO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 22,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  42 => 
  array (
    'id' => 93,
    'nombre' => '11VO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 23,
    'modalidad_id' => 4,
    'es_final' => 0,
  ),
  43 => 
  array (
    'id' => 95,
    'nombre' => '1ER PERIODO',
    'estatus' => 'A',
    'consecutivo' => 13,
    'modalidad_id' => 5,
    'es_final' => 0,
  ),
  44 => 
  array (
    'id' => 96,
    'nombre' => '2DO PERIODO',
    'estatus' => 'A',
    'consecutivo' => 14,
    'modalidad_id' => 5,
    'es_final' => 0,
  ),
  45 => 
  array (
    'id' => 97,
    'nombre' => '3ER PERIODO',
    'estatus' => 'A',
    'consecutivo' => 15,
    'modalidad_id' => 5,
    'es_final' => 0,
  ),
  46 => 
  array (
    'id' => 98,
    'nombre' => '4TO PERIODO',
    'estatus' => 'A',
    'consecutivo' => 16,
    'modalidad_id' => 5,
    'es_final' => 0,
  ),
  47 => 
  array (
    'id' => 99,
    'nombre' => '5TO PERIODO',
    'estatus' => 'A',
    'consecutivo' => 17,
    'modalidad_id' => 5,
    'es_final' => 0,
  ),
  48 => 
  array (
    'id' => 61,
    'nombre' => '3ER SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 15,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  49 => 
  array (
    'id' => 67,
    'nombre' => '9NO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 21,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  50 => 
  array (
    'id' => 68,
    'nombre' => '10MO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 22,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  51 => 
  array (
    'id' => 69,
    'nombre' => '11VO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 23,
    'modalidad_id' => 2,
    'es_final' => 0,
  ),
  52 => 
  array (
    'id' => 18,
    'nombre' => '6TO GRADO',
    'estatus' => 'A',
    'consecutivo' => 12,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  53 => 
  array (
    'id' => 17,
    'nombre' => '6TO AÑO',
    'estatus' => 'A',
    'consecutivo' => 18,
    'modalidad_id' => 1,
    'es_final' => 1,
  ),
  54 => 
  array (
    'id' => 15,
    'nombre' => '5TO AÑO',
    'estatus' => 'A',
    'consecutivo' => 17,
    'modalidad_id' => 1,
    'es_final' => 1,
  ),
  55 => 
  array (
    'id' => 100,
    'nombre' => '6TO PERIODO',
    'estatus' => 'A',
    'consecutivo' => 18,
    'modalidad_id' => 5,
    'es_final' => 1,
  ),
  56 => 
  array (
    'id' => 94,
    'nombre' => '12VO TERMINO',
    'estatus' => 'A',
    'consecutivo' => 24,
    'modalidad_id' => 4,
    'es_final' => 1,
  ),
  57 => 
  array (
    'id' => 82,
    'nombre' => '12VO TRIMESTRE',
    'estatus' => 'A',
    'consecutivo' => 24,
    'modalidad_id' => 3,
    'es_final' => 1,
  ),
  58 => 
  array (
    'id' => 70,
    'nombre' => '12VO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => 24,
    'modalidad_id' => 2,
    'es_final' => 1,
  ),
  59 => 
  array (
    'id' => 20,
    'nombre' => 'MISION RIBAS I',
    'estatus' => 'A',
    'consecutivo' => 20,
    'modalidad_id' => 6,
    'es_final' => 0,
  ),
  60 => 
  array (
    'id' => 101,
    'nombre' => 'MISION RIBAS II',
    'estatus' => 'A',
    'consecutivo' => 21,
    'modalidad_id' => 6,
    'es_final' => 1,
  ),
  61 => 
  array (
    'id' => 102,
    'nombre' => '13VO SEMESTRE',
    'estatus' => 'A',
    'consecutivo' => NULL,
    'modalidad_id' => NULL,
    'es_final' => 0,
  ),
  62 => 
  array (
    'id' => 0,
    'nombre' => 'PREESCOLAR',
    'estatus' => 'A',
    'consecutivo' => 0,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
  63 => 
  array (
    'id' => 11,
    'nombre' => '3ER NIVEL',
    'estatus' => 'A',
    'consecutivo' => 6,
    'modalidad_id' => 1,
    'es_final' => 0,
  ),
)		; 

	}
}