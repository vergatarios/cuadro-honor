<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CSeccion extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 1,
    'nombre' => 'U',
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 7,
    'nombre' => 'F',
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 2,
    'nombre' => 'A',
    'estatus' => 'A',
  ),
  3 => 
  array (
    'id' => 3,
    'nombre' => 'B',
    'estatus' => 'A',
  ),
  4 => 
  array (
    'id' => 4,
    'nombre' => 'C',
    'estatus' => 'A',
  ),
  5 => 
  array (
    'id' => 5,
    'nombre' => 'D',
    'estatus' => 'A',
  ),
  6 => 
  array (
    'id' => 6,
    'nombre' => 'E',
    'estatus' => 'A',
  ),
  7 => 
  array (
    'id' => 8,
    'nombre' => 'G',
    'estatus' => 'A',
  ),
  8 => 
  array (
    'id' => 9,
    'nombre' => 'H',
    'estatus' => 'A',
  ),
  9 => 
  array (
    'id' => 10,
    'nombre' => 'I',
    'estatus' => 'A',
  ),
  10 => 
  array (
    'id' => 11,
    'nombre' => 'J',
    'estatus' => 'A',
  ),
  11 => 
  array (
    'id' => 12,
    'nombre' => 'K',
    'estatus' => 'A',
  ),
  12 => 
  array (
    'id' => 13,
    'nombre' => 'L',
    'estatus' => 'A',
  ),
  13 => 
  array (
    'id' => 14,
    'nombre' => 'M',
    'estatus' => 'A',
  ),
  14 => 
  array (
    'id' => 15,
    'nombre' => 'N',
    'estatus' => 'A',
  ),
  15 => 
  array (
    'id' => 16,
    'nombre' => 'O',
    'estatus' => 'A',
  ),
  16 => 
  array (
    'id' => 17,
    'nombre' => 'P',
    'estatus' => 'A',
  ),
  17 => 
  array (
    'id' => 18,
    'nombre' => 'Q',
    'estatus' => 'A',
  ),
  18 => 
  array (
    'id' => 19,
    'nombre' => 'R',
    'estatus' => 'A',
  ),
  19 => 
  array (
    'id' => 20,
    'nombre' => 'S',
    'estatus' => 'A',
  ),
  20 => 
  array (
    'id' => 21,
    'nombre' => 'T',
    'estatus' => 'A',
  ),
  21 => 
  array (
    'id' => 22,
    'nombre' => 'V',
    'estatus' => 'A',
  ),
  22 => 
  array (
    'id' => 23,
    'nombre' => 'W',
    'estatus' => 'A',
  ),
  23 => 
  array (
    'id' => 24,
    'nombre' => 'X',
    'estatus' => 'A',
  ),
  24 => 
  array (
    'id' => 25,
    'nombre' => 'Y',
    'estatus' => 'A',
  ),
  25 => 
  array (
    'id' => 26,
    'nombre' => 'Z',
    'estatus' => 'A',
  ),
)		; 

	}
}