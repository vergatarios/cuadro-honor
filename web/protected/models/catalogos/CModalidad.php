<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CModalidad extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 2,
    'nombre' => 'JÓVENES, ADULTOS Y ADULTAS',
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 1,
    'nombre' => 'SISTEMA REGULAR',
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 17,
    'nombre' => 'EDUCACIÓN MILITAR',
    'estatus' => 'A',
  ),
  3 => 
  array (
    'id' => 3,
    'nombre' => 'EDUCACIÓN ESPECIAL',
    'estatus' => 'A',
  ),
  4 => 
  array (
    'id' => 5,
    'nombre' => 'EDUCACIÓN EN FRONTERAS',
    'estatus' => 'A',
  ),
  5 => 
  array (
    'id' => 4,
    'nombre' => 'EDUCACIÓN RURAL',
    'estatus' => 'A',
  ),
  6 => 
  array (
    'id' => 7,
    'nombre' => 'EDUCACIÓN PARA LAS ARTES',
    'estatus' => 'A',
  ),
  7 => 
  array (
    'id' => 6,
    'nombre' => 'EDUCACIÓN INTERCULTURAL',
    'estatus' => 'A',
  ),
  8 => 
  array (
    'id' => 18,
    'nombre' => 'EDUCACIÓN INTERCULTURAL BILINGÜE',
    'estatus' => 'A',
  ),
  9 => 
  array (
    'id' => 19,
    'nombre' => 'TECNICO MEDIO EN ELECTRICIDAD',
    'estatus' => 'E',
  ),
  10 => 
  array (
    'id' => 9,
    'nombre' => 'TÉCNICA LABORAL',
    'estatus' => 'E',
  ),
  11 => 
  array (
    'id' => 8,
    'nombre' => 'TÉCNICA',
    'estatus' => 'E',
  ),
  12 => 
  array (
    'id' => 20,
    'nombre' => 'TECNICA',
    'estatus' => 'A',
  ),
  13 => 
  array (
    'id' => 21,
    'nombre' => 'TECNICA LABORAL',
    'estatus' => 'A',
  ),
  14 => 
  array (
    'id' => 22,
    'nombre' => 'EDUCACION MEDIA GENERAL ASISTENCIAL',
    'estatus' => 'A',
  ),
)		; 

	}
}