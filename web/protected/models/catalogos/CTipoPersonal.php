<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CTipoPersonal extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 4,
    'nombre' => 'DOCENTE',
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 5,
    'nombre' => 'COORDINADOR',
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 6,
    'nombre' => 'ENFERMERA',
    'estatus' => 'A',
  ),
  3 => 
  array (
    'id' => 1,
    'nombre' => 'ADMINISTRATIVO ',
    'estatus' => 'A',
  ),
  4 => 
  array (
    'id' => 7,
    'nombre' => 'MENSAJERO',
    'estatus' => 'A',
  ),
  5 => 
  array (
    'id' => 8,
    'nombre' => 'LOCUTOR',
    'estatus' => 'A',
  ),
  6 => 
  array (
    'id' => 2,
    'nombre' => 'OBRERO',
    'estatus' => 'A',
  ),
)		; 

	}
}