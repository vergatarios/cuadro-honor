<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CEspecialidad extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 4,
    'nombre' => 'SDASS',
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 5,
    'nombre' => 'MUSICA',
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 6,
    'nombre' => 'MATEMATICA',
    'estatus' => 'A',
  ),
  3 => 
  array (
    'id' => 8,
    'nombre' => 'CIENCIAS',
    'estatus' => 'A',
  ),
  4 => 
  array (
    'id' => 9,
    'nombre' => 'CIENCIA GENERICA',
    'estatus' => 'A',
  ),
  5 => 
  array (
    'id' => 10,
    'nombre' => 'CIENCIA GENERICA',
    'estatus' => 'A',
  ),
  6 => 
  array (
    'id' => 11,
    'nombre' => 'PREPARAS LAS FOTOS',
    'estatus' => 'A',
  ),
  7 => 
  array (
    'id' => 12,
    'nombre' => 'WWWWWWWW',
    'estatus' => 'E',
  ),
  8 => 
  array (
    'id' => 7,
    'nombre' => 'DIDID',
    'estatus' => 'E',
  ),
  9 => 
  array (
    'id' => 1,
    'nombre' => 'ESPECIALIDAD ADMIN',
    'estatus' => 'A',
  ),
  10 => 
  array (
    'id' => 2,
    'nombre' => 'ESPECIALIDAD OBR',
    'estatus' => 'A',
  ),
  11 => 
  array (
    'id' => 3,
    'nombre' => 'ESPECIALIDAD DOC',
    'estatus' => 'A',
  ),
)		; 

	}
}