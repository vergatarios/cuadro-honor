<?php

/**
 * This is the model class for table "gplantel.congreso_pedagogico".
 *
 * The followings are the available columns in table 'gplantel.congreso_pedagogico':
 * @property integer $id
 * @property integer $plantel_id
 * @property integer $periodo_id
 * @property integer $linea_investigacion_id
 * @property integer $ambito_investigacion_id
 * @property string $fecha_inicio
 * @property string $fecha_final
 * @property string $observacion
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Plantel $plantel
 * @property PeriodoEscolar $periodo
 */
class CongresoPedagogicoForm extends CFormModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.congreso_pedagogico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('plantel_id, periodo_id, linea_investigacion_id, ambito_investigacion_id, fecha_inicio, fecha_final, fecha_ini, usuario_ini_id', 'required'),
			array('plantel_id, periodo_id, linea_investigacion_id, ambito_investigacion_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>1),
			array('observacion, fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, plantel_id, periodo_id, linea_investigacion_id, ambito_investigacion_id, fecha_inicio, fecha_final, observacion, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
			'periodo' => array(self::BELONGS_TO, 'PeriodoEscolar', 'periodo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'plantel_id' => 'Plantel',
			'periodo_id' => 'Periodo',
			'linea_investigacion_id' => 'Linea Investigacion',
			'ambito_investigacion_id' => 'Ambito Investigacion',
			'fecha_inicio' => 'Fecha Inicio',
			'fecha_final' => 'Fecha Final',
			'observacion' => 'Observacion',
			'fecha_ini' => 'Fecha Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('plantel_id',$this->plantel_id);
		$criteria->compare('periodo_id',$this->periodo_id);
		$criteria->compare('linea_investigacion_id',$this->linea_investigacion_id);
		$criteria->compare('ambito_investigacion_id',$this->ambito_investigacion_id);
		$criteria->compare('fecha_inicio',$this->fecha_inicio,true);
		$criteria->compare('fecha_final',$this->fecha_final,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CongresoPedagogico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
