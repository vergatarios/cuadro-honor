<?php

/**
 * This is the model class for table "personal.personal_plantel_espacios".
 *
 * The followings are the available columns in table 'personal.personal_plantel_espacios':
 * @property string $id
 * @property string $personal_plantel_id
 * @property string $area_comun_plantel_id
 * @property integer $cantidad
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property string $fecha_act
 * @property integer $usuario_act_id
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property AreaComunPlantel $areaComunPlantel
 * @property PersonalPlantel $personalPlantel
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 */
class PersonalPlantelEspacios extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal.personal_plantel_espacios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('area_comun_plantel_id, cantidad', 'required','message' => 'El campo: {attribute}, no debe estar vacio', 'on'=>'gestionPersonalPlantelEspacios'),
			array('cantidad, personal_plantel_id, area_comun_plantel_id', 'numerical', 'integerOnly'=>true,'message' => 'El campo: {attribute}, debe ser un valor numérico', 'on'=>'gestionPersonalPlantelEspacios'),
			array('estatus', 'length', 'max'=>1),
			//array('fecha_ini, fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, personal_plantel_id, area_comun_plantel_id, cantidad, fecha_ini, usuario_ini_id, fecha_act, usuario_act_id, fecha_elim, estatus', 'safe', 'on'=>'search_espacios'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'areaComunPlantel' => array(self::BELONGS_TO, 'AreaComunPlantel', 'area_comun_plantel_id'),
			'personalPlantel' => array(self::BELONGS_TO, 'PersonalPlantel', 'personal_plantel_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Espacio',
			'personal_plantel_id' => 'Personal Plantel',
			'area_comun_plantel_id' => '&Aacute;rea de Ubicaci&oacute;n',
			'cantidad' => 'Cantidad',
			'fecha_ini' => 'Fecha Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_act' => 'Fecha Act',
			'usuario_act_id' => 'Usuario Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search_espacios($personal_plantel_id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		 $criteria=new CDbCriteria;
                
                 $criteria->compare('personal_plantel_id',$personal_plantel_id);

		//$criteria->compare('id',$this->id,true);
		//$criteria->compare('personal_plantel_id',$this->personal_plantel_id,true);
		//$criteria->compare('area_comun_plantel_id',$this->area_comun_plantel_id,true);
		//$criteria->compare('cantidad',$this->cantidad);
		//$criteria->compare('fecha_ini',$this->fecha_ini,true);
		//$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		//$criteria->compare('fecha_act',$this->fecha_act,true);
		//$criteria->compare('usuario_act_id',$this->usuario_act_id);
		//$criteria->compare('fecha_elim',$this->fecha_elim,true);
		//$criteria->compare('estatus',$this->estatus,true);

		$sort = new CSort();
                $sort->defaultOrder = 't.estatus ASC, t.id DESC';

                return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'sort' => $sort,
                ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PersonalPlantelEspacios the static model class
	 */
        
        
        public function getErroresAdicionales($erroresAdicionales)
        {  
            
            
                 $erroresAdicionales.="</br></br>".' <b>Nota: </b> En  caso de persistir el error o tiene alguna duda al respecto, por favor contacte al personal de soporte mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>'; 
                 $this->addError('erroresAdicionales',$erroresAdicionales);
                 
                     
            
        } // fin de la funcion para mostrar los errores adicionales generados despues del metodo validate
        
        
        
        public function getValidarPersonalPlantelEspacios($id,$personal_plantel_id,$area_comun_plantel_id,$pantalla)
        {
            $criteria = new CDbCriteria();
            
            if($pantalla=="registro")
            {
                $criteria->condition = " personal_plantel_id='$personal_plantel_id' and area_comun_plantel_id='$area_comun_plantel_id' and estatus='A' "; 
            }
            
            if($pantalla=="edicion")
            {
                $criteria->condition = "id <> '$id' and personal_plantel_id='$personal_plantel_id' and area_comun_plantel_id='$area_comun_plantel_id' and estatus='A'  "; 
            }
                
            $resultado = PersonalPlantelEspacios::model()->findAll($criteria);
            return $resultado;
        } // fin del metodo para buscar los datos del personal, esquema personal 
        
        
        
        public function getEspacios($plantel_id)
        {
  
  
            $resultado = array();

            $sql = "select ac.nombre,acp.id,acp.cantidad,acp.plantel_id from gplantel.area_comun_plantel acp
                            inner join gplantel.area_comun ac on acp.area_comun_id=ac.id
                            where acp.plantel_id='$plantel_id' and acp.estatus='A'";      


            $resultado= $this->getDbConnection()->createCommand($sql)->queryAll();

             foreach ($resultado as $key => $value) 
             {
                   $resultado[$value['id']] = array(
                       'id' => $value['id'],
                       'nombre' => $value['nombre']
                   );
             }

       
        return $resultado; 
           
            
        } // fin del metodo para buscar los datos del area comun del plantel
        
        
        public function getCantidadEspacios($area_comun_plantel_id)
        {
            $criteria = new CDbCriteria();           
            $criteria->condition = " id='$area_comun_plantel_id' "; 
            $resultado = AreaComunPlantel::model()->findAll($criteria);
            return $resultado;
        } // fin del metodo para buscar la cantidad de espacios en funcion del plantel
        
        
         public function getNombreEspacio($area_comun_plantel_id)
         {
            
  
            $resultado = array();

            $sql = "select ac.nombre,acp.id,acp.cantidad,acp.plantel_id 
                            from gplantel.area_comun_plantel acp
                            inner join gplantel.area_comun ac on acp.area_comun_id=ac.id
                            inner join personal.personal_plantel_espacios ppe on acp.id=ppe.area_comun_plantel_id
                            where 
                            ppe.area_comun_plantel_id='$area_comun_plantel_id' limit 1";     
            $resultado= $this->getDbConnection()->createCommand($sql)->queryAll();        

            return $resultado; 
           
            
        } // fin del metodo para buscar los datos del area comun del plantel
        
        
        
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}


?>


