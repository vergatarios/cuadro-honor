<?php

/**
 * This is the model class for table "matricula.tipo_evaluacion".
 *
 * The followings are the available columns in table 'matricula.tipo_evaluacion':
 * @property integer $id
 * @property string $nombre
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 */
class TipoEvaluacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'matricula.tipo_evaluacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, usuario_ini_id, fecha_ini', 'required'),
			array('usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>180),
                        array('nombre', 'unique'),
			array('fecha_act, fecha_elim', 'safe'),
			array('estatus', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_ini' => 'Fecha Ini',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
                if(is_numeric($this->id)){
		$criteria->compare('id',$this->id);
                }

                $criteria->compare('nombre', strtoupper($this->nombre), true);
                
                if(is_numeric($this->usuario_ini_id)){
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
                }
                
                if(is_numeric($this->usuario_act_id)){
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
                }
                
                if(strlen($this->fecha_ini)>0 && Utiles::dateCheck($this->fecha_ini)){
                    //$criteria->compare('fecha_ini',$this->fecha_ini,true);
                    $criteria->addCondition("TO_CHAR(t.fecha_ini,'DD-MM-YYYY') = :fecha_ini");
                    $criteria->params = array(':fecha_ini'=>$this->fecha_ini);
                }
                
                if(strlen($this->fecha_act)>0 && Utiles::dateCheck($this->fecha_act)){
                    //$criteria->compare('fecha_act',$this->fecha_act,true);
                    $criteria->addCondition("TO_CHAR(t.fecha_act,'DD-MM-YYYY') = :fecha_act");
                    $criteria->params =  array_merge($criteria->params, array(':fecha_act'=>$this->fecha_act));
                }
                
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		
                if(in_array($this->estatus,array('A','I','E'))){
                $criteria->compare('estatus',$this->estatus);
                }
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TipoEvaluacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
