<?php

/**
 * This is the model class for table "titulo.registro_programado_serial".
 *
 * The followings are the available columns in table 'titulo.registro_programado_serial':
 * @property integer $id
 * @property string $serial_inicial
 * @property string $serial_final
 * @property integer $zona_educativa_id
 * @property string $prefijo
 * @property string $checksum
 * @property string $observacion
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property string $fecha_carga
 * @property integer $usuario_eli_id
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property ZonaEducativa $zonaEducativa
 * @property UsergroupsUser $usuarioEli
 * @property UsergroupsUser $usuarioIni
 */
class RegistroProgramadoSerial extends CActiveRecord
{

    public $username_ini;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'titulo.registro_programado_serial';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('serial_inicial, serial_final, usuario_ini_id, direccion_ip, zona_educativa_id', 'required'),
            array('usuario_ini_id, usuario_act_id, usuario_eli_id, serial_inicial, serial_final, zona_educativa_id', 'numerical', 'integerOnly'=>true),
            array('serial_inicial, serial_final', 'length', 'max'=>20),
            array('prefijo', 'length', 'max'=>2),
            array('checksum', 'length', 'max'=>255),
            array('estatus', 'length', 'max'=>1),
            array('observacion, fecha_ini, fecha_carga, fecha_elim, fecha_act', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, serial_inicial, serial_final, checksum, observacion, usuario_ini_id, fecha_ini, zona_educativa_id, usuario_act_id, fecha_act, fecha_carga, usuario_eli_id, fecha_elim, estatus', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'zonaEducativa' => array(self::BELONGS_TO, 'ZonaEducativa', 'zona_educativa_id'),
            'usuarioEli' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_eli_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'prefijo' => 'Prefijo',
            'serial_inicial' => 'Serial Inicial',
            'serial_final' => 'Serial Final',
            'checksum' => 'Checksum',
            'observacion' => 'Observacion',
            'zona_educativa_id' => 'Zona Educativa',
            'usuario_ini_id' => 'Usuario que efectúa la petición del registro del rango de seriales',
            'fecha_ini' => 'Fecha en la que se hace la petición de Registro de un rango de seriales.',
            'fecha_carga' => 'Fecha y Hora en la que se ejecuta el registro de este rango de seriales',
            'usuario_eli_id' => 'Usuario Eli',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;


        $criteria->with = array(
            'usuarioIni' => array('alias' => 'ui'),
            'zonaEducativa' => array('alias' => 'z'),
        );

        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.prefijo',$this->prefijo,true);
        $criteria->compare('t.serial_inicial',$this->serial_inicial,true);
        $criteria->compare('t.serial_final',$this->serial_final,true);
        $criteria->compare('t.checksum',$this->checksum,true);
        $criteria->compare('t.observacion',$this->observacion,true);
        $criteria->compare('t.usuario_ini_id',$this->usuario_ini_id);
        //$criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('t.usuario_act_id',$this->usuario_act_id);
        //$criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('t.fecha_carga',$this->fecha_carga,true);
        $criteria->compare('t.usuario_eli_id',$this->usuario_eli_id);
        $criteria->compare('t.fecha_elim',$this->fecha_elim,true);
        $criteria->compare('t.estatus',$this->estatus,true);
        
        if(is_numeric($this->zona_educativa_id)){
           $criteria->compare('t.zona_educativa_id', $this->zona_educativa_id);
        }

        if (strlen($this->username_ini) > 0) {
            //$criteria->addSearchCondition("ui.username", $this->username_ini, false, 'AND', 'ILIKE');
            $criteria->addCondition("ui.username ILIKE :username_ini");
            $criteria->params = array_merge($criteria->params, array(':username_ini' => $this->username_ini));
        }

        if (strlen($this->fecha_ini) > 0 && Utiles::dateCheck($this->fecha_ini)) {
            $this->fecha_ini = Utiles::transformDate($this->fecha_ini);
            $criteria->addSearchCondition("TO_CHAR(t.fecha_ini, 'YYYY-MM-DD')", $this->fecha_ini, false, 'AND', '=');
        } else {
            $this->fecha_ini = '';
        }

        $sort = new CSort();
        $sort->defaultOrder = ' t.estatus ASC,t.fecha_act DESC, t.fecha_ini DESC ';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
        ));

    }
    
    public function registroProgramado(){

        $sql = 'SELECT titulo.registro_programado_seriales() AS resultado;';

        $query = Yii::app()->db->createCommand($sql);
        
        $queryResponse = $query->queryRow();
        
        return $queryResponse['resultado'];

    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RegistroProgramadoSerial the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
