<?php

/**
 * This is the model class for table "personal.especificacion_estatus".
 *
 * The followings are the available columns in table 'personal.especificacion_estatus':
 * @property integer $id
 * @property string $nombre
 * @property integer $estatus_docente_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Docente[] $docentes
 * @property UsergroupsUser $usuarioAct
 * @property EstatusDocente $estatusDocente
 * @property UsergroupsUser $usuarioIni
 */
class EspecificacionEstatus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal.especificacion_estatus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, estatus_docente_id, fecha_ini, usuario_ini_id', 'required'),
			array('estatus_docente_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>40),
                        array('nombre','unique'),
			array('estatus', 'length', 'max'=>1),
			array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, estatus_docente_id, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'docentes' => array(self::HAS_MANY, 'Docente', 'especificación_estatus_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'estatusDocente' => array(self::BELONGS_TO, 'EstatusDocente', 'estatus_docente_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'estatus_docente_id' => 'Estatus del Docente',
			'fecha_ini' => 'Fecha Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)){
                    $criteria->compare('id',$this->id);
                }
//		$criteria->compare('nombre',$this->nombre,true);
                $criteria->compare('nombre', strtoupper($this->nombre), true);
		$criteria->compare('estatus_docente_id',$this->estatus_docente_id);
                
		 if(strlen($this->fecha_ini)>0 && Utiles::dateCheck($this->fecha_ini)){
                    //$criteria->compare('fecha_ini',$this->fecha_ini,true);
                    $criteria->addCondition("TO_CHAR(t.fecha_ini,'DD-MM-YYYY') = :fecha_ini");
                    $criteria->params = array(':fecha_ini'=>$this->fecha_ini);
                }
                
		if(is_numeric($this->usuario_ini_id)){
                    $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
                }
		if(is_numeric($this->usuario_act_id)){
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
                }
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
                
		 if(in_array($this->estatus,array('A','I','E',))){
                    $criteria->compare('estatus',$this->estatus);
                }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EspecificacionEstatus the static model class
	 */
        
        
        
        
        public function getEspecificacionEstatusFiltro($estatus_docente_id)
        {
            $criteria=new CDbCriteria;
            $criteria->select='id, nombre,estatus_docente_id';
            $criteria->condition="estatus_docente_id=:estatus_docente_id and estatus='A' ";
            $criteria->params=array(':estatus_docente_id'=>$estatus_docente_id);
            $resultado = EspecificacionEstatus::model()->findAll($criteria);
            return $resultado;
        } // funcion para obtener la lista de elementos filtrados a mostrar
        
        
        
        
        
        
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
