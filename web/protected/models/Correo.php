<?php

/**
 * This is the model class for table "legacy.correo".
 *
 * The followings are the available columns in table 'legacy.correo':
 * @property integer $id
 * @property string $zona
 * @property string $nombre_drcee_zona
 * @property string $apellido_drcee_zona
 * @property string $correo_drcee_zona
 * @property string $telefono_drcee_zona
 * @property string $nombre_director
 * @property string $apellido_director
 * @property string $correo_director_actualizado
 * @property string $usuario_director
 * @property string $clave_activacion_director
 * @property string $url_corto
 * @property string $url_largo
 * @property string $sexo
 * @property string $estatus
 * @property integer $estado_id
 */
class Correo extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'legacy.correo_directores';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('estado_id', 'numerical', 'integerOnly' => true),
            array('sexo, estatus', 'length', 'max' => 1),
            array('zona, nombre_drcee_zona, apellido_drcee_zona, correo_drcee_zona, telefono_drcee_zona, nombre_director, apellido_director, correo_director_actualizado, usuario_director, clave_activacion_director, url_corto, url_largo', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, zona, nombre_drcee_zona, apellido_drcee_zona, correo_drcee_zona, telefono_drcee_zona, nombre_director, apellido_director, correo_director_actualizado, usuario_director, clave_activacion_director, url_corto, url_largo, sexo, estatus, estado_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'zona' => 'Zona',
            'nombre_drcee_zona' => 'Nombre Drcee Zona',
            'apellido_drcee_zona' => 'Apellido Drcee Zona',
            'correo_drcee_zona' => 'Correo Drcee Zona',
            'telefono_drcee_zona' => 'Telefono Drcee Zona',
            'nombre_director' => 'Nombre Director',
            'apellido_director' => 'Apellido Director',
            'correo_director_actualizado' => 'Correo Director Actualizado',
            'usuario_director' => 'Usuario Director',
            'clave_activacion_director' => 'Clave Activacion Director',
            'url_corto' => 'Url Corto',
            'url_largo' => 'Url Largo',
            'sexo' => 'Sexo',
            'estatus' => 'Estatus',
            'estado_id' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('zona', $this->zona, true);
        $criteria->compare('nombre_drcee_zona', $this->nombre_drcee_zona, true);
        $criteria->compare('apellido_drcee_zona', $this->apellido_drcee_zona, true);
        $criteria->compare('correo_drcee_zona', $this->correo_drcee_zona, true);
        $criteria->compare('telefono_drcee_zona', $this->telefono_drcee_zona, true);
        $criteria->compare('nombre_director', $this->nombre_director, true);
        $criteria->compare('apellido_director', $this->apellido_director, true);
        $criteria->compare('correo_director_actualizado', $this->correo_director_actualizado, true);
        $criteria->compare('usuario_director', $this->usuario_director, true);
        $criteria->compare('clave_activacion_director', $this->clave_activacion_director, true);
        $criteria->compare('url_corto', $this->url_corto, true);
        $criteria->compare('url_largo', $this->url_largo, true);
        $criteria->compare('sexo', $this->sexo, true);
        $criteria->compare('estatus', $this->estatus, true);
        $criteria->compare('estado_id', $this->estado_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function countPersonasEstado($estado_id) {
        $resultado = null;
        if (is_numeric($estado_id)) {
            $sql = "SELECT COUNT(id) FROM legacy.correo_directores "
                    . "WHERE estado_id = :estado_id  AND estatus is null LIMIT 1;";

            $buqueda = Yii::app()->db->createCommand($sql);
            $buqueda->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
            $resultado = $buqueda->queryScalar();
        }
        return $resultado;
    }

    public function getDatosPersonasEstado($estado_id) {
        $resultado = null;
        if (is_numeric($estado_id)) {

            $sql = "SELECT * FROM legacy.correo_directores "
                    . "WHERE estado_id = :estado_id AND estatus is null";

            $buqueda = Yii::app()->db->createCommand($sql);
            $buqueda->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
            $resultado = $buqueda->queryAll();
        }
        return $resultado;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Correo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
