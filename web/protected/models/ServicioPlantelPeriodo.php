<?php

/**
 * This is the model class for table "gplantel.servicio_plantel_periodo".
 *
 * The followings are the available columns in table 'gplantel.servicio_plantel_periodo':
 * @property integer $id
 * @property integer $plantel_id
 * @property integer $servicio_id
 * @property integer $condicion_servicio_id
 * @property integer $cantidad
 * @property integer $periodo_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property CondicionServicio $condicionServicio
 * @property Plantel $plantel
 * @property Servicio $servicio
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 */
class ServicioPlantelPeriodo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.servicio_plantel_periodo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('plantel_id, servicio_id, condicion_servicio_id, cantidad', 'numerical', 'integerOnly'=>true),
			array('fecha_ini, fecha_act, fecha_elim', 'length', 'max'=>6),
			array('estatus', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, plantel_id, servicio_id, condicion_servicio_id, cantidad, periodo_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'condicionServicio' => array(self::BELONGS_TO, 'CondicionServicio', 'condicion_servicio_id'),
			'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
			'servicio' => array(self::BELONGS_TO, 'Servicio', 'servicio_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'plantel_id' => 'Plantel',
			'servicio_id' => 'Servicio',
			'condicion_servicio_id' => 'Condicion Servicio',
			'cantidad' => 'Cantidad',
			'periodo_id' => 'Periodo',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('plantel_id',$this->plantel_id);
		$criteria->compare('servicio_id',$this->servicio_id);
		$criteria->compare('condicion_servicio_id',$this->condicion_servicio_id);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('periodo_id',$this->periodo_id);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ServicioPlantelPeriodo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function buscarColeccionBicentenaria($servicio, $periodo_Escolar_id, $plantel_id) {

        $sql = "SELECT count(id)
  FROM gplantel.servicio_plantel_periodo
  WHERE plantel_id=:plantel_id and periodo_id=:periodo_id  and servicio_id=:servicio_id
  ;
";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":servicio_id", $servicio, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_id", $periodo_Escolar_id, PDO::PARAM_INT);
        $resultado = $consulta->queryScalar();

        return $resultado;
        
    }

}
