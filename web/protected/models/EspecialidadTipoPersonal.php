<?php

/**
 * This is the model class for table "personal.especialidad_tipo_personal".
 *
 * The followings are the available columns in table 'personal.especialidad_tipo_personal':
 * @property integer $id
 * @property integer $especialidad_id
 * @property integer $tipo_personal_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property TipoPersonal $tipoPersonal
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 * @property Especialidad $especialidad
 */
class EspecialidadTipoPersonal extends CActiveRecord
{



    public $especialidad_nom;
    public $tipo_personal_nom;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'personal.especialidad_tipo_personal';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('especialidad_id, tipo_personal_id, usuario_ini_id,estatus', 'required'),
            array('especialidad_id, tipo_personal_id', 'ECompositeUniqueValidator', 'attributesToAddError'=>'tipo_personal_id', 'message'=>'Ya se Encuentra Asignada la Especialidad al Tipo de Personal .'),
            array('especialidad_id, tipo_personal_id, usuario_ini_id, usuario_act_id, usuario_elim_id', 'numerical', 'integerOnly'=>true),
            array('estatus', 'length', 'max'=>1),
            array('fecha_ini, fecha_act, fecha_elim', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, especialidad_id, tipo_personal_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tipoPersonal' => array(self::BELONGS_TO, 'TipoPersonal', 'tipo_personal_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioElim' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_elim_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'especialidad' => array(self::BELONGS_TO, 'Especialidad', 'especialidad_id'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'especialidad_id' => 'Especialidad',
            'tipo_personal_id' => 'Tipo Personal',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'usuario_elim_id' => 'Usuario Elim',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
        );
    }


    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
        $criteria->alias='etp';
        $criteria->with=array(
            'tipoPersonal'=>array('alias'=>'tp'),
            'especialidad'=>array('alias'=>'e')
        );
        $criteria->addSearchCondition('tp.nombre', '%' . $this->tipo_personal_nom . '%', false, 'AND', 'ILIKE');
        $criteria->addSearchCondition('e.nombre', '%' . $this->especialidad_nom . '%', false, 'AND', 'ILIKE');
        /*$criteria->compare('id',$this->id);
        $criteria->compare('especialidad_id',$this->especialidad_id);
        $criteria->compare('tipo_personal_id',$this->tipo_personal_id);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('usuario_act_id',$this->usuario_act_id);
        $criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('usuario_elim_id',$this->usuario_elim_id);
        $criteria->compare('fecha_elim',$this->fecha_elim,true);*/
        $criteria->compare('etp.estatus',$this->estatus,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

  
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
