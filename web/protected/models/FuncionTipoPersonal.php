<?php

/**
 * This is the model class for table "personal.funcion_tipo_personal".
 *
 * The followings are the available columns in table 'personal.funcion_tipo_personal':
 * @property integer $id
 * @property integer $funcion_id
 * @property integer $tipo_personal_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UserGroupsUser $usuarioIni
 * @property UserGroupsUser $usuarioElim
 * @property UserGroupsUser $usuarioAct
 * @property Funcion $funcion
 * @property TipoPersonal $tipoPersonal
 */
class FuncionTipoPersonal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal.funcion_tipo_personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('funcion_id, tipo_personal_id', 'ECompositeUniqueValidator', 'attributesToAddError'=>'tipo_personal_id', 'message'=>'Ya se Encuentra Asignada la Función .'),
			array('funcion_id, tipo_personal_id, usuario_ini_id', 'required'),
			array('funcion_id, tipo_personal_id, usuario_ini_id, usuario_act_id, usuario_elim_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>1),
			array('fecha_ini, fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, funcion_id, tipo_personal_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioIni' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_ini_id'),
			'usuarioElim' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_elim_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_act_id'),
			'funcion' => array(self::BELONGS_TO, 'Funcion', 'funcion_id'),
			'tipoPersonal' => array(self::BELONGS_TO, 'TipoPersonal', 'tipo_personal_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'funcion_id' => 'Funcion',
			'tipo_personal_id' => 'Tipo Personal',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'usuario_elim_id' => 'Usuario Elim',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.tipo_personal_id',$this->tipo_personal_id);
		$criteria->compare('t.usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('t.fecha_ini',$this->fecha_ini,true);
		$criteria->compare('t.usuario_act_id',$this->usuario_act_id);
		$criteria->compare('t.fecha_act',$this->fecha_act,true);
		$criteria->compare('t.usuario_elim_id',$this->usuario_elim_id);
		$criteria->compare('t.fecha_elim',$this->fecha_elim,true);
		$criteria->compare('t.estatus',$this->estatus,true);
        $criteria->order='t.estatus ASC, f.nombre ASC';


        $criteria->with = array('funcion'=>array('alias'=>'f', 'select'=>'f.id, f.nombre'));

        if(isset($this->funcion_id) AND is_string($this->funcion_id)){
            $criteria->addSearchCondition('f.nombre','%'.addslashes($this->funcion_id).'%',false,'AND','ILIKE');
        }


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FuncionTipoPersonal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
