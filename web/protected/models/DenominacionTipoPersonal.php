<?php

/**
 * This is the model class for table "personal.denominacion_tipo_personal".
 *
 * The followings are the available columns in table 'personal.denominacion_tipo_personal':
 * @property integer $id
 * @property integer $denominacion_id
 * @property integer $tipo_personal_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Denominacion $denominacion
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioAct
 * @property TipoPersonal $tipoPersonal
 */
class DenominacionTipoPersonal extends CActiveRecord
{
	public $denominacion_nom;
	public $tipoPersonal_nom;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal.denominacion_tipo_personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('denominacion_id, tipo_personal_id, usuario_ini_id', 'required'),
			array('denominacion_id, tipo_personal_id', 'ECompositeUniqueValidator', 'attributesToAddError'=>'denominacion_id', 'message'=>'Ya se Encuentra Asignada la Denominación al Tipo de Personal seleccionado .'),
			array('denominacion_id, tipo_personal_id, usuario_ini_id, usuario_act_id, usuario_elim_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>1),
			array('fecha_ini, fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, denominacion_id, tipo_personal_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'denominacion' => array(self::BELONGS_TO, 'DenominacionPersonal', 'denominacion_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_ini_id'),
			'usuarioElim' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_elim_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_act_id'),
			'tipoPersonal' => array(self::BELONGS_TO, 'TipoPersonal', 'tipo_personal_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'denominacion_id' => 'Denominacion',
			'tipo_personal_id' => 'Tipo de Personal',
			'usuario_ini_id' => 'Creado Por',
			'fecha_ini' => 'Fecha de Creacion',
			'usuario_act_id' => 'Actualizado Por',
			'fecha_act' => 'Fecha de Actualizacion',
			'usuario_elim_id' => 'Eliminado Por',
			'fecha_elim' => 'Fecha de Eliminacion',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias='t';
		$criteria->with=array(
			'tipoPersonal'=>array('alias'=>'tp'),
			'denominacion'=>array('alias'=>'d')

		);
		/*$criteria->compare('id',$this->id);
		$criteria->compare('denominacion_id',$this->denominacion_id);
		$criteria->compare('tipo_personal_id',$this->tipo_personal_id);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('usuario_elim_id',$this->usuario_elim_id);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);*/
		if(isset($this->tipoPersonal_nom)){
			$criteria->addSearchCondition('tp.nombre', '%' . $this->tipoPersonal_nom . '%', false, 'AND', 'ILIKE');
		}
		if(isset($this->denominacion_nom)){
			$criteria->addSearchCondition('d.nombre', '%' . $this->denominacion_nom . '%', false, 'AND', 'ILIKE');
		}
		$criteria->compare('t.estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DenominacionTipoPersonal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
