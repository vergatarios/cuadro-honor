CREATE OR REPLACE FUNCTION gplantel.get_id_periodo_by_anio_fin(anio_fin_vi INTEGER)
RETURNS INTEGER
AS
$BODY$
DECLARE

    cant_datos_exists_v INTEGER := 0; -- Indicara la Cantidad de Veces que un dato es encontrado en la tabla (Por regla debe aparecer maximo una vez).
    resultado_v INTEGER;

BEGIN

    -- ESTA FUNCION PERMITE OBETENER EL ID DEL PERIODO ESCOLAR INGRESANDO EL ANIO FIN DE DICHO PERIODO ESCOLAR.
    -- @author
    -- @date 2015-01-13 03:36

    RAISE NOTICE 'ANIO INI: %', anio_fin_vi;

    RAISE NOTICE 'INICIO: Proceso de Obtencion del Id del Periodo Escolar';

    SELECT COUNT(1) INTO cant_datos_exists_v FROM gplantel.periodo p WHERE p.anio_fin = anio_fin_vi;

    IF anio_fin_vi IS NOT NULL AND cant_datos_exists_v = 1 THEN
        SELECT p.id INTO resultado_v FROM gplantel.periodo p WHERE p.anio_fin = anio_fin_vi LIMIT 1;
    END IF;

    RETURN resultado_v;

EXCEPTION

    WHEN OTHERS THEN
        RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %)', SQLERRM, SQLSTATE;
    RETURN resultado_v;

END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
