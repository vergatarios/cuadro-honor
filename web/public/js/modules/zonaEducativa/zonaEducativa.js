

//-------------------------------------------------->DEPLEGAR FORMULARIO DE DETALLES 




//    var zona_id = $("#zona_id").val();
//    var data = {
//        id: id,
//        zona_id: zona_id
//    };
//     
//    $.ajax({
//        url: "eliminarAutoridad",
//        data: data,
//        dataType: 'html',
//        type: 'get',
//        success: function(resp) {
//            $("#_formAutoridades").html(resp);
//        }
//        
//    });



function eliminarAutoridad(id, zona_id) {
    displayDialogBox("#dialogPantalla", 'alert', '¿Estas Seguro que Desea Desvincular a esta Autoridad de la Zona Educativa?');
    var dialog = $("#dialogPantalla").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> Cargo </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i> Volver",
                "class": "btn btn-xs btn-orange",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i> Desvincular esta Autoridad",
                "class": "btn btn-danger btn-xs",
                id: 'btnEliminarAutoridadZona',
                click: function() {
                    var divResult = "#dialogPantalla";
                    var urlDir = "eliminarAutoridad";
                    var zona_id = $("#zona_id").val();
                    var datos = {id: id, zona_id: zona_id};
                    var conEfecto = true;
                    var showHTML = true;
                    var method = "POST";
                    var callback = function() {
                        $('#autoridades-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    };
                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                    //$(this).dialog("close");

                }
            }
        ],
    });


    Loading.hide();
}



function consultarZonaEducativa(id) {

    direccion = '/zonaEducativa/zonaEducativa/consultarZonaEducativa';

    title = 'Detalles de Zona Educativa';

    Loading.show();
    var data =
            {
                id: id
            };

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '700px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs ",
                        click: function() {
                            $(this).dialog("close");
                            refrescarGrid();
                        }
                    }
                ]
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

//-------------------------------------------->DESPLEGAR FORMULARIO UPDATE

function modificarZonaEducativa(id) {


    direccion = 'modificarZonaEducativa';

    title = 'Modificar Zona Educativa';
    Loading.show();
    var data = {id: id};
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                            refrescarGrid();
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            procesarCambio();
                            refrescarGrid();
                        }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}


//--------------------------------------------->desplegar pop-up para registrar

function registrarZonaEducativa() {

    direccion = 'create';
    title = 'Crear Nueva ZonaEducativa';
    Loading.show();

    //var data = {id: id};

    $.ajax({
        url: direccion,
        //data: data,
        dataType: 'html',
        type: 'get',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            crearZonaEducativa();
                            refrescarGrid();
                            // $("#nombre_zonaEducativa").val("");
                        }

                    }

                ],
            });
            $("#dialogPantalla").html(result);

        }
    });
    Loading.hide();
}



//-------------------------------------------------------->REGISTRAR 
function crearZonaEducativa()
{


    direccion = 'crear';

    var nombre = $('#nombre_zonaEducativa').val();

    var data = {nombre: nombre};
    //alert(data);
    executeAjax('error', direccion, data, false, true, 'POST', refrescarGrid);
    refrescarGrid();

}


//----------------------------------------------->>MODIFICAR NOMBRE  sin serialize
function procesarCambio()
{

    direccion = 'procesarCambio';

    var id = $('#id').val();
    var nombre = $('#nombre_zonaEducativa').val();

    var data = {ZonaEducativa: {id: id, nombre: nombre}};

    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);
    refrescarGrid();

}

function borrar(id) {


    direccion = 'eliminar';

    var dialog = $("#dialogEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar ZonaEducativa</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");

                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                    };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogEliminar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxReg").show("slow");
                    $("#dialogBoxHab").hide("slow");
                    refrescarGrid();
                }
            }
        ]
    });
}

//* .-------------------REACTIVACION .-------------------------------------------

function reactivar(id) {


    direccion = 'reactivar';

    var dialog = $("#dialogReactivar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Reactivar ZonaEducativa</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");

                }
            },
            {
                html: "<i class='icon-ok bigger-110'></i>&nbsp; Reactivar",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id,
                    };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogReactivar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxReg").hide("slow");
                    $("#dialogBoxHab").show("slow");
                }
            }
        ]
    });
}

//------------------------------------------------------------------------------

function refrescarGrid() {

    $('#zonaEducativa-grid').yiiGridView('update', {
        data: $(this).serialize()
    });

}
function cerrar_dialogo()


{

    Loading.show();

//               $("#dialogPantalla").dialog("close");
    window.location.reload();
    document.getElementById("guardo").style.display = "block";


    Loading.hide();


}



//------------------------funciones para MODIFICAR AUTORIDADES----------------->

function dialogo_error(mensaje) {
    $("#dialog_error p").html(mensaje);
    var dialog = $("#dialog_error").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Mensaje de Error</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function guardarAutoridad(id, autoridades) {

    $.ajax({
        url: "guardarAutoridad",
        data: {id: id,
            autoridades: autoridades},
        dataType: 'html',
        type: 'post',
        success: function(resp, resp2, resp3) {
            try {
                var json = jQuery.parseJSON(resp3.responseText);
                $("#dialog_cargo").dialog("close");
                dialogo_error(json.mensaje);
            } catch (e) {
                $("#_formAutoridades").html(resp);
                $("#guardoAutoridades").show();
            }

        }
    });
}

function CedulaFormat(vCedulaName, evento) {
    tecla = getkey(evento);
    vCedulaName.value = vCedulaName.value.toUpperCase();
    vCedulaValue = vCedulaName.value;
    valor = vCedulaValue.substring(2, 12);
    tam = vCedulaValue.length;
    var numeros = '0123456789/';
    var digit;
    var shift;
    var ctrl;
    var alt;
    var escribo = true;
    tam = vCedulaValue.length;

    if (shift && tam > 1) {
        return false;
    }
    for (var s = 0; s < valor.length; s++) {
        digit = valor.substr(s, 1);
        if (numeros.indexOf(digit) < 0) {
            noerror = false;
            break;
        }
    }
    if (escribo) {
        if (tecla == 8 || tecla == 37) {
            if (tam > 2)
                vCedulaName.value = vCedulaValue.substr(0, tam - 1);
            else
                vCedulaName.value = '';
            return false;
        }
        if (tam == 0 && tecla == 69) {
            vCedulaName.value = 'E-';
            return false;
        }
        if (tam == 0 && tecla == 86) {
            vCedulaName.value = 'V-';
            return false;
        }
        else if ((tam == 0 && !(tecla < 14 || tecla == 69 || tecla == 86 || tecla == 46)))
            return false;
        else if ((tam > 1) && !(tecla < 14 || tecla == 16 || tecla == 46 || tecla == 8 || (tecla >= 48 && tecla <= 57) || (tecla >= 96 && tecla <= 105)))
            return false;
    }
}


function buscarCedulaAutoridad(cedula) {
    var id = $("#zona_id").val();
    if (cedula != '' || cedula != null) {
        $.ajax({
            url: "buscarCedula",
            data: {cedula: cedula,
                id: id},
            dataType: 'json',
            type: 'get',
            success: function(resp) {
                if (resp.statusCode === "mensaje")
                    dialogo_error(resp.mensaje);
                if (resp.statusCode === "successC")
                    mostrarBusquedasCedula(id, resp.autoridades);
                // agregarCargo
                if (resp.statusCode === "successU")
                    mostrarDialog(resp.nombre, resp.apellido, resp.usuario);
                // agregarUsuario

            }

        });
    }
}

function mostrarBusquedasCedula(id, autoridades,cargo) {
    var cedula_value = $("#cedula").val();
    var cedula = cedula_value.substring(2, 10);
    if (cedula != '') {
        //$("#dialog_cargo").removeClass('hide');
        var dialog = $("#dialog_cargo").removeClass('hide').dialog({
            modal: true,
            width: '500px',
            dragable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Cargo a Asignar</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs",
                    click: function() {
                        $("#cargo_id_c").val('');
                        $(this).dialog("close");
                    }
                },
                {
                    html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                    "class": "btn btn-primary btn-xs",
                    click: function() {
                        

                        //  alert($("#cargo_id").val());
                        if ($("#cargo_id_c").val() !== '') {
                            var data = {
                                cedula: cedula,
                                zona_id: id,
                                cargo:  $("#cargo_id_c").val(),
                                        // key: key
                            };
                            // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                            //   executeAjax('_formAutoridades', 'agregarAutoridad', data, false, true, 'get', '');

                            $.ajax({
                                url: "agregarAutoridad",
                                data: data,
                                dataType: 'html',
                                type: 'post',
                                /* success: function(resp) {
                                 //   alert(resp);
                                 console.log(resp);
                                 //   $("#cargo_id").val('');
                                 //  $("#dialog_cargo").dialog("close");
                                 $("#_formAutoridades").html(resp);
                                 //  document.getElementById("botones").style.display = "block";
                                 //  document.getElementById("servicioCalid").setAttribute("class", "widget-box");
                                 }
                                 */
                                success: function(resp, resp2, resp3) {
                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);
                                        $("#dialog_cargo").dialog("close");
                                        dialogo_error(json.mensaje);
                                    } catch (e) {
                                        $("#cargo_id").val('');
                                        $("#dialog_cargo").dialog("close");
                                        $("#_formAutoridades").html(resp);
                                    }

                                }
                            });
                        }
                        else {
                            $("#autoridad_error").show();
                        }
                    }
                }
            ]
        });
        $("#dialog-autoridades").show();
        // $("#dialog_cargo").dialog("open");
    }
}

function mostrarDialog(nombre, apellido, usuario) {
    var cedula = $("#cedula").val();
    var zona_id = $("#zona_id").val();
    $("#UserGroupsUser_nombre").val(nombre);
    $("#UserGroupsUser_apellido").val(apellido);
    $("#UserGroupsUser_cedula").val(cedula);
    $("#UserGroupsUser_username").val(usuario);
    $("#UserGroupsUser_cedula").attr('readOnly', true);
    $("#UserGroupsUser_username").attr('readOnly', true);
    var dialogAutoridad = $("#agregarAutoridad").removeClass('hide').dialog({
        modal: true,
        width: '850px',
        height: '520',
        dragable: false,
        resizable: true,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Vincular Autoridad a la Zona Educativa</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-xs",
                click: function() {
                    $("#cargo_id").val('');
                    $("#UserGroupsUser_cedula").val('');
                    $("#UserGroupsUser_username").val('');
                    $("#UserGroupsUser_nombre").val('');
                    $("#UserGroupsUser_apellido").val('');
                    $("#UserGroupsUser_email").val('');
                    $('#UserGroupsUser_telefono').val('');
                    $("#errorSummaryA p").html('');
                    $("#errorSummaryA").hide();
                    dialogAutoridad.dialog("close");

                }
            },
            {
                html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                "class": "btn btn-primary btn-xs",
                click: function() {
                    var cargo = $.trim($("#cargo_id").val());
                    var email = $.trim($("#UserGroupsUser_email").val());
                    var telefono_fijo = $.trim($("#UserGroupsUser_telefono").val());
                    var telefono_celular = $.trim($("#UserGroupsUser_telefono_celular").val());
                    var nombreC = $.trim($("#UserGroupsUser_nombre").val());
                    var apellido = $.trim($("#UserGroupsUser_apellido").val());
                    if (cargo == "" || nombre == "" || email == "" || telefono_fijo == "" || telefono_celular == "" || nombreC == '' || apellido == '') {
                        displayDialogBox('validacionesA', 'error', 'DATOS FALTANTES: Los campos Cargo, Email, Telefono Fijo, Nombre, Apellido y Telefono Celular no pueden estar vacios.');
                    } 
                    else {
                        $.ajax({
                            url: "guardarNuevaAutoridad",
                            data: $("#zonaAgregarAutoridad-form").serialize(),
                            dataType: 'html',
                            type: 'post',
                            success: function(resp, resp1, resp3) {                                
                                try {
                                    var json = jQuery.parseJSON(resp);
                                    displayDialogBox('validacionesA', 'error', json.mensaje);
                                } catch (e) {
                                    $("#errorSummaryA p").html('');
                                    $("#errorSummaryA").hide();
                                    $("#cargo_id").val('');
                                    $("#UserGroupsUser_cedula").val('');
                                    $("#UserGroupsUser_username").val('');
                                    $("#UserGroupsUser_nombre").val('');
                                    $("#UserGroupsUser_apellido").val('');
                                    $("#UserGroupsUser_email").val('');
                                    $('#UserGroupsUser_telefono').val('');
                                    dialogAutoridad.dialog("close");

                                    /* Nuevo Dialogo Confirmar Registro NUEVO */
                                    $("#dialog_success p").html("Usuario Registrado exitosamente, está pendiente por su activaci&oacute;n en el sistema");
                                    var dialog_success = $("#dialog_success").removeClass('hide').dialog({
                                        modal: true,
                                        width: '450px',
                                        dragable: false,
                                        resizable: false,
                                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Registro Exitoso</h4></div>",
                                        title_html: true,
                                        buttons: [
                                            {
                                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                                                "class": "btn btn-xs",
                                                click: function() {
                                                    $("#dialog_success p").html('');
                                                    dialog_success.dialog("close");
                                                }
                                            }
                                        ]
                                    });

                                    // renderizar cambios
                                    $("#_formAutoridades").html(resp);

                                }


                            }
                        });
                    }

                }
            }
        ]
    });
}



//$("#dialog_agregarAutoridad").show();

function getkey(e) {
    if (window.event) {

        shift = event.shiftKey;
        ctrl = event.ctrlKey;
        alt = event.altKey;
        return window.event.keyCode;
    }
    else if (e) {
        var valor = e.which;
        if (valor > 96 && valor < 123) {
            valor = valor - 32;
        }
        return valor;
    }
    else
        return null;
}
/* Se usan en Modificar Zona */


//------------------------------------------------------------------------------

$(document).ready(function() {

    $('#UserGroupsUser_cedula').bind('keyup blur', function() {
        keyText(this, true, true);
        clearField(this);
        //$('#UserGroupsUser_username').val($(this).val());
    });

    $('#ZonaEducativa_nombre').bind('keyup blur', function() {
        keyText(this, true, true);
        makeUpper(this);
    });
    $("#zona-educativa-form").submit(function(evt) {
        evt.preventDefault();
        $.ajax({
            url: "actualizarDatosGenerales",
            data: $("#zona-educativa-form").serialize(),
            dataType: 'html',
            type: 'post',
            success: function(resp) {
                if (isNaN(resp)) {
                    //document.getElementById("resultado").style.display = "none";
                    document.getElementById("resultadoZona").style.display = "block";
                    $("html, body").animate({scrollTop: 0}, "fast");
                    //alert(resp);
                    $("#resultadoZona").html(resp);

                } else {
                    //document.getElementById("resultado").style.display = "none";
                    //   document.getElementById("success").setAttribute("class","successDialogBox");
                    document.getElementById("guardo").style.display = "block";
                }

            }
        });
    });


    $("#Zona_direccion").bind('keyup blur', function() {
        keyText(this, false);
    });

    $("#correo").bind('keyup blur', function() {
        keyEmail(this, false);
        makeLower(this);
        isValidEmail(this);
    });

    $("#nombre").bind('keyup blur', function() {
        keyAlpha(this, false);
        makeUpper(this);
    });




});

