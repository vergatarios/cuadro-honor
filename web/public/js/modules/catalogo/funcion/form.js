/**
 * Created by ignacio on 23/09/14.
 */
$(document).ready(function(){
    $('#Funcion_nombre').unbind('keyup');
    $('#Funcion_nombre').bind('keyup', function() {
        keyAlphaNum(this,true,false);
        makeUpper(this);
    });
    $('#Funcion_nombre').unbind('blur');
    $('#Funcion_nombre').bind('blur', function() {
        clearField(this);
    });
});