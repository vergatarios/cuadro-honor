$(document).ready(function() {

    $('#Aplicacion_fecha_ini').datepicker();
    $('#Aplicacion_fecha_act').datepicker();
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: 'dd-mm-yy',
        showOn: 'focus',
        showOtherMonths: false,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        minDate: new Date(1800, 1, 1),
        maxDate: 'today'
    });
    
    $('#Aplicacion_nombre').bind('keyup blur', function() {
        keyText(this, true);
    });

    $('#Aplicacion_nombre').on('blur', function() {
        clearField(this);
    });
    
    $('#Aplicacion_fecha_act, #Aplicacion_fecha_ini').on('dblclick', function(){
        $(this).val('');
        $('#aplicacion-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
    });
  
});
function VentanaDialog(id, direccion, title, accion, datos) {
    accion = accion;
    Loading.show();
    var data =
            {
                id: id,
                datos: datos
            };
    if (accion == "create") {
        $.ajax({
            url: direccion,
            data: data,
            dataType: 'html',
            type: 'GET',
            success: function(result, action)
            {
                var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                    modal: true,
                    width: '1200px',                   
                    dragable: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class":"btn btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                            html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                            "class": "btn btn-primary",
                            /*executeAjax('_formEndogeno', '../../agregarProyecto', data, false, true, 'post', '');*/

                            click: function() {
                             
                                var divResult = "dialogPantalla";
                                var urlDir = "/catalogo/aplicacion/registro"//;?id=" + id;
                                //
                                var datos = $("#aplicacion-form").serialize();
                                var conEfecto = true;
                                var showHTML = true;
                                var method = "POST";
                                var callback = function() {
                                    $('#aplicacion-grid').yiiGridView('update', {
                                        data: $(this).serialize()
                                    });
                                };
                                  var Aplicacion_nombre = $.trim($('#Aplicacion_nombre').val());
                                    if(Aplicacion_nombre==" "){
                                                                         
                                       }else if (datos) {
                                        
                                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                                    }
                            $("html, body").animate({scrollTop: 0}, "fast");    
                            }                           
                        },
                    ],
                });
                $("#dialogPantalla").html(result);
                $('#dialogPantalla').dialog('option', 'position', 'center'); 
            }
        });
        Loading.hide();
    }
    else if (accion=="update"){
    $.ajax({
            url: direccion,
            data: data,
            dataType: 'html',
            type: 'GET',
            success: function(result, action)
            {
                var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                    modal: true,
                    width: '700px',
                    dragable: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class": "btn btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                            html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                            "class": "btn btn-primary",
                            /*executeAjax('_formEndogeno', '../../agregarProyecto', data, false, true, 'post', '');*/

                            click: function() {
                             
                                var divResult = "dialogPantalla";
                                var urlDir = "/catalogo/aplicacion/edicion?id=" + id;
                                //
                                var datos = $("#aplicacion-form").serialize();
                                var conEfecto = true;
                                var showHTML = true;
                                var method = "POST";
                                var callback = function() {
                                    $('#aplicacion-grid').yiiGridView('update', {
                                        data: $(this).serialize()
                                    });
                                };
                                  var Aplicacion_nombre = $.trim($('#Aplicacion_nombre').val());
                                    if(Aplicacion_nombre==""){
                                        
                                    }else if (datos) {
                                        
                                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                                    }
                                }
                            
                        },
                    ],
                });
                $("#dialogPantalla").html(result);
                $('#dialogPantalla').dialog('option', 'position', 'center');
            }
        });
        Loading.hide();
       }
    else if (accion == "view") {
        $.ajax({
            url: direccion,
            data: data,
            dataType: 'html',
            type: 'GET',
            success: function(result, action)
            {
                var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                    modal: true,
                    width: '700px',                                      
                    dragable: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class": "btn btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                    ],
                });

                $("#dialogPantalla").html(result);
                $('#dialogPantalla').dialog('option', 'position', 'center');
            }
        });
        Loading.hide();

    }
    else if (accion == "borrar") {

        $("#dialogPantalla").html('¿Desea Inhabilitar esta Aplicación?');

        var dialog = $("#dialogPantalla").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp;Inhabilitar Aplicación",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacion";
                        var urlDir = "/catalogo/aplicacion/eliminacion?id=" + id;
                        var datos = {id: id, accion: accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function() {
                            $('#aplicacion-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };

                        $("html, body").animate({scrollTop: 0}, "fast");
                        if (datos) {
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    }
                }

            ],
        });
        Loading.hide();

    }
    else if (accion == "activar") {
        $("#dialogPantalla").html('Estas seguro que desea activarlo?');
        var dialog = $("#dialogPantalla").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Activar Aplicación",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacion";
                        var urlDir = "/catalogo/aplicacion/activar?id=" + id;
                        var datos = {id: id, accion: accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function() {
                            $('#aplicacion-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };

                        $("html, body").animate({scrollTop: 0}, "fast");
                        if (datos) {
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    }
                }
            ],
        });
        Loading.hide();
    }
}


