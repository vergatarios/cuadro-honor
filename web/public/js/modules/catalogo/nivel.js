
$('#Nivel_nombre').bind('keyup blur', function() {
    makeUpper(this);
    keyAlphaNum(this, true, true);
});
$('#Nivel_cantidad').bind('keyup blur', function() {
    keyNum(this, false);// true acepta la ñ y para que sea español
});
$('#Nivel_nombre').bind('blur', function() {
    clearField(this);
});
$('#Nivel_cant_lapsos').bind('keyup blur', function() {
    keyNum(this, false);// true acepta la ñ y para que sea español
});



function consultarNivel(id) {
    direccion = 'informacion';
    title = 'Detalles del nivel';
    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantallaConsultar").removeClass('hide').dialog({
                modal: true,
                width: '800px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    }

                ]
            });
            $("#dialogPantallaConsultar").html(result);
        }
    });
    Loading.hide();
}

function registrarNivel() {
    direccion = 'registrar';
    title = 'Crear nuevo nivel';
    Loading.show();
    //var data = {id: id};
    $.ajax({
        url: direccion,
        //data: data,
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '620px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            registrar();
                        }
                    }
                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

function registrar()
{
    direccion = 'crear';
    var nombre = $('#Nivel_nombre_form').val();
    var nivel = $('#Nivel_tipo_periodo_id').val();
    var cantidad = $('#Nivel_cantidad_form').val();
    var cant_lapsos = $('#Nivel_cant_lapsos_form').val();
    var permite_materia_pendiente = $('#permite_materia_pendiente').val();

    var data = {Nivel: {nombre: nombre, nivel: nivel, cantidad: cantidad, cant_lapsos: cant_lapsos, permite_materia_pendiente: permite_materia_pendiente}};
    //executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
    executeAjax('dialogPantalla', direccion, data, true, true, 'POST', refrescarGrid);

    $('#Nivel_nombre_form').val('');
    $('#Nivel_tipo_periodo_id').val('');
    $('#Nivel_cantidad_form').val('');
    $('#Nivel_cant_lapsos_form').val('');

}

function modificarNivel(id) {

    //alert(id);
    direccion = 'modificar';
    title = 'Modificar Nivel';
    Loading.show();
    var data = {id: id};
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '620px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            procesarCambio();
                        }
                    }
                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

function procesarCambio()
{
    direccion = 'procesarCambio';
    var id = $('#id').val();
    var nombre = $('#Nivel_nombre_form').val();
    var nivel = $('#Nivel_tipo_periodo_id').val();
    var cantidad = $('#Nivel_cantidad_form').val();
    var cant_lapsos = $('#Nivel_cant_lapsos_form').val();
    var permite_materia_pendiente = $('#permite_materia_pendiente').val();
    var data = {Nivel: {id: id, nombre: nombre, nivel: nivel, cantidad: cantidad, cant_lapsos: cant_lapsos, permite_materia_pendiente: permite_materia_pendiente}};
    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);

}

function eliminarNivel(id) {
    var dialog = $("#dialogPantallaEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Nivel</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar Nivel",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                        plantel_id: $("#plantel_id").val()
                    };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_form', 'eliminarNivel', data, true, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function activarNivel(id) {
    var dialogAct = $("#dialogPantallaActivacion").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Nivel</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Activar Nivel",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id
                    };
                    executeAjax('_form', 'activarNivel', data, true, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                }
            }
        ]
    });
}
function quitar(id, nivel_id) {
    var dialog = $("#dialogPantallaEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Nivel</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar Nivel",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data =
                            {
                                id: id,
                                nivel_id: nivel_id
                            };
                    executeAjax('listaGrado', '/catalogo/nivel/QuitarGrado', data, true, true, 'GET', '');
                    $(this).dialog("close");
                }
            }
        ]
    });

}

function refrescarGrid() {
    $('#nivel-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}