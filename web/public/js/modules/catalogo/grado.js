   $(document).ready(function() {
        $('#grado-form').on('submit', function(evt) {
            evt.preventDefault();
            isValidDate();
            crearGrado();
        });

        $('#nombre_grado').bind('keyup blur', function() {
            keyTextDash(this, true, true);
            makeUpper(this);
        });
        $('#Grado_nombre').bind('keyup blur', function() {
        keyTextDash(this, true, true);
        makeUpper(this);
        });

   
    });



//------------------->DESPLEGAR DETALLES DE REGISTROS INACTIVOS>>-----


function consultarGradoInactivo(id) {

    direccion = 'consultarGradoInactivo';
    title = 'Detalles de Grado Inactivo';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '1100px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-power-off red'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs ",
                        click: function() {
                            $(this).dialog("close");
                            refrescarGrid();
                        }
                    }
                ]
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

//---------->DEPLEGAR FORMULARIO DE DETALLES PARA REGISTROS ACTIVOS

function consultarGrado(id) {

    direccion = 'consultarGrado';
    title = 'Detalles de Grado';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '1100px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs ",
                        click: function() {
                            $(this).dialog("close");
                            refrescarGrid();
                        }
                    }
                ]
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

//-------------------------------------------->DESPLEGAR FORMULARIO UPDATE

function modificarGrado(id) {
    
    
    direccion = 'modificarGrado';
    title = 'Modificar Grado';
    Loading.show();
    var data = {id: id};
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                       modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                
             
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            procesarCambio();
                        }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}


//--------------------------------------------->desplegar pop-up para registrar

function registrarGrado() {
    
    direccion = 'create';
    title = 'Crear Nueva Grado';
    Loading.show();
    
     //var data = {id: id};
    
    $.ajax({
        url: direccion,
         //data: data,
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                
                modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                          click: function() {
                            crearGrado();
                           // $("#nombre_credencial").val("");
                            }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
               
        }
    });
    Loading.hide();
}



//-------------------------------------------------------->REGISTRAR 
function crearGrado() 
{
    
    direccion = 'crear';
    var nombre = $('#nombre_grado').val();
    
    var data = {nombre: nombre};
  
    //alert(data);
    //executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
    executeAjax('error', direccion, data, true, true, 'POST', refrescarGrid);
    document.getElementById("grado-form").reset();
    
    //refrescarGrid();
     
}


//----------------------------------------------->>MODIFICAR NOMBRE  sin serialize--EN USO
function procesarCambio()
{
    direccion = 'procesarCambio';
    var id = $('#id').val();
    var nombre = $('#nombre_grado').val();
    
    var data = { Grado: {id: id, nombre: nombre} };
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);
    refrescarGrid();
}

//----------------------------------------------->>MODIFICAR NOMBRE con serialize
function procesarCambioDirecto()
{
    direccion = 'procesarCambio';
    
  
    $.ajax({
        url: direccion,
        data: $("#grado-form").serialize(),
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            
            refrescarGrid();
            $('#dialogPantalla').dialog('close');
          
            alert('Su modificacion fue procesada');
            
        }
    });

}

function borrar(id) {
    
    direccion = 'eliminar';
    var dialog = $("#dialogEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Grado</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                    
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogEliminar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    $("#dialogBoxReg").show("slow");
                    $("#dialogBoxHab").hide("slow");
                }
            }
        ]
    });
}

//------------------------------------------------------------------------------
///-----------------------------Reactivar---------------------------------------


function reactivar(id) {
    
    direccion = 'reactivar';
    var dialog = $("#dialogReactivar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Reactivar Grado</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                    
                }
            },
            {
                html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Reactivar",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogEliminar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxReg").hide("slow");
                    $("#dialogBoxHab").show("slow");
                }
            }
        ]
    });
}
//___________________________________________________________________________


function borrar33(id) 
{

    direccion = 'eliminar';

    
    var data = {id: id};
    
  //  alert(id);
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'GET', refrescarGrid);
}
function refrescarGrid(){
    
    $('#grado-grid').yiiGridView('update', {
    data: $(this).serialize()
});
    
}
function cerrar_dialogo()


{

    Loading.show();

//               $("#dialogPantalla").dialog("close");
    window.location.reload();
    document.getElementById("guardo").style.display = "block";


    Loading.hide();


}



