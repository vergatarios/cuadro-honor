
   $(document).ready(function() {
       
         $('#Credencial_nombre').bind('keyup blur', function() {
         keyAlphaNum(this, true, true);
         makeUpper(this);
         });
     
         $('#credencial-form').on('submit', function(evt) {
            evt.preventDefault();
            crearDependencia();
        });

        $('#nombre_credencial').bind('keyup blur', function() {
            keyAlphaNum(this, true, true);
            //makeUpper(this);
        });

        $('#nombre_credencial').bind('blur', function() {
            clearField(this);
        });
    });


//-------------------------------------------------->DEPLEGAR FORMULARIO DE DETALLES 

function consultarCredencial(id) {

    direccion = 'consultarCredencial';

    title = 'Detalles de Credencial';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '1100px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs ",
                        click: function() {
                            $(this).dialog("close");
                            refrescarGrid();
                        }
                    }
                ]
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

//-------------------------------------------->DESPLEGAR FORMULARIO UPDATE

function modificarCredencial(id) {
    
    
    direccion = 'modificarCredencial';

    title = 'Modificar Credencial';
    Loading.show();
    var data = {id: id};
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                       modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                
             
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                            refrescarGrid();
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            procesarCambio();
                            refrescarGrid();
                        }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}


//--------------------------------------------->desplegar pop-up para registrar

function registrarCredencial() {

    direccion = 'create';
    title = 'Crear Nueva Credencial';
    Loading.show();
    
     //var data = {id: id};
    
    $.ajax({
        url: direccion,
         //data: data,
        dataType: 'html',
        type: 'get',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                
                modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                          click: function() {
                            crearCredencial();
                            refrescarGrid();
                           // $("#nombre_credencial").val("");
                            }

                    }

                ],
                
            });
            $("#dialogPantalla").html(result);
               
        }
    });
    Loading.hide();
}



//-------------------------------------------------------->REGISTRAR 
function crearCredencial() 
{
    

    direccion = 'crear';

    var nombre = $('#nombre_credencial').val();
    
    var data = {nombre: nombre};
    //alert(data);
    executeAjax('error', direccion, data, false, true, 'POST', refrescarGrid);
   document.getElementById("credencial-form").reset();
     
}


//----------------------------------------------->>MODIFICAR NOMBRE  sin serialize
function procesarCambio()
{

    direccion = 'procesarCambio';

    var id = $('#id').val();
    var nombre = $('#nombre_credencial').val();
    
    var data = { Credencial: {id: id, nombre: nombre} };
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);
    refrescarGrid();

}

//----------------------------------------------->>MODIFICAR NOMBRE con serialize
function procesarCambioDirecto()
{

    direccion = 'procesarCambio';

    
  
    $.ajax({
        url: direccion,
        data: $("#credencial-form").serialize(),
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            
            refrescarGrid();
            $('#dialogPantalla').dialog('close');
          
            alert('Su modificacion fue procesada');
            
        }
    });

}

function borrar(id) {
    

    direccion = 'eliminar';

    var dialog = $("#dialogEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Credencial</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                    
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogEliminar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxReg").show("slow");
                    $("#dialogBoxHab").hide("slow");
                    refrescarGrid();
                }
            }
        ]
    });
}

//* .-------------------REACTIVACION .-------------------------------------------

function reactivar(id) {
    

    direccion = 'reactivar';

    var dialog = $("#dialogReactivar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Reactivar Credencial</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                    
                }
            },
            {
                html: "<i class='icon-ok bigger-110'></i>&nbsp; Reactivar",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogReactivar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxReg").hide("slow");
                    $("#dialogBoxHab").show("slow");
                }
            }
        ]
    });
}

//------------------------------------------------------------------------------

function borrar33(id) 
{
    

    direccion = 'eliminar';

    
    var data = {id: id};
  //  alert(id);
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'GET', refrescarGrid);
}
function refrescarGrid(){
    
    $('#credencial-grid').yiiGridView('update', {
    data: $(this).serialize()
});
    
}
function cerrar_dialogo()


{

    Loading.show();

//               $("#dialogPantalla").dialog("close");
    window.location.reload();
    document.getElementById("guardo").style.display = "block";


    Loading.hide();


}





