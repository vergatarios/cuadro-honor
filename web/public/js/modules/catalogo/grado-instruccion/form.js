/**
 * Created by isalaz01 on 22/09/14.
 */
$(document).ready(function(){
    $('#GradoInstruccion_nombre').unbind('keyup');
    $('#GradoInstruccion_nombre').bind('keyup', function() {
        keyAlphaNum(this,true,true);
        makeUpper(this);
    });
    $('#GradoInstruccion_nombre').unbind('blur');
    $('#GradoInstruccion_nombre').bind('blur', function() {
        clearField(this);
    });
});