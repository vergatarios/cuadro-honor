/**
 * Created by ignacio on 15/09/14.
 */
$(document).ready(function() {
    $('#CampoConocimiento_nombre').bind('keyup blur', function() {
        makeUpper(this);
        keyText(this, true);
    });
    $('#CampoConocimiento_estatus').bind('keyup blur', function() {
        makeUpper(this);
        keyText(this, true);
    });
});