function dialogo_error(mensaje) {
    $("#dialog_error p").html(mensaje);
    var dialog = $("#dialog_error").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Mensaje de Error</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}
function dialog_success(mensaje, scroll) {
    $("#dialog_success p").html(mensaje);
    var dialog_success = $("#dialog_success").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Registro Exitoso</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $("#dialog_success p").html('');
                    dialog_success.dialog("close");
                    if (scroll) {
                        scrollUp("fast");
                    }
                }
            }
        ]
    });
}
function verDetalleGrado(id, plan_id) {
    var url = "/catalogo/planEstudio/verDetalleGrado/";
    var datos = {
        id: id,
        plan_id: plan_id
    };
    executeAjax('detalleGrado', url, datos, true, true, 'get');
    $("#detalleGrado").removeClass('hide').dialog({
        width: 500,
        resizable: false,
        draggable: false,
        position: ['center', 50],
        modal: true,
        title: "<div class='widget-header'><h4 class='smaller blue'><i class='icon-search'></i> Asignaturas </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    $("#detalleGrado").dialog("close");
                }
            }
        ],
        close: function() {
            $("#detalleGrado").html("");
        }
    });
}

$(document).ready(function() {
    var nivel_id, divResult, urlDir, datos, conEfecto, showHTML, method, callback;
    $('.look-data-plan').unbind('click');
    $('.look-data-plan').on('click',
            function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var plan_id = $(this).attr('data-plan_id');
                verDetalleGrado(id, plan_id);
            }
    );
    $('#Plan_nombre').bind('keyup blur', function() {
        keyLettersAndSpaces(this, true);
        //clearField(this);
    });
    //clearField(this);
    $("#Plan_nivel_id").on("select2-selecting", function(e) {
        nivel_id = e.val;
        if (nivel_id !== null && nivel_id !== '') {
            divResult = 'gridGrados';
            urlDir = '/catalogo/planEstudio/gradosNivel/';
            datos = {nivel_id: nivel_id};
            conEfecto = true;
            showHTML = true;
            method = 'get';
            callback = null;
            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
            $("#wBoxGrados").removeClass('hide');
        }
    });
    $("#nivel_id").on("select2-selecting", function(e) {
        var nivel_id = e.val;
        var plan_id = $("#plan_id").val();
        var codigo_plan = $("#codigo_plan").val();

        var nivel_id_encoded = base64_encode(nivel_id);
        if (nivel_id !== null && nivel_id !== '' && codigo_plan !== '') {
            var divResult = 'gradosAsignaturas';
            var urlDir = '/catalogo/planEstudio/obtenerGradosAsignaturas/';
            var datos = {id: plan_id, nivel_id: nivel_id_encoded, codigo_plan: base64_encode(codigo_plan)};
            var conEfecto = true;
            var showHTML = true;
            var method = 'get';
            var callback = function() {
                $("#wBoxAsignaturasGrados").removeClass('hide');
            };
            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
        }
    });
    $('#Plan_cod_plan').bind('keyup blur', function() {
        keyNum(this, false);
        clearField(this);
    });

    $("#btnGuardarPlan").click(function() {
        $("#errorSummary").addClass('hide');
        $("#errorSummary").html('');
        $.ajax({
            url: "/catalogo/planEstudio/crearPlan",
            data: $("#plan-estudio-form").serialize(),
            dataType: 'html',
            type: 'post',
            success: function(resp, resp1, resp3) {
                try {
                    var json = jQuery.parseJSON(resp3.responseText);
                    if (json.statusCode === "mensajeError") {
                        dialog_error(json.mensaje);
                    }
                    else
                    if (json.statusCode === "mensajeSuccess") {

                        $("#Plan_nombre").val('');
                        $("#Plan_credencial_id").val('');
                        $("#Plan_fund_juridico_id").val('');
                        $("#Plan_mencion_id").val('');
                        $("#Plan_cod_plan").val('');
                        $("#errorSummary").html('');
                        $("#errorSummary").addClass('hide');
                        dialog_success(json.mensaje, "fast");
                        /*
                         * Permite sincronizar la pestaña asignaturas  segun el nivel que posee el plan
                         */
                        var divResult = 'gradosAsignaturas';
                        var urlDir = '/catalogo/planEstudio/obtenerGradosAsignaturas/';
                        var datos = {id: json.id, nivel_id: json.nivel_id, codigo_plan: json.cod_plan};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = 'get';
                        var callback = function() {
                            $(".tab-pane").removeClass('active');
                            $(".nav-tabs li.active").removeClass('active');
                            $("#asignaturasGrados").addClass('active');
                            $("#asignaturasPlan").addClass('active');
                            $("#wBoxAsignaturasGrados").removeClass('hide');
                        };
                        executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                    }
                } catch (e) {
                    scrollUp("fast");
                    $("#errorSummary").html(resp).removeClass('hide');
                }

            }
        });

    });
    $("#btnModificarPlan").click(function() {
        var plan_id = $("#plan_id").val();
        $.ajax({
            url: "/catalogo/planEstudio/actualizarDatosGeneralesPlan",
            data: $("#plan-estudio-form").serialize(),
            dataType: 'html',
            type: 'post',
            success: function(resp, resp1, resp3) {
                try {
                    var json = jQuery.parseJSON(resp3.responseText);
                    if (json.statusCode === "mensajeError") {
                        dialog_error(json.mensaje);
                    }
                    else
                    if (json.statusCode === "mensajeSuccess") {
                        $("#errorSummary").html('');
                        $("#errorSummary").addClass('hide');
                        dialog_success(json.mensaje, "fast");
//                        /*
//                         * Permite sincronizar la pestaña asignaturas  segun el nivel que posee el plan
//                         */
//                        var divResult = 'gradosAsignaturas';
//                        var urlDir = '/catalogo/planEstudio/obtenerGradosAsignaturas/';
//                        var datos = {id: json.id, nivel_id: json.nivel_id};
//                        var conEfecto = true;
//                        var showHTML = true;
//                        var method = 'get';
//                        var callback = function() {
////                            $(".tab-pane").removeClass('active');
////                            $(".nav-tabs li.active").removeClass('active');
////                            $("#asignaturasGrados").addClass('active');
////                            $("#asignaturasPlan").addClass('active');
////                            $("#wBoxAsignaturasGrados").removeClass('hide');
//                        };
//                        executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                    }
                } catch (e) {
                    scrollUp("fast");
                    $("#errorSummary").html(resp).removeClass('hide');
                }

            }
        });

    });



});
function cambiarEstatusAsignatura(grado, plan, asignatura, description, accion) {

    var accionDes = new String();
    var boton = new String();
    var botonClass = new String();

    $('#confirm-description').html(description);

    if (accion === 'A') {
        accionDes = 'Activar';
        boton = "<i class='icon-ok bigger-110'></i>&nbsp; Activar Asignatura";
        botonClass = 'btn btn-primary btn-xs';
    } else {
        accionDes = 'Inactivar';
        boton = "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar Asignatura";
        botonClass = 'btn btn-danger btn-xs';
    }

    $(".confirm-action").html(accionDes);

    $("#confirm-status").removeClass('hide').dialog({
        width: 800,
        resizable: false,
        draggable: false,
        modal: true,
        position: ['center', 50],
        title: "<div class='widget-header'><h4 class='smaller'><i class='icon-warning-sign red'></i> Cambio de Estatus de Asignatura</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: boton,
                "class": botonClass,
                click: function() {

                    var divResult = "div-result-message";
                    var urlDir = "/catalogo/planEstudio/cambiarEstatusAsignatura/";
                    var datos = {grado_id: grado, plan_id: plan, asignatura_id: asignatura, accion: accion};
                    var conEfecto = true;
                    var showHTML = true;
                    var method = "POST";
                    var callback = function() {
                        //refrescarGrid();
                    };
                    $.ajax({
                        type: method,
                        url: urlDir,
                        dataType: "html",
                        data: datos,
                        beforeSend: function() {
                            if (conEfecto) {
                                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                                displayHtmlInDivId(divResult, url_image_load);
                            }
                            // mostrarNotificacion();
                            peticionActiva = true;
                        },
                        afterSend: function() {
                            peticionActiva = false;
                        },
                        success: function(resp, textStatus, jqXHR) {
                            try {
                                var json = jQuery.parseJSON(jqXHR.responseText);
                                //refrescarGrid();
                                if (json.statusCode == 'success') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'exito', json.mensaje);
                                    urlDir = '/catalogo/planEstudio/buscarAsignaturas/';
                                    datos = {grado_id: json.grado, plan_id: json.plan};
                                    executeAjax('materiasAsignadas', urlDir, datos, conEfecto, showHTML, method);
                                    $("#" + divResult).removeClass('hide');
                                }
                                else if (json.statusCode == 'error') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'error', json.mensaje);
                                }
                            } catch (e) {
                                if (showHTML) {
                                    displayHtmlInDivId(divResult, resp, conEfecto);
                                }
                            }
                        },
                        statusCode: {
                            404: function() {
                                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            },
                            400: function() {
                                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
                            },
                            401: function() {
                                displayDialogBox(divResult, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            403: function() {
                                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            500: function() {
                                if (typeof callback == "function")
                                    callback.call();
                                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            },
                            503: function() {
                                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            },
                            999: function(resp) {
                                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            //alert(thrownError);
                            if (xhr.status == '401') {
                                document.location.href = "http://" + document.domain + "/";
                            } else if (xhr.status == '400') {
                                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            } else if (xhr.status == '500') {
                                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            } else if (xhr.status == '503') {
                                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            }
                            else if (xhr.status == '999') {
                                displayDialogBox('dialog_asignarPlan', "error", xhr.status + ': ' + xhr.responseText);
                            }
                        }

                    });
                    $(this).dialog("close");

                }
            }

        ]
    });

}


function cambiarEstatusAsignatura(grado, plan, asignatura, description, accion) {

    var accionDes = new String();
    var boton = new String();
    var botonClass = new String();

    $('#confirm-description').html(description);

    if (accion === 'A') {
        accionDes = 'Activar';
        boton = "<i class='icon-ok bigger-110'></i>&nbsp; Activar Asignatura";
        botonClass = 'btn btn-primary btn-xs';
    } else {
        accionDes = 'Inactivar';
        boton = "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar Asignatura";
        botonClass = 'btn btn-danger btn-xs';
    }

    $(".confirm-action").html(accionDes);

    $("#confirm-status").removeClass('hide').dialog({
        width: 800,
        resizable: false,
        draggable: false,
        modal: true,
        position: ['center', 50],
        title: "<div class='widget-header'><h4 class='smaller'><i class='icon-warning-sign red'></i> Cambio de Estatus de Asignatura</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: boton,
                "class": botonClass,
                click: function() {

                    var divResult = "div-result-message";
                    var urlDir = "/catalogo/planEstudio/cambiarEstatusAsignatura/";
                    var datos = {grado_id: grado, plan_id: plan, asignatura_id: asignatura, accion: accion};
                    var conEfecto = true;
                    var showHTML = true;
                    var method = "POST";
                    var callback = function() {
                        //refrescarGrid();
                    };
                    $.ajax({
                        type: method,
                        url: urlDir,
                        dataType: "html",
                        data: datos,
                        beforeSend: function() {
                            if (conEfecto) {
                                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                                displayHtmlInDivId(divResult, url_image_load);
                            }
                            // mostrarNotificacion();
                            peticionActiva = true;
                        },
                        afterSend: function() {
                            peticionActiva = false;
                        },
                        success: function(resp, textStatus, jqXHR) {
                            try {
                                var json = jQuery.parseJSON(jqXHR.responseText);
                                //refrescarGrid();
                                if (json.statusCode == 'success') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'exito', json.mensaje);
                                    urlDir = '/catalogo/planEstudio/buscarAsignaturas/';
                                    datos = {grado_id: json.grado, plan_id: json.plan};
                                    executeAjax('materiasAsignadas', urlDir, datos, conEfecto, showHTML, method);
                                    $("#" + divResult).removeClass('hide');
                                }
                                else if (json.statusCode == 'error') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'error', json.mensaje);
                                }
                            } catch (e) {
                                if (showHTML) {
                                    displayHtmlInDivId(divResult, resp, conEfecto);
                                }
                            }
                        },
                        statusCode: {
                            404: function() {
                                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            },
                            400: function() {
                                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
                            },
                            401: function() {
                                displayDialogBox(divResult, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            403: function() {
                                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            500: function() {
                                if (typeof callback == "function")
                                    callback.call();
                                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            },
                            503: function() {
                                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            },
                            999: function(resp) {
                                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            //alert(thrownError);
                            if (xhr.status == '401') {
                                document.location.href = "http://" + document.domain + "/";
                            } else if (xhr.status == '400') {
                                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            } else if (xhr.status == '500') {
                                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            } else if (xhr.status == '503') {
                                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            }
                            else if (xhr.status == '999') {
                                displayDialogBox('dialog_asignarPlan', "error", xhr.status + ': ' + xhr.responseText);
                            }
                        }

                    });
                    $(this).dialog("close");

                }
            }

        ]
    });
}


function actualizarAsignatura(id, hora_teorica, hora_practica, grado, plan, asignatura, description, accion) {

    var accionDes = new String();
    var boton = new String();
    var botonClass = new String();

    $('#confirm-description').html(description);

//        accionDes = 'Activar';
        boton = "<i class='icon-ok bigger-110'></i>&nbsp; Actualizar Asignatura";
        botonClass = 'btn btn-primary btn-xs';

//    $(".confirm-action").html(accionDes);

    $("#update-status").removeClass('hide').dialog({
        width: 800,
        resizable: false,
        draggable: false,
        modal: true,
        position: ['center', 50],
        title: "<div class='widget-header'><h4 class='smaller'><i class='icon-warning-sign red'></i> Actualización de Asignatura</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: boton,
                "class": botonClass,
                click: function() {

                    var divResult = "div-result-message";
                    var urlDir = "/catalogo/planEstudio/actualizarAsignatura/";
                    var datos = {id: id, hora_teorica: hora_teorica, hora_practica:hora_practica, grado_id: grado, plan_id: plan, asignatura_id: asignatura, accion: accion};
                    var conEfecto = true;
                    var showHTML = true;
                    var method = "POST";
                    var callback = function() {
                        //refrescarGrid();
                    };
                    $.ajax({
                        type: method,
                        url: urlDir,
                        dataType: "html",
                        data: datos,
                        beforeSend: function() {
                            if (conEfecto) {
                                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                                displayHtmlInDivId(divResult, url_image_load);
                            }
                            // mostrarNotificacion();
                            peticionActiva = true;
                        },
                        afterSend: function() {
                            peticionActiva = false;
                        },
                        success: function(resp, textStatus, jqXHR) {
                            try {
                                var json = jQuery.parseJSON(jqXHR.responseText);
                                //refrescarGrid();
                                if (json.statusCode == 'success') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'exito', json.mensaje);
                                    urlDir = '/catalogo/planEstudio/buscarAsignaturas/';
                                    datos = {grado_id: json.grado, plan_id: json.plan};
                                    executeAjax('materiasAsignadas', urlDir, datos, conEfecto, showHTML, method);
                                    $("#" + divResult).removeClass('hide');
                                }
                                else if (json.statusCode == 'error') {
                                    $("#div-result-message").removeClass('hide');
                                    displayDialogBox('div-result-message', 'error', json.mensaje);
                                }
                            } catch (e) {
                                if (showHTML) {
                                    displayHtmlInDivId(divResult, resp, conEfecto);
                                }
                            }
                        },
                        statusCode: {
                            404: function() {
                                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            },
                            400: function() {
                                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
                            },
                            401: function() {
                                displayDialogBox(divResult, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            403: function() {
                                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            500: function() {
                                if (typeof callback == "function")
                                    callback.call();
                                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            },
                            503: function() {
                                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            },
                            999: function(resp) {
                                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            //alert(thrownError);
                            if (xhr.status == '401') {
                                document.location.href = "http://" + document.domain + "/";
                            } else if (xhr.status == '400') {
                                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            } else if (xhr.status == '500') {
                                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            } else if (xhr.status == '503') {
                                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            }
                            else if (xhr.status == '999') {
                                displayDialogBox('dialog_asignarPlan', "error", xhr.status + ': ' + xhr.responseText);
                            }
                        }

                    });
                    $(this).dialog("close");

                }
            }

        ]
    });

}