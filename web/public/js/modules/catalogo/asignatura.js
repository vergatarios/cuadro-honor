
//----------------------validacion de campos basicos


   $(document).ready(function() {
       
       $('#Asignatura_nombre').bind('keyup blur', function() {
        keyTextDash(this, true, true);
        makeUpper(this);
        });

        $('#Asignatura_abreviatura').bind('keyup blur', function() {
            keyAlpha(this, true);
            makeUpper(this);

            clearField(this);
        });

        $('#asignatura-form').on('submit', function(evt) {
            evt.preventDefault();
            crearAsignatura();
        });

        $('#nombre_asignatura').bind('keyup blur', function() {
            keyTextDash(this, true, true);
            //makeUpper(this);
        });

        $('#nombre_asignatura').bind('blur', function() {
            clearField(this);
        });
    });



//------------------->DESPLEGAR DETALLES DE REGISTROS INACTIVOS>>-----


function consultarAsignaturaInactivo(id) {

    direccion = 'consultarAsignaturaInactivo';
    title = 'Detalles de Asignatura Inactivo';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '700px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-power-off red'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs ",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

//---------->DEPLEGAR FORMULARIO DE DETALLES PARA REGISTROS ACTIVOS

function consultarAsignatura(id) {

    direccion = 'consultarAsignatura';
    title = 'Detalles de Asignatura';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '700px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs ",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

//-------------------------------------------->DESPLEGAR FORMULARIO UPDATE

function modificarAsignatura(id) {
    
    
    direccion = 'modificarAsignatura';
    title = 'Modificar Asignatura';
    Loading.show();
    var data = {id: id};
    //alert(id);
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                       modal: true,
                width: '700px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                
             
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            procesarCambio();
                        }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}


//--------------------------------------------->desplegar pop-up para registrar

function registrarAsignatura() {
    
    direccion = 'create';
    title = 'Crear Nueva Asignatura';
    Loading.show();
    
     //var data = {id: id};
    
    $.ajax({
        url: direccion,
         //data: data,
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                
                modal: true,
                width: '700px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                          click: function() {
                            crearAsignatura();
                           // $("#nombre_credencial").val("");
                            }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
               
        }
    });
    Loading.hide();
}



//-------------------------------------------------------->REGISTRAR 
function crearAsignatura() 
{
    
    direccion = 'crear';
    var nombre = $('#nombre_asignatura').val();
    var abrev = $('#abreviatura_asignatura').val();
    var data = {nombre: nombre, abrev:abrev};
    //alert(data);
    //executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
    executeAjax('error', direccion, data, true, true, 'POST', refrescarGrid);
    document.getElementById("asignatura-form").reset(); 
}


//----------------------------------------------->>MODIFICAR NOMBRE  sin serialize--EN USO
function procesarCambio()
{
    direccion = 'procesarCambio';
    var id = $('#id').val();
    var nombre = $('#nombre_asignatura').val();
    var abrev = $('#abreviatura_asignatura').val();
   // alert(abrev);
    
    var data = { Asignatura: {id: id, nombre: nombre, abrev:abrev} };
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);

}

//----------------------------------------------->>MODIFICAR NOMBRE con serialize
function procesarCambioDirecto()
{
    direccion = 'procesarCambio';
    
  
    $.ajax({
        url: direccion,
        data: $("#asignatura-form").serialize(),
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            
            refrescarGrid();
            $('#dialogPantalla').dialog('close');
          
            alert('Su modificacion fue procesada');
            
        }
    });

}

//------------------------------------------------------------------------------
//.---------------------.DESACTIVACION DE REGISTROS.----------------------------

function borrar(id) {
    
    direccion = 'eliminar';
    var dialog = $("#dialogEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Asignatura</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                    
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogEliminar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxHab").hide("slow");
                    $("#dialogBoxReg").show("slow");
                }
            }
        ]
    });
}


//-------------------------------------------------------------------------------

//_____________________________.REACTIVACION.___________________________________

function reactivar(id) {
    

    direccion = 'reactivar';

    var dialog = $("#dialogReactivar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Reactivar Credencial</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                    
                }
            },
            {
                html: "<i class='icon-ok bigger-110'></i>&nbsp; Reactivar",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogReactivar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxReg").hide("slow");
                    $("#dialogBoxHab").show("slow");
                }
            }
        ]
    });
}

//------------------------------------------------------------------------------

function eliminacionPrueba(id) 
{
    
    direccion = 'eliminar';
    
    var data = {id: id};
  //  alert(id);
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'GET', refrescarGrid);
}
function refrescarGrid(){
    
    $('#asignatura-grid').yiiGridView('update', {
    data: $(this).serialize()
});
    
}
function cerrar_dialogo()


{

    Loading.show();

//               $("#dialogPantalla").dialog("close");
    window.location.reload();
    document.getElementById("guardo").style.display = "block";


    Loading.hide();


}




