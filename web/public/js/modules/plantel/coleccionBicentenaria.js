$(document).on('ready', function() {


});

$('#guardarColeccionB').bind('click', function(e) {
    e.preventDefault();
    var cantidad;
    var condicionServicio;
    var plantel_id;

    plantel_id = $('#ServicioPlantelPeriodo_plantel_id').val();
    condicionServicio = $('#ServicioPlantelPeriodo_condicion_servicio_id').val();
    cantidad = $('#ServicioPlantelPeriodo_cantidad').val();
//    $("#tipoReporteText").html("Estadístico Diario");
//    $("#selectTipoReporteText").html(": Estadístico Diario");
    //$("html, body").animate({ scrollTop: 0 }, "fast");
    if (plantel_id != '' && condicionServicio != '' && cantidad != '') {

        var datos = 'cantidad=' + cantidad + '&condicionServicio=' + condicionServicio + '&plantel_id=' + plantel_id;
        var divResultAlerta = 'msgAlerta';
        var divResult = "coleccionBicentenaria";
        var urlDir = "/planteles/ColeccionBicentenaria/GuardarBicentenaria";
        var method = "POST";
        var msg = "<p>Estimado usuario, debe ingresar todos los campos presente.</p>";
        var style = 'alerta';
        $.ajax({
            url: urlDir,
            data: datos,
            dataType: 'html',
            type: method,
            success: function(respuesta) {
                $("#servicio-plantel-periodo-form").html(respuesta);
            },
            error: function(respuesta) {
                $("#coleccionBicentenaria").html(respuesta);

            }
        });
    } else {


        displayDialogBox(divResultAlerta, style, msg);

    }
});