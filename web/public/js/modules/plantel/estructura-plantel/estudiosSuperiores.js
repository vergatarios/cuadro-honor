


function VentanaDialogEstSup(id,direccion,title,accion,datos) 
{



    accion=accion;
    Loading.show();
    var data =
            { 
                id: id,
                datos:datos  
            };




     if(accion=="registroPersonalEstudio" || accion=="edicionPersonalEstudio")
     {    
         
         
  
         
         
         
            $.ajax({
                url: direccion,
                data: data,
                dataType: 'html',
                type: 'GET',
                success: function(result,action)
                {
                    var dialog = $("#dialogPantallaPersonalEstudio").removeClass('hide').dialog({
                        modal: true,
                        width: '1100px',
                        dragable: false,
                        resizable: false,


                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='icon-book'></i> " + title + "</h4></div>",
                        title_html: true,

                                buttons: [
                                {   
                                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                    "class": "btn btn-danger",
                                    click: function() 
                                    {
                                        $(this).dialog("close");
                                    } // fin de la funcion click
                                },
                                {   
                                    html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                                    "class": "btn btn-primary",                                    

                                    click: function() 
                                    {


                                        var divResult = "dialogPantallaPersonalEstudio";
                                        var urlDir = "/planteles/estructura/"+accion+"/"+id;
                                        var datos = $("#personal-estudio-form").serialize();
                                        var conEfecto = true;
                                        var showHTML = true;
                                        var method = "POST";
                                        var callback = function(){
                                            $('#personal-estudio-grid').yiiGridView('update', {
                                                data: $(this).serialize()
                                            });
                                        };

                                        $("html, body").animate({ scrollTop: 0 }, "fast");
                                        if(datos){
                                        executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);

                                        }
                                        else{
                                        $(this).dialog("close");    
                                        }


                                    } // fin de la funcion click
                                },


                            ],






                                });






                        $("#dialogPantallaPersonalEstudio").html(result);
                    } // fin del success
                });
                Loading.hide();
        
    } // fin del if create o update 

    else if(accion=="consultaPersonalEstudio")
    {

        $.ajax({
            url: direccion,
            data: data,
            dataType: 'html',
            type: 'GET',
            success: function(result,action)
            {
                var dialog = $("#dialogPantallaPersonalEstudio").removeClass('hide').dialog({
                    modal: true,
                    width: '1100px',
                    dragable: false,
                    resizable: false,


                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='icon-book'></i> " + title + "</h4></div>",
                    title_html: true,

                            buttons: [
                            {   
                                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                                "class": "btn btn-danger",
                                click: function() {
                                    $(this).dialog("close");
                                }
                            },


                        ],






                            });






                    $("#dialogPantallaPersonalEstudio").html(result);
                }
            });
            Loading.hide();


    }  // fin del if view

    else if(accion=="borrarPersonalEstudio")
    {

         $("#dialogPantallaPersonalEstudio").html('<div class="alert alert-warning"> ¿Esta seguro que desea inactivar este registro?</div>');

        var dialog = $("#dialogPantallaPersonalEstudio").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,


            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='icon-book'></i> " + title + "</h4></div>",
            title_html: true,

                    buttons: [
                                {
                                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                                    "class": "btn btn-xs",
                                    click: function() 
                                    {
                                        $(this).dialog("close");
                                    } // fin de la funcion click
                                },
                                {
                                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar ",
                                    "class": "btn btn-danger btn-xs",
                                    click: function() 
                                    {
                                                    var divResult = "resultadoOperacionPersonalEstudio";
                                                    var urlDir = "/planteles/estructura/"+accion+"/";
                                                    var datos = {id:id, accion:accion};
                                                    var conEfecto = true;
                                                    var showHTML = true;
                                                    var method = "POST";
                                                    var callback = function(){
                                                        $('#personal-estudio-grid').yiiGridView('update', {
                                                            data: $(this).serialize()
                                                        });
                                                    };

                                                    $("html, body").animate({ scrollTop: 0 }, "fast");
                                                    if(datos){
                                                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                                                    $(this).dialog("close");
                                                    }
                                      } // fin de la funcion click
                                  }
                ],

                        });


        Loading.hide();


    }
    
    else if(accion=="activarPersonalEstudio")
    {

         $("#dialogPantallaPersonalEstudio").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Esta seguro que desea activar este registro? </p></div>');

        var dialog = $("#dialogPantallaPersonalEstudio").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,


            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + "</h4></div>",
            title_html: true,

                    buttons: [
                                {
                                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                                    "class": "btn btn-xs",
                                    click: function() {
                                        $(this).dialog("close");
                                    }
                                },
                                {
                                    html: "<i class='icon-check bigger-110'></i>&nbsp; Reactivar",
                                    "class": "btn btn-success btn-xs",
                                    click: function() {
                                                    var divResult = "resultadoOperacionPersonalEstudio";
                                                    var urlDir = "/planteles/estructura/"+accion+"/";
                                                    var datos = {id:id, accion:accion};
                                                    var conEfecto = true;
                                                    var showHTML = true;
                                                    var method = "POST";
                                                    var callback = function(){
                                                        $('#personal-estudio-grid').yiiGridView('update', {
                                                            data: $(this).serialize()
                                                        });
                                                    };

                                                    $("html, body").animate({ scrollTop: 0 }, "fast");
                                                    if(datos){
                                                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                                                    $(this).dialog("close");
                                                    }
                                                }
                                            }
                                ], // botones


                        });


        Loading.hide();


    }

} // fin de la funcion principal







function cerrarDialog()
{
      $("#dialogoVentana").dialog("close");   
    
    
    
} // fin de la funcion cerrar la ventana de dialogo // 



