// div_atributo_docente




function dialogo_error(mensaje,title,redireccionar) {
    displayDialogBox('dialog_error','info',mensaje);
    //$("#dialog_error p").html(mensaje);
    var dialog = $("#dialog_error").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> "+title+" </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if(redireccionar){
                        window.location.reload();
                    }
                }
            }
        ]
    });
}
$(document).ready(function(){
    



        // inicio atributo cobra  
        $("input[name= 'PersonalPlantel[cobra]' ]").change(function () {	 

        var cobra=$(this).val();                    
        var labora=$("input:radio[name='PersonalPlantel[labora]']:checked").val();




        if(cobra=="S" && labora=="S")
        {

             $("#etiqueta_dependencia").html("");
             $('#div_seccion_dependencia').hide(); 
        }

        if(cobra=="S" && labora=="N")
        {

             $("#etiqueta_dependencia").html("¿En qu&eacute; plantel labora?");
             $('#div_seccion_dependencia').show(); 
        }

        if(cobra=="N" && labora=="S")
        {

             $("#etiqueta_dependencia").html("¿En qu&eacute; plantel cobra?");
             $('#div_seccion_dependencia').show(); 
        }

        if(cobra=="N" && labora=="N")
        {

             $("#etiqueta_dependencia").html("");
             $('#div_seccion_dependencia').hide(); 
        }






        });  // fin del atributo cobra
                        
                        
                        
        // inicio atributo labora  
        $("input[name= 'PersonalPlantel[labora]' ]").change(function () {	 

        var labora=$(this).val();                    
        var cobra=$("input:radio[name='PersonalPlantel[cobra]']:checked").val();


        if(cobra=="S" && labora=="S")
        {

             $("#etiqueta_dependencia").html("");
             $('#div_seccion_dependencia').hide(); 
        }

        if(cobra=="S" && labora=="N")
        {

             $("#etiqueta_dependencia").html("¿En qu&eacute; plantel labora?");
             $('#div_seccion_dependencia').show(); 
        }

        if(cobra=="N" && labora=="S")
        {

             $("#etiqueta_dependencia").html("¿En qu&eacute; plantel cobra?");
             $('#div_seccion_dependencia').show(); 
        }

        if(cobra=="N" && labora=="N")
        {

             $("#etiqueta_dependencia").html("");
             $('#div_seccion_dependencia').hide(); 
        }






    });  // fin del atributo labora 
    
    
    
    //$("#Personal_documento_identidad").mask("999999999999999");
    $('#Personal_nombres').attr('readOnly','readOnly');
    $('#Personal_apellidos').attr('readOnly','readOnly');
    //
    //$("#Personal_telefono_fijo").mask("99999999999");
    ///$("#Personal_telefono_celular").mask("99999999999");
    $('#Personal_documento_identidad').unbind('keyup');
    $('#Personal_documento_identidad').bind('keyup', function() {
        makeUpper(this);
        keyAlphaNum(this,false,false);
    
    });
    
    
    $('#Personal_correo').unbind('keyup');
    $('#Personal_correo').bind('keyup', function() {
        makeUpper(this);
        keyEmail(this,true);
    });
    
    
    
    $('#PersonalPlantel_plantel_id_dep_pers').unbind('keyup');
    $('#PersonalPlantel_plantel_id_dep_pers').bind('keyup', function() {
    makeUpper(this);
    keyAlphaNum(this,false,false);

    });
        
        
     $('#Personal_telefono_fijo').unbind('keyup');
        $('#Personal_telefono_fijo').bind('keyup', function() {
        makeUpper(this);
        keyAlphaNum(this,false,false);
    
     }); 
     
     
     $('#Personal_telefono_celular').unbind('keyup');
        $('#Personal_telefono_celular').bind('keyup', function() {
        makeUpper(this);
        keyAlphaNum(this,false,false);
    
     });
     
     $('#PersonalPlantel_denominacion_especificacion').unbind('keyup');
        $('#PersonalPlantel_denominacion_especificacion').bind('keyup', function() {
        makeUpper(this);
        //keyAlphaNum(this,false,false);
    
     });
        
    
    
    


    $('#EstatusDocente_id').unbind('change');
    $('#EstatusDocente_id').bind('change', function() {
       
       var estatusDocente_id= $(this).val(); 
       var data= {
                    estatusDocente_id: estatusDocente_id        
                 };
             
        $.ajax({
        url: "/planteles/estructura/actualizarComboEspecificacionEstatus",
        data: data,
        dataType: 'html',
        type: 'POST',
        success: function(resp) 
        {
           $("#Personal_especificacion_estatus_id").html(resp);  
            
        }
         });
       
       
       
    });
    
    
    
        
    $('#PersonalPlantel_denominacion_id').unbind('change');
    $('#PersonalPlantel_denominacion_id').bind('change', function() {
       
        var denominacion_id= $(this).val();
        var denom_doc_aula=$('#denom_doc_aula').val();         
        
        if(denominacion_id==denom_doc_aula) 
        {
            $('#div_atributo_denom_espec').hide();
        }
        else
        {
            $('#div_atributo_denom_espec').show(); 
        }
                  
       
      }); // fin de la funcionalidad de cambio de opcion para la denominacion
    
    
    
    
    $('#PersonalPlantel_tipo_personal_id').unbind('change');
    $('#PersonalPlantel_tipo_personal_id').bind('change', function() {
       
       
       
          var personalPlantel_tipo_personal_id= $(this).val();
        //var tp_personal_docente=$('#tp_personal_docente').val();
        
        // solicitado y mandado a quitar, si lo piden de nuevo se debe descomentar y listo
        /*if(personalPlantel_tipo_personal_id==tp_personal_docente) 
        {
            $('#div_atributo_docente').show();
        }
        else
        {
            $('#div_atributo_docente').hide(); 
        }*/
            
        


        var data= {
                     personalPlantel_tipo_personal_id: personalPlantel_tipo_personal_id        
                  };
                  
                  
         $.ajax({
                    url: "/planteles/estructura/actualizarComboDenominacion",
                    data: data,
                    dataType: 'html',
                    type: 'POST',
                    success: function(resp) 
                    {
                       $("#PersonalPlantel_denominacion_id").html(resp);  

                    }
                }); 
        

         $.ajax({
                    url: "/planteles/estructura/actualizarComboEspecialidadPersonal",
                    data: data,
                    dataType: 'html',
                    type: 'POST',
                    success: function(resp) 
                    {
                       $("#PersonalPlantel_especialidad_id").html(resp);  

                    }
                });


         $.ajax({
                    url: "/planteles/estructura/actualizarComboFuncionPersonal",
                    data: data,
                    dataType: 'html',
                    type: 'POST',
                    success: function(resp) 
                    {
                       $("#PersonalPlantel_funcion_id").html(resp);  

                    }
           });
       
       
       
      }); // fin de la funcionalidad de cambio de opcion para el tipo de personal
    
    
     
      
     $('#PersonalPlantel_horas_asignadas').click(function() {
                   
        $.mask.definitions['A'] = '[0-5]';
        $.mask.definitions['B'] = '[0-9]';
        $.mask.definitions['C'] = '[0-9]';
        $.mask.definitions['D'] = '[0-9]';
        $("#PersonalPlantel_horas_asignadas").mask('AB.CD'); 
    
      });  
    
    
    
        /*$.mask.definitions['A'] = '[1-5]';
        $.mask.definitions['B'] = '[0-9]';
        $.mask.definitions['C'] = '[0-9]';
        $.mask.definitions['D'] = '[0-9]';
        $("#PersonalPlantel_horas_asignadas").mask('AB.CD');  */     
        
        $('#PersonalPlantel_horas_asignadas').unbind('change, blur');
        $("#PersonalPlantel_horas_asignadas").bind('change, blur', function() {
             
             
             
             
             var horas_sin_formato=$(this).val();
             var horas_con_formato="";

             horas_con_formato=horas_sin_formato.replace("__", "00");
             horas_con_formato=horas_sin_formato.replace("_", "0");
             horas_con_formato=horas_con_formato.replace("__", "00");
             horas_con_formato=horas_con_formato.replace("_", "0");
             
             if(horas_con_formato=="00.00")
             {
                 $(this).val("01.00");
             }
             else
             {
                 $(this).val(horas_con_formato);
             }

            


        }); // fin de funcionalidad para dar formato al campo horas asignadas 
    

    
}); // fin del document.ready 



    
    
    
    

    
    

    
function buscaPersonal(documento_identidad)
{ 


       

        var tdocumento_identidad=$('#Personal_tdocumento_identidad').val();
        
        
        
        if (documento_identidad != '' || documento_identidad != null) 
        {
            
        $.ajax({
            url: "/planteles/estructura/obtenerDatosPersona",
            data: {
                    documento_identidad: documento_identidad,
                    tdocumento_identidad: tdocumento_identidad
                   
                  },
            dataType: 'json',
            type: 'POST',
            success: function(resp) 
            {   
                
                $("#Personal_nombres").val("");
                $("#Personal_apellidos").val("");                
                $("#Personal_correo").val("");
                $("#Personal_sexo").val("");
                $("#Personal_fecha_nacimiento").val("");
                $("#Personal_telefono_fijo").val("");
                $("#Personal_telefono_celular").val("");
                $("#Personal_grado_instruccion_id").val("");
                $("#Personal_especificacion_estatus_id").val("");
                $("#PersonalPlantel_tipo_personal_id").val("");
                
                
                
                
                
                
                
                $("#Personal_nombres").attr('readonly', true);
                $("#Personal_apellidos").attr('readonly', true);
                $("#Personal_telefono_fijo").attr('readonly', false);                        
                $("#Personal_telefono_celular").attr('readonly', false);
                $("#Personal_correo").attr('readonly', false);
                
               

                if(resp!=null)
                {
                    
                                
                    
                    if(resp.statusCode=="SUCCESS")
                    {
                        $("#Personal_nombres").val(resp.nombres);
                        $("#Personal_apellidos").val(resp.apellidos);
                        $("#Personal_fecha_nacimiento").val(resp.fecha_nacimiento);
                        $("#Personal_sexo").val(resp.sexo);
                    }  
                    
                    if(resp.statusCode=="SUCCESS_2")
                    {
                        $("#Personal_nombres").val(resp.nombres);
                        $("#Personal_apellidos").val(resp.apellidos);
                        $("#Personal_correo").val(resp.correo);
                        $("#Personal_sexo").val(resp.sexo);
                        $("#Personal_fecha_nacimiento").val(resp.fecha_nacimiento);
                        $("#Personal_telefono_fijo").val(resp.telefono_fijo);
                        $("#Personal_telefono_celular").val(resp.telefono_celular);
                        $("#Personal_grado_instruccion_id").val(resp.grado_instruccion_id);
                        $("#Personal_especificacion_estatus_id").val(resp.especificacion_estatus_id);                        
                        
                        //$("#Personal_telefono_fijo").attr('readonly', true);                        
                        //$("#Personal_telefono_celular").attr('readonly', true);
                        //$("#Personal_correo").attr('readonly', true);


                    }

                    
                    if(resp.statusCode =='ERROR')
                    {
                        
                        //alert("seccion de error");
                        dialogo_error(resp.mensaje,resp.title);
                        
                    }
                    
                    
                    
                    
                }
                
                
 
            } // FIN DEL success
        });
    } // si la cedula es diferente de null o vacia
    
    
    
} // funcion para buscar la cedula de la persona a la cual se le va a crear la solicitud 
  




    
    






