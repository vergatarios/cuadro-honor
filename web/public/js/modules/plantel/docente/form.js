/**
 * Created by isalaz01 on 25/09/14.
 */
function dialogo_error(mensaje,title,redireccionar) {
    displayDialogBox('dialog_error','info',mensaje);
    //$("#dialog_error p").html(mensaje);
    var dialog = $("#dialog_error").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> "+title+" </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if(redireccionar){
                        window.location.reload();
                    }
                }
            }
        ]
    });
}
$(document).ready(function(){
    $('#Docente_nombres').attr('readOnly','readOnly');
    $('#Docente_apellidos').attr('readOnly','readOnly');
    $('#Docente_documento_identidad').unbind('keyup');
    $('#Docente_documento_identidad').bind('keyup', function() {
        makeUpper(this);
        keyAlphaNum(this,false,false);
    });

    $('#Docente_documento_identidad').unbind('blur');
    $('#Docente_documento_identidad').bind('blur', function() {
        clearField(this);
        var documento_identidad = $(this).val();
        var id_nombres="Docente_nombres";
        var id_apellidos="Docente_apellidos";
        var tdocumento_identidad = $('#Docente_tdocumento_identidad').val();
        var title;
        var mensaje;
        var divResult='';
        var urlDir = '/planteles/docente/obtenerDatosPersona';
        var datos;
        var loadingEfect=false;
        var showResult=false;
        var method='GET';
        var responseFormat='json';
        var beforeSendCallback=function(){};
        var successCallback;
        var errorCallback=function(){};

        if(documento_identidad !=null && documento_identidad != '' ){
            if(tdocumento_identidad !=null && tdocumento_identidad != '' ){
                datos=$('#docente-form').serialize();
                successCallback=function(response){
                    if(response.statusCode =='SUCCESS'){
                        $('#'+id_apellidos).val(response.apellidos);
                        $('#'+id_nombres).val(response.nombres);
                    }
                    if(response.statusCode =='ERROR'){
                        dialogo_error(response.mensaje,response.title);
                        $('#'+id_apellidos).val('');
                        $('#'+id_nombres).val('');
                        $(this).val('');
                    }
                };
                executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
            }
            else {
                title = 'Notificación de Error';
                mensaje='Estimado usuario debe seleccionar un Tipo de Documento de Identidad Valido.';
                dialogo_error(mensaje,title);
            }

        }

    });
});