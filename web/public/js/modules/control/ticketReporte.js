$(document).on('ready', function(){
    
    $("#repEstadistico").on('click', function(evt){
        evt.preventDefault();
        reporteEstadistico('region', 'x');
    });
    
    $("#repGrafico").on('click', function(evt){
        evt.preventDefault();
        reporteGraficoTicket();
    });
    
    $('#control_zona_observacion').bind('keyup blur', function () {
        keyText(this, true);
    });
    
    $('#control_zona_observacion').bind('blur', function () {
        clearField(this);
    });
    
});
function reporteEstadistico(nivel, dependency){
    $("#fechaCondicion").addClass('hide');
    var divResult = "resultado";
     var urlDir = "/control/estadisticoTicket/estadisticas";
    var datos = 'nivel='+nivel+'&dependency='+dependency;
    var conEfecto = true;
    var showHTML = true;
    var method = "POST";
    var callback = null;
    $("#tipoReporteText").html("Estadístico General");
    $("#selectTipoReporteText").html(": Estadístico General");
    //$("html, body").animate({ scrollTop: 0 }, "fast");
    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
    
}

function reporteEstadisticoEstado(id){
    $("#fechaCondicion").addClass('hide');
    var divResult = "resultado";
     var urlDir = "/control/estadisticoTicket/estadisticasEstado";
    var datos = 'id='+id;
    var conEfecto = true;
    var showHTML = true;
    var method = "POST";
    var callback = null;
    $("#tipoReporteText").html("Estadístico General");
    $("#selectTipoReporteText").html(": Estadístico General");
    //$("html, body").animate({ scrollTop: 0 }, "fast");
    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
    
}


function reporteEstadisticoEstatus(id){
    $("#fechaCondicion").addClass('hide');
    var divResult = "resultado";
     var urlDir = "/control/estadisticoTicket/estadisticasEstatus";
    var datos = 'id='+id;
    var conEfecto = true;
    var showHTML = true;
    var method = "POST";
    var callback = null;
    $("#tipoReporteText").html("Estadístico General");
    $("#selectTipoReporteText").html(": Estadístico General");
    //$("html, body").animate({ scrollTop: 0 }, "fast");
    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
    
}
