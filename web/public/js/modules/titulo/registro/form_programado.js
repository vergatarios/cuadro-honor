$(document).ready(function(evt){

    $('#FormRegistroProgramadoSerial_serial_inicial').bind('keyup blur', function () {
        keyNum(this, false);
    });

    $('#FormRegistroProgramadoSerial_serial_inicial').bind('blur', function () {
        clearField(this);
    });

    $('#FormRegistroProgramadoSerial_serial_final').bind('keyup blur', function () {
        keyNum(this, false);
    });

    $('#FormRegistroProgramadoSerial_serial_final').bind('blur', function () {
        clearField(this);
    });

    $('#FormRegistroProgramadoSerial_prefijo').bind('keyup blur', function () {
        keyAlpha(this, false);
        makeUpper(this);
    });

    $('#FormRegistroProgramadoSerial_prefijo').bind('blur', function () {
        clearField(this);
    });
});