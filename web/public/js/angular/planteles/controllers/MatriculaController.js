/**
 * Created by isalaz01 on 12/12/14.
 */
var matricula = angular.module('Matricula',['trNgGrid','ngResource']);

matricula.factory('getEstudiantesInterceptor',['$rootScope',function($rootScope) {
        return {
            request: function (config) {
                return config;
            },
            response: function(response) {
                if(response.data.status =="SUCCESS"){
                    $('#loadingWidget').addClass('hide');
                    $rootScope.estudiantesSinMatricular = response.data.estudiantes;
                    $(".contenidoInscripcion").removeClass('hide').fadeIn();
                }
                if(response.data.status =="ERROR"){
                    $('#loadingWidget').addClass('hide');
                    displayDialogBox('msgAlerta', response.data.classDialog, response.data.mensaje);
                }
            },
            responseError : function(response){
            }

        }
    }
    ]
);
matricula.factory('getEstudianteInterceptor',['$rootScope',function($rootScope) {
        return {
            request: function (config) {
                return config;
            },
            response: function(response) {
                $rootScope.msgLoadingEstud='Procesando Respuesta...';
                if(response.data.status =="SUCCESS"){
                    $('#loadingWidgetEstudDialog').addClass('hide');
                    $('#respuestaBusqueda').removeClass('hide');
                    $('#gridEstudBusqueda').removeClass('hide');
                    $rootScope.estudiantesBusqueda =response.data.estudiantes;
                }
                if(response.data.status == "NO_EXISTE"){
                    $('#loadingWidgetEstudDialog').addClass('hide');
                    $('#respuestaBusqueda').removeClass('hide');
                    displayDialogBox('respuestaBusqueda', response.data.classDialog, response.data.mensaje,true,true);
                }
                if(response.data.status =="ERROR"){
                    $('#loadingWidgetEstudDialog').addClass('hide');
                    $('#respuestaBusqueda').removeClass('hide');
                    displayDialogBox('respuestaBusqueda', response.data.classDialog, response.data.mensaje,true,true);
                }
            },
            responseError : function(response){
                $('#loadingWidgetEstudDialog').addClass('hide');
                $('#respuestaBusqueda').removeClass('hide');
                displayDialogBox('respuestaBusqueda', response.data.classDialog, response.data.mensaje,true,true)
            }
        }
    }
    ]
);
matricula.factory('getEstudiantesService', ['$resource', 'getEstudiantesInterceptor','getEstudianteInterceptor', function($resource, getEstudiantesInterceptor,getEstudianteInterceptor) {

        return $resource(
            "/",
            {},
            {
                buscarEstudiante: {method: 'GET', isArray: false, interceptor: getEstudianteInterceptor,url: "/planteles/matricula15/getEstudiante/"},
                buscarEstudiantes: {method: 'GET', isArray: false, interceptor: getEstudiantesInterceptor,url: "/planteles/matricula15/getEstudiantes/"}


            }
        );
    }
    ]
);
matricula.filter('ageFilter', function () {
        function calculateAge (fecha_nacimiento) {
            var date = new Date(fecha_nacimiento);
            var ageDifMs = Date.now() - date.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }
        return function (unused,item) {
            return calculateAge(item.fecha_nacimiento);
        };
    }
);
matricula.filter("documentoIdentidadFilter", function () {
        return function (documento_identidad, estudiante) {
            var identificacion;
            if (estudiante.documento_identidad != '' && estudiante.documento_identidad != null) {
                if (estudiante.tdocumento_identidad != '' && estudiante.tdocumento_identidad != null) {
                    identificacion = estudiante.tdocumento_identidad + '-' + estudiante.documento_identidad;
                } else {
                    identificacion = estudiante.documento_identidad;
                }
            } else {
                if (estudiante.cedula_escolar != '' && estudiante.cedula_escolar != null) {
                    identificacion = 'C.E: ' + estudiante.cedula_escolar;
                } else {
                    identificacion = 'NO POSEE';
                }
            }
            return identificacion;
        };
    }
);
matricula.controller('MatriculaController',function($scope,$http,getEstudiantesService) {
        $scope.estudSelecSinMatricular=[];
        $scope.estudSelecPreInscritos=[];
        $scope.eestudiantesSinMatricular=[];
        $scope.estudiantesPreInscritos=[];

        $scope.patternNumbers = /^\d+$/;
        $scope.estudiante = {
            tdocumento_identidad:'',
            documento_identidad:'',
            nombres:'',
            apellidos:'',
            cedula_escolar:'',
            ci_representante:''
        };
        $scope.myLocale = 'es';
        $scope.defaultOrder = 'documento_identidad';


        $scope.alertOnSelectionChange = function(){
            $scope.$watch("estudSelecSinMatricular.length", function(){});
            $scope.$watch("estudSelecPreInscritos.length", function(){});
        };

        $scope.getEstudiantes = function(){
            var params = {
                seccion_plantel_id: $scope.seccion_plantel_id,
                plantel_id: $scope.plantel_id,
                periodo_id: $scope.periodo_id
            };
            getEstudiantesService.buscarEstudiantes(params);
        };

        $scope.inicializar = function(){
            var defaultTranslation = {};
            var esTranslation = angular.extend({}, defaultTranslation,
                {
                    "Search": "Buscar",
                    "Page":"Página",
                    "First Page": "Primera Página",
                    "Next Page": "Página Siguiente",
                    "Previous Page": "Página Anterior",
                    "Last Page": "Última Página",
                    "Sort": "Ordenar",
                    "No items to display": "No se ha encontrado ningun resultado",
                    "displayed": "mostrando",
                    "in total": "en total"
                }
            );
            esTranslation[TrNgGrid.translationDateFormat] = "dd/MM/yyyy";
            TrNgGrid.translations["es"] = esTranslation;
            TrNgGrid.columnSortActiveCssClass = "tr-ng-sort-active text-info";
            TrNgGrid.columnSortInactiveCssClass = "tr-ng-sort-inactive text-muted ";
            TrNgGrid.columnSortReverseOrderCssClass = "tr-ng-sort-order-reverse icon-chevron-down";
            TrNgGrid.columnSortNormalOrderCssClass = "tr-ng-sort-order-normal icon-chevron-up";

            $scope.seccion_plantel_id = $("#seccion_plantel_id").val();
            $scope.plantel_id = $("#plantel_id").val();
            $scope.periodo_id = $("#periodo_id").val();
            $scope.grado_id = $("#grado_id").val();
            $scope.msgLoadingInit='Cargando estudiantes...';
            $('#loadingWidget').removeClass('hide');
            $('#wBoxInscripcion').click();
            $scope.getEstudiantes();
        };

        $scope.pushEstudiantePreInscrito = function(item) {
            if(item){
                $scope.estudiantesPreInscritos.push(item);
                $scope.estudiantesSinMatricular.splice($scope.estudiantesSinMatricular.indexOf(item),1);
            }
            else {
                if ($scope.estudSelecSinMatricular.length) {
                    angular.forEach($scope.estudSelecSinMatricular, function (estudiante) {
                        $scope.estudiantesPreInscritos.push(estudiante);
                        $scope.estudiantesSinMatricular.splice($scope.estudiantesSinMatricular.indexOf(estudiante),1);
                    });
                }
                $scope.estudSelecSinMatricular = [];
            }
        };

        $scope.pushEstudianteSinMatricular = function(item){
            if(item){
                $scope.estudiantesSinMatricular.push(item);
                $scope.estudiantesPreInscritos.splice($scope.estudiantesPreInscritos.indexOf(item),1);
            }
            else {
                angular.forEach($scope.estudSelecPreInscritos, function (estudiante) {
                    $scope.estudiantesSinMatricular.push(estudiante);
                    $scope.estudiantesPreInscritos.splice($scope.estudiantesPreInscritos.indexOf(estudiante),1);

                });
                $scope.estudSelecPreInscritos = [];
            }
        };

        $scope.getEstudiante = function(){
            var params = {
                seccion_plantel_id: $scope.seccion_plantel_id,
                plantel_id: $scope.plantel_id,
                periodo_id: $scope.periodo_id,
                grado_id: $scope.grado_id,
                tdocumento_identidad: $scope.estudiante.tdocumento_identidad,
                documento_identidad: $scope.estudiante.documento_identidad,
                nombres: $scope.estudiante.nombres,
                apellidos: $scope.estudiante.apellidos,
                cedula_escolar: $scope.estudiante.cedula_escolar,
                ci_representante: $scope.estudiante.ci_representante,
            };
            $('#loadingWidgetEstudDialog').removeClass('hide');
            $scope.msgLoadingEstud='Realizando la búsqueda...';
            getEstudiantesService.buscarEstudiante(params);

        };

        $scope.dialogEstudianteExistente = function () {
            $('#respuestaBusqueda').addClass('hide');
            $('#gridEstudBusqueda').addClass('hide');
            $('#loadingWidgetEstudDialog').addClass('hide');
            $scope.estudiantesBusqueda =[];
            $("#incluir_Estudiante").removeClass('hide').dialog({
                    width: 900,
                    resizable: false,
                    draggable: false,
                    modal: true,
                    position: ['center', 50],
                    title: "<div class='widget-header'><h4 class='smaller blue'><i class='icon-search blue'></i> Búsqueda de Estudiantes</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp;Volver",
                            "class": "btn btn-xs btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                    ]
                }
            );
        };
    }

);